<?php
// ---------------------------
//
// FOUNDRY ADMIN LOGIN RECALL
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
//
include("inc/_core/loader.php");

$admloginquery = getRequestVar('admloginquery');
if($admloginquery == '') $admloginquery = $_SERVER['QUERY_STRING'];

$admerr = "";
$remoteip = md5($_SERVER['REMOTE_ADDR']);
$ready_to_reset_pwd = false;
if(getRequestVar('ready_to_reset_pwd') != '') $ready_to_reset_pwd = true;
$k = getRequestVar('k');
$n = getRequestVar('n');

// create login form trigger events
$nonce = $_sec->createEncString();
$args = serialize(array(
    "url" => WEB_URL.ADMIN_FOLDER."admloginrecall.php?n=".$nonce."&admrequest_".$nonce,
    "admerr" => $admerr,
));
$_events->createTriggerEvent('admin_loginrecall_altform', $args, '');
$args = serialize(array(
    "host" => WEB_URL,
));
$_events->createTriggerEvent('admin_loginrecall_validate', $args, '');
$_events->createTriggerEvent('admin_loginrecall', WEB_URL, '', 'altlogin');

if (getRequestVar('admrequest_'.$n) != ''){
    $useremail = getRequestVar('admuser_'.$n);
    if($useremail != ""){
        $acct = $_db_control->setTable(ACCOUNTS_TABLE)->setWhere("username = '$useremail' OR email = '$useremail'")->setLimit()->getRec();
        if (count($acct) > 0){
            if($acct[0]['activated'] == 1){
                if($acct[0]['email'] != ''){
                    $pkey = $acct[0]['pcle'];
                    $link = WEB_URL.ADMIN_FOLDER."admloginrecall.php?k=".$pkey;
                    $msg = "Hello,\n\n";
                    $msg.= "We hear that you are having problems recalling your password.\n\n";
                    $msg.= "Let's help you reset your ".SITE_NAME." Admin account password. Please follow the instructions below to set a new password.\n\n";
                    $msg.= "If you did not initiate this reset request, please disregard and/or delete this email and your password will not be changed.\n\n";
                    $msg.= "Click the following link to set a new password:\n\n";
                    $msg.= $link."\n\n";
                    $msg.= "If clicking the link doesn't work you can copy it into your browser's address bar or type it there directly.\n\n";
                    $msg.= "Regards,\n\n";
                    $msg.= SITE_NAME." Admin System";
                    $result = MailerClass::sendmail(array($acct[0]['email']), array(ADMIN_EMAIL => SITE_NAME), SITE_NAME." Admin Account - Password Reset", $msg);
                    $admerr = "The password reset email has been sent.  Check your email in a moment.";
                    $_error->saveEventLog($acct[0]['id'], "Core", "Login (Recall)", "notice", "A password recovery email was sent to '".$acct[0]['email']."'.");
                }else{
                    $admerr = "Your account was found, but an email address was not recorded.";
                    $_error->saveEventLog($acct[0]['id'], "Core", "Login (Recall)", "notice", "Password recovery incomplete because no email address was saved to user record.");
                }
            }else{
                $admerr = "Your account has not been activated.  Please contact the system administrator.";
                $_error->saveEventLog($acct[0]['id'], "Core", "Login (Recall)", "notice", "Password recovery failed because user account has not been activated.");
            }
        }else{
            $admerr = "Your username/email did not match account records!";
            $_error->saveEventLog(0, "Core", "Login (Recall)", "notice", "Password recovery attempt using user/email='$useremail' failed.");
        }
    }else{
        $admerr = "Please provide a username or email address!";
        $_error->saveEventLog(0, "Core", "Login (Recall)", "notice", "Password recovery failed because user/email missing.");
    }
} elseif (!empty($k)) {
    $acct = $_db_control->setTable(ACCOUNTS_TABLE)->setWhere("pcle = '$k'")->setLimit()->getRec();
    if(count($acct) > 0){
        $ready_to_reset_pwd = true;
        if(getRequestVar('admpwdreset_'.$n) != ''){
            // save new password
            $admerr = "";
            $user = getRequestVar('admuser_'.$n);
            $pwd  = getRequestVar('admpwd_'.$n);
            $pwdc = getRequestVar('admpwdconfirm_'.$n);

            if($user == '' || $pwd == '' || $pwdc == '') {
                $admerr = "The username and both passwords are required.";
            } elseif($user != $acct[0]['username']) {
                $admerr = "The username is not the username for this account.";
            } elseif($pwd != $pwdc) {
                $admerr = "The passwords were not the same.";
            } else {
                $pwd = $_sec->createHash($pwd, false);
                $phash = '';
                $pcle = md5($user.$pwd);
                if($_db_control->setTable(ACCOUNTS_TABLE)->setFieldvals(array("pcle" => $pcle, "phash" => $phash, "password" => $pwd))->setWhere("id = ".$acct[0]['id'])->updateRec()){
                    // log user into system
					$_SESSION['admlogin'] = true;
                    $_SESSION['admuserid'] = $acct[0]['id'];
                    $_SESSION['admuserlevel'] = $acct[0]['level'];
					setcookie('admlogin', date("Y-m-d H:i:s"), time()+3600*24*(2), '/');
                    $_db_control->setTable(SESSION_TABLE)->setFieldvals(array("user_id" => $acct[0]['id'], "ip_hash" => $remoteip, "username" => $user, "section" => "admin", "logged_in" => 1, "logged_in_date" => date("Y-m-d H:i:s")))->insertRec();
                    $_error->saveEventLog($acct[0]['id'], "Core", "Login (Recall)", "notice", "Password recovery succeeded and password changed.");
					$_filesys->gotoPage (WEB_URL.ADMIN_FOLDER);	// return to home page
                }
            }
        }
    }else{
        $admerr = "The password reset link you clicked on or typed in is invalid.  Please try again.";
        $_error->saveEventLog($acct[0]['id'], "Core", "Login (Recall)", "notice", "Password recovery failed because reset key invalid.");
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo BUSINESS?> Admin: Forgot Password</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="<?php echo COPYRIGHT_NAME?>" />
<meta name="distribution" content="Global" />
<meta name="content-language" content="EN" />
<?php
$_themes->prepHeadSection();
list($alt_login_plugins_active, , , $obj) = $_events->executeTrigger('admin_loginrecall');
if (!is_null(getRequestVar('admrequest_'.$n)) && $alt_login_plugins_active){
    // validate login using alternate login recall plugin
    list($altadmerr, , , $obj) = $_events->executeTrigger('admin_loginrecall_validate');
    if(!empty($altadmerr)) $admerr = $altadmerr;
}

$smtpserverconnect = MailerClass::checkSMTPHost(SMTP_HOST, SMTP_SECURE_MODE, SMTP_PORT);
?>
</head>

<body class="login">
    <div id="gotowebsite_loginbtn">
        <a href="<?php echo WEB_URL?>">
            <i class="fa fa-home fa-lg"></i>
            &lt;&lt; Return to <?php echo SITE_NAME?> Home Page
        </a>
    </div>
	<div id="wrapper">
		<div id="display_core_msg"></div>
		<div id="display_runtime_msg"></div>
		<div id="content-wrapper" class="login-wrap">
			<div id="admloginbox" class="clearfix">
				<h2>Create a New Login Password</h2>
                <div id="admlogininnerbox">
                    <?php if($smtpserverconnect !== false) { ?>
                        <?php if(!$ready_to_reset_pwd){ ?>
        				<p>Enter your username or email address so that a password renewal email can be sent with instructions on re-establishing your password.</p>
                        <div class="admerror<?php echo (($admerr != '') ? ' shown' : '')?>"><span><?php echo $admerr?></span></div>
        				<form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
                            <input type="hidden" name="admloginquery" value="<?php echo $admloginquery;?>"/>
                            <input type="hidden" name="n" value="<?php echo $nonce;?>"/>
        					<p>
                                <label for="admuser">Username or Email:</label><br />
                                <input type="text" name="admuser_<?php echo $nonce?>" id="admuser" size="15" value="" />
                            </p>
        					<div style="text-align: center; margin-top: 15px; clear: both;">
        						<input type="submit" name="admrequest_<?php echo $nonce?>" id="admrequest" value="Send Instructions"/>
        					</div>
        				</form>
                        <?php } else { ?>
        				<p>You're almost finished. Please enter your username and a NEW password below.</p>
                        <div class="admerror<?php echo (($admerr != '') ? ' shown' : '')?>"><span><?php echo $admerr?></span></div>
        				<form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
                            <input type="hidden" name="ready_to_reset_pwd" value="<?php echo $ready_to_reset_pwd;?>"/>
                            <input type="hidden" name="k" value="<?php echo $k;?>"/>
                            <input type="hidden" name="n" value="<?php echo $nonce;?>"/>
        					<p>
                                <label for="admuser">Username:</label><br />
                                <input type="text" name="admuser_<?php echo $nonce?>" id="admuser" size="15" value="" />
                            </p>
        					<p>
                                <label for="admpwd">Password:</label><br />
                                <input type="password" name="admpwd_<?php echo $nonce?>" id="admpwd" size="15" value="" autocomplete="off" />
                            </p>
        					<p>
                                <label for="admpwdconfirm">Password Confirmation:</label><br />
                                <input type="password" name="admpwdconfirm_<?php echo $nonce?>" id="admpwdconfirm" size="15" value="" autocomplete="off" />
                            </p>
        					<div style="text-align: center; margin-top: 15px; clear: both;">
        						<input type="submit" name="admpwdreset_<?php echo $nonce?>" id="admpwdreset" value="Reset Password and Log Me In"/>
        					</div>
        				</form>
                        <?php } ?>
                    <?php } else { ?>
                        <p>The Mail settings aren't setup properly.  As a result, the login recall feature has been disabled.</p>
                    <?php } ?>
                    <div id="admloginbox_login"><a href="admlogin.php">Try Logging In</a></div>
                </div>
			</div>

<?php
if (!$_users->isloggedin){
?>
            <script type="text/javascript" language="javascript">
	            jQuery('#admuser').focus();
            </script>
<?php }

$_render->showFooter();
?>