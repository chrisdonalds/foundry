<?php
// ---------------------------
//
// FOUNDRY ADMIN LOGIN
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
//
$rurl = urldecode((isset($_REQUEST['rurl']) ? $_REQUEST['rurl'] : ''));
include("inc/_core/loader.php");

// admin login
$admerr = "";
if(VHOST != "/") $rurl = str_replace(VHOST, "", $rurl);
if($rurl == "") $rurl = ADMIN_FOLDER;

$login = array("normal" => "", "alt" => " style=\"display: none\"");
$n = getRequestVar('n');

// create login form trigger events
$nonce = $_sec->createEncString();
$args = serialize(array(
    "url" => WEB_URL.ADMIN_FOLDER."admlogin.php?n=".$nonce."&rurl=".$rurl."&admsubmit_".$nonce,
    "formstate" => $login["alt"],
    "admerr" => $admerr,
    "rurl" => $rurl
));
$_events->createTriggerEvent('admin_login_altform', $args, '', 'altlogin');
$args = serialize(array(
    "formstate" => $login["normal"],
    "host" => WEB_URL,
    "rurl" => getRequestVar('rurl')
));
$_events->createTriggerEvent('admin_login_validate', $args, '', 'altlogin');
$_events->createTriggerEvent('admin_login', WEB_URL, '', 'altlogin');

if (getRequestVar('admsubmit_'.$n) == "Enter"){
    // validate form
    $pwd = getRequestVar('admpwd_'.$n);
    $user = getRequestVar('admuser_'.$n);
    unset($_SESSION['admlogin']);

    $logincount = $_users->getLoginAttempts();
    $maxtries = intval($_users->getMaxLoginAttemptsAllowed());

    if($logincount < $maxtries || $maxtries == 0){
        if(!empty($pwd) && !empty($user)){
            $acct = $_users->getAccountbyLogin($user, $pwd);

            if(is_array($acct)){
                $admerr = $_users->completeLogin($acct, $user, $rurl);
            }else{
                $logincount++;
                $admerr = "The username and/or password is not correct! (Attempt ".$logincount."/".$maxtries.")";
                if($logincount < $maxtries){
                    $_error->saveEventLog(0, "Core", "Login", "notice", "Login attempt (#$logincount) using user='$user', pass='$pwd' failed.");
                }else{
                    $admerr .= "  You won't be able to login for the next 5 minutes.";
                    $_error->saveEventLog(0, "Core", "Login", "warning", "Login attempt (#$logincount) using user='$user', pass='$pwd' failed.  IP address has been blocked for 5 minutes.");
                }
            }
        }else{
            $admerr = "Please provide a valid username and password!";
            $_error->saveEventLog(0, "Core", "Login", "notice", "Login attempt failed because username and/or password missing.");
        }
    }else{
        $admerr = "You are prevented from logging in for 5 minutes since last failed attempt.";
    }
} elseif (getRequestVar('admsubmit') == "Logout"){
    // log user out of system
    if($_users->terminateLogin()) $admerr = "You are now logged out.";
} elseif ($_users->isUserLoggedin()){
    // already logged in elsewhere
    $_filesys->gotoPage(WEB_URL.$rurl);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo BUSINESS?> Admin: Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="<?php echo COPYRIGHT_NAME?>" />
<meta name="distribution" content="Global" />
<meta name="content-language" content="EN" />
<?php
$_themes->prepHeadSection();
$rm_admuser = getIfSet($_COOKIE["admuser"]);
$rm_admpwd = getIfSet($_COOKIE["admpwd"]);

// execute alternate login trigger functions
list($alt_login_plugins_active, , , $obj) = $_events->executeTrigger('admin_login');
$alt_login_obj = (($alt_login_plugins_active) ? $obj[0]['object'] : null);
if (!is_null(getRequestVar('admsubmit_'.$n)) && $alt_login_plugins_active){
    // validate login using alternate login plugin
    list($altadmerr, , , $obj) = $_events->executeTrigger('admin_login_validate');
    if(!empty($altadmerr)) $admerr = $altadmerr;
}

// get logo
$login_img = $_media->getLoginImage();
$logo = $_media->getFlexThumbnail($login_img[0]);
$logo_class = (($login_img[1] == DEF_IMG_LOGO_SYS) ? ' admloginboxsystemlogo' : '');
?>
</head>

<body class="login">
    <div id="wrapper">
        <div id="display_core_msg"></div>
        <div id="display_runtime_msg"></div>
        <div id="gotowebsite_loginbtn">
            <a href="<?php echo WEB_URL?>">
                <i class="fa fa-home fa-lg"></i>
                &lt;&lt; Return to <?php echo SITE_NAME?> Home Page
            </a>
        </div>
        <div id="content-wrapper" class="login-wrap">
            <div id="admloginbox" class="clearfix<?php echo $logo_class ?>"<?php if($login_img != '') echo ' style="background: url(\''.$logo.'\') 50% 0px  no-repeat"'; ?>>
                <h2<?php if($login_img != '') echo ' class="topmargin"';?>>Welcome to <?php echo SYS_NAME ?></h2>
                <div id="admlogininnerbox"<?php echo $login["normal"]?>>
                    <div class="admerror<?php echo (($admerr != '') ? ' shown' : '')?>"><span><?php echo $admerr?></span></div>
                    <form method="post" action="admlogin.php">
                        <input type="hidden" name="rurl" value="<?php echo $rurl;?>"/>
                        <input type="hidden" name="n" value="<?php echo $nonce;?>"/>
                        <div id="admloginuser">
                            <p>
                                <label for="admuser">Username/Email:</label><br />
                                <input type="text" name="admuser_<?php echo $nonce?>" id="admuser" size="15" value="<?php echo $rm_admuser?>" />
                            </p>
                        </div>
                        <div id="admloginpass">
                            <p>
                                <label for="admpwd">Password:</label><br />
                                <input type="password" name="admpwd_<?php echo $nonce?>" id="admpwd" size="15" value="<?php echo $rm_admpwd?>" autocorrect="off" autocapitalize="off" />
                            </p>
                        </div>
                        <div style="margin: 15px 0px; clear: both;">
                            <input type="submit" name="admsubmit_<?php echo $nonce?>" id="admsubmit" value="Enter"/>
                        </div>
                    </form>

                    <div class="admloginbox_opts">
                        <a href="admloginrecall.php?<?php echo $_SERVER['QUERY_STRING']?>">I forgot my password</a>
                        <?php if($alt_login_plugins_active) { ?>&nbsp;|&nbsp;<a href="#" id="admlogin_alt">Alternate Login</a><?php } ?>
                    </div>

                    <?php if(defined('FACEBOOKLOGIN') && FACEBOOKLOGIN == 1) { ?>
                    <div id="admloginsocial">
                        Tip: You can login using Facebook&reg;
                    </div>
                    <?php } ?>
                </div>

                <?php
                if($alt_login_plugins_active){
                    $_events->executeTrigger('admin_login_altform');
                }
                ?>
            </div>
        </div>

<?php
if (getIfSet($_SESSION['admlogin']) != true || isset($rurl)){
?>
            <script type="text/javascript" language="javascript">
                jQuery('#admuser').focus();
            </script>
<?php }

$_render->showFooter();
?>