<?php
// ---------------------------
//
// FOUNDRY ADMIN RE-LOGIN
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
//
$rurl = urldecode((isset($_REQUEST['rurl']) ? $_REQUEST['rurl'] : ''));
include_once("inc/_core/loader.php");

// admin login
if($rurl == "")
    $rurl = ADMIN_FOLDER;
else
    $rurl = str_replace(WEB_URL, "", $rurl);

list($alt_login_plugins_active, , , $obj) = $_events->processTriggerEvent('admin_relogin', WEB_URL, '', 'altrelogin');
$alt_login_obj = (($alt_login_plugins_active) ? $obj[0]['object'] : null);
$login = array("normal" => "", "alt" => " style=\"display: none\"");
$n = getRequestVar('n');
$lcode = getRequestVar('admsubmit');
$nonce = $_sec->createEncString();
switch($lcode){
    case "Logincheck":
        $admerr = "You have been automatically logged out, and will need to login again to continue working...";
        break;
    default:
        $admerr = "";
        break;
}

?>
    <div id="admlogininnerbox"<?php echo $login["normal"]?>>
		<div class="admerror<?php echo (($admerr != '') ? ' shown' : '')?>"><span><?php echo $admerr?></span></div>
		<form method="post" action="<?php echo WEB_URL.ADMIN_FOLDER ?>admlogin.php">
            <input type="hidden" name="rurl" value="<?php echo $rurl;?>"/>
            <input type="hidden" name="n" value="<?php echo $nonce;?>"/>
            <div id="admloginuser">
                <p>
                    <label for="admuser">Username/Email:</label><br />
                    <input type="text" name="admuser_<?php echo $nonce?>" id="admuser" size="15" value="" />
                </p>
            </div>
            <div id="admloginpass">
                <p>
                    <label for="admpwd">Password:</label><br />
                    <input type="password" name="admpwd_<?php echo $nonce?>" id="admpwd" size="15" value="" autocorrect="off" autocapitalize="off" />
                </p>
            </div>
			<div style="margin: 15px 0px; clear: both;">
				<input type="submit" name="admsubmit_<?php echo $nonce?>" id="admsubmit" value="Enter"/>
			</div>
		</form>

        <div class="admloginbox_opts">
            <a href="admloginrecall.php?<?php echo $_SERVER['QUERY_STRING']?>">I forgot my password</a>
            <?php if($alt_login_plugins_active) { ?>&nbsp;|&nbsp;<a href="#" id="admlogin_alt">Alternate Login</a><?php } ?>
        </div>

        <?php if(defined('FACEBOOKLOGIN') && FACEBOOKLOGIN == 1) { ?>
        <div id="admloginsocial">
            Tip: You can login using Facebook&reg;
        </div>
        <?php } ?>
    </div>
<?php
?>

