<?php

// ---------------------------
//
// FOUNDRY ADMIN SETTINGS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
//

if(defined("SHOW_ALL_ERRORS")){
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}
$fu_obj = array();

if(!defined("IN_SETTINGS")) define("IN_SETTINGS", true);
(strpos(SERVER, "adminsys.nav") > 0) ? $is_adminsys = true : $is_adminsys = false;
$_settings->clearAll();

// process actions keyed to add custom settings tabs
$activetabs = $_settings->getSettingsTabs();

foreach($activetabs as $tabkey => $tabarry){
    if(isset($tabarry[0]) && !is_array($tabarry[0])){
        $file = SITE_PATH.ADMIN_FOLDER.SETTINGS_FOLDER."settings-".strtolower($tabkey).".php";
        if(file_exists($file))
            $_filesys->includeFile($file);
    }
}

// ---------------------------
// SAVE SETTINGS
// ---------------------------

if (getRequestVar('cfgsubmit') != ""){
    $_settings->validateChanges($is_adminsys);
}

// ---------------------------
// PAGE CONTENT
// ---------------------------
$_render->showHeader("", "nestedsortable,tabs");
$currenttab = getIfSet($_GET['tab'], "general");

?>

    <div id="content-wrapper">
        <div id="waitoverlay"><br/><p><img src="<?php echo WEB_URL.ADMIN_FOLDER.CORE_FOLDER?>images/general/loading.gif" alt="Please Wait" /> Please Wait...<br/><br/>The <?php echo SYS_NAME; ?> settings are loading</p></div>
    	<div id="contentarea" class="hidden">
    		<div id="title"><?php echo SYS_NAME?> Settings</div>
    		<form id="cfgform" method="post" action="<?php echo $_SERVER['REQUEST_URI'];?>" enctype="multipart/form-data" accept-charset="UTF-8">
    	        <p id="issues" class="setissue<?php if($_settings->getSettingsIssuesCount() == 0) echo ' disabled';?>"><?php $_settings->showSettingsIssues(); ?></p>
                <?php
                $_render->startButtonBlock();
                $_render->showEditorButtons(DEF_SETBUT_SAVE);
                $_render->endBlock();
                ?>
    	        <p class="clearfix"></p>
    			<div id="settingstabs" class="clearfix">
    				<ul class="settingstab-nav">
    					<?php
                        foreach($activetabs as $tabkey => $tabarry){
                            $tabkey = strtolower($tabkey);
                            $is_current = ($currenttab == $tabkey);
                            if(isset($tabarry[0])){
                                if(!is_array($tabarry[0])){
                                    // system tab
                                    list($overhtml) = $_events->processTriggerEvent("settings_override_".$tabkey."_tab_menu");
                                    if(empty($overhtml)){
                                        $func = "settings_".$tabkey."_menu";
                                        if(function_exists($func)) $func($currenttab, $is_current);
                                    }else
                                        echo $overhtml;
                                }else{
                                    // custom tab
                                    foreach($tabarry as $tabobj){
                                        $_settings->showCustomSettingsTab($tabkey, $tabobj);
                                    }
                                }
                            }
                        }
                        ?>
    				</ul>

                    <?php
                    // tab contents
                    foreach($activetabs as $tabkey => $tabarry){
                        if(isset($tabarry[0])){
                            $tabkey = strtolower($tabkey);
                            $is_current = ($currenttab == $tabkey);
                            if(!is_array($tabarry[0])){
                                // system tab
                                if($is_current)
                                    list($overhtml) = $_events->processTriggerEvent("settings_override_".$tabkey."_tab_contents");
                                else
                                    list($overhtml) = $_events->processTriggerEvent("settings_override_".$tabkey."_tab_shell");
                                if(empty($overhtml)){
                                    $func = "settings_".$tabkey."_content_wrapper";
                                    if(function_exists($func)) $func($currenttab, $is_current);
                                }else
                                    echo $overhtml;
                            }else{
                                // custom tab
                                foreach($tabarry as $tabobj){
                                    $_settings->showCustomSettingsTabContent($tabobj, $is_current);
                                }
                            }
                        }
                    }
                    ?>
    			</div>

    			<div id="settingscolorpicker" title="Color Picker" style="display: none"></div>
    		</form>
    	</div>
    </div>

    <div id="admSettingsPopupBalloon"><div class="inner_a"></div><div class="inner_b"></div></div>
	<div id="pluginsettingsdialog" title="Settings" style="display: none;"></div>
    <div id="themesettingsdialog" title="Settings" style="display: none;"></div>

<?php
$_render->showFooter();
?>