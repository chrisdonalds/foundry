<?php

// ---------------------------
//
// FOUNDRY ADMIN SETTINGS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
// GENERAL TAB
//

function settings_general_menu($currenttab){
    global $_users, $_settings, $_events;

    list($retn) = $_events->processTriggerEvent(__FUNCTION__);
    $retn .= ' General';

    if($_users->userIsAllowedTo(UA_VIEW_GENERAL_SETTINGS)) { ?><li><a href="#tabs-general" class="settingstabs_link"><?php echo $retn ?><?php $_settings->showSettingsIssuesIndicator('general'); ?></a></li><?php echo PHP_EOL; }
}

function settings_general_content_wrapper($currenttab, $show){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_VIEW_GENERAL_SETTINGS)) { ?>
    <div id="tabs-general">
        <?php
        if($show){
            settings_general_content($currenttab);
            $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_GENERAL);
        }
        ?>
    </div>
    <?php }
}

function settings_general_content($currenttab){
    global $_system, $_users, $_settings;


    if($_users->userIsAllowedTo(UA_VIEW_GENERAL_SETTINGS)) { ?>
        <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_EDIT_GENERAL_SETTINGS)); ?>
        <p id="issues-general" class="setissue<?php if($_settings->getSettingsIssuesCount('general') == 0) echo ' disabled';?>"><?php $_settings->showSettingsIssues('general'); ?></p>
        <h3 class="header">Business</h3>
        <div class="setlabel">Business Name: <a href="#" class="hovertip" alt="The name of the business (may be the same as the website name)">[?]</a><?php echo REQD_ENTRY?></div>
            <div class="setdata"><input type="text" id="BUSINESS" name="newcfg[BUSINESS]" size="30" value="<?php echo getIfSet($_system->configs['BUSINESS'])?>"/> <?php $_settings->showResetLink('BUSINESS')?></div>
        <div class="setlabel">Website Name: <a href="#" class="hovertip" alt="The name of the site">[?]</a><?php echo REQD_ENTRY?></div>
            <div class="setdata"><input type="text" id="SITE_NAME" name="newcfg[SITE_NAME]" size="30" value="<?php echo getIfSet($_system->configs['SITE_NAME'])?>"/> <?php $_settings->showResetLink('SITE_NAME')?></div>
        <div class="setlabel">Website Short Description: <a href="#" class="hovertip" alt="Use this to record a byline or sentence that describes your site">[?]</a></div>
            <div class="setdata"><input type="text" id="SITE_BYLINE" name="newcfg[SITE_BYLINE]" size="50" value="<?php echo getIfSet($_system->configs['SITE_BYLINE'])?>"/> <?php $_settings->showResetLink('SITE_BYLINE')?></div>
        <div class="setlabel">Owner's Email: <a href="#" class="hovertip" alt="The main public email of the business or site owner">[?]</a><?php echo REQD_ENTRY?></div>
            <div class="setdata"><input type="text" id="OWNER_EMAIL" name="newcfg[OWNER_EMAIL]" size="30" value="<?php echo getIfSet($_system->configs['OWNER_EMAIL'])?>"/> <?php $_settings->showResetLink('OWNER_EMAIL')?></div>
        <div class="setlabel">Administrator Email: <a href="#" class="hovertip" alt="The email used to issue system or critical alerts and messages">[?]</a><?php echo REQD_ENTRY?></div>
            <div class="setdata"><input type="text" id="ADMIN_EMAIL" name="newcfg[ADMIN_EMAIL]" size="30" value="<?php echo getIfSet($_system->configs['ADMIN_EMAIL'])?>"/> <?php $_settings->showResetLink('ADMIN_EMAIL')?></div>
        <div class="setlabel">Business Address: <a href="#" class="hovertip" alt="This information can be shown on the contact page and in the footer">[?]</a></div>
            <div class="setdata"><textarea id="BUS_ADDRESS" name="newcfg[BUS_ADDRESS]" cols="42" rows="6"><?php echo getIfSet($_system->configs['BUS_ADDRESS'])?></textarea> <?php $_settings->showResetLink('BUS_ADDRESS')?></div>
        <div class="setlabel">Business Phone:</div>
            <div class="setdata"><input type="text" id="BUS_PHONE" name="newcfg[BUS_PHONE]" size="30" value="<?php echo getIfSet($_system->configs['BUS_PHONE'])?>"/> <?php $_settings->showResetLink('BUS_PHONE')?></div>
        <div class="setlabel">Business Fax:</div>
            <div class="setdata"><input type="text" id="BUS_FAX" name="newcfg[BUS_FAX]" size="30" value="<?php echo getIfSet($_system->configs['BUS_FAX'])?>"/> <?php $_settings->showResetLink('BUS_FAX')?></div>
        <h3 class="header">Date and Time</h3>
        <div class="setlabel">Date Format: <a href="#" class="hovertip" alt="The date format is more than cosmetic.  It may affect time-sensitive features of the site">[?]</a></div>
            <div class="setdata">
                <select name="newcfg[PHP_DATE_FORMAT]" id="PHP_DATE_FORMAT" size="1">
                <?php $df = array("Y-m-d", "m-d-Y", "d-m-Y", "Y/m/d", "m/d/y", "d/m/Y");
                $cfgdateformat = getIfSet($_system->configs['PHP_DATE_FORMAT']);
                foreach($df as $d){
                    $sel = (($d == $cfgdateformat) ? ' selected="selected"' : '');
                    echo '<option value="'.$d.'"'.$sel.'>'.$d.(($d == 'Y-m-d') ? ' [default]' : '').'</option>'.PHP_EOL;
                }
                ?>
                </select>
                <p class="settingsdescr">The date looks like this: <?php echo date($cfgdateformat)?></p>
            </div>
        <div class="setlabel">Timezone: <a href="#" class="hovertip" alt="The timezone setting affects the login timer, garbage collection, simcron tasks, and any other time-sensitive functions">[?]</a></div>
            <div class="setdata">
                <?php $_settings->showSettingsTimeZoneData(); ?>
            </div>
        <p>&nbsp;</p>
    <?php }

    $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_GENERAL);
}

function settings_general_form_process(){
    global $_db_control, $_users;

    $errstr = array();

    return $errstr;
}
?>