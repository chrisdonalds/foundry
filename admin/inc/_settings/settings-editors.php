<?php

// ---------------------------
//
// FOUNDRY ADMIN SETTINGS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
// EDITORS TAB
//

function settings_editors_menu($currenttab){
    global $_users, $_settings, $_events;

    list($retn) = $_events->processTriggerEvent(__FUNCTION__);
    $retn .= ' Editors';

    if($_users->userIsAllowedTo(UA_VIEW_EDITOR_SETTINGS)) { ?><li><a href="#tabs-editors" class="settingstabs_link<?php echo (($currenttab == 'editors') ? ' loaded' : '') ?>"><?php echo $retn ?></a></li><?php echo PHP_EOL; }
}

function settings_editors_content_wrapper($currenttab, $show){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_VIEW_EDITOR_SETTINGS)) { ?>
    <div id="tabs-editors">
        <?php
        if($show){
            settings_editors_content($currenttab);
        }
        ?>
    </div>
    <?php }
}

function settings_editors_content($currenttab){
    global $_system, $_users, $_settings, $_plugins, $_editors;

    if($_users->userIsAllowedTo(UA_VIEW_EDITOR_SETTINGS)) { ?>
        <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_EDIT_EDITOR_SETTINGS)); ?>
        <p id="issues-editors" class="setissue<?php if($_settings->getSettingsIssuesCount('editors') == 0) echo ' disabled';?>"><?php $_settings->showSettingsIssues('editors'); ?></p>
        <div class="setlabel">Character Encoding: <a href="#" class="hovertip" alt="The encoding tells <?php echo SYS_NAME?> how to render page content.  Ideally, match this value with the encoding set for the database.">[?]</a></div>
            <div class="setdata"><input type="text" id="CHARSET" name="newcfg[CHARSET]" maxlength="50" size="20" value="<?php echo getIfSet($_system->configs['CHARSET'])?>"/> <?php $_settings->showResetLink('CHARSET')?>
                <p class="settingsdescr">The most common are 'UTF-8' and 'ISO-8859-1'</p>
            </div>
        <div class="setlabel">Content Editor: <a href="#" class="hovertip" alt="By default <?php echo SYS_NAME?> is set to use CKEditor as the CMS editor.  This option allows you to switch to other installed editors.">[?]</a></div>
            <div class="setdata">
                <select name="newcfg[CMS_EDITOR]" id="CMS_EDITOR" size="1">
                    <?php
                    $cmses = $_editors->getListofCMSEditors();
                    $cms_set = getIfSet($_system->configs['CMS_EDITOR']);
                    $cms_settings = array();
                    foreach($cmses as $cms){
                        if($cms['incl'] == $cms_set) {
                            $sel = ' selected="selected"';
                            $hidden = '';
                        }else{
                            $sel = '';
                            $hidden = ' hidden';
                        }
                        $active = (($cms['incl'] == $cms_set) ? ' selected="selected"' : '');
                        echo '<option value="'.$cms['incl'].'"'.$sel.'>'.$cms['name'].'</option>'.PHP_EOL;
                        $cms_settings[] = '<a rel="'.$cms['incl'].'_settings|'.$cms['folder'].$cms['initfile'].'|'.$cms['id'].'" id="editor_'.$cms['incl'].'" class="plugin_settings_link'.$hidden.'" href="#"><b>'.$cms['name'].' Settings</b></a>';
                    }
                    ?>
                </select>
                <p id="cms_editor_settings">
                    <?php echo implode('', $cms_settings); ?>
                </p>
                <p class="settingsdescr">
                    Although <?php echo SYS_NAME?> includes CKEditor as its default CMS editor, other popular editors may be installed.<br/>
                    The above setting allows you to switch between any of the installed and enabled editors.<br/>
                    Additionally, you can install media managers, file browsers, or FTP clients that are designed to augment <?php echo SYS_NAME?> CMS editors.</p>
            </div>
    <?php }

    $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_EDITORS);
}

function settings_editors_form_process(){
    global $_db_control, $_users;

    $errstr = array();

    return $errstr;
}
?>