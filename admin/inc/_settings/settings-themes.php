<?php

// ---------------------------
//
// FOUNDRY ADMIN SETTINGS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
// THEMES TAB
//

function settings_themes_menu($currenttab){
    global $_users, $_settings, $_events;

    list($retn) = $_events->processTriggerEvent(__FUNCTION__);
    $retn .= ' Themes';

    if($_users->userIsAllowedTo(UA_VIEW_THEMES)) { ?><li><a href="#tabs-themes" class="settingstabs_link<?php echo (($currenttab == 'themes') ? ' loaded' : '') ?>"><?php echo $retn ?></a></li><?php echo PHP_EOL; }
}

function settings_themes_content_wrapper($currenttab, $show){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_VIEW_THEMES)) { ?>
    <div id="tabs-themes">
        <?php
        if($show){
            settings_themes_content($currenttab);
        }
        ?>
    </div>
    <?php }
}

function settings_themes_content($currenttab){
    global $_users, $_settings, $_themes;

    if($_users->userIsAllowedTo(UA_VIEW_THEMES)) { ?>
        <p id="issues-themes" class="setissue<?php if($_settings->getSettingsIssuesCount('themes') == 0) echo ' disabled';?>"><?php $_settings->showSettingsIssues('themes'); ?></p>
        <div class="settingstabs2">
            <ul class="settingstabs2-nav">
                <li><a href="#theme_website" class="settingstabs_sublink">Website Themes</a></li>
                <li><a href="#theme_admin" class="settingstabs_sublink">Admin Themes</a></li>
                <?php $_settings->showCustomSettingsSubTabs(SETTINGS_SUBTAB_THEMES_ADMIN); ?>
            </ul>
            <div id="theme_website">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_EDIT_WEBSITE_THEME)); ?>
                <?php $_themes->showSettingsThemesList('website'); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_THEMES, SETTINGS_SUBTAB_THEMES_WEBSITE); ?>
            </div>
            <div id="theme_admin">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_EDIT_ADMIN_THEME)); ?>
                <?php $_themes->showSettingsThemesList('admin'); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_THEMES, SETTINGS_SUBTAB_THEMES_ADMIN); ?>
            </div>
            <?php $_settings->showCustomSettingsSubTabsContent(SETTINGS_SUBTAB_THEMES_ADMIN); ?>
        </div>
    <?php
    }
}

function settings_themes_form_process(){
    global $_db_control, $_users, $_filesys, $_themes;

    $errstr = array();
    if($_users->userIsAllowedTo(UA_EDIT_ADMIN_THEME)){
    }

    return $errstr;
}
?>