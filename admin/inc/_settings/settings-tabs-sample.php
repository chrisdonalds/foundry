<?php

// ---------------------------
//
// SAMPLE SETTINGS TAB OVERRIDE
//
// ---------------------------

/*
 - Used as a replacement for a specific system settings tab
 - Place these functions and triggers in your theme_ops.php or executable
    plugin file

 - If you are planning to replace a system settings tab, the following
    conventions are recommended:

    1. Try to completely replace all of the functions of the current system
        tab.  If the intention is to add new or improve upon existing features,
        do so without eliminating features.

    2. Remember to call the $_settings->showCustomSettingsTabContentAppended()
        method in your contents trigger function.  It offers other developers
        or website designers a means to extend the tab contents with more
        functions.

    3. Register a function with each of the three display trigger events
        (..menu, ..shell, and ..contents) in order to properly render the tab.
        The fourth trigger event, ..validator, is optional and is only
        required if you are validating posted settings data.

    4. Although you are replacing the system settings tab, you are required
        to keep the tab's id.  Without it the Settings API script will not
        properly render or detect the tab.

    5. The function called by the ..shell trigger event must be an empty <div>,
        while the one called by the ..contents trigger event is the same <div>
        including contents.  The latter is called if a) it is the target of the
        URL, or b) the user clicks its tab at which point the API will load the
        tab's contents.

    6. The return of the ..menu, ..shell, and ..contents trigger function must
        be the HTML for the element.  The return of the ..validator trigger
        function must be either false if there are no errors, a string with the
        error message, or an array of string messages.  A null will defeat your
        validation.
*/

//

/**
 * This function displays the tab menu button
 * @return string $html
 */
function sample_tab_menu(){
    $html = <<<EOT
    <li><a href="#tabs-general" class="settingstabs_link">Sample Tab</a></li>
EOT;
    return $html;
}
$_events->setTriggerFunction('settings_override_general_tab_menu', 'sample_tab_menu');

/**
 * The tab content shell (empty div outputted before user clicks tab) goes in here
 * - Note: You MUST use the tab id of the tab body you are overriding
 * @return string $html
 */
function sample_tab_content_wrapper(){
    $html  = '<div id="tabs-general"></div>'.PHP_EOL;
    return $html;
}

/**
 * The contents function...
 * - Note: You MUST use the tab id of the tab body you are overriding
 * @return string $html
 */
function sample_tab_content(){
    global $_settings;

    $html  = '<div id="tabs-general">';
    $html .= '<h3 class="header">Sample Tab</h3>';
    $html .= '<div class="setlabel">Label: <a href="#" class="hovertip" alt="This is a sample hovertip)">[?]</a>'.REQD_ENTRY.'</div>';
    $html .= '<div class="setdata">Sample Information</div>';
    $html .= '</div>'.PHP_EOL;

    $html .= $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_GENERAL);        // The parameter is one of the SETTINGS_TAB_??? constants
    return $html;
}

/**
 * And finally this function handles the data validation
 * @return array $errstr                Return an empty array to indicate that there were no validation errors
 */
function sample_tab_form_validate(){
    global $_db_control;

    $errstr = array();

    return $errstr;
}

// Set the triggers to your functions
// - settings_override_general...
// - settings_override_media...
// - settings_override_editors...
// - settings_override_menus...
// - settings_override_themes...
// - settings_override_plugins...
// - settings_override_users...
// - settings_override_advanced...
$_events->setTriggerFunction('settings_override_general_tab_menu', 'sample_tab_menu');
$_events->setTriggerFunction('settings_override_general_tab_shell', 'sample_tab_content_wrapper');
$_events->setTriggerFunction('settings_override_general_tab_contents', 'sample_tab_content');
$_events->setTriggerFunction('settings_override_general_tab_validator', 'sample_tab_form_validate');
?>