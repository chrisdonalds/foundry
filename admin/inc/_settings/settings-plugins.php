<?php

// ---------------------------
//
// FOUNDRY ADMIN SETTINGS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
// PLUGINS TAB
//

function settings_plugins_menu($currenttab){
    global $_users, $_settings, $_events;

    list($retn) = $_events->processTriggerEvent(__FUNCTION__);
    $retn .= ' Plugins';

    if($_users->userIsAllowedTo(UA_VIEW_PLUGINS)) { ?><li><a href="#tabs-plugins" class="settingstabs_link<?php echo (($currenttab == 'plugins') ? ' loaded' : '') ?>"><?php echo $retn ?><?php $_settings->showSettingsIssuesIndicator('plugins'); ?></a></li><?php echo PHP_EOL; }
}

function settings_plugins_content_wrapper($currenttab, $show){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_VIEW_PLUGINS)) { ?>
    <div id="tabs-plugins">
        <?php
        if($show){
            settings_plugins_content($currenttab);
        }
        ?>
    </div>
    <?php }
}

function settings_plugins_content($currenttab){
    global $_users, $_settings, $_plugins;

    $permission = $_users->userIsAllowedTo(UA_ACTIVATE_PLUGINS) || $_users->userIsAllowedTo(UA_DELETE_PLUGINS) || $_users->userIsAllowedTo('edit_plugins') || $_users->userIsAllowedTo(UA_REPAIR_PLUGINS) || $_users->userIsAllowedTo(UA_UPDATE_PLUGINS) || $_users->userIsAllowedTo(UA_INSTALL_PLUGINS);
    if($_users->userIsAllowedTo(UA_VIEW_PLUGINS)) { ?>
        <p id="issues-plugins" class="setissue<?php if($_settings->getSettingsIssuesCount('plugins') == 0) echo ' disabled';?>"><?php $_settings->showSettingsIssues('plugins'); ?></p>
        <div class="settingstabs2">
            <ul class="settingstabs2-nav">
                <li><a href="#plugin_installed" class="settingstabs_sublink">Installed Plugins</a></li>
                <li><a href="#plugin_extensions" class="settingstabs_sublink">Extensions</a></li>
                <li><a href="#plugin_problem" class="settingstabs_sublink">Problem/Deleted Plugins</a></li>
                <li><a href="#plugin_frameworks" class="settingstabs_sublink">Frameworks</a></li>
                <li><a href="#plugin_settings" class="settingstabs_sublink">Settings</a></li>
                <li><a href="#plugin_updates" class="settingstabs_sublink">Updates</a></li>
                <li><a href="#plugin_lib" class="settingstabs_sublink">Repository</a></li>
                <?php $_settings->showCustomSettingsSubTabs(SETTINGS_SUBTAB_PLUGINS_REPOSITORY); ?>
            </ul>
            <div id="plugin_installed">
                <?php $_settings->showSettingsTabOverlay($permission); ?>
                <?php $_plugins->showSettingsPluginsInstalledList(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_PLUGINS, SETTINGS_SUBTAB_PLUGINS_INSTALLED); ?>
            </div>
            <div id="plugin_extensions">
                <?php $_settings->showSettingsTabOverlay($permission); ?>
                <?php $_plugins->showSettingsPluginsExtensions(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_PLUGINS, SETTINGS_SUBTAB_PLUGINS_EXTENSIONS); ?>
            </div>
            <div id="plugin_problem">
                <?php $_settings->showSettingsTabOverlay($permission); ?>
                <?php $_plugins->showSettingsPluginsProblemList(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_PLUGINS, SETTINGS_SUBTAB_PLUGINS_PROBLEMS); ?>
            </div>
            <div id="plugin_frameworks">
                <?php $_settings->showSettingsTabOverlay($permission); ?>
                <?php $_plugins->showSettingsPluginsFrameworks(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_PLUGINS, SETTINGS_SUBTAB_PLUGINS_FRAMEWORKS); ?>
            </div>
            <div id="plugin_settings">
                <?php $_settings->showSettingsTabOverlay($permission); ?>
                <?php $_plugins->showSettingsPluginsSettings(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_PLUGINS, SETTINGS_SUBTAB_PLUGINS_SETTINGS); ?>
            </div>
            <div id="plugin_updates">
                <?php $_settings->showSettingsTabOverlay($permission); ?>
                <?php $_plugins->showSettingsPluginsUpdates(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_PLUGINS, SETTINGS_SUBTAB_PLUGINS_UPDATES); ?>
            </div>
            <div id="plugin_lib">
                <?php $_settings->showSettingsTabOverlay($permission); ?>
                <?php $_plugins->showSettingsPluginsRepository(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_PLUGINS, SETTINGS_SUBTAB_PLUGINS_REPOSITORY); ?>
            </div>
             <?php $_settings->showCustomSettingsSubTabsContent(SETTINGS_SUBTAB_PLUGINS_REPOSITORY); ?>
       </div>
    <?php }
}

function settings_plugins_form_process(){
    global $_db_control;

    $errstr = array();

    return $errstr;
}
?>