<?php

// ---------------------------
//
// FOUNDRY ADMIN SETTINGS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
// MEDIA TAB
//

function settings_media_menu($currenttab){
    global $_users, $_settings, $_events;

    list($retn) = $_events->processTriggerEvent(__FUNCTION__);
    $retn .= ' Media';

    if($_users->userIsAllowedTo(UA_VIEW_MEDIA_SETTINGS)) { ?><li><a href="#tabs-media" class="settingstabs_link<?php echo (($currenttab == 'media') ? ' loaded' : '') ?>"><?php echo $retn ?><?php $_settings->showSettingsIssuesIndicator('media'); ?></a></li><?php echo PHP_EOL; }
}

function settings_media_content_wrapper($currenttab, $show){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_VIEW_MEDIA_SETTINGS)) { ?>
    <div id="tabs-media">
        <?php
        if($show){
            settings_media_content($currenttab);
        }
        ?>
    </div>
    <?php }
}

function settings_media_content($currenttab){
    global $_system, $_themes, $_users, $_filesys, $_settings, $_fields, $_media;

    $fu_obj = array();
    if($_users->userIsAllowedTo(UA_VIEW_MEDIA_SETTINGS)) { ?>
        <p id="issues-media" class="setissue<?php if($_settings->getSettingsIssuesCount('media') == 0) echo ' disabled';?>"><?php $_settings->showSettingsIssues('media'); ?></p>
        <div class="settingstabs2">
            <ul class="settingstabs2-nav">
                <li><a href="#media_basic" class="settingstabs_link loaded">Universal Settings</a></li>
                <li><a href="#media_formats" class="settingstabs_link loaded">Media Formats</a></li>
                <?php $_settings->showCustomSettingsSubTabs(SETTINGS_SUBTAB_MEDIA_FORMATS); ?>
            </ul>

            <div id="media_basic">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_EDIT_MEDIA_SETTINGS)); ?>
                <h3 class="header">Storage Folders</h3>
                <div class="setlabel">Images Folder: <a href="#" class="hovertip" alt="The path, from document root, to the images folder">[?]</a><?php echo REQD_ENTRY?><br/>(default 'images/')</div>
                    <div class="setdata"><?php echo MEDIA_FOLDER?><input type="text" id="IMG_UPLOAD_FOLDER" name="newcfg[IMG_UPLOAD_FOLDER]" maxlength="64" size="20" value="<?php echo getIfSet($_system->configs['IMG_UPLOAD_FOLDER'])?>"/> <?php $_settings->showResetLink('IMG_UPLOAD_FOLDER')?></div>
                <div class="setlabel">Thumbnails Folder: <a href="#" class="hovertip" alt="The path, from document root, to the thumbnails folder.  It can be the same as the images folder, if you wish">[?]</a><?php echo REQD_ENTRY?><br/>(default 'images/')</div>
                    <div class="setdata"><?php echo MEDIA_FOLDER?><input type="text" id="THM_UPLOAD_FOLDER" name="newcfg[THM_UPLOAD_FOLDER]" maxlength="64" size="20" value="<?php echo getIfSet($_system->configs['THM_UPLOAD_FOLDER'])?>"/> <?php $_settings->showResetLink('THM_UPLOAD_FOLDER')?></div>
                <div class="setlabel">Documents Folder: <a href="#" class="hovertip" alt="The path, from document root, to the documents folder.">[?]</a><?php echo REQD_ENTRY?><br/>(default 'files/')</div>
                    <div class="setdata"><?php echo MEDIA_FOLDER?><input type="text" id="FILE_UPLOAD_FOLDER" name="newcfg[FILE_UPLOAD_FOLDER]" maxlength="64" size="20" value="<?php echo getIfSet($_system->configs['FILE_UPLOAD_FOLDER'])?>"/> <?php $_settings->showResetLink('FILE_UPLOAD_FOLDER')?></div>
                <div class="setlabel"></div>
                    <div class="setdata">
                        Media is located within <?php echo SITE_PATH.MEDIA_FOLDER ?>.
                        <br/>Media files will be automatically saved to sub-folders named for the file's associated data type.  Files uploaded in the Media editor will be saved directly in the images or files folders.
                        <br/>Upload size limits, maximum dimensions, and more can be customized in the Media Formats tab.
                    </div>

                <h3 class="header">Maximum File Upload Limits</h3>
                <div class="setlabel">Max. Image Upload Size: <a href="#" class="hovertip" alt="This value is the absolute maximum upload size for images.  It cannot be more than the PHP maximum.">[?]</a><?php echo REQD_ENTRY?></div>
                    <div class="setdata">
                        <input type="text" id="IMG_MAX_UPLOAD_SIZE" name="newcfg[IMG_MAX_UPLOAD_SIZE]" maxlength="20" size="20" value="<?php echo getIfSet($_system->configs['IMG_MAX_UPLOAD_SIZE'])?>"/> <?php $_settings->showResetLink('IMG_MAX_UPLOAD_SIZE')?>
                    </div>
                <div class="setlabel">Max. File Upload Size: <a href="#" class="hovertip" alt="This value is the absolute maximum upload size for files.  It cannot be more than the PHP maximum.">[?]</a><?php echo REQD_ENTRY?></div>
                    <div class="setdata">
                        <input type="text" id="FILE_MAX_UPLOAD_SIZE" name="newcfg[FILE_MAX_UPLOAD_SIZE]" maxlength="20" size="20" value="<?php echo getIfSet($_system->configs['FILE_MAX_UPLOAD_SIZE'])?>"/> <?php $_settings->showResetLink('FILE_MAX_UPLOAD_SIZE')?>
                    </div>
                <div class="setlabel"></div>
                    <div class="setdata">
                        The above sizes can be expressed as a number to denote a size in bytes, or with a size unit such as kB, MB, GB, TB...
                        <br/>The maximum size that PHP allows is <?php echo intval(ini_get('upload_max_filesize')); ?> MB.
                    </div>

                <h3 class="header">Logos and Icons</h3>
                <div class="setlabel">Admin Login Logo/Image: <a href="#" class="hovertip" alt="An optional logo to display on the login page">[?]</a></div>
                    <div class="setdata">
                        <?php
                        $sizelimit = $_media->getFileSizeLimit('large', MT_IMAGE);
                        $lastlogo = getIfSet($_system->configs['IMG_LOGIN_LOGO']);
                        $fu_obj['elems'][] = "img_login_logo";
                        $fu_obj['lastimg'][] = $lastlogo;
                        $fu_obj['allowedexts'][] = $_media->getMediaTypes(MT_IMAGE);
                        $fu_obj['sizelimits'][] = $sizelimit;
                        $params = array(
                            "ids" => array("img_login_logo", "img_login_logo_last", "img_login_logo_thm", "img_login_logo_del"),
                            "values" => array($lastlogo),
                            "displayed" => array(true),
                            "displaytype" => FLD_DATA
                        );
                        $_fields->showImageField($params);
                        ?>
                        <input type="hidden" name="newcfg[IMG_LOGIN_LOGO]" id="IMG_LOGIN_LOGO" value="<?php echo getIfSet($_system->configs['IMG_LOGIN_LOGO'])?>" />
                        <p class="settingsdescr">The visible area of the login form is 430 x 200 pixels.  Uploads are limited to <?php echo $_filesys->formatFileSize($sizelimit)?></p>
                    </div>
                <div class="setlabel">Site Favicon: <a href="#" class="hovertip" alt="An icon that is displayed in browser tabs or title bars which represents the site.">[?]</a></div>
                    <div class="setdata">
                        <?php
                        $sizelimit = $_media->getFileSizeLimit('thumbnail', MT_ICON);
                        $lastlogo = getIfSet($_system->configs['IMG_FAVICON']);
                        $fu_obj['elems'][] = "img_favicon";
                        $fu_obj['lastimg'][] = $lastlogo;
                        $fu_obj['allowedexts'][] = $_media->getMediaTypes(MT_ICON);
                        $fu_obj['sizelimits'][] = $sizelimit;
                        $params = array(
                            "ids" => array("img_favicon", "img_favicon_last", "", "img_favicon_del"),
                            "values" => array($lastlogo),
                            "displayed" => array(true),
                            "displaytype" => FLD_DATA
                        );
                        $_fields->showImageField($params);
                        ?>
                        <input type="hidden" name="newcfg[IMG_FAVICONS]" id="IMG_FAVICONS" value="<?php echo getIfSet($_system->configs['IMG_FAVICONS'])?>" />
                        <p class="settingsdescr">A 16x16 pixel ICO image saved to the website root.  Uploads are limited to <?php echo $_filesys->formatFileSize($sizelimit)?></p>
                    </div>

                <h3 class="header">Image Processor Library</h3>
                <div class="setlabel">Default PHP Image Library: <a href="#" class="hovertip" alt="The preferred image processor that will be used for system operations">[?]</a><?php echo REQD_ENTRY?></div>
                    <div class="setdata">
                        <select name="newcfg[DEF_IMAGE_PROCESSOR]" id="DEF_IMAGE_PROCESSOR" size="1">
                        <?php
                            $imgprocs = array();
                            if(isGDLibLoaded()) $imgprocs['gdlib'] = 'GD Lib';
                            if(isImageMagickLoaded()) $imgprocs['imlib'] = 'Image Magick';
                            $imgproc_set = getIfSet($_system->configs['DEF_IMAGE_PROCESSOR']);
                            foreach($imgprocs as $imgpkey => $imgp){
                                $sel = (($imgpkey == $imgproc_set) ? ' selected="selected"' : '');
                                echo '<option value="'.$imgpkey.'"'.$sel.'>'.$imgp.'</option>'.PHP_EOL;
                            }
                        ?>
                        </select>
                    </div>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_MEDIA, SETTINGS_SUBTAB_MEDIA_UNIVERSAL); ?>
            </div>

            <div id="media_formats" class="clearfix">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_EDIT_MEDIA_SETTINGS)); ?>
                <p>Media Formats are used to alter the layout, size or rendering of a media file.  Customarily used to scale or crop images, Media Formats can be applied to
                    video files, video-streaming addresses, media popup handlers (ColorBox, ThickBox, etc.) to size display viewports, or audio files to pre-process audio.</p>
                <p>These formats are instructions used by the Media system to process a file and does not modify the original files.</p>
                <?php
                echo '<div id="media-header" class="table-header fixed">';
                echo '<div class="col-lg-2"><h3 class="header">Media Format <a href="" class="media_format_new fa fa-plus" title="Add new format"></a></h3></div><div class="col-lg-2"><h3 class="header">Min.</h3></div><div class="col-lg-2"><h3 class="header">Max.</h3></div><div class="col-lg-2"><h3 class="header">Size Limit*</h3></div><div class="col-lg-2"><h3 class="header">File Types</h3></div><div class="col-lg-2"><h3 class="header">Actions</h3></div>'.PHP_EOL;
                echo '</div>'.PHP_EOL;
                echo '<div id="media-body" class="table-body">';
                settings_show_media_formats_list();
                echo '</div>'.PHP_EOL;
                ?>
                <p>* File limits for a chosen format are applied when uploading new files.  Sizes are further limited by the maximum size set in the Univeral Settings.</p>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_MEDIA, SETTINGS_SUBTAB_MEDIA_FORMATS); ?>
            </div>

            <?php $_settings->showCustomSettingsSubTabsContent(SETTINGS_SUBTAB_MEDIA_FORMATS); ?>
        <?php

        if(isset($fu_obj)){
            $_filesys->uploaderPrepScript(UPLOAD_TEMPFOLDER, 0, 100);
            $_filesys->uploaderStart($fu_obj['elems'], $fu_obj['lastimg'], array(""), $fu_obj['allowedexts'], $fu_obj['sizelimits']);
        }
    ?>
        </div>
    <?php
    }
}

function settings_show_media_formats_list(){
    global $_media, $_themes, $_users;

    $mediaformats = $_media->getFormats();
    if($mediaformats !== false){
        foreach($mediaformats as $id => $set){
            $implements = "Implemented by: ".ucwords($set['source_type']);
            if(getIntValIfSet($set['backup_id']) > 0) $implements .= " [Overridden]";

            $actions = '';
            if($set['active'] == 0)
                $actions .= '<a href="#" class="media_formats_act" rel="'.$set['id'].'">Activate</a>';
            else
                $actions .= (($actions == '') ? '' : '&nbsp;|&nbsp;').'<a href="#" class="media_formats_act" rel="'.$set['id'].'">Deactivate</a>';
            if($set['source_type'] != 'system')
                $actions .= (($actions == '') ? '' : '&nbsp;|&nbsp;').'<a href="#" class="media_formats_del" rel="'.$set['id'].'">Delete</a>';
            else
                if($set['backup_id'] == 0)
                    $actions .= (($actions == '') ? '' : '&nbsp;|&nbsp;').'<a href="#" class="media_formats_ovr" rel="'.$set['id'].'">Override</a>';
                else
                    $actions .= (($actions == '') ? '' : '&nbsp;|&nbsp;').'<a href="#" class="media_formats_ovr" rel="'.$set['id'].'">Revert</a>';

            $locked = ($set['source_type'] == 'system' && $set['backup_id'] == 0);

            echo '<div class="media_formats_row settings_hover_row col-lg-12 dottedborder-bottom smallerfont row" rel="'.$set['id'].'">'.PHP_EOL;
            echo '<div class="col-lg-2 mf-col-title">';
            if($locked || $set['source_type'] == 'system'){
                echo '<strong>'.ucwords($set['name']).'</strong><br/>';
            }else{
                echo '<a href="#" class="media_formats_title_trigger" rel="'.$set['id'].'" title="Click to edit"><strong>'.ucwords($set['name']).'</strong>&nbsp;<i class="fa fa-pencil"></i></a>&nbsp;';
                echo '<input type="text" name="media_format['.$set['id'].'][code]" value="'.$set['name'].'" class="media_formats_title hidden" rel="'.$set['id'].'" /><br/>';
            }
            echo $implements.'<br/><span class="media_actions action_items hidden">'.$actions.'</span></div>'.PHP_EOL;
            echo '<input type="hidden" name="media_format_id" class="media_format_id" value="'.$set['id'].'" />'.PHP_EOL;
            foreach($set as $setkey => $atts){
                switch($setkey){
                    case 'min':
                    case 'max':
                        echo '<div class="col-lg-2 mf-col-minmax">';
                        $arry = json_decode($atts, true);
                        if(is_array($arry)){
                            foreach($arry as $attr => $val){
                                if($locked)
                                    echo '<div class="gap-5">'.ucwords($attr).': '.$val.'</div>';
                                else
                                    echo '<div class="gap-5">'.ucwords($attr).': <input type="text" class="media_format_val" name="media_format['.$set['id'].']['.$setkey.']['.$attr.']" maxlength="20" size="5" value="'.$val.'"/></div>';
                            }
                        }
                        echo '</div>';
                        break;
                    case 'filelimit':
                        echo '<div class="col-lg-2 mf-col-filelimit">';
                        if($locked)
                            echo '<div class="gap-5">'.$atts.'</div>';
                        else
                            echo '<div class="gap-5"><input type="text" class="media_format_val" name="media_format['.$set['id'].']['.$setkey.']" maxlength="20" size="5" value="'.$atts.'"/></div>';
                        echo '</div>';
                        break;
                    case 'extensions':
                        echo '<div class="col-lg-2 mf-col-filelimit">';
                        if($locked){
                            echo '<div class="gap-5">'.$atts.'</div>';
                        }else{
                            echo '<div class="gap-5"><select class="media_format_val" name="media_format['.$set['id'].']['.$setkey.'][]" multiple="multiple">';
                            echo $_media->getMediaTypesMenu($atts);
                            echo '</select></div>';
                        }
                        echo '</div>';
                        break;
                    case 'actions':
                        echo '<div class="col-lg-2 mf-col-actions">';
                        $arry = json_decode($atts, true);
                        if(is_array($arry)){
                            $incr = 1;
                            foreach($arry as $actionindx => $params){
                                $action = preg_replace("/\|(.*)/", "", $actionindx);
                                echo '<div class="gap-5 media_formats_action"><strong>'.ucwords($action).'</strong><br/>';

                                // media_formats: one of [width,text], [focal,menu:left,right...]
                                $media_formats = $_media->getMediaFormatActionAttributes($action);
                                foreach($params as $param => $val){
                                    // param: width, focal,...
                                    // val: 1280px, center
                                    if($locked){
                                        echo ucwords($param).': '.$val.'&nbsp;&nbsp;';
                                    }else{
                                        if(isset($media_formats[$param])){
                                            switch(true){
                                                case ($media_formats[$param] == "text"):
                                                    echo ucwords($param).' <input type="text" class="media_format_val" name="media_format['.$set['id'].'][action]['.$action.'|'.$incr.']['.$param.']" maxlength="20" size="3" value="'.$val.'"/> ';
                                                    break;
                                                case preg_match("/^menu:(.*)/i", $media_formats[$param]):
                                                    echo ucwords($param).' <select class="media_format_val" name="media_format['.$set['id'].'][action]['.$action.'|'.$incr.']['.$param.']">';
                                                    $menuoptions = explode(",", substr($media_formats[$param], 5));
                                                    if(!empty($menuoptions)){
                                                        foreach($menuoptions as $menu) echo '<option value="'.$menu.'"'.(($menu == $val) ? ' selected="selected"' : '').'>'.$menu.'</option>';
                                                    }
                                                    echo '</select>'.PHP_EOL;
                                                    break;
                                            }
                                        }
                                    }
                                }
                                if(!$locked) echo '<a href="#" class="media_formats_actions_delete fa fa-trash fa-lg" rel="'.$set['id'].":".$actionindx.'" title="Delete this format action"></a>';
                                echo '</div>';
                                $incr++;
                            }
                        }
                        if(!$locked) {
                            $actions = $_media->getMediaFormatActionTypes();
                            if(is_array($actions)){
                                $selectmenu = '<select class="media_formats_actions_types" rel="'.$set['id'].'"><option value="">- Add New Action -</option>';
                                foreach($actions as $key){
                                    $selectmenu .= '<option value="'.$key['code'].'">'.$key['name'].'</option>';
                                }
                                $selectmenu .= '</select>';
                                echo $selectmenu;
                            }
                        }
                        echo '</div>';
                        break;
                }
            }
            echo '</div>'.PHP_EOL;
        }
    } else {
        echo '<p>There are no media formats created.</p>';
    }
}

function settings_show_media_format_new_row(){
    global $_media, $_themes, $_users, $_events;

    $_events->processTriggerEvent(__FUNCTION__);
    $set = array(
        "id" => 0,
        "name" => null,
        "code" => null,
        "source_type" => "custom",
        "source_code" => null,
        "min" => array("width" => null, "height" => null),
        "max" => array("width" => null, "height" => null),
        "filelimit" => null,
        "extensions" => null,
        "actions" => array(),
        "active" => 1,
        "ref_id" => 0,
        "backup_id" => null
    );

    $actions = '<a href="#" class="media_formats_del" rel="'.$set['id'].'">Delete</a>';
    $locked = false;
    $implements = "Implemented by: ".ucwords($set['source_type']);

    echo '<div class="media_formats_row settings_hover_row col-lg-12 dottedborder-bottom smallerfont row" rel="'.$set['id'].'">'.PHP_EOL;
    echo '<div class="col-lg-2 mf-col-title">';
    echo '<a href="#" class="media_formats_title_trigger" rel="'.$set['id'].'" title="Click to edit"><strong class="hidden">'.ucwords($set['name']).'&nbsp;'.$_themes->showIcon('admin', 'edit', array('class' => 'clickable')).'</strong></a>';
    echo '<input type="text" name="media_format['.$set['id'].'][code]" value="'.$set['name'].'" class="media_formats_title" rel="'.$set['id'].'" /><br/>';
    echo $implements.'<br/><span class="media_actions action_items hidden">'.$actions.'</span></div>'.PHP_EOL;
    echo '<input type="hidden" name="media_format_id" class="media_format_id" value="'.$set['id'].'" />'.PHP_EOL;
    foreach($set as $setkey => $atts){
        switch($setkey){
            case 'min':
            case 'max':
                echo '<div class="col-lg-2 mf-col-minmax">';
                foreach($atts as $attr => $val){
                    echo '<div class="gap-5">'.ucwords($attr).': <input type="text" class="media_format_val" name="media_format['.$set['id'].']['.$setkey.']['.$attr.']" maxlength="20" size="5" value="'.$val.'"/></div>';
                }
                echo '</div>';
                break;
            case 'filelimit':
                echo '<div class="col-lg-2 mf-col-filelimit">';
                echo '<div class="gap-5"><input type="text" class="media_format_val" name="media_format['.$set['id'].']['.$setkey.']" maxlength="20" size="5" value="'.$atts.'"/></div>';
                echo '</div>';
                break;
            case 'extensions':
                echo '<div class="col-lg-2 mf-col-filelimit">';
                echo '<div class="gap-5"><select class="media_format_val" name="media_format['.$set['id'].']['.$setkey.'][]" multiple="multiple">';
                echo $_media->getMediaTypesMenu($atts);
                echo '</select></div>';
                echo '</div>';
                break;
            case 'actions':
                echo '<div class="col-lg-2 mf-col-actions">';
                $actions = $_media->getMediaFormatActionTypes();
                if(is_array($actions)){
                    $selectmenu = '<select class="media_formats_actions_types" rel="'.$set['id'].'"><option value="">- Add New Action -</option>';
                    foreach($actions as $key){
                        $selectmenu .= '<option value="'.$key['code'].'">'.$key['name'].'</option>';
                    }
                    $selectmenu .= '</select>';
                    echo $selectmenu;
                }
                echo '</div>';
                break;
        }
    }
    echo '</div>'.PHP_EOL;
}

function settings_media_form_process(){
    global $_db_control, $_filesys;

    $errstr = array();

    // logo
    list($logo_img) = $_filesys->saveImage(array('img_login_logo', 'img_login_logo_last', 'img_login_logo_thm'), ADMIN_FOLDER.IMG_UPLOAD_FOLDER."logo", "", false, false, false, "logo");
    $_db_control->setTable(SETTINGS_TABLE)->setFieldvals(array("value" => $logo_img))->setWhere("name = 'IMG_LOGIN_LOGO'")->updateRec();

    list($favicon_img) = $_filesys->saveImage(array('img_favicon', 'img_favicon_last', ''), "", "", false, false, false, "logo");
    $_db_control->setTable(SETTINGS_TABLE)->setFieldvals(array("value" => $favicon_img))->setWhere("name = 'IMG_FAVICON'")->updateRec();

    // media formats
    if(isset($_POST['media_format'])){
        foreach($_POST['media_format'] as $id => $format){
            $actions = array();
            foreach($format['action'] as $actionkey => $actionval){
                $actionkey = preg_replace("/([^|]*)\|(.*)/", "$1", $actionkey);
                $actions[$actionkey] = $actionval;
            }
            $extensions = join(", ", $format['extensions']);
            if($id > 0){
                $fieldvals = array(
                    "min" => json_encode($format['min']),
                    "max" => json_encode($format['max']),
                    "filelimit" => $format['filelimit'],
                    "extensions" => $extensions,
                    "actions" => json_encode($actions)
                );
                $_db_control->setTable(MEDIA_FORMATS_TABLE)->setFieldvals($fieldvals)->setWhere("id = '$id'")->updateRec();
            }else{
                $fieldvals = array(
                    "name" => $format['code'],
                    "code" => codify($format['code']),
                    "min" => json_encode($format['min']),
                    "max" => json_encode($format['max']),
                    "filelimit" => $format['filelimit'],
                    "extensions" => $extensions,
                    "actions" => json_encode($actions)
                );
                $_db_control->setTable(MEDIA_FORMATS_TABLE)->setFieldvals($fieldvals)->insertRec();
            }
        }
    }
    return $errstr;
}
?>