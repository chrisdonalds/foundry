<?php

// ---------------------------
//
// FOUNDRY ADMIN SETTINGS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
// MENUS TAB
//

function settings_menus_menu($currenttab){
    global $_users, $_settings, $_events;

    list($retn) = $_events->processTriggerEvent(__FUNCTION__);
    $retn .= ' Menus';

    if($_users->userIsAllowedTo(UA_VIEW_MENU_SETTINGS)) { ?><li><a href="#tabs-menus" class="settingstabs_link<?php echo (($currenttab == 'menus') ? ' loaded' : '') ?>"><?php echo $retn ?><?php $_settings->showSettingsIssuesIndicator('menus'); ?></a></li><?php echo PHP_EOL; }
}

function settings_menus_content_wrapper($currenttab, $show){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_VIEW_MENU_SETTINGS)) { ?>
    <div id="tabs-menus">
        <?php
        if($show) {
            settings_menus_content($currenttab);
        }
        ?>
    </div>
    <?php }
}

function settings_menus_content($currenttab){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_VIEW_MENU_SETTINGS)) { ?>
        <p id="issues-menus" class="setissue<?php if($_settings->getSettingsIssuesCount('menus') == 0) echo ' disabled';?>"><?php $_settings->showSettingsIssues('menus'); ?></p>
        <div class="settingstabs2">
            <ul class="settingstabs2-nav">
                <li><a href="#menus_website" class="settingstabs_sublink">Website Menus</a></li>
                <li><a href="#menus_admin" class="settingstabs_sublink">Admin Menus</a></li>
                <?php $_settings->showCustomSettingsSubTabs(SETTINGS_SUBTAB_MENUS_ADMIN); ?>
            </ul>

            <div id="menus_website">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_EDIT_WEBSITE_MENUS)); ?>
                <?php $_settings->showSettingsMenusforWebsite(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_MENUS, SETTINGS_SUBTAB_MENUS_WEBSITE); ?>
            </div>

            <div id="menus_admin">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_EDIT_ADMIN_MENUS)); ?>
                <?php $_settings->showSettingsMenusforAdmin(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_MENUS, SETTINGS_SUBTAB_MENUS_ADMIN); ?>
            </div>
            <?php $_settings->showCustomSettingsSubTabsContent(SETTINGS_SUBTAB_MENUS_ADMIN); ?>
        </div>
    <?php
    }
}

function settings_menus_form_process(){
    global $_db_control;

    $errstr = array();

    return $errstr;
}
?>