<?php

// ---------------------------
//
// FOUNDRY ADMIN SETTINGS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
// ADVANCED TAB
//

function settings_advanced_menu($currenttab){
    global $_users, $_settings, $_events;

    list($retn) = $_events->processTriggerEvent(__FUNCTION__);
    $retn .= ' Advanced';

    if($_users->userIsAllowedTo(UA_VIEW_ADVANCED_SETTINGS)) { ?><li><a href="#tabs-advanced" class="settingstabs_link<?php echo (($currenttab == 'advanced') ? ' loaded' : '') ?>"><?php echo $retn ?><?php $_settings->showSettingsIssuesIndicator('advanced'); ?></a></li><?php echo PHP_EOL; }
}

function settings_advanced_content_wrapper($currenttab, $show){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_VIEW_ADVANCED_SETTINGS)) { ?>
    <div id="tabs-advanced">
        <?php
        if($show){
            settings_advanced_content($currenttab);
        }
        ?>
    </div>
    <?php }
}

function settings_advanced_content($currenttab){
    global $_system, $_users, $_filesys, $_data, $_settings, $_plugins, $_themes, $_sec, $_cron;

    if($_users->userIsAllowedTo(UA_VIEW_ADVANCED_SETTINGS)) { ?>
        <p id="issues-adv" class="setissue<?php if($_settings->getSettingsIssuesCount('advanced') == 0) echo ' disabled';?>"><?php $_settings->showSettingsIssues('advanced'); ?></p>
        <div class="settingstabs2">
            <ul class="settingstabs2-nav">
                <li><a href="#adv_aliases" class="settingstabs_sublink">Aliases</a></li>
                <li><a href="#adv_users" class="settingstabs_sublink">Users Manager</a></li>
                <li><a href="#adv_links" class="settingstabs_sublink">SEO</a></li>
                <li><a href="#adv_visibility" class="settingstabs_sublink">Site Visibility</a></li>
                <li><a href="#adv_filesys" class="settingstabs_sublink">Files/Mail</a></li>
                <li><a href="#adv_tuning" class="settingstabs_sublink">Tuning</a></li>
                <li><a href="#adv_cron" class="settingstabs_sublink">Cron</a></li>
                <li><a href="#adv_debugger" class="settingstabs_sublink">Error Handling</a></li>
                <li><a href="#adv_reports" class="settingstabs_sublink">Reports</a></li>
                <li><a href="#adv_core" class="settingstabs_sublink">Core</a></li>
                <?php $_settings->showCustomSettingsSubTabs(SETTINGS_SUBTAB_ADVANCED_CORE); ?>
            </ul>

            <?php settings_advanced_content_data(); ?>

            <?php settings_advanced_content_users_mgr(); ?>

            <?php settings_advanced_content_seo(); ?>

            <?php settings_advanced_content_visibility(); ?>

            <?php settings_advanced_content_filesmail(); ?>

            <?php settings_advanced_content_tuning(); ?>

            <?php settings_advanced_content_cron(); ?>

            <?php settings_advanced_content_errors(); ?>

            <?php settings_advanced_content_reports(); ?>

            <?php settings_advanced_content_core(); ?>

            <?php $_settings->showCustomSettingsSubTabsContent(SETTINGS_SUBTAB_ADVANCED_CORE); ?>
        </div>

    <?php }
}

function settings_advanced_content_data(){
    global $_settings, $_users;
    ?>
            <!-- DATA ALIASES -->
            <div id="adv_aliases">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_MANAGE_DATA_TYPES)); ?>
                <p>
                    <?php echo SYS_NAME; ?> offers many ways to customize URLs for pages and data.  Beyond the standard query-based URL format, which might resemble <i><?php echo WEB_URL?>page.php?id=1)</i>, the system also allows
                    <a href="#" class="hovertip" alt="Custom admin editor and menu URLs.">Admin Aliases</a>,
                    <a href="#" class="hovertip" alt="Page organization using a familiar folder-format hierarchy without ugly file extensions or queries.">Page Aliases</a>,
                    <a href="#" class="hovertip" alt="Paths that describe categorized content.">Taxonomy/Term Aliases</a>,
                    <a href="#" class="hovertip" alt="Generic URLs that point to custom data.">Data Aliases</a>,
                    <a href="#" class="hovertip" alt="Of course, &lt;strong&gt;<?php echo SYS_NAME; ?>&lt;/strong&gt; doesn't prevent you from using mod-rewrites.  Actually, it makes it easier with an editor right here in the Settings.">Mod-Rewrites</a>, and
                    <a href="#" class="hovertip" alt="Lastly, files can be served directly without interference.">Direct File access</a>.
                </p>
                <p>
                    Note: Aliases cannot be deleted or created here.  This can be done via a page, data, term or taxonomy editor.
                </p>

                <div id="adv_data_aliases" class="gap-10">
                    <h3 class="header">Data Aliases</h3>
                    <p><i>Data Aliases</i> are generic URLs that point to custom data.</p>
                    <div class="row">
                        <div class="col-lg-4"><u>Data Alias Pattern</u></div>
                        <div class="col-lg-8"><u>Associated Data Type</u></div>
                    </div>
                    <?php $_settings->showSettingsDataAliases(); ?>
                </div>

                <div id="adv_tax_aliases" class="gap-10">
                    <h3 class="header">Taxonomy Aliases</h3>
                    <p>Taxonomy Aliases, like Data Aliases, allow you to design links for multiple taxonomy terms using one alias pattern.</p>
                    <div class="row">
                        <div class="col-lg-12"><u>Taxonomy Alias Pattern</u></div>
                    </div>
                    <?php $_settings->showSettingsTaxonomyAliases(); ?>
                </div>

                <div id="adv_admin_aliases" class="gap-10">
                    <h3 class="header">Admin Aliases</h3>
                    <p>An admin alias is a pattern used by admin menus to link to page, data, or term editors.  Note: page and terms editor aliases cannot be modified once they are created.</p>
                    <div class="row">
                        <div class="col-lg-4"><u>Admin Alias</u></div>
                        <div class="col-lg-8"><u>Associated Data Type</u></div>
                    </div>
                    <?php $_settings->showSettingsAdminAliases(); ?>
                </div>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_ADVANCED, SETTINGS_SUBTAB_ADVANCED_ALIASES); ?>
            </div>
    <?php
}

function settings_advanced_content_users_mgr(){
    global $_settings, $_users, $_plugins, $_system;
    ?>
            <!-- USERS MANAGER -->
            <div id="adv_users">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_MANAGE_USER_SYSTEM)); ?>
                <h3 class="header">Admin Login Options</h3>
                <div class="setlabel">Allow User to Login with Their Facebook Identity?: <a href="#" class="hovertip" alt="Set to 'yes' to allow <?php echo SYS_NAME?> to connect with Facebook to verify user login identity.">[?]</a></div>
                    <div class="setdata">
                        <input type="radio" id="FACEBOOKLOGIN_on" name="newcfg[FACEBOOKLOGIN]" value="1"<?php echo ((getIfSet($_system->configs['FACEBOOKLOGIN']) == 1) ? ' checked="checked"' : '')?> /> Yes
                        <input type="radio" id="FACEBOOKLOGIN_off" name="newcfg[FACEBOOKLOGIN]" value="0"<?php echo ((getIfSet($_system->configs['FACEBOOKLOGIN']) == 0) ? ' checked="checked"' : '')?> /> No
                    </div>

                <h3 class="header">User Types and Allowances</h3>
                <div class="setlabel">Additional User Types</div>
                <div class="setdata">
                    <p>Other than the Standard User Types (Developer, Sysadmin, Owner, User, Author, and Guest), additional User Types can be defined here or in code using the <i>$_users->createCustomUserType()</i> function.</p>
                    <p class="settingsdescr">Note: User Types must be unique and cannot start with 'admlevel'.</p>
                    <p><input type="button" id="ut_addtype" value="Add User Type"/></p>
                    <div id="ut_list">
                    <?php
                    $ut_arry = $_users->getUserTypes();
                    $c_ut_arry = $_users->getCustomUserTypes();
                    $ua_depth = $_users->getAllowanceDepth();

                    $ut_menu = '<option value="">- None -</option>'.PHP_EOL;
                    foreach(($ut_arry + $c_ut_arry) as $k => $v) { if(getBinaryPower($v, true) < $ua_depth) $ut_menu .= '<option value="'.$v.'">'.$k.'</option>'.PHP_EOL; }
                    echo '<select id="ut_std_types_ref" style="display: none;">'.$ut_menu.'</select>';

                    if(count($c_ut_arry) > 0){
                        foreach($c_ut_arry as $k => $v){
                            echo '<p>User Type: <input type="text" class="ut_name" name="ut_name['.$v.']" value="'.$k.'" rel="'.$v.'" /> Copy Allowances from <select name="ut_copy_str_types['.$v.']" class="ut_copy_std_types">'.$ut_menu.'</select> <a href="#" class="ut_delete" rel="'.$k.'">Delete Type</a></p>'.PHP_EOL;
                        }
                    }
                    ?>
                    </div>
                </div>

                <div class="setlabel">User Allowances</div>
                <div class="setdata">
                    <p>This version of <?php echo SYS_NAME?> has <?php echo count($_users->allowances)?> Allowances, or tasks that a user type is permitted to perform, which should cover almost anything one can do in the system.  If you need more, you can create Custom Allowances to handle further permissions.</p>
                    <div id="ua_container">
                        <div id="ua_lower_container">
                            <div id="ua_items">
                                <?php
                                $ua_allowance_groups = $_users->allowance_groups;
                                foreach($ua_allowance_groups as $item_group_name => $item_group){
                                    echo '<span id="ua_item_group_'.codify($item_group_name).'" class="ua_item"><strong>'.$item_group_name.'</strong></span>'.PHP_EOL;
                                    foreach($item_group as $name){
                                        $key = $_users->getAllowanceKeyFromName($name);
                                        switch($key){
                                            case UA_MANAGE_USER_SYSTEM:
                                            case UA_VIEW_ADVANCED_SETTINGS:
                                            case UA_EDIT_ADVANCED_SETTINGS:
                                                $name .= ' <a href="#" class="hovertip red" alt="At least one User Type should be granted `'.$name.'`, otherwise this interface will become unavailable.">*</a>'.PHP_EOL;
                                                break;
                                            }
                                        echo '<span id="ua_item_'.$key.'" class="ua_item"> ...'.$name;
                                        if($item_group_name == 'Custom Allowances') echo ' <a title="Delete this custom allowance" class="ua_delete" href="#" rel="'.$key.'"></a>'.PHP_EOL;
                                        echo '</span>'.PHP_EOL;
                                    }
                                }
                                ?>
                            </div>
                            <div id="ua_values">
                                <div id="ua_usertypes">
                                    <?php
                                    $ua_ut = $_users->getUserTypes() + $_users->getCustomUserTypes();
                                    foreach($ua_ut as $ut_key => $ut_key_val){
                                        echo '<span>'.$_users->getAllowanceNameFromKey($ut_key).'</span>'.PHP_EOL;
                                    }
                                    ?>
                                </div>
                                <?php
                                $ua_items = $_users->allowances + $_users->allowances_custom;
                                foreach($ua_allowance_groups as $item_group_name => $item_group){
                                    $ua_group_key = $_users->getAllowanceKeyFromName($item_group_name);
                                    echo '<div class="ua_value_group_title ua_value_row" rel="'.codify($item_group_name).'">'.PHP_EOL;
                                    foreach($ua_ut as $ut_name => $ut_key){
                                        $key = getBinaryPower($ut_key, true);
                                        echo '<span>';
                                        echo '<input type="checkbox" id="ua_group_toggle_'.codify($ua_group_key).'_'.$key.'" class="ua_groups_toggle" rel="'.$ua_group_key.'|'.$key.'" /> All';
                                        echo '&nbsp;<a href="#" title="Reset this Group for User Type" class="ua_groups_reset fa fa-fast-backward" rel="'.$ua_group_key.'|'.$key.'"></a>';
                                        echo '</span>'.PHP_EOL;
                                    }
                                    echo '</div>'.PHP_EOL;
                                    foreach($item_group as $name){
                                        $ua_item_key = $_users->getAllowanceKeyFromName($name);
                                        if(!isset($ua_items[$ua_item_key])) $ua_items[$ua_item_key] = array_fill(0, 6, "0");
                                        echo '<div class="ua_value_row" id="ua_value_row_'.codify($ua_item_key).'" rel="'.$ua_item_key.'">'.PHP_EOL;
                                        $ua_value_set = $ua_items[$ua_item_key];
                                        foreach($ua_value_set as $ua_value_key => $value){
                                            echo '<span><input type="checkbox" name="ua_value['.$ua_item_key.']['.$ua_value_key.']" class="'.$ua_group_key.'_group_item_'.$ua_value_key.'" value="1"'.(($value == 1) ? ' checked="checked"' : '').' /></span>'.PHP_EOL;
                                        }
                                        echo '</div>'.PHP_EOL;
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div style="padding-left: 8px">
                            <p><strong>Custom Allowances</strong></p>
                            <p>Virtually any number of Custom Allowances may be added.</p>
                            <p>Name of new Custom Allowance: <input type="text" id="ua_addname" value="" /> <input type="button" id="ua_addcustom" value="Add Custom Allowance" /></p>
                        </div>
                    </div>
                </div>

                <?php
                    $ua_cancelacct_action = getIfSet($_system->configs['ua_cancelacct_action']);
                    $ua_cancelacct_content = getIfSet($_system->configs['ua_cancelacct_content']);
                    $ua_cancelacct_user = getIfSet($_system->configs['ua_cancelacct_users']);
                ?>
                <h3 class="header">Cancelling User Accounts</h3>
                <div class="setdata">
                    <p>When cancelling a user account,
                    <select id="ua_cancelacct_action">
                        <option value="disable"<?php echo (($ua_cancelacct_action == 'disable') ? ' selected="selected"' : '') ?>>DISABLE</option>
                        <option value="delete"<?php echo (($ua_cancelacct_action == 'delete') ? ' selected="selected"' : '') ?>>DELETE</option>
                    </select> the account and
                    <select id="ua_cancelacct_content">
                        <option value="keep"<?php echo (($ua_cancelacct_content == 'keep') ? ' selected="selected"' : '') ?>>KEEP the contents</option>
                        <option value="delete"<?php echo (($ua_cancelacct_content == 'delete') ? ' selected="selected"' : '') ?>>DELETE the contents</option>
                        <option value="reassign"<?php echo (($ua_cancelacct_content == 'reassign') ? ' selected="selected"' : '') ?>>REASSIGN the contents to</option>
                    </select>
                    <select id="ua_cancelacct_users"<?php echo (($ua_cancelacct_content != 'reassign') ? ' class="hidden"' : '') ?>>
                        <option value=""<?php echo ((intval($ua_cancelacct_user) == 0) ? ' selected="selected"' : '') ?>>- Select User -</option>
                        <?php
                        $users = $_users->getUsers();
                        foreach($users as $user){
                            echo '<option value="'.$user['id'].'"'.(($ua_cancelacct_user == $user['id']) ? ' selected="selected"' : '').'>'.$user['username'].'</option>';
                        }
                        ?>
                    </select>
                </div>

                <h3 class="header">Account Messages</h3>
                <div class="setdata">
                    <?php
                    $ecaplugins = $_plugins->getPluginsInstalledbyGroup("ECA Agents");
                    if(empty($ecaplugins)){
                        echo '<p>No ECA Agent extensions have been installed.  An ECA Agent with a "Mail New Message" action is needed to process mail messages.</p>';
                    }else{
                    }
                    ?>
                </div>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_ADVANCED, SETTINGS_SUBTAB_ADVANCED_USERS_MANAGER); ?>
            </div>
    <?php
}

function settings_advanced_content_seo(){
    global $_settings, $_users;
    ?>
            <!-- SEO -->
            <div id="adv_links">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_MANAGE_SEO)); ?>
                <?php $_settings->showSettingsLinksArea(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_ADVANCED, SETTINGS_SUBTAB_ADVANCED_SEO); ?>
            </div>
    <?php
}

function settings_advanced_content_visibility(){
    global $_settings, $_system, $_users;
    ?>
            <!-- VISIBILITY -->
            <div id="adv_visibility">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_MANAGE_VISIBILITY)); ?>
                <h3 class="header">Site Presence</h3>
                <div class="setlabel">Site Visibility State: <a href="#" class="hovertip" alt="Set the viewable status of your site: online, offline, or private.">[?]</a></div>
                    <div class="setdata">
                        <select name="newcfg[SITEOFFLINE]">
                            <option value="0"<?php echo ((getIfSet($_system->configs['SITEOFFLINE']) == 0) ? ' selected="selected"' : '')?>>Online and Ready (Default)</option>
                            <option value="1"<?php echo ((getIfSet($_system->configs['SITEOFFLINE']) == 1) ? ' selected="selected"' : '')?>>Offline Maintenance Mode</option>
                            <option value="2"<?php echo ((getIfSet($_system->configs['SITEOFFLINE']) == 2) ? ' selected="selected"' : '')?>>Private Viewing Only</option>
                        </select>
                    </div>
                <div class="setlabel">Offline Message:</div>
                    <div class="setdata"><textarea id="SITEOFFLINE_MSG" name="newcfg[SITEOFFLINE_MSG]" cols="42" rows="4"><?php echo getIfSet($_system->configs['SITEOFFLINE_MSG'])?></textarea> <?php $_settings->showResetLink('SITEOFFLINE_MSG')?></div>
                <?php $_settings->showSettingsSiteVisibilityArea(); ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_ADVANCED, SETTINGS_SUBTAB_ADVANCED_VISIBILITY); ?>
            </div>
    <?php
}

function settings_advanced_content_filesmail(){
    global $_settings, $_system, $_users, $_filesys;
    ?>
            <!-- FILE SYSTEM & EMAILS -->
            <div id="adv_filesys">
                <?php
                if(empty($_system->configs['TEMPFOLDER'])) $_system->configs['TEMPFOLDER'] = $_filesys->getTempFolder();
                if(empty($_system->configs['UPLOADTEMPFOLDER'])) $_system->configs['UPLOADTEMPFOLDER'] = ini_get('upload_tmp_dir');
                ?>
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_MANAGE_FILE_MAIL_SYS)); ?>
                <h3 class="header">Folders</h3>
                <div class="setlabel">Document Root: <a href="#" class="hovertip" alt="The absolute server path to the base folder.">[?]</a></div>
                    <div class="setdata">
                        <?php echo SITE_PATH ?>
                        <p class="settingsdescr">This path is where <?php echo SYS_NAME?> was installed and is represented in code by the SITE_PATH constant.</p>
                    </div>
                <div class="setlabel">Domain Root:</div>
                    <div class="setdata">
                        <?php echo WEB_URL ?>
                        <p class="settingsdescr">This path was prepared by the web/hosting administrator and is represented in code by the WEB_URL constant.</p>
                    </div>
                <h3 class="header">File Transfer (FTP)</h3>
                <p>With a properly configured FTP setup, system features such as plugin updates and extension installations can execute smoothly.  If you don't want to provide FTP services, leave the fields empty.</p>
                <div class="setlabel">Hostname: <a href="#" class="hovertip" alt="The name of the server host, if unknown try 'localhost'.">[?]</a></div>
                    <div class="setdata">
                        <input type="text" id="FTPHOST" name="newcfg[FTPHOST]" value="<?php echo getIfSet($_system->configs['FTPHOST'])?>" />
                    </div>
                <div class="setlabel">Username: <a href="#" class="hovertip" alt="The username of an FTP account available to <?php echo SYS_NAME ?>.">[?]</a></div>
                    <div class="setdata">
                        <input type="text" id="FTPUSER" name="newcfg[FTPUSER]" value="<?php echo getIfSet($_system->configs['FTPUSER'])?>" />
                    </div>
                <div class="setlabel">Password:</div>
                    <div class="setdata">
                        <input type="password" id="FTPPASS" name="newcfg[FTPPASS]" value="<?php echo getIfSet($_system->configs['FTPPASS'])?>" />
                    </div>
                <div class="setlabel">Connect Using:</div>
                    <div class="setdata">
                        <input type="radio" id="FTPCONN" name="newcfg[FTPCONN]" value="ftp"<?php echo ((getIfSet($_system->configs['FTPCONN']) == 'ftp') ? ' checked="checked"' : '')?> /> FTP&nbsp;&nbsp;
                        <input type="radio" id="FTPCONN" name="newcfg[FTPCONN]" value="ftps"<?php echo ((getIfSet($_system->configs['FTPCONN']) == 'ftps') ? ' checked="checked"' : '')?> /> FTPS
                        <input type="hidden" id="FTPVALID" name="newcfg[FTPVALID]" value="<?php echo getIfSet($_system->configs['FTPVALID']) ?>" />
                        <p>
                            <input type="button" id="ftp_test" value="Test Connection" />
                        </p>
                    </div>

                <h3 class="header">Temporary Files</h3>
                <div class="setlabel">Default Temp Folder: <a href="#" class="hovertip" alt="This is the local filesystem path where temporary files are stored.">[?]</a></div>
                    <div class="setdata">
                        <input type="text" id="TEMPFOLDER" name="newcfg[TEMPFOLDER]" value="<?php echo getIfSet($_system->configs['TEMPFOLDER']) ?>" style="width: 300px" />
                        <p class="settingsdescr">This is the local filesystem path where temporary files are stored.  It should be inaccessible to the public.</p>
                    </div>
                <div class="setlabel">Upload Temp Folder: <a href="#" class="hovertip" alt="This is the server filesystem path used to store files during file uploads.">[?]</a></div>
                    <div class="setdata">
                        <input type="text" id="UPLOADTEMPFOLDER" name="newcfg[UPLOADTEMPFOLDER]" value="<?php echo getIfSet($_system->configs['UPLOADTEMPFOLDER']) ?>" style="width: 300px" />
                        <p class="settingsdescr">
                            This is the server filesystem path used to store files during file uploads.  It should be inaccessible to the public.
                            <br/>If left blank, the Default Temp Folder will be used.
                            <br/>The 'Max Upload File' size setting can be found in the <a href="<?php echo WEB_URL.ADMIN_FOLDER?>settings?tab=media">Media tab</a>.
                        </p>
                    </div>

                <h3 class="header">SMTP Email Settings</h3>
                <p>
                    <?php echo SYS_NAME ?> includes a robust mail sub-system that offers full SMTP, Sendmail, Postfix or custom mail transports, encryption, and powerful MIME capabilities.
                </p>
                <div class="setlabel">Host Server: <a href="#" class="hovertip" alt="The domain of the server that handles outgoing SMTP traffic">[?]</a></div>
                    <div class="setdata">
                        <input type="text" id="SMTP_HOST" name="newcfg[SMTP_HOST]" value="<?php echo getIfSet($_system->configs['SMTP_HOST']) ?>" style="width: 300px" />
                    </div>
                <div class="setlabel">Username:</div>
                    <div class="setdata">
                        <input type="text" id="SMTP_USERNAME" name="newcfg[SMTP_USERNAME]" value="<?php echo getIfSet($_system->configs['SMTP_USERNAME']) ?>" style="width: 300px" />
                    </div>
                <div class="setlabel">Password:</div>
                    <div class="setdata">
                        <input type="password" id="SMTP_PASSWORD" name="newcfg[SMTP_PASSWORD]" value="<?php echo getIfSet($_system->configs['SMTP_PASSWORD']) ?>" style="width: 300px" />
                    </div>
                <div class="setlabel">Port: <a href="#" class="hovertip" alt="The SMTP port.  Typically 465 or 587">[?]</a></div>
                    <div class="setdata">
                        <input type="text" id="SMTP_PORT" name="newcfg[SMTP_PORT]" value="<?php echo getIfSet($_system->configs['SMTP_PORT']) ?>" style="width: 200px" />
                    </div>
                <div class="setlabel">Encryption: <a href="#" class="hovertip" alt="The encryption method: No encryption, SSL or TLS">[?]</a></div>
                    <div class="setdata">
                        <select id="SMTP_SECURE_MODE" name="newcfg[SMTP_SECURE_MODE]">
                            <option value=""<?php echo ((getIfSet($_system->configs['SMTP_SECURE_MODE']) != 'ssl' && getIfSet($_system->configs['SMTP_SECURE_MODE']) != 'tls') ? ' selected="selected"' : '')?>>No encryption</option>
                            <option value="ssl"<?php echo ((getIfSet($_system->configs['SMTP_SECURE_MODE']) == 'ssl') ? ' selected="selected"' : '')?>>SSL encrypted</option>
                            <option value="tls"<?php echo ((getIfSet($_system->configs['SMTP_SECURE_MODE']) == 'tls') ? ' selected="selected"' : '')?>>STARTTLS authentication</option>
                        </select>
                        <input type="hidden" id="SMTP_VALID" name="newcfg[SMTP_VALID]" value="<?php echo getIfSet($_system->configs['SMTP_VALID']) ?>" />
                        <p class="settingsdescr">
                            The default SMTP port is 25.  If you are using an encrypted socket such as SSL or TLS the common ports are 465 or 587 respectively.<br/>
                            SSL requires the line 'extension=php_openssl.dll' in your php.ini file, and libeay32.dll and/or ssleay32.dll loaded in system32 if the server is running in Windows.
                        </p>
                        <input type="button" id="smtp_test" value="Test Connection" />
                    </div>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_ADVANCED, SETTINGS_SUBTAB_ADVANCED_FILES_EMAIL); ?>
            </div>
    <?php
}

function settings_advanced_content_tuning(){
    global $_settings, $_system, $_cache;
    ?>
            <!-- TUNING -->
            <div id="adv_tuning">
                <h3 class="header">Caching</h3>
                <p>
                    This system includes a multi-level file caching and combination mechanism.  The settings here control whether or not CSS and/or Javascript script files are cached and combined.
                    Files are re-combined and re-cached automatically when the lifespan expires or when the caches are cleared.  It is recommended to turn caching on for
                    production sites as cached files are served faster than source files.
                </p>
                <div class="setlabel">CSS Combining and Compressing:</div>
                    <div class="setdata">
                        <input type="checkbox" id="CACHE_CSS" name="newcfg[CACHE_CSS]"<?php echo (getIfSet($_system->configs['CACHE_CSS']) ? ' checked="checked"' : '') ?> value="1" /> Combine and Cache&nbsp;&nbsp;
                        <input type="checkbox" id="COMPRESS_CSS" name="newcfg[COMPRESS_CSS]"<?php echo (getIfSet($_system->configs['COMPRESS_CSS']) ? ' checked="checked"' : '') ?> value="1" /> Compress
                    </div>
                <div class="setlabel">Javascript Combining:</div>
                    <div class="setdata">
                        <input type="checkbox" id="CACHE_JS" name="newcfg[CACHE_JS]"<?php echo (getIfSet($_system->configs['CACHE_JS']) ? ' checked="checked"' : '') ?> value="1" /> Combine and Cache
                    </div>
                <div class="setlabel">Cache Expiration Lifespan:</div>
                    <div class="setdata">
                        <p>Cache files will be available for at least this much time.  After which time the system will automatically regenerate them.</p>
                        <p>It is recommended that the time be set to a period that is not too short or too long.  Too short and the cache files will be regenerated too often, thus undermining the benefits of caching.  Too long and changes to the source CSS and Javascript files will not be seen.</p>
                        <input type="text" id="CACHE_LIFESPAN" name="newcfg[CACHE_LIFESPAN]" value="<?php echo getIfSet($_system->configs['CACHE_LIFESPAN']) ?>" />
                        <select id="CACHE_LIFESPAN_PERIOD" name="newcfg[CACHE_LIFESPAN_PERIOD]">
                            <option value="min"<?php echo ((getIfSet($_system->configs['CACHE_LIFESPAN_PERIOD']) == 'min') ? ' selected="selected"' : '') ?>>Minute(s)</option>
                            <option value="hrs"<?php echo ((getIfSet($_system->configs['CACHE_LIFESPAN_PERIOD']) == 'hrs') ? ' selected="selected"' : '') ?>>Hour(s)</option>
                            <option value="Days"<?php echo ((getIfSet($_system->configs['CACHE_LIFESPAN_PERIOD']) == 'days') ? ' selected="selected"' : '') ?>>Day(s)</option>
                        </select>
                    </div>
                <p class="clear">
                    <input type="button" id="reset_caches" value="Reset Cache" />
                </p>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_ADVANCED, SETTINGS_SUBTAB_ADVANCED_TUNING); ?>
            </div>
    <?php
}

function settings_advanced_content_cron(){
    global $_settings, $_sec, $_users, $_cron;
    ?>
            <!-- CRON -->
            <div id="adv_cron">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_MANAGE_CRON_SYS)); ?>
                <h3 class="header">Server Crontab Commands</h3>
                <p>The cron system can be executed from a crontab using one of the following two methods.</p>
                <div class="setlabel">PHP-FPM/PHP-CGI:</div>
                    <div class="setdata">php <?php echo SITE_PATH.CRON_ALIAS."?ckey=".$_sec->createUniqueID(WEB_URL.ADMIN_FOLDER, STATIC_SALT, 16)?></div>
                <div class="setlabel">PHP-CLI:</div>
                    <div class="setdata">wget -q -O nul <?php echo WEB_URL.CRON_ALIAS."?ckey=".$_sec->createUniqueID(WEB_URL.ADMIN_FOLDER, STATIC_SALT, 16)?></div>

                <h3 class="header">Scheduled Crons</h3>
                <p>The <?php echo SYS_NAME?> Cron system is checked each time a site resource is loaded.  Unlike initiating the Cron system from a server crontab, the site timer depends on traffic.  If you require precise timing, a server crontab is recommended.</p>
                <div class="table-header fixed dottedborder-bottom row">
                    <?php
                    $crons = $_cron->get_crons();
                    $cols = array(
                        "2,function" => "Function", // 2/12
                        "3,scheduled" => "Next Run",  // 2/12
                        "2,last_run" => "Last Run", // 2/12
                        "1,interval" => "Interval", // 1/12
                        "1,recurrence" => "Runs Left", // 1/12
                        "2,final_datetime" => "End Time", // 2/12
                        "1,active" => "State" // 1/12
                    );
                    foreach($cols as $k => $col){
                        echo '<div class="col-lg-'.substr($k, 0, 1).'">'.$col.'</div>'.PHP_EOL;
                    }
                    ?>
                </div>
                <?php
                $states = array('inactive', 'active', 'in-progress');
                foreach($crons as $cron){
                    $conditions = json_decode($cron['conditions'], true);
                    echo '<div class="table-body col-lg-12 dottedborder-bottom smallerfont row" rel="'.$cron['id'].'">';
                    foreach($cols as $k => $col){
                        $key = substr($k, 2);
                        $val = "--";
                        switch($key){
                            case 'last_run':
                                if(isset($conditions[$key])){
                                    $val = date("Y-m-d H:i:s", $conditions[$key]);
                                    $val .= "<br/>".getTimeDiff(time(), $conditions[$key]);
                                }
                                break;
                            case 'final_datetime':
                                if(isset($conditions[$key])){
                                    $val = date("Y-m-d H:i:s", $conditions[$key]);
                                    $val .= "<br/>".getTimeDiff(time(), $conditions[$key]);
                                }
                                break;
                            case 'interval':
                                if(isset($conditions[$key]))
                                    $val = '<span title="'.settings_advanced_interval_to_string($conditions[$key]).'">'.$conditions[$key].'</span>';
                                break;
                            case 'recurrence':
                                if(isset($conditions[$key]))
                                    $val = intval($conditions[$key]);
                                if($val < 0)
                                    $val = 'unlimited';
                                elseif($val == 0)
                                    $val = 'expired';
                                break;
                            case 'scheduled':
                                if($cron['active']){
                                    $val = date("Y-m-d H:i:s", $cron[$key]);
                                    $val .= "<br/>".getTimeDiff(time(), $cron[$key]);
                                    $val .= '<a href="" class="cron-run fa fa-forward left-gutter" title="Run now"></a>';
                                }else{
                                    $val = "--";
                                }
                                break;
                            case 'active':
                                $val = $states[$cron['active']];
                                if($cron['active'] == CRON_ACTIVE || $cron['active'] == CRON_IN_PROCESS){
                                    $val .= '<a href="" class="cron-disable fa fa-pause left-gutter" title="Disable"></a>';
                                }elseif($cron['active'] == CRON_INACTIVE){
                                    $val .= '<a href="" class="cron-enable fa fa-play green left-gutter" title="Enable"></a>';
                                    $val .= '<a href="" class="cron-delete fa fa-trash red left-gutter" title="Delete"></a>';
                                }
                                break;
                            case 'function':
                                $val = $cron[$key];
                                break;
                        }
                        echo '<div class="col-lg-'.substr($k, 0, 1).'">'.$val.'&nbsp;</div>';
                    }
                    echo '</div>'.PHP_EOL;
                }
                ?>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_ADVANCED, SETTINGS_SUBTAB_ADVANCED_CRON); ?>
            </div>
    <?php
}

function settings_advanced_content_errors(){
    global $_settings, $_system, $_debug, $_users, $_data;
    ?>
            <!-- ERRORS -->
            <div id="adv_debugger">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_MANAGE_ERROR_HANDLER)); ?>
                <h3 class="header">Error Logging and Debugger</h3>
                <p><?php echo SYS_NAME?> is equipped with a fully articulated error handling and debugger system.  It can be customized to respond to different levels of errors, and output a variety of diagnostic data to the console, browser, a file or an email.</p>
                <div class="setlabel">Error Handler Sensitivity: <a href="#" class="hovertip" alt="What type of error(s) the system will trap">[?]</a></div>
                    <div class="setdata">
                        <select name="newcfg[ERROR_SENSITIVITY]" id="ERROR_SENSITIVITY" size="1">
                        <?php
                        $es = array(E_ERROR | E_WARNING | E_PARSE => "Default detection", E_ERROR => "Stop on fatal errors only", E_ERROR | E_WARNING => "Stop on fatal and display warning errors", E_ALL => "Detect all errors", E_STRICT => "Warn of strict compatibility issues");
                        $cfges = getIfSet($_system->configs['ERROR_SENSITIVITY']);
                        foreach($es as $key => $er){
                            $sel = (($key == $cfges) ? ' selected="selected"' : '');
                            echo '<option value="'.$key.'"'.$sel.'>'.$er.'</option>'.PHP_EOL;
                        }
                        ?>
                        </select>
                    </div>
                <div class="setlabel">Error Logging: <a href="#" class="hovertip" alt="What to do if an error is encountered">[?]</a></div>
                    <div class="setdata">
                        If an error occurs, <select name="newcfg[ERROR_LOG_TYPE]" id="ERROR_LOG_TYPE" size="1">
                        <?php
                        $es = array(0 => "Log to system", 1 => "Send email (".ADMIN_EMAIL.")", 3 => "Save to log file (".WEB_URL.ADMIN_FOLDER.INC_FOLDER."_cache/error.log".")");
                        $cfges = getIfSet($_system->configs['ERROR_LOG_TYPE']);
                        foreach($es as $key => $er){
                            $sel = (($key == $cfges) ? ' selected="selected"' : '');
                            echo '<option value="'.$key.'"'.$sel.'>'.$er.'</option>'.PHP_EOL;
                        }
                        ?>
                        </select>
                        <p><a href="<?php echo WEB_URL.ADMIN_FOLDER."settings?tab=advanced&sub=adv_reports&tid=report_error_log"; ?>">View <?php echo SYS_NAME?> error log report</a></p>
                        <p class="settingsdescr">* Depends on an error log mechanism on the server and may not be available on Windows installations.</p>
                    </div>
                <div class="setlabel">Activate the Debug System: <a href="#" class="hovertip" alt="Use the primary function '$_debug->toggle_debug(true|false)' to start and stop debug output. $_debug->_e(), $_debug->_pr(), $_debug->_vd() and more provide different methods to output debug content.">[?]</a></div>
                    <div class="setdata">
                        <input type="radio" id="ALLOW_DEBUGGING_on" name="newcfg[ALLOW_DEBUGGING]" value="1"<?php echo ((getIfSet($_system->configs['ALLOW_DEBUGGING']) == 1) ? ' checked="checked"' : '')?> /> Yes
                        <input type="radio" id="ALLOW_DEBUGGING_off" name="newcfg[ALLOW_DEBUGGING]" value="0"<?php echo ((getIfSet($_system->configs['ALLOW_DEBUGGING']) == 0) ? ' checked="checked"' : '')?> /> No (Debugger messages will not be displayed)
                        <br/>Use the primary function '$_debug->toggle_debug(true|false)' to start and stop debug output. $_debug->_e(), $_debug->_pr(), $_debug->_vd() and more provide different methods to output debug content.
                    </div>
                <div class="setlabel">PHP Error Configuration:</div>
                    <div class="setdata">
                        Display_errors = <?php echo ini_get('display_errors')?><br/>
                        Display_startup_errors = <?php echo ini_get('display_startup_errors')?><br/>
                        Error_log (Server) = <?php echo ini_get('error_log')?><br/>
                        Html_errors = <?php echo ini_get('html_errors')?><br/>
                        Error_append_string = <?php echo ini_get('error_append_string')?><br/>
                        Error_prepend_string = <?php echo ini_get('error_prepend_string')?><br/>
                        Error_reporting = <?php echo ini_get('error_reporting')?><br/>
                        Ignore_repeated_errors = <?php echo ini_get('ignore_repeated_errors')?><br/>
                        Log_errors = <?php echo ini_get('log_errors')?><br/>
                        Log_errors_max_len = <?php echo ini_get('log_errors_max_len')?><br/>
                        Track_errors = <?php echo ini_get('track_errors')?>
                        <p>More PHP error configurations can be seen in Reports > PHP Config</p>
                    </div>
                <h3 class="header">Error Documents</h3>
                <p>Select the pages that will represent specific server error response codes.
                    Error response codes not associated with a page will have default, system-generated content.
                    Note: server configuration may override these settings.</p>
                <?php
                    $pages = $_data->getPageAliases("`published` = 1 AND `homepage` = 0", "", "", "titlearray");
                    $err_404 = getIntValIfSet($_system->configs['ERROR_404_PAGE'], 0);
                    $err_403 = getIntValIfSet($_system->configs['ERROR_403_PAGE'], 0);
                ?>
                <div class="setlabel">404 Page (Page not found):</div>
                    <div class="setdata">
                        <select name="newcfg[ERROR_404_PAGE]" id="ERROR_404_PAGE" size="1">
                            <option value="" <?php echo (($err_404 == '') ? ' selected="selected"' : '') ?>>- Not selected -</option>
                        <?php
                        foreach($pages as $id => $title){
                            $sel = (($id == $err_404) ? ' selected="selected"' : '');
                            echo '<option value="'.$id.'"'.$sel.'>'.$title.'</option>';
                        }
                        ?>
                        </select>
                    </div>
                <div class="setlabel">403 Page (Forbidden Page):</div>
                    <div class="setdata">
                        <select name="newcfg[ERROR_403_PAGE]" id="ERROR_403_PAGE" size="1">
                            <option value="" <?php echo (($err_403 == '') ? ' selected="selected"' : '') ?>>- Not selected -</option>
                        <?php
                        foreach($pages as $id => $title){
                            $sel = (($id == $err_403) ? ' selected="selected"' : '');
                            echo '<option value="'.$id.'"'.$sel.'>'.$title.'</option>';
                        }
                        ?>
                        </select>
                        <p class="settingsdescr">Error responses are handled in the following order when a URL cannot be reached:</p>
                        <ol class="settingsdescr">
                            <li>A 404.php or 403.php template file located in the active theme folder</li>
                            <li>A page saved with a '404' or '403' alias</li>
                            <li>Any page selected to represent 404 or 403 errors</li>
                        </ol>
                    </div>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_ADVANCED, SETTINGS_SUBTAB_ADVANCED_ERRORS); ?>
            </div>
    <?php
}

function settings_advanced_content_reports(){
    global $_settings, $_users;
    ?>
            <!-- REPORTS -->
            <div id="adv_reports">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_VIEW_REPORTS)); ?>
                <p>All site configuration settings and installation status warnings and errors are listed here.</p>
                <div class="setlabel">System Health :</div>
                    <div class="setdatapanel" id="report_health">
                        <div class="setreportpanel">
                            <?php
                            $report = $_settings->showSettingsReports('system_health');
                            ?>
                        </div>
                    </div>
                <div class="setlabel">PHP Config:</div>
                    <div class="setdatapanel" id="report_php">
                        <div class="setreportpanel">
                            <?php
                            $report = $_settings->showSettingsReports('php_config');
                            ?>
                        </div>
                    </div>
                <div class="setlabel">System Environment <a href="#" class="hovertip" alt="Various properties of the webserver, database structure, and PHP environment as it relates to the <?php echo SYS_NAME ?>.">[?]</a>:</div>
                    <div class="setdatapanel" id="report_environ">
                        <div class="setreportpanel">
                            <?php
                            $report = $_settings->showSettingsReports('system_environ');
                            ?>
                        </div>
                    </div>
                <div class="setlabel">Updates Monitor:</div>
                    <div class="setdatapanel" id="report_updates">
                        <div class="setreportpanel">
                        </div>
                    </div>
                <div class="setlabel">Error Log:</div>
                    <div class="setdatapanel" id="report_error_log">
                        <div class="setreportpanel">
                            <?php
                            $report = $_settings->showSettingsReports('error_log');
                            ?>
                        </div>
                    </div>
                <div class="setlabel">Audit Log:</div>
                    <div class="setdatapanel" id="report_audit_log">
                        <div class="setreportpanel">
                            <?php
                            $report = $_settings->showSettingsReports('audit_log');
                            ?>
                        </div>
                    </div>
                <div class="setlabel">SEO Errors:</div>
                    <div class="setdatapanel" id="report_seo_errors">
                        <div class="setreportpanel">
                        </div>
                    </div>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_ADVANCED, SETTINGS_SUBTAB_ADVANCED_REPORTS); ?>
            </div>
    <?php
}

function settings_advanced_content_core(){
    global $_settings, $_db_control, $_users;
    ?>
            <!-- DATABASE -->
            <div id="adv_core">
                <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_MANAGE_CORE)); ?>
                <h3 class="header">System Reset</h3>
                <div class="setlabel">Clear and Reload the Following:</div>
                    <div class="setdata"><select id="core_reset_component"><option value="plugins">Plugins List</option><option value="themes">Themes List</option><option value="register">Registry</option></select><input type="button" value="Reset" id="core_reset"></div>
                <h3 class="header">Database Configuration</h3>
                <p>Below are the current database connection settings.  If you require direct modification of these parameters, the Database Manager can perform those changes.</p>
                <div class="setlabel">Database Host:</div>
                    <div class="setdata"><?php echo DBHOST?></div>
                <div class="setlabel">Database Name:</div>
                    <div class="setdata"><?php echo DBNAME?></div>
                <div class="setlabel">Database Username:</div>
                    <div class="setdata"><?php echo DBUSER?></div>
                <div class="setlabel">Database Password:</div>
                    <div class="setdata"><?php echo DBPASS?></div>
                <div class="setlabel">Database Port:</div>
                    <div class="setdata"><?php echo ((DBPORT != 0) ? DBPORT : 'Default (3306)') ?></div>
                <div class="setlabel">Data Tables Prefix:</div>
                    <div class="setdata">
                        <input type="text" id="DB_TABLE_PREFIX" name="newcfg[DB_TABLE_PREFIX]" maxlength="10" size="10" value="<?php echo DB_TABLE_PREFIX ?>"/> <?php $_settings->showResetLink('DB_TABLE_PREFIX')?>
                        <p>Although you can create any table for use in <?php echo SYS_NAME?>, ones starting with this prefix will be included in the Data Alias structure, Admin Menus, and other subsystems.  Changing this settings will adversely affect existing menu and data relationships.</p>
                    </div>
                <p class="clear alignright"><i class="fa fa-database"></i> Need to edit the database settings? <a href="<?php echo WEB_URL.DB_CFG_ALIAS?>?f=settings&amp;fc=configDB&amp;fp=100" style="font-weight: bold;">Launch the Database Manager...</a></p>
                <h3 class="header">Database Statistics</h3>
                <div class="setlabel">Storage Size:</div>
                    <div class="setdata"><?php echo $_db_control->getDatabaseSize(); ?></div>
                <div class="setlabel">System Status:</div>
                    <div class="setdata">
                        <?php
                        $stats = $_db_control->getDatabaseInfo();
                        echo join("<br/>", $stats);
                        ?>
                    </div>
                <?php $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_ADVANCED, SETTINGS_SUBTAB_ADVANCED_CORE); ?>
            </div>
    <?php
}

function settings_advanced_form_process(){
    global $_db_control, $_users, $_data;

    $errstr = array();

    // User Allowances and User Types
    if(isset($_POST['ua_value'])){
        // Allowances
        $default_allowances = array_keys($_users->allowances);
        $allowances = $_POST['ua_value'];
        foreach($default_allowances as $ua_key) if(!isset($allowances[$ua_key])) $allowances[$ua_key] = array();
        $ua_num = $_users->getAllowanceDepth();
        $ua_fill = array_fill(0, $ua_num, 0);                // an array of zeros, one for each user type
        foreach($allowances as $ua_key => $ua_values){
            $ua_diff = array_diff_key($ua_fill, $ua_values); // whatever elements are missing
            $allowances[$ua_key] = $ua_values + $ua_diff;    // are added as zeros
            ksort($allowances[$ua_key]);                     // and sorted by key
        }
        $allowances_str = str_replace('"1"', '1', json_encode($allowances));
        $_db_control->setTable(SETTINGS_TABLE)->setFieldvals(array("value" => $allowances_str, "name" => "ALLOWANCES"))->setWhere("`name` = 'ALLOWANCES'")->replaceRec();

        // User Types
        $ut_array = array();
        $ut_allow = array();
        $new_ut_array = array();
        $new_ut_allow = array();
        $std_ut_array = $_users->getUserTypes();
        if(isset($_POST['ut_name'])){
            $ut_key_last = ADMLEVEL_GUEST;
            // pre-existing user type
            foreach($_POST['ut_name'] as $ut_key => $ut_name){
                $ut_slug = trim(strtoupper(preg_replace("/(".USERTYPE_PREFIX."|[^a-z0-9_])/i", "", $ut_name)));

                // name must be unique and not the same as a standard type
                if(array_key_exists($ut_slug, $std_ut_array) || array_search($ut_slug, $ut_array) !== false || array_search($ut_slug, $new_ut_array) !== false) {
                    $errstr[] = "Cannot use `$ut_name` to update a Custom User Type.  The new name already exists.";
                    continue;
                }elseif($ut_slug == ""){
                    $errstr[] = "User Type cannot be '".USERTYPE_PREFIX."'.";
                    continue;
                }
                if($ut_key > ADMLEVEL_GUEST){
                    $ut_key_bit = getBinaryPower($ut_key, true);
                    $ut_array[$ut_key] = $ut_name;
                    $ut_allow[$ut_key] = (($_POST['ut_copy_str_types'][$ut_key] != '') ? $_POST['ut_copy_str_types'][$ut_key] : $ut_key);
                    if($ut_key > $ut_key_last) $ut_key_last = $ut_key;
                }
            }

            // new user types to add
            if(isset($_POST['ut_newname']) && count($_POST['ut_newname']) > 0){
                foreach($_POST['ut_newname'] as $ut_key => $ut_newname){
                    $ut_newname = trim(strtoupper(preg_replace("/(".USERTYPE_PREFIX."|[^a-z0-9_])/i", "", $ut_newname)));
                    if(array_key_exists($ut_newname, $std_ut_array) || array_search($ut_newname, $ut_array) !== false || array_search($ut_newname, $new_ut_array) !== false) {
                        $errstr[] = "Cannot create Custom User Type using the name `$ut_newname`.  The name already exists.";
                        continue;
                    }elseif($ut_newname == ""){
                        $errstr[] = "Custom User Type cannot be '".USERTYPE_PREFIX."'.";
                        continue;
                    }

                    $ut_key_last *= 2;  // next key bit
                    $ut_array[$ut_key_last] = $ut_newname;
                    $ut_allow[$ut_key_last] = ((isset($_POST['ut_copy_str_types'][$ut_key]) && $_POST['ut_copy_str_types'][$ut_key] != '') ? $_POST['ut_copy_str_types'][$ut_key] : 0);
                    $new_ut_array[] = $ut_newname;
                    if(!defined(USERTYPE_PREFIX.$ut_newname)) define(USERTYPE_PREFIX.$ut_newname, $ut_key_last);
                }
            }

            // save custom user types to database
            $_db_control->setTable(SETTINGS_TABLE)->setFieldvals(array("value" => json_encode($ut_array), "name" => "CUSTOM_USER_TYPES"))->setWhere("`name` = 'CUSTOM_USER_TYPES'")->replaceRec();

            // update allowances to incorporate custom user type copies and removals
            $ut_key_std_bit = getBinaryPower(ADMLEVEL_GUEST, true);
            foreach($ut_allow as $ut_key => $ut_copy_key){
                $ut_key_bit = getBinaryPower($ut_key, true);
                if($ut_key_bit > $ut_key_std_bit){
                    // only copy allowances to custom user types
                    $ut_copy_key_bit = getBinaryPower($ut_copy_key, true);
                    foreach($allowances as $ua_key => $ua_values){
                        if($ut_copy_key != '')
                            $allowances[$ua_key][$ut_key_bit] = $ua_values[$ut_copy_key_bit];
                        else
                            $allowances[$ua_key][$ut_key_bit] = 0;
                    }
                }
            }
            $allowances_str = str_replace('"1"', '1', json_encode($allowances));
            $_db_control->setTable(SETTINGS_TABLE)->setFieldvals(array("value" => $allowances_str, "name" => "ALLOWANCES"))->setWhere("`name` = 'ALLOWANCES'")->replaceRec();
        }

        $_users->initAllowances();
    }

    // Htaccess content
    if(!empty($_POST['ht_mod1']) || !empty($_POST['ht_mod2'])){
        // first backup the current file
        $filename = SITE_PATH.time().".htaccess";
        if(copy(SITE_PATH.".htaccess", $filename)){
            $ht_sect = array();
            $ht_sect['301'][] = "# ----- 301 Redirects";
            for($i=0; $i<count($_POST['ht_301_to']); $i++){
                $val = "redirect 301 ".$_POST['ht_301_from'][$i]." ".$_POST['ht_301_to'][$i];
                if($_POST['ht_301_active'][$i] != 1) $val = "#".$val;
                $ht_sect['301'][] = $val;
            }

            $ht_sect['seo'][] = "# ----- SEO";
            for($i=0; $i<count($_POST['ht_seo_to']); $i++){
                $val = "RewriteRule ".$_POST['ht_seo_from'][$i]." ".$_POST['ht_seo_to'][$i];
                if($_POST['ht_seo_active'][$i] != 1) $val = "#".$val;
                $ht_sect['seo'][] = $val;
            }

            $ht_sect['www'][] = "# ----- WWW Rewrites";
            $ht_sect['www'][] = $_POST['ht_www_data'];

            $ht_sect['img'][] = "# ----- Image Hotlinking";
            $ht_sect['img'][] = $_POST['ht_img_data'];

            $ht_contents = $_POST['ht_line_data'];
            foreach($ht_sect as $key => $ht_elem){
                // put the sections back into the content where they came from
                if(strpos($ht_contents, "#< {$key} >") !== false){
                    $ht_contents = str_replace("#< {$key} >", join(PHP_EOL, $ht_elem), $ht_contents);
                }
            }

            $ht_contents = str_replace(array('\\\\', '\\\/', '\\"', '\"', '\\\.', "\'"), array("\\", '\/', '"', '"', "\.", "'"), $ht_contents);

            if(!file_put_contents(SITE_PATH.".htaccess", $ht_contents)){
                $errstr[] = "Changes to ".SITE_PATH.".htaccess were not saved!";
            }
        }else{
            $errstr[] = "Cannot backup ".SITE_PATH.".htaccess!";
        }
    }

    // Visibility
    $ua = getRequestVar('ua');
    $da = getRequestVar('da');
    $currules = str_replace(array("&#34;", "&#92;"), array('"', '\\'), getRequestVar('currules'));
    $newrules = array();
    $newrules_str = "";
    $newrules_header = "# Standard robots.txt
# System folders
User-agent: *
Disallow: /".ADMIN_FOLDER."
Disallow: /".INC_FOLDER."
Disallow: /".PLUGINS_FOLDER."
Disallow: /".CORE_FOLDER."
Disallow: /".THEME_FOLDER."
Disallow: /".JS_FOLDER."
# End of System folders
    ";

    if(is_array($ua)){
        foreach($ua as $rkey => $bot) {
            if($bot != '' && isset($da[$rkey]) && trim($da[$rkey]) != ''){
                $newrules_str .= "\nUser-agent: ".$bot."\n";
                $newpaths = explode("\n", $da[$rkey]);
                foreach($newpaths as $path){
                    if(trim($path) != '') $newrules_str .= "Disallow: ".$path."\n";
                }
                $newrules[$bot] = str_replace("\r", "", $da[$rkey]);
            }
        }
    }

    $newrules = json_encode($newrules);

    if($currules != $newrules){
        // first backup the current file
        chmod(SITE_PATH."robots.txt", 0777);
        if(copy (SITE_PATH."robots.txt", SITE_PATH.ADMIN_FOLDER.REV_FOLDER."robots.".date("YmdHis").".txt")){
            @file_put_contents(SITE_PATH."robots.txt", $newrules_header.$newrules);
        }else{
            $errstr[] = "Cannot backup or modify robots.txt!";
        }
        chmod(SITE_PATH."robots.txt", 0644);
    }

    // save data aliases
    if(!empty($_POST['dataalias_meta'])){
        foreach($_POST['dataalias_meta'] as $id => $meta){
            if(!empty($_POST['dataalias_tables'][$id]))
                $_data->saveDataAlias(array("data_type" => $_POST['dataalias_tables'][$id], "attribute_class" => ATTR_CLASS_DATA_ALIAS, "newalias" => $meta, "alias_id" => $id));
        }
    }
    if(!empty($_POST['new_dataalias_meta'])){
        foreach($_POST['new_dataalias_meta'] as $i => $meta){
            if(!empty($_POST['new_dataalias_tables'][$i]))
                $_data->saveDataAlias(array("data_type" => $_POST['new_dataalias_tables'][$i], "attribute_class" => ATTR_CLASS_DATA_ALIAS, "newalias" => $meta, "alias_id" => 0));
        }
    }
    // save admin aliases
    if(!empty($_POST['admin_dataalias_meta'])){
        foreach($_POST['admin_dataalias_meta'] as $id => $meta){
            if(!empty($_POST['admin_dataalias_tables'][$id]))
                $_data->saveDataAlias(array("data_type" => $_POST['admin_dataalias_tables'][$id], "attribute_class" => ATTR_CLASS_ADMIN_ALIAS, "newalias" => $meta, "alias_id" => $id));
        }
    }
    if(!empty($_POST['new_admin_dataalias_meta'])){
        foreach($_POST['new_admin_dataalias_meta'] as $i => $meta){
            if(!empty($_POST['new_admin_dataalias_tables'][$i]))
                $_data->saveDataAlias(array("data_type" => $_POST['new_admin_dataalias_tables'][$i], "attribute_class" => ATTR_CLASS_ADMIN_ALIAS, "newalias" => $meta, "alias_id" => 0));
        }
    }
    // save taxonomy aliases
    if(!empty($_POST['taxalias_meta'])){
        foreach($_POST['taxalias_meta'] as $id => $meta){
            $_data->saveDataAlias(array("data_type" => TAXONOMIES_TABLE, "attribute_class" => ATTR_CLASS_TAX_ALIAS, "newalias" => $meta, "alias_id" => $id));
        }
    }
    if(!empty($_POST['new_taxalias_meta'])){
        foreach($_POST['new_taxalias_meta'] as $i => $meta){
            $_data->saveDataAlias(array("data_type" => TAXONOMIES_TABLE, "attribute_class" => ATTR_CLASS_TAX_ALIAS, "newalias" => $meta, "alias_id" => 0));
        }
    }

    return $errstr;
}

function settings_advanced_interval_to_string($interval){
    $int = explode(",", $interval);
    $str = null;
    foreach($int as $i => $p){
        $p = intval($p);
        if($p <= 0) continue;
        if(!is_null($str)) $str .= ', ';
        switch($i){
            case 0:
                $str .= $p.' secs';
                break;
            case 1:
                $str .= $p.' mins';
                break;
            case 2:
                $str .= $p.' hrs';
                break;
            case 3:
                $str .= $p.' days';
                break;
            case 4:
                $str .= $p.' wks';
                break;
            case 5:
                $str .= $p.' mths';
                break;
        }
    }
    return $str;
}
?>