<?php

// ---------------------------
//
// FOUNDRY ADMIN SETTINGS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
// USERS TAB
//

function settings_users_menu($currenttab){
    global $_users, $_settings, $_events;

    list($retn) = $_events->processTriggerEvent(__FUNCTION__);
    $retn .= ' Users';

    if($_users->userIsAllowedTo(UA_VIEW_USERS)) { ?><li><a href="#tabs-users" class="settingstabs_link<?php echo (($currenttab == 'users') ? ' loaded' : '') ?>"><?php echo $retn ?><?php $_settings->showSettingsIssuesIndicator('users'); ?></a></li><?php echo PHP_EOL; }
}

function settings_users_content_wrapper($currenttab, $show){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_VIEW_USERS)) { ?>
    <div id="tabs-users">
        <?php
        if($show) {
            settings_users_content($currenttab);
        }
        ?>
    </div>
    <?php }
}

function settings_users_content($currenttab){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_VIEW_USERS)) { ?>
        <p id="issues-users" class="setissue<?php if($_settings->getSettingsIssuesCount('users') == 0) echo ' disabled';?>"><?php $_settings->showSettingsIssues('users'); ?></p>
        <p><strong>More:</strong> <a href="<?php echo WEB_URL.ADMIN_FOLDER?>settings?tab=adv&amp;sub=adv_users">Edit login options, user types and allowances</a></p>
        <div id="users">
            <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_EDIT_USER)); ?>
            <?php $_settings->showSettingsUsersList(); ?>
        </div>
    <?php }

    $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_USERS);
}

function settings_users_form_process(){
    global $_db_control, $_users, $_filesys, $_sec;

    $errstr = array();
    $user_id_array = getIfSet($_POST['users_id']);
    $user_name_array = getIfSet($_POST['users_name']);
    $user_pass_array = getIfSet($_POST['users_pass']);
    $user_email_array = getIfSet($_POST['users_email']);
    $user_level_array = getIfSet($_POST['users_level']);
    $user_active_array = getIfSet($_POST['users_active']);
    $user_firstname_array = getIfSet($_POST['users_firstname']);
    $user_lastname_array = getIfSet($_POST['users_lastname']);
    $user_facebook_array = getIfSet($_POST['users_facebook_link']);
    $user_twitter_array = getIfSet($_POST['users_twitter_link']);
    $user_google_array = getIfSet($_POST['users_google_plus_link']);
    $user_avatar_array = getIfSet($_POST['users_avatar']);
    $_filesys->ensureFolderExists(IMG_UPLOAD_FOLDER."/avatars");

    if(is_array($user_id_array)){
        foreach($user_id_array as $key => $user_id){
            $user_ready_to_save = true;
            $user_name = getIfSet($user_name_array[$key]);
            $user_pass = getIfSet($user_pass_array[$key]);
            $user_email = getIfSet($user_email_array[$key]);
            $user_level = getIntValIfSet($user_level_array[$key]);
            $user_active = ((!empty($user_active_array[$key])) ? 1 : 0);
            $user_firstname = getIfSet($user_firstname_array[$key]);
            $user_lastname = getIfSet($user_lastname_array[$key]);
            $user_facebook = getIfSet($user_facebook_array[$key]);
            $user_twitter = getIfSet($user_twitter_array[$key]);
            $user_google = getIfSet($user_google_array[$key]);
            $user_avatar = getIfSet($user_avatar_array[$key]);

            list($user_saveimg, $user_savethm) = $_filesys->saveImage(array('users_image'.$key, 'users_lastimg'.$key, 'users_lastthm'.$key), IMG_UPLOAD_FOLDER."avatars", "", false, false, false, "u".$key."_avatar");

            $flds = array();
            if (!empty($user_name)) {
                if($_db_control->setTable(ACCOUNTS_TABLE)->setFields("username")->setWhere("username = '$user_name' and id != $user_id")->getRecItem() == ""){
                    $flds['username'] = $user_name;
                }else{
                    $errstr[] = "The USERNAME, '$user_name', already exists";
                    $user_ready_to_save = false;
                }
            }else{
                $errstr[] = "The USERNAME is required.";
                $user_ready_to_save = false;
            }

            if (!empty($user_pass)) {
                // hash the password
                $flds['password'] = $_sec->createHash($user_pass, false);
                $flds['phash'] = '';
                $flds['pcle'] = md5($user_name.$user_pass);
            }

            if(!empty($user_email)) {
                $flds['email'] = $user_email;
            }else{
                $errstr[] = "The EMAIL was not provided for user '$user_name'.  It is required if you forget your password.";
                $user_ready_to_save = false;
            }

            if($user_ready_to_save){
                $flds['activated'] = $user_active;
                $flds['level'] = $user_level;
                $flds['firstname'] = $user_firstname;
                $flds['lastname'] = $user_lastname;
                $flds['facebook_link'] = $user_facebook;
                $flds['twitter_link'] = $user_twitter;
                $flds['google_plus_link'] = $user_google;
                $flds['avatar'] = $user_avatar;
                $flds['image'] = $user_saveimg;
                $_SESSION['userdata']['avatar'] = $user_avatar;     // normally this is changed at login
                $_users->avatar = $user_avatar;

                if($user_id > 0){
                    if($user_level != intval($_db_control->setTable(ACCOUNTS_TABLE)->setFields("level")->setWhere("id = '$user_id'")->getRecItem())) $loadpage = WEB_URL.ADMIN_FOLDER."admlogin.php?admsubmit=Logout";
                    $fldlist = array();
                    foreach($flds as $key => $fld) $fldlist[$key] = $fld;
                    $_db_control->setTable(ACCOUNTS_TABLE)->setFieldvals($fldlist)->setWhere("id = $user_id")->updateRec();
                    if($_users->getUserID() == $user_id) $_SESSION['admuserlevel'] = $user_level;
                }else{
                    $_db_control->setTable(ACCOUNTS_TABLE)->setFieldvals($flds)->insertRec();
                }
            }
        }
    }

    return $errstr;
}
?>