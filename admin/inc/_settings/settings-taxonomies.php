<?php

// ---------------------------
//
// FOUNDRY ADMIN SETTINGS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
// TAXONOMIES TAB
//

function settings_taxonomies_menu($currenttab){
    global $_users, $_settings, $_events;

    list($retn) = $_events->processTriggerEvent(__FUNCTION__);
    $retn .= ' Taxonomies';

    if($_users->userIsAllowedTo(UA_CREATE_TAXONOMIES)) { ?><li><a href="#tabs-taxonomies" class="settingstabs_link<?php echo (($currenttab == 'taxonomies') ? ' loaded' : '') ?>"><?php echo $retn ?><?php $_settings->showSettingsIssuesIndicator('taxonomies'); ?></a></li><?php echo PHP_EOL; }
}

function settings_taxonomies_content_wrapper($currenttab, $show){
    global $_users, $_settings;

    if($_users->userIsAllowedTo(UA_CREATE_TAXONOMIES)) { ?>
    <div id="tabs-taxonomies">
        <?php
        if($show){
            settings_taxonomies_content($currenttab);
        }
        ?>
    </div>
    <?php }
}

function settings_taxonomies_content($currenttab){
    global $_system, $_themes, $_users, $_filesys, $_settings, $_fields;

    $fu_obj = array();
    if($_users->userIsAllowedTo(UA_CREATE_TAXONOMIES)) { ?>
        <p id="issues-taxonomies" class="setissue<?php if($_settings->getSettingsIssuesCount('taxonomies') == 0) echo ' disabled';?>"><?php $_settings->showSettingsIssues('taxonomies'); ?></p>
        <?php $_settings->showSettingsTabOverlay($_users->userIsAllowedTo(UA_CREATE_TAXONOMIES)); ?>
        <p>A taxonomy is a systematic classification of objects.  In <?php echo SYS_NAME ?>, taxonomies are created generically, can be grouped hierarchically, can relate to any data type, and are accessible by discrete aliases, both in admin and on the front.</p>
        <p>During the creation of a taxonomy you can also prepare a corresponding admin terms menu, with which contained terms are added and managed.</p>

        <h3 class="header">Custom Taxonomies&nbsp;<a title="Add new taxonomy" id="tax_new" class="fa fa-plus" href="#"></a></h3>
        <div id="tax_list">
            <?php $_settings->showSettingsTaxonomyEditor(); ?>
        </div>

    <?php
    }

    $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_TAX);
}

function settings_show_taxonomies_formats_list(){
    global $_taxonomies, $_themes, $_users;

}

function settings_show_taxonomies_format_new_row(){
    global $_taxonomies, $_themes, $_users, $_events;

    $_events->processTriggerEvent(__FUNCTION__);
}

function settings_taxonomies_form_process(){
    global $_db_control, $_filesys;

    $errstr = array();
    return $errstr;
}
?>