/**
 * Foundry UI Scripts -- Form Control
 * @author Chris Donalds (cdonalds01@gmail.com)
 * @requires jQuery 1.4.4+ and ui.script.js
 */
/* ------------------------------------------------------------------------------------- */
jQuery(function($){
	$(document).ready(function(){
		var page_url = $('#page_url').val();
		var page_subject = $('#page_subject').val();
		var page_ingroup = $('#page_ingroup').val();
		var page_taxonomy_id = $('#taxonomy_id').val();
		var page_term_id = $('#term_id').val();
		var page_parentgroup = $('#page_parentgroup').val();
		var page_childsubject = $('#page_childsubject').val();
		var full_delete = $('#full_delete').val();

		// Search

	    $("#search_content_toggle").click(function(e){
	    	e.preventDefault();
	        if($(this).hasClass('toggleclose')){
	            $("#search_content").hide("blind", { direction: "vertical" }, 500);
	            $(this).attr('class', 'toggleopen').css('background-position', 'top');
	        }else{
	            $("#search_content").show("blind", { direction: "vertical" }, 500);
	            $(this).attr('class', 'toggleclose').css('background-position', 'bottom');
	        }
	        return false;
	    });

	    $("#page_search").click(function(e){
	    	$('#list_form').submit();
	    });

	    // List Pages

	    $('.listcol-sort').click(function(e){
	    	e.preventDefault();
	    	var dir = $(this).attr('rel');
	    	$('#sort_dir').val(dir);
	    	$('#page_search').trigger('click');
	    });

	    $('.list_button_page_prev, .list_button_page_next').click(function(e){
	    	e.preventDefault();
	    	var rel = $(this).attr('rel');
	    	$('#page').val(rel);
	    	$('#list_form').submit();
	    });

	    // List Page Bulk Action/Checkboxes

	    $('html').click(function(){
	    	// hide bulk option box
	    	var this_id = $(this).attr('id');
	    	if(this_id != 'listrow-bulk-actions-check' && this_id != 'listrow-bulk-actions'){
		    	$('#listrow-bulk-actions').hide();
	    	}
	    });

	    $('#listrow-bulk-actions-check').click(function(e){
	    	// "check" button (displays bulk actions list div)
	    	e.stopPropagation();
	    	var chkstate = $(this).is(':checked');
	   		$('#listrow-bulk-actions').show();
	    });
	    $('#listrow-bulk-actions a').click(function(e){
	    	// action chosen
	    	e.stopPropagation();
	    	var action = $(this).attr('href').replace('#', '');
	    	var more = ((full_delete && action == 'delete') ? ' (this cannot be undone)' : '');
	    	$('#listrow-bulk-actions').hide();

	    	// prompt
	    	if(action != '' && action != '-' && action != 'select_all' && action != 'deselect_all' && action != 'advanced'){
	    		var numselected = $('.listrow-check:checked').length;
	    		if(numselected > 0){
		    		var pluralsubject = ((numselected > 1) ? page_subject.pluralize() : page_subject);
	    	    	if(!confirm('Are you sure you want to '+action.toUpperCase()+' the selected '+pluralsubject+more + '?')){
	    	    		action = ''; // do nothing...
	    	    		return false;
	    	    	}
	    		}else{
	    			alert('Please select something to '+action.toUpperCase()+'.');
	    			return false;
	    		}
	    	}

	    	// init bulk action
	    	if(action == 'advanced'){
	    		var row_ids = [];
		    	$('.listrow').each(function(){
		    		var check_elem = $(this).find('.listrow-check');		// does this row have a bulk action checkbox?
		    		if(check_elem.is(':checked')){
		    			row_ids.push(check_elem.val());
		    		}
		    	});

		    	if(row_ids.length > 0){
		    		// get info for the advanced bulk action dialog
			    	var x_data = $('#x_data').val();
		    		var pagedata = {'page_url':encodeURIComponent(page_url), 'page_subject':page_subject, 'page_ingroup':page_ingroup};
		            $.foundry.showThrobber();
		            $.post(
		                $.foundry.ajax_url,
		                {'op':'preplistactionadvancedbox', 'row_ids':row_ids, 'pagedata':pagedata, 'x_data':x_data},
		                function(jsondata){
		                	if(jsondata.success){
		                		$('#listrow-bulk-actions-adv .listrow-bulk-actions-adv-content').html(jsondata.rtndata.html);
		                		$('#listrow-bulk-actions-adv').show();
		                	}else{
		                		alert('Sorry, we were unable to prepare the advanced bulk actions editor.');
		                	}
		                    $.foundry.hideThrobber();
		                	return false;
		                },
		                "json"
		            );
		      	}else{
		      		alert('Please select one or more records to work on.');
		      	}
	    	}else{
	    		// do chosen action to group of rows
		    	$('.listrow').each(function(){
		    		var check_elem = $(this).find('.listrow-check');		// does this row have a bulk action checkbox?
		    		if(action == 'select_all'){
		    			check_elem.attr('checked', true);
		    		}else if(action == 'deselect_all'){
		    			check_elem.attr('checked', false);
		    		}else if(action == 'advanced'){

		    		}else if(action != '' && action != '-' && check_elem.is(':checked')){
		    			var action_elem = $(this).find('.action_'+action);	// does this row include the related action?
			    	    var row_id = action_elem.attr('rel');
		    			if(row_id != '' && row_id != null){
			    	    	var altparam = action_elem.attr('altparam');
			    	    	var altgroup = action_elem.attr('altgroup');
			    	    	var addquery = action_elem.attr('addquery');
			    	    	var result = do_listrow_action(action, row_id, true);
		    			}
		    		}
		    	});
		    }
	    });

	    // List Page Hover Box

	    $('.listrow-hoverbox').mouseenter(function(e){
	    	var rel = $(this).attr('rel');
	    	$('#'+rel).appendTo('body');
	    });
	    $('.listrow-hoverbox').mouseover(function(e){
	    	var rel = $(this).attr('rel');
	    	$('#'+rel).show();
	        $('#'+rel).css({
	            left:  e.pageX,
	            top:   e.pageY
	        });
	    });
	    $('.listrow-hoverbox').mouseout(function(){
	    	var rel = $(this).attr('rel');
	    	$('#'+rel).hide();
	    });

	    // List Page Row Actions

	    $('.listtable').delegate('.listrow', 'mouseover', function(){
	    	var elem = $(this);
            var hl = elem.attr('class');
	        elem.addClass('listrow-hover').find('.list-actions').show();
            elem.attr('rel', hl);
	    });

	    $('.listtable').delegate('.listrow', 'mouseout', function(){
	    	var elem = $(this);
            var hl = elem.attr('rel');
	        elem.find('.list-actions').hide();
	        elem.attr('class', hl);
	    });

	    // - publish, unpublish, delete, undelete, archive, unarchive, activate, deactivate
	    $(document).delegate('.action_publish, .action_delete, .action_archive, .action_activate, .action_unpublish, .action_undelete, .action_unarchive, .action_deactivate', 'click', function(){
	    	var row_id   = $(this).attr('rel');
	    	var action   = $(this).attr('class').substr(7);
	    	var altparam = $(this).attr('altparam');
	    	var altgroup = $(this).attr('altgroup');
	    	var addquery = $(this).attr('addquery');
	    	var more     = ((full_delete && action == 'delete') ? ' (this cannot be undone)' : '');
	    	if(confirm('Are you sure you want to ' + action.toUpperCase() + ' this '+page_subject+more + '?')){
	    		var result = do_listrow_action(action, row_id, true);
	    	}
	    });

	    // - edit (call to edit page)
	    $(document).delegate('.action_edit', 'click', function(){
	    	var row_id = $(this).attr('rel');
	    	if($(this).hasClass('editfromorg')){
	    		if($('#organize_mod').val() != ''){
	    			if(confirm('You have changed the list order.  Do you want to save the changes first?')){
	    				$('.action_saveorg').trigger('click');
	    			}
	    		}
	    	}
	    	do_listrow_action('edit', row_id, true);
	    });

	    // - add (call to add page)
	    $(document).delegate('.action_add', 'click', function(){
	    	var row_id = $(this).attr('rel');
	    	do_listrow_action('add', row_id, true);
	    });

	    // - view (call to view page)
	    $(document).delegate('.action_view', 'click', function(){
	    	var row_id = $(this).attr('rel');
	    	do_listrow_action('view', row_id, true);
	    });

	    // - view child records (deprecated)
	    $(document).delegate('.action_viewrecs', 'click', function(){
	    	var row_id = $(this).attr('rel');
	    	if(page_childsubject == '' || page_childsubject == undefined || page_childsubject == null) page_childsubject = page_ingroup.replace(/_cat/i, '');
	    	do_listrow_action('listsub', row_id, true);
	    });

	    // - view pages (deprecated)
	    $(document).delegate('.action_viewpages', 'click', function(){
	    	var row_id = $(this).attr('rel');
	    	if(page_childsubject == '' || page_childsubject == undefined || page_childsubject == null) page_childsubject = page_ingroup.replace(/_cat/i, '');
	    	do_listrow_action('listsub', row_id, true);
	    });

	    // - clone
	    $(document).delegate('.action_clone', 'click', function(){
	    	var row_id   = $(this).attr('rel');
	    	var action   = $(this).attr('class').substr(7);
	    	var tag		 = $(this).attr('tag');
			var cloneinput = prompt('Enter a unique title for the new record:', tag+'_new');
			if(cloneinput != '' && cloneinput != null){
				if(tag.toLowerCase() == cloneinput.toLowerCase()) {
				   	alert("The new record must have a unique title.");
				}else{
					alert("Attention: Please remember that the new record will share the same images, documents and other uploaded files as the original record.\n\nKeep this in mind when deleting either record.")
					$('#x_data').val(cloneinput);
					var result = do_listrow_action(action, row_id, true);
				}
			}
	    });

	    // - set default
	    $(document).delegate('.action_default', 'click', function(){
	    	var row_id = $(this).attr('rel');
	    	var action   = $(this).attr('class').substr(7);
	    	if(confirm('Are you sure you want to set this '+page_subject+' as the default?')){
	    		var result = do_listrow_action(action, row_id, true);
	    	}
	    });

	    // Listrow action commander - do the action via AJAX
	    function do_listrow_action(action, row_id, showalerts){
	    	var x_data = $('#x_data').val();
    		var param = {'action':action, 'row_id':row_id, 'x_data':x_data};
    		var pagedata = {'page_url':encodeURIComponent(page_url), 'page_subject':page_subject, 'page_ingroup':page_ingroup, 'page_taxonomy_id':page_taxonomy_id, 'page_term_id':page_term_id};
            $.foundry.showThrobber();
            $.post(
                $.foundry.ajax_url,
                {'op':'executelistaction', 'param':param, 'pagedata':pagedata},
                function(jsondata){
                	if(jsondata.success){
                		if(showalerts && jsondata.rtndata.alert != 'null' && jsondata.rtndata.alert != null) alert(jsondata.rtndata.alert);
                		if(jsondata.rtndata.gotopage != 'null' && jsondata.rtndata.gotopage != null){
                			window.location = jsondata.rtndata.gotopage;
                		}else{
                			$('#listbody').html(jsondata.rtndata.html);
                		}
                	}else{
                		if(showalerts) alert('The process to '+action+' the selected record(s) did not succeed. '+jsondata.rtndata.alert);
                	}
                    $.foundry.hideThrobber();
                	return jsondata.success;
                },
                "json"
            );
	    }

	    // Save advanced bulk action modifications
	    $(document).delegate('#advaction_submit', 'click', function(e){
	    	e.preventDefault();
	    	var data = [];
	    	if($('.advaction_check:checked').length == 0)
	    		if(confirm('You have not selected anything to change.  Do you want to return and check the items that will be changed?'))
	    			return false;
	    	$('.advaction_check:checked').each(function(){
	    		var item = $(this).closest('.advaction_row').find('.advaction_data');
	    		var name = item.attr('name');
	    		var datum = item.val();
	    		if(item.is('input')){
	    			if(item.attr('type') == 'checkbox' && !item.is(':checked'))	datum = 0;
	    			if(name == 'advaction_protected')
	    				data.push("password>>>"+$('#advaction_password').val());
	    		}
	    		if(datum != undefined) data.push(name.replace("advaction_", "")+">>>"+datum);
	    	});
	    	if(data.length > 0){
		    	var x_data = $('#x_data').val();
	    		// var param = {'action':action, 'row_id':row_id, 'x_data':x_data};
	    		var pagedata = {'page_url':encodeURIComponent(page_url), 'page_subject':page_subject, 'page_ingroup':page_ingroup};
	            $.foundry.showThrobber();
	            $.post(
	                $.foundry.ajax_url,
	                {'op':'saveadvactions', 'data':data, 'row_ids':$('#advaction_rowids').val(), 'pagedata':pagedata},
	                function(jsondata){
	                	if(jsondata.success){
	                		alert('The bulk changes were completed successfully.');
	                		$('#listbody').html(jsondata.rtndata.html);
	                	}else{
	                		if(showalerts) alert('The bulk changes could not be completed at this time.');
	                	}
	                    $.foundry.hideThrobber();
	                },
	                "json"
	            );
	    	}
			$('#listrow-bulk-actions-adv').hide();
	    });

	    // List Page Pagination Block Button Actions

	    $(document).delegate('.action_goback', 'click', function(){
	    	if(page_parentgroup == '' || page_parentgroup == undefined || page_parentgroup == null) page_parentgroup = page_ingroup + '_cat';
	    	window.location = 'list-'+page_parentgroup+'.php';
	    });

	    $(document).delegate('.action_organize', 'click', function(){
    		var pagedata = {'page_url':encodeURIComponent(page_url), 'page_subject':page_subject, 'page_ingroup':page_ingroup};
			var tag = $('<div id="organizepanel"></div>'); // This tag will the hold the dialog content.
            $.foundry.showThrobber();
            $.post(
                $.foundry.ajax_url,
                {'op':'loadorganizer', 'pagedata':pagedata},
                function(html){
                	// - display the dialog
					tag.html(html)
						.dialog({modal: false, width: 700, title: 'Organize'})
						.dialog('open');
					$("#organize").sortable({
						opacity: 0.6,
						cursor: 'move',
						revert: true,
						update: function(event, ui) { $('#organize_mod').val('touched'); }
					});
                    $.foundry.hideThrobber();
                },
                "text"
            );
	    });

	    $(document).delegate('.action_saveorg', 'click', function(){
	    	// - collect the ids in DOM order from the .orgitem objects
	    	var id = 0;
	    	var rtn = '';
	    	var rank = 1;
	    	$('.orgitem').each(function(){
	    		id = $(this).val();
	    		if(rtn != '') rtn += ',';
	    		rtn += id+':'+rank;
	    		rank++;
	    	});
	    	$('#x_data').val(rtn);
	    	var result = do_listrow_action('saveorganize', 0, true);
	    	$('#organizepanel').dialog('close').remove();
	    });

	    // Editor Pages

	    if($('#page_type').length > 0){
	    	$('input').change(function(){
	    		window.onbeforeunload = $.foundry.confirmexit;
	    	});
	    }

	    $('#date_published_scheduled').datepicker({
	    	dateFormat: $('#date_published_scheduled').attr("rel"),
			changeMonth: true,
			changeYear: true
	    });

	    $(document).delegate('.editor_button_prev_page', 'click', function(e){
	    	e.preventDefault();
	    	window.location = $(this).attr('rel');
	    });

	    $(document).delegate('.editor_button_goback', 'click', function(e){
	    	e.preventDefault();
	    	window.location = "list-"+$(this).attr('rel')+".php";
	    });

	    $(document).delegate('.editor_button_preview', 'click', function(e){
	    	e.preventDefault();
            var url;
	    	if($('#pagename') != undefined){
	    		url = $('#pagename').val();
	    	}else{
	    		url = $('#code').val();
	    	}
    		url = $.foundry.base_url.replace("admin/", "")+url;
    		window.open(url, '_blank');
	    });

	    $(document).delegate('.editor_button_add_new', 'click', function(e){
	    	e.preventDefault();
	    	window.location = $(this).attr('rel');
	    });

	    $(document).delegate('.editor_button_info', 'click', function(e){
	    	e.preventDefault();
	    	$('.dialogpanel.info_content').dialog('open');
	    });

	    $(document).delegate('.editor_button_save', 'click', function(e){
	    	var form = $('#_n').parent('form');
	    	var button = $(this);
	    	if(validate()){
		    	var pub = 1, draft = 0;
		    	if($('#content_status').is('select')){
			    	if($('#content_status').val() == 'draft'){
			    		pub = 0; draft = 1;
			    	}
			    }else{
			    	if($('#content_status').attr('checked') != 'checked'){
			    		pub = 0; draft = 1;
			    	}
			    }
		    	$('#published').val(pub);
		    	$('#draft').val(draft);
		    	if($('#date_published_scheduled').length > 0){
		    		if($('#date_published_scheduled').val() != '') $('#date_published').val($('#date_published_scheduled').val()+" "+$('#date_published_scheduled_time').val());
		    	}else{
		    		$('#date_published').val('');
		    	}

		    	$('#_savebuttonpressed').val(button.attr('name'));
		    	form.submit();
		    }
	    });

	    $(document).delegate('.editor_button_status', 'click', function(e){
	    	e.preventDefault();
            $('#stats_content').siblings('div').hide();
            if($('#stats_content').is(':visible')){
            	$('#stats_content').slideUp(300);
            }else{
            	$('#stats_content').slideDown(300);
            }
	    });

	    $(document).delegate('.editor_button_more', 'click', function(e){
	    	e.preventDefault();
	    	var elem = $(this);
            $('#update_options').siblings('div').hide();
            if($('#update_options').is(':visible')){
            	$('#update_options').slideUp(300);
            	elem.removeClass('fa-arrow-up').addClass('fa-arrow-down');
            }else{
            	$('#update_options').slideDown(300);
            	elem.removeClass('fa-arrow-down').addClass('fa-arrow-up');
            }
	    });

	    $(document).delegate('#edit_content *', 'click', function(){
            if($('#update_options').is(':visible')){
            	$('#update_options').slideUp(300);
            	$('.editor_button_more').css('background-position', '-3px 2px').removeClass('fa-arrow-up').addClass('fa-arrow-down');
            }
	    });

	    $(document).delegate('.editpage_copydata', 'click', function(e){
	    	e.preventDefault();
	    	var rel = $(this).attr('rel').split(',');
	    	$('#'+rel[1]).val(removeHTMLTags($('#'+rel[0]).val(), false, 255));
	    });

	    $(document).delegate('.noac', 'click', function(){
	    	$(this).removeAttr('readonly');
	    });

	    $(".datepicker").click(function(){
		    $(this).datepicker({dateFormat:$(this).attr('rel')});
		    $(this).datepicker("setDate", $(this).val());
		    $(this).datepicker("show");
	    });

	    if($('#in_use').length > 0 && $('#in_use').val() == '1'){
	    	var takeover = false;
	    	var row_id = $('#row_id').val();
	    	var data_type = $('#page_ingroup').val();
    		$('#contentarea').append('<div id="overlay"></div>');
	    	if(confirm("This data is currently locked for editing by another user.\n\nDo you want to take over editorial control?")){
	            $.post(
	                $.foundry.ajax_url,
	                {'op':'takeoveredit', 'data_id':row_id, 'data_type':data_type},
	                function(jsondata){
	                	if(jsondata.success) {
	                		takeover = true;
				    		$('#overlay').remove();
				    	}
	                },
	                "json"
	            )
	    	}
	    }

	    function validate(){
	    	var elem, invalid = '', validatemessage;
	    	$('.validate').each(function(){
	    		elem = $(this);
                if(elem.hasAttr('data-validate'))
                    validatemessage = elem.attr('data-validate');
                else
                    validatemessage = elem.attr('name');
	    		if(elem.hasClass('ckeditor')) elem.val(CKEDITOR.instances.content.getData());

	    		if(elem.val() == '') invalid += validatemessage+"\n";
	    	});
	    	if(invalid != ''){
	    		alert("The following item(s) are required:\n\n"+invalid);
	    		return false;
	    	}else{
	    		return true;
	    	}
	    }
	});
});
