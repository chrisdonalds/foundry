/**
 * Foundry UI Scripts
 * @author Chris Donalds (cdonalds01@gmail.com)
 * @requires jQuery 1.4.4+
 */
/* ------------------------------------------------------------------------------------- */
if(typeof jQuery == undefined){
    document.write(unescape("%3Cscript src='../jquery.min.js' type='text/javascript'%3E%3C/script%3E"));
}

var $jq = jQuery;       // as an alternate, users/developers can use $jq in place of jQuery or a general $
var jQx = 0;
var jQy = 0;

$jq(function($) {

    // IE 7/8 specific script handler
    if ($.browser.msie) {
        var elm = $("#admloginbox");
        if(elm.length > 0){
            var pos = $(elm).position();
            elm.after("<div id='admloginbox-ie-shadow'></div>");
            $("#admloginbox-ie-shadow").width($(elm).width() + 30).height($(elm).height() + 30).css("left", (pos.left - 15) + "px").css("top",(pos.top + 151) + "px");
        }
    }

    // Prepare global Foundry object
    $.foundry = {
        setup: function(){
            $.foundry.in_process = false;
            $.foundry.base_url = $('#base_url').val();
            $.foundry.admin_core_url = (($('#admin_folder').length > 0) ? $('#admin_folder').val() : ((this.base_url != undefined) ? this.base_url + 'inc/_core/' : undefined));
            $.foundry.ajax_url = ((this.admin_core_url != undefined) ? this.admin_core_url + 'ajaxwrapper.php' : undefined);
            $.foundry.lastPopupBalloonTrigger = null;
            $.foundry.popupBalloonObj = undefined;
            $.foundry.pulse = setInterval(function() { $.foundry.pulseEvent(); }, 10000);
            $.foundry.extenders = {
                pulseactive: true,
                pulseEvent: undefined,
                grid: undefined,
                warnpageunload: true,
                hovertip: true
            };
        },
        pulseEvent: function(){
            if(this.ajax_url != undefined && $.foundry.extenders.pulseactive == true){
                $.ajax({
                    type: "POST",
                    url: this.ajax_url,
                    data: "op=pulseaction&val=logincheck",
                    dataType: "json",
                    async: false,
                    success: function(jsondata){
                        if(jsondata.success){
                            clearInterval($.foundry.pulse);
                            if(jsondata.rtndata.logincheck == false)
                                $.foundry.loginCheckPopup();
                        }
                    },
                    complete: function(){
                        if('function' === typeof $.foundry.extenders.pulseEvent)
                            $.foundry.extenders.pulseEvent();
                    }
                });
            }
        },
        setupeditorparams: function(editor){
            $.ajax({
                type: "POST",
                url: this.ajax_url,
                data: "op=getcmsparams&editor="+editor,
                dataType: "json",
                async: false,
                success: function(jsondata){
                    if(jsondata.success){
                        for(var prop in jsondata.rtndata){
                            $.foundry[prop] = jsondata.rtndata[prop];
                        }
                    }else{
                        alert('CMS parameter loading error!');
                    }
                }
            });
        },
        getVar: function(variable){
            $.post(
                $.foundry.ajax_url,
                {'op':'getvar'},
                function(jsondata){
                    if(jsondata.success){
                        return jsondata.rtndata;
                    }
                },
                'json'
            );
        },
        showPopupBalloon: function(elem, balloonobj, op, nudgex, nudgey, width, contents, gridoptions){
            if($.foundry.lastPopupBalloonTrigger != elem.attr('id')) {
                $.foundry.lastPopupBalloonTrigger = elem.attr('id');
                $.foundry.popupBalloonObj = $(balloonobj);
                var left = elem.offset().left - width + ((nudgex != undefined) ? nudgex : 0);
                var top = elem.position().top + ((nudgey != undefined) ? nudgey : 0);
                var url = $.foundry.ajax_url+'?op='+op;
                if(url == undefined || url == '') return false;

                if(contents != undefined) url += '&data='+contents;
                if(width == undefined) width = 280;
                $.foundry.showThrobber();
                $.foundry.popupBalloonObj.find('div.inner_a').load(url, function(){
                    $.foundry.popupBalloonObj.css({'left':left, 'top':top, 'width':width}).attr('rel', elem.attr('id')).fadeIn().removeClass('stayvisible');
                    if(gridoptions != undefined){
                        if('function' === typeof $.foundry.extenders.grid)
                            $.foundry.extenders.grid(gridoptions);
                    }
                });
                $.foundry.hideThrobber();
            }
        },
        closePopupBalloon: function(){
            $.foundry.popupBalloonObj.fadeOut(200);
            $.foundry.lastPopupBalloonTrigger = null;
        },
        showThrobber: function(x, y, text){
            if(x == undefined) x = '50%';
            if(y == undefined) y = '50%';
            if(text == undefined)
                text = 'Please Wait...';

            if($('#display_throbber').length > 0){
                $('#display_throbber').css({top: y, left: x, position: 'fixed'}).text(text).show();
            }
        },
        hideThrobber: function(){
            if($('#display_throbber').length > 0) $('#display_throbber').hide();
        },
        confirmexit: function(){
            if($.foundry.extenders.warnpageunload){
                return "You are attempting to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
            }
        },
        resize: function(){
            console.log($('#wrapper').height() - parseInt($('#content-wrapper').css('padding-top')));
            $('#content-wrapper').height($('#wrapper').height() - parseInt($('#content-wrapper').css('padding-top')));
        },
        loginCheckPopup: function(){
            $('body').append('<div id="admlogin_flash"></div>');
            $('#admlogin_flash').dialog({
                modal: true,
                resizable: false,
                width: 450,
                title: "Login Required",
                beforeClose: function(event, ui) {
                    return false;
                },
                open: function(event, ui){
                    $('#admlogin_flash').load($('#base_url').val() + 'admrelogin.php?admsubmit=logincheck&rurl='+encodeURIComponent(window.location.href));
                }
            })
        }
    };
    $.foundry.setup();

	$(document).ready(function(){
        $("#helptabs").tabs();
        $("#cfgdiv").tabs();

	    $(".dialogpanel").dialog({
			autoOpen: false,
			width: $(this).attr('data-width'),
	        height: $(this).attr('data-height'),
			modal: true,
	        resizable: false
	    });

        if($(".accordion").length > 0){
            $(".accordion").accordion({
                autoHeight: false,
                navigation: true,
                collapsible: true,
                header: '.cfgpanel'
            });
        }

        $('html').click(function(e){
            if($.foundry.popupBalloonObj != undefined && $.foundry.popupBalloonObj.is(':visible') && !$.foundry.popupBalloonObj.hasClass('stayvisible')) {
                $.foundry.closePopupBalloon();
            }
        });

        $(document).delegate('.closebutton', 'click', function(e){
            e.preventDefault();
            $(this).parent().hide();
        });

        /* Loading Throbber */

        $('body').append('<span id="display_throbber"></span>')

        /* Accordion Expanders */

        $(document).delegate('.expand-button', 'click', function(e){
            e.preventDefault();
            var elem = $(this);
            var parent = elem.parent();
            var parent_height = parseInt(parent.height()) + 2;
            var container = parent.closest('.expandable');
            if(!container.hasClass('collapsed')){
                // collapse
                var h = container.height();
                container.attr('data-height', h).animate({height: parent_height+'px'}, 300, function(){
                    $(this).addClass('collapsed');
                    elem.addClass('expand-closed fa-arrow-down').removeClass('fa-arrow-up');
                });
            }else{
                // expand
                container.animate({height: container.attr('data-height')+'px'}, 600, function(){
                    $(this).css({'height':''}).removeClass('collapsed');
                    elem.removeClass('expand-closed fa-arrow-down').addClass('fa-arrow-up');
                });
            }
        });

        $(document).delegate('.clickvars a', 'click', function(e){
            e.preventDefault();
            var sel = $(this).siblings('select');
            sel.toggle();
        });

        $(document).delegate('.clickvars select', 'change', function(){
            var clicktarget = $(this).parent().parent().find('.clickvar_target');
            console.log(clicktarget);
            if(clicktarget.length > 0) clicktarget.val(clicktarget.val() + $(this).val());
        })

        $(document).delegate('.accordion_container', 'mouseover', function(e){
            e.preventDefault();
            var ul = $(this).closest('ul');
            var li = $(this).closest('li');
            var ex = ul.find('.expand-button');
            ul.find('.accordion_child').not(':animated').slideUp().each(function(){
                $(this).closest('li').find('.expand-button').addClass('expand-closed');
            });
            li.find('.accordion_child').not(':animated').slideToggle(function(){
                var eb = $(this).closest('li').find('.expand-button');
                if($(this).is(':visible'))
                    eb.removeClass('expand-closed');
                else
                    eb.addClass('expand-closed');
            });
        });

        /* Toolbar */

        $('div').delegate('#admuser_logout', 'click', function(){
            window.location = $.foundry.base_url+'admlogin.php?admsubmit=Logout';
        });

        $('div').delegate('#admuser_close', 'click', function(){
            $.foundry.closePopupBalloon();
        });

        $('#admusertool').click(function(e){
            e.preventDefault();
            $.foundry.showPopupBalloon($(this), '#admPopupBalloon', 'getadmuserpopup', 24, 30, 280);
        });

        $('#admsettings').click(function(e){
            e.preventDefault();
            $.foundry.showPopupBalloon($(this), '#admPopupBalloon', 'getadmsettingspopup', 24, 30, 600, '', {selector: '#admsettings_quick_menu', items: '.admsettings_menu_item', itemwidth: 200});
        });

        $('#admhelp').click(function(e){
            e.preventDefault();
            $.foundry.showPopupBalloon($(this), '#admPopupBalloon', 'getadmhelppopup', 24, 30, 400, $(this).find('a').attr('rel'));
        });

        $(document).delegate('#help_phpcfg_link', 'click', function(e){
            e.preventDefault();
            $.post(
                $.foundry.ajax_url,
                {'op':'getphpinfo'},
                function(html){
                    if(html != ''){
                        var w = window.open();
                        $(w.document.body).html(html);
                    }
                }
            );
        });

	    if($.foundry.base_url != undefined){
            // Dialog
            $('.dialogbutton').click(function(){
                var w = $(this).attr('data-width');
                var h = $(this).attr('data-height');
                var u = $(this).attr('data-url');
                $('.dialogpanel.helpdialog').load(
                    $.foundry.base_url + u,
                    function(){
                        $('.dialogpanel.helpdialog').dialog({width: w, minHeight: h}).dialog('open');
                    });
                return false;
            });

            // Form Control
	    	$.getScript($.foundry.base_url + 'inc/_js/ui/ui.script_form.js');

            // Hover Tip (tm)
            if($.foundry.extenders.hovertip == true) $.getScript($.foundry.base_url + 'inc/_js/ui/ui.hovertip.js');
	    }

        /* Settings */

        if($('#settingstabs').length > 0){
            // $.getScript($.foundry.base_url+'inc/_js/ui/ui.script_settings.js');
        }

        /* Login Check */

        /* Error Messages */

        $(document).delegate('.msg-close', 'click', function(e){
            e.preventDefault();
            var ul = $(this).closest('ul');
            $(this).closest('li').remove();
            if(ul.find('li').length == 0) ul.parent().html('').hide();
        });
    });

    $.fn.hasAttr = function(name){
        return this.attr(name) !== undefined && this.attr(name) != '' && this.attr(name) != null;
    };

    $.fn.hasAncestor = function(a){
        return ($(this).closest(a).length > 0);
    };

    $.fn.extend({
        insertAtCaret: function(myValue){
            return this.each(function(i) {
                if (document.selection) {
                    //For browsers like Internet Explorer
                    this.focus();
                    var sel = document.selection.createRange();
                    sel.text = myValue;
                    this.focus();
                } else if (this.selectionStart || this.selectionStart == '0') {
                    //For browsers like Firefox and Webkit based
                    var startPos = this.selectionStart;
                    var endPos = this.selectionEnd;
                    var scrollTop = this.scrollTop;
                    this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
                    this.focus();
                    this.selectionStart = startPos + myValue.length;
                    this.selectionEnd = startPos + myValue.length;
                    this.scrollTop = scrollTop;
                } else {
                    this.value += myValue;
                    this.focus();
                }
            });
        }
    });

    /*
    * hoverIntent | Copyright 2011 Brian Cherne
    * http://cherne.net/brian/resources/jquery.hoverIntent.html
    * modified by the jQuery UI team
    */
    $.event.special.hoverintent = {
        setup: function() {
            $( this ).bind( "mouseover", jQuery.event.special.hoverintent.handler );
        },
        teardown: function() {
            $( this ).unbind( "mouseover", jQuery.event.special.hoverintent.handler );
        },
        handler: function( event ) {
                var currentX, currentY, timeout,
                args = arguments,
                target = $( event.target ),
                previousX = event.pageX,
                previousY = event.pageY;
                function track( event ) {
                currentX = event.pageX;
                currentY = event.pageY;
            };
            function clear() {
                target
                .unbind( "mousemove", track )
                .unbind( "mouseout", clear );
                clearTimeout( timeout );
            }
            function handler() {
                var prop,
                orig = event;
                if ( ( Math.abs( previousX - currentX ) +
                Math.abs( previousY - currentY ) ) < 7 ) {
                    clear();
                    event = $.Event( "hoverintent" );
                    for ( prop in orig ) {
                        if ( !( prop in event ) ) {
                            event[ prop ] = orig[ prop ];
                        }
                    }
                    // Prevent accessing the original event since the new event
                    // is fired asynchronously and the old event is no longer
                    // usable (#6028)
                    delete event.originalEvent;
                    target.trigger( event );
                } else {
                    previousX = currentX;
                    previousY = currentY;
                    timeout = setTimeout( handler, 100 );
                }
            }
            timeout = setTimeout( handler, 100 );
            target.bind({
                mousemove: track,
                mouseout: clear
            });
        }
    };

});