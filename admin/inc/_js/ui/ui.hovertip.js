/**
 * Foundry UI Scripts -- Hovertip
 * @author Chris Donalds (cdonalds01@gmail.com)
 * @requires jQuery 1.4.4+ and ui.script.js
 */
/* ------------------------------------------------------------------------------------- */
jQuery(function($) {
    $(document).ready(function(){
        $('body').append('<div id="hovertip_div"></div>');

        $(document).delegate('.hovertip', 'click', function(e){
            e.preventDefault();
        });
        $(document).delegate('.hovertip', 'mouseenter', function(e){
            var contents = $(this).attr('alt');
            if(contents != ''){
                $('#hovertip_div').html(contents);
            }
        });
        $(document).delegate('.hovertip', 'mousemove', function(e){
            if($(this).attr('alt') != ''){
                var hw = $('#hovertip_div').width();
                var hh = $('#hovertip_div').height();
                var ww = $(document).width();
                var wh = $(document).height();
                var px = ((e.pageX + 20 + hw < ww) ? e.pageX + 20 : ww - hw);
                var py = ((e.pageY + 4 + hh < wh) ? e.pageY + 14 : wh - hh);
                $('#hovertip_div').show();
                $('#hovertip_div').css({
                    left:  px,
                    top:   py
                });
            }
        });
        $(document).delegate('.hovertip', 'mouseleave', function(e){
            $('#hovertip_div').hide();
        });

        $(document).delegate('.triggerhelp', 'click', function(e){
            e.preventDefault();
            var elem = $(this);
            var helpdiv = elem.attr('rel');
            if($(helpdiv).length > 0){
                var hw = $(helpdiv).width();
                var hh = $(helpdiv).height();
                var ww = $(document).width();
                var wh = $(document).height();
                var px = elem.position().left + 20;
                var py = elem.position().top + 16;

                if(elem.hasClass('align_bottom')){
                    py -= $(helpdiv).height()+elem.height();
                }
                $(helpdiv).show();
                $(helpdiv).css({
                    left:  px,
                    top:   py
                });
                $(helpdiv).find('.close').attr('rel', helpdiv);
                if(elem.hasAttr('data-rel')) $(helpdiv).attr('rel', elem.attr('data-rel'));
            }
        });

        $(document).delegate('.help_div .close', 'click', function(e){
            e.preventDefault();
            var helpdiv = $(this).attr('rel');
            $(helpdiv).hide();
        });
    });
});
