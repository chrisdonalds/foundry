/**
 * Foundry UI Scripts -- Settings
 * @author Chris Donalds (cdonalds01@gmail.com)
 * @requires jQuery 1.4.4+ and ui.script.js
 */
/* ------------------------------------------------------------------------------------- */
jQuery(function($){
    $("#settingstabs").tabs();
    $("#tabs-themes").tabs();
    $("#tabs-menus").tabs();
    $("#tabs-plugins").tabs();
    $("#tabs-adv").tabs();
    $("#tabs-media").tabs();

    $("#waitoverlay").addClass('hidden');
    $("#contentarea").removeClass('hidden');

	$(document).ready(function(){

        if($('#settingstabs').length > 0){
            // Settings tab/sub-tab redirection
            var t = window.location.search.replace(/\?/g, "");
            if (t != "") {
                t = t.split("&");
                var n = "";
                var r = "";
                var s = "";
                for (i in t) {
                    var o = t[i].split("=");
                    if (o[0] == "tab") n = o[1];
                    else if (o[0] == "sub") r = o[1];
                    else if (o[0] == "tch") s = o[1]
                }
                if (n != "") {
                    $("#settingstabs").tabs("select", n);
                    var n2 = $("#tabs-" + n).find('.settingstabs2');
                    if (n2.length > 0) {
                        n2.tabs().removeClass('ui-widget-content');
                        if(r != "")
                            n2.tabs("select", r);
                        else
                            n2.tabs("select", 0);
                    }
                }
                if (s != "") {
                    s = s.split(",");
                    if (s.length == 1) {
                        $(s[0]).trigger("click");
                    } else if (s.length == 2) {
                        $(s[0]).eq(s[1]).trigger("click");
                    }
                }
            }

            // Settings Dialog

            $(document).delegate('#settingsdialog', 'mouseover', function(e){
                jQx = e.pageX - this.offsetLeft;
                jQy = e.pageY - this.offsetTop;
            });

            $("#pluginsettingsdialog").dialog({
                autoOpen: false,
                width: 600,
                height: 400,
                modal: true,
                resizable: false
            });

            $("#themesettingsdialog").dialog({
                autoOpen: false,
                width: 800,
                height: 600,
                modal: true,
                resizable: false
            });

            // Tabs

            $('.settingstabs_link').click(function(e){
                var elem = $(this);
                var loaded = elem.hasClass('loaded');

                if(!loaded){
                    var tab = elem.attr('href').toLowerCase().replace("#tabs-", "");
                    var tab_id = elem.attr('href');
                    var custom = (elem.hasClass('settingstab_custom')) ? true : false;
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'getadminsettingstabcontents', val:tab, custom:custom},
                        function(html){
                            if(html != ''){
                                // contents
                                elem.addClass("loaded");
                                $(tab_id).html(html);
                                $("#settingstabs").tabs("select", tab_id);
                                var tabs2 = $(tab_id).find('.settingstabs2');
                                if(tabs2.length > 0){
                                    // subtab
                                    tabs2.tabs().removeClass('ui-widget-content');
                                }
                            }
                            $.foundry.hideThrobber();
                        },
                        'html'
                    );
                }
            });

            // Saving.  Process items for saving

            $(document).delegate('#cfgsubmit', 'click', function(){
                // user types
                $('.ut_name').each(function(){
                    if($.trim($(this).val()) == '') $(this).parent().hide();
                    if($(this).is(':visible')){
                        //$(this).val($(this).val() + '|' + $(this).attr('rel'));
                    }else{
                        $(this).parent().remove();
                    }
                });
                return true;
            });

            // Misc

            $('div').delegate('.settings_hover_row', 'mouseover', function(){
                $(this).addClass('highlight-row').find('.action_items').show().removeClass('hidden');
            });

            $('div').delegate('.settings_hover_row', 'mouseout', function(){
                $(this).removeClass('highlight-row').find('.action_items').hide();
            });

            // Media

            $('div').delegate('.media_format_new', 'click', function(e){
                e.preventDefault();
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'addmediaformat'},
                    function(jsondata){
                        if(jsondata.success){
                            $('#media-body').append(jsondata.rtndata);
                            $('.media_formats_title:last').focus();
                        }
                        $.foundry.hideThrobber();
                    },
                    'json');
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.media_formats_act', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var p_row = elem.closest('.media_formats_row');
                var p_id = elem.attr('rel');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'setmediaformatactivestate', id:p_id},
                    function(jsondata){
                        if(jsondata.success){
                            alert("The '" + p_row.find('strong').text() + "' format has been "+((jsondata.rtndata == 1) ? "activated" : "deactivated")+".");
                            elem.text(((jsondata.rtndata == 1) ? 'Deactivate' : 'Activate'));
                        }
                        $.foundry.hideThrobber();
                    },
                    'json');
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.media_formats_title_trigger', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                elem.find('strong').addClass('hidden');
                elem.siblings('input.media_formats_title').removeClass('hidden').focus();
            });

            $('div').delegate('.media_formats_title', 'blur', function(e){
                var elem = $(this);
                var p_id = elem.attr('rel');
                var displayelem = elem.siblings('.media_formats_title_trigger').find('strong');
                if(elem.val().toLowerCase().trim() != displayelem.text().toLowerCase().trim()){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'renamemediaformattitle', id:p_id, val:elem.val()},
                        function(jsondata){
                            if(jsondata.success){
                                var text = elem.val().substr(0,1).toUpperCase() + elem.val().substr(1);
                                displayelem.text(text).removeClass('hidden');
                                elem.addClass('hidden');
                                if(jsondata.rtndata != null && p_id > 0) alert("The format has been renamed.");
                            }else {
                                alert(jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }else if(elem.val().trim() != ''){
                    displayelem.removeClass('hidden');
                    elem.addClass('hidden');
                }else{
                    alert('The media format title cannot be blank.')
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.media_formats_ovr', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var p_row = elem.closest('.mf-col-title');
                var p_id = elem.attr('rel');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'overridemediaformat', id:p_id},
                    function(jsondata){
                        if(jsondata.success){
                            var text = ((elem.text() == 'Revert') ? 'media format has been reverted' : 'media format is ready for editing');
                            $('#media-body').html(jsondata.rtndata);
                            alert("The '" + p_row.find('strong').text() + "' " + text + ".");
                        }
                        $.foundry.hideThrobber();
                    },
                    'json');
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.media_formats_del', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var p_row = elem.closest('.media_formats_row');
                var p_id = elem.attr('rel');
                if(confirm("Do you want to permanently delete the media format '" + p_row.find('strong').text() + "'?")){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'deletemediaformat', val:'', id:p_id},
                        function(jsondata){
                            if(jsondata.success){
                                p_row.effect("pulsate", {times: 2}, 200, function(){p_row.remove();});
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.media_formats_actions_types', 'change', function(e){
                var elem = $(this);
                var p_row = elem.closest('.mf-col-actions');
                var id = elem.attr('rel');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'addmediaformataction', val:elem.val(), id:id, name:elem.children("option:selected").text()},
                    function(jsondata){
                        if(jsondata.success){
                            elem.before(jsondata.rtndata);
                        }else{
                            alert(jsondata.rtndata);
                        }
                        elem.val('');
                        $.foundry.hideThrobber();
                    },
                    'json');
                return false;
            });

            $('div').delegate('.media_formats_actions_delete', 'click', function(e){
                var elem = $(this);
                var rel = elem.attr('rel');
                var p_row = elem.closest('.media_formats_action');
                if(confirm("Do you want to permanently delete this media format action?")){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'deletemediaformataction', val:rel},
                        function(jsondata){
                            if(jsondata.success){
                                p_row.effect("pulsate", {times: 2}, 200, function(){p_row.remove();});
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });


            // Themes

            $('div').delegate('.theme_act', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var p_row = elem.closest('.theme_row');
                var p_id = p_row.find('.theme_id').val();
                var p_zone = ((elem.hasClass("theme_admin") ? "admin" : "website"));
                var ok = true;
                if(p_row.hasClass('theme_state_active') && $('#theme_'+p_zone+' .theme_state_active').length == 1){
                    ok = false;
                    if(confirm('This is the only active '+p_zone+' theme.  Disabling it might cause user interface problems.  Continue?')) ok = true;
                }
                if(ok){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'setthemeactivestate', id:p_id},
                        function(jsondata){
                            if(jsondata.success){
                                alert("The '"+p_row.find('.theme_name').val()+"' theme has been "+((jsondata.rtndata == 1) ? "activated" : "deactivated")+".");
                                $.post($.foundry.ajax_url,
                                    {op:'getthemetabcontents', zone:p_zone},
                                    function(html){
                                        $('#theme_'+p_zone).html(html);
                                    }, 'html'
                                );
                                $('#cfgsubmit').trigger('click');
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.theme_del', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var p_row = elem.closest('.theme_row');
                var p_id = p_row.find('.theme_id').val();
                var p_name = p_row.find('.theme_name').val();
                if(confirm("Do you want to permanently delete the theme '" + p_name + "' and all files?")){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'deletetheme', val:'', id:p_id},
                        function(jsondata){
                            var lead = '<br/>&nbsp;&nbsp;&bull;&nbsp;';
                            if(jsondata.success){
                                p_row.effect("pulsate", {times: 2}, 200, function(){p_row.remove();});
                            }else{
                                $('#issue-themes').removeClass('disabled');
                                $('#issue-themes').append(lead + jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.theme_more', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var p_row = elem.closest('.theme_row');
                if(elem.text() == 'More'){
                    // show
                    var p_id = p_row.find('.theme_id').val();
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'getthemedata', id:p_id},
                        function(jsondata){
                            if(jsondata.success){
                                p_row.find('.theme_info').html(jsondata.rtndata).show(500);
                                elem.text('Less');
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }else{
                    // hide
                    p_row.find('.theme_info').hide(500);
                    elem.text('More');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.theme_settings_link', 'click', function(e){
                e.preventDefault();
                var settingsfunc = $(this).attr('rel');
                if(settingsfunc != ''){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'runthemesettingsfunc', val:settingsfunc},
                        function(jsondata){
                            if(jsondata.contents != ''){
                                // build and show dialog box & save button
                                var contents = jsondata.contents;
                                $('#themesettingsdialog').html(contents).dialog('open').dialog('option', 'title', jsondata.title);
                                $('#themesettingsdialog').parent().css({width:'80%', left:'50%', 'margin-left':'-40%'})
                                $('#themesettingsdialog').attr('rel', settingsfunc);
                            }else{
                                alert("There was a problem calling the settings function '"+jsondata.func+"'.");
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('#theme_settings_dialog_save', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var settingsfunc = elem.attr('rel');
                var settingsdata = $('#themesettingsform').serialize();
                if(settingsfunc != '' && !e.isPropagationStopped()){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'runthemesettingsaction', val:settingsfunc, action:'1', data:settingsdata},
                        function(jsondata){
                            if(jsondata.success){
                                // respond to save button and close dialog
                                if(jsondata.message != '') alert(jsondata.message);
                                if(jsondata.closedialog) {
                                    $('#themesettingsdialog').dialog('close');
                                    elem.attr('rel', '');
                                }
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });

            $('#themesettingsdialog').bind("dialogbeforeclose", function(e) {
                var elem = $(this);
                var settingsfunc = elem.attr('rel');
                if(settingsfunc != '' && !e.isPropagationStopped()){
                    elem.attr('rel', '');
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'runthemesettingsaction', val:settingsfunc, action:'2'},
                        function(jsondata){
                            if(jsondata.success){
                                // respond to close button (x)
                                if(jsondata.message != '') alert(jsondata.message);
                                $('#themesettingsdialog').dialog('close');
                            }
                            e.stopPropagation();
                            $.foundry.hideThrobber();
                            return false;
                        },
                        'json');
                }
            });

            // Editors

            $(document).delegate('#CMS_EDITOR', 'change', function(e){
                var cms = $(this).val();
                $('#cms_editor_settings a.plugin_settings_link').addClass('hidden');
                $('#editor_'+cms).removeClass('hidden');
            });

            // Website Menus

            var websitemenu_ns = {
                                    listType: 'ul',
                                    disableNesting: 'no-nest',
                                    forcePlaceholderSize: true,
                                    handle: 'span',
                                    items: 'li',
                                    maxLevels: 5,
                                    opacity: .6,
                                    placeholder: 'nestedsortable-placeholder',
                                    revert: 250,
                                    tabSize: 5,
                                    tolerance: 'pointer',
                                    toleranceElement: '> span',
                                    noJumpFix: 1,
                                    update: function(){
                                        updateWMB();
                                    }
                                };

            function updateWMB(){
                // update the ul classes
                var elem;
                $('#websitemenu_mainnav ul, #websitemenu_mainnav li').each(function(){
                    elem = $(this);
                    if(elem.prop('tagName').toLowerCase() == 'ul'){
                        if(!elem.hasClass('websitemenu_subnav')) elem.addClass('websitemenu_subnav');
                        if(!elem.hasClass('ui-sortable')) elem.addClass('ui-sortable');
                    }else if(elem.prop('tagName').toLowerCase() == 'li'){
                        elem.removeClass('websitemenu_subnav').removeClass('ui-sortable');
                    }
                });

                var ser = $('#websitemenu_mainnav').nestedSortable('serialize', {startingDepth: 0}).replace(/\&/g, '|');
                $.foundry.showThrobber();
                $.ajax({
                    type: "POST",
                    url: $.foundry.ajax_url,
                    data: "op=updatewebsitemenulayout&val="+ser,
                    success: function(){
                        $.foundry.hideThrobber();
                    }
                });
            }

            $('#websitemenu_menuitems_list ul').nestedSortable(websitemenu_ns);

            $(document).delegate('#websitemenu_menubar_select', 'change', function(){
                var elem = $(this);
                var key = elem.val().split(':');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'refreshwebsitemenulist', val:key[1]},
                    function(jsondata){
                        if(jsondata.success){
                            $('#websitemenu_mainnav').remove();
                            $('#websitemenu_menuitems_list').append(jsondata.rtndata);
                            $('#websitemenu_menuitems_list ul').nestedSortable(websitemenu_ns);

                            if(key[0] > 0){
                                $('#websitemenu_menubar_edit').removeClass('hidden');
                                $('#websitemenu_menubar_delete').removeClass('hidden');
                                $('#websitemenu_menuitems_add').removeClass('hidden');
                                if(jsondata.rtndata != '')
                                    $('#websitemenu_menuitems_list .websitemenu_title').text("Menu items in the '"+elem.find('option:selected').text()+"' menu:");
                                else
                                    $('#websitemenu_menuitems_list .websitemenu_title').text("No menu items are in the "+elem.find('option:selected').text()+" website menubar.  Click 'Add Menu(s)' to add one.");
                                $('#websitemenu_menubar_id').val(key[0]);
                            }else{
                                $('#websitemenu_menubar_edit').addClass('hidden');
                                $('#websitemenu_menubar_delete').addClass('hidden');
                                $('#websitemenu_menuitems_add').addClass('hidden');
                                if(jsondata.rtndata != '')
                                    $('#websitemenu_menuitems_list .websitemenu_title').text("The following menu items are disassociated from any menubars:");
                                else
                                    $('#websitemenu_menuitems_list .websitemenu_title').text("Great! No menus are currently orphaned.");
                                $('#websitemenu_menubar_id').val('0');
                            }
                        }else{
                            alert('There was a problem retrieving the menu items.');
                        }
                        $.foundry.hideThrobber();
                    },
                    "json"
                );

                $('#websitemenu_editor').html('Select a menu from left or create a new menu item...');
            });

            $(document).delegate('#websitemenu_menubar_edit, #websitemenu_menubar_create', 'click', function(e){
                e.preventDefault();
                if(!$('#websitemenu_menubar_editor').is(':visible')){
                    if($(this).attr('id') == 'websitemenu_menubar_edit'){
                        var id = $('#websitemenu_menubar_select').val().split(':') || null;
                        $.foundry.showThrobber();
                        $.post(
                            $.foundry.ajax_url,
                            {op:'getwebsitemenubarinfo', val:id[0]},
                            function(jsondata){
                                if(jsondata.success){
                                    $('#websitemenu_menubar_editor_id').val(id[0]);
                                    $('#websitemenu_menubar_title').val(jsondata.rtndata.title);
                                    $('#websitemenu_menubar_descr').val(jsondata.rtndata.descr);
                                    $('#websitemenu_menubar_slot').val(jsondata.rtndata.parent_id);
                                    $('#websitemenu_menubar_editor h3').text('Edit Menubar');
                                    $('#websitemenu_menubar_select').attr('disabled', 'disabled');
                                    $('#websitemenu_menubar_editor').slideDown(400);
                                }else{
                                    alert('The menubar info could not be retrieved!');
                                }
                                $.foundry.hideThrobber();
                            },
                            "json"
                        );
                    }else{
                        $('#websitemenu_menubar_editor_id').val('0');
                        $('#websitemenu_menubar_title').val('');
                        $('#websitemenu_menubar_descr').val('');
                        $('#websitemenu_menubar_slot').removeAttr('checked');
                        $('#websitemenu_menubar_editor h3').text('Create New Menubar');
                        $('#websitemenu_menubar_select').attr('disabled', 'disabled');
                        $('#websitemenu_menubar_editor').slideDown(400);
                    }
                }else{
                    $('#websitemenu_menubar_cancel').trigger('click');
                }
            });

            $(document).delegate('#websitemenu_menubar_cancel', 'click', function(e){
                e.preventDefault();
                $('#websitemenu_menubar_editor').slideUp(400);
                $('#websitemenu_menubar_select').removeAttr('disabled');
            });

            $(document).delegate('#websitemenu_menubar_save', 'click', function(e){
                e.preventDefault();
                var id = $('#websitemenu_menubar_editor_id').val();
                var title = $('#websitemenu_menubar_title').val();
                var descr = $('#websitemenu_menubar_descr').val();
                var slot = $('#websitemenu_menubar_slot').val();
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'savewebsitemenubarinfo', val:id, title:title, descr:descr, parent_id:slot},
                    function(jsondata){
                        if(jsondata.success){
                            if(id > 0){
                                $('#websitemenu_menubar_select option:selected').text(title);
                                alert('The menubar has been updated.');
                            }else{
                                var val = jsondata.rtndata.id+':'+jsondata.rtndata.key;
                                $('#websitemenu_menubar_select').append('<option value="'+val+'">'+title+'</option>');
                                $('#websitemenu_menubar_select').val('0:-').trigger('change');
                                alert('The menubar has been created.');
                            }
                            $('#websitemenu_menubar_cancel').trigger('click');
                        }else{
                            alert(jsondata.rtndata);
                        }
                        $.foundry.hideThrobber();
                    },
                    "json"
                );
            });

            $(document).delegate('#websitemenu_menubar_delete', 'click', function(e){
                e.preventDefault();
                var id = $('#websitemenu_menubar_select').val().split(':');
                if(confirm('Delete this menubar (all contained menu items will become orphaned)?')){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'deletewebsitemenubar', val:id[0]},
                        function(jsondata){
                            if(jsondata.success){
                                $('#websitemenu_menubar_select option:selected').remove();
                                $('#websitemenu_menubar_select').val('0:-').trigger('change');
                            }else{
                                alert(jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        "json"
                    );
                }
            });

            $(document).delegate('.websitemenu_menuitem span', 'click', function(e){
                e.preventDefault();
                $.foundry.showThrobber();
                $('.hasfocus').blur();
                if($('#websitemenu_dirty').val() != '' && $('#websitemenu_dirty').length > 0){
                    if(confirm('The current menu has been modified.  Save first?')){
                        $('#websitemenu_save').trigger('click');
                    }
                }
                var mnu = $(this);
                var rel = mnu.attr('rel');
                var mnu_parent = mnu.parent();
                var menubar = $('#websitemenu_menubar_select').val().split(':');
                $('#websitemenu_mainnav li').each(function(){
                    $(this).removeClass('chosen selected');
                });
                mnu_parent.addClass('chosen selected');
                $.post(
                    $.foundry.ajax_url,
                    {op:'getwebsitemenueditorhtml', val:rel, menubar:menubar[1]},
                    function(html){
                        var html_parts = html.split("||");
                        if(html_parts[0] == '') html_parts[0] = 'Select a menu from left or create a new menu item... ';
                        $('#websitemenu_editor').html(html_parts[0]);
                        $('.websitemenu_subnav').html(html_parts[1]);
                        $.foundry.hideThrobber();
                    }
                );
                return false;
            });

            $(document).delegate('.websitemenu_menuitem_clone', 'click', function(e){
                e.preventDefault();
                $('.hasfocus').blur();
                if($('#websitemenu_dirty').val() != '' && $('#websitemenu_dirty').length > 0){
                    if(confirm('The current menu has been modified.  Save first?')){
                        $('#websitemenu_save').trigger('click');
                    }
                }
                var mnu = $(this);
                var rel = 'clone>>'+mnu.attr('rel');
                var mnu_parent = mnu.parent();
                var menubar = $('#websitemenu_menubar_select').val().split(':');
                $('#websitemenu_mainnav li').each(function(){
                    $(this).removeClass('chosen selected');
                });
                mnu_parent.addClass('chosen selected');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'getwebsitemenueditorhtml', val:rel, menubar:menubar[1]},
                    function(html){
                        var html_parts = html.split("||");
                        if(html_parts[0] == '') html_parts[0] = 'Select a menu from left or create a new menu item... ';
                        $('#websitemenu_editor').html(html_parts[0]);
                        $('.websitemenu_subnav').html(html_parts[1]);
                        $.foundry.hideThrobber();
                    }
                );
                return false;
            });

            $(document).delegate('#websitemenu_menuitems_add', 'click', function(e){
                if(!$('#websitemenu_menuitems_add_list').is(':visible')){
                    $('#websitemenu_menuitems_add_list .chzn-container').hide();
                    $('#websitemenu_menuitems_add_list').slideDown(300, function(){
                        $('#websitemenu_menuitems_add_list .chzn-container').show();
                    });
                }else{
                    $('#websitemenu_menuitems_add_list .chzn-container').hide();
                    $('#websitemenu_menuitems_add_list').slideUp(300);
                }
            });

            $(document).delegate('#websitemenu_menuitems_add_close', 'click', function(e){
                $('#websitemenu_menuitems_add_list .chzn-container').hide();
                $('#websitemenu_menuitems_add_list').slideUp(300);
            });

            $(document).delegate('#websitemenu_menuitems_add_blank', 'click', function(e){
                e.preventDefault();
                $('.hasfocus').blur();
                if($('#websitemenu_dirty').val() != '' && $('#websitemenu_dirty').length > 0){
                    if(confirm('The current menu has been modified.  Save first?')){
                        $('#websitemenu_save').trigger('click');
                    }
                }

                $('#websitemenu_menuitems_add_list .chzn-container').hide();
                $('#websitemenu_menuitems_add_list').slideUp(300);

                var menubar = $('#websitemenu_menubar_select').val().split(':');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'getwebsitemenueditorhtml', val:'', menubar:menubar[1]},
                    function(html){
                        var html_parts = html.split("||");
                        if(html_parts[0] == '') html_parts[0] = 'Select a menu from left or create a new menu item... ';
                        $('#websitemenu_editor').html(html_parts[0]);
                        $('.websitemenu_subnav').html(html_parts[1]);
                        $.foundry.hideThrobber();
                    }
                );
                return false;
            });

            $(document).delegate('#websitemenu_menuitems_add_list .multiselect', 'change', function(){
                var selected = false;
                $('#websitemenu_menuitems_add_list .multiselect').each(function(){
                    if($(this).chosen().val() !== null) selected = true;
                });
                if(selected)
                    $('#websitemenu_menuitems_add_items').removeClass('gray').removeAttr('disabled');
                else
                    $('#websitemenu_menuitems_add_items').addClass('gray').attr('disabled', 'disabled');
            });

            $(document).delegate('#websitemenu_menuitems_add_items', 'click', function(e){
                e.preventDefault();
                var selected = [];
                $('#websitemenu_menuitems_add_list .multiselect').each(function(){
                    if($(this).chosen().val() !== null) selected.push($(this).attr('rel') + '=' + $(this).chosen().val());
                });
                if(selected.length == 0){
                    alert('Please select something to add to the menubar.');
                    return false;
                }else{
                    var id = $('#websitemenu_menubar_select').val().split(':') || null;
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'addwebsitemenucollection', val:selected, menu_bar_id:id[0]},
                        function(jsondata){
                            if(jsondata.success){
                                $('#websitemenu_menubar_select').trigger('change');
                            }
                            $.foundry.hideThrobber();
                        },
                        'json'
                    );
                }
            });

            var website_menuitem_delete = $(document).delegate('.websitemenu_menuitem_delete', 'click', function(e){
                e.preventDefault();
                $('#websitemenu_dirty').val('');
                var btn = $(this);
                var rel = parseInt(btn.attr('rel'));
                if(confirm('Delete the \''+btn.parent().find('span').text()+'\' menu? (Submenus will become orphaned)')){
                    if(rel > 0 && !isNaN(rel)){
                        $.foundry.showThrobber();
                        $.post(
                            $.foundry.ajax_url,
                            {op:'deletewebsitemenu', val:rel},
                            function(jsondata){
                                if(jsondata.success){
                                    $('#websitemenu_menubar_select').trigger('change');
                                }
                                $.foundry.hideThrobber();
                            },
                            'json'
                        );
                    }
                }
                return false;
            });

            $(document).delegate('#websitemenu_save', 'click', function(e){
                e.preventDefault();
                var menu_bar_id = $('#websitemenu_menubar_id').val();
                var menu_bar_id2= $('#websitemenu_menubar').val();
                var id          = $('#websitemenu_id').val();
                var title       = $('#websitemenu_title').val().replace(/(\|)/g, '');
                var parent_id   = $('#websitemenu_parent').val();
                var target      = $('#websitemenu_target').val();
                var wind        = $('#websitemenu_window').val();
                var active      = (($('#websitemenu_active').is(':checked')) ? 1 : 0);
                var alias       = $('#websitemenu_alias').val();
                var term_alias  = $('#websitemenu_termalias').val();
                var taxonomy    = $('#websitemenu_tax').val();

                var errmsg      = '';
                if(title == '') errmsg += '- The menu title.\n';
                if(alias > 0) { taxonomy = 0; term_alias = 0; }
                if(term_alias > 0) { taxonomy = 0; alias = term_alias; }
                if(taxonomy > 0) { alias = 0; term_alias = 0; }
                if(taxonomy == 0 && alias == 0 && term_alias == 0 && target == '') errmsg += '- The alias, taxonomy, or direct link.\n';
                if(errmsg != '') {
                    alert('The following entries are required: \n\n'+errmsg);
                }else{
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'savewebsitemenu', val:id, menu_bar_id:menu_bar_id, new_menu_bar_id:menu_bar_id2, parent_id:parent_id, title:title, alias:alias, taxonomy_id:taxonomy, target:target, wind:wind, custom:0, active:active},
                        function(jsondata){
                            if(jsondata.success){
                                $('#websitemenu_dirty').val('');
                                alert('The menu changes have been saved.');
                                $('#websitemenu_menubar_select').trigger('change');
                            }else{
                                if(jsondata.rtndata == 'undefined') jsondata.rtndata = '';
                                alert('There was a problem saving the menu changes.  '+jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        "json"
                    );
                }
                return false;
            });

            $(document).delegate('#websitemenu_title, #websitemenu_table, #websitemenu_target, #websitemenu_targettype, #websitemenu_restricted, #websitemenu_alias, #websitemenu_active', 'blur', function(){
                $(this).removeClass('hasfocus');
            });

            $(document).delegate('#websitemenu_title, #websitemenu_table, #websitemenu_target, #websitemenu_targettype, #websitemenu_restricted, #websitemenu_alias, #websitemenu_active', 'focus', function(){
                $(this).addClass('hasfocus');
            });

            $(document).delegate('#websitemenu_editor input, #websitemenu_editor select', 'change', function(){
                $('#websitemenu_dirty').val('1');
            });

            $(document).delegate('#websitemenu_active', 'change', function(){
                if(!$(this).is(':checked'))
                    $('#websitemenu_restricted').attr('disabled', 'disabled');
                else
                    $('#websitemenu_restricted').removeAttr('disabled');
            });

            $(document).delegate('#websitemenu_restricted', 'change', function(){
                $('#websitemenu_dirty').val('1');
            });

            $(document).delegate('.websitemenu_delete', 'click', function(e){
                e.preventDefault();
                var rel = $(this).attr('rel');
                $('.websitemenu_menuitem_delete[rel="'+rel+'"]').trigger('click');
            });

            $(document).delegate('#websitemenu_code_gen', 'click', function(e){
                e.preventDefault();
                var title = $('#websitemenu_title').val();
                if(title != ''){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'generatewebsitemenucode', val:title},
                        function(jsondata){
                            if(jsondata.success){
                                $('#websitemenu_code').val(jsondata.rtndata);
                            }else{
                                alert("The menu key cannot be generated at this time.  Perhaps adjust the title and try again.");
                            }
                            $.foundry.hideThrobber();
                        },
                        "json"
                    );
                }
                return false;
            });

            // Admin Menus

            var adminmenu_ns = {
                                    listType: 'ul',
                                    disableNesting: 'no-nest',
                                    forcePlaceholderSize: true,
                                    handle: 'span',
                                    items: 'li',
                                    maxLevels: 5,
                                    opacity: .6,
                                    placeholder: 'nestedsortable-placeholder',
                                    revert: 250,
                                    tabSize: 5,
                                    tolerance: 'pointer',
                                    toleranceElement: '> span',
                                    noJumpFix: 1,
                                    update: function(){
                                        updateAMB();
                                    }
                                };

            function updateAMB(){
                // update the ul classes
                var elem;
                $('#adminmenu_mainnav ul, #adminmenu_mainnav li').each(function(){
                    elem = $(this);
                    if(elem.prop('tagName').toLowerCase() == 'ul'){
                        if(!elem.hasClass('adminmenu_subnav')) elem.addClass('adminmenu_subnav');
                        if(!elem.hasClass('ui-sortable')) elem.addClass('ui-sortable');
                    }else if(elem.prop('tagName').toLowerCase() == 'li'){
                        elem.removeClass('adminmenu_subnav').removeClass('ui-sortable');
                    }
                });

                var ser = $('#adminmenu_mainnav').nestedSortable('serialize', {startingDepth: 0}).replace(/\&/g, '|');
                $.foundry.showThrobber();
                $.ajax({
                    type: "POST",
                    url: $.foundry.ajax_url,
                    data: "op=updateadminmenulayout&val="+ser,
                    success: function(){
                        $.foundry.hideThrobber();
                    }
                });
            }

            $('#adminmenu_menuitems_list ul').nestedSortable(adminmenu_ns);

            $(document).delegate('#adminmenu_menubar_select', 'change', function(){
                var elem = $(this);
                var key = elem.val().split(':');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'refreshadminmenulist', val:key[1]},
                    function(jsondata){
                        if(jsondata.success){
                            $('#adminmenu_mainnav').remove();
                            $('#adminmenu_menuitems_list').append(jsondata.rtndata);
                            $('#adminmenu_menuitems_list ul').nestedSortable(adminmenu_ns);

                            if(key[0] > 0){
                                $('#adminmenu_menubar_edit').removeClass('hidden');
                                $('#adminmenu_menubar_delete').removeClass('hidden');
                                $('#adminmenu_menuitems_add').removeClass('hidden');
                                if(jsondata.rtndata != '')
                                    $('#adminmenu_menuitems_list .adminmenu_title').text("Menu items in the '"+elem.find('option:selected').text()+"' menu:");
                                else
                                    $('#adminmenu_menuitems_list .adminmenu_title').text("No menu items are in the "+elem.find('option:selected').text()+" admin menubar.  Click 'Add Menu(s)' to add one.");
                                $('#adminmenu_menubar_id').val(key[0]);
                            }else{
                                $('#adminmenu_menubar_edit').addClass('hidden');
                                $('#adminmenu_menubar_delete').addClass('hidden');
                                $('#adminmenu_menuitems_add').addClass('hidden');
                                if(jsondata.rtndata != '')
                                    $('#adminmenu_menuitems_list .adminmenu_title').text("The following menu items are disassociated from any menubars:");
                                else
                                    $('#adminmenu_menuitems_list .adminmenu_title').text("Great! No menus are currently orphaned.");
                                $('#adminmenu_menubar_id').val('0');
                            }
                        }else{
                            alert('There was a problem retrieving the menu items.');
                        }
                        $.foundry.hideThrobber();
                    },
                    "json"
                );

                $('#adminmenu_editor').html('Select a menu from left or create a new menu item...');
            });

            $(document).delegate('#adminmenu_menubar_edit, #adminmenu_menubar_create', 'click', function(e){
                e.preventDefault();
                if(!$('#adminmenu_menubar_editor').is(':visible')){
                    if($(this).attr('id') == 'adminmenu_menubar_edit'){
                        var id = $('#adminmenu_menubar_select').val().split(':') || null;
                        $.foundry.showThrobber();
                        $.post(
                            $.foundry.ajax_url,
                            {op:'getadminmenubarinfo', val:id[0]},
                            function(jsondata){
                                if(jsondata.success){
                                    $('#adminmenu_menubar_editor_id').val(id[0]);
                                    $('#adminmenu_menubar_title').val(jsondata.rtndata.title);
                                    $('#adminmenu_menubar_descr').val(jsondata.rtndata.descr);
                                    if(jsondata.rtndata.parent_id == 1)
                                        $('#adminmenu_menubar_slot').attr('checked', 'checked');
                                    else
                                        $('#adminmenu_menubar_slot').removeAttr('checked');
                                    $('#adminmenu_menubar_editor h3').text('Edit Menubar');
                                    $('#adminmenu_menubar_select').attr('disabled', 'disabled');
                                    $('#adminmenu_menubar_editor').slideDown(400);
                                }else{
                                    alert('The menubar info could not be retrieved!');
                                }
                                $.foundry.hideThrobber();
                            },
                            "json"
                        );
                    }else{
                        $('#adminmenu_menubar_editor_id').val('0');
                        $('#adminmenu_menubar_title').val('');
                        $('#adminmenu_menubar_descr').val('');
                        $('#adminmenu_menubar_editor h3').text('Create New Menubar');
                        $('#adminmenu_menubar_slot').removeAttr('checked');
                        $('#adminmenu_menubar_select').attr('disabled', 'disabled');
                        $('#adminmenu_menubar_editor').slideDown(400);
                    }
                }else{
                    $('#adminmenu_menubar_cancel').trigger('click');
                }
            });

            $(document).delegate('#adminmenu_menubar_cancel', 'click', function(e){
                e.preventDefault();
                $('#adminmenu_menubar_editor').slideUp(400);
                $('#adminmenu_menubar_select').removeAttr('disabled');
            });

            $(document).delegate('#adminmenu_menubar_save', 'click', function(e){
                e.preventDefault();
                var id = $('#adminmenu_menubar_editor_id').val();
                var title = $('#adminmenu_menubar_title').val();
                var descr = $('#adminmenu_menubar_descr').val();
                var slot = (($('#adminmenu_menubar_slot').is(':checked')) ? $('#adminmenu_menubar_slot').val() : 0);
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'saveadminmenubarinfo', val:id, title:title, descr:descr, parent_id:slot},
                    function(jsondata){
                        if(jsondata.success){
                            if(id > 0){
                                $('#adminmenu_menubar_select option:selected').text(title);
                                alert('The menubar has been updated.');
                            }else{
                                var val = jsondata.rtndata.id+':'+jsondata.rtndata.key;
                                $('#adminmenu_menubar_select').append('<option value="'+val+'">'+title+'</option>');
                                $('#adminmenu_menubar_select').val('0:-').trigger('change');
                                alert('The menubar has been created.');
                            }
                            $('#adminmenu_menubar_cancel').trigger('click');
                        }else{
                            alert(jsondata.rtndata);
                        }
                        $.foundry.hideThrobber();
                    },
                    "json"
                );
            });

            $(document).delegate('#adminmenu_menubar_delete', 'click', function(e){
                e.preventDefault();
                var id = $('#adminmenu_menubar_select').val().split(':');
                if(confirm('Delete this menubar (all contained menu items will become orphaned)?')){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'deleteadminmenubar', val:id[0]},
                        function(jsondata){
                            if(jsondata.success){
                                $('#adminmenu_menubar_select option:selected').remove();
                                $('#adminmenu_menubar_select').val('0:-').trigger('change');
                            }else{
                                alert(jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        "json"
                    );
                }
            });

            $(document).delegate('.adminmenu_menuitem span', 'click', function(e){
                e.preventDefault();
                $.foundry.showThrobber();
                $('.hasfocus').blur();
                if($('#adminmenu_dirty').val() != '' && $('#adminmenu_dirty').length > 0){
                    if(confirm('The current menu has been modified.  Save first?')){
                        $('#adminmenu_save').trigger('click');
                    }
                }
                var mnu = $(this);
                var rel = mnu.attr('rel');
                var mnu_parent = mnu.parent();
                var menubar = $('#adminmenu_menubar_select').val().split(':');
                $('#adminmenu_mainnav li').each(function(){
                    $(this).removeClass('chosen selected');
                });
                mnu_parent.addClass('chosen selected');
                $.post(
                    $.foundry.ajax_url,
                    {op:'getadminmenueditorhtml', val:rel, menubar:menubar[1]},
                    function(html){
                        var html_parts = html.split("||");
                        if(html_parts[0] == '') html_parts[0] = 'Select a menu from left or create a new menu item... ';
                        $('#adminmenu_editor').html(html_parts[0]);
                        $('.adminmenu_subnav').html(html_parts[1]);
                        mnu.addClass('aw_menuitem_active');
                        $.foundry.hideThrobber();
                    }
                );
                return false;
            });

            $(document).delegate('.adminmenu_menuitem_clone', 'click', function(e){
                e.preventDefault();
                $('.hasfocus').blur();
                if($('#adminmenu_dirty').val() != '' && $('#adminmenu_dirty').length > 0){
                    if(confirm('The current menu has been modified.  Save first?')){
                        $('#adminmenu_save').trigger('click');
                    }
                }
                var mnu = $(this);
                var rel = 'clone>>'+mnu.attr('rel');
                var mnu_parent = mnu.parent();
                var menubar = $('#adminmenu_menubar_select').val().split(':');
                $('#adminmenu_mainnav li').each(function(){
                    $(this).removeClass('chosen selected');
                });
                mnu_parent.addClass('chosen selected');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'getadminmenueditorhtml', val:rel, menubar:menubar[1]},
                    function(html){
                        var html_parts = html.split("||");
                        if(html_parts[0] == '') html_parts[0] = 'Select a menu from left or create a new menu item... ';
                        $('#adminmenu_editor').html(html_parts[0]);
                        $('.adminmenu_subnav').html(html_parts[1]);
                        $.foundry.hideThrobber();
                    }
                );
                return false;
            });

            $(document).delegate('#adminmenu_menuitems_add', 'click', function(e){
                e.preventDefault();
                $('.hasfocus').blur();
                if($('#adminmenu_dirty').val() != '' && $('#adminmenu_dirty').length > 0){
                    if(confirm('The current menu has been modified.  Save first?')){
                        $('#adminmenu_save').trigger('click');
                    }
                }
                var menubar = $('#adminmenu_menubar_select').val().split(':');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'getadminmenueditorhtml', val:'', menubar:menubar[1]},
                    function(html){
                        var html_parts = html.split("||");
                        if(html_parts[0] == '') html_parts[0] = 'Select a menu from left or create a new menu item... ';
                        $('#adminmenu_editor').html(html_parts[0]);
                        $('.adminmenu_subnav').html(html_parts[1]);
                        $.foundry.hideThrobber();
                    }
                );
                return false;
            });

            var admin_menuitem_delete = $(document).delegate('.adminmenu_menuitem_delete', 'click', function(e){
                e.preventDefault();
                $('#adminmenu_dirty').val('');
                var btn = $(this);
                var rel = parseInt(btn.attr('rel'));
                if(confirm('Delete the \''+btn.parent().find('span').text()+'\' menu? (Submenus will become orphaned)')){
                    if(rel > 0 && !isNaN(rel)){
                        $.foundry.showThrobber();
                        $.post(
                            $.foundry.ajax_url,
                            {op:'deleteadminmenu', val:rel},
                            function(jsondata){
                                if(jsondata.success){
                                    $('#adminmenu_menubar_select').trigger('change');
                                }
                                $.foundry.hideThrobber();
                            },
                            'json'
                        );
                    }
                }
                return false;
            });

            $(document).delegate('#adminmenu_save', 'click', function(e){
                e.preventDefault();
                var menu_bar_id = $('#adminmenu_menubar_id').val();
                var menu_bar_id2= $('#adminmenu_menubar').val();
                var id          = $('#adminmenu_id').val();
                var title       = $('#adminmenu_title').val().replace(/(\|)/g, '');
                var parent_id   = $('#adminmenu_parent').val();
                var alias       = $('#adminmenu_alias').val();
                var targettype  = $('#adminmenu_targettype').val();
                var datatype    = $('#adminmenu_datatype').val();
                var tax_id      = $('#adminmenu_taxonomy').val();
                var target      = $('#adminmenu_target').val().replace(/([^a-z0-9/\\_\-])/ig, '');
                var active      = (($('#adminmenu_active').is(':checked')) ? 1 : 0);
                var restr       = (($('#adminmenu_restricted').is(':checked')) ? 1 : 0);

                var errmsg      = '';
                if(title == '') errmsg += '- The menu title.\n';
                if(targettype == '') errmsg += '- The menu target type.\n';
                if(datatype == '') errmsg += '- The data type.\n';
                if(alias == 0 && target == '') errmsg += '- Choose or enter the alias path.\n';
                if(alias > 0) $('#adminmenu_target').val('');
                if(targettype == 'terms' && tax_id == 0) errmsg += '- The taxonomy.\n';
                if(errmsg != '') {
                    alert('The following entries are required: \n\n'+errmsg);
                }else{
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'saveadminmenu', val:id, menu_bar_id:menu_bar_id, new_menu_bar_id:menu_bar_id2, parent_id:parent_id, title:title, alias:alias, target:target, targettype:targettype, datatype:datatype, taxonomy_id:tax_id, restricted:restr, custom:0, active:active},
                        function(jsondata){
                            if(jsondata.success){
                                $('#adminmenu_dirty').val('');
                                alert('The menu changes have been saved.');
                                $('#adminmenu_menubar_select').trigger('change');
                            }else{
                                if(jsondata.rtndata == 'undefined') jsondata.rtndata = '';
                                alert('There was a problem saving the menu changes.  '+jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        "json"
                    );
                }
                return false;
            });

            $(document).delegate('#adminmenu_title, #adminmenu_table, #adminmenu_target, #adminmenu_targettype, #adminmenu_restricted, #adminmenu_alias, #adminmenu_active', 'blur', function(){
                $(this).removeClass('hasfocus');
            });

            $(document).delegate('#adminmenu_title, #adminmenu_table, #adminmenu_target, #adminmenu_targettype, #adminmenu_restricted, #adminmenu_alias, #adminmenu_active', 'focus', function(){
                $(this).addClass('hasfocus');
            });

            $(document).delegate('#adminmenu_editor input, #adminmenu_editor select', 'change', function(){
                $('#adminmenu_dirty').val('1');
            });

            $(document).delegate('#adminmenu_active', 'change', function(){
                if(!$(this).is(':checked'))
                    $('#adminmenu_restricted').attr('disabled', 'disabled');
                else
                    $('#adminmenu_restricted').removeAttr('disabled');
            });

            $(document).delegate('#adminmenu_restricted', 'change', function(){
                $('#adminmenu_dirty').val('1');
            });

            $(document).delegate('.adminmenu_delete', 'click', function(e){
                e.preventDefault();
                var rel = $(this).attr('rel');
                $('.adminmenu_menuitem_delete[rel="'+rel+'"]').trigger('click');
            });

            $(document).delegate('#adminmenu_generate_target', 'click', function(){
                var data_type = $('#adminmenu_datatype option:selected').text().toLowerCase();
                var targettype = $('#adminmenu_targettype').val();
                if(data_type != '' && targettype != ''){
                    if(targettype == 'terms') targettype += '/list';
                    $('#adminmenu_target').val(data_type+'/'+targettype);
                }
            });

            $(document).delegate('#adminmenu_targettype', 'change', function(){
                if($(this).val() == 'terms')
                    $('#adminmenu_taxdiv').removeClass('hidden');
                else
                    $('#adminmenu_taxdiv').addClass('hidden');
            });

            // Taxonomies

            $(document).delegate('.tax_edit', 'click', function(e){
                e.preventDefault();
                var row = $(this).closest('div.table-body');
                $('#tax_edit_id').val(row.find('.tax_id').val());
                $('#tax_edit_name').val(row.find('.tax_name').val());
                $('#tax_edit_code').val(row.find('.tax_code').val());
                $('#tax_edit_parent_id').val(row.find('.tax_parent_id').val());
                $('#tax_edit_data_alias').val(row.find('.tax_data_alias_id').val());
                $('#tax_edit_alias').val('');
                $('#tax_edit_data_type').val('');
                $('#tax_edit_type').val(row.find('.tax_type').val());
                row.addClass('row-active').after($('#tax_edit_row'));
                if(row.find('.tax-term-menus').hasClass('has-menus'))
                    $('#tax_edit_create_menu').addClass('hidden');
                else
                    $('#tax_edit_create_menu').removeClass('hidden');
                $('#tax_edit_row').removeClass('hidden').slideDown();
                $('#tax_edit_name').focus();
            });

            $(document).delegate('#tax_new', 'click', function(e){
                e.preventDefault();
                $('#tax_edit_id').val('0');
                $('#tax_edit_name').val('');
                $('#tax_edit_code').val('');
                $('#tax_edit_parent_id').val('');
                $('#tax_edit_data_alias').val('');
                $('#tax_edit_alias').val('');
                $('#tax_edit_data_type').val('');
                $('#tax_edit_type').val('category');
                $('#tax_list .table-head').after($('#tax_edit_row'));
                $('#tax_edit_create_menu').removeClass('hidden');
                $('.tax-row').removeClass('row-active');
                $('#tax_edit_row').removeClass('hidden').slideDown();
                $('#tax_edit_name').focus();
            });

            $(document).delegate('#tax_edit_save', 'click', function(e){
                e.preventDefault();
                var id = $('#tax_edit_id').val();
                var name = $('#tax_edit_name').val().trim();
                var code = $('#tax_edit_code').val().trim().toLowerCase();
                var parent_id = $('#tax_edit_parent_id').val();
                var data_alias = $('#tax_edit_data_alias').val();
                var alias = $('#tax_edit_alias').val().trim();
                var data_type = $('#tax_edit_data_type').val();
                var type = $('#tax_edit_type').val();
                var create_menu = false;

                var err = '';
                if(name == '') err += "- The name is required\n";
                if(parent_id != '' && parent_id == id) err += "- The taxonomy and its parent must be different\n";
                if(data_alias == '' && alias == '') err += "- An existing or new data alias is required\n";
                if(type == '') err += "- The type is required\n";
                if(err != ''){
                    alert("The following items need attention:\n\n"+err);
                    return false;
                }
                if($('#tax_create_menu').is(':checked')) create_menu = true;

                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'savetaxonomy', val:id, name:name, code:code, parent_id:parent_id, data_alias:data_alias, alias:alias, type:type, create_menu:create_menu},
                    function(jsondata){
                        if(jsondata.success){
                            $('#tax_list').html(jsondata.rtndata);
                        }else{
                            alert('The changes to the taxonomy could not be saved.');
                        }
                        $.foundry.hideThrobber();
                    },
                    'json');
            });

            $(document).delegate('#tax_edit_name', 'keyup', function(){
                $('#tax_edit_code').val($(this).val().toLowerCase().replace(/[^\w\s]/gi, '').replace(/[\s]/gi, '-'));
            });

            $(document).delegate('#tax_edit_close', 'click', function(e){
                e.preventDefault();
                var row = $('#tax_edit_row');
                row.slideUp(function(){ row.addClass('hidden'); $('.tax-row').removeClass('row-active'); });
            });

            $(document).delegate('.tax-enable, .tax-disable', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var row = elem.closest('div.table-body');
                var id = row.find('.tax_id').val();
                var state = ((elem.hasClass('tax-enable')) ? 1 : 0);
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'changetaxonomystate', val:id, state:state},
                    function(jsondata){
                        if(jsondata.success){
                            if(state == 1){
                                elem.addClass('tax-disable fa-pause').removeClass('tax-enable fa-play green').attr('title', 'Deactivate');
                                elem.siblings('.tax-state').text('Active');
                            }else{
                                elem.addClass('tax-enable fa-play green').removeClass('tax-disable fa-pause').attr('title', 'Activate');
                                elem.siblings('.tax-state').text('Inactive');
                            }
                        }else{
                            alert('The taxonomy state could not be changed.');
                        }
                        $.foundry.hideThrobber();
                    },
                    'json');
            });

            $(document).delegate('.tax-delete', 'click', function(e){
                e.preventDefault();
                var row = $(this).closest('div.table-body');
                var id = row.find('.tax_id').val();
                if(confirm('Are you sure you want to delete this taxonomy?')){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'deletetaxonomy', val:id},
                        function(jsondata){
                            if(jsondata.success){
                                $('#tax_list').html(jsondata.rtndata);
                            }else{
                                alert('The taxonomy could not be deleted.');
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
            });

            // Plugins

            $(document).delegate('#plugin_usage_zones', 'click', function(e){
                e.preventDefault();
                $.foundry.showPopupBalloon($(this), '#admSettingsPopupBalloon', 'getusagezoneslegend', 52, 132, 180);
            });

            $(document).delegate('#plugin_bulkcheck, #plugin_fw_bulkcheck, #plugin_ext_bulkcheck', 'click', function(){
                var state = $(this).is(':checked');
                $('.plugin_checks').each(function(){
                    if($(this).is(':visible')) $(this).attr('checked', state);
                });
            });

            $('div').delegate('.plugin_act', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                if(elem.hasClass('blocked')){
                    alert('This plugin cannot be activated!  Click on the \'More\' link to learn why.');
                }else{
                    var p_id = elem.attr('rel');
                    var tostate = 0;
                    var process = true;
                    if(elem.text().toLowerCase() == 'activate') tostate = 1;
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op: 'getplugindependants', val:p_id},
                        function(data){
                            data = parseInt(data);
                            if(data > 0 && tostate == 0)
                                if(!confirm('This plugin has '+data+' dependant(s)/child(ren).  Deactivating it will cause all dependants/children to be deactivated as well.'))
                                    process = false;

                            if(process){
                                // either plugin is being activated or user accepted the warning above...
                                $.post(
                                    $.foundry.ajax_url,
                                    {op:'setpluginactivestate', val:tostate, id:p_id},
                                    function(jsondata){
                                        if(jsondata.success){
                                            var p_row = elem.closest('.plugin_row');
                                            if(jsondata.rtndata.result == 1){
                                                // activated
                                                if(elem.hasClass('proxy')){
                                                    $('#plugin_actions_'+p_id).find('.plugin_act').text('Deactivate');
                                                    p_row.find('.plugin_actions').find('.plugin_act').removeClass('blocked').text('Activate');
                                                }else{
                                                    elem.text('Deactivate');
                                                }
                                                $('#plugin_name_'+p_id).removeClass('notactive');
                                            }else{
                                                // deactivated
                                                elem.text('Activate');
                                                $('#plugin_name_'+p_id).addClass('notactive');
                                            }
                                            $.post(
                                                $.foundry.ajax_url,
                                                {op:'reloadpluginstab', val:'installed'},
                                                function(html){
                                                    if(html != '') $('#plugin_installed').html(html);
                                                },
                                                'html'
                                            );
                                        } else if(jsondata.rtndata != null) {
                                            alert(jsondata.rtndata);
                                        }
                                        $.foundry.hideThrobber();
                                    },
                                    'json'
                                );
                            }else{
                                $.foundry.hideThrobber();
                            }
                        },
                        'text'
                    );
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.plugin_del', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var p_row = elem.closest('.plugin_row');
                var p_id = elem.attr('rel');
                var p_name = $('#plugin_title_'+p_id).val();
                if(confirm("Do you want to delete the plugin '" + p_name + "'?")){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'deleteplugin', val:'', id:p_id},
                        function(jsondata){
                            var lead = '<br/>&nbsp;&nbsp;&bull;&nbsp;';
                            if(jsondata.success){
                                $('#plugin_problem').append(jsondata.rtndata.row);
                                if(jsondata.rtndata.setting != '') $('#plugin_setting_'+jsondata.rtndata.setting).remove();
                                p_row.addClass('theme_bkgcolor1').fadeOut(1000, function(){ $(this).remove() });
                            }else{
                                $('#issue-plugins').removeClass('disabled');
                                $('#issue-plugins').append(lead + jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.plugin_undel', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var p_row = elem.closest('.plugin_row');
                var p_id = elem.attr('rel');
                var p_name = $('#plugin_title_'+p_id).val();
                if(confirm("Do you want to undelete the plugin '" + p_name + "'?")){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'undeleteplugin', val:'', id:p_id},
                        function(jsondata){
                            if(jsondata.success){
                                $('#plugin_installed').append(jsondata.rtndata.row);
                                if(jsondata.rtndata.setting != '') $('#plugin_settings').append(jsondata.rtndata.setting);
                                p_row.addClass('theme_bkgcolor1').fadeOut(1000, function(){ $(this).remove(); alert('The plugin has been restored.') });
                            }else{
                                alert('The plugin could not be restored.');
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.plugin_scrap', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var p_row = elem.closest('.plugin_row');
                var p_id = elem.attr('rel');
                var p_name = $('#plugin_title_'+p_id).val();
                if(confirm("Do you want to permanently scrap the plugin '" + p_name + "' (recorded as ID #" + p_id + ")?")){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'scrapplugin', val:'', id:p_id},
                        function(jsondata){
                            var lead = '<br/>&nbsp;&nbsp;&bull;&nbsp;';
                            if(jsondata.success){
                                p_row.effect("pulsate", {times: 2}, 200, function(){p_row.remove();});
                            }else{
                                $('#issue-plugins').removeClass('disabled');
                                $('#issue-plugins').append(lead + jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('#plugin_fix_submit', 'click', function(e){
                // part 2: try to repair plugin.info file
                e.preventDefault();
                var p_row = $(this).closest('.plugin_info');
                var p_id = p_row.find('.plugin_repair_plugin_id').val();
                if(!e.isPropagationStopped()){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'repairplugincfg', val:p_id},
                        function(jsondata){
                            if(jsondata.success){
                                $('#plugin_installed').append(jsondata.rtndata.row);
                                if(jsondata.rtndata.setting != '') $('#plugin_settings').append(jsondata.rtndata.setting);
                                p_row.hide(400);
                                p_row.remove();
                                $('#issues-plugins').html(jsondata.rtndata.plugins_issues);
                                $('#issues').html(jsondata.rtndata.settings_issues);
                                if($('#issues-plugins').text == '') $('#issues-plugins').hide();
                                if($('#issues').text == '') $('#issues').hide();
                                alert('The plugin.info file has been repaired.');
                            }else{
                                alert('Unable to repair the plugin.info file because:\n\n'+jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        "json");
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.plugin_more', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var p_row = elem.closest('.plugin_row');
                var is_prob = ((elem.hasClass('prob')) ? 'prob' : '');
                var p_id = p_row.find('.plugin_id').val();
                if(elem.text() == 'More'){
                    // show
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'getplugindata', val:is_prob, id:p_id},
                        function(jsondata){
                            if(jsondata.success){
                                p_row.find('.plugin_info').html(jsondata.rtndata).slideDown(500);
                                elem.text('Less');
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }else{
                    // hide
                    p_row.find('.plugin_info').slideUp(500);
                    elem.text('More');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('.plugin_datamod', 'blur', function(){
                var elem = $(this);
                var rel = elem.attr('rel');
                var val = elem.val();
                if(val != ''){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'updateplugindata', val:val, rel:rel},
                        function(jsondata){
                            if(jsondata.success){
                                alert('The setting was updated.');
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }else{
                    alert('The value cannot be blank.  Nothing was saved.');
                }
                return false;
            });

            $('div').delegate('.plugin_settings_link', 'click', function(e){
                e.preventDefault();
                var settingsfunc = $(this).attr('rel');
                var settingsfunc_parts = settingsfunc.split('|');
                if(settingsfunc != ''){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'runpluginsettingsfunc', val:settingsfunc},
                        function(jsondata){
                            if(jsondata.contents != ''){
                                // build and show dialog box & save button
                                var buttons = '<p class="clearfix" style="float: right"><input type="button" id="plugin_settings_dialog_save" rel="'+settingsfunc+'" value="Save"/>&nbsp;<input type="button" id="plugin_settings_dialog_cancel" rel="'+settingsfunc+'" value="Cancel"/></p>';
                                var contents = '<form action="" method="POST" id="pluginsettingsform">' + jsondata.contents + buttons + '</form>';
                                $('#pluginsettingsdialog').html(contents).dialog('open').dialog('option', 'title', jsondata.title + ' Settings');
                                $('#pluginsettingsdialog').attr('rel', settingsfunc);
                                $('#pluginsettingsdialog').on('dialogbeforeclose', function(e, ui){
                                    $.ajax({
                                        type: 'POST',
                                        url: $.foundry.ajax_url,
                                        data: {op:'runpluginsettingsaction', val:settingsfunc, action:'2'},
                                        dataType: 'json',
                                        async: false,
                                        success: function(jsondata){
                                            $.foundry.hideThrobber();
                                            if(jsondata.success){
                                                // respond to close button (x)
                                                if(jsondata.message != '' && jsondata.message != null) alert(jsondata.message);
                                                if(jsondata.closedialog == false) {
                                                    return false;
                                                }
                                            }else{
                                                alert("There was a problem handling the closure of the settings function '"+settingsfunc+"' in this plugin.");
                                            }
                                        }
                                    });
                                });
                            }else{
                                alert("There was a problem calling the settings function '"+jsondata.func+"' in this plugin.");
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('#plugin_settings_dialog_save', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var settingsfunc = elem.attr('rel');
                var settingsdata = $('#pluginsettingsform').serialize();
                if(settingsfunc != '' && !e.isPropagationStopped()){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'runpluginsettingsaction', val:settingsfunc, action:'1', data:settingsdata},
                        function(jsondata){
                            if(jsondata.success){
                                // respond to save button and close dialog
                                if(jsondata.message != '' && jsondata.message != null) alert(jsondata.message);
                                if(jsondata.closedialog != false) {
                                    $('#pluginsettingsdialog').dialog('close');
                                    elem.attr('rel', '');
                                }
                            }else{
                                alert("There was a problem handling the saving/closure of the settings function '"+jsondata.func+"' in this plugin.");
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                e.stopPropagation();
                return false;
            });

            $('div').delegate('#plugin_settings_dialog_cancel', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                $('#pluginsettingsdialog').dialog('close');
                elem.attr('rel', '');
                return false;
            });

            $('div').delegate('.plugin_help_link', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var helpfile = elem.attr('data-help-file');
                var p_row = elem.closest('.plugin_row');
                var p_id = p_row.find('.plugin_id').val();
                var p_name = p_row.find('.plugin_title').val();
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'getsettingshelpfile', val:helpfile},
                    function(html){
                        $('.dialogpanel.pluginhelp').html(html);
                        $('.dialogpanel.pluginhelp').dialog('close').dialog('open');
                        $('#ui-dialog-title-genpanel3').text('Plugin Help - '+p_name);
                        $.foundry.hideThrobber();
                    },
                    'html'
                );
                e.stopPropagation();
                return false;
            });

            $(document).delegate('#plugin_bulkact, #plugin_fw_bulkact, #plugin_ext_bulkact', 'click', function(e){
                e.preventDefault();
                var opt;
                if($(this).attr('id') == 'plugin_bulkact'){
                    opt = $('#plugin_bulkopt').val();
                }else if($(this).attr('id') == 'plugin_fw_bulkact'){
                    opt = $('#plugin_fw_bulkopt').val();
                }else if($(this).attr('id') == 'plugin_ext_bulkact'){
                    opt = $('#plugin_ext_bulkopt').val();
                }
                if(opt != '' && opt != undefined){
                    $('.plugin_checks').each(function(){
                        if($(this).is(':checked') && $(this).is(':visible')){
                            if(opt == 'deactivate'){
                                $(this).siblings('.plugin_actions').find('.plugin_act').trigger('click');
                            }else if(opt == 'delete'){
                                $(this).siblings('.plugin_actions').find('.plugin_del').trigger('click');
                            }
                        }
                    });
                }
            });

            $(document).delegate('#plugin_zonesel_front, #plugin_zonesel_admin, #plugin_zonesel_both', 'click', function(){
                var elem = $(this);
                var zone = elem.attr('id').substr(15);
                var is_already_selected = elem.hasClass('active');
                $('.plugin_midside').each(function(){
                    if($(this).hasClass('zone_'+zone) || is_already_selected)
                        $(this).parent().show();
                    else
                        $(this).parent().hide();
                });
                $('.plugin_zonesel').css('border', 'none').removeClass('active');
                if(!is_already_selected) elem.css('border', '1px dotted black').addClass('active');
            });

            $('div').delegate('.plugin_search_submit', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var searchtext = elem.parent().find('.plugin_search').val();
                var filterstate = elem.parent().find('.plugin_filter_state').val();
                var filterzone = elem.parent().find('.plugin_filter_zone').val();
                var listid = elem.attr('rel');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'reloadpluginstab', val:(listid == 'plugin_installed' ? 'find' : 'findext'), search:searchtext, filterstate:filterstate, filterzone:filterzone},
                    function(html){
                        if(html != '') $('#'+listid).html(html);
                        $.foundry.hideThrobber();
                    },
                    'html'
                );
                return false;
            });

            $('div').delegate('.plugin_filter_state, .plugin_filter_zone', 'change', function(e){
                e.preventDefault();
                var elem = $(this);
                var searchtext = elem.parent().find('.plugin_search').val();
                var filterstate = elem.parent().find('.plugin_filter_state').val();
                var filterzone = elem.parent().find('.plugin_filter_zone').val();
                var listid = elem.attr('rel');
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'reloadpluginstab', val:(listid == 'plugin_installed' ? 'find' : 'findext'), search:searchtext, filterstate:filterstate, filterzone:filterzone},
                    function(html){
                        if(html != '') $('#'+listid).html(html);
                        $.foundry.hideThrobber();
                    },
                    'html'
                );
                return false;
            });

            // Users

            $('div').delegate('.user_act', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var u_row = elem.closest('.user_row');
                var u_id = u_row.find('.user_id').val();
                var u_adms = parseInt($('#users_admins').val());
                var tostate = 0;
                if(elem.text() == 'Activate') tostate = 1;
                if(u_adms < 2 && tostate == 0){
                    alert('This user is the last active admin and cannot be deactivated.');
                }else{
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'setuseractivestate', val:tostate, id:u_id},
                        function(jsondata){
                            if(jsondata.success){
                                if(jsondata.rtndata == 1){
                                    // activated
                                    elem.text('Deactivate');
                                    u_row.find('.user_name').removeClass('notactive');
                                    u_adms++;
                                }else{
                                    // deactivated
                                    elem.text('Activate');
                                    u_row.find('.user_name').addClass('notactive');
                                    u_adms--;
                                }
                                $('#users_admins').val(u_adms);
                            }
                            $.foundry.hideThrobber();
                        },
                        'json');
                }
                return false;
            });

            $('div').delegate('.user_del', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var u_row = elem.closest('.user_row');
                var u_id = u_row.find('.user_id').val();
                var u_name = u_row.find('.user_name').text();
                var u_cur = u_row.find('.user_cur').val();
                var u_count = parseInt($('#users_count').val());
                if(u_count < 2 && u_id > 0){
                    alert('This user is the last user account and cannot be deleted.');
                    return false;
                }
                if(u_cur != ''){
                    alert('You cannot delete your own account.');
                    return false;
                }
                if(confirm("Delete the user '" + u_name + "'?\n\nThis cannot be undone.")){
                    if(u_id > 0){
                        $.foundry.showThrobber();
                        $.post(
                            $.foundry.ajax_url,
                            {op:'deleteuserdata', val:'', id:u_id},
                            function(jsondata){
                                if(jsondata.success){
                                    $('users_count').val(u_count--);
                                    $('#issue-users').html('User "' + u_name + '" has been deleted.').removeClass('disabled');
                                    //u_row.effect("pulsate", {times: 2}, 200, function(){u_row.remove();});
                                    u_row.addClass('theme_bkgcolor1').fadeOut(1500, function(){ $(this).remove() });
                                }else{
                                    $('#issue-users').html(jsondata.rtndata).removeClass('disabled');
                                }
                                $.foundry.hideThrobber();
                            },
                            'json');
                    }else{
                        $('users_count').val(u_count--);
                        u_row.remove();
                    }
                }
                return false;
            });

            $('div').delegate('.user_editprofile', 'click', function(e){
                e.preventDefault();
                var u_row = $(this).closest('.user_row');
                u_profile_div = u_row.find('.user_profile');
                if(u_profile_div.is(':hidden'))
                    u_profile_div.slideDown(300);
                else
                    u_profile_div.slideUp(300);
                $(this).text((($(this).text() == 'Hide Profile') ? 'View' : 'Hide')+' Profile');
                return false;
            });

            $(document).delegate('#admnewuser', 'click', function(e){
                e.preventDefault();
                var u_count = parseInt($('#users_count').val());
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'addnewuser', val:u_count},
                    function(data){
                        $('#tabs-users').append(data);
                        $('users_count').val(u_count++);
                        $.foundry.hideThrobber();
                    },
                    'text');
                return false;
            });

            $(document).delegate('#user_bulkcheck', 'click', function(){
                var state = $(this).is(':checked');
                $('.user_checks').each(function(){
                    if($(this).is(':visible')) $(this).attr('checked', state);
                });
            });

            $(document).delegate('#user_bulkact', 'click', function(e){
                e.preventDefault();
                var opt = $('#user_bulkopt').val();
                if(opt != ''){
                    $('.user_checks').each(function(){
                        if($(this).is(':checked')){
                            if(opt == 'deactivate'){
                                $(this).siblings('.user_actions').find('.user_act').trigger('click');
                            }else if(opt == 'delete'){
                                $(this).siblings('.user_actions').find('.user_del').trigger('click');
                            }
                        }
                    });
                }
            });

            $(document).delegate('.users_pass', 'keyup', function(){
                var elem = $(this);
                var pass = elem.val();
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'checkpasswordstrength', val:pass},
                    function(data){
                        elem.siblings('.users_pass_strength').text(data.label).removeClass('state0 state1 state2 state3 state4 state5').addClass('state'+data.strength+' dottedborder');
                        $.foundry.hideThrobber();
                    },
                    'json'
                );
            });

            $(document).delegate('.users_pass_generate_open', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var pos = elem.position();
                var box = elem.closest('.row').find('.small-box');
                box.css({top: pos.top, left: pos.left}).removeClass('hidden');
            });

            $(document).delegate('.user_pass_generate_cancel', 'click', function(){
                $(this).closest('.small-box').addClass('hidden');
            });

            $('div').delegate('.user_pass_generate_ok', 'click', function(){
                var elem = $(this);
                var pwdfield = elem.closest('.user_profile').find('.users_pass');
                var pwdopts = elem.closest('.small-box').find('input[name^="user_pass_generate_opts"]').serialize();
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'getpassword', opts:pwdopts},
                    function(val){
                        pwdfield.val(val).trigger('keyup');
                        $.foundry.hideThrobber();
                    },
                    'text');
                elem.closest('.small-box').addClass('hidden');
                return false;
            });

            // -- Advanced --

            // Data Aliases

            $(document).delegate('#dataalias_add', 'click', function(){
                var newitem = $('#adv_data_aliases').find('.newalias');
                newitem.before(newitem.clone().removeClass('newalias hidden'));
            });

            $(document).delegate('#taxalias_add', 'click', function(){
                var newitem = $('#adv_tax_aliases').find('.newalias');
                newitem.before(newitem.clone().removeClass('newalias hidden'));
            });

            $(document).delegate('#admin_dataalias_add', 'click', function(){
                var newitem = $('#adv_admin_aliases').find('.newalias');
                newitem.before(newitem.clone().removeClass('newalias hidden'));
            });

            $(document).delegate('.dataalias_delete', 'click', function(){
                var row = $(this).closest('.row');
                var id = row.attr('rel');
                if(id > 0){
                    if(confirm('Deleting this alias will remove its association to any menus and other related data.\n\nIf you continue make sure taxonomies and menus are rechecked.\n\nContinue deleting?')){
                        $.foundry.showThrobber();
                        $.post(
                            $.foundry.ajax_url,
                            {'op':'deletealias', 'val':id},
                            function(jsondata){
                                if(jsondata.success){
                                    row.remove();
                                }
                                $.foundry.hideThrobber();
                            },
                            "json"
                        );
                    }
                }else{
                    row.remove();
                }
            });

            $(document).delegate('.taxalias_meta_add', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                if($('#taxaliasmetas').hasAttr('rel')){
                    var input = $('#taxaliasmetas').attr('rel');
                    var val = $(input).val();
                    $(input).val(val + elem.attr('rel'));
                }
            });

            // SEO

            $(document).delegate('.ht_toggle', 'click', function(){
            var tag = $(this).attr('id').substring(0, 6) + '_data';
            var label = $(this).text();
            if(label == 'Less'){
                $('#'+tag).hide(500);
                $(this).text('More');
            }else{
                $('#'+tag).show(200);
                $(this).text('Less');
            }
            });

            $('div').delegate(".ht_seo_from, .ht_seo_to, .ht_301_from, .ht_301_to", 'change', function(){
                var orig = $(this).attr('rel');
                $("#ht_mod1").val((($(this).val() != orig) ? 'y' : ''));
                return false;
            });

            $('div').delegate(".ht_seo_active, .ht_301_active", 'click', function(){
                var orig = $(this).attr('rel');
                if($(this).attr('checked')){
                    $("#ht_mod1").val(((orig == '') ? 'y' : ''));
                }else{
                    $("#ht_mod1").val(((orig == 'y') ? 'y' : ''));
                }
                return false;
            });

            $('div').delegate(".ht_delete", 'click', function(e){
                e.preventDefault();
                if(confirm("Delete this row?")){
                    $(this).closest("div").remove();
                }
                return false;
            });

            $(document).delegate('#ht_www1', 'change', function(){
                ht_sel($(this).val(), 'www');
            });

            $(document).delegate('#ht_www_data', 'keyup', function(){
                $("#ht_www1").val('');
                $("#ht_mod2").val('y');
            });

            $(document).delegate('#ht_img1', 'change', function(){
                ht_sel($(this).val(), 'img');
            });

            $(document).delegate('#ht_img_data', 'keyup', function(){
                $("#ht_img1").val('');
                $("#ht_mod2").val('y');
            });

            function ht_sel(sel, sect){
                if(sel == ''){
                    $("#ht_"+sect+"_data").html($("#"+sect+"_data").val());
                    $("#ht_mod2").val('');
                }else if(sel == 'disable'){
                    var data = $("#ht_"+sect+"_data").html().split(/(\r\n|\n|\r)/gm);
                    for(var i in data){
                        var line = data[i].replace(/(\r\n|\r|\n)/, '');
                        if(line.substr(0, 1) != '#' && line != '') data[i] = '#'+line;
                    }
                    $("#ht_"+sect+"_data").html(data.join(''));
                    $("#ht_mod2").val('y');
                }else{
                    $("#ht_"+sect+"_data").html($("#"+sel).val());
                    $("#ht_mod2").val('y');
                }
            }

            // Robots

            $(document).delegate('#robots_revision', 'click', function(e){
                e.preventDefault();
                var file = $("#robots_revfile").val();
                $.get($.foundry.admin_core_url+file, function(data){
                    if(confirm("The selected file's contents are:\n\n"+data+"\nIs this the one you want?")){
                        $.foundry.showThrobber();
                        $.post(
                            $.foundry.ajax_url,
                            {op:'revertrobotfile', val:file},
                            function(jsondata){
                                if(jsondata.success){
                                    alert('The reversion was successful.');
                                }else{
                                    alert('There was a problem trying to revert the robots file.');
                                }
                                $.foundry.hideThrobber();
                            },
                            'json'
                        );
                    }
                });
            });

            // User Manager

            $('.ua_value_row').width($('.ua_value_row:first span').length * $('.ua_value_row:first span').width());
            $('.ua_value_group_title').width($('.ua_value_group_title:first span').length * $('.ua_value_group_title span').width());
            $('#ua_usertypes').width($('#ua_usertypes span').length * $('#ua_usertypes span').width());

            $(document).delegate('.ua_value_row', 'mouseover', function(){
                var rel = $(this).attr('rel');
                $(this).addClass('highlight-row');
                if($('#ua_item_'+rel).length > 0)
                    $('#ua_item_'+rel).addClass('dark-highlight-row');
                else
                    $('#ua_item_group_'+rel).addClass('dark-highlight-row');
            });

            $(document).delegate('.ua_value_row', 'mouseout', function(){
                var rel = $(this).attr('rel');
                $(this).removeClass('highlight-row');
                if($('#ua_item_'+rel).length > 0)
                    $('#ua_item_'+rel).removeClass('dark-highlight-row');
                else
                    $('#ua_item_group_'+rel).removeClass('dark-highlight-row');
            });

            $(document).delegate('#ut_addtype', 'click', function(){
                if($('.ut_name:visible').length < 10){
                    var ut_menu = $('#ut_std_types_ref').html();
                    $('#ut_list').append('<p>User Type: <input type="text" class="ut_name" name="ut_newname[]" value="" rel="" /> Copy Allowances from <select name="ut_new_copy_str_types[]" class="ut_copy_std_types">' + ut_menu + '</select> <a href="#" class="ut_delete" rel="">Delete Type</a></p>');
                    $('#ut_list .ut_name:last').focus();
                }else{
                    alert("No further User Types can be added.");
                }
            });

            $(document).delegate('.ut_delete', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var rel = elem.siblings('.ut_name').attr('rel');
                var do_delete = false;
                if(rel > 0){
                    do_delete = (confirm('Delete this User Type?'));
                }else{
                    do_delete = true;
                }
                if(do_delete){
                    elem.attr("rel", "deleted");
                    elem.parent().hide();
                }
            });

            $(document).delegate('.ua_groups_toggle', 'click', function(){
                var elem = $(this);
                var rel = elem.attr('rel').split('|');
                var group = rel[0];
                var key = rel[1];
                if(elem.is(':checked')){
                    $('.'+group+'_group_item_'+key).each(function(){
                        $(this).attr('checked', 'checked');
                    });
                }else{
                    $('.'+group+'_group_item_'+key).each(function(){
                        $(this).removeAttr('checked');
                    });
                }
            });

            $(document).delegate('.ua_groups_reset', 'click', function(){
                if(confirm("This will revert all changes to the selected Group and User Type.  Continue?")){
                    var elem = $(this);
                    var rel = elem.attr('rel').split('|');
                    var group = rel[0];
                    var key = rel[1];
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'revertuagroup', val:key, group:group},
                        function(jsondata){
                            if(jsondata.success){
                                var indx = 0;
                                $('.'+group+'_group_item_'+key).each(function(){
                                    if(jsondata.rtndata[indx] == 1){
                                        $(this).attr('checked', 'checked');
                                    }else{
                                        $(this).removeAttr('checked');
                                    }
                                    indx++;
                                });
                            }else{
                                alert('There was a problem trying to revert the allowances.');
                            }
                            $.foundry.hideThrobber();
                        },
                        'json'
                    );
                }
            });

            $('div').delegate('.ua_delete', 'click', function(e){
                e.preventDefault();
                if(confirm('Delete this Custom Allowance?')){
                    var rel = $(this).attr('rel');
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'deleteua', val:rel},
                        function(jsondata){
                            if(jsondata.success){
                                $('#ua_item_'+rel).addClass('theme_bkgcolor1').fadeOut(1500, function(){ $(this).remove() });
                                $('#ua_value_row_'+rel).addClass('theme_bkgcolor1').fadeOut(1500, function(){ $(this).remove() });
                            }else{
                                alert('The allowance could not be deleted.');
                            }
                            $.foundry.hideThrobber();
                        },
                        'json'
                    );
                }
                return false;
            });

            $(document).delegate('#ua_addcustom', 'click', function(e){
                e.preventDefault();
                var name = $('#ua_addname').val();
                if(name == '') { alert('No name was provided.'); return false; }
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'addnewua', val:name},
                    function(jsondata){
                        if(jsondata.success){
                            $('#ua_items').append(jsondata.rtndata.i);
                            $('#ua_values').append(jsondata.rtndata.v);
                            $('#ua_values').animate({
                                scrollTop: $('#ua_values')[0].scrollHeight}, 500
                            );
                        }else{
                            alert(jsondata.rtndata);
                        }
                        $.foundry.hideThrobber();
                    },
                    'json'
                );
            });

            $(document).delegate('#ua_cancelacct_content', 'change', function(){
                if($(this).val() == 'reassign')
                    $('#ua_cancelacct_users').removeClass('hidden');
                else
                    $('#ua_cancelacct_users').addClass('hidden');
            });

            // Files and Email

            $(document).delegate('#smtp_test', 'click', function(e){
                var host = $('#SMTP_HOST').val();
                var user = $('#SMTP_USERNAME').val();
                var pass = $('#SMTP_PASSWORD').val();
                var port = $('#SMTP_PORT').val();
                var encr = $('#SMTP_SECURE_MODE').val();
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'smtptest', host:host, port:port, encr:encr},
                    function(jsondata){
                        if(jsondata.success){
                            alert('Your settings worked and communication went through.');
                            $('#SMTP_VALID').val('valid');
                        }else{
                            alert(jsondata.rtndata);
                            $('#SMTP_VALID').val('not valid');
                        }
                        $.foundry.hideThrobber();
                    },
                    'json'
                );
            });

            $(document).delegate('#ftp_test', 'click', function(e){
                var host = $('#FTPHOST').val();
                var user = $('#FTPUSER').val();
                var pass = $('#FTPPASS').val();
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'ftptest', host:host, user:user, pass:pass},
                    function(jsondata){
                        if(jsondata.success){
                            alert('Your settings worked.');
                            $('#FTPVALID').val('valid');
                        }else{
                            alert(jsondata.rtndata);
                            $('#FTPVALID').val('not valid');
                        }
                        $.foundry.hideThrobber();
                    },
                    'json'
                );
            });

            $(document).delegate('#reset_caches', 'click', function(e){
                e.preventDefault();
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:'resetcaches'},
                    function(jsondata){
                        if(jsondata.success){
                            alert('All caches have been reset.');
                        }else{
                            alert('There was a problem resetting caches.');
                        }
                        $.foundry.hideThrobber();
                    },
                    'json'
                );
                return false;
            });

            // Crons

            $(document).delegate('.cron-run, .cron-enable, .cron-disable', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var id = elem.closest('.table-body').attr('rel');
                var action = elem.attr('class').split(' ')[0];
                $.foundry.showThrobber();
                $.post(
                    $.foundry.ajax_url,
                    {op:action, val:id},
                    function(jsondata){
                        if(jsondata.success){
                            if(action == 'cron-run')
                                alert('The cron has run successfully.');
                            else if(action == 'cron-enable')
                                alert('The cron has been enabled successfully.');
                            else if(action == 'cron-disable')
                                alert('The cron has been disabled successfully.');
                            window.location.href = $.foundry.admin_core_url+'settings?tab=advanced&sub=adv_cron';
                        }else{
                            if(action == 'cron-run')
                                alert('There was a problem running this cron.');
                            else if(action == 'cron-enable')
                                alert('There was a problem been enabling this cron.');
                            else if(action == 'cron-disable')
                                alert('There was a problem been disabling this cron.');
                        }
                        $.foundry.hideThrobber();
                    },
                    'json'
                );
            });

            $(document).delegate('.cron-delete', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                var id = elem.closest('.table-body').attr('rel');
                if(confirm('Are you sure you want to delete this cron?')){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'cron-delete', val:id},
                        function(jsondata){
                            if(jsondata.success){
                                alert('The cron has been deleted.');
                                window.location.href = $.foundry.admin_core_url+'settings?tab=advanced&sub=adv_cron';
                            }else{
                                alert('There was a problem deleting this cron.');
                            }
                            $.foundry.hideThrobber();
                        },
                        'json'
                    );
                }
            });

            // Reports

            $(document).delegate('#settingstabs .setdatapanel', 'click', function(e){
                e.preventDefault();
                var panel = $(this);
                if(panel.hasClass('expanded')){
                    panel.animate({height: '1em'}, 400).removeClass('expanded');
                }else{
                    panel.animate({height: '100%'}, 400).addClass('expanded');
                }
            });

            $(document).delegate('.reportresetuseraudit', 'click', function(e){
                e.preventDefault();
                var rel = $(this).attr('rel').split('|');
                if(confirm('Clear all '+rel[1]+' events for this user?')){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'clearreportauditlogs', val:rel[0], category:rel[1]},
                        function(jsondata){
                            if(jsondata.success){
                                alert('The event logs have been cleared.');
                                $('.setreportrow[rel="'+rel[0]+"|"+rel[1]+'"]').remove();
                            }else{
                                alert(jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        'json'
                    );
                }
            });

            $(document).delegate('.reportclearerrorevents', 'click', function(e){
                e.preventDefault();
                var elem = $(this);
                if(confirm('Clear all error log events?')){
                    $.foundry.showThrobber();
                    $.post(
                        $.foundry.ajax_url,
                        {op:'clearreporterrorlogs'},
                        function(jsondata){
                            if(jsondata.success){
                                alert('The event logs have been cleared.');
                                elem.closest('.setreportpanel').find('.setreportrow').remove();
                            }else{
                                alert(jsondata.rtndata);
                            }
                            $.foundry.hideThrobber();
                        },
                        'json'
                    );
                }
            });

            // -- Misc --

            // Colorpicker

            $("#settingscolorpicker").dialog({
                autoOpen: false,
                width: 240,
                height: 250,
                modal: true,
                resizable: false,
                close: function(){
                    var button_id = $("#colorpicker_buttonid").val();
                    $(button_id).removeClass('active');
                }
            });

            $(document).delegate('.colorpicker_button', 'click', function(){
                if($('#settingscolorpicker').html() == ''){
                    $.post(
                        $.foundry.ajax_url,
                        {op:'getcolorpickercontents', val:''},
                        function(html){
                            $('#settingscolorpicker').html(html);
                        },
                        "html"
                    );
                }
                $('#settingscolorpicker').dialog('option', 'position', [jQx, jQy]);
                $('#settingscolorpicker').dialog('open');

                var button_id = "#"+this.id;
                var field_id = this.id;
                field_id = field_id.substr(3);
                var field_color = $("#"+field_id).val();
                $(button_id).addClass('active');

                $("#colorpicker_buttonid").val(button_id);
                $("#colorpicker_fieldid").val(field_id);
                $("#colorpicker_fieldcolor").html(field_color);
            });

            $(".colorpicker_swatch").click(function(){
                var button_id = $("#colorpicker_buttonid").val();
                var field_id = $("#colorpicker_fieldid").val();
                var newcolor = $("#colorpicker_hoverdiv").html();
                $(button_id).css('background-color', newcolor);
                $(button_id+"__reset").removeClass('disabled');
                $("#"+field_id).val(newcolor);
            });

            $(".colorpicker_swatch").mouseover(function(){
                $("#colorpicker_hoverdiv").html($(this).attr('title').trim());
            });

            $(".colorpicker_reset").click(function(){
                var oldcolor = $(this).attr('rel');
                var this_id = $(this).attr('id');
                var button_id = "#"+this_id.substring(0, this_id.indexOf('__reset'));
                var field_id = "#"+button_id.substring(4);
                $(button_id).css('background-color', oldcolor);
                $(button_id+"__reset").addClass('disabled');
                $(field_id).val(oldcolor);
            });

            $("#colorpicker_closebutton").click(function(){
                $("#settingscolorpicker").dialog('close');
                return false;
            });
        }
    });
});