<?php
// ---------------------------
//
// FOUNDRY ADMIN SYSTEM CONFIGURATION
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

if(!defined("VALID_LOAD")) die("This file cannot be accessed directly!");

//*****************************************************************************
//   FOUNDRY SYSTEM VERSIONS
//*****************************************************************************

define ("SYS_NAME", "Foundry");
define ("SYS_TAG", "fdry");
define ("AUTHOR_NAME", "Chris Donalds");
define ("COPYRIGHT_NAME", "Foundry Development Group");
define ("COPYRIGHT_WEB", "http://www.foundrypowered.com");

define ("CODE_VER", "5.9.0");
define ("CODE_VER_CORE", "5900");
define ("CODE_VER_NAME", "Titania");
define ("CODE_VER_TAG", "fttn");
define ("SHOW_ALL_ERRORS", false);

checkServerVersions('PHP');
checkServerVersions('MySQL');
checkEnviron('memory_limit');
checkServerGlobals();

//*****************************************************************************
//   MODIFY PHP DIRECTIVES
//*****************************************************************************

define ("F_MEMORY_LIMIT", "128M");
@ini_set("memory_limit", F_MEMORY_LIMIT);
@ini_set("display_errors", "on");
if(defined("SHOW_ALL_ERRORS")) {
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}

//*****************************************************************************
//   ROOT FOLDER
//*****************************************************************************

if(!defined ("LOADER_DOCUMENT_ROOT")){
	// Ok, how did we get here without the LOADER_DOCUMENT_ROOT setting?
	die("Loader.php not executed prior to config!");
}else{
	// Build document_root (what's used internally) from loader values
	define ("DOCUMENT_ROOT", LOADER_DOCUMENT_ROOT.VHOST);
}

//*****************************************************************************
//   FILE SYSTEM SETTINGS
//*****************************************************************************

// - site_path: server/path/to/file
// - web_url: http://server/path/to/file

if(!defined ("LIVE")) define ("LIVE", false);
define ("PROTOCOL", ((isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 1 || strtolower($_SERVER['HTTPS']) == 'on')) || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')) ? "https://" : "http://");
define ("SERVER", PROTOCOL.$_SERVER['HTTP_HOST']);
define ("WEB_URL", SERVER.VHOST);

if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] != '' && php_sapi_name() !== 'cli'){
	// normal, browser (not CLI) loaded
	define ("SSL_SERVER", "https://".$_SERVER['HTTP_HOST']);	// forced to be SSL
	define ("SSL_WEB_URL", SSL_SERVER.VHOST);
	define ("PHP_CLI", false);
}else{
	// most likely loaded from PHP CLI.  setup constants for CLI/CRON use
	define ("SSL_SERVER", null);
	define ("SSL_WEB_URL", null);
	define ("QUICK_LOAD", true);
	define ("PHP_CLI", true);
}

define ("SITE_PATH", DOCUMENT_ROOT);				// FQDN of site (eg. /user/domain/httpdocs/)
define ("ROOT_FOLDER", "../");
define ("INC_FOLDER", "inc/");						// Folder where engine files are located (css, js, classes, rss)
define ("ADMIN_FOLDER", "admin/");					// Admin folder root
define ("MEDIA_FOLDER", "media/");					// Media folder root
define ("CORE_FOLDER", INC_FOLDER."_core/");		// Core sub-folder (front/admin)
define ("CONFIG_FOLDER", INC_FOLDER."_config/");	// Configurations sub-folder (admin)
define ("SETTINGS_FOLDER", INC_FOLDER."_settings/");// Admin settings
define ("PLUGINS_FOLDER", INC_FOLDER."_plugins/");	// Plugins sub-folder (admin)
define ("EXTENSIONS_FOLDER", INC_FOLDER."_extensions/");	// Extensions sub-folder (admin)
define ("LIB_FOLDER", INC_FOLDER."_lib/");          // Libraries sub-folder (admin)
define ("JS_FOLDER", INC_FOLDER."_js/");			// Javascript sub-folder (front/admin)
define ("DB_ENGINE", "mysqli");
define ("DB_FOLDER", LIB_FOLDER."db_".DB_ENGINE."/");             // Database classes sub-folder (admin)
define ("ADM_THEME_FOLDER", INC_FOLDER."_themes/"); // Themes sub-folder (admin)
define ("ADM_THEME", ".sys/");
define ("ADM_CSS_FOLDER", "css/");                  // System CSS folder (admin)
define ("EDITOR_FOLDER", INC_FOLDER."_editors/");	// Location of CMS editors (CKEditor, TinyMCE...)
define ("CACHE_FOLDER", INC_FOLDER."_cache/");		// Cache folder (front/admin)
define ("REV_FOLDER", CACHE_FOLDER."revisions/");	// File revisions storage (admin)

define ("CSS_FOLDER", "css/");                      // System CSS sub-folder (front)
define ("THEME_FOLDER", INC_FOLDER."_themes/");     // Themes sub-folder (front)
define ("RSS_FOLDER", "rss/");						// RSS sub-folder (front)
define ("ATOM_FOLDER", "rss/atom/");				// Atom feed sub-folder (front)
define ("RDF_FOLDER", "rss/rdf/");					// RDF feed sub-folder (front)

//*****************************************************************************
//   LIBRARIES
//*****************************************************************************

define ('DB_CFG_ALIAS', "__dbcfg");
define ('THUMB_GEN', ADMIN_FOLDER.LIB_FOLDER."thumb/phpThumb.php");
define ('THUMB_GEN_ALIAS', "__thumb");
define ('REST_ALIAS', "__rest");
define ('CRON_ALIAS', "__cron");
define ('AJAX_ALIAS', "__ajax");
define ('INSTALLER_ALIAS', "__install");

define ('RSSRDF_GEN', CORE_FOLDER."rssrdf.php");
define ('RSS_ALIAS', "rss");
define ('RDF_ALIAS', "rdf");
define ('ATOM_ALIAS', "atom");
if(!defined("QUICK_LOAD")) define("QUICK_LOAD", false);

//*****************************************************************************
//   SECURITY
//*****************************************************************************

// Attention! It is highly recommended that you change this value
// from the default, since hackers may already know it.

define ("STATIC_SALT", 'btVBFBD|,g%0]g1Vi7@LffNpV}3g/dzbM~0]x2vEBxZ$`&X_?,Wk:Nb8Db8Rngc@');

//*****************************************************************************
//   OTHER SETTINGS
//*****************************************************************************

define ("SECTIONING", false);                       	// Data subdivisioning
define ("SITE_VISIBILITY_NORMAL", 0);
define ("SITE_VISIBILITY_MAINTENANCE", 1);
define ("SITE_VISIBILITY_PRIVATE", 2);

//*****************************************************************************
//   RECORD-KEEPING SETTINGS
//*****************************************************************************

define ("ALLOW_ARCHIVE", true);			// 'archive' action
define ("ALLOW_DELETE", true);			// 'delete' action
define ("FULL_DELETE", true);			// delete record or set 'delete' field
define ("ALLOW_UNDELETE", true);		// 'undelete' action
define ("ALLOW_PUBLISH", true);			// 'publish' action and 'save & publish' option
define ("ALLOW_ACTIVATE", true);		// 'activate' action and 'save & activate' option
define ("ALLOW_DRAFT", true);			// 'save to draft' option
define ("ALLOW_UNPUB_SAVE", true);		// 'save' option displayed
define ("ALLOW_SORT", true);        	// column sorting on list page
define ("ALLOW_SEARCH", true);      	// search function on list page
define ("USE_SECTIONS", false);     	// universal divisioning on/off
define ("ALLOW_ADDPAGE", true);     	// shows 'add sub-page' on page list
define ("ALLOW_METAPAGE", true);    	// shows 'edit meta-data' on page list
define ("ROOT_ID", "sectionid");		// session root table id where root data is pulled

define ("PAGE_EDITOR", 1);
define ("PAGE_DB", 2);
define ("PAGE_FORM", 3);

//*****************************************************************************
//   START DATABASE INITIALIZATION
//*****************************************************************************

define ("PHP_CLI_DBHOST", "__CLI__");
define ("DB_SYS_TABLE_PREFIX", "");		// this constant prefixes all system tables.
										// set it to anything but an empty string if you are planning
										// on running multiple copies of Foundry on the same site.

require(SITE_PATH.ADMIN_FOLDER.CONFIG_FOLDER."db_configs.php");

//*****************************************************************************
//   CUSTOMIZABLE/VARIABLE SETTINGS
//*****************************************************************************

$defcfg  = array(	"BUSINESS" => "My Business",
					"SITE_NAME" => "My ".SYS_NAME." Site",
					"OWNER_EMAIL" => "",
					"ADMIN_EMAIL" => "",
					"BUS_ADDRESS" => "",
					"BUS_PHONE" => "",
					"BUS_FAX" => "",
                    "TIMEZONE" => "America/Vancouver",
					"IMG_MAX_WIDTH" => 800,
					"IMG_MAX_HEIGHT" => 600,
					"IMG_MAX_UPLOAD_SIZE" => "500KB",
					"THM_MAX_WIDTH" => 100,
					"THM_MAX_HEIGHT" => 100,
					"THM_MED_MAX_WIDTH" => 200,
					"THM_MED_MAX_HEIGHT" => 200,
					"THM_MAX_UPLOAD_SIZE" => "50KB",
					"ORG_THM_MAX_WIDTH" => 100,
					"ORG_THM_MAX_HEIGHT" => 100,
                    "IMG_UPLOAD_FOLDER" => "images/",
                    "THM_UPLOAD_FOLDER" => "images/",
                    "FILE_UPLOAD_FOLDER" => "files/",
					"FILE_MAX_UPLOAD_SIZE" => "2MB",
					"ACTION_ICONS" => 0,
					"IMG_LOGIN_LOGO" => "",
					"IMG_LOGIN_LOGOS" => "",
					"EMAIL_CONFIRM" => "",
					"EMAIL_NOTIFY" => "",
					"THEMES_ENABLED" => true,
					"PHP_DATE_FORMAT" => "Y-m-d",
					"PHP_TIME_FORMAT" => "g:i:s",
					"SITEOFFLINE" => false,
					"SITEOFFLINE_MSG" => "This site is down for maintenance. Please check back again soon.",
					"DB_TABLE_PREFIX" => "data_",
					"DB_SYS_TABLE_PREFIX" => "",
					"ERROR_SENSITIVITY" => E_ERROR | E_WARNING | E_PARSE,
					"ALLOW_DEBUGGING" => true,
					"ERROR_LOG_TYPE" => 3,
                    "JQUERY_FALLBACK_VER" => "1.7.1",
                    "JQUERYUI_FALLBACK_VER" => "1.9.1",
					"CMS_EDITOR" => "ckeditor",
                    "DEF_THEME_CSS" => "master.css",
                    "DEF_THEME_JS" => "scripts.js",
					"CHARSET" => "UTF-8",
					"LANGUAGE" => "english",
					"LANG_ISO" => "en-CA",
					"FACEBOOKLOGIN" => false,
					"FTPUSER" => "",
					"FTPPASS" => "",
					"FTPHOST" => "localhost",
					"FTPCONN" => "ftp",
					"DEF_IMAGE_PROCESSOR" => "gdlib",
					"ERROR_404_PAGE" => 0,
					"ERROR_403_PAGE" => 0,
					"SMTP_HOST" => "",
					"SMTP_USERNAME" => "",
					"SMTP_PASSWORD" => "",
					"SMTP_PORT" => "25",
					"SMTP_SECURE_MODE" => "",
					"CACHE_JS" => true,
					"CACHE_CSS" => true,
					"COMPRESS_CSS" => true,
					"CACHE_LIFESPAN" => 5,
					"CACHE_LIFESPAN_PERIOD" => 'min',
					"CHMOD_FILE" => 0644,
					"CHMOD_FOLDER" => 0755,
					"CHMOD_TEMP_FOLDER" => 0777,
					"MAX_LOGIN_TRIES" => 3,
					"CONTEXTS" => 'targettype,datatype,targetaction,url,usertype,allowance,queryvar'
				);
$configs = getCustomConfigVals();

#----------- INTERFACE ----------------------------

#----------- GENERAL SETTINGS ---------------------

define ("DEF_LANGUAGE", LANGUAGE);
define ("DEF_LANG_ISO", LANG_ISO);

#----------- TIMEZONE -----------------------------

ini_set("date.timezone", TIMEZONE);
date_default_timezone_set(TIMEZONE);

define ("BLANK_DATE", "0000-00-00 00:00:00");

#----------- LANGUAGES ----------------------------

#----------- IMAGES, FILES AND THUMBS -------------

//Images
define ("IMG_WARNING", "<span style=\"color:red\">(Will be resized to max ".IMG_MAX_WIDTH." x ".IMG_MAX_HEIGHT." pixels. Maximum upload size is ".(IMG_MAX_UPLOAD_SIZE / 1000)." MB)</span>");
define ("NO_IMG", "no_image.png");												// No-image file name

//Thumbs
define ("NO_THM", "no_thumb.png");

#----------- LISTS AND TABLES -------------

define ("LIST_ROWLIMIT", 100);
define ("LIST_PAGESSHOWN", 20);
define ("LIST_ROWCOLOR1", "#ffffff");
define ("LIST_ROWCOLOR2", "#eeeeee");
define ("HEADER_BGCOLOR", "#ccccff");
define ("DATE_FORMAT", "yyyy-mm-dd");
define ("DATE_SEPARATOR", "-");
define ("EXCERPT_CHAR_LIMIT", 40);
define ("REQD_ENTRY", "<span class=\"reqd\">*</span>");

//*****************************************************************************
//   VALIDATION FUNCTIONS
//*****************************************************************************

#----------- VALIDATE FOLDER ACCESS --------------

checkFolders();

/**
 * Checks for the existence of critical control files and folders.
 * Doing so will return more description of the missing file or folder.
 * Also, make sure specific files and folders are writable
 */
function checkFolders(){
	$setold = 0;
	$setnew = 0;
	// $setold += checkFileExistence(SITE_PATH.ADMIN_FOLDER.INC_FOLDER."common.php", SITE_PATH.ADMIN_FOLDER.INC_FOLDER);
	$setnew += checkFileExistence(SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."common_core.php", SITE_PATH.ADMIN_FOLDER.CORE_FOLDER);
	if($setnew < 1) die("CONFIG ERR 0x010: One or more common_x files are missing!");
	checkFileExistence(SITE_PATH.ADMIN_FOLDER.JS_FOLDER, SITE_PATH.ADMIN_FOLDER.JS_FOLDER, true);
	checkFileExistence(SITE_PATH.ADMIN_FOLDER.DB_FOLDER, SITE_PATH.ADMIN_FOLDER.DB_FOLDER, true);

    checkFolderExistence(SITE_PATH.MEDIA_FOLDER.IMG_UPLOAD_FOLDER);
    checkFolderExistence(SITE_PATH.MEDIA_FOLDER.THM_UPLOAD_FOLDER);
    checkFolderExistence(SITE_PATH.MEDIA_FOLDER.FILE_UPLOAD_FOLDER);
    checkFolderExistence(SITE_PATH.ADMIN_FOLDER.REV_FOLDER);
    checkFolderExistence(SITE_PATH.ADMIN_FOLDER.CACHE_FOLDER);
}

/**
 * Check a file for existence
 * @param string $testfor
 * @param string $where
 * @param boolean $stophere
 */
function checkFileExistence($testfor, $where, $stophere = false){
	if(file_exists($testfor)){
		return 1;
	}elseif($stophere){
		print "CONFIG ERR 0x011: Cannot find ".basename($testfor)." in $where!";
		die();
	}else{
		return 0;
	}
}

/**
 * Check a folder for existence and create it if it does not
 * @param string $folder
 */
function checkFolderExistence($folder){
    if(file_exists($folder)){
        if(!is_writable($folder)) @chmod($folder);
    }else{
    	@mkdir($folder);
    }
}

//*****************************************************************************
//   SUPPORT FUNCTIONS
//*****************************************************************************

/**
 * Check the versions of specific servers to ensure they meet minimum requirements
 * @param string $server 				PHP, MySQL
 * @param bool $returnval 				True to return the error, if any
 */
function checkServerVersions($server, $returnval = false){
	$prob = "";
	$server = strtolower($server);
	if($server == 'php'){
		// PHP 5.2 required
		if(version_compare(phpversion(), '5.2.0', '<')) $prob = "CONFIG ERR 0x020: PHP version 5.2.0 minimum required to operate ".SYS_NAME." v".CODE_VER;
		// $reqd_ext = array("date", "ereg", "iconv", "json", "mcrypt", "session");
		// foreach($reqd_ext as $ext){
		// 	if(extension_loaded($ext)) unset($reqd_ext[$ext]);
		// }
		// if(count($reqd_ext) > 0) die("CONFIG ERR 0x021: The following PHP extensions are required: ".join(", ", $reqd_ext).".");
		if(!function_exists('mysqli_get_client_version')) die("CONFIG ERR 0x022: MySQL or MySQLi are either not properly installed, or the extension is missing, in your PHP environment.");
	}elseif($server == 'mysql'){
		// MySQL 4.24 required
		$mysqlver = floatval(mysqli_get_client_version());
		if($mysqlver < 50000) $prob = "CONFIG ERR 0x023: MySQL version 5.0.0 minimum required to operate ".SYS_NAME;
	}
	if($prob != ""){
		if($returnval)
			return $prob;
		else
			die($prob);
	}
}
/**
 * Check the status of specific PHP or Apache settings
 * @param string $var 					Installation/environment variable
 * @param bool $returnval 				True to return the error, if any
 */
function checkEnviron($var, $returnval = false){
	$prob = "";
	$var = strtolower($var);
	if($var == 'memory_limit'){
		// 32mb required
		if(intval(ini_get($var)) <= 32) $prob = "CONFIG ERR 0x031: Memory limit setting must be more than 32MB to operate ".SYS_NAME;
	}
	if($prob != ""){
		if($returnval)
			return $prob;
		else
			die($prob);
	}
}

/**
 * Load config values from Settings table
 * @return array Array of values
 */
function getCustomConfigVals(){
	// retrieve customizable config values from 'settings' table
	// any invalid or missing data will revert to defaults
	// first connect to database
	global $defcfg;

    $csval = $defcfg;

	if(DB_USED){
		$conn = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME) or die("CONFIG ERR 0x041: Cannot connect to ".DBNAME." (".DBHOST.", ".DBNAME.", ".DBUSER.")");
		$query = "SELECT * FROM settings";
		$settings = mysqli_query($conn, $query);
		$err = ($settings === false);
	}else{
		$err = true;
	}

	if(!$err){
		$err = false;
		while($rec = mysqli_fetch_assoc($settings)){
			switch($rec['type']){
				case "str":
					$csval[$rec['name']] = $rec['value'];
					break;
				case "int":
					$csval[$rec['name']] = floatval($rec['value']);
					break;
				case "upd":
					if(empty($rec['value'])) doInitUpdate($rec['name'], $conn);
					break;
			}
		}
	}
	$csval['DBSETTINGSOK'] = !$err;

	foreach($csval as $ckey => $cval) if(!defined($ckey)) define ($ckey, $cval);

	return $csval;
}

/**
 * Prepare/update initial settings
 * @param string $fieldname
 * @param object $conn
 */
function doInitUpdate($fieldname, $conn){
	// update data or file based on requested fieldname
	switch($fieldname){
		case "CKE_CSS_COLORS":
			$ckefolder = SITE_PATH.ADMIN_FOLDER.EDITOR_FOLDER."ckeditor/";

			// read all color attributes from "{root}css/layout.css" file
			if(!is_writable($ckefolder."config.js")) chmod($ckefolder."config.js", 0777);

			$values = array();
			$public_css_folder = SITE_PATH."css/";      // test css first
			(file_exists($public_css_folder."layout.css")) ? $cssfile = $public_css_folder."layout.css" : $cssfile = $public_css_folder."master.css";
			if($fp = @fopen($cssfile, "r")){
				while($line = fgets($fp)){
					$line = strtolower(trim($line));
					if(substr($line, 0, 6) == "color:"){
						preg_match("/([0-9a-f]+);/i", $line, $valfound);
						if(strlen($valfound[1]) == 3) $valfound[1] .= $valfound[1];
						$valfound[1] = strtoupper($valfound[1]);
						if(!in_array($valfound[1], $values)) $values[] = $valfound[1];
					}
				}
				fclose($fp);

				$valstr = implode (",", $values);
				if($valstr != ""){
					if($fcontents = file_get_contents($ckefolder."config.js")){
						// contents retrieved
						$fcontents = preg_replace("/colorButton_colors = '(.)+'/i", "colorButton_colors = '".$valstr."'", $fcontents);
						if(file_put_contents($ckefolder."config.js", $fcontents) !== false){
							// file updated with new contents
							$query = "UPDATE settings SET `value` = '$valstr' WHERE `name` = '$fieldname' LIMIT 1";
							mysqli__query($conn, $query);
						}
					}
				}
			}
			break;
	}
}

/**
 * Provide any missing elements in $_SERVER superglobal array that are not provided by server or due to the loader
 * being initiated from either FastCGI-PHP or CLI PHP.  Normally PHP will automatically populate the $_SERVER
 * superglobal when started with the server parameters.  However, sometimes running from the command-line these
 * attibutes are not presented.
 */
function checkServerGlobals(){
	$overrides = array(
		'HTTP_HOST' => 'localhost',
		'SCRIPT_NAME' => NULL,
		'REMOTE_ADDR' => '127.0.0.1',
		'REQUEST_METHOD' => 'GET',
		'SERVER_NAME' => NULL,
		'SERVER_SOFTWARE' => NULL,
		'HTTP_USER_AGENT' => NULL,
	);

	$_SERVER = $_SERVER + $overrides;
}
?>