<?php
// ---------------------------
//
// FOUNDRY DATABASE CONFIGS AND OPS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

if(!defined("VALID_LOAD")) die("This file cannot be accessed directly!");

/* REQUIRES CONFIGS.PHP TO BE CALLED FIRST */

if(!defined('ACCOUNTS_TABLE')) define ("ACCOUNTS_TABLE", DB_SYS_TABLE_PREFIX."admin_accts");
if(!defined('USERS_TABLE')) define ("USERS_TABLE", DB_SYS_TABLE_PREFIX."admin_accts");
if(!defined('EVENT_LOG_TABLE')) define ("EVENT_LOG_TABLE", DB_SYS_TABLE_PREFIX."event_log");
if(!defined('FIELDS_TABLE')) define ("FIELDS_TABLE", DB_SYS_TABLE_PREFIX."fields");
if(!defined('FIELDS_OBJECTS_TABLE')) define ("FIELDS_OBJECTS_TABLE", DB_SYS_TABLE_PREFIX."fields_objects");
if(!defined('FIELDS_DATA_TABLE')) define ("FIELDS_DATA_TABLE", DB_SYS_TABLE_PREFIX."fields_data_rel");
if(!defined('MENUS_TABLE')) define ("MENUS_TABLE", DB_SYS_TABLE_PREFIX."menus");
if(!defined('PAGE_TYPES_TABLE')) define ("PAGE_TYPES_TABLE", DB_SYS_TABLE_PREFIX."page_types");
if(!defined('PAGES_TABLE')) define ("PAGES_TABLE", DB_SYS_TABLE_PREFIX."pages");
if(!defined('MEDIA_TABLE')) define ("MEDIA_TABLE", DB_SYS_TABLE_PREFIX."media");
if(!defined('ATTRIBUTES_TABLE')) define ("ATTRIBUTES_TABLE", DB_SYS_TABLE_PREFIX."attributes");
if(!defined('PLUGINS_TABLE')) define ("PLUGINS_TABLE", DB_SYS_TABLE_PREFIX."plugins");
if(!defined('REGISTER_TABLE')) define ("REGISTER_TABLE", DB_SYS_TABLE_PREFIX."register");
if(!defined('SESSION_TABLE')) define ("SESSION_TABLE", DB_SYS_TABLE_PREFIX."session_login");
if(!defined('SETTINGS_TABLE')) define ("SETTINGS_TABLE", DB_SYS_TABLE_PREFIX."settings");
if(!defined('THEMES_TABLE')) define ("THEMES_TABLE", DB_SYS_TABLE_PREFIX."themes");
if(!defined('TERMS_TABLE')) define ("TERMS_TABLE", DB_SYS_TABLE_PREFIX."terms");
if(!defined('TERMS_DATA_TABLE')) define ("TERMS_DATA_TABLE", DB_SYS_TABLE_PREFIX."terms_data_rel");
if(!defined('TAXONOMIES_TABLE')) define ("TAXONOMIES_TABLE", DB_SYS_TABLE_PREFIX."taxonomies");
if(!defined('MEDIA_TABLE')) define ("MEDIA_TABLE", DB_SYS_TABLE_PREFIX."media");
if(!defined('MEDIA_FORMATS_TABLE')) define ("MEDIA_FORMATS_TABLE", DB_SYS_TABLE_PREFIX."media_formats");
if(!defined('MEDIA_FORMATS_ACTIONS_TABLE')) define ("MEDIA_FORMATS_ACTIONS_TABLE", DB_SYS_TABLE_PREFIX."media_formats_actions");

define ('DB_ERR_INIT', 0);
define ('DB_ERR_NOTREG', 1);
define ('DB_ERR_SVRCONN', 2);
define ('DB_ERR_LOAD', 3);
define ('DB_ACT_CONNECT', 100);
define ('DB_ACT_BACKREST', 101);
$db_link = null;

// setup db
if(DB_USED){
    // check if MySQL support is present in PHP
    if (!function_exists('mysqli_connect')) {
        die('Unable to use the MySQL database because the MySQLi extension for PHP is not installed. Check your php.ini to see how you can enable it.');
    }

    // initialize the database controller
    initDB();
	$db_link = @mysqli_connect(DBHOSTPORT, DBUSER, DBPASS, DBNAME);
    if($db_link->connect_errno > 0) configDB(DB_ERR_SVRCONN);
    checkServerVersions('MySQL');

	define("DB_VER", floatval(mysqli_get_client_version()));
}else{
	define("DB_VER", 999);
}

if(!defined('LIVE')) define ("LIVE", false);
if(!defined('BASICDB') && DB_USED) checkDB();

/*******************************************************************************/

/**
 * Connect and start the database controller
 * From this point, Foundry will attempt to connect to and build missing required
 * tables and data structures or redirect you to the Database Manager
 */
function initDB(){
    global $db_link;

    // looks in db.ini file for either the server host name or,
    // if host includes an extension, the extension
    // if not found or if db.ini is missing, configDB interface will be called
    require_once (SITE_PATH.ADMIN_FOLDER.DB_FOLDER."db_wrapper.class.php");
    if(!defined("DBWRAPLOADED")) die("Database Abstraction Library was not loaded!");
    include_once (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."db-control.class.php");
    if(!defined("DBLOADED")) die("Database Controller Library was not loaded!");
    $dbset = readDBINI();
    $host = str_replace(array(PROTOCOL.'www.', PROTOCOL), '', SERVER);
    $hostext = substr(SERVER, -3);
    if(is_null($host) || $host == '' || defined('PHP_CLI')) $host = PHP_CLI_DBHOST;

    if(!isset($dbset) || count($dbset) == 0){
        configDB(DB_ERR_INIT);
    }else{
        if(isset($dbset[$host])){
            // HOST by domain
            // continue loading site
            if(!isset($dbset[$host]['dbport']) || $dbset[$host]['dbport'] == 0) $dbset[$host]['dbport'] = 3306;
            foreach($dbset[$host] as $key => $val) define(strtoupper($key), $val);
            define ("DBHOSTPORT", ((DBPORT != 3306 && DBPORT > 0) ? DBHOST.":".DBPORT : DBHOST));
            if(DBHOST != '' && DBNAME != '' && DBUSER != ''){
                return;
            }else{
                configDB(DB_ERR_INIT);
            }
        }elseif(isset($dbset[$hostext])){
            // HOST by TLD or TLD-CC
            // continue loading site
            if(!isset($dbset[$host]['dbport']) || $dbset[$host]['dbport'] == 0) $dbset[$host]['dbport'] = 3306;
        	foreach($dbset[$hostext] as $key => $val) define(strtoupper($key), $val);
            define ("DBHOSTPORT", ((DBPORT != 3306) ? DBHOST.":".DBPORT : DBHOST));
            if(DBHOST != '' && DBNAME != '' && DBUSER != ''){
                return;
            }else{
                configDB(DB_ERR_INIT);
            }
        }elseif(isset($dbset['SYS_GO'])){
            configDB(DB_ERR_NOTREG);
        }else{
            configDB(DB_ERR_INIT);
        }
    }
}

/**
 * Builds Database Manager page, triggered by:
 *  - DB initialization call (first run)
 *  - DB connection problem (from anywhere DB is accessed)
 *  - DB connection alteration call (from Settings)
 *  - DB restoration call (from Settings)
 *  - DB backup call (from Settings)
 * @param integer $reason
 * @param array $dbset
 */
function configDB($reason = null, $dbset = null){
    global $db_link;

    if(PHP_CLI)
        die("ERROR: The database settings not configured properly for CLI processes.  Go to ".WEB_URL.DB_CFG_ALIAS."?fn=cli&fc=configDB&fp=100 your browser to invoke the Database Manager.\n\n");

    include_once (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."getvars.php");
    include_once (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."geterrormsgs.php");
    $const = get_defined_constants();
    $host = str_replace('www.', '', $_SERVER['HTTP_HOST']);
    date_default_timezone_set('America/Vancouver');

    if(is_null($dbset)){
        $dbset = array( 'db_host' => getIfSet($const['DBHOST']),
                        'db_name' => getIfSet($const['DBNAME']),
                        'db_user' => getIfSet($const['DBUSER']),
                        'db_pass' => getIfSet($const['DBPASS']),
                        'db_port' => getIfSet($const['DBPORT'])
                    );
    }

    $jumpto = (($reason >= DB_ACT_CONNECT) ? 'jQuery("#cfgdiv").tabs("select", '.($reason - DB_ACT_CONNECT).');' : '');
    $cfgothertabs = (($reason >= DB_ACT_CONNECT) ? '<li><a href="#db_backup">Backup/Export</a></li><li><a href="#db_restore">Restore/Import</a></a></li>' : '');
    $ajaxurl = $const['VHOST'].$const['ADMIN_FOLDER'].$const['CORE_FOLDER']."ajaxwrapper.php";

    echo <<<EOT
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Admin: Database Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="content-language" content="EN" />
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="-1"/>
    <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.js"></script>
    <link href="{$const['WEB_URL']}{$const['ADMIN_FOLDER']}{$const['JS_FOLDER']}ui/ui.theme.css" rel="stylesheet" type="text/css" media="screen" />
    <script type="text/javascript" language="javascript" src="{$const['WEB_URL']}{$const['ADMIN_FOLDER']}{$const['JS_FOLDER']}ui/ui.script.js"></script>
    <script type="text/javascript" language="javascript" src="{$const['WEB_URL']}{$const['ADMIN_FOLDER']}{$const['JS_FOLDER']}ui/ui.dialog.js"></script>
    <link href="{$const['WEB_URL']}{$const['ADMIN_FOLDER']}{$const['ADM_THEME_FOLDER']}{$const['ADM_THEME']}{$const['ADM_CSS_FOLDER']}master.css" rel="stylesheet" type="text/css" media="screen" />
</head>

<body>
    <script type="text/javascript" language="javascript">
        jQuery(document).ready(function($){
            jQuery(document).delegate("#db_submit", "click", function(e){
                e.preventDefault();
                var ajax = "{$ajaxurl}";
                var d = '{$host}';
                var h = jQuery("#db_host").val();
                var u = jQuery("#db_user").val();
                var p = jQuery("#db_pass").val();
                var n = jQuery("#db_name").val();
                var r = jQuery("#db_port").val();
                jQuery.post(ajax,
                    { op: 'trytoaccessdb', db_used: 0, d: d, h: h, u: u, p: p, n: n, r: r },
                    function(data) {
                        if(data.success){
                            alert(data.rtndata);
                            var lastpage = jQuery("#referer").val();
                            if(lastpage == '' || lastpage == 'undefined') {
                                location.reload();
                            }else{
                                window.location = lastpage;
                            }
                        }else{
                            alert(data.rtndata);
                        }
                    }, "json");
                return false;
            });
            jQuery(document).delegate("#db_back_getdump", "click", function(e){
                var ajax = "{$ajaxurl}";
                var new_dom = jQuery("#db_back_new_domain").val();
                var new_root = jQuery("#db_back_new_root").val();
                var table_treat = jQuery("#db_back_tables_restore_treat").val();
                var record_treat = jQuery("#db_back_records_restore_treat").val();
                var sel_tables = '';
                jQuery(".db_back_tables").each(function(){
                    if(jQuery(this).is(":checked")){
                        sel_tables += ((sel_tables != '') ? ',' : '') + jQuery(this).val();
                    }
                });
                jQuery.post(ajax,
                    { op: 'getdbdump', sel: sel_tables, new_dom: new_dom, new_root: new_root, table_treat:table_treat, record_treat:record_treat },
                    function(data){
                        if(data.success){
                            jQuery("#db_back_out").load('{$const['WEB_URL']}' + data.rtndata, function(){
                                document.location.href = ajax + "?op=downloadfile&t=fdbx&f=" + encodeURIComponent(data.rtndata);
                            });
                        }else{
                            alert('The database file could not be prepared.  Please ensure that the _cache folder is writable.');
                        }
                    }, "json");
                return false;
            });
            jQuery("#db_back_out").focus(function(){
                jQuery(this).select();
            });
            jQuery(".db_back_tables").click(function(){
                if(!jQuery(this).hasClass("all_tables"))
                    jQuery(".all_tables").removeAttr("checked");
                else
                    jQuery(".single_table").removeAttr("checked");
            });
            {$jumpto}
        });
    </script>

    <div id="wrapper">
        <div id="header" style="background-color: #ccccff">
            <h1>{$const['SYS_NAME']} Database Manager</h1>
            <div id="admtoolbar">
                <div id="admdbcfg">
                    <a href="{$const['WEB_URL']}{$const['ADMIN_FOLDER']}"><i class="fa fa-home">&nbsp;</i></a>
                </div>
            </div>
        </div>

        <div id="content-wrapper">
            <div id="contentarea">
                <div id="cfgdiv">
                    <ul>
                        <li><a href="#db_connect">Database Connection</a></li>
                        {$cfgothertabs}
                    </ul>
EOT;

    // config form
    echo PHP_EOL;
    echo <<<EOT
                    <div id="db_connect">
                        <form action="javascript: void(0);" method="post" id="db_conn_form">
EOT;

    echo PHP_EOL;
    $return_to_admin = '';
    if($reason == DB_ERR_INIT){
        echo "              <p>Welcome to {$const['SYS_NAME']}...</p>\n";
        echo "              <p>A robust website content management system, crafted with a fine balance between ease of use to get started quickly, and feature richness to carry your site to the highest levels.</p>";
        echo "              <p>In a moment your website will be ready for use.  However, first you will need to provide some information in order to setup a database for use with this website.</p>\n";
        echo "              <input type=\"hidden\" id=\"referer\" value=\"".WEB_URL.ADMIN_FOLDER."\"/>\n";
    }elseif($reason < DB_ACT_CONNECT){
        if(!function_exists('mysqli_connect')){
            echo "              <p>You have been redirected here because the MySQL/Mysqli extension for PHP is not installed.  Check your php.ini to enable it.</p>\n";
            echo "              <p>Once enabled then you can check the following connection settings.</p>\n";
        }else{
            echo "              <p>You have been redirected to this page because a database has not been properly setup for use with this website, one or more of the connection settings need checking, or the MySQL server is not responding.</p>\n";
            echo "              <p>Let's check the following connection settings and in a moment you'll be back in business.</p>\n";
        }
        echo "              <input type=\"hidden\" id=\"referer\" value=\"".WEB_URL.ADMIN_FOLDER."\"/>\n";
    }else{
        echo "              <p>The {$const['SYS_NAME']} Database Manager Connection area allows for easy modification of database connection parameters.</p>\n";
        echo "              <input type=\"hidden\" id=\"referer\" value=\"".WEB_URL.ADMIN_FOLDER."\"/>\n";
        $return_to_admin = '<p class="cfgblock"><a href="'.WEB_URL.ADMIN_FOLDER.'">... or return to the Admin</a></p>';
    }

    echo <<<EOT
                        <p>If you don't have the database connection information ready, you can return here after obtaining it from your web host or system administrator.</p>
                        <p class="cfgblock">
                            <span class="cfglabel">Domain</span><span class="cfgdata"><input type="text" name="db_domain" id="db_domain" value="{$_SERVER['HTTP_HOST']}" readonly="readonly" style="background-color: #ddd"/></span><span class="cfgnote"></span><br/>
                            <span class="cfglabel">Database Host</span><span class="cfgdata"><input type="text" name="db_host" id="db_host" value="{$dbset['db_host']}"/></span><span class="cfgnote">Try <b>localhost</b>.  Otherwise you will need to contact your provider.</span><br/>
                            <span class="cfglabel">Database Name</span><span class="cfgdata"><input type="text" name="db_name" id="db_name" value="{$dbset['db_name']}"/></span><span class="cfgnote">The name of the database you want to use with this site.</span><br/>
                            <span class="cfglabel">Username</span><span class="cfgdata"><input type="text" name="db_user" id="db_user" value="{$dbset['db_user']}"/></span><span class="cfgnote">The username which grants access to this database.</span><br/>
                            <span class="cfglabel">Password</span><span class="cfgdata"><input type="password" name="db_pass" id="db_pass" value="{$dbset['db_pass']}"/></span><span class="cfgnote">The password for that username.</span><br/>
                            <span class="cfglabel">Port</span><span class="cfgdata"><input type="text" name="db_port" id="db_port" value="{$dbset['db_port']}"/></span><span class="cfgnote">Typically set to <b>3306</b>.</span>
                        </p>
                        <p class="cfgblock">
                            For your convenience, once a successful connection to the database has been made, this configuration will be made available to CLI processes.
                        </p>
                        <p class="cfgblock">
                            When ready, ensure that the <strong>{$const['ADMIN_FOLDER']}{$const['CONFIG_FOLDER']}db.ini</strong> file is writable (CHMOD 777), then click ...
                        </p>
                        <p class="cfgblock">
                            <span class="cfgdata"><input type="submit" name="db_submit" id="db_submit" value="Save Connection Settings"/>
                        </p>
                        {$return_to_admin}
                    </form>
                </div>
EOT;

    if($reason >= DB_ACT_CONNECT){
        // backup/restore form
        echo PHP_EOL;
   		$rs = mysqli_query($db_link, "SHOW TABLES");
        $tables = array("*" => "All Tables");
		while($row = mysqli_fetch_array($rs, MYSQLI_ASSOC)){
            $t = current($row);
            $tables[$t] = $t;
        }
        asort($tables);

        echo <<<EOT
                <div id="db_backup">
                    <h2>Data Backup</h2>
                    <form action="javascript: void(0);" method="post" id="db_back_form">
                        <p>Whether you are planning to move this site to a new domain or need to save the database for added insurance, the Database Backup service is the ideal tool.</p>
                        <p><strong>Step 1</strong></p>
                        <p>Select the table(s) that will be included in the export file.</p>
                        <div style="border: 1px solid gray; width: 600px; height: 220px; min-height: 0px !important; overflow: auto; padding: 5px;">
EOT;
        foreach($tables as $key => $table){
            if($key == '*'){
                echo '<span style="width: 200px; float: left;">';
                echo '<input type="checkbox" class="db_back_tables all_tables" value="'.$key.'" />&nbsp;'.$table.'</span>';
                $clear_style = ' clear: left;';
            }else{
                echo '<span style="width: 200px; float: left;'.$clear_style.'">';
                echo '<input type="checkbox" class="db_back_tables single_table" value="'.$key.'" />&nbsp;'.$table.'</span>';
                $clear_style = '';
            }
        }
        echo <<<EOT
                        </div>
                        <p><strong>Step 2</strong></p>
                        <p>If the site is moving to a new domain, complete the entries below.  This part will process the data by changing references of the current domain to the new domain.</p>
                        <p><em>As a point-of-information, {$const['SYS_NAME']} does not use absolute URLs in its native storage of data, but other custom tables might.</em></p>
                        <p>New domain URL: <input type="text" id="db_back_new_domain" name="db_back_new_domain" value="" /> ie. http://www.domain.com/</p>
                        <p>New root path: <input type="text" id="db_back_new_root" name="db_back_new_root" value="" /> ie. /var/public/html/domain.com/</p>
                        <p><strong>Step 3</strong></p>
                        <p>Decide how the tables and records will be handled during the import/restore process.</p>
                        <p>Tables are <select id="db_back_tables_restore_treat" name="db_back_tables_restore_treat"><option value="created_if_not_exists">Created only if they do not exist</option><option value="dropped_then_created">Recreated from export file</option></select></p>
                        <p>Records are <select id="db_back_records_restore_treat" name="db_back_records_restore_treat"><option value="inserted">Inserted</option><option value="truncated_then_inserted">Truncated, then inserted</option></select></p>
                        <p><input type="button" id="db_back_getdump" value="Get Export File" /></p>
                        <p><br/><strong>File Contents</strong></p>
                        <p>Click anywhere within the text area and press CTRL+C (Windows/Unix) or CMD+C (Mac) to copy.</p>
                        <textarea id="db_back_out" style="width: 70%; height: 400px; max-width: 70%; background-color: #eee;"></textarea>
                    </form>
                </div>
                <div id="db_restore">
                    <h2>Data Restore</h2>
                    <form action="javascript: void(0);" method="post" id="db_rest_form">
                        <p>With the Data Restore tool, a previously backed up database can be restored to the current or a new domain.  </p>
                    </form>
                </div>
EOT;
    }

    echo PHP_EOL;
    echo "  </div>\n";

    include (SITE_PATH.ADMIN_FOLDER.ADM_THEME_FOLDER.ADM_THEME."footer.php");
    exit;
}

/**
 * Return array of all required system tables
 * @return array
 */
function getRequiredTables(){
    return array(SETTINGS_TABLE,
                 ACCOUNTS_TABLE,
                 ATTRIBUTES_TABLE,
                 EVENT_LOG_TABLE,
                 FIELDS_TABLE,
                 FIELDS_OBJECTS_TABLE,
                 FIELDS_DATA_TABLE,
                 MEDIA_TABLE,
                 MEDIA_FORMATS_TABLE,
                 MEDIA_FORMATS_ACTIONS_TABLE,
                 MENUS_TABLE,
                 PAGE_TYPES_TABLE,
                 PAGES_TABLE,
                 PLUGINS_TABLE,
                 REGISTER_TABLE,
                 SESSION_TABLE,
                 TAXONOMIES_TABLE,
                 TERMS_TABLE,
                 TERMS_DATA_TABLE,
                 THEMES_TABLE,
            );
}

/**
 * Verifies existence of critical database structures
 */
function checkDB($db_link = false){
    global $db_link;

    if(empty($db_link) || $db_link === false){
        $db_link = new mysqli(DBHOSTPORT, DBUSER, DBPASS, DBNAME);
        if($db_link->connect_errno > 0) configDB(DB_ERR_SVRCONN);
    	if(!defined("DB_VER")) define("DB_VER", floatval(mysqli_get_client_version()));
    }

	// locate basic required tables
	if($db_link) {
		// check that all required tables are present
        $reqd_tables = checkRequiredTables(true);

        // (re)create any missing required tables
        $sql = "";
        $sql2= "";
		foreach($reqd_tables as $table){
			switch($table){
				case SETTINGS_TABLE:
					$sql = "CREATE TABLE `".SETTINGS_TABLE."` (
					  `name` varchar(50) NOT NULL DEFAULT '',
					  `value` text DEFAULT NULL,
					  `type` char(3) DEFAULT 'str',
					  PRIMARY KEY (`name`)
					)";
					break;
				case ACCOUNTS_TABLE:
					$sql = "CREATE TABLE `".ACCOUNTS_TABLE."` (
					  `id` bigint(11) unsigned NOT NULL auto_increment,
					  `username` varchar(50) DEFAULT NULL,
					  `password` varchar(255) DEFAULT NULL,
					  `phash` varchar(255) DEFAULT NULL,
					  `pcle` varchar(255) DEFAULT NULL,
                      `hint` varchar(255) DEFAULT NULL,
					  `level` int(2) DEFAULT '0',
					  `email` varchar(255) DEFAULT NULL,
					  `fails` int(2) DEFAULT '0',
					  `firstname` varchar(20) DEFAULT NULL,
					  `lastname` varchar(20) DEFAULT NULL,
					  `twitter_link` varchar(255) DEFAULT NULL,
					  `google_plus_link` varchar(255) DEFAULT NULL,
					  `facebook_link` varchar(255) DEFAULT NULL,
                      `openid_identity` text DEFAULT NULL,
					  `image` varchar(255) DEFAULT NULL,
					  `thumb` varchar(255) DEFAULT NULL,
					  `avatar` varchar(255) DEFAULT NULL,
					  `activated` tinyint(1) DEFAULT '1',
					  `blocked` tinyint(1) DEFAULT '0',
					  `blocked_time` datetime DEFAULT '0000-00-00 00:00:00',
                      `timezone` varchar(255) DEFAULT NULL,
                      `sites` varchar(255) DEFAULT NULL,
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";
					break;
                case ATTRIBUTES_TABLE:
                    $sql = "CREATE TABLE `".ATTRIBUTES_TABLE."` (
                      `attribute_id` int(11) unsigned NOT NULL auto_increment,
                      `attribute_class` varchar(255) DEFAULT 'data',
                      `data_type` varchar(255) DEFAULT '0',
                      `data_id` int(11) DEFAULT '0',
                      `key` varchar(255) DEFAULT NULL,
                      `alias` varchar(255) DEFAULT NULL,
                      `path` varchar(255) DEFAULT NULL,
                      `parent_id` int(11) NOT NULL DEFAULT '0',
                      `metatitle` varchar(255) DEFAULT NULL,
                      `metadescr` varchar(255) DEFAULT NULL,
                      `metakeywords` varchar(512) DEFAULT NULL,
                      `viewable_from` date NULL DEFAULT '0000-00-00',
                      `viewable_to` date NULL DEFAULT '0000-00-00',
                      `language` varchar(50) NOT NULL DEFAULT 'english',
                      `sitemap_state` varchar(10) NOT NULL DEFAULT '1:0.5',
                      `locked` tinyint(1) NOT NULL DEFAULT '1',
                      `homepage` tinyint(1) NOT NULL DEFAULT '0',
                      `searchable` tinyint(1) NOT NULL DEFAULT '1',
                      `displayed` tinyint(1) NOT NULL DEFAULT '1',
                      `protected` tinyint(1) NOT NULL DEFAULT '0',
                      `password` varchar(255) NULL DEFAULT '',
                      `draft` tinyint(1) NOT NULL DEFAULT '0',
                      `deleted` tinyint(1) NOT NULL DEFAULT '0',
                      `published` tinyint(1) NOT NULL DEFAULT '1',
                      `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `date_updated` timestamp NULL DEFAULT '0000-00-00 00:00:00',
                      `date_published` timestamp NULL DEFAULT '0000-00-00 00:00:00',
                      `user_editing` int(11) NOT NULL DEFAULT '0',
                      `user_id` int(11) DEFAULT '0',
                      `plugins_incl` text DEFAULT NULL,
                      `sort_order` int(11) DEFAULT '0',
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY  (`attribute_id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case EVENT_LOG_TABLE:
                    $sql = "CREATE TABLE `".EVENT_LOG_TABLE."` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `date` datetime NULL DEFAULT '0000-00-00 00:00:00',
                      `source` varchar(50) NOT NULL,
                      `category` varchar(50) NULL,
                      `type` enum('information','notice','warning','critical') NOT NULL DEFAULT 'information',
                      `user_id` int(11) NOT NULL DEFAULT '0',
                      `ip_address` varchar(255) NULL,
                      `notes` text DEFAULT NULL,
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case FIELDS_TABLE:
                    $sql = "CREATE TABLE `".FIELDS_TABLE."` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `name` varchar(50) NOT NULL DEFAULT '',
                      `key` varchar(50) NOT NULL DEFAULT '',
                      `object` varchar(50) DEFAULT NULL,
                      `label` varchar(255) DEFAULT NULL,
                      `fld_id` varchar(50) DEFAULT NULL,
                      `fld_class` varchar(255) DEFAULT NULL,
                      `label_id` varchar(50) DEFAULT NULL,
                      `label_class` varchar(255) DEFAULT NULL,
                      `container_id` int(11) NOT NULL DEFAULT '0',
                      `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
                      `label_format` varchar(255) DEFAULT NULL,
                      `help` varchar(255) DEFAULT NULL,
                      `size` varchar(50) DEFAULT NULL,
                      `value` text DEFAULT NULL,
                      `list_values` text DEFAULT NULL,
                      `default_value` varchar(255) DEFAULT NULL,
                      `conditions` text DEFAULT NULL,
                      `style` varchar(255) DEFAULT NULL,
                      `wrapper_text` varchar(255) DEFAULT NULL,
                      `themes` varchar(255) DEFAULT NULL,
                      `sort_order` int(5) NOT NULL DEFAULT '0',
                      `active` tinyint(1) DEFAULT '1',
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case FIELDS_OBJECTS_TABLE:
                    $sql = "CREATE TABLE `".FIELDS_OBJECTS_TABLE."` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `name` varchar(50) NOT NULL DEFAULT '',
                      `key` varchar(50) NOT NULL DEFAULT '',
                      `html_object` varchar(50) DEFAULT NULL,
                      `render_func` varchar(255) DEFAULT NULL,
                      `source_type` enum('system', 'custom') DEFAULT NULL,
                      `active` tinyint(1) DEFAULT '1',
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case FIELDS_DATA_TABLE:
                    $sql = "CREATE TABLE `".FIELDS_DATA_TABLE."` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `data_type` varchar(255) NOT NULL DEFAULT '',
                      `data_id` int(11) NOT NULL DEFAULT '0',
                      `field_id` int(11) NOT NULL DEFAULT '0',
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case MEDIA_TABLE:
                    $sql = "CREATE TABLE `".MEDIA_TABLE."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `name` varchar(255) NOT NULL,
                      `file_name` varchar(255) NOT NULL,
                      `alias` varchar(255) NOT NULL,
                      `file_path` varchar(255) NOT NULL,
                      `file_type` varchar(50) DEFAULT null,
                      `file_size` int(11) NOT NULL DEFAULT '0',
                      `width` int(5) NOT NULL DEFAULT '0',
                      `height` int(5) NOT NULL DEFAULT '0',
                      `description` varchar(255) NOT NULL,
                      `caption` varchar(255) NOT NULL,
                      `alt_text` varchar(50) NULL,
                      `data_type` varchar(50) DEFAULT null,
                      `data_id` int(11) NOT NULL DEFAULT '0',
                      `active` tinyint(1) NOT NULL DEFAULT '1',
                      `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `date_updated` timestamp NULL DEFAULT '0000-00-00 00:00:00',
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case MEDIA_FORMATS_TABLE:
                    $sql = "CREATE TABLE `".MEDIA_FORMATS_TABLE."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `name` varchar(255) NOT NULL,
                      `code` varchar(255) NOT NULL,
                      `source_type` enum('plugin', 'theme', 'system', 'custom', 'backup') DEFAULT 'system',
                      `source_code` varchar(255) DEFAULT null,
                      `ref_id` int(11) NOT NULL DEFAULT '0',
                      `min` varchar(255) NOT NULL,
                      `max` varchar(255) NOT NULL,
                      `filelimit` varchar(50) NULL,
                      `extensions` varchar(255) DEFAULT NULL,
                      `actions` text DEFAULT null,
                      `active` tinyint(1) NOT NULL DEFAULT '1',
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case MEDIA_FORMATS_ACTIONS_TABLE:
                    $sql = "CREATE TABLE `".MEDIA_FORMATS_ACTIONS_TABLE."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `name` varchar(255) NOT NULL,
                      `code` varchar(255) NOT NULL,
                      `description` varchar(255) NOT NULL,
                      `options` varchar(255) NOT NULL,
                      `active` tinyint(1) NOT NULL DEFAULT '1',
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case MENUS_TABLE:
                    $sql = "CREATE TABLE `".MENUS_TABLE."` (
                      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                      `in_admin` tinyint(1) NOT NULL DEFAULT '1',
                      `menu_class` varchar(20) NOT NULL,
                      `menu_bar_id` int(11) NOT NULL DEFAULT '0',
                      `parent_id` int(11) NOT NULL DEFAULT '0',
                      `title` varchar(50) NOT NULL,
                      `key` varchar(50) NOT NULL,
                      `table` varchar(255) NOT NULL,
                      `alias` varchar(255) DEFAULT NULL,
                      `descr` varchar(255) DEFAULT NULL,
                      `target` varchar(255) NOT NULL,
                      `targettype` varchar(50) DEFAULT NULL,
                      `taxonomy_id` int(11) NOT NULL DEFAULT '0',
                      `window` varchar(255) NOT NULL DEFAULT '',
                      `restricted` tinyint(1) NOT NULL DEFAULT '0',
                      `custom` tinyint(1) NOT NULL DEFAULT '0',
                      `active` tinyint(1) NOT NULL DEFAULT '1',
                      `sort_order` int(11) NOT NULL DEFAULT '0',
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
				case PAGES_TABLE:
					$sql = "CREATE TABLE `".PAGES_TABLE."` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `pagename` varchar(50) NOT NULL,
					  `pagetitle` varchar(50) NOT NULL,
					  `description` varchar(255) NOT NULL DEFAULT '',
                      `content` text,
                      `link` varchar(255) DEFAULT NULL,
                      `layout` varchar(255) DEFAULT NULL,
					  `pagetypeid` int(11) NOT NULL DEFAULT '1',
                      `sites` varchar(255) DEFAULT NULL,
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";
					break;
				case PAGE_TYPES_TABLE:
					$sql = "CREATE TABLE `".PAGE_TYPES_TABLE."` (
					  `id` int(11) unsigned NOT NULL auto_increment,
					  `type` varchar(50) NOT NULL,
					  `description` varchar(50) NOT NULL,
					  `locked` tinyint(1) NOT NULL,
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";
					break;
                case PLUGINS_TABLE:
                    $sql = "CREATE TABLE `".PLUGINS_TABLE."` (
                      `id` int(5) NOT NULL AUTO_INCREMENT,
                      `repoid` varchar(50) DEFAULT NULL,
                      `name` varchar(50) DEFAULT NULL,
                      `ver` varchar(10) DEFAULT NULL,
                      `sysver` float(4,2) DEFAULT '3.00',
                      `author` varchar(255) DEFAULT NULL,
                      `created` timestamp NULL DEFAULT '0000-00-00 00:00:00',
                      `revised` timestamp NULL DEFAULT '0000-00-00 00:00:00',
                      `next_revision_ver` varchar(10) DEFAULT NULL,
                      `descr` varchar(255) DEFAULT NULL,
                      `license` varchar(50) DEFAULT 'free',
                      `website` varchar(255) DEFAULT NULL,
                      `usedin` varchar(50) DEFAULT 'both',
                      `folder` varchar(255) DEFAULT NULL,
                      `depends` varchar(255) DEFAULT NULL,
                      `parent` varchar(255) DEFAULT NULL,
                      `parent_type` enum('plugin','extension','framework','theme','themeset','package') DEFAULT 'plugin',
                      `group` varchar(255) DEFAULT NULL,
                      `incl` varchar(50) DEFAULT NULL,
                      `initfile` varchar(255) DEFAULT NULL,
                      `initcallers` text DEFAULT NULL,
                      `initcontexts` text DEFAULT NULL,
                      `headerfunc` varchar(255) DEFAULT NULL,
                      `settingsfunc` varchar(255) DEFAULT NULL,
                      `inline_settings` varchar(255) DEFAULT NULL,
                      `custom_settings` text DEFAULT NULL,
                      `nodisable` tinyint(1) DEFAULT '0',
                      `nodelete` tinyint(1) DEFAULT '0',
                      `builtin` tinyint(1) DEFAULT '0',
                      `autoincl` tinyint(1) DEFAULT '0',
                      `active` tinyint(1) DEFAULT '1',
                      `is_framework` tinyint(1) DEFAULT '0',
                      `is_editor` tinyint(1) DEFAULT '0',
                      `is_deleted` tinyint(1) DEFAULT '0',
                      `errors` text DEFAULT NULL,
                      `error_code` int(3) DEFAULT '0',
                      `updflag` varchar(5) DEFAULT NULL,
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
                    break;
                case REGISTER_TABLE:
                    $sql = "CREATE TABLE `".REGISTER_TABLE."` (
                      `id` int(5) NOT NULL AUTO_INCREMENT,
                      `type` varchar(255) NOT NULL,
                      `alias` varchar(255) NOT NULL,
                      `db_table` varchar(50) DEFAULT NULL,
                      `db_child_table` varchar(50) DEFAULT NULL,
                      `function` varchar(255) DEFAULT NULL,
                      `parameters` text DEFAULT NULL,
                      `conditions` text DEFAULT NULL,
                      `actions` text DEFAULT NULL,
                      `scheduled` int(20) DEFAULT '0',
                      `priority` int(2) DEFAULT '50',
                      `user_id` int(11) DEFAULT '0',
                      `active` tinyint(1) NOT NULL DEFAULT '1',
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
                    break;
				case SESSION_TABLE:
					$sql = "CREATE TABLE `".SESSION_TABLE."` (
					  `id` int(5) NOT NULL auto_increment,
					  `user_id` int(11) DEFAULT '0',
					  `ip_hash` varchar(255) DEFAULT NULL,
					  `username` varchar(255) DEFAULT NULL,
					  `section` varchar(255) DEFAULT NULL,
					  `logged_in_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
					  `logged_in` tinyint(1) DEFAULT '0',
                      `sites` varchar(255) DEFAULT NULL,
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8";
					break;
                case TERMS_TABLE:
                    $sql = "CREATE TABLE `".TERMS_TABLE."` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `taxonomy_id` int(11) NOT NULL DEFAULT '0',
                      `parent_id` int(11) NOT NULL DEFAULT '0',
                      `code` varchar(255) NOT NULL,
                      `name` varchar(255) NOT NULL,
                      `description` varchar(255) DEFAULT NULL,
                      `active` tinyint(1) NOT NULL DEFAULT '1',
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case TERMS_DATA_TABLE:
                    $sql = "CREATE TABLE `".TERMS_DATA_TABLE."` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `term_id` int(11) NOT NULL DEFAULT '0',
                      `data_id` int(11) NOT NULL DEFAULT '0',
                      `sites` varchar(255) DEFAULT NULL,
                     PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case TAXONOMIES_TABLE:
                    $sql = "CREATE TABLE `".TAXONOMIES_TABLE."` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `code` varchar(255) NOT NULL,
                      `name` varchar(255) NOT NULL,
                      `parent_id` int(11) NOT NULL DEFAULT '0',
                      `type` enum('tag','faq','link','category') NOT NULL DEFAULT 'category',
                      `data_alias` int(11) NOT NULL DEFAULT '0',
                      `active` tinyint(1) NOT NULL DEFAULT '1',
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
                case THEMES_TABLE:
                    $sql = "CREATE TABLE `".THEMES_TABLE."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `type` varchar(10) NOT NULL,
                      `name` varchar(50) NOT NULL,
                      `ver` varchar(10) DEFAULT NULL,
                      `sysver` float(4,2) DEFAULT '3.00',
                      `folder` varchar(255) NOT NULL,
                      `descr` varchar(255) NOT NULL,
                      `author` varchar(50) NULL,
                      `website` varchar(255) DEFAULT NULL,
                      `custom_settings` text DEFAULT null,
                      `settingsfunc` varchar(255) DEFAULT NULL,
                      `headerfunc` varchar(255) DEFAULT NULL,
                      `stylefiles` varchar(255) DEFAULT NULL,
                      `scriptfiles` varchar(255) DEFAULT NULL,
                      `menuslots` varchar(255) DEFAULT NULL,
                      `locations` text DEFAULT NULL,
                      `layoutfolder` varchar(50) DEFAULT NULL,
                      `layouts` text DEFAULT NULL,
                      `errors` text DEFAULT NULL,
                      `error_code` int(3) DEFAULT '0',
                      `active` tinyint(1) NOT NULL DEFAULT '0',
                      `parent` varchar(50) DEFAULT NULL,
                      `updflag` varchar(5) DEFAULT NULL,
                      `sites` varchar(255) DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    break;
			}
			if($sql != "") {
				mysqli_query($db_link, $sql) or die("Error creating table '$table': ".mysqli_error($db_link)."<br /><br />SQL: $sql");
			}
			$sql = "";
			$sql2 = "";
		}

        checkRequiredTableDefaultRecords(false);
        // getTableCodes();
	}
}

/**
 * Return the structures of the existing required tables as coded string sets
 * @return array
 */
function getTableCodes(){
    global $db_link;

	$tables = mysqli_query($db_link, "SHOW TABLES");
	$retn_tables = array();
	$reqd_tables = getRequiredTables();
	while($row = mysqli_fetch_array($tables, MYSQLI_ASSOC)){
        $table = $row['Tables_in_'.DBNAME];
		$key = array_search($table, $reqd_tables);
		if($key !== false){
			$sql = "SHOW COLUMNS FROM `{$table}`";
			$columns = mysqli_query($db_link, $sql);
            print "\t\t\"".$table."\" => Array(\n";
			while($col = mysqli_fetch_assoc($columns)){
				$data = array($col['Type'], $col['Key'], $col['Default'], $col['Extra'], $col['Null']);
                $retn_tables[$table][$col['Field']] = urlencode(json_encode($data));
                print "\t\t\t\"".$col['Field']."\" => \"".urlencode(json_encode($data))."\",\n";
			}
            print "\t\t),\n";
		}
	}

	exit;
}

/**
 * Validates existence of required tables
 * @return boolean
 */
function checkRequiredTables($checkreqdtablestructs = true){
    global $db_link;

    // check that all required tables are present
    $reqd_tables = getRequiredTables();

    $tables = mysqli_query($db_link, "SHOW TABLES");
    while($row = mysqli_fetch_assoc($tables)){
        $table = current($row);
        $key = array_search($table, $reqd_tables);
        if($key !== false) {
            // table is one of the required tables
            checkRequiredTablesStructures($table);
            unset($reqd_tables[$key]);
        }
    }
    return $reqd_tables;
}

/**
 * Insert default rows into required tables
 * @param boolean $reset                If set to true will clear all default rows from required tables
 */
function checkRequiredTableDefaultRecords($reset = false){
    global $db_link;

    $reqd_tables = getRequiredTables();
    $sql = "";
    foreach($reqd_tables as $table){
        $numrowsql = "SELECT * FROM `".$table."`";
        $rowsneeded = 1;
        $sql = "";
        switch($table){
            case SETTINGS_TABLE:
                $sql = "INSERT INTO `".SETTINGS_TABLE."` (`name`, `value`, `type`) VALUES
                ('DEF_METAKEYWORDS', 'Default Keywords', 'str'),
                ('DEF_METADESCRIPTION', 'Default Description', 'str'),
                ('DEF_METATITLE', 'Default Title', 'str'),
                ('BUSINESS', '', 'str'),
                ('SITE_NAME', '', 'str'),
                ('OWNER_EMAIL', '', 'str'),
                ('ADMIN_EMAIL', '', 'str'),
                ('BUS_ADDRESS', '', 'str'),
                ('BUS_PHONE', '', 'str'),
                ('BUS_FAX', '', 'str'),
                ('TIMEZONE', 'America/Vancouver', 'str'),
                ('IMG_UPLOAD_FOLDER', 'images/', 'str'),
                ('THM_UPLOAD_FOLDER', 'images/', 'str'),
                ('FILES_UPLOAD_FOLDER', 'files/', 'str'),
                ('CKE_CSS_COLORS', '', 'upd'),
                ('EMAIL_CONFIRM', null, 'str'),
                ('EMAIL_NOTIFY', null, 'str'),
                ('IMG_LOGIN_LOGO', '', 'str'),
                ('IMG_LOGIN_LOGOS', '', 'str'),
                ('THEME', 'genesis_4', 'str'),
                ('THEMES_ENABLED', '0', 'int'),
                ('JQUERY_FALLBACK_VER', '1.7.1', 'str'),
                ('JQUERYUI_FALLBACK_VER', '1.8.18', 'str'),
                ('CMS_EDITOR', 'ckeditor', 'str'),
                ('ALLOW_DEBUGGING', '1', 'int'),
                ('ERROR_LOG_TYPE', '3', 'int'),
                ('SITEOFFLINE', '0', 'int'),
                ('SITEOFFLINE_MSG', '', 'str'),
                ('FACEBOOKLOGIN', '0', 'int'),
                ('SITE_BYLINE', '', 'str'),
                ('CHARSET', 'UTF-8', 'str'),
                ('CUSTOM_USER_TYPES', '', 'str'),
                ('ALLOWANCES', '', 'str'),
                ('DB_TABLE_PREFIX', 'data_', 'str'),
                ('DB_SYS_TABLE_PREFIX', '', 'str'),
                ('PHP_DATE_FORMAT', 'Y-m-d', 'str'),
                ('FTPUSER', '', 'str'),
                ('FTPPASS', '', 'str'),
                ('FTPHOST', '', 'str'),
                ('FTPCONN', 'ftp', 'str'),
                ('DEF_IMAGE_PROCESSOR', 'gdlib', 'str'),
                ('SMTP_HOST', '', 'str'),
                ('SMTP_USERNAME', '', 'str'),
                ('SMTP_PASSWORD', '', 'str'),
                ('SMTP_PORT', '25', 'int'),
                ('SMTP_SECURE_MODE', '', 'str'),
                ('CACHE_CSS', '0', 'int'),
                ('CACHE_JS', '0', 'int'),
                ('USER_ACCOUNT_MSG_NEW_ACCT', '', 'str'),
                ('USER_ACCOUNT_MSG_NEW_APPR', '', 'str'),
                ('USER_ACCOUNT_MSG_ACCT_ACT', '', 'str'),
                ('USER_ACCOUNT_MSG_ACCT_BAN', '', 'str'),
                ('USER_ACCOUNT_MSG_ACCT_CCL', '', 'str'),
                ('USER_ACCOUNT_MSG_PASS_REC', '', 'str'),
                ('MAX_LOGIN_TRIES', '3', 'int'),
                ('CONTEXTS', 'targettype,datatype,targetaction,url,usertype,allowance,queryvar', 'str')
                ";
                break;
            case ACCOUNTS_TABLE:
                // $pwd = '$2a$08$sEzt80EzlGP98XTJr7302OaIcwLoPg3nc3OXYfQOZdhuioCzQJwki';
                // $sql = "INSERT INTO `".ACCOUNTS_TABLE."` (`username`, `password`, `phash`, `pcle`, `email`, `fails`, `activated`, `blocked`, `blocked_time`, `level`) VALUES
                // ('admin', '$pwd', '', '5f4dcc3b5aa765d61d8327deb882cf99', null, '', '1', '0', null, 0)";
                break;
            case FIELDS_OBJECTS_TABLE:
                $sql = "INSERT INTO `".FIELDS_OBJECTS_TABLE."` (`name`, `key`, `html_object`, `render_func`, `source_type`) VALUES
                ('Label', 'label', 'label', 'showLabel', 'system'),
                ('Text', 'text', 'text input', 'showTextField', 'system'),
                ('Multiline Text', 'multiline-text', 'textarea', 'showTextareaField', 'system'),
                ('Text Editor', 'text-editor', 'textarea', 'showHTMLEditorField', 'system'),
                ('Password', 'password', 'text input', 'showPasswordField', 'system'),
                ('Button', 'button', 'button input', 'showButtonField', 'system'),
                ('Image', 'image', 'image file', 'showImageField', 'system'),
                ('File', 'file', 'file', 'showFileField', 'system'),
                ('Hidden Text', 'hidden-text', 'hidden', 'showHiddenField', 'system'),
                ('Hidden Label', 'hidden-label', 'label', 'showHiddenLabelField', 'system'),
                ('Checkbox', 'checkbox', 'checkbox input', 'showCheckbox', 'system'),
                ('Checkbox List', 'checkbox-list', 'checkbox input', 'showCheckboxList', 'system'),
                ('Radio Button List', 'radio-button-list', 'radio input', 'showRadioList', 'system'),
                ('Dropdown Menu', 'dropdown-menu', 'selectmenu', 'showMenu', 'system'),
                ('Map', 'map', 'map', 'showMapBox', 'system'),
                ('Address Fields', 'address-fields', 'text input', 'showAddressBox', 'system')";
                $numrowsql .= " WHERE `key` IN ('label', 'text', 'multiline-text', 'text-editor', 'password', 'button', 'image', 'file', 'hidden-text', 'hidden-label', 'checkbox', 'checkbox-list', 'radio-button-list', 'dropdown-menu', 'map', 'address-fields')";
                $rowsneeded = 17;
                break;
            case PAGE_TYPES_TABLE:
                $sql = "INSERT INTO `".PAGE_TYPES_TABLE."` (`type`, `description`, `locked`) VALUES
                ('editor', 'Page is constructed with a WYSIWYG editor', '0'),
                ('data', 'Page provides access to database-driven content', '1'),
                ('form', 'Page includes a simple form', '1')";
                $numrowsql .= " WHERE `type` IN ('editor', 'data', 'form')";
                $rowsneeded = 3;
               break;
            case MEDIA_FORMATS_TABLE:
                $sql = "INSERT INTO `".MEDIA_FORMATS_TABLE."` (`name`, `code`, `source_type`, `min`, `max`, `filelimit`, `extensions`, `actions`) VALUES
                ('Thumbnail', 'thumbnail', 'system', '".json_encode(array("width" => 0, "height" => 0))."', '".json_encode(array("width" => '600px', "height" => '600px'))."', '50KB', 'png, jpg, jpeg, gif', '".json_encode(array("scaledown" => array("width" => "100px", "height" => "100px")))."'),
                ('Medium', 'medium', 'system', '".json_encode(array("width" => 0, "height" => 0))."', '".json_encode(array("width" => '600px', "height" => '600px'))."', '50KB', 'png, jpg, jpeg, gif', '".json_encode(array("scaledown" => array("width" => "300px", "height" => "300px")))."'),
                ('Large', 'large', 'system', '".json_encode(array("width" => 0, "height" => 0))."', '".json_encode(array("width" => '1280px', "height" => '1024px'))."', '100KB', 'png, jpg, jpeg, gif', '".json_encode(array("scaledown" => array("width" => "800px", "height" => "600px")))."'),
                ('Organizer', 'organizer', 'system', '".json_encode(array("width" => 0, "height" => 0))."', '".json_encode(array("width" => '600px', "height" => '600px'))."', '50KB', 'png, jpg, jpeg, gif', '".json_encode(array("scaledown" => array("width" => "100px", "height" => "100px")))."')";
                $numrowsql .= " WHERE `code` IN ('thumbnail', 'medium', 'large', 'organizer')";
                $rowsneeded = 4;
                break;
            case MEDIA_FORMATS_ACTIONS_TABLE:
                $sql = "INSERT INTO `".MEDIA_FORMATS_ACTIONS_TABLE."` (`name`, `code`, `description`, `options`) VALUES
                ('Scale', 'scale', 'Either enlargen or shrink an image', '".json_encode(array("width" => "text", "height" => "text"))."'),
                ('Scaledown', 'scaledown', 'Only shrink an image','".json_encode(array("width" => "text", "height" => "text"))."'),
                ('Crop', 'crop', 'Eliminate all but a portion of an image','".json_encode(array("width" => "text", "height" => "text", "focal" => "menu:top-left,top,top-right,left,center,right,bottom-left,bottom,bottom-right"))."'),
                ('Fade', 'fade', 'Reduce the opacity of an image','".json_encode(array("level" => "text"))."'),
                ('Sharpen', 'sharpen', 'Enhance contrast edge of an image','".json_encode(array("level" => "text"))."'),
                ('Gauss', 'gauss', '','".json_encode(array("level" => "text"))."'),
                ('Blur', 'blur', 'Reduce the contrast edge of an image','".json_encode(array("level" => "text"))."'),
                ('Rotate', 'rotate', '','".json_encode(array("angle" => "text"))."')";
                $numrowsql .= " WHERE `code` IN ('scale', 'scaledown', 'crop', 'fade', 'sharpen', 'gauss', 'blur', 'rotate')";
                $rowsneeded = 8;
                break;
            default:
                $rowsneeded = 0;
                break;
        }

        if($sql != "") {
            $rows = mysqli_query($db_link, $numrowsql);
            $count = mysqli_num_rows($rows);
            if($count < $rowsneeded || $reset){
                // one or all default rows are missing
                if($rowsneeded > 1 || $reset){
                    // purge a partial set of default rows
                    $numrowsql = str_replace("SELECT * ", "DELETE ", $numrowsql);
                    mysqli_query($db_link, $numrowsql);
                }
                // reinsert rows
                mysqli_query($db_link, $sql) or die("Error inserting initial values in '$table': ".mysqli_error($db_link)."<br /><br />SQL: $sql");
            }
        }
    }
}

/**
 * Validates a required table and attempts to reset corrupt tables to standard structuring if not structured correctly
 * @param string $tablekey          Subject table to validate
 */
function checkRequiredTablesStructures($tablekey){
    global $db_link;

    // %22 = "
    // %28 = =
    // %29 =
    // %5B = [
    // %5D = ]
    // %20 = space
	$hash_tables = array(
        ACCOUNTS_TABLE => Array(
            "id" => "%5B%22bigint%2811%29+unsigned%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "username" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "password" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "phash" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "pcle" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "hint" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "level" => "%5B%22int%282%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "email" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "fails" => "%5B%22int%282%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "firstname" => "%5B%22varchar%2820%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "lastname" => "%5B%22varchar%2820%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "twitter_link" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "google_plus_link" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "facebook_link" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "openid_identity" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "image" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "thumb" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "avatar" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "activated" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22YES%22%5D",
            "blocked" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "blocked_time" => "%5B%22datetime%22%2C%22%22%2C%220000-00-00+00%3A00%3A00%22%2C%22%22%2C%22YES%22%5D",
            "timezone" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        ATTRIBUTES_TABLE => Array(
            "attribute_id" => "%5B%22int%2811%29+unsigned%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "attribute_class" => "%5B%22varchar%28255%29%22%2C%22%22%2C%22data%22%2C%22%22%2C%22YES%22%5D",
            "data_type" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "data_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "key" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "alias" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "path" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "parent_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "metatitle" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "metadescr" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "metakeywords" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "viewable_from" => "%5B%22date%22%2C%22%22%2C%220000-00-00%22%2C%22%22%2C%22YES%22%5D",
            "viewable_to" => "%5B%22date%22%2C%22%22%2C%220000-00-00%22%2C%22%22%2C%22YES%22%5D",
            "language" => "%5B%22varchar%2810%29%22%2C%22%22%2C%22us-en%22%2C%22%22%2C%22YES%22%5D",
            "sitemap_state" => "%5B%22varchar%2810%29%22%2C%22%22%2C%221%3A0.5%22%2C%22%22%2C%22YES%22%5D",
            "locked" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "homepage" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "searchable" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22YES%22%5D",
            "displayed" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22NO%22%5D",
            "protected" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "password" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "draft" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "deleted" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "published" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22YES%22%5D",
            "date_created" => "%5B%22timestamp%22%2C%22%22%2C%220000-00-00+00%3A00%3A00%22%2C%22%22%2C%22NO%22%5D",
            "date_updated" => "%5B%22timestamp%22%2C%22%22%2C%220000-00-00+00%3A00%3A00%22%2C%22on+update+CURRENT_TIMESTAMP%22%2C%22NO%22%5D",
            "date_published" => "%5B%22timestamp%22%2C%22%22%2C%220000-00-00+00%3A00%3A00%22%2C%22%22%2C%22YES%22%5D",
            "user_editing" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "user_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "plugins_incl" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "sort_order" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        EVENT_LOG_TABLE => Array(
            "id" => "%5B%22int%2811%29%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "date" => "%5B%22datetime%22%2C%22%22%2C%220000-00-00+00%3A00%3A00%22%2C%22%22%2C%22YES%22%5D",
            "source" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "category" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "type" => "%5B%22enum%28%27information%27%2C%27critical%27%2C%27notice%27%2C%27warning%27%29%22%2C%22%22%2C%22information%22%2C%22%22%2C%22YES%22%5D",
            "user_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "ip_address" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "notes" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        FIELDS_TABLE => Array(
            "id" => "%5B%22int%2811%29%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "name" => "%5B%22varchar%2850%29%22%2C%22%22%2C%22%22%2C%22%22%2C%22NO%22%5D",
            "key" => "%5B%22varchar%2850%29%22%2C%22%22%2C%22%22%2C%22%22%2C%22NO%22%5D",
            "object" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "label" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "fld_id" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "fld_class" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "label_id" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "label_class" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "container_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "is_hidden" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "label_format" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "help" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "size" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "value" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "list_values" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "default_value" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "conditions" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "style" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "wrapper_text" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "themes" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "sort_order" => "%5B%22int%285%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        FIELDS_DATA_TABLE => Array(
            "id" => "%5B%22int%2811%29%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "data_type" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "data_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "field_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        FIELDS_OBJECTS_TABLE => Array(
            "id" => "%5B%22int%2811%29%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "name" => "%5B%22varchar%2850%29%22%2C%22%22%2C%22%22%2C%22%22%2C%22NO%22%5D",
            "key" => "%5B%22varchar%2850%29%22%2C%22%22%2C%22%22%2C%22%22%2C%22NO%22%5D",
            "html_object" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "render_func" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "source_type" => "%5B%22enum%28%27system%27%2C%27custom%27%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        MEDIA_TABLE => Array(
            "id" => "%5B%22int%2811%29+unsigned%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "name" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "file_name" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "alias" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "file_path" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "file_type" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "file_size" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "width" => "%5B%22int%285%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "height" => "%5B%22int%285%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "description" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "caption" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "alt_text" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "data_type" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "data_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22NO%22%5D",
            "date_created" => "%5B%22timestamp%22%2C%22%22%2C%22CURRENT_TIMESTAMP%22%2C%22%22%2C%22NO%22%5D",
            "date_updated" => "%5B%22timestamp%22%2C%22%22%2C%220000-00-00+00%3A00%3A00%22%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        MEDIA_FORMATS_TABLE => Array(
            "id" => "%5B%22int%2811%29+unsigned%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "name" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "code" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "source_type" => "%5B%22enum%28%27plugin%27%2C%27theme%27%2C%27system%27%2C%27custom%27%2C%27backup%27%29%22%2C%22%22%2C%22custom%22%2C%22%22%2C%22YES%22%5D",
            "source_code" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "ref_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "min" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "max" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "filelimit" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "extensions" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "actions" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22NO%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        MEDIA_FORMATS_ACTIONS_TABLE => Array(
            "id" => "%5B%22int%2811%29+unsigned%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "name" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "code" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "description" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "options" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22NO%22%5D",
        ),
        MENUS_TABLE => Array(
            "id" => "%5B%22int%2811%29+unsigned%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "in_admin" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22NO%22%5D",
            "menu_class" => "%5B%22varchar%2820%29%22%2C%22%22%2C%22menu%22%2C%22%22%2C%22YES%22%5D",
            "menu_bar_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "parent_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "title" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "key" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "table" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "alias" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "descr" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "target" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "targettype" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "taxonomy_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "window" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "restricted" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "custom" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22NO%22%5D",
            "sort_order" => "%5B%22int%2811%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        PAGE_TYPES_TABLE => Array(
            "id" => "%5B%22int%2811%29+unsigned%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "type" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "description" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "locked" => "%5B%22tinyint%281%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        PAGES_TABLE => Array(
            "id" => "%5B%22int%2811%29+unsigned%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "pagename" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "pagetitle" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "description" => "%5B%22varchar%28255%29%22%2C%22%22%2C%22%22%2C%22%22%2C%22NO%22%5D",
            "content" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "link" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "layout" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "pagetypeid" => "%5B%22int%2811%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22YES%22%5D",
        ),
        PLUGINS_TABLE => Array(
            "id" => "%5B%22int%285%29%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "repoid" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "name" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "ver" => "%5B%22varchar%2810%29%22%2C%22%22%2C%221.0%22%2C%22%22%2C%22YES%22%5D",
            "sysver" => "%5B%22varchar%2810%29%22%2C%22%22%2C%223.00%22%2C%22%22%2C%22YES%22%5D",
            "author" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "created" => "%5B%22timestamp%22%2C%22%22%2C%220000-00-00+00%3A00%3A00%22%2C%22%22%2C%22YES%22%5D",
            "revised" => "%5B%22timestamp%22%2C%22%22%2C%220000-00-00+00%3A00%3A00%22%2C%22%22%2C%22YES%22%5D",
            "next_revision_ver" => "%5B%22varchar%2810%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "descr" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "license" => "%5B%22varchar%2850%29%22%2C%22%22%2C%22free%22%2C%22%22%2C%22YES%22%5D",
            "website" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "usedin" => "%5B%22varchar%2850%29%22%2C%22%22%2C%22both%22%2C%22%22%2C%22YES%22%5D",
            "folder" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "depends" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "parent" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "parent_type" => "%5B%22enum%28%27plugin%27%2C%27extension%27%2C%27framework%27%2C%27theme%27%2C%27themeset%27%2C%27package%27%29%22%2C%22%22%2C%22plugin%22%2C%22%22%2C%22YES%22%5D",
            "group" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "incl" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "initfile" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "initcallers" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "initcontexts" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "headerfunc" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "settingsfunc" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "inline_settings" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "custom_settings" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "nodisable" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "nodelete" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "builtin" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "autoincl" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22YES%22%5D",
            "is_framework" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "is_editor" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "is_deleted" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "errors" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "error_code" => "%5B%22int%283%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "updflag" => "%5B%22varchar%285%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        REGISTER_TABLE => Array(
            "id" => "%5B%22int%285%29%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "type" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "alias" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "db_table" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "db_child_table" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "function" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "parameters" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "conditions" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "actions" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "scheduled" => "%5B%22int%282%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "priority" => "%5B%22int%2820%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "user_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "db_parent_table" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        SESSION_TABLE => Array(
            "id" => "%5B%22int%285%29%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "user_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "ip_hash" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "username" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "section" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "logged_in_date" => "%5B%22timestamp%22%2C%22%22%2C%22CURRENT_TIMESTAMP%22%2C%22on+update+CURRENT_TIMESTAMP%22%2C%22YES%22%5D",
            "logged_in" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        SETTINGS_TABLE => Array(
            "name" => "%5B%22varchar%2850%29%22%2C%22PRI%22%2C%22%22%2C%22%22%2C%22NO%22%5D",
            "value" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "type" => "%5B%22char%283%29%22%2C%22%22%2C%22str%22%2C%22%22%2C%22YES%22%5D",
        ),
        TAXONOMIES_TABLE => Array(
            "id" => "%5B%22int%2811%29%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "code" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "name" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "parent_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "type" => "%5B%22enum%28%27tag%27%2C%27faq%27%2C%27link%27%2C%27category%27%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "data_alias" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22NO%22%5D",
        ),
        TERMS_TABLE => Array(
            "id" => "%5B%22int%2811%29%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "taxonomy_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "parent_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "code" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "name" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22NO%22%5D",
            "description" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%221%22%2C%22%22%2C%22NO%22%5D",
        ),
        TERMS_DATA_TABLE => Array(
            "id" => "%5B%22int%2811%29%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "term_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "data_id" => "%5B%22int%2811%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22NO%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        ),
        THEMES_TABLE => Array(
            "id" => "%5B%22int%2811%29+unsigned%22%2C%22PRI%22%2Cnull%2C%22auto_increment%22%2C%22NO%22%5D",
            "type" => "%5B%22varchar%2810%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "name" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "ver" => "%5B%22varchar%2810%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "sysver" => "%5B%22float%284%2C2%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "folder" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "descr" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "author" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "website" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "custom_settings" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "settingsfunc" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "headerfunc" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "stylefiles" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "scriptfiles" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "menuslots" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "locations" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "layoutfolder" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "layouts" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "errors" => "%5B%22text%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "error_code" => "%5B%22int%283%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "active" => "%5B%22tinyint%281%29%22%2C%22%22%2C%220%22%2C%22%22%2C%22YES%22%5D",
            "parent" => "%5B%22varchar%2850%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "updflag" => "%5B%22varchar%285%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
            "sites" => "%5B%22varchar%28255%29%22%2C%22%22%2Cnull%2C%22%22%2C%22YES%22%5D",
        )
	);

	if(isset($hash_tables[$tablekey])){
        // prepare hashes of subject table columns
		$sql = "SHOW COLUMNS FROM `{$tablekey}`";
        $tablecols = array();
		$tablecols_rs = mysqli_query($db_link, $sql);
		while($col = mysqli_fetch_assoc($tablecols_rs)){
			$tablecols[$col['Field']] = urlencode(json_encode(array($col['Type'], $col['Key'], $col['Default'], $col['Extra'], $col['Null'])));
        }

        $reqdcols = $hash_tables[$tablekey];
        foreach($reqdcols as $fieldkey => $reqdcolhash){
            if(!isset($tablecols[$fieldkey])){
                // one of the columns from the subject table is missing
                $reqdcolunhash = json_decode(urldecode($reqdcolhash));
                $sql = "ALTER TABLE `$tablekey` ADD `$fieldkey` ".$reqdcolunhash[0];
                if($reqdcolunhash[1] != '') $sql .= " ".$reqdcolunhash[1];                  // key
                if(!is_null($reqdcolunhash[2]) && $reqdcolunhash[2] != ''){
                    $sql .= " DEFAULT '".$reqdcolunhash[2]."'";      // default
                    if($reqdcolunhash[3] != '') $sql .= " ".$reqdcolunhash[3];              // extra
                }else{
                    if(strpos($reqdcolunhash[0], 'varchar') !== false){
                        if(strtolower($reqdcolunhash[4]) == 'yes')
                            $sql .= " default null";
                        else
                            $sql .= " not null";
                    }
                }
                mysqli_query($db_link, $sql);
            }elseif($reqdcolhash != $tablecols[$fieldkey]){
                // one of the columns from the subject table is not structured correctly
                $reqdcolunhash = json_decode(urldecode($reqdcolhash));
                $tablecolunhash = json_decode(urldecode($tablecols[$fieldkey]));
                $sql = "ALTER TABLE `$tablekey` MODIFY COLUMN `$fieldkey` ".$reqdcolunhash[0];
                if($reqdcolunhash[1] != '') $sql .= " ".$reqdcolunhash[1];                  // key
                if(!is_null($reqdcolunhash[2]) && $reqdcolunhash[2] != ''){
                    $sql .= " default '".$reqdcolunhash[2]."'";      // default
                    if($reqdcolunhash[3] != '') $sql .= " ".$reqdcolunhash[3];              // extra
                }else{
                    if(strpos($reqdcolunhash[0], 'varchar') !== false){
                        if(strtolower($reqdcolunhash[4]) == 'yes')
                            $sql .= " default null";
                        else
                            $sql .= " not null";
                    }
                }
                mysqli_query($db_link, $sql);
            }
        }
    }
}

/**
 * Read db.ini database settings
 * @return array
 */
function readDBINI(){
    $dbset = array();
    if(file_exists(SITE_PATH.ADMIN_FOLDER.CONFIG_FOLDER."db.ini")){
        // load each line of db.ini into $dbset where lines starting with # as the parent key
        if(($fh = @fopen(SITE_PATH.ADMIN_FOLDER.CONFIG_FOLDER."db.ini", "r")) !== false){
            $dbkey = "";
            while($line = fgets($fh)){
                $line = trim($line);
                if(substr($line, 0, 1) == "#"){
                    if($dbkey != substr($line, 1)) {
                        $dbkey = substr($line, 1);
                        $dbset[$dbkey] = array();
                    }
                }elseif($dbkey != ''){
                    $nameval = explode("=", $line);
                    if(strtolower($nameval[0]) == 'dbport'){
                        $nameval[1] = intval($nameval[1]);
                    }
                    $dbset[$dbkey][strtolower($nameval[0])] = $nameval[1];
                }
            }
            fclose($fh);
        }
    }else{
        file_put_contents(SITE_PATH.ADMIN_FOLDER.CONFIG_FOLDER."db.ini", PHP_EOL);
    }
    return $dbset;
}

/**
 * Update db.ini file with database settings
 * @param array $dbset
 */
function updateDBINI($dbset){
    // write $dbset nodes to db.ini file
    $dbini_loc = SITE_PATH.ADMIN_FOLDER.CONFIG_FOLDER."db.ini";
    if(!is_writable($dbini_loc)){
        return 'The control file, '.$dbini_loc.', or its parent folder is not writable'.PHP_EOL.'or the web server is not granted write permission to it.'.PHP_EOL.PHP_EOL.'Note: Beyond setting the mode to 777, Apache requires group www-data to have write access.';
    }

    $sys_go = (isset($dbset["SYS_GO"]));
    if($sys_go) unset($dbset["SYS_GO"]);                        // ensures SYS_GO is the last line
    chmod($dbini_loc, 0777);

    $contents = null;
    foreach($dbset as $key => $vals){
        $contents .= "#".$key.PHP_EOL;
        foreach($vals as $name => $val){
            if($name == 'DBPORT' && $val == 0) $val = '';   // don't write 0
            $contents .= strtoupper($name)."=".$val.PHP_EOL;
        }
    }
    $contents .= "#SYS_GO".PHP_EOL;
    file_put_contents($dbini_loc, $contents);

    // lock up the file from further writing
    chmod($dbini_loc, 0644);
    sleep(1);
    return '';
}

/**
 * Both read and update db.ini file
 * @param string $db_domain
 * @param string $db_host
 * @param string $db_name
 * @param string $db_user
 * @param string $db_pass
 * @return string
 */
function prepDBINI($db_domain, $db_host, $db_name, $db_user, $db_pass, $db_port = "3306"){
    $key = "";
    $db_domainext = substr($db_domain, -3);

    // if a key for this domain exists, update the settings
    if(isset($dbset[$db_domain])){
        $key = $db_domain;
    }elseif(isset($dbset[$db_domainext])){
        $key = $db_domainext;
    }else{
        // ... otherwise add a new key
        $key = $db_domain;
    }

    // set the values
    $dbset[$key] = array("DBNAME" => $db_name,
                         "DBHOST" => $db_host,
                         "DBUSER" => $db_user,
                         "DBPASS" => $db_pass,
                         "DBPORT" => $db_port,
                        );

    // set the CLI values
    $dbset[PHP_CLI_DBHOST] = array("DBNAME" => $db_name,
                         "DBHOST" => $db_host,
                         "DBUSER" => $db_user,
                         "DBPASS" => $db_pass,
                         "DBPORT" => $db_port,
                        );

    // put it back in the file
    $retn = updateDBINI($dbset);
    return $retn;
}

/**
 * Save or return database SQL file or database table dump
 * @param string $tables                    Table name(s) (default = *) [optional]
 * @param string $new_dom                   URL of new domain
 * @param string $new_root                  Path to root of new domain
 * @param string $tables_dropped_first      True if tables are dropped first before creating during restore/import
 * @param string $records_trunc_first       True if records are truncated first before inserting during restore/import
 * @return boolean
 */
function getDBDumpSQL($tables = "*", $new_dom = "", $new_root = "", $tables_dropped_first = false, $records_trunc_first = false){
    global $_sec, $db_link;

    $db_link = mysqli_connect(DBHOSTPORT, DBUSER, DBPASS, DBNAME);

    // get all of the tables
    if($tables == '*'){
        $tables = array();
        $result = mysqli_query($db_link, 'SHOW TABLES');
        while($row = mysqli_fetch_row($result)){
            $tables[] = current($row);
        }
    }else{
        $tables = is_array($tables) ? $tables : explode(',', $tables);
    }

    // skip the records for tables that are re-filled and re-validated on init
    $table_data_to_skip = array(EVENT_LOG_TABLE, SESSION_TABLE, PLUGINS_TABLE, REGISTER_TABLE, THEMES_TABLE);

    // cycle through
    $return = "# ".SYS_NAME." Database SQL Backup File\n";
    $return.= "# Version: ".CODE_VER."\n";
    $return.= "#\n";
    $return.= "# Site: ".WEB_URL."\n";
    $return.= "# Hostname: ".DBHOST."\n";
    $return.= "# Database: `".DBNAME."`\n";
    $return.= "# Generated: ".date("F j, Y g:i a")."\n";
    $return.= "# --------------------------------------------------------\n\n";

    foreach($tables as $table){
        $result = mysqli_query($db_link, 'SELECT * FROM '.$table);
        $num_fields = mysqli_num_fields($result);

        $return.= "# --------------------------------------------------------\n";
        $return.= "# TABLE: `".$table."`\n";
        $return.= "# --------------------------------------------------------\n\n";

        $return.= "# Drop table if it exists: `".$table."`\n\n";
        $return.= "DROP TABLE IF EXISTS `".$table."`;\n\n";

        $row2 = mysqli_fetch_row(mysqli_query($db_link, 'SHOW CREATE TABLE '.$table));
        $return.= "# Table structure: `".$table."`\n\n";
        $return.= $row2[1].";\n\n";

        if(!in_array($table, $table_data_to_skip)){
            $return.= "# Table data: `".$table."`\n\n";
            for ($i = 0; $i < $num_fields; $i++){
                while($row = mysqli_fetch_row($result)){
                    $return .= "INSERT INTO `".$table."` VALUES(";
                    for($j = 0; $j < $num_fields; $j++){
                        if (isset($row[$j])){
                            if(is_null($row[$j]))
                                $return .= 'NULL';
                            else {
                                if($new_dom != '')
                                    $row[$j] = replaceStringinHybridString(WEB_URL, $row[$j], $new_dom);
                                if($new_root != '')
                                    $row[$j] = replaceStringinHybridString(SITE_PATH, $row[$j], $new_root);

                                $row[$j] = addslashes($row[$j]);
                                $row[$j] = preg_replace("/\n/", "\\n", $row[$j]);
                                $return .= '"'.$row[$j].'"';
                            }
                        }else
                            $return .= 'NULL';
                        if ($j < ($num_fields-1)) $return .= ',';
                    }
                    $return .= ");\n";
                }
            }
            $return .= "\n";
        }
    }

    // save file
    $file = ADMIN_FOLDER.CACHE_FOLDER.date("ymd").'-'.(md5(implode(',', $tables))).'.sql';
    if(file_exists($file)) unlink($file);
    $handle = fopen(SITE_PATH.$file, 'w+');
    if($handle){
        fwrite($handle, $return);
        fclose($handle);

        return $file;
    }else{
        return false;
    }
}

/**
 * Produce and return FDBX file of database or database table dump
 * @param string $tables                    Table name(s) (default = *) [optional]
 * @param string $new_dom                   URL of new domain
 * @param string $new_root                  Path to root of new domain
 * @param string $tables_dropped_first      True if tables are dropped first before creating during restore/import
 * @param string $records_trunc_first       True if records are truncated first before inserting during restore/import
 * @return boolean
 */
function getDBDumpFDBX($tables = "*", $new_dom = "", $new_root = "", $tables_dropped_first = false, $records_trunc_first = false){
    global $db_link;

    $db_link = mysqli_connect(DBHOSTPORT, DBUSER, DBPASS, DBNAME);

    // get all of the tables
    if($tables == '*'){
        $tables = array();
        $result = mysqli_query($db_link, 'SHOW TABLES');
        while($row = mysqli_fetch_row($result)){
            $tables[] = current($row);
        }
    }else{
        $tables = is_array($tables) ? $tables : explode(',', $tables);
    }

    $table_data_to_skip = array(EVENT_LOG_TABLE, SESSION_TABLE, PLUGINS_TABLE, REGISTER_TABLE, THEMES_TABLE);

    $imp = new DOMImplementation;
    $dom = $imp->createDocument();
    $dom->formatOutput = true;
    $dom->encoding = 'UTF-8';
    $dom->standalone = false;

    $root = $dom->createElement("fdbx");
    $root->setAttribute("id", CODE_VER);
    $dom->appendChild($root);

    // cycle through
    $dbn = $dom->createElement("database");
    $dbn->setAttribute("id", DBNAME);
    $root->appendChild($dbn);

    $child = $dom->createElement("host");
    $child->setAttribute("id", DBHOST);
    $dbn->appendChild($child);

    $child = $dom->createElement("site");
    $child->setAttribute("id", WEB_URL);
    $dbn->appendChild($child);

    foreach($tables as $table){
        $result = mysqli_query($db_link, 'SELECT * FROM '.$table);
        $num_fields = mysqli_num_fields($result);

        $tbl = $dom->createElement("table");
        $tbl->setAttribute("id", $table);
        $dbn->appendChild($tbl);

        $row2 = mysqli_fetch_row(mysqli_query($db_link, 'SHOW CREATE TABLE '.$table));

        $node = $dom->createElement("create");
        if($tables_dropped_first) $node->setAttribute("task_before", "drop_table");
        $tbl->appendChild($node);

        $struct = $row2[1];
        $struct = explode("\n", preg_replace("/CREATE TABLE `(.+)` \(\n/i", "", $struct));
        foreach($struct as $line){
            if(strpos($line, "PRIMARY KEY") !== false){
                preg_match("/PRIMARY KEY \(`(.+)`\)/i", $line, $vals);
                $key = "PRIMARY_KEY";
                $val = $vals[1];
            }elseif(strpos($line, "ENGINE") !== false){
                preg_match("/\) ENGINE=(.+)/i", $line, $vals);
                $key = "ENGINE";
                $val = $vals[1];
            }else{
                preg_match("/`(.+)` (.+),/i", $line, $vals);
                $key = $vals[1];
                $val = $vals[2];
            }
            $item = $dom->createElement($key);
            $node->appendChild($item);
            $value = $dom->createTextNode($val);
            $item->appendChild($value);
        }

        if(!in_array($table, $table_data_to_skip)){
            $node = $dom->createElement("insert");
            if($records_trunc_first) $node->setAttribute("task_before", "truncate_records");
            $node = $tbl->appendChild($node);
            for ($i = 0; $i < $num_fields; $i++){
                while($row = mysqli_fetch_row($result)){
                    $val = array();
                    for($j = 0; $j < $num_fields; $j++){
                        if (isset($row[$j])){
                            if(is_null($row[$j]))
                                $val[] = NULL;
                            else {
                                if($new_dom != '')
                                    $row[$j] = replaceStringinHybridString(WEB_URL, $row[$j], $new_dom);
                                if($new_root != '')
                                    $row[$j] = replaceStringinHybridString(SITE_PATH, $row[$j], $new_root);

                                $row[$j] = addslashes($row[$j]);
                                $row[$j] = preg_replace("/\n/", "\\n", $row[$j]);
                                $val[] .= $row[$j];
                            }
                        }else
                            $val[] = NULL;
                    }
                    $item = $dom->createElement("row");
                    $node->appendChild($item);
                    $value = $dom->createTextNode(json_encode($val));
                    $item->appendChild($value);
                }
            }
        }
    }

    $file = ADMIN_FOLDER.CACHE_FOLDER.date("ymd").'-'.(md5(implode(',', $tables))).'.fdbx';
    $xml_fsize = $dom->save(SITE_PATH.$file);
    if($xml_fsize > 0){
        return $file;
    }else{
        return false;
    }
}
?>