<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("SYSLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/* SYSTEM GLOBAL CLASSES
 *
 * Stores and manages system data
 * - plugins
 * - pluginsincl
 * - pluginsprob
 * - frameworks
 */
class SystemClass extends ArrayObject {
	// overloaded data
	private $_keys = array( "plugins" => array(), "pluginsincl" => array(), "pluginsprob" => array(), "pluginscontexts" => array(),
                            "frameworks" => array(), "themes" => array(), "themepaths" => array(), "incl" => array(),
                            "currentexecplugin" => array(), "get" => array(), "post" => array(), "filesys" => array(),
							"info" => array(), "themepaths" => array()
                            );
    private $_subkeys = array();
	protected static $_instance = null;
    public $configs = array();

	public function __construct() {
        // Allow accessing properties as either array keys or object properties:
        // parent::__construct(array(), ArrayObject::ARRAY_AS_PROPS);
	}

	private function __clone(){
	}

	public static function init($configs){
		$s = new self;
		// $s->_system['configs'] = $configs;
        $s->configs = $configs;
		return $s;
	}

	public function show_properties(){
        foreach($this->_keys as $k) print "[$k] = ".$this->_keys[$k]."<br>";
        foreach($this->_subkeys as $k){
            if(isset($this->_subkeys[$k])) {
                print "[$k] = ";
                if(is_array($this->_subkeys[$k]))
                    print "<pre>".print_r($this->_subkeys[$k], true)."</pre><br/>";
                else
                    print $this->_subkeys[$k]."<br/>";
            }
        }
	}

	public function __get($name){
        $v = null;
		if(array_key_exists($name, $this->_keys)){
			// return scalar value
            $v = $this->_keys[$name];
		}elseif(array_key_exists($name, $this->_subkeys)){
			// return subarray value
            $v = $this->_subkeys[$name];
		}else{
            $d = debug_backtrace();
			trigger_error("Cannot get '$name'.  It is not a valid _SYSTEM property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
        return $v;
	}

	public function __set($name, $value){
		if(array_key_exists($name, $this->_keys)){
			// set scalar value
			$this->_keys[$name] = $value;
		}elseif(array_key_exists($name, $this->_subkeys)){
			// set subarray value
			$this->_subkeys[$name][] = $value;
		}else{
            $d = debug_backtrace();
			trigger_error("Cannot set '$name'.  It is not a valid _SYSTEM property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
	}
}

?>