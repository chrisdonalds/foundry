<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("FILESYSLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

define('UPLOAD_TYPE_GIF', 1);
define('UPLOAD_TYPE_JPG', 2);
define('UPLOAD_TYPE_PNG', 3);
define('UPLOAD_COMPRESSION', 6);

define('UPLOAD_IMG_ORIG_RESIZE_TO_MAX', true);		// if true, Uploader will resize original image to IMG_MAX_WIDTH/HEIGHT

define("UPLOAD_FOLDER", MEDIA_FOLDER."uploads/");
define("UPLOAD_TEMPFOLDER", MEDIA_FOLDER."temp/");
define("UPLOAD_LIBFOLDER", UPLOAD_FOLDER."library/");

define("TARGETTYPE_PHPFILE", "phpfile");
define("TARGETTYPE_SETTINGS", "settings");
define("TARGETTYPE_SYSFILE", "systemfile");

/* FILE SYSTEM CLASS
 *
 * Provides the system with various file system-related functions
 */
class FileSysClass {
	// overloaded data
	private $_files = array();
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'_filesys');				// alert triggered functions when function executes
		return $s;
	}

	// -------------------- Filesystem -----------------

	/**
	 * Return whether or not SSL is in use
	 * @return boolean
	 */
	function checkSSL() {
		if(isset($_SERVER['HTTPS'])){
			if(strtolower($_SERVER['HTTPS']) == 'on' || intval($_SERVER['HTTPS']) == 1) return true;
		}elseif(isset($_SERVER['SERVER_PORT'])){
			if($_SERVER['SERVER_PORT'] == '443') return true;
		}
		return false;
	}

	/**
	 * Return a byte-formatted size
	 * @param string $data
	 * @return string
	 */
	function formatFileSize($data) {
		$data = intval($data);
		$val = $data;
		if($data < 1024) {
			// bytes
	    	$val = $data . " B";
	    } else if($data < pow(2, 20)) {
			// kilobytes
	    	$val = round(($data / pow(2, 10)), 1) . " kB";
	    } else if( $data < pow(2, 30)) {
			// megabytes
	        $val = round(($data / pow(2, 20)), 1) . " MB";
	    } else if( $data < pow(2, 40)) {
	    	// gigabytes
	        $val = round(($data / pow(2, 30)), 1) . " GB";
	    } else if( $data < pow(2, 50)) {
	    	// terabytes
	        $val = round(($data / pow(2, 40)), 1) . " TB";
	    } else {
	    	// gigabytes
	        $val = round(($data / pow(2, 50)), 1) . " PB";
	    }
	    return $val;
	}

	/**
	 * Return a byte-formatted size (xx kB) converted back to decimal
	 * @param string $data
	 * @return integer
	 */
	function evaluateFileSize($data){
		$units = array("kb" => 10, "k" => 10, "mb" => 20, "m" => 20, "gb" => 30, "g" => 30, "tb" => 40, "t" => 40, "pb" => 50, "p" => 50);
		$data = strtolower($data);
		$val = floatval($data);
		foreach($units as $unit => $power){
			if(strpos($data, $unit) !== false){
				$val = floatval($data) * pow(2, $power);
				break;
			}
		}
		return $val;
	}

	/**
	 * Return array of limited image dimensions
	 * @param string $filename
	 * @param integer $wdim [optional]
	 * @param integer $hdim [optional]
	 * @return array
	 */
	function constrainImage($filename, $wdim = 0, $hdim = 0, $exact = false) {
		if($filename != "") {
			if(!file_exists($filename) || substr($filename, -1, 1) == "/") return false;
			list($width, $height, $type, $attr) = @getimagesize($filename);
			$origwidth = $width;
			$origheight= $height;
			if ($wdim > 0) {
				// if $width > $wdim, get reducer ratio and multiple by $height
				if (($width > $wdim || ($width != $wdim && $exact)) && $height > 0) {
					$ratio = $wdim / $width;
					$width = $wdim;
					$height = intval($ratio * $height);
				}
			}
			if ($hdim > 0) {
				// if $height > $hdim, get reducer ratio and multiple by $width
				if (($height > $hdim || ($height != $hdim && $exact)) && $width > 0) {
					$ratio = $hdim / $height;
					$height = $hdim;
					$width = intval($ratio * $width);
				}
			}
		}
		return array($width, $height, $origwidth, $origheight);
	}

	/**
	 * Sanitize URL and include file into system (only .php files are included)
	 * @param string $url
	 * @param boolean $required 		(require instead of include)
	 */
	function includeFile($url, $required = false){
		global $_events;

		$url = trim(preg_replace("/^[a-z0-9_\-]+$/i", "", $url));
		if(preg_match("/[\s]/i", $url)) $url = urlencode($url);
		$_events->processTriggerEvent(__FUNCTION__, $url);				// alert triggered functions when function executes

		$path_parts = pathinfo($url);
		$url_clean = $path_parts['dirname']."/".$path_parts['filename'].".php";

		extract($GLOBALS, EXTR_REFS);
		if(file_exists($url_clean)){
			if($required)
				require($url_clean);
			else
				include($url_clean);
			$_events->processTriggerEvent(__FUNCTION__.'_done', $url);				// alert triggered functions when function executes
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Redirect browser to URL
	 * @param string $url
	 */
	function gotoPage($url){
		global $_events;

		$url = trim(preg_replace("/^[a-z0-9_\-]+$/i", "", $url));
		if(preg_match("/[\s]/i", $url)) $url = urlencode($url);
		list($abort) = $_events->processTriggerEvent(__FUNCTION__, $url, false);				// alert triggered functions when function executes

		if ($url != "" && !$abort){
			if(strpos($url, WEB_URL) === false) $url = WEB_URL.$url;
			if(!headers_sent()){
				header("location: ".$url);
			}else{
		 		print "<script type=\"text/Javascript\">window.location='$url';</script>";
			}
			$_events->processTriggerEvent(__FUNCTION__.'_done', $url);				// alert triggered functions when function executes
		}
	}

	/**
	 * Redirect browser to Edit Page alias
	 * @param string $url
	 */
	function gotoEditPage($datatype, $action = 'edit'){
		global $_page, $_data, $_events;

		list($abort) = $_events->processTriggerEvent(__FUNCTION__, $datatype, false);				// alert triggered functions when function executes
		if(empty($datatype)) return false;

		$action = strtolower($action);
		if(!in_array($action, array('edit', 'add', 'list'))) $action = 'edit';

		$url = $_data->getAdminDataAlias($datatype, $action);
		$url = trim(preg_replace("/^[a-z0-9_\-]+$/i", "", $url));
		if(preg_match("/[\s]/i", $url)) $url = urlencode($url);

		if($url != ""){
			if(strpos($url, WEB_URL) === false) $url = WEB_URL.$url;
			$url .= "?row_id={$_page->row_id}";
			if(!headers_sent()){
				header("location: ".$url);
			}else{
		 		print "<script type=\"text/Javascript\">window.location='$url';</script>";
		 	}
			$_events->processTriggerEvent(__FUNCTION__.'_done', $datatype);				// alert triggered functions when function executes
		}
	}

	/**
	 * Check file existence and return either file or blank
	 * @param string $file
	 * @param string $inpath
	 * @return string
	 */
	function checkFilePath($file, $inpath) {
		if(substr($inpath, -1, 1) != "/") $inpath .= "/";
		$path 		= pathinfo($file);
		$filename 	= $path['basename'];
		$file       = $inpath.$filename;
		if ($filename == "" || !@file_exists(SITE_PATH.$file)) $file = "";
		return $file;
	}

	/**
	 * Check if image exists
	 * @param object $image
	 * @param object $inpath
	 * @return
	 */
	function checkImagePath($image, $inpath, $noimgurl = NO_IMG, $fileprefix = null) {
	    if($image != ""){
	        if(substr($inpath, -1, 1) != "/") $inpath .= "/";
	        $path         = pathinfo($image);
	        $filename     = $path['basename'];
	        $photo_pic    = $inpath.$fileprefix.$filename;
	        if ($filename == "" || !@file_exists(SITE_PATH.$photo_pic)){
	            $photo_pic = ((!empty($noimgurl)) ? MEDIA_FOLDER.IMG_UPLOAD_FOLDER.$noimgurl : "");
	        }
	    }
	    return $photo_pic;
	}


	/**
	 * Check if thumbnail exists
	 * @param object $image
	 * @param object $inpath
	 * @return
	 */
	function checkThumbPath($image, $inpath, $nothmurl = NO_THM, $fileprefix = null) {
	    if($image != ""){
	        if(substr($inpath, -1, 1) != "/") $inpath .= "/";
	        $path         = pathinfo($image);
	        $filename     = $path['basename'];
	        $photo_pic    = $inpath.$fileprefix.$filename;
	        if ($filename == "" || !@file_exists(SITE_PATH.$photo_pic)){
	            $photo_pic = ((!empty($nothmurl)) ? MEDIA_FOLDER.THM_UPLOAD_FOLDER.$nothmurl : "");
	        }
	    }
	    return $photo_pic;
	}

	/**
	 * Delete image and/or thumb files.  Returns array of blanks if successful
	 * @param string $image
	 * @param string $thumb
	 * @return string
	 */
	function deleteImage($image, $thumb) {
		global $_events;

		list($abort) = $_events->processTriggerEvent(__FUNCTION__, array($image, $thumb), false);				// alert triggered functions when function executes
		if($abort) return false;

		if(file_exists($image) && is_file($image)) {
			if(unlink($image)) $image = "";
		}else{
			$image = "";
		}
	    $thumb_a = str_replace("thm_", "thm_a_", $thumb);
		if(file_exists($thumb) && is_file($thumb)) {
			if(unlink($thumb)) $thumb = "";
		}else{
			$thumb = "";
		}
		if(file_exists($thumb_a) && is_file($thumb_a)) {
			if(unlink($thumb)) $thumb_a = "";
		}else{
			$thumb_a = "";
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
		return array($image, $thumb);
	}

	/**
	 * Delete file.  Returns blank if successful
	 * @param string $file
	 * @return string
	 */
	function deleteFile($file) {
		global $_events;

		list($abort) = $_events->processTriggerEvent(__FUNCTION__, array($file), false);				// alert triggered functions when function executes
		if($abort) return false;

		if(file_exists($file) && is_file($file)) {
			if(unlink($file)) $file = "";
		}else{
			$file = "";
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
		return $file;
	}

	/**
	 * Recursively delete folders and all contents (except for symbolic links) within directory tree
	 * @param string $dir
	 */
	function deleteFolderTree($dir, $recurse = false){
		global $_events;

		if(!$recurse){
			list($abort) = $_events->processTriggerEvent(__FUNCTION__, array($dir), false);				// alert triggered functions when function executes
			if($abort) return false;
		}

		$ok = false;
		$dir = trim($dir, "/");
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != ".." && !is_link($dir)) {
					if (filetype($dir."/".$object) == "dir")
						$this->deleteFolderTree($dir."/".$object, true);
					else
						unlink($dir."/".$object);
				}
			}
			reset($objects);
			rmdir($dir);
			$ok = true;
		}
		return $ok;
	}

	/**
	 * Read text file and return either false on error or array of delimited content
	 * @param string $file
	 * @param string $lineend
	 * @param string $delim
	 * @param string $enclosure
	 * @return false|array
	 */
	function parseTextFile($file, $lineend = PHP_EOL, $delim = ",", $enclosure = ""){
		if(!file_exists($file)) return false;
		$arry = array();
		$contents = @file_get_contents($file);
		$lines = explode($lineend, $contents);
		foreach($lines as $line){
			$chunks = explode($delim, $line);
			$arry[] = $chunks;
		}
		return $arry;
	}

	/**
	 * Return the last/lowest subfolder in path
	 * @param string $dir
	 * @return string
	 */
	function getLowestChildFolder($dir){
	    if($dir != ''){
	        $dir_arry = explode("/", rtrim($dir, "/"));
	        $last = count($dir_arry) - 1;
	        return (($last >= 0) ? $dir_arry[$last] : "");
	    }
	}

	/**
	 * Return path relative to root (either URL or filesys)
	 * @param string $dir
	 * @param string $root
	 * @return string
	 */
	function getRelativePath($dir, $root = SITE_PATH){
		$dir = str_replace("\\", "/", $dir);
		$root = str_replace("/", "\/", $root);
	    return (preg_replace("/^".$root."/i", "", $dir));
	}

	/**
	 * Return the relative path to the current script
	 * @param string $dir
	 * @return string
	 */
	function getCurrentPath($atRoot=FALSE, $atCore=FALSE, $parse=FALSE){
	    if (isset($_SERVER['HTTP_HOST'])) {
	    	if ($_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off'))
	    		$http = 'https';
	    	else
	    		$http = 'http';
	        $hostname = $_SERVER['HTTP_HOST'];
	        $dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

	        $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
	        $core = $core[0];

	        $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
	        $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
	        $base_url = sprintf( $tmplt, $http, $hostname, $end );
	    } else
	    	$base_url = 'http://localhost/';

	    if ($parse) {
	        $base_url = parse_url($base_url);
	        if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
	    }

	    return $base_url;
	}

	/**
	 * Add a slash to the trailing or leading end of path
	 * @param string $path  		The path to check
	 * @param string $slashchar		The slash character instead of DIRECTORY_SEPARATOR
	 * @param boolean $leading 		If true, path will be checked for leading slash and prepended if none exists
	 * @return string
	 */
	function addSlash($path, $slashchar = "/", $leading = false){
		if(!is_string($path)) return false;
		if($slashchar == "") $slashchar = DIRECTORY_SEPARATOR;
		if ($leading){
			if(substr($path, 0, 1) != $slashchar) $path = $slashchar.$path;
		}else{
			if(substr($path, -1, 1) != $slashchar) $path .= $slashchar;
		}
		return $path;
	}

	/**
	 * Convert octal string to mode array (opposite of convertArray2Mode)
	 * @param string $mode
	 * @return array
	 */
	function convertMode2Array($mode){
		$level = array(1=>"user", 2=>"group", 3=>"other");
		$outp = array();
		$inp = str_split($mode, 1);
		for($i=1; $i<4; $i++){
			$outp[$level[$i]]['read'] = (($inp[$i] & 1) > 0);
			$outp[$level[$i]]['write'] = (($inp[$i] & 2) > 0);
			$outp[$level[$i]]['exec'] = (($inp[$i] & 4) > 0);
		}
		return $outp;
	}

	/**
	 * Convert mode array to octal string (opposite of convertMode2Array)
	 * @param array $arry
	 * @return string
	 */
	function convertArray2Mode(array $arry){
		$outp = "";
		$level = array(1=>"user", 2=>"group", 3=>"other");
		for($i=1; $i<4; $i++){
			$num = (getIntValIfSet($arry[$level[$i]]['read'])) + (getIntValIfSet($arry[$level[$i]]['write']) * 2) + (getIntValIfSet($arry[$level[$i]]['exec']) * 4);
			$outp .= "$num";
		}
		return $outp;
	}

	/**
	 * Return existence of file using cURL
	 * @param string $url
	 * @return boolean
	 */
	function fileExists2($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);

		/* Check for 404 (file not found). */
		$httpCode = intval(curl_getinfo($ch, CURLINFO_HTTP_CODE));
		curl_close($ch);

		if($response !== false) {
			// url good
			if(in_array($httpCode, array(301, 302, 200)))
				return true;
			else
				return false;
		}else{
			return false;
		}
	}

	/**
	 * Get file/folder permissions using PHP fileperms
	 * @param string $file
	 * @return string
	 */
	function getFilePerms($file, $tostr = false){
		if($file != ''){
			clearstatcache();
			$rtn = substr(sprintf('%o', @fileperms($file)), -4);
			if(intval($rtn) == 0) $rtn = $this->getFileACL($file, $tostr);
			return $rtn;
		}
		return false;
	}

	/**
	 * Use server getfacl to obtain file/folder attributes (more powerful than getFilePerms)
	 * @param string $file
	 */
	function getFileACL($file, $tostr = false){
		$retn = array();
		$outp = array();
		clearstatcache();
		if($file != '' && file_exists($file)){
			exec ("getfacl {$file}", $outp, $status);
			foreach($outp as $outl){
				$outl = strtolower($outl);
				if(strpos($outl, "# owner: ") !== false){
					$retn['owner'] = substr($outl, 8);
				}elseif(strpos($outl, "# group: ") !== false){
					$retn['grpown'] = substr($outl, 8);
				}elseif(strpos($outl, "user::") !== false){
					$retn['user']['read'] = (strpos(substr($outl, 6), 'r') !== false);
					$retn['user']['write'] = (strpos(substr($outl, 6), 'w') !== false);
					$retn['user']['exec'] = (strpos(substr($outl, 6), 'x') !== false);
				}elseif(strpos($outl, "group::") !== false){
					$retn['group']['read'] = (strpos(substr($outl, 6), 'r') !== false);
					$retn['group']['write'] = (strpos(substr($outl, 6), 'w') !== false);
					$retn['group']['exec'] = (strpos(substr($outl, 6), 'x') !== false);
				}elseif(strpos($outl, "other::") !== false){
					$retn['other']['read'] = (strpos(substr($outl, 6), 'r') !== false);
					$retn['other']['write'] = (strpos(substr($outl, 6), 'w') !== false);
					$retn['other']['exec'] = (strpos(substr($outl, 6), 'x') !== false);
				}
			}
			if($tostr) $retn = $this->convertArray2Mode($retn);
			return $retn;
		}
		return false;
	}

	/**
	 * Use multiple techniques to change file mode
	 * @param string $file
	 * @param string $perms
	 * @return boolean 					True if file permission was changed
	 */
	function chmod2($file, $mode = null){
		if($mode == null) $mode = CHMOD_FILE;
		if($file != '' && file_exists($file)){
			$perms_str = $this->getFilePerms($file, true);
			$mode_str = substr(strval($mode), -3);			// we are only interested in the permissions not file type
			if(intval($perms_str) < intval($mode_str)){
				// use PHP chmod (if web server has access)
				@chmod ($file, octdec($mode));
				$perms_str = $this->getFilePerms($file, true);
				if(intval($perms_str) < intval($mode_str)){
					// use system otherwise
					system ("chmod {$mode_str} {$file}");
					$perms_str = $this->getFilePerms($file, true);
					if(intval($perms_str) < intval($mode_str)) return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Use multiple techniques to create folder
	 * @param string $folder
	 * @param octal $perms
	 * @param boolean $recursive
	 * @return boolean 					True if folder was created
	 */
	function mkdir2($pathname, $mode = null, $recursive = true){
		if($mode == null) $mode = CHMOD_FOLDER;
		if(!empty($pathname)){
			if(!@mkdir($pathname, octdec($mode), (boolean)$recursive)){
				system ("mkdir {$pathname}");
			}
		}
		return file_exists($pathname);
	}

	/**
	 * Instead of just returning whether a folder exists, this function will create it if it does not.
	 * @param string $folder
	 * @param octal $permission
	 * @param boolean $recursive
	 * @return boolean
	 */
	function ensureFolderExists($folder, $permission = 0777, $recursive = true){
		$rtn = false;
	    if(!file_exists(SITE_PATH.$folder)){
	        $rtn = $this->mkdir2(SITE_PATH.$folder, $permission, $recursive);
	    }
	    return $rtn;
	}

	/**
	 * Convert all \ (windows directory separator) to unix-style /
	 * @param string $path
	 * @return string
	 */
	function convertPath2Unix($path){
		return str_replace("\\", "/", $path);
	}

	/**
	 * Return full url information in array
	 * @param string $url
	 * @deprecated
	 * @return array
	 */
	function parseUrl($url) {
	    $r  = "^(?:(?P<scheme>\w+)://)?";
	    $r .= "(?:(?P<login>\w+):(?P<pass>\w+)@)?";
	    $r .= "(?P<host>(?:(?P<subdomain>[\w\.]+)\.)?" . "(?P<domain>\w+\.(?P<extension>\w+)))";
	    $r .= "(?::(?P<port>\d+))?";
	    $r .= "(?P<path>[\w/]*/(?P<file>\w+(?:\.\w+)?)?)?";
	    $r .= "(?:\?(?P<arg>[\w=&]+))?";
	    $r .= "(?:#(?P<anchor>\w+))?";
	    $r = "!$r!";                                                // Delimiters

	    preg_match ($r, $url, $out);
	    return $out;
	}

	/**
	 * Get the file extension
	 * @param string $str
	 * @return string
	 */
	function getExtension($str) {
	    $i = strrpos($str,".");
	    if (!$i) { return ""; }
	    $l = strlen($str) - $i;
	    $ext = substr($str,$i+1,$l);
	    return $ext;
	}

	/**
	 * Return a specific full URL or path
	 * @param string $type 	home (default), homepath, plugins, pluginspath, rss, rss2, atom,
	 *		  				theme, themepath, themecss, themecsspath,
	 *		  				admintheme, adminthemepath, adminthemecss, adminthemecsspath
	 * @return string
	 */
	function getFolder($type = 'home', $name = ''){
		global $_plugins;

		switch(strtolower($type)){
			case 'home':
				return WEB_URL;
				break;
			case 'homepath':
				return SITE_PATH;
				break;
			case 'plugins':
				return WEB_URL.ADMIN_FOLDER.PLUGINS_FOLDER;
				break;
			case 'pluginspath':
				return SITE_PATH.ADMIN_FOLDER.PLUGINS_FOLDER;
				break;
			case 'rss':
			case 'rss2':
				return WEB_URL.RSS_FOLDER;
				break;
			case 'rdf':
				return WEB_URL.RDF_FOLDER;
				break;
			case 'atom':
				return WEB_URL.ATOM_FOLDER;
				break;
	        case 'adminscript':
	            return WEB_URL.ADMIN_FOLDER.JS_FOLDER;
	            break;
	        case 'adminscriptpath':
	            return SITE_PATH.ADMIN_FOLDER.JS_FOLDER;
	            break;
	        case 'adminscriptui':
	            return WEB_URL.ADMIN_FOLDER.JS_FOLDER."ui/";
	            break;
	        case 'adminscriptuipath':
	            return SITE_PATH.ADMIN_FOLDER.JS_FOLDER."ui/";
	            break;
			case 'webtheme':
				return WEB_URL.THEME_FOLDER.DEF_THEME;
				break;
			case 'webthemepath':
				return SITE_PATH.THEME_FOLDER.DEF_THEME;
				break;
			case 'webthemecss':
				return WEB_URL.THEME_FOLDER.DEF_THEME_CSS;
				break;
			case 'webthemecsspath':
				return SITE_PATH.THEME_FOLDER.DEF_THEME_CSS;
				break;
			case 'admintheme':
				return WEB_URL.ADMIN_FOLDER.ADM_THEME_FOLDER;
				break;
			case 'adminthemepath':
				return SITE_PATH.ADMIN_FOLDER.ADM_THEME_FOLDER;
				break;
			case 'adminthemecss':
				return WEB_URL.ADMIN_FOLDER.ADM_THEME_FOLDER.ADM_CSS_FOLDER;
				break;
			case 'adminthemecsspath':
				return SITE_PATH.ADMIN_FOLDER.ADM_THEME_FOLDER.ADM_CSS_FOLDER;
				break;
			case 'plugin':
			case 'pluginpath':
				if($name == ''){
					$stack = debug_backtrace();
					$stackindx = 0;
					if(count($stack) > 1 && $stack[1]['function'] == 'getPluginInfo') $stackindx = 1;
					$file = $this->convertPath2Unix($stack[$stackindx]['file']);
					if(strpos($file, SITE_PATH.ADMIN_FOLDER.PLUGINS_FOLDER) !== false){
						if($type == 'pluginpath')
							return dirname($file);
						else
							return str_replace(SITE_PATH, WEB_URL, dirname($file));
					}else{
						return null;
					}
				}else{
					$plugin = $_plugins->getPluginInstalledbyVerb($name);
					if(!is_array($plugin))
						return __FUNCTION__." cannot be used with the `plugin` parameter from the global scope of a plugin.";
					else
						if($type == 'pluginpath')
							return $plugin['folder'];
						else
							return str_replace(SITE_PATH, WEB_URL, $plugin['folder']);
				}
				break;
			default:
				return null;
		}
	}

	/**
	 * Return the system temp folder, not upload temp folder
	 * @return string
	 */
	function getTempFolder() {
		if (function_exists('sys_get_temp_dir')) return sys_get_temp_dir();
		if (!empty($_ENV['TMP'])) return realpath($_ENV['TMP']);
		if (!empty($_ENV['TMPDIR'])) return realpath( $_ENV['TMPDIR']);
		if (!empty($_ENV['TEMP'])) return realpath( $_ENV['TEMP']);
		$tempfile = tempnam(__FILE__, '');
		if (file_exists($tempfile)) {
			unlink($tempfile);
			return realpath(dirname($tempfile));
		}
		return null;
	}

	// --------------------- Uploader ---------------------

	/*
	* Uploader based on the FileUploader plugin
	*/

	/**
	 * Output the HTML code to enable the Uploader script
	 * @global string $incl
	 * @param string $dest_folder
	 * @param integer $width
	 * @param integer $height
	 * @param boolean $showButtonText
	 * @param string $uploadFilename
	 * @param boolean $allowdelete
	 */
	function uploaderPrepScript($dest_folder = "", $width = 100, $height = 100, $showButtonText = true, $uploadFilename = ''){
		global $incl, $_fields, $_plugins, $_events;

		if(defined("UPLOAD_INIT")) die("'uploaderPrepScript' can only be called once.  Instead call 'showImageField' for each image entry.");
		define("UPLOAD_INIT", true);
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes

		// folders/files
		$filever = "";
		$app_folder = WEB_URL.ADMIN_FOLDER.CORE_FOLDER."uploader/";
		$web_folder = WEB_URL;

		if(strpos($dest_folder, UPLOAD_TEMPFOLDER) !== false) $dest_folder = "";
		if(empty($dest_folder)) {
			$dest_folder = UPLOAD_FOLDER;
		}
		$dest_folder_svr = SITE_PATH.$dest_folder;
		$dest_folder_web = WEB_URL.$dest_folder;

		// ensure folders exist
		$this->ensureFolderExists($dest_folder, CHMOD_FOLDER, true);

		$js_width = "";
		$css_width = "";
		if($width > 0) {
			$js_width = "img.style.width = \"{$width}px\";";
			$css_width = "width: {$width}px; ";
		}

		$js_height = "";
		$css_height = "";
		if($height > 0) {
			$js_height = "img.style.height = \"{$height}px\";";
			$css_height = "height: {$height}px; ";
		}

		echo PHP_EOL;
		echo '<!-- UPLOADER -->'.PHP_EOL;

		// store dynamic values in JSON attribute
		$attr = array('app_folder' => $app_folder, 'web_folder' => $web_folder,
					  'dest_folder' => $dest_folder, 'dest_folder_svr' => $dest_folder_svr, 'dest_folder_web' => $dest_folder_web,
					  'uploadFilename' => $uploadFilename, 'width' => $width, 'height' => $height);
		$_fields->showHiddenField(array("id" => "uploader_params", "value" => urlencode(json_encode($attr))));

		// call post-PHP page build script code
		$_plugins->addScriptSourceLine(
			'script',
			WEB_URL.ADMIN_FOLDER.CORE_FOLDER."uploader/inc/",
			"uploader.core.js",
			"",
			"",
			"",
			false,
			true
		);
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Attach Uploader via Javascript code block
	 * @global string $incl
	 * @param array $elems
	 * @param array $lastfiles
	 * @param array $lastthms
	 * @param array $allowedexts
	 */
	function uploaderStart($elems, $lastfiles, $lastthms = array(), $allowedexts = array(), $sizelimits = array()){
		global $incl, $_events, $_media;

		if(defined("UPLOAD_ATTACHED")) die(__FUNCTION__." can only be called once.  Instead call 'showImageField' for each image entry.");
		if(!is_array($lastfiles)) die(__FUNCTION__." parameter lastfiles must be an array.");
		if(!is_array($lastthms)) die(__FUNCTION__." parameter lastthms must be an array (empty array if not used).");
		if(!is_array($allowedexts)) die(__FUNCTION__." parameter allowedexts must be an array (empty array if not used).");
		define("UPLOAD_ATTACHED", true);
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes

		$media_types = $_media->getMediaTypes();
		print "\n<script language=\"javascript\" type=\"text/Javascript\">\n";
		if(is_array($elems)){
			foreach($elems as $key => $elem){
				$filetype = "file";
				if(getIfArraysIntersect($media_types[MT_IMAGE], $allowedexts[$key]))
					$filetype = 'img';
				elseif(getIfArraysIntersect($media_types[MT_ICON], $allowedexts[$key]))
					$filetype = 'img';
				elseif(getIfArraysIntersect($media_types[MT_DOC], $allowedexts[$key]))
					$filetype = 'doc';
				elseif(getIfArraysIntersect($media_types[MT_WEB], $allowedexts[$key]))
					$filetype = 'web';
				elseif(getIfArraysIntersect($media_types[MT_GEO], $allowedexts[$key]))
					$filetype = 'file';
				elseif(getIfArraysIntersect($media_types[MT_AV], $allowedexts[$key]))
					$filetype = 'av';
				elseif(getIfArraysIntersect($media_types[MT_GENERIC], $allowedexts[$key]))
					$filetype = 'file';
				if (strpos($incl, "imgedit") !== false && $filetype == 'img') $filetype = 'imgedit';

				$lastfiles_str = ((!empty($lastfiles[$key])) ? $lastfiles[$key] : '');
				$lastthumbs_str = ((!empty($lastthms[$key])) ? $lastthms[$key] : '');
				$allowedexts_str = ((!empty($allowedexts[$key])) ? "'".join("','", $allowedexts[$key])."'" : '');
				$sizelimits_str = ((!empty($sizelimits[$key])) ? intval($sizelimits[$key]) : 0);

				print "	buildUploaderBlock(\"{$elem}\", \"{$lastfiles_str}\", \"{$lastthumbs_str}\", [{$allowedexts_str}], \"{$sizelimits_str}\", \"$filetype\");\n";
			}
		}else{
			$filetype = "file";
			if(getIfArraysIntersect($media_types[MT_IMAGE], $allowedexts))
				$filetype = 'img';
			elseif(getIfArraysIntersect($media_types[MT_ICON], $allowedexts))
				$filetype = 'img';
			elseif(getIfArraysIntersect($media_types[MT_DOC], $allowedexts))
				$filetype = 'doc';
			elseif(getIfArraysIntersect($media_types[MT_WEB], $allowedexts))
				$filetype = 'web';
			elseif(getIfArraysIntersect($media_types[MT_GEO], $allowedexts))
				$filetype = 'file';
			elseif(getIfArraysIntersect($media_types[MT_AV], $allowedexts))
				$filetype = 'av';
			elseif(getIfArraysIntersect($media_types[MT_GENERIC], $allowedexts))
				$filetype = 'file';
			if (strpos($incl, "imgedit") !== false && $filetype == 'img') $filetype = 'imgedit';

			$allowedexts = "'".join("','", $allowedexts)."'";
			if(empty($sizelimits)){
				$sizelimits = 0;
			}elseif(is_array($sizelimits)){
				$sizelimits = intval(current($sizelimits));
			}
			print "	buildUploaderBlock(\"{$elems}\", \"{$lastfiles}\", \"\", [{$allowedexts}], \"{$sizelimits}\", \"{$filetype}\")\n";
		}
		print "</script>\n";
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Move the uploaded file to the destination, and optionally create thumbnail
	 * @param string $modstatus
	 * @param string $src
	 * @param string $destfolder
	 * @param string $destthumbfolder
	 * @param boolean $createThumb
	 * @param array $thumbdim (width, height)
	 * @param array $altdim (alternate dimension -- eg. mid size)
	 * @param string $altfile (alternate file name prefix)
	 * @return array $retn_file, $retn_thm
	 */
	function moveTempFile($modstatus, $src, $destfolder, $destthumbfolder = "", $createThumb = false, $thumbdim = false, $altdim = false, $altfile = null){
		global $err, $_error, $_media;

		$err_occurred = false;
		$retn_file = "";
		$retn_thm = "";
		if($src != "" && $destfolder != "" && $modstatus != "deleted"){
			$sfile = basename($src);
			$ext = strtolower($this->getExtension($sfile));
			$image_types = $_media->getMediaTypes(array(MT_IMAGE, MT_ICON));
			$is_image = (in_array($ext, $image_types));
			if(is_null($altfile)){
				$dfile = preg_replace("/( |\*|\?|\&|\+|'|`|\%|=|\||\<|\>|:|\/|\\|\"|\#039;|\#39;|\#034;|\#34;|\#)/", "", $sfile);
			}else{
				$dfile = $altfile.".".$ext;
			}
			$srcfolder = dirname($src);
			if(substr($srcfolder, -1, 1) != "/") $srcfolder .= "/";
			if(substr($destfolder, -1, 1) != "/") $destfolder .= "/";
			$dest = $destfolder.$dfile;
			$this->chmod2(SITE_PATH.$srcfolder, CHMOD_TEMP_FOLDER);
			$this->chmod2(SITE_PATH.$destfolder, CHMOD_TEMP_FOLDER);
			$fs_err = "";

			if($createThumb){
				if(substr($destthumbfolder, -1, 1) != "/") $destthumbfolder .= "/";
				$this->chmod2(SITE_PATH.$destthumbfolder, CHMOD_TEMP_FOLDER);
				$srcthumb = UPLOAD_TEMPFOLDER."thumbs/thm_".$sfile;
				$destthumb = $destthumbfolder."thm_".$dfile;
			}

			if($modstatus == "unmodified"){
				// file has not yet been moved to destination (ie. new file)
				if($src != $dest){
					if(!file_exists(SITE_PATH.$destfolder)) {
						$_error->addErrorMsg("Destination '$destfolder' does not exist.");
						$err_occurred = true;
					}elseif(!file_exists(SITE_PATH.$src)) {
						$_error->addErrorMsg("Source file '$src' not found.  It might have already been moved if you reloaded this page.");
						$err_occurred = true;
					}else{
						if(file_exists(SITE_PATH.$dest)) unlink(SITE_PATH.$dest);
						$this->chmod2(SITE_PATH.$src, CHMOD_TEMP_FOLDER);
						if($is_image){
							// only work with images
							if(!UPLOAD_IMG_ORIG_RESIZE_TO_MAX){
								// destination image is not manipluated, and is moved as is
								if(!rename(SITE_PATH.$src, SITE_PATH.$dest)){
									$_error->addErrorMsg("Problem moving file to '{$dest}'");
									$err_occurred = true;
								}else{
									$retn_file = $dest;
								}
							}else{
								// destination image is resized (if required) before moving
								// we will use the generateSizedImage function with max image dimensions
								$imgdim = array(IMG_MAX_WIDTH, IMG_MAX_HEIGHT);
								$retn_file = $this->generateSizedImage(SITE_PATH.$src, SITE_PATH.$dest, $imgdim);
								$retn_file = substr($retn_file, strlen(SITE_PATH));
								if($retn_file) unlink(SITE_PATH.$src);
							}
						}else{
							// destination file is not manipluated, and is moved as is
							if(!rename(SITE_PATH.$src, SITE_PATH.$dest)){
								$_error->addErrorMsg("Problem moving file to '{$dest}'");
								$err_occurred = true;
							}else{
								$retn_file = $dest;
							}
						}
					}

					if($createThumb && !$err_occurred){
						// create thumbnail now.
						$this->chmod2(SITE_PATH.$dest, CHMOD_FOLDER);
						$retn_file = $dest;
						if($thumbdim === false) $thumbdim = array(THM_MAX_WIDTH, THM_MAX_HEIGHT);
						$retn_thm = generateSizedImage(SITE_PATH.$dest, SITE_PATH.$destthumb, $thumbdim, $altdim);
						$retn_thm = substr($retn_thm, strlen(SITE_PATH));
					}
				}else{
					// do nothing
					$retn_file = $dest;
					$retn_thm  = $destthumb;
				}
			}elseif($modstatus == "thumbmod"){
				// file is in src folder and fileupload temp thumbs folder
				// this status should only be set for thumbnail edits
				if($src != $dest){
					if(!file_exists(SITE_PATH.$destfolder)) {
						$_error->addErrorMsg("Destination '$destfolder' does not exist.");
						$err_occurred = true;
					}elseif(!file_exists(SITE_PATH.$src)) {
						$_error->addErrorMsg("Source file '$src' not found.  It might have already been moved if you reloaded this page.");
						$err_occurred = true;
					}else{
						if(file_exists(SITE_PATH.$dest)) unlink(SITE_PATH.$dest);
						$this->chmod2(SITE_PATH.$src, CHMOD_FOLDER);
						if(!rename(SITE_PATH.$src, SITE_PATH.$dest)){
							$_error->addErrorMsg("Problem moving file to '{$dest}'");
						}else{
							$this->chmod2(SITE_PATH.$dest, CHMOD_FOLDER);
							$retn_file = $dest;
						}
					}
				}else{
					// do nothing
					$retn_file = $dest;
				}

				if($srcthumb != $destthumb && $createThumb && !$err_occurred){
					if(!file_exists(SITE_PATH.$destthumbfolder)) {
						$_error->addErrorMsg("Destination '$destthumbfolder' does not exist.");
						$err_occurred = true;
					}elseif(!file_exists(SITE_PATH.$srcthumb)) {
						$_error->addErrorMsg("Source file '$srcthumb' not found.  It might have already been moved if you reloaded this page.");
						$err_occurred = true;
					}else{
						if(file_exists(SITE_PATH.$destthumb)) unlink(SITE_PATH.$destthumb);
						$this->chmod2(SITE_PATH.$srcthumb, CHMOD_FOLDER);
						if(!rename(SITE_PATH.$srcthumb, SITE_PATH.$destthumb)){
							$_error->addErrorMsg("Problem moving file to '{$destthumb}'");
						}else{
							$this->chmod2(SITE_PATH.$destthumb, CHMOD_FOLDER);
							$retn_thm = $destthumb;
						}
					}
				}else{
					// do nothing
					$retn_thm = $destthumb;
				}
			}else{
				// do nothing to either file
				$retn_file = $dest;
				$retn_thm = $destthumb;
			}

			// Some basic housecleaning
			$tmpfile = SITE_PATH.UPLOAD_TEMPFOLDER.$sfile;
			if(file_exists($tmpfile) !== false) unlink($tmpfile);
		}else{
			// return blanks since the input parameters were not complete
			$retn_file = "";
			$retn_thm = "";
		}

		return array($retn_file, $retn_thm);
	}

	/**
	 * Single-line image saving.
	 * @param array $imgfields 	image, lastimage, lastthumb
	 * @param string $destfolder
	 * @param string $destthumbfolder
	 * @param boolean $createThumb
	 * @param array $thumbdim (width, height)
	 * @param array $altdim (alternate dimension -- eg. mid size)
	 * @param string $altfile (alternate file name prefix)
	 * @return
	 */
	function saveImage($imgfields, $destfolder, $destthumbfolder = "", $createThumb = false, $thumbdim = false, $altdim = false, $altfile = null){
		global $_events;

		$image = getRequestVar($imgfields[0].'_fld');
		$image_mod = getRequestVar($imgfields[0].'_mod');

		$_events->processTriggerEvent(__FUNCTION__, $image);				// alert triggered functions when function executes
		if($image != "" && $image_mod != ""){
			$r = $this->moveTempFile($image_mod, $image, $destfolder, $destthumbfolder, $createThumb, $thumbdim, $altdim, $altfile);
		}elseif($image_mod != 'deleted' && count($imgfields) > 1){
			$r = array(getRequestVar($imgfields[1]), getRequestVar($imgfields[2]));
		}else{
			$r = array("", "");
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
		return $r;
	}

	/**
	 * Single-line file saving.
	 * @param array $filefields 	file, lastfile
	 * @param string $destfolder
	 * @param string $altfile (alternate file name prefix)
	 * @return
	 */
	function saveFile($filefields, $destfolder, $altfile = null){
		global $_events;

		$file = getRequestVar($filefields[0].'_fld');
		$file_mod = getRequestVar($filefields[0].'_mod');
		$_events->processTriggerEvent(__FUNCTION__, $file);				// alert triggered functions when function executes

		if($file != "" && $file_mod != ""){
			$r = $this->moveTempFile($file_mod, $file, $destfolder, "", false, false, false, $altfile);
		}elseif($file_mod != 'deleted' && count($filefields) > 1){
			$r = array(getRequestVar($filefields[1]));
		}else{
			$r = array("");
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
		return $r;
	}

	/**
	 * Checks the Uploader file modification flag and returns true if the file is unmodified
	 * 		(newly selected), or modified (changed using the imgeditor tool)
	 * return boolean
	 */
	function fileIsNew($filemodindicator){
		if($filemodindicator == 'unmodified' || $filemodindicator == 'modified')
			return true;
		else
			return false;
	}

	/**
	 * Return the location of the 404/403 error document handler
	 * @param string $type 			"404" or "403"
	 * @return mixed 				A file location, or false if nothing exists to handle error
	 */
	function getErrorDocFile($sitetype, $type){
		global $_themes, $_render;

		if($type != "404" && $type != "403") return false;

		if($sitetype == "website"){
			$themefolder = $_themes->getThemePathUnder("website");
			if(file_exists(SITE_PATH.$themefolder.$type.".php")){
				// 404.php/403.php first
				return SITE_PATH.$themefolder.$type.".php";
			}elseif($_render->setupPage($type) && file_exists(SITE_PATH.$themefolder."page.php")){
				// page.php as alternate
				return SITE_PATH.$themefolder."page.php";
			}
		}elseif($sitetype == "admin"){

		}
		return false;
	}

	/* --- Media System */

	/**
	 * Generate an image using GDLib or ImageMagick depending on library availability
	 * return $string 				filename
	 */
	function generateSizedImage($src, $dest, $dim1, $dim2 = false){
		if(isImageMagickLoaded())
			return $this->generateSizedImageIM($src, $dest, $dim1, $dim2);
		elseif(isGDLibLoaded())
			return $this->generateSizedImageGD($src, $dest, $dim1, $dim2);
	}

	/**
	 * Generate a sized image from image file using GD Lib
	 * @param string $src
	 * @param string $dest
	 * @param integer $dim1 		First image dimensions
	 * @param integer $dim2			Second image dimensions (optional)
	 * @return boolean|string 		Destination file or false
	 */
	function generateSizedImageGD($src, $dest, $dim1, $dim2 = false){
		global $err, $img_prefix, $_error;

		$newfilename = "";

		if($src != "" && $dest != ""){
			if(file_exists($src)){
			    //Get Image size info
			    list($orig_width, $orig_height, $image_type) = @getimagesize($src);

				//Create Image GD from src
			    switch ($image_type){
			        case UPLOAD_TYPE_GIF: $im = imagecreatefromgif($src); break;
			        case UPLOAD_TYPE_JPG: $im = imagecreatefromjpeg($src);  break;
			        case UPLOAD_TYPE_PNG: $im = imagecreatefrompng($src); break;
			        default: $_error->addErrorMsg(IMG_BAD_FORMAT); return false;
			    }

				$successes = 0;
				for($n = 1; $n < 3; $n++){
					if(${'dim'.$n} !== false){
						// file name that can be altered
						switch($n){
							case 1:
								// regular thumb/image resize
								$the_dest = $dest;
								break;
							case 2:
								// alternate thumb
								$the_dest = str_replace("/thm_", "/thm_a_", $dest);
								break;
						}

						// Get new aspect size for thumbnail
						$maxwidth = ${'dim'.$n}[0];
						$maxheight = ${'dim'.$n}[1];

						$width = $orig_width;
						$height = $orig_height;
						if($height > $maxheight && $maxheight > 0 && $width > 0) {
							$ratio = $maxheight / $height;
							$height = $maxheight;
							$width = intval($ratio * $width);
						}
						if($width > $maxwidth && $maxwidth > 0 && $height > 0) {
							$ratio = $maxwidth / $width;
							$width = $maxwidth;
							$height = intval($ratio * $height);
						}

						//create new image GD
						$newImg = imagecreatetruecolor($width, $height);
						if($newImg){
							//Check if this image is PNG or GIF, then set transparency
							if($image_type == UPLOAD_TYPE_GIF || $image_type == UPLOAD_TYPE_PNG){
								imagealphablending($newImg, false);
								imagesavealpha($newImg,true);
								$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
								imagefilledrectangle($newImg, 0, 0, $width, $height, $transparent);
							}
							$success = imagecopyresampled($newImg, $im, 0, 0, 0, 0, $width, $height, $orig_width, $orig_height);

							if($success){
								//Save file
								if(file_exists($the_dest)) unlink($the_dest);
								switch ($image_type){
									case UPLOAD_TYPE_GIF: $success = imagegif($newImg, $the_dest); break;
									case UPLOAD_TYPE_JPG: $success = imagejpeg($newImg, $the_dest, 100); break;
									case UPLOAD_TYPE_PNG: $success = imagepng($newImg, $the_dest, 0); break;
									default: $_error->addErrorMsg(IMG_RESIZE_ERROR); return false;
								}

								if($success){
									$this->chmod2($the_dest, CHMOD_FILE);
									$successes++;
								}else{
									$_error->addErrorMsg(IMG_FAILURE_COPY.$the_dest);
								}
							}else{
								$_error->addErrorMsg(IMG_RESIZE_ERROR);
							}
						}elseif($n == 1){
							$_error->addErrorMsg(IMG_CREATE_ERROR);
						}
					}
				}
				if($successes > 0) return $the_dest;			// at least one file was created ok
			}else{
				$_error->addErrorMsg(IMG_NOT_FOUND.$src);
			}
		}else{
			$_error->addErrorMsg(MISSING_ARG);
		}
		return false;
	}

	/**
	 * Generate a sized image from image file using ImageMagick
	 * @param string $src
	 * @param string $dest
	 * @param integer $dim1 		First image dimensions
	 * @param integer $dim2			Second image dimensions (optional)
	 * @return boolean|string 		Destination file or false
	 */
	function generateSizedImageIM($src, $dest, $dim1, $dim2 = false){
		global $err, $img_prefix, $_error;

		$newfilename = "";

	    //Check if GD extension is loaded
	    if (!extension_loaded('gd') && !extension_loaded('gd2')){
	        $_error->addErrorMsg("GD Lib is not loaded.");
	        return false;
	    }

		if($src != "" && $dest != ""){
			if(file_exists($src)){
			    //Get Image size info
			    list($orig_width, $orig_height, $image_type) = @getimagesize($src);

				//Create Image GD from src
			    switch ($image_type){
			        case UPLOAD_TYPE_GIF: $im = imagecreatefromgif($src); break;
			        case UPLOAD_TYPE_JPG: $im = imagecreatefromjpeg($src);  break;
			        case UPLOAD_TYPE_PNG: $im = imagecreatefrompng($src); break;
			        default: $_error->addErrorMsg(IMG_BAD_FORMAT); return false;
			    }

				$successes = 0;
				for($n = 1; $n < 3; $n++){
					if(${'dim'.$n} !== false){
						// file name that can be altered
						switch($n){
							case 1:
								// regular thumb/image resize
								$the_dest = $dest;
								break;
							case 2:
								// alternate thumb
								$the_dest = str_replace("/thm_", "/thm_a_", $dest);
								break;
						}

						// Get new aspect size for thumbnail
						$maxwidth = ${'dim'.$n}[0];
						$maxheight = ${'dim'.$n}[1];

						$width = $orig_width;
						$height = $orig_height;
						if($height > $maxheight && $maxheight > 0 && $width > 0) {
							$ratio = $maxheight / $height;
							$height = $maxheight;
							$width = intval($ratio * $width);
						}
						if($width > $maxwidth && $maxwidth > 0 && $height > 0) {
							$ratio = $maxwidth / $width;
							$width = $maxwidth;
							$height = intval($ratio * $height);
						}

						//create new image GD
						$newImg = imagecreatetruecolor($width, $height);
						if($newImg){
							/* Check if this image is PNG or GIF, then set if Transparent*/
							if($image_type == UPLOAD_TYPE_GIF || $image_type == UPLOAD_TYPE_PNG){
								imagealphablending($newImg, false);
								imagesavealpha($newImg,true);
								$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
								imagefilledrectangle($newImg, 0, 0, $width, $height, $transparent);
							}
							$success = imagecopyresampled($newImg, $im, 0, 0, 0, 0, $width, $height, $orig_width, $orig_height);

							if($success){
								//Save file
								if(file_exists($the_dest)) unlink($the_dest);
								switch ($image_type){
									case UPLOAD_TYPE_GIF: $success = imagegif($newImg, $the_dest); break;
									case UPLOAD_TYPE_JPG: $success = imagejpeg($newImg, $the_dest, 100); break;
									case UPLOAD_TYPE_PNG: $success = imagepng($newImg, $the_dest, 0); break;
									default: $_error->addErrorMsg(IMG_RESIZE_ERROR); return false;
								}

								if($success){
									$this->chmod2($the_dest, CHMOD_FILE);
									$successes++;
								}else{
									$_error->addErrorMsg(IMG_FAILURE_COPY.$the_dest);
								}
							}else{
								$_error->addErrorMsg(IMG_RESIZE_ERROR);
							}
						}elseif($n == 1){
							$_error->addErrorMsg(IMG_CREATE_ERROR);
						}
					}
				}
				if($successes > 0) return $the_dest;			// at least one file was created ok
			}else{
				$_error->addErrorMsg(IMG_NOT_FOUND.$src);
			}
		}else{
			$_error->addErrorMsg(MISSING_ARG);
		}
		return false;
	}

	/**
	 * Resize an image
	 * @param string $src
	 * @param string $target
	 * @param string $dest
	 * @param array $alt_max_size
	 * @return string|boolean
	 */
	function resizeImage($src, $target, $dest, $alt_max_size){
		global $err, $_error, $_events;

		$newfilename = "";

		$_events->processTriggerEvent(__FUNCTION__, $src);				// alert triggered functions when function executes
		if($src != "" && $target != "" && $dest != ""){
			$src = $dest.$src;
			if(file_exists($src)){
			    //Get Image size info
			    list($orig_width, $orig_height, $image_type) = @getimagesize($src);

				//Create Image GD from src
			    switch ($image_type){
			        case TYPE_GIF: $im = imagecreatefromgif($src); break;
			        case TYPE_JPG: $im = imagecreatefromjpeg($src);  break;
			        case TYPE_PNG: $im = imagecreatefrompng($src); break;
			        default: $_error->addErrorMsg(IMG_BAD_FORMAT); return false;
			    }

				// Get new aspect size for image
				if ($alt_max_size['h'] > 0 && $orig_height >= $orig_width && $orig_height > 0) {
					// if $height > IMG_MAX_HEIGHT, get reducer ratio and multiple by $width
					if($orig_height != $alt_max_size['h']) {
						$ratio = $alt_max_size['h'] / $orig_height;
						$height = $alt_max_size['h'];
						$width = intval($ratio * $orig_width);
					}else{
						$width = $orig_width;
						$height = $orig_height;
					}
				}
				if ($alt_max_size['w'] > 0 && $orig_width >= $orig_height && $orig_width > 0) {
					// if $width > IMG_MAX_WIDTH, get reducer ratio and multiple by $height
					if($orig_width != $alt_max_size['w']) {
						$ratio = $alt_max_size['w'] / $orig_width;
						$width = $alt_max_size['w'];
						$height = intval($ratio * $orig_height);
					}else{
						$width = $orig_width;
						$height = $orig_height;
					}
				}

				//create new image GD
			    $newImg = imagecreatetruecolor($width, $height);
			   	if($newImg){
				    /* Check if this image is PNG or GIF, then set if Transparent*/
				    if($image_type == TYPE_GIF || $image_type == TYPE_PNG){
				        imagealphablending($newImg, false);
				        imagesavealpha($newImg,true);
				        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
				        imagefilledrectangle($newImg, 0, 0, $width, $height, $transparent);
				    }
				    $success = imagecopyresampled($newImg, $im, 0, 0, 0, 0, $width, $height, $orig_width, $orig_height);

				   	if($success){
					    //Generate the file, and save it to $newfilename
						$target = preg_replace("/[\(\) \\\*\?\'\"/]/i", "_", $target);
						$newfilename = $dest.$target;
						if(file_exists($newfilename)) @unlink($newfilename);
					    switch ($image_type){
					        case TYPE_GIF: $success = imagegif($newImg, $newfilename); break;
					        case TYPE_JPG: $success = imagejpeg($newImg, $newfilename); break;
					        case TYPE_PNG: $success = imagepng($newImg, $newfilename); break;
					        default: $_error->addErrorMsg(IMG_RESIZE_ERROR); return false;
					    }

						if($success){
							$this->chmod2($newfilename, CHMOD_FILE);
							unlink($src);
							$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
						    return $newfilename;
						}else{
							$_error->addErrorMsg(IMG_FAILURE_COPY.$newfilename);
						}
					}else{
						$_error->addErrorMsg(IMG_RESIZE_ERROR);
					}
				}else{
					$_error->addErrorMsg(IMG_CREATE_ERROR);
				}
			}else{
				$_error->addErrorMsg(IMG_NOT_FOUND.$src);
			}
		}else{
			$_error->addErrorMsg(MISSING_ARG);
		}
		return false;
	}
}

?>