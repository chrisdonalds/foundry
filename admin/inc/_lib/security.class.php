<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("SECLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/* SECURITY GLOBAL CLASS */
// Portable PHP password hashing framework.
//
// Version 0.3 / genuine.
//
// Written by Solar Designer <solar at openwall.com> in 2004-2006 and placed in
// the public domain.  Revised in subsequent years, still public domain.
//
// There's absolutely no warranty.
//
// The homepage URL for this framework is:
//
//	http://www.openwall.com/phpass/
//
// Please be sure to update the Version line if you edit this file in any way.
// It is suggested that you leave the main version number intact, but indicate
// your project name (after the slash) and add your own revision information.
//
// Please do not change the "private" password hashing method implemented in
// here, thereby making your hashes incompatible.  However, if you must, please
// change the hash type identifier (the "$P$") to something different.
//
// Obviously, since this code is in the public domain, the above are not
// requirements (there can be none), but merely suggestions.
/**
 * @author Solar Designer
 * @copyright Copyright (c) 2010, Alexander Peslyak
 * @license http://www.opensource.org/licenses/mit-license.php MIT
 */

class SecurityClass {
	var $itoa64;
	var $iteration_count_log2;
	var $portable_hashes;
	var $random_state;

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$_events->processTriggerEvent(__FUNCTION__.'_security');				// alert triggered functions when function executes
		return $s;
	}

	function SecurityClass($iteration_count_log2 = 8, $portable_hashes = false) {
		$this->itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

		if ($iteration_count_log2 < 4 || $iteration_count_log2 > 31)
			$iteration_count_log2 = 8;
		$this->iteration_count_log2 = $iteration_count_log2;

		$this->portable_hashes = $portable_hashes;

		$this->random_state = microtime();
		if (function_exists('getmypid'))
			$this->random_state .= getmypid();
	}

	function get_random_bytes($count) {
		$output = '';
		if (is_readable('/dev/urandom') &&
		    ($fh = @fopen('/dev/urandom', 'rb'))) {
			$output = fread($fh, $count);
			fclose($fh);
		}

		if (strlen($output) < $count) {
			$output = '';
			for ($i = 0; $i < $count; $i += 16) {
				$this->random_state =
				    md5(microtime() . $this->random_state);
				$output .=
				    pack('H*', md5($this->random_state));
			}
			$output = substr($output, 0, $count);
		}

		return $output;
	}

	function encode64($input, $count) {
		$output = '';
		$i = 0;
		do {
			$value = ord($input[$i++]);
			$output .= $this->itoa64[$value & 0x3f];
			if ($i < $count)
				$value |= ord($input[$i]) << 8;
			$output .= $this->itoa64[($value >> 6) & 0x3f];
			if ($i++ >= $count)
				break;
			if ($i < $count)
				$value |= ord($input[$i]) << 16;
			$output .= $this->itoa64[($value >> 12) & 0x3f];
			if ($i++ >= $count)
				break;
			$output .= $this->itoa64[($value >> 18) & 0x3f];
		} while ($i < $count);

		return $output;
	}

	function gensalt_private($input) {
		$output = '$P$';
		$output .= $this->itoa64[min($this->iteration_count_log2 +
			((PHP_VERSION >= '5') ? 5 : 3), 30)];
		$output .= $this->encode64($input, 6);
		return $output;
	}

	function crypt_private($password, $setting) {
		$output = '*0';
		if (substr($setting, 0, 2) == $output)
			$output = '*1';

		$id = substr($setting, 0, 3);
		// We use "$P$", phpBB3 uses "$H$" for the same thing
		if ($id != '$P$' && $id != '$H$')
			return $output;

		$count_log2 = strpos($this->itoa64, $setting[3]);
		if ($count_log2 < 7 || $count_log2 > 30)
			return $output;

		$count = 1 << $count_log2;

		$salt = substr($setting, 4, 8);
		if (strlen($salt) != 8)
			return $output;

		// We're kind of forced to use MD5 here since it's the only
		// cryptographic primitive available in all versions of PHP
		// currently in use.  To implement our own low-level crypto
		// in PHP would result in much worse performance and
		// consequently in lower iteration counts and hashes that are
		// quicker to crack (by non-PHP code).
		if (PHP_VERSION >= '5') {
			$hash = md5($salt . $password, TRUE);
			do {
				$hash = md5($hash . $password, TRUE);
			} while (--$count);
		} else {
			$hash = pack('H*', md5($salt . $password));
			do {
				$hash = pack('H*', md5($hash . $password));
			} while (--$count);
		}

		$output = substr($setting, 0, 12);
		$output .= $this->encode64($hash, 16);
		return $output;
	}

	function gensalt_extended($input) {
		$count_log2 = min($this->iteration_count_log2 + 8, 24);
		// This should be odd to not reveal weak DES keys, and the
		// maximum valid value is (2**24 - 1) which is odd anyway.
		$count = (1 << $count_log2) - 1;

		$output = '_';
		$output .= $this->itoa64[$count & 0x3f];
		$output .= $this->itoa64[($count >> 6) & 0x3f];
		$output .= $this->itoa64[($count >> 12) & 0x3f];
		$output .= $this->itoa64[($count >> 18) & 0x3f];

		$output .= $this->encode64($input, 3);

		return $output;
	}

	function gensalt_blowfish($input) {
		// This one needs to use a different order of characters and a
		// different encoding scheme from the one in encode64() above.
		// We care because the last character in our encoded string will
		// only represent 2 bits.  While two known implementations of
		// bcrypt will happily accept and correct a salt string which
		// has the 4 unused bits set to non-zero, we do not want to take
		// chances and we also do not want to waste an additional byte
		// of entropy.
		$itoa64 = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

		$output = '$2a$';
		$output .= chr(ord('0') + $this->iteration_count_log2 / 10);
		$output .= chr(ord('0') + $this->iteration_count_log2 % 10);
		$output .= '$';

		$i = 0;
		do {
			$c1 = ord($input[$i++]);
			$output .= $itoa64[$c1 >> 2];
			$c1 = ($c1 & 0x03) << 4;
			if ($i >= 16) {
				$output .= $itoa64[$c1];
				break;
			}

			$c2 = ord($input[$i++]);
			$c1 |= $c2 >> 4;
			$output .= $itoa64[$c1];
			$c1 = ($c2 & 0x0f) << 2;

			$c2 = ord($input[$i++]);
			$c1 |= $c2 >> 6;
			$output .= $itoa64[$c1];
			$output .= $itoa64[$c2 & 0x3f];
		} while (1);

		return $output;
	}

	function getHash($password) {
		$random = '';

		if (CRYPT_BLOWFISH == 1 && !$this->portable_hashes) {
			$random = $this->get_random_bytes(16);
			$hash =
			    crypt($password, $this->gensalt_blowfish($random));
			if (strlen($hash) == 60)
				return $hash;
		}

		if (CRYPT_EXT_DES == 1 && !$this->portable_hashes) {
			if (strlen($random) < 3)
				$random = $this->get_random_bytes(3);
			$hash =
			    crypt($password, $this->gensalt_extended($random));
			if (strlen($hash) == 20)
				return $hash;
		}

		if (strlen($random) < 6)
			$random = $this->get_random_bytes(6);
		$hash =
		    $this->crypt_private($password,
		    $this->gensalt_private($random));
		if (strlen($hash) == 34)
			return $hash;

		// Returning '*' on error is safe here, but would _not_ be safe
		// in a crypt(3)-like function used _both_ for generating new
		// hashes and for validating passwords against existing hashes.
		return '*';
	}

	function validatePassword($password, $stored_hash) {
		global $_events;

		$hash = $this->crypt_private($password, $stored_hash);
		if ($hash[0] == '*'){
			$hash = crypt($password, $stored_hash);
        }

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
		return $hash == $stored_hash;
	}

	// --- Foundry Methods ---

	/**
	 * Generate ciphergraphic random-salted hash
	 * @param string $plainText
	 * @param boolean $forcelower       True to force the use of a weaker protable hash,
	 *                                  false to use stronger, system-specific hashing (with automatic fallback to weaker hash)
	 * @param integer $entropy          The number of iterations used for password hash stretching
	 * @return string
	 */
	function createHash($plainText, $forcelower = false, $entropy = 8){
	    $this->SecurityClass($entropy, $forcelower);
	    return $this->getHash($plainText);
	}

	/**
	 * Check password against stored set of hashes
	 * @param string $username
	 * @param string $password
	 * @param boolean $forcelower       True to force the use of a weaker portable hash,
	 *                                  false to use stronger, system-specific hashing (with automatic fallback to weaker hash)
	 * @param integer $entropy          The number of iterations used for password hash stretching
	 * @return mixed
	 */
	function checkPassword($username, $password, $forcelower = false, $entropy = 8){
	    global $_db_control, $_events;

	    $accts = $_db_control->setTable(ACCOUNTS_TABLE)->setWhere("(username = '$username' OR email = '$username')")->getRec();
	    if(count($accts) > 0) {
	        $this->SecurityClass($entropy, $forcelower);
	        foreach($accts as $acct){
	            $ok = $this->validatePassword($password, $acct['password']);
	            if($ok) return $acct;
	        }
	    }
	    return null;
	}

	/**
	 * Generate a one-way ciphergraphic, cyclical-salted hash from the plain text
	 * @param string $plainText
	 * @param string $salt [optional]
	 * @param string $encdata [optional]
	 * @return string hash
	 */
	function createCipher($plainText, $salt = null, $encdata = false, $cipherType = 'blowfish', $strength = '08'){
	    if(empty($strength)) $strength = '08';

	    switch(strtolower($cipherType)){
	        case 'blowfish':
	            $prefix = '$2a$'.$strength.'$';
	            break;
	        case 'md5':
	            $prefix = '$1$';
	            break;
	        case 'sha256':
	            $prefix = '$5$rounds='.intval($strength).'$';
	            break;
	        case 'sha512':
	            $prefix = '$6$rounds='.intval($strength).'$';
	            break;
	        default:
	            return false;
	    }

	    // if encrypted data is passed, check it against input ($plainText)
	    if ($encdata) {
	        // use Crypt_Blowfish algorithm
	        if (substr($encdata, 0, 60) == crypt($plainText, $prefix.substr($encdata, 60))) {
	            return true;
	        } else {
	            return false;
	        }
	    } else {
	        // make a salt and hash it with input, and add salt to end
	        if(is_null($salt) || $salt == ""){
	            $salthash = "";
	            for ($i = 0; $i < 22; $i++) {
	                $salthash .= substr("./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", mt_rand(0, 63), 1);
	            }
	        }else{
	            if(strlen($salt) < 22) $salt = str_repeat($salt, intval(22 / strlen($salt)) + 1);
	            $salthash = substr($salt, 0, 22);
	        }

	        //return 82 char string (60 char hash & 22 char salt)
	        return crypt($plainText, $prefix.$salthash).$salthash;
	    }
	}

	/**
	 * Create a nonce (Number used ONCE) value used to validate form data
	 * @param string $param             Leave blank to have createNonce calculate the input parameter
	 * @param integer $ttl              Nonce "time-to-live".  Default = 600 seconds (10 minutes)
	 * @param integer $Length 			A number between 30 seconds and 12 hours in which nonce remains valid.
	 */
	function createNonce($param = '', $ttl = 600, $length = 32){
	    global $_users, $_page, $_error;

	    $length = intval($length);
	    if($length < 1 || $length > 128) $length = 32;
	    if($ttl < 30 || $ttl > 43200) $ttl = 600;           // ttl less than 0.5 minutes or more than 12 hours, set to 10 minutes
	    if(is_string($param)){
	    	if(isset($_page))
	        	$salt = md5($_page->title);
	        else
	        	$salt = STATIC_SALT;
	        if(!empty($param)){
	            // use specific parameter
	            $nonce = $this->createUniqueID($param, $salt, $length);
	            return $nonce;
	        }else{
	            // use system-generated parameter
	            $exptime = intval(strtotime("now") / $ttl);
			    $path = explode("?", $_page->uri);
	            if(IN_ADMIN){
	                $nonce = $this->createUniqueID(SYS_NAME.$_users->id.$path[0].$exptime, $salt, $length);
	            }else{
	                $nonce = $this->createUniqueID(SYS_NAME.session_id().$path[0].$exptime, $salt, $length);
	            }
	            return $nonce;
	        }
	    }else{
	        $_error->addErrorMsg("CreateNonce expects a string parameter.");
	        return false;
	    }
	}

	/**
	 * Check nonce value against expected value
	 * @param string $nonce             Leave blank to use the nonce received from the referring URI
	 * @param string $nonceparam        Leave blank to use the nonce from the $_page object
	 */
	function validateNonce($nonce = '', $nonceparam = ''){
	    global $_users, $_page, $_error;

	    if(is_string($nonce)){
	        // first check if nonces match
	        if(empty($nonce)) $nonce = $_REQUEST['_n'];
	        if(!empty($nonceparam))
	            $checknonce = $this->createNonce($nonceparam);
	        else
	            $checknonce = $_page->nonce;

	        $valid = false;
	        if(!empty($nonce) && !empty($checknonce))
	        	$valid = ($nonce == $checknonce);

	        if(!$valid)
	        	$_error->addErrorMsg("The requested action is not authorized. Perhaps the request was already performed or too much time passed", RUNTIME_ERR);
	        return $valid;
	    }else{
	        $_error->addErrorMsg("ValidateNonce expects a string parameter.");
	        return false;
	    }
	}

	/**
	 * Create an encrypted string (security level 2)
	 * @param string $length
	 * @return string
	 */
	function createEncString($length = 8){
	    // get random character length
	    $string = '';
	    $index = md5(uniqid(mt_rand(), true));
	    // loop random times defined by $length
	    for ($i=0; $i < $length; $i++) {
	        // get random character index
	        $string .= $index[mt_rand(0, strlen($index) - 1)];
	    }
	    return $string;
	}

	/**
	 * Create a password (security level 2)
	 * @param string $length
	 * @return string
	 */
	function createPassword($length = 8, $opts = null){
	    # get random character length
	    $string = '';
	    if(empty($opts))
	    	$index = str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()[]{}');
	    else{
	    	$key = "";
	    	if(in_array("a-z", $opts)) $key .= "abcdefghijklmnopqrstuvwxyz";
	    	if(in_array("0-9", $opts)) $key .= "0123456789";
	    	if(in_array("symbols", $opts)) $key .= "!@#$%^&*()[]{}";
	    	if(in_array("upper/lower", $opts)) $key .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    	$index = str_shuffle($key);
	    }
	    # loop random times defined by $length
	    for ($i=0; $i < $length; $i++) {
	        # get random character index
	        $string .= $index[mt_rand(0, strlen($index) - 1)];
	    }
	    return $string;
	}

	/**
	 * Create a unique ID using sha512 encryption
	 * @param string $id                If $id is NULL, the generated string will be random.
	 * @param string $salt
	 * @param string $length            Length of the generated string. If NULL, the default 128 characters is used.
	 * @return string
	 */
	function createUniqueID($id = NULL, $salt = NULL, $length = NULL){
	    $id = ($id == NULL) ? uniqid(hash("sha512", rand()), TRUE) : $id;
	    $code = hash("sha512", $id.$salt);
	    return $length == NULL ? $code : substr($code, 0, $length);
	}

	/**
	 * Create a unique GUID using com_create_guid or md5+uniqid
	 * @return string
	 */
	function getGUID(){
	    if (function_exists('com_create_guid')){
	        return com_create_guid();
	    }else{
	        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
	        $charid = strtoupper(md5(uniqid(rand(), true)));
	        $hyphen = chr(45);// "-"
	        $uuid = chr(123)// "{"
	            .substr($charid, 0, 8).$hyphen
	            .substr($charid, 8, 4).$hyphen
	            .substr($charid,12, 4).$hyphen
	            .substr($charid,16, 4).$hyphen
	            .substr($charid,20,12)
	            .chr(125);// "}"
	        return $uuid;
	    }
	}

	/**
	 * Use hash_hmac (if present) or custom hmac hasher if not.
	 * @param string $algo
	 * @param string $data
	 * @param string $key
	 * @param boolean $raw_output
	 */
	function getHMAC($algo, $data, $key, $raw_output = false){
	    if(function_exists('hash_hmac')){
	        return hash_hmac($algo, $data, $key);
	    }else{
	        $algo = strtolower($algo);
	        $pack = 'H'.strlen($algo('test'));
	        $size = 64;
	        $opad = str_repeat(chr(0x5C), $size);
	        $ipad = str_repeat(chr(0x36), $size);

	        if(strlen($key) > $size){
	            $key = str_pad(pack($pack, $algo($key)), $size, chr(0x00));
	        }else{
	            $key = str_pad($key, $size, chr(0x00));
	        }

	        for ($i = 0; $i < strlen($key) - 1; $i++){
	            $opad[$i] = $opad[$i] ^ $key[$i];
	            $ipad[$i] = $ipad[$i] ^ $key[$i];
	        }

	        $output = $algo($opad.pack($pack, $algo($ipad.$data)));
	        return ($raw_output) ? pack($pack, $output) : $output;
	    }
	}

	/**
	 * Generates a 320 bit private key: The SHA-1 hash function is preformed on the UNIX epoch and an additional random 750 to 1000 bits of entropy generated by the /dev/urandom algorithm.
	 * @return string               Hashed private key
	 */
	function getPrivateKey() {
	    $privateKey = sha1(time().shell_exec('head -c '.mt_rand(750,1000).' < /dev/urandom'));
	    return $privateKey;
	}

	/**
	 * Generates a 256 bit public key: The MD5 hash function is performed on the UNIX epoch and an additional random amount of entropy from the /urandom function.
	 * @return string               Hashed public key
	 */
	function getPublicKey() {
	    $publicKey = md5(time().shell_exec('head -c '.mt_rand(250,350).' < /dev/urandom'));
	    return $publicKey;
	}

	/**
	 * Returns an encrypted cipherstream provided plaintext and a private key.
	 * @param string $plainText     String to encrypt
	 * @param string $privateKey    Private key string
	 * @return string               Encrypted string
	 */
	function getEncrypted($plainText, $privateKey) {
	    $publicKey = $this->getPublicKey();
	    $textArray = str_split($plainText);

	    $shiftKeyArray = array();
	    for($i=0; $i<ceil(sizeof($textArray)/40); $i++)
	        array_push($shiftKeyArray, sha1($privateKey.$i.$publicKey));
	    $shiftKey = join("", $shiftKeyArray);

	    $cipherTextArray = array();
	    for($i=0; $i<sizeof($textArray); $i++) {
	        $cipherChar = ord($textArray[$i]) + ord(substr($shiftKey, $i, 1));
	        $cipherChar -= floor($cipherChar/255) * 255;
	        $cipherTextArray[$i] = dechex($cipherChar);
	    }

	    unset($textarray);
	    unset($shiftKeyArray);
	    unset($cipherChar);

	    $cipherStream = implode("", $cipherTextArray).":".$publicKey;

	    unset($publicKey);
	    unset($cipherTextArray);

	    return $cipherStream;
	}

	/**
	 * Returns plaintext given the cipherstream and the same private key used to make it.
	 * @param string $cipherStream      The hash pair to decrypt
	 * @param string $privateKey        The private key
	 * @return string                   Decrypted string
	 */
	function getDecrypted($cipherStream, $privateKey) {
	    $cipherStreamArray = explode(":", $cipherStream);
	    unset($cipherStream);
	    $cipherText = $cipherStreamArray[0];
	    $publicKey = $cipherStreamArray[1];
	    unset($cipherStreamArray);

	    $cipherTextArray = array();
	    for($i=0; $i<strlen($cipherText); $i+=2)
	        array_push($cipherTextArray, substr($cipherText, $i, 2));
	    unset($cipherText);

	    $shiftKeyArray = array();
	    for($i=0; $i<ceil(sizeof($cipherTextArray)/40); $i++)
	        array_push($shiftKeyArray, sha1($privateKey.$i.$publicKey));
	    unset($privateKey);
	    unset($publicKey);

	    $plainChar = null;
	    $plainTextArray = array();
	    $shiftKey = join("", $shiftKeyArray);
	    for($i=0; $i<sizeof($cipherTextArray); $i++){
	        $plainChar = hexdec($cipherTextArray[$i]) - ord(substr($shiftKey, $i, 1));
	        $plainChar -= floor($plainChar/255) * 255;
	        $plainTextArray[$i] = chr($plainChar);
	    }

	    unset($cipherTextArray);
	    unset($shiftKeyArray);
	    unset($plainChar);

	    $plainText = implode("", $plainTextArray);
	    return $plainText;
	}

	/**
	 * Return the strength and number between 0 and 5 indicating the strength of the provided password.
	 * @param string $pwd
	 * @return array
	 */
	function checkPasswordStrength($pwd) {
	    $strength = array("Blank", "Very Weak", "Weak", "Medium", "Strong", "Very Strong");
	    $score = 1;

	    if (strlen($pwd) < 1)
	        return array($strength[0], 0);

	    if (strlen($pwd) < 4)
	        return array($strength[1], 1);

	    if (strlen($pwd) >= 8)
	        $score++;

	    if (strlen($pwd) >= 10)
	        $score++;

	    if (preg_match("/[a-z]/", $pwd) && preg_match("/[A-Z]/", $pwd))
	        $score++;

	    if (preg_match("/[0-9]/", $pwd))
	        $score++;

	    if (preg_match("/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/", $pwd))
	        $score++;

	    if($score > 5) $score = 5;

	    return array($strength[$score], $score);
	}
}

?>
