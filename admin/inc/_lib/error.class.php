<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("ERRORLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

define ("ERR_LOG", 0);
define ("STAT_ERR", 1);
define ("CORE_ERR", 2);
define ("RUNTIME_ERR", 4);
define ("DEBUGGER_ERR", 8);

/**
 * ERRORCLASS
 * Synchronizes and administers error trapping and debugging routines
 */
class ErrorClass {
	// overloaded data
	private $_db = null;
	public $err_suppress = false;
	public $err = array();
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control;

		$s = new self;

		// error detection trapping level (set in configs.php)
		// - Standard = E_ERROR | E_WARNING | E_PARSE
		// - Strict = E_ALL
		error_reporting(ERROR_SENSITIVITY);
		if(floatval(phpversion()) >= 5) set_error_handler(array($s, "customErrorHandler"));
		$s->_db = $_db_control;
		$s->initErrorMsg();
		return $s;
	}

	/**
	 * Debugger-aware dbError output
	 * Debugger-aware means that this function will only execute if the debugger system is active
	 * @return mixed dbError
	 */
	public function _db_error(){
		global $_debug, $_events;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	    if($_debug->debugger) {
	    	_log(dbError());
	        return dbError();
	    }
	}

	/**
	 * Error logger
	 * @param string $msg
	 */
	public function _log($msg){
		if($msg != '') $this->submitErrorLog($msg, false);
	}

	/**
	 * Sets the state of the $this->err_suppress setting.  $this->err_suppress is used by
	 * customErrorHandler() to hide output of errors that aren't suppressed by PHP's @ operator.
	 * @param boolean $state
	 */
	public function setErrorSuppression($state = true){
	    $this->err_suppress = ($state === TRUE);
	}

	/**
	 * Custom error handler
	 * @param integer $errno
	 * @param string $errstr
	 * @param string $errfile
	 * @param integer $errline
	 * @return boolean
	 */
	public function customErrorHandler($errno, $errstr, $errfile, $errline){
		global $_events;

	    $terminate = false;
	    $outp = "";
	    if(!$this->err_suppress){
	        switch ($errno) {
	            case E_COMPILE_ERROR:
	            case E_RECOVERABLE_ERROR:
	    		case E_USER_ERROR:
	            case E_STRICT:
	    		case E_ERROR:
	    			$outp .= "<b>CRITICAL ERROR</b> [$errno] $errstr. ";
	    			$outp .= "  Critical error on line $errline in file $errfile\n";
	    			$outp .= "Aborting...<br/>\n";
	                $terminate = true;
	    			break;

	    		case E_USER_WARNING:
	    		case E_WARNING:
	    			$outp .= "<b>WARNING</b> [$errno] $errstr. ";
	    			$outp .= "  Warning on line $errline in file $errfile<br/>\n";
	    			break;

	    		case E_USER_NOTICE:
	    		case E_NOTICE:
	    			$outp .= "<b>NOTICE</b> [$errno] $errstr. ";
	    			$outp .= "  Notice on line $errline in file $errfile<br/>\n";
	    			break;

	    		default:
	    			$outp .= "<b>UNKNOWN error type</b>: [$errno] $errstr. ";
	    			$outp .= "  Error on line $errline in file $errfile<br/>\n";
	    			break;
	    	}
	        if($outp != ''){
	            echo "<div style=\"clear: both; position: relative; border: 1px solid black; background: white; padding: 2px;\">\n";
	            echo $outp."</div>\n";
	            $this->submitErrorLog($outp);
	        }
	    }

		$_events->processTriggerEvent('error_occurred', array($errno, $errstr, $errfile, $errline));				// alert triggered functions when function executes
	    if($terminate){
	        exit(1);
	    }else{
	        /* Don't execute PHP's internal error handler */
	        return true;
	    }
	}

	/**
	 * Submit error log message to either file or email
	 * @param string $msg
	 * @param boolean $withDate
	 */
	public function submitErrorLog($msg, $withDate = true){
	    if($withDate) $msg = date("Y-m-d h:i:s")." - ".str_replace("<br/>", PHP_EOL, $msg);
	    if(ERROR_LOG_TYPE == 0){
	        // to console
	        error_log($msg, 0);
	    }elseif(ERROR_LOG_TYPE == 1){
	        // to email
	        error_log($msg, 1, ADMIN_EMAIL);
	    }elseif(ERROR_LOG_TYPE == 3){
	        // to file
	        $file = SITE_PATH.ADMIN_FOLDER.CACHE_FOLDER."error.log";
	        error_log($msg, 3, $file);
	    }

	    // store log
	    $this->err[ERR_LOG][date("Y-m-d h:i:s")] = $msg;
	}

	/**
	 * Prepare a database error message line
	 * @param string $file
	 * @param string $line
	 * @param string $query
	 * @param string $error
	 * @return string
	 */
	public function strucDBErrorMsg($file, $line, $query, $error){
	    return "DB ERROR in filename: ".$file." at Line: ".$line." (".str_replace(array("\\n", "\\r", "\\n\\r"), "", $query).") Problem: ".$error.PHP_EOL;
	}

	/**
	 * Save a log record to the event log table
	 * @param integer $user_id
	 * @param string $source
	 * @param string $category
	 * @param string $type
	 * @param string $notes
	 * @return integer          false if failed
	 */
	public function saveEventLog($user_id = 0, $source = "Core", $category = null, $type = "information", $notes = null){
		global $_users;

	    $user_id = abs(intval($user_id));
	    if(!in_array($type, array("information", "notice", "warning", "critical"))){
	        $this->addErrorMsg("Invalid event log type '$type'");
	        return false;
	    }
	    $category = addslashes($category);
	    $notes = addslashes($notes);
	    $source = addslashes($source);
	    $ipaddress = $_users->getClientIP();
	    $id = $this->_db->setTable(EVENT_LOG_TABLE)->setFieldvals(array("date" => date("Y-m-d H:i:s"), "source" => $source, "category" => $category, "type" => $type, "user_id" => $user_id, "ip_address" => $ipaddress, "notes" => $notes))->insertRec();
	    return ($id !== false);
	}

	/**
	 * Return records from the Event Log table
	 * @param array $atts           Criteria attributes (category, source, type, user_id, date, order, limit)
	 * @return array                Resulting records
	 */
	public function getEventLog($atts = array()){
	    $crit = '';
	    if(isset($atts['category'])) $crit .= (($crit != '') ? ' AND ' : '')."`category` LIKE '%".prepareTextForDB($atts['category'])."%'";
	    if(isset($atts['source'])) $crit .= (($crit != '') ? ' AND ' : '')."`source` = '".prepareTextForDB($atts['source'])."'";
	    if(isset($atts['type'])) $crit .= (($crit != '') ? ' AND ' : '')."`type` = '".prepareTextForDB($atts['type'])."'";
	    if(isset($atts['user_id'])) $crit .= (($crit != '') ? ' AND ' : '')."`user_id` = '".intval($atts['user_id'])."'";
	    if(isset($atts['date'])) $crit .= (($crit != '') ? ' AND ' : '')."`date` LIKE '%".$atts['date']."%'";
	    $order = ((isset($atts['order'])) ? $atts['order'] : 'date');
	    $limit = ((isset($atts['limit'])) ? intval($atts['limit']) : "");
	    $joins = array(
	        array("type" => "LEFT JOIN", "table" => ACCOUNTS_TABLE." u", "comp" => "e.user_id = u.id")
	    );
	    $arry = $this->_db->setTable(EVENT_LOG_TABLE." e")->setFields("e.*, u.username")->setJoins($joins)->setWhere($crit)->setOrder($order)->getRecJoin();
	    return $arry;
	}

	/**
	 * Return the function that called the most recent function
	 * @param integer $back 		Number of functions back
	 */
	public function getCallingMethod($back = 1){
		$back += 1;
		$callers = debug_backtrace();
		return $callers[$back];
	}

	/**
	 * Initialize error log
	 *  - index 0 = record handling status errors (STAT_ERR)
	 *  - index 1 = core errors (CORE_ERR)
	 *  - index 2 = runtime errors (RUNTIME_ERR)
	 */
	public function initErrorMsg(){
		$this->err = array();
		$this->err[ERR_LOG] = array();
		$this->err[STAT_ERR] = array();		// reserved for status message
		$this->err[CORE_ERR] = array();
		$this->err[RUNTIME_ERR] = array();
		$this->err[DEBUGGER_ERR] = array();
	}

	/**
	 * Add a status message to error log
	 * @param string $msg
	 */
	public function addErrorStatMsg($msg){
		if($msg != '')
			$this->err[STAT_ERR][] = $msg;
	}

	/**
	 * Add a message to error log
	 * @param string $msg
	 * @param integer $err_grp
	 */
	public function addErrorMsg($msg, $err_grp = RUNTIME_ERR){
		if(in_array($err_grp, array(CORE_ERR, STAT_ERR, RUNTIME_ERR, DEBUGGER_ERR)))
			if(!in_array($msg, $this->err[$err_grp])) $this->err[$err_grp][] = $msg;
	}

	/**
	 * Remove a message from error log
	 * @param integer $num
	 * @param integer $err_grp 				STAT_ERR, RUNTIME_ERR, CORE_ERR, DEBUGGER_ERR
	 */
	public function removeErrorMsg($num, $err_grp = RUNTIME_ERR){
		if($num > 0)
			unset($this->err[$err_grp][$num]);
	}

	/**
	 * Return if specific status message exists
	 * @param message $msg
	 * @return boolean
	 */
	public function getErrorStatMsg($msg){
		return (array_search($msg, $this->err[STAT_ERR]) !== false);
	}

	/**
	 * Return if specific message exists
	 * @param string $msg
	 * @param integer $err_grp 				STAT_ERR, RUNTIME_ERR, CORE_ERR, DEBUGGER_ERR
	 * @return boolean
	 */
	public function getErrorMsg($msg, $err_grp = RUNTIME_ERR){
		if(in_array($err_grp, array(CORE_ERR, STAT_ERR, RUNTIME_ERR, DEBUGGER_ERR))){
			return (array_search($msg, $this->err[$err_grp]) !== false);
		}else{
			return false;
		}
	}

	/**
	 * Return if any message exists
	 * @param integer $err_grp 				STAT_ERR, RUNTIME_ERR, CORE_ERR, DEBUGGER_ERR
	 * @return boolean
	 */
	public function errorMsgExists($err_grp = RUNTIME_ERR){
		$errfound = 0;
		$errs = array(CORE_ERR, STAT_ERR, RUNTIME_ERR, DEBUGGER_ERR);
		foreach($errs as $err_num){
			if(($err_grp & $err_num) > 0){
				$errfound += count($this->err[$err_num]);
			}
		}
		return ($errfound > 0);
	}

	/**
	 * Return the JS alert function with the error messages
	 * @return string
	 */
	public function prepErrorAlert($extra_msg = '', $err_grp = 5){
		if($this->errorMsgExists()){
			if($err_grp == RUNTIME_ERR + STAT_ERR) {
				$e = $this->err[RUNTIME_ERR] + $this->err[STAT_ERR];
			}else{
				$e = $this->err[$err_grp];
			}
			$msg = join("\" + '\\n' + \"", $e);
			if($extra_msg != '') $msg .= "\" + '\\n' + \"".$extra_msg;
			return "alert(\"".$msg."\"); ";
		}else{
			return "";
		}
	}

	/**
	 * Output JS block with alert message
	 * @param string $alert
	 */
	public function showErrorAlertScript($alert){
		echo "<script type=\"text/Javascript\">{$alert}</script>\n";
	}

	/**
	 * Output error div with error message
	 * @param integer $err_grp 			STAT_ERR, RUNTIME_ERR, CORE_ERR, DEBUGGER_ERR
	 */
	public function showErrorMsg($err_grp = STAT_ERR){
		global $_debug;

		if($err_grp == STAT_ERR && $this->errorMsgExists($err_grp)){
			// output status message div since this type of msg occurs after POST
			// and can just be sent to the output
			$msg = '<ul>';
			foreach($this->err[STAT_ERR] as $errid => $errmsg){
				$msg .= '<li class="stat-msg-'.$errid.'">'.str_replace("'", "&#39;", $errmsg).'<a href="#" class="msg-close fa fa-times fa-lg"></a></li>';
			}
			$msg .= '</ul>';
			echo "<div class=\"display_msg\">{$msg}</div>\n";
			return;
		}
		if(($err_grp & CORE_ERR) > 0 && $this->errorMsgExists(CORE_ERR)){
			// use jQuery to output the error message(s) in the div
			$msg = str_replace("'", "&#39;", join("<br/>", $this->err[CORE_ERR]));
			echo "	jQuery(\"#display_core_msg\").html('{$msg}').css('display', 'block');\n";
			return;
		}
		if( (($err_grp & DEBUGGER_ERR) > 0 && $this->errorMsgExists(DEBUGGER_ERR))
			|| (($err_grp & RUNTIME_ERR) > 0 && $this->errorMsgExists(RUNTIME_ERR)) ){

			// use jQuery to output the error message(s) in the div
			$msg = '<ul>';
			if($_debug->debugger) {
				foreach($this->err[DEBUGGER_ERR] as $errid => $errmsg){
					$msg .= '<li class="debug-msg-'.$errid.'">'.str_replace("'", "&#39;", $errmsg).'<a href="#" class="msg-close fa fa-times fa-lg"></a></li>';
				}
			}
			foreach($this->err[RUNTIME_ERR] as $errid => $errmsg){
				$msg .= '<li class="runtime-msg-'.$errid.'">'.str_replace("'", "&#39;", $errmsg).'<a href="#" class="msg-close fa fa-times fa-lg"></a></li>';
			}
			$msg .= '</ul>';
			echo "	jQuery(\"#display_runtime_msg\").html('{$msg}').css('display', 'block');\n";
		}
	}
}

?>