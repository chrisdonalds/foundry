<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("CRONLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

define('CRON_INACTIVE', 0);
define('CRON_ACTIVE', 1);
define('CRON_IN_PROCESS', 2);

/**
 * CRONCLASS
 * Maintains and adminsters the scheduling system
 *
 * Cron schedules are stored in the Register table as:
 *		type => 'cron',
 *	 	function => callback function
 *		parameters => callback function parameters
 *		priority => 0 to 99 (run sooner to later)
 *		active => 1/0
 *		conditions => {interval, recurrence, final_datetime}
 *		start_datetime => initial datetime
 */
class CronClass {
	// overloaded data
	private $processing_cron = false;
	private $active_crons = array();

	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'_cron');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Create multiple cron schedules
	 * @param array $args 			array containing function, interval (both required)
	 *									parameters, recurrence, final_datetime, start_datetime
	 * @return integer 				An array of cron ids or false if failed to create cron
	 */
	public function create_crons($args){
		if(!is_array($args) || count($args) == 0) return false;

		$ids = array();
		foreach($args as $arg){
			$ids[] = $this->create_cron($arg);
		}
		return $ids;
	}

	/**
	 * Create a cron schedule
	 * @param array $args 			Function, interval (both required)
	 *									parameters, recurrence, final_datetime, start_datetime
	 * @return integer 				The cron id or false if failed to create cron
	 */
	public function create_cron($args){
		global $_db_control, $_events;

		list($event_args) =  $_events->processTriggerEvent(__FUNCTION__, $args);

		if(!is_array($args) || !isset($args['function']) || !isset($args['interval'])) return false;
		$function = strval(getIfSet($args['function']));

		$ok = false;
		$now = strtotime("now");
		$interval = strval(getIfSet($args['interval']));
		$recurrence = intval(getIfSet($args['recurrence'], -1));
		$parameters = getIfSet($args['parameters']);
		if(!is_array($parameters)) $parameters = array($parameters);

		if(isset($args['priority'])){
			$priority = intval(getIfSet($args['priority']));
			if($priority > 99) $priority = 99;
			if($priority < 0) $priority = 0;
		}else
			$priority = 50;

		$start_datetime = getIfSet($args['start_datetime']);
		$final_datetime = getIfSet($args['final_datetime']);
		if(!empty($start_datetime) && isDate($start_datetime) && $now < strtotime($start_datetime)){
			$start_datetime = strtotime($start_datetime);
		}else{
			// start_datetime is either not a valid date, is missing or is in the past
			// add the interval value (s,n,h,d,m) to the current timestamp
			$start_datetime = $this->calcDateFromIntervals($interval);
		}

		if(!empty($final_datetime) && isDate($final_datetime) && $now < strtotime($final_datetime)){
			// final_datetime is both not empty and is a valid date
			$final_datetime = strtotime($final_datetime);
			if($start_datetime > $final_datetime)
				list($start_datetime, $final_datetime) = swap($start_datetime, $final_datetime);
		}else{
			$final_datetime = null;
		}

		// save cron schedule
		$conditions = json_encode(array("interval" => $interval, "recurrence" => $recurrence, "final_datetime" => $final_datetime));
		$parameters = json_encode(($parameters));
		$id = $_db_control
			->setTable(REGISTER_TABLE)
			->setFieldvals(array("type" => "cron", "function" => $function, "conditions" => $conditions, "priority" => $priority, "parameters" => $parameters, "scheduled" => $start_datetime, "active" => true))
			->insertRec();
		$ok = $id;
		$this->active_crons = $this->get_crons(array("active" => CRON_ACTIVE));
		return $ok;
	}

	/**
	 * Update a cron schedule designated by function name
	 * @param array $args 			Function (required),
	 *									parameters, interval, recurrence, final_datetime, start_datetime
	 * @return boolean
	 */
	public function update_cron($args){
		global $_db_control, $_events;

		list($args) =  $_events->processTriggerEvent(__FUNCTION__, $args, $args);

		if(!isset($args['function'])) return false;
		$function = strval(getIfSet($args['function']));

		$ok = false;
		$rec = $_db_control->setTable(REGISTER_TABLE)->setWhere("`function` = '".$function."' AND `type` = 'cron'")->getRec(true);
		if(!empty($rec)){
			$conditions = json_decode($rec['conditions'], true);	// preserve current values
			$update_arry = array();
			$now = strtotime("now");
			if(isset($args['interval']))
				$conditions['interval'] = $interval = strval(getIfSet($args['interval']));
			if(isset($args['recurrence']))
				$conditions['recurrence'] = intval(getIfSet($args['recurrence']));
			if(isset($args['parameters']))
				$update_arry['parameters'] = json_encode(getIfSet($args['parameters']));
			if(isset($args['priority'])){
				$priority = intval(getIfSet($args['priority']));
				if($priority > 99) $priority = 99;
				if($priority < 0) $priority = 0;
				$update_arry['priority'] = $priority;
			}

			$start_datetime = getIfSet($args['start_datetime']);
			$final_datetime = getIfSet($args['final_datetime']);

			if(!empty($start_datetime) && isDate($start_datetime) && $now < strtotime($start_datetime)){
				$update_arry['scheduled'] = $start_datetime = strtotime($start_datetime);
			}elseif(!empty($interval)){
				// start_datetime is either not a valid date, is missing or is in the past
				// add the interval value (s,n,h,d,m) to the current date
				$update_arry['scheduled'] = $start_datetime = $this->calcDateFromIntervals($interval);
			}

			if(!empty($final_datetime) && isDate($final_datetime) && $now < strtotime($final_datetime)){
				// final_datetime is both not empty and is a valid date
				$final_datetime = strtotime($final_datetime);
				if($start_datetime > $final_datetime)
					list($start_datetime, $final_datetime) = swap($start_datetime, $final_datetime);
				$conditions['final_datetime'] = $final_datetime;
			}

			// update cron schedule
			if(!empty($conditions)) $update_arry['conditions'] = json_encode($conditions);
			$_db_control
				->setTable(REGISTER_TABLE)
				->setFieldvals($update_arry)
				->setWhere("`function` = '".$function."' AND `type` = 'cron'")
				->updateRec();
			$ok = true;
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done', $ok);				// alert triggered functions when function executes
		return $ok;
	}

	/**
	 * Delete a cron task by function name
	 * @param string $function 				Function that designates the cron to delete
	 * @return boolean
	 */
	public function delete_cron($function = null, $id = 0){
		global $_db_control, $_events;

		$id = intval($id);
		list($abort) =  $_events->processTriggerEvent(__FUNCTION__, $function, false);	// allow triggered functions to abort action
		if((empty($function) && $id == 0) || $abort) return false;

		if(!empty($function))
			$where = "`function` = '".$function."'";
		elseif($id > 0)
			$where = "`id` = '".$id."'";

		$ok = false;
		$_db_control
			->setTable(REGISTER_TABLE)
			->setWhere($where." AND `type` = 'cron'")
			->deleteRec();
		$ok = true;
		$this->active_crons = $this->get_crons(array("active" => CRON_ACTIVE));
		$_events->processTriggerEvent(__FUNCTION__.'_done', $ok);				// alert triggered functions when function executes
		return $ok;
	}

	/**
	 * Suspend an active cron task by function name
	 * @param string $args 				Function that designates the cron to suspend
	 * @return boolean
	 */
	public function suspend_crons($function = null, $id = 0){
		global $_db_control, $_events;

		$id = intval($id);
		list($abort) =  $_events->processTriggerEvent(__FUNCTION__, $function, false);
		if((empty($function) && $id == 0) || $abort) return false;

		if(!empty($function))
			$where = "`function` = '".$function."'";
		elseif($id > 0)
			$where = "`id` = '".$id."'";

		$ok = false;
		$rec = $_db_control->setTable(REGISTER_TABLE)->setWhere($where." AND `type` = 'cron' AND `active` = ".CRON_ACTIVE)->getRec(true);
		if(!empty($rec)){
			$_db_control
				->setTable(REGISTER_TABLE)
				->setFieldvals(array('active' => CRON_INACTIVE))
				->setWhere($where." AND `type` = 'cron'")
				->updateRec();
			$ok = true;
			$this->active_crons = $this->get_crons(array("active" => CRON_ACTIVE));
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done', $ok);				// alert triggered functions when function executes
		return $ok;
	}

	/**
	 * Resume a suspended cron task by function name
	 * @param string $args 				Function that designates the cron to suspend
	 * @return boolean
	 */
	public function resume_crons($function = null, $id = 0){
		global $_db_control, $_events;

		$id = intval($id);
		list($abort) =  $_events->processTriggerEvent(__FUNCTION__, $function, false);	// allow triggered functions to abort action
		if((empty($function) && $id == 0) || $abort) return false;

		if(!empty($function))
			$where = "`function` = '".$function."'";
		elseif($id > 0)
			$where = "`id` = '".$id."'";

		$ok = false;
		$rec = $_db_control->setTable(REGISTER_TABLE)->setWhere($where." AND `type` = 'cron' AND `active` = ".CRON_INACTIVE)->getRec(true);
		if(!empty($rec)){
			$fields = array('active' => CRON_ACTIVE);
			$conditions = $rec['conditions'];
			if(!empty($conditions)){
				$conditions = json_decode($conditions, true);
				if($conditions['recurrence'] < 1 && $conditions['recurrence'] != -1){
					$conditions['recurrence'] = 1;  		// allow one more run
				}
				$fields['conditions'] = json_encode($conditions);
			}
			$_db_control
				->setTable(REGISTER_TABLE)
				->setFieldvals($fields)
				->setWhere($where." AND `type` = 'cron'")
				->updateRec();
			$ok = true;
			$this->active_crons = $this->get_crons(array("active" => CRON_ACTIVE));
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done', $ok);				// alert triggered functions when function executes
		return $ok;
	}

	/**
	 * Execute functions attached to pending scheduled cron jobs
	 * @return boolean
	 */
	public function run_crons($args = array(), $validate = false, $access_code = null){
		global $_db_control, $_events, $_sec;

		if(!DB_USED) return;
		if($validate && $access_code != $_sec->createUniqueID(WEB_URL.ADMIN_FOLDER, STATIC_SALT, 16)) die('Cron error: Key is invalid.');

		$ok = false;
		$now = strtotime("now");

		// conditions
		$crit = "";
		if(is_array($args) && !empty($args)){
			if(isset($args['id']) && intval($args['id']) > 0) $crit .= " AND `id` = '".intval($args['id'])."'";
			if(isset($args['function']) && !empty($args['function'])) $crit .= " AND `function` = '".$args['function']."'";
		}
		if($crit == ""){
			$crit = " AND `scheduled` <= '".$now."'";
		}

		$recs = $_db_control->setTable(REGISTER_TABLE)->setWhere("`type` = 'cron' AND `active` = ".CRON_ACTIVE.$crit)->getRec();
		$_events->processTriggerEvent(__FUNCTION__, $recs);				// alert triggered functions when crons are run

		if(!empty($recs)){
			foreach($recs as $rec){
				if(empty($rec['conditions'])) continue;

				$conditions = json_decode($rec['conditions'], true);
				if(is_array($conditions)){
					// conditions are an array
					$interval = getIfSet($conditions['interval']);
					if(!empty($interval)){
						// intervals ok
						$last_scheduled = $rec['scheduled'];
						$next_scheduled = $this->calcDateFromIntervals($interval, $now);
						$recurrence = getIntvalIfSet($conditions['recurrence']);
						$func = $rec['function'];

						$active = true;
						// check if next scheduled time is beyond final/end time
						if(isset($conditions['final_datetime']) && $next_scheduled > $conditions['final_datetime'])
							$active = false;	// deactivate cron

						// check if recurrence is at or less than 1 (this would be the last run)
						if($recurrence > -1){
							$recurrence--;
							if($recurrence < 0) $recurrence = 0;
							if($recurrence < 1) $active = false;
						}

						$conditions['recurrence'] = $recurrence;
						$conditions['last_run'] = time();
						$cond_json = json_encode($conditions);

						// update the next scheduled time, active state, and enable the processing action
						$_db_control
							->setTable(REGISTER_TABLE)
							->setFieldvals(array('scheduled' => $next_scheduled, 'conditions' => $cond_json, 'active' => CRON_IN_PROCESS))
							->setWhere("`id` = '".$rec['id']."'")
							->updateRec();

						// execute the callback function
						if(function_exists($func)){
							$func($rec, $rec['parameters']);
						}

						// disable the processing action
						$_db_control
							->setTable(REGISTER_TABLE)
							->setFieldvals(array('active' => $active))
							->setWhere("`id` = '".$rec['id']."'")
							->updateRec();

						$ok = true;
					}
				}
			}
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done', $ok);				// alert triggered functions when function executes
		return $ok;
	}

	/**
	 * Return cron jobs based on certain criteria
	 * @param array $args  					function (string/array/regex), active_state (boolean), scheduled (date/timestamp), intervals (string)
	 * @return array
	 */
	public function get_crons($args = array()){
		global $_db_control;

		if(!is_array($args)) $args = array();

		$function = getIfSet($args["function"]);
		$active_state = getIfSet($args["active"]);
		$scheduled = getIfSet($args["scheduled"]);
		$intervals = getIfSet($args["interval"]);

		$where = "`type` = 'cron'";
		if(!empty($function)){
			if(is_array($function)){
				$where .= " AND `function` IN ('".join("','", $function)."')";
			}else{
				$where .= " AND `function` LIKE '".$function."'";
			}
		}
		if(!empty($active)){
			$where .= " AND `active` = '".((bool) $active)."'";
		}
		if(!empty($scheduled)){
			if(is_string($scheduled)){
				$where .= " AND `scheduled` <= ".strtotime($scheduled);
			}else{
				$where .= " AND `scheduled` <= ".$scheduled;
			}
		}

		$recs = $_db_control->setTable(REGISTER_TABLE)->setFields(array("id", "function", "parameters", "priority", "active", "conditions", "scheduled"))->setWhere($where)->setOrder("scheduled DESC")->getRec();
		return $recs;
	}

	/**
	 * Add an interval to a timestamp and return result
	 * @param string $interval 					seconds, minutes, hours, days, weeks, months
	 * @param mixed $fromdate 					either a valid date string or timestamp
	 * @return timestamp
	 */
	private function calcDateFromIntervals($interval, $fromdate = "now"){
		$periods = explode(",", $interval);
		if(is_numeric($fromdate))
			$timestamp = $fromdate;
		else
			$timestamp = strtotime($fromdate);
		foreach($periods as $indx => $period){
			$p = intval($period);
			if($p > 0){
				switch($indx){
					case 0:
						$timestamp += $p;					// seconds
						break;
					case 1:
						$timestamp += ($p * 60);			// minutes
						break;
					case 2:
						$timestamp += ($p * 60 * 60);		// hours
						break;
					case 3:
						$timestamp += ($p * 3600 * 24); 	// days
						break;
					case 4:
						$timestamp += ($p * 3600 * 24 * 7);	// weeks
						break;
					case 5:
						$timestamp += ($p * 3600 * 24 * 30);// months
						break;
				}
			}
		}
		return $timestamp;
	}

	private function checkCaller($action){
		$stack = debug_backtrace();
		$callerfile = $stack[1]['file'];
		if(strpos($callerfile, "_core") === false){
			die("Calling Users::$action in $callerfile not allowed!");
		}
	}
}

?>