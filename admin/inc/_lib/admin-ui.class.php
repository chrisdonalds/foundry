<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

/* ADMIN UI GLOBAL CLASSES */

define ("ADMINUILIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/**
 * PAGECLASS
 * Stores and manages page structure data
 * - header
 * - footer
 * - menu
 * - help
 * - sectionid
 * - scriptlines[]
 */
class PageClass extends ArrayObject {
	// overloaded data
	private $_keys = array("header" => "", "footer" => "", "menu" => "", "menus" => array(), "help" => "", "sectionid" => 0,
							"id" => 0, "name" => "", "alias" => "", "urlpath" => "",		// resource identity
							"pagenum" => 0, "uri" => "", "title" => "",
							"target" => "", "targettype" => "", "targetaction" => "",	// file target (i.e. list-pages.php)
							"data" => array(), "datatype" => "",		// data content
							"nonce" => "", "found" => false, "dbrec" => array(), "attributes" => array(),
							"query" => "", "queryvars" => "", "custom_query" => "",	// supplemental information
							"search_list" => "", "search_text" => "",			// searches
							"search_by" => "", "sort_by" => "",
							"sort_dir" => "", "offset" => "", "limit" => "", "group" => "",
							"row_id" => 0, "cat_id" => 0, "where_clause" => "",		// database elements
							"concat" => "", "subject" => "", "ingroup" => "",			// other parameters
							"childsubject" => "", "parentgroup" => "",
							"altparams" => "", "altgroups" => "", "addqueries" => "",
							"savebuttonpressed" => "", "titlefld" => "",
							"imagefld" => "", "thumbfld" => "", "userediting",
							"taxonomy_id" => 0, "term_id" => 0);
	private $_subkeys = array("scriptlines" => array(),
							"cacheablescriptlines" => array(),
							"combinedscriptlines" => array(),
							"embedscripts" => array(),
							"embedstyles" => array(),
							"sections" => array());
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_events;

		$s = new self;
		$s->setAttributes();
		$_events->processTriggerEvent(__FUNCTION__.'_admin_ui');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Return whether or not the user clicked on a 'save...' button
	 * (Prelude to data saving function or handling code)
	 * @return boolean
	 */
	public function formDataIsReadyForSaving(){
		return ($this->savebuttonpressed);
	}

	/**
	 * Prepare the list page search querying capability.  Returns: $_page->where_clause and $_page->concat
	 * @param array $initarray array("sort_by", "sort_dir", "search_list", "custom_query")
	 * @tutorial 	"sort_by" => default value of sort_by if nothing passed
	 * 				"sort_dir" => default value of sort_dir if nothing passed
	 * 				"search_list" => array("field1", "field2"...)<br/>
	 * 				"custom_query" => array("key matched to search_by" => "SQL query portion")
	 */
	public function prepSearch($initarray = null){
		global $_error;

		if(is_array($initarray)){
			foreach($initarray as $key => $initvalue){
				if(array_key_exists($key, $this->_keys)){
					if(empty($this->_keys[$key])){
						$this->_keys[$key] = $initvalue;
					}
				}else{
					$d = debug_backtrace();
					$_error->addErrorMsg("Cannot set search parameter '$key'.  It is not a valid PAGE SEARCH property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ", CORE_ERR);
					return;
				}
			}
		}else{
			$_error->addErrorMsg("An array is required as the optional parameter for _page::prepSearch.", CORE_ERR);
			return;
		}

		$search_text = getIfSet($this->_keys['search_text']);
		$search_by = getIfSet($this->_keys['search_by']);
		$search_list = getIfSet($this->_keys['search_list']);
		$where_clause = "";
		$concat = "";
		$search_clause = "";

		if(is_array($search_list)) $search_clause = join(" like '%".$search_text."%' OR ", $search_list);
		if($search_text != "" && $search_by != ""){
		    switch($search_by){
		        case 'all':
		            $where_clause .= $concat." ({$search_clause} like '%$search_text%') ";
		    		$concat = " AND";
		            break;
		        case (isset($custom_query[$search_by])):
		        	$where_clause .= $concat.$custom_query[$search_by];
		        	$concat = " AND";
		        	break;
		        default:
		            $where_clause .= $concat." ".$search_by." like '%$search_text%' ";
		    		$concat = " AND";
		            break;
			}
		}else{
		    switch($search_by){
		        case 'published':
		            $where_clause .= $concat." published = 1 ";
		    		$concat = " AND";
		            break;
		        case 'activated':
		            $where_clause .= $concat." activated = 1 ";
		    		$concat = " AND";
		            break;
		        case 'archived':
		            $where_clause .= $concat." archived = 1 ";
		    		$concat = " AND";
		            break;
		        case 'unpublished':
		            $where_clause .= $concat." published = 0 ";
		    		$concat = " AND";
		            break;
		        case 'deactivated':
		            $where_clause .= $concat." activated = 0 ";
		    		$concat = " AND";
		            break;
		        case 'unarchived':
		            $where_clause .= $concat." archived = 0 ";
		    		$concat = " AND";
		            break;
		        case 'deleted':
		            $where_clause .= $concat." deleted = 1 ";
		    		$concat = " AND";
		            break;
		        case 'draft':
		            $where_clause .= $concat." draft = 1 OR published = 0 ";
		    		$concat = " AND";
		            break;
		        default:
		    	    break;
			}
		}
		$this->_keys['where_clause'] = $where_clause;
		$this->_keys['concat'] = $concat;
	}

	public function createSection($key, $data){
		$sect = new SectionsClass($data);
		if(!is_null($sect->attributes) && !empty($sect->attributes)){
			$this->_subkeys["sections"][$key] = $sect->attributes;
			return true;
		}else{
			return false;
		}
	}

	public function deleteSection($key){
		if(isset($this->_subkeys["sections"][$key])) {
			unset($this->_subkeys["sections"][$key]);
			return true;
		}else{
			return false;
		}
	}

	public function setAttributes($recData = array()){
		$atts = array();
		if(is_array($recData)){
			$attskeys = array(
				"published" => true, "draft" => false, "deleted" => false,
				"date_created" => "", "date_published" => "", "date_updated" => "",
				"viewable_from" => "", "viewable_to" => "", "language" => "", "sitemap_state" => "1:0.5",
				"searchable" => true, "protected" => false, "locked" => false, "password" => "",
				"metatitle" => "", "metakeywords" => "", "metadescr" => "", "in_use" => false,
				"corrupt" => false
			);
			foreach($attskeys as $attkey => $attval){
				if(isset($recData[0]) && isset($recData[0][$attkey]))
					$atts[$attkey] = $recData[0][$attkey];
				elseif(isset($recData[$attkey]))
					$atts[$attkey] = $recData[$attkey];
				else
					$atts[$attkey] = $attval;
			}
		}
		$this->_keys["attributes"] = $atts;
		return $recData;
	}

	public function __get($name){
		$v = null;
		if(array_key_exists($name, $this->_keys)){
			// return scalar value
			if(isset($this->_keys[$name])) $v = $this->_keys[$name];
		}elseif(array_key_exists($name, $this->_subkeys)){
			// return subarray value
			if(isset($this->_subkeys[$name])) $v = $this->_subkeys[$name];
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot get '$name'.  It is not a valid _PAGE property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
		return $v;
	}

	public function __set($name, $value){
		if(array_key_exists($name, $this->_keys)){
			// set scalar value
			$this->_keys[$name] = $value;
		}elseif(array_key_exists($name, $this->_subkeys)){
			// set subarray value
			$this->_subkeys[$name][] = $value;
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot set '$name'.  It is not a valid _PAGE property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
	}
}

/**
 * JSBLOCK
 * Constructs Javascript code
 */
class JSBlock {
	private $output;
	private $checkitems;
	private $jq_output;
	public $subject;
	public $section;
	protected static $_instance = null;

	public function __construct(){
		$this->checkitems = array();
		$this->subject = '';
		$this->section = '';
		$this->output = '';
	}

	private function __clone(){
	}

	public static function init(){
		global $_events;

		$s = new self;
		$_events->processTriggerEvent(__FUNCTION__.'_js_block');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Create the main check_form function
	 */
	public function buildCheckFormFunc(){
		$this->output = "$(document).delegate('.editor_button_save', 'click', function(e){\n";
		if(count($this->checkitems)){
			foreach($this->checkitems as $item){
				$this->output .= $item."\n";
			}
		}
		$this->output .= "
		$('#_savebuttonpressed').val($(this).attr('name'));
		$('#edit_form').submit();
	});";
	}

    /**
     * Create a jQuery event function
     * @param string $obj
     * @param string $jq_event
     * @param string $code
     */
	public function buildJQueryCode($obj, $jq_event, $code){
		$this->jq_output .= <<<EOT
$("{$obj}").{$jq_event}(function(e){
	{$code}
});

EOT;
	}

    /**
     * Create a jQuery Ajax function
     * @param string $obj
     * @param string $objtype
     * @param string $destobj
     * @param string $destlabel
     * @param string $table
     * @param string $idfld
     * @param string $datafld
     * @param string $crit
     * @param string $limit
     * @param string $order
     */
	public function buildJQueryAjaxCode($obj, $objtype, $destobj, $destlabel,
                                        $table, $idfld, $datafld,
                                        $crit = "", $limit = "", $order = ""){
		switch($objtype){
			case 'selectmenu':
				$jq_event = 'change';
				$jq_destprocess = "$('#{$destobj}').find('option').remove().end().append('<option value=\"\">-- Select {$destlabel} --</option>').val('');
					if(jsonResponse.success){
						var arry = eval(jsonResponse.rtndata);
						var nr = arry.length;
						for(var i=0; i<nr; i++){
							var arry_elem = arry[i];
							$('#{$destobj}').append('<option value=\"'+arry_elem['id']+'\">'+arry_elem['{$datafld}']+'</option>'+\"\\n\");
						}
						if(nr == 1) $('#{$destobj}').val(arry[0]['id']);
					}";
				break;
			case 'selectoption':
				$jq_event = 'change';
				$jq_destprocess = "
					if(jsonResponse.success){
						var arry = eval(jsonResponse.rtndata);
						if(arry.length > 0){
							var arry_elem = arry[0];
							$('#{$destobj}').val(arry[0]['id']);
						}
					}";
				break;
			case 'text':
				$js_event = 'blur';
				$jq_destprocess = "$('#{$destobj}').val(jsonResponse.rtndata);";
				break;
			default:
				die("Invalid datatype in jsblock->buildJQueryCode!");
				return false;
		}

        $web = WEB_URL;
		$this->jq_output .= <<<EOT
$("#{$obj}").{$jq_event}(function(e){
	e.preventDefault();
	var val = $(this).val();
	var crit = '{$crit}';
	if(val != ''){
		$.ajax({
			type: "POST",
			url: "{$web}admin/inc/_core/ajaxwrapper.php",
			data: {op:'{$objtype}', table:'{$table}', fld:'{$datafld}', crit:crit, limit:'{$limit}', order:'{$order}'},
			dataType: "json",
			cache: false,
			success: function(jsonResponse){
			{$jq_destprocess}
			}
		});
	}
});
EOT;
	}

	/**
	 * Add a validation test to check_form function
	 * @param string $object
	 * @param string $msg
	 */
	public function addCheckReqEntry($object, $msg){
		$item = "";
		if($object != '' && $msg != ''){
			$item .= "	if(!checkRequiredField('{$object}', '{$msg}')) return false;";
			$this->checkitems[] = $item;
		}else{
			Throw new exception("Object and message are required for ".__FUNCTION__);
		}
	}

	/**
	 * Add a CMS editor validation test to check_form function
	 * @param string $object
	 * @param string $msg
	 */
	public function addCheckReqHTMLEditorEntry($object, $msg){
		if($object != '' && $msg != ''){
			if(CMS_EDITOR == 'ckeditor')
				$item = "	$('#{$object}').val(CKEDITOR.instances.{$object}.getData());\n";
			elseif(CMS_EDITOR == 'tiny_mce')
				$item = "	$('#{$object}').val(tinyMCE.get('{$object}').getContent());\n";
			$item.= "	if(!checkRequiredField('{$object}', '{$msg}')) return false;";
			$this->checkitems[] = $item;
		}else{
			Throw new exception("Object and message are required for ".__FUNCTION__);
		}
	}

	/**
	 * Add a number validation test to check_form function
	 * @param string $object
	 * @param float $min
	 * @param float $max
	 * @param string $msg
	 */
	public function addCheckReqNumEntry($object, $min, $max, $msg){
		if($object != '' && $msg != ''){
			$min = floatval($min);
			$max = floatval($max);
			$item.= "	if(!checkRequiredNumField('{$object}', {$min}, {$max}, '{$msg}')) return false;";
			$this->checkitems[] = $item;
		}else{
			Throw new exception("Object, min, max, and message are required for ".__FUNCTION__);
		}
	}

    /**
     * Create a minimum character validation function
     * @param string $object
     * @param integer $min
     * @param string $msg
     */
	public function addCheckReqMinCharEntry($object, $min, $msg){
		if($object != '' && $msg != ''){
			$min = floatval($min);
			$item.= "	if(!checkMinChars('{$object}', {$min}, '{$msg}')) return false;";
			$this->checkitems[] = $item;
		}else{
			Throw new exception("Object, min, and message are required for ".__FUNCTION__);
		}
	}

    /**
     * Create an email validation function
     * @param string $object
     */
	public function addCheckReqEmailEntry($object){
		if($object != ''){
			$item.= "	if(!validateEmail('{$object}')) return false;";
			$this->checkitems[] = $item;
		}else{
			Throw new exception("Object is required for ".__FUNCTION__);
		}
	}

    /**
     * Create a string comparison function
     * @param string $object
     * @param string $object2
     * @param string $msg
     */
	public function addCheckReqCompareStr($object, $object2, $msg){
		if($object != '' && $object2 != ''){
			$item.= "	if(!compareTheString('{$object}', '{$object2}', '{$msg}')) return false;";
			$this->checkitems[] = $item;
		}else{
			Throw new exception("Object and object2 are required for ".__FUNCTION__);
		}
	}

	/**
	 * Create a checkdate function
	 */
	public function buildCheckDateFunc(){
		$this->output .= "
function updateDate(obj){
	var m = $('#'+obj+'_m').val();
	var d = $('#'+obj+'_d').val();
	var y = $('#'+obj+'_y').val();
	$('#'+obj).val(y+'-'+m+'-'+d);
}
";
	}

	/**
	 * Create a copydata function
	 */
	public function buildCopyDataFunc(){
		$this->output .= "
function copydata(src, target){
	$('#'+target).val(removeHTMLTags(src, false));
}
";
	}

	/**
	 * Create a password icon indicator function
	 */
	public function buildPasswordIconFunc(){
		$this->output .= "

	$('.passmatch').change(function(){
		var matchflds = $(this).attr('rel').split;
		var fld1 = $('#'+matchflds[0]);
		var fld2 = $('#'+matchflds[1]);

		if(fld1.val() != fld2.val()){
			$('#cpassbad_'+matchflds[0]).show();
			$('#cpassok_'+matchflds[0]).hide();
		}else{
			$('#cpassbad_'+matchflds[0]).hide();
			$('#cpassok_'+matchflds[0]).show();
		}
	});
";
	}

	/**
	 * Create a custom page redirction function
	 * @param string $fname
	 * @param string $page
	 */
	public function buildCustomPageFunc($fname, $page){
		$this->output .= "
function {$fname}(){
	window.location = '{$page}';
}
";
	}

    /**
     * Return Validator script
     */
	private function showValidatorCode(){
        global $incl;

        if(strpos($incl, "validator") === false) return null;

        preg_match("/validator\(([a-z0-9, \-_]*)\)/i", $incl, $formlist);
        if($formlist[1] != '' && isset($formlist[1])){
            $forms = preg_split("/[\s,]+/", $formlist[1]);
            foreach($forms as $key=>$value) $forms[$key] = "form#".$value;
            $form_id = join(",", $forms);
        }else{
            $form_id = "form";
        }
        return "\n\n	$(\"{$form_id}\").validate();";
 	}

	/**
	 * Output the finalized JS list block
	 */
	public function showJSListBlock(){
		$content = $this->output;
		$jq_out = $this->jq_output;

		print <<<EOT
<script type="text/javascript" language="javascript">
(function($){
	{$content}
	{$jq_out}
}) (jQuery);
</script>

EOT;
	}

	/**
	 * Output finalized JS edit block
	 */
	public function showJSEditBlock(){
		$content = $this->output;
		$jq_out = $this->jq_output.self::showValidatorCode();

		print <<<EOT
<script type="text/javascript" language="javascript">
(function($){
	{$content}
	{$jq_out}
}) (jQuery);
</script>

EOT;
	}
}

?>