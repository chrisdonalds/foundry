<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("CACHELIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/**
 * CACHE CLASS
 * Serves as the entrypoint for and offers basic caching facilities
 */
class CacheClass {
	// overloaded data
	private $_keys = array(	);
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'_cache');                // alert triggered functions when function executes
		return $s;
	}

	/**
	 * Check if file caching and/or combination (minification) is required, testing existence, timestamp and lifespan
	 * @param $fileArry array 				Array of CSS file paths
	 * @param $extension string 			The service extension (.css or .js)
	 */
	public function check($fileArry, $extension){
		global $_events, $_filesys;

		$extension = strtolower($extension);
		list($abort) = $_events->processTriggerEvent(__FUNCTION__, array($fileArry, $extension), false);				// alert triggered functions when function executes
		if(empty($fileArry) || $abort) return false;

		$cached = (($extension == '.js' && CACHE_JS == 1) || ($extension == '.css' && CACHE_CSS == 1));
		if($cached){
			$update_needed = true;
			$lastmodified = 0;
			$cacheableFileArry = array();
			$hash = '';

			foreach($fileArry as $key => $set){
				$filepath = SITE_PATH.str_replace(WEB_URL, "", $set['filepath']);
				if(file_exists($filepath)){
					$lastmodified = max($lastmodified, filemtime($filepath));
					$cacheableFileArry[$key] = $fileArry[$key];
					unset($fileArry[$key]);
					$hash .= basename($filepath).",";
				}
			}

			// create cache folder if it does not exist
			if(!file_exists(SITE_PATH.ADMIN_FOLDER.CACHE_FOLDER))
				$_filesys->mkdir2(SITE_PATH.ADMIN_FOLDER.CACHE_FOLDER);

			$hash = md5($hash);
			$combinedFile = ADMIN_FOLDER.CACHE_FOLDER."fcache-".$hash.$extension;
			$lifespan = 300;
			if(defined('CACHE_LIFESPAN') && defined('CACHE_LIFESPAN_PERIOD')){
				$lifespan_period = array("min" => 60, "hrs" => 60 * 60, "days" => 60 * 60 * 24);
				if(isset($lifespan_period[CACHE_LIFESPAN_PERIOD]))
					$lifespan = intval(CACHE_LIFESPAN) * $lifespan_period[CACHE_LIFESPAN_PERIOD];
			}

			if(file_exists(SITE_PATH.$combinedFile))								// cache file exists
				if(filemtime(SITE_PATH.$combinedFile) > $lastmodified)				// cache file is still fresh
					if(filemtime(SITE_PATH.$combinedFile) >= time() - $lifespan)	// cache file lifespan ok
						$update_needed = false;

			if($update_needed){
				// re-combine source files
				$this->combine($cacheableFileArry, $extension, $combinedFile);
			}

			// Append cached, combined file to array
			switch($extension){
				case ".js":
					$fileArry[] = array("line" => '<script type="text/javascript" language="javascript" src="'.WEB_URL.$combinedFile.'"></script>', "filepath" => WEB_URL.$combinedFile);
					break;
				case ".css":
					$fileArry[] = array("line" => '<link href="'.WEB_URL.$combinedFile.'" rel="stylesheet" type="text/css" media="screen" />', "filepath" => WEB_URL.$combinedFile);
					break;
			}
		}
		return $fileArry;
	}

	/**
	 * Combine multiple files into a single file
	 * @param $fileArry array 				Array of file paths
	 * @param $extension string 			The service extension (.css or .js)
	 * @param $combinedFile string 			The path of the combined file
	 */
	public function combine($fileArry, $extension, $combinedFile){
		global $_events;

		$extension = strtolower($extension);
		list($abort) = $_events->processTriggerEvent(__FUNCTION__, array($fileArry, $extension), false);				// alert triggered functions when function executes
		if(empty($fileArry) || $abort) return false;

		$combined = null;
		$contentsArry = array();

		// bubble all CSS filepaths with files that contain @import strings to the top
		if($extension == ".css"){
			$sortedFileArry = array();
			foreach($fileArry as $key => $set){
				$filepath = str_replace(WEB_URL, "", $set['filepath']);
				$contents = file_get_contents(SITE_PATH.$filepath);
				if(strpos($contents, '@import') !== false){
					// add to top
					array_unshift($sortedFileArry, $set);
					array_unshift($contentsArry, $contents);
				}else{
					// add to end
					$sortedFileArry[$key] = $set;
					$contentsArry[$key] = $contents;
				}
			}
			$fileArry = $sortedFileArry;
		}

		foreach($fileArry as $key => $set){
			$filepath = str_replace(WEB_URL, "", $set['filepath']);
			if($extension == ".css"){
				$aliasparts = pathinfo($filepath);
				$contents = $contentsArry[$key];
				$contents = preg_replace("/url\(('|)([a-z0-9_\-.\/]*)('|)\)/i", "url(".WEB_URL.$aliasparts['dirname']."/$2)", $contents);
				if(COMPRESS_CSS == 1) $contents = $this->compress($contents);
			}else{
				$contents = file_get_contents(SITE_PATH.$filepath);
			}

			$combined .= '/**'.PHP_EOL;
			$combined .= ' * '.$set['filepath'].PHP_EOL;
			$combined .= ' */'.PHP_EOL;
			$combined .= $contents.PHP_EOL;
		}

		if(!is_null($combined)){
			// Save the combined content to cache file
			if(file_exists(SITE_PATH.$combinedFile)) unlink(SITE_PATH.$combinedFile);
			file_put_contents(SITE_PATH.$combinedFile, $combined);
		}
		return true;
	}

	/**
	 * Compress file contents by removing spaces, comments, tabs and new lines
	 * @param $contents string
	 */
	public function compress($contents){
		global $_events;

		$_events->processTriggerEvent('compressCSS');
		$contents = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $contents);		// remove comments
		$contents = str_replace(array("\r\n", "\r", "\n", "\t", '  '), '', $contents);	// remove tabs, spaces, newlines, etc.
		$contents = str_replace('{ ', '{', $contents);									// remove unnecessary spaces.
		$contents = str_replace(' }', '}', $contents);
		$contents = str_replace('; ', ';', $contents);
		$contents = str_replace(', ', ',', $contents);
		$contents = str_replace(' {', '{', $contents);
		$contents = str_replace('} ', '}', $contents);
		$contents = str_replace(': ', ':', $contents);
		$contents = str_replace(' ,', ',', $contents);
		$contents = str_replace(' ;', ';', $contents);
		return $contents;
	}

	/**
	 * Ensure the master theme css file is loaded first
	 * @param $fileArry array 				Array of CSS file paths
	 */
	public function sortCSSPaths($fileArry){
		global $_themes;

		if(!is_array($fileArry)) return $fileArry;

		if(IN_ADMIN){
			$theme_path = $_themes->getThemePathUnder('admin');
			$theme_file = $_themes->getThemeInfo('admin', null, 'stylefiles');
			if(empty($theme_file) && strpos($theme_path, ".sys") !== false) $theme_file = 'css/master.css';  // ensure something is loaded
			$theme_css = WEB_URL.$theme_path.$theme_file;
		}else{
			$theme_path = $_themes->getThemePathUnder('website');
			$theme_file = $_themes->getThemeInfo('website', null, 'stylefiles');
			$theme_css = WEB_URL.$theme_path.$theme_file;
		}
		$f = $fileArry;
		foreach($f as $k => $fset){
			if($fset['filepath'] == $theme_css){
				unset($fileArry[$k]);
				$fileArry = array('master' => $fset) + $fileArry;
				break;
			}
		}
		return $fileArry;
	}

	/**
	 * Delete all cache files stored in the cache folder
	 */
	public function clear(){
		global $_events;

		$_events->processTriggerEvent('resetCaches');
		array_map('unlink', glob(SITE_PATH.ADMIN_FOLDER.CACHE_FOLDER."fcache-*.*"));
		$result = file_exists(SITE_PATH.ADMIN_FOLDER.CACHE_FOLDER."fcache-*.*");
		return $result;
	}

	public function &__get($name){
		$v = null;
		if(in_array($name, $this->_keys)){
			// return scalar value
			$v = $this->_class[$name];
		}else{
			trigger_error("Cannot get '$name'.  It is not a valid _CLASS property. ");
		}
		return $v;
	}

	public function __set($name, $value){
		if(in_array($name, $this->_keys)){
			// set scalar value
			$this->_class[$name] = $value;
		}else{
			trigger_error("Cannot set '$name'.  It is not a valid _CLASS property. ");
		}
	}
}

?>