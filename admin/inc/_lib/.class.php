<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("_LIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/**
 * CLASS
 * 
 */
class _Class {
	// overloaded data
	private $_keys = array(	);
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'');                // alert triggered functions when function executes
		return $s;
	}

	public function &__get($name){
		$v = null;
		if(in_array($name, $this->_keys)){
			// return scalar value
			$v = $this->_class[$name];
		}else{
			trigger_error("Cannot get '$name'.  It is not a valid _CLASS property. ");
		}
		return $v;
	}

	public function __set($name, $value){
		if(in_array($name, $this->_keys)){
			// set scalar value
			$this->_class[$name] = $value;
		}else{
			trigger_error("Cannot set '$name'.  It is not a valid _CLASS property. ");
		}
	}
}

?>