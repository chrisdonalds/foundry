<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("SECTIONSLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/* SECTIONS GLOBAL CLASS
 *
 * Stores information about individual sections
 */
class SectionsClass {
	// overloaded data
	public $attributes = null;

	public function __construct($recData){
		$atts = array();
		if(is_array($recData)){
			$attskeys = array(
				"title", "description", "parentkey",
				"theme", "area", "userlevel", "handler",
				"published", "draft", "deleted",
				"date_created", "date_updated",
				"viewable_from", "viewable_to", "language",
				"htmlclass", "htmlobj"
			);
			foreach($attskeys as $att){
				if(isset($recData[0][$att]))
					$atts[$att] = $recData[0][$att];
				elseif(isset($recData[$att]))
					$atts[$att] = $recData[$att];
				else
					$atts[$att] = null;
			}
		}
		$this->attributes = $atts;
		return $this;
	}
}

?>