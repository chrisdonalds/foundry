<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

/* RENDER CLASS
 *
 * All functions related to admin area display
 */
define ("ADMRENDERLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

define ("DEF_ACTION_ADD", "add");
define ("DEF_ACTION_BULK", "bulk");
define ("DEF_ACTION_CLONE", "clone");
define ("DEF_ACTION_DEFAULT", "default");
define ("DEF_ACTION_DELETE", "delete");             // bulk eligible
define ("DEF_ACTION_DEMOTE", "demote");
define ("DEF_ACTION_EDIT", "edit");
define ("DEF_ACTION_EDITFORM", "editfrm");
define ("DEF_ACTION_EXPORT", "export");
define ("DEF_ACTION_LIST", "list");
define ("DEF_ACTION_OPEN", "open");
define ("DEF_ACTION_PROMOTE", "promote");
define ("DEF_ACTION_PUBLISH", "publish");           // bulk eligible
define ("DEF_ACTION_REPLY", "reply");
define ("DEF_ACTION_SAVEORG", "saveorganize");
define ("DEF_ACTION_SEND", "send");                 // bulk eligible
define ("DEF_ACTION_SUBSCRIBE", "subscribe");       // bulk eligible
define ("DEF_ACTION_UNARCHIVE", "unarchive");       // bulk eligible
define ("DEF_ACTION_UNDELETE", "undelete");         // bulk eligible
define ("DEF_ACTION_UNPUBLISH", "unpublish");       // bulk eligible
define ("DEF_ACTION_UNSUBSCRIBE", "unsubscribe");   // bulk eligible
define ("DEF_ACTION_VIEW", "view");
define ("DEF_ACTION_VIEWPAGES", "viewpages");
define ("DEF_ACTION_VIEWRECS", "viewrecs");

define ("DEF_EDITBUT_SAVE", 1);
define ("DEF_EDITBUT_SAVEADD", 2);
define ("DEF_EDITBUT_UPDATE", 4);
define ("DEF_EDITBUT_DRAFT", 8);
define ("DEF_EDITBUT_PUB", 16);
define ("DEF_EDITBUT_REPLY", 128);
define ("DEF_EDITBUT_BACK", 256);
define ("DEF_EDITBUT_DELETE", 512);
define ("DEF_EDITBUT_STATS", 1024);
define ("DEF_EDITBUT_INFO", 2048);
define ("DEF_EDITBUT_LASTINFO", 4096);
define ("DEF_EDITBUT_PREVIEW", 8192);
define ("DEF_SETBUT_SAVE", 16384);
define ("DEF_EDITBUT_ADD_NEW", 32768);

define ("DEF_POST_ACTION_SAVE", "save");
define ("DEF_POST_ACTION_SAVEDRAFT", "savedraft");
define ("DEF_POST_ACTION_SAVEPUB", "savepub");
define ("DEF_POST_ACTION_SAVEREPLY", "savereply");
define ("DEF_POST_ACTION_SAVEADD", "saveadd");
define ("DEF_POST_ACTION_DELETE", "delete");

define ("DEF_PAGEBUT_ADDNEW", 1);
define ("DEF_PAGEBUT_ORGANIZER", 2);
define ("DEF_PAGEBUT_GOBACK", 4);
define ("DEF_PAGEBUT_EXPORT", 8);

define ("SAVEBUTTONPRESSED", "_savebuttonpressed");

define("EDITOR_BLOCK_NOT_STARTED_ERROR", "Editor block not started properly.  Include 'id' attribute in 'editorblock' element of page layout.");
define("EDITOR_FORM_NOT_STARTED_ERROR", "Editor form not started properly.  Include 'id', 'action', 'method', 'addenctype' attribute in 'form' element of page layout.");

class AdminRenderClass {
	// overloaded data
	private $form_id = null;
	private $layout_obj = array();
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_events;

		$s = new self;
		$_events->processTriggerEvent(__FUNCTION__.'_admin_render');				// alert triggered functions when function executes
		return $s;
	}

	// ----------- RENDERER AND RENDER LAYOUT OBJECT FUNCTIONS ---------------

	/**
	 * Process and display page elements stored in Render Layout Object
	 * @param array $layout_arry
	 */
	function renderAdminPage($layout_arry){
		global $_error, $_events, $_fields;

		if(!is_array($layout_arry)) {
			$_error->addErrorMsg("Page layout object must be an array!");
			return;
		}

	    // triggers
	    list($retn, ) = $_events->processTriggerEvent(__FUNCTION__, $layout_arry, array());
	    if(!empty($retn)) $layout_arry = $retn;

		$err = null;
		$caller = debug_backtrace();
		$callfile = (isset($caller[0]['file']) ? $caller[0]['file'] : 'unknown');
		$callline = (isset($caller[0]['line']) ? $caller[0]['line'] : 0);

		// first level, expecting: buttonblock, editorblock, and stats
		$buttons = 0;
		$this->form_id = null;
		$this->layout_obj = $layout_arry;

	    list($retn, ) = $_events->processTriggerEvent('addsections', array(), array());
	    if(!empty($retn)) {
	    	if(isset($retn[0]))
	    		foreach($retn as $arry) $this->addSections($arry);
	    	else
	    		$this->addSections($retn);
	    }

	    list($retn, ) = $_events->processTriggerEvent('addpanels', array(), array());
	    if(!empty($retn)) {
	    	if(isset($retn[0]))
	    		foreach($retn as $arry) $this->addPanels($arry);
	    	else
	    		$this->addPanels($retn);
	    }

	    list($retn, ) = $_events->processTriggerEvent('addobjects', array(), array());
	    if(!empty($retn)) {
	    	if(isset($retn[0]))
	    		foreach($retn as $arry) $this->addObjects($arry);
	    	else
	    		$this->addObjects($retn);
	    }

	    if(!is_array($this->layout_obj)){
			$_error->customErrorHandler(E_USER_NOTICE, "The Render Layout Object cannot be processed.", $callfile, $callline);
			return false;
	    }

		foreach($this->layout_obj as $l1key => $l1val){
			switch(strtolower($l1key)){
				case 'buttonblock':
					// expecting: prevpagebuttons and editorbuttons
					$this->startButtonBlock();
					foreach($l1val as $l2key => $l2val){
						switch(strtolower($l2key)){
							case 'prevpagebuttons':
								$this->showPrevPageButtons($l2val);
								break;
							case 'editorbuttons':
								$buttons = $l2val;
								$this->showEditorButtons($l2val);
								break;
							default:
								$_error->customErrorHandler(E_USER_NOTICE, "Button block key '$l2key' not valid in page layout.", $callfile, $callline);
								return false;
						}
					}
					if(($buttons & DEF_EDITBUT_STATS) > 0) $this->showStats();
					if(($buttons & DEF_EDITBUT_UPDATE) > 0 || ($buttons & DEF_EDITBUT_SAVE) > 0 || ($buttons & DEF_EDITBUT_SAVEADD) > 0) $this->showUpdateOptions();
					echo "</div>\n";
					break;
				case 'editorblock':
					// expecting: id, pagetitle, showreqdtextline and editorform
					$editorblockstarted = false;
					foreach($l1val as $l2key => $l2val){
						switch(strtolower($l2key)){
							case 'id':
								$editorblockstarted = $this->startEditorBlock($l2val);
								break;
							case 'pagetitle':
								if($editorblockstarted)
									$this->showPageTitle($l2val);
								else{
									$_error->customErrorHandler(E_USER_NOTICE, EDITOR_BLOCK_NOT_STARTED_ERROR, $callfile, $callline);
									return false;
								}
								break;
							case 'showreqdtextline':
								if($editorblockstarted){
									if($l2val) $_fields->showReqdTextLine();
								} else {
									$_error->customErrorHandler(E_USER_NOTICE, EDITOR_BLOCK_NOT_STARTED_ERROR, $callfile, $callline);
									return false;
								}
								break;
							case 'form':
								if($editorblockstarted){
									$atts = getIfSetArray($l2val, array("id" => "", "method" => "post", "action" => "", "addenctype" => "", "additionalfields" => null, "sections" => null));
									if(!empty($atts["id"])){
										// create form
										// expecting: id, action, method, addenctype, additionalfields
										$this->form_id = $atts["id"];
										if($this->startPageForm($atts['action'], $atts['method'], $atts['addenctype'], $atts['additionalfields'])){
											// expecting: sections
											if(!empty($atts["sections"])){
												$this->renderAdminSections($atts['sections']);
											} else {
												$_error->customErrorHandler(E_USER_NOTICE, "Editor forms must include a 'sections' array element.", $callfile, $callline);
											}
											$this->endPageForm();
										} else {
											$_error->customErrorHandler(E_USER_NOTICE, EDITOR_FORM_NOT_STARTED_ERROR, $callfile, $callline);
											return false;
										}
									} else {
										$_error->customErrorHandler(E_USER_NOTICE, EDITOR_FORM_NOT_STARTED_ERROR, $callfile, $callline);
										return false;
									}
								}
								break;
							default:
								$_error->customErrorHandler(E_USER_NOTICE, "Editor block key '$l2key' not valid in page layout.", $callfile, $callline);
								return false;
						}
					}
					if($editorblockstarted) $this->endBlock(BLOCK_DIV);
					break;
				default:
					$_error->customErrorHandler(E_USER_NOTICE, "Page key '$l1key' not valid key in Render Layout Object.", $callfile, $callline);
					return false;
			}
		}
		return true;
	}

	/**
	 * Prepare and output section blocks
	 * @param array $sections_atts
	 */
	function renderAdminSections($sections_atts){
		global $_fields, $_error;

		// expecting: key => section array
		$caller = debug_backtrace();
		$callfile = (isset($caller[0]['file']) ? $caller[0]['file'] : 'unknown');
		$callline = (isset($caller[0]['line']) ? $caller[0]['line'] : 0);
		if(is_array($sections_atts)){
			foreach($sections_atts as $section_key => $section_atts) {
				if(!is_array($section_atts)){
					$_error->customErrorHandler(E_USER_NOTICE, "The section element for '$section_key' must be an array.", $callfile, $callline);
					return false;
				}
				// expecting: class, panels
				$section_class = null;
				foreach($section_atts as $panel_key => $panel_atts) {
					switch($panel_key) {
						case "class":
							$section_class = $panel_atts;
							break;
						case "panels":
							$this->startSection(array("id" => $section_key, "class" => $section_class));
							$this->renderAdminSectionPanels($panel_atts);
							$this->endSection();
							break;
					}
				}
			}
		} else {
			$_error->customErrorHandler(E_USER_NOTICE, "Editor sections must be arrays", $callfile, $callline);
		}
	}

	/**
	 * Prepare and output panels contained in a section block
	 * @param array $panels_atts
	 */
	function renderAdminSectionPanels($panels_atts){
		global $_fields, $_error;

		// expecting: id, object arrays
		$caller = debug_backtrace();
		$callfile = (isset($caller[0]['file']) ? $caller[0]['file'] : 'unknown');
		$callline = (isset($caller[0]['line']) ? $caller[0]['line'] : 0);
		$allowed_panel_types = array("accordion", "div", "section");

		if(is_array($panels_atts)) {
			$panel_started = false;
			foreach($panels_atts as $panel_id => $panel_atts) {
				// we're at the meat of things now -- fields and data
				// each key in this subarray is the id of the fieldrow
				if(is_array($panel_atts)){
					if(!empty($panel_id) && !is_numeric($panel_id)) {
						$panel_type = getIfSet($panel_atts['type']);
						if(in_array($panel_type, $allowed_panel_types)){
							$panel_atts['id'] = $panel_id;
							if(isset($panel_atts['objects']) && is_array($panel_atts['objects'])) {
								if($panel_type != 'section' && $panel_type != 'div')
									$panel_atts['class'] = $panel_type;

								$objects = $panel_atts['objects'];
								unset($panel_atts['type']);
								unset($panel_atts['objects']);
								$_fields->showObject($this->form_id, "divblock", $panel_atts);

								// panel objects
								foreach($objects as $item_id => $item_val) {
									$item_type = strtolower(getIfSet($item_val['type']));
									if(!empty($item_type) && is_string($item_type)) {
										unset($item_val['type']); 	// remove the type since we have it now
										$item_val['id'] = $item_id;	// add the id for field functions
										$_fields->showObject($this->form_id, $item_type, $item_val);
									} else {
										$_error->customErrorHandler(E_USER_NOTICE, "The type attribute for section panel object '$item_id' is required and must be a string.", $callfile, $callline);
										return false;
									}
								}
								$this->endBlock(BLOCK_DIV);
							} else {
								$_error->customErrorHandler(E_USER_NOTICE, "Editor section panels require an array of contained objects.", $callfile, $callline);
								return false;
							}
						} else {
							$_error->customErrorHandler(E_USER_NOTICE, "Allowed editor section panel types are '".join("', '", $allowed_panel_types)."'.", $callfile, $callline);
							return false;
						}
					} else {
						$_error->customErrorHandler(E_USER_NOTICE, "Editor section panels must have non-numeric ids.", $callfile, $callline);
						return false;
					}
				} else {
					$_error->customErrorHandler(E_USER_NOTICE, "Editor section panels must be arrays.", $callfile, $callline);
					return false;
				}
			}
		} else {
			$_error->customErrorHandler(E_USER_NOTICE, "Editor section panels must be arrays", $callfile, $callline);
		}
		return true;
	}

	/**
	 * Adds one or more sections to the form the Render Layout Object,
	 * 		before a specific section (designated by key), or at end if $before is null
	 * @param array $sections_meta			Either an array containing section meta data
	 *											"sections" => sections (required),
	 *											"before" => key before which the new sections will be added (optional)
	 *											"data_type" => related to specific data type (optional)
	 *											"target_type" => related to specific target type (optional)
	 *											"action_type" => related to specific action type (optional)
	 *										or
	 *											just an array of sections
	 * @return boolean
	 */
	function addSections($sections_meta){
		global $_error, $_page;

		$ok = false;
		$caller = debug_backtrace();
		$callfile = (isset($caller[0]['file']) ? $caller[0]['file'] : 'unknown');
		$callline = (isset($caller[0]['line']) ? $caller[0]['line'] : 0);
		if(is_array($sections_meta)) {
			if(isset($sections_meta['data_type']) && !in($_page->datatype, $sections_meta['data_type'])) return false;
			if(isset($sections_meta['target_type']) && !in($_page->targettype, $sections_meta['target_type'])) return false;
			if(isset($sections_meta['action_type']) && !in($_page->targetaction, $sections_meta['action_type'])) return false;

			if(isset($this->layout_obj['editorblock']['form'])){
				if(!isset($this->layout_obj['editorblock']['form']['sections']))
					$this->layout_obj['editorblock']['form']['sections'] = array();

				$sections_obj = $this->layout_obj['editorblock']['form']['sections'];
				reset($sections_obj);

				$sections = isset($sections_meta['sections']) ? $sections_meta['sections'] : $sections_meta;
				$before = isset($sections_meta['before']) ? strtolower($sections_meta['before']) : null;
				if(is_null($before)) {
					// append
					$sections_obj_before = $sections_obj;
					$sections_obj_after = array();
				} else if($before == key($sections_obj)) {
					// prepend
					$sections_obj_before = array();
					$sections_obj_after = $sections_obj;
				} else {
					// split
					list($sections_obj_before, $sections_obj_after) = splitArray($sections_obj, $before);
				}

				foreach($sections as $key => $section) {
					if(!array_key_exists(strtolower($key), array_map('strtolower', array_keys($sections_obj)))) {
						$sections_obj_before[$key] = $section;
						$ok = true;
					}
				}
				if($ok) {
					$this->layout_obj['editorblock']['form']['sections'] = $sections_obj_before + $sections_obj_after;
				}
			}
		} else {
			$_error->customErrorHandler(E_USER_NOTICE, "The \$sections_meta must be an array", $callfile, $callline);
		}
		return $ok;
	}

	/**
	 * Adds one or more panels to a section in the Render Layout Object,
	 * 		before a specific panel (designated by key), or at end if $before is null
	 * @param array $panels_meta			An array containing panels meta data with required elements:
	 *											"panels" => panels,
	 *											"parent_section" => key of the containing section,
	 *											"before" => key before which the new panels will be added
	 *												or null to add to end
	 *											"data_type" => related to specific data type (optional)
	 *											"target_type" => related to specific target type (optional)
	 *											"action_type" => related to specific action type (optional)
	 * @return boolean
	 */
	function addPanels($panels_meta) {
		global $_error, $_page;

		$ok = false;
		$caller = debug_backtrace();
		$callfile = (isset($caller[0]['file']) ? $caller[0]['file'] : 'unknown');
		$callline = (isset($caller[0]['line']) ? $caller[0]['line'] : 0);
		if(is_array($panels_meta)) {
			if(isset($panels_meta['data_type']) && !in($_page->datatype, $panels_meta['data_type'])) return false;
			if(isset($panels_meta['target_type']) && !in($_page->targettype, $panels_meta['target_type'])) return false;
			if(isset($panels_meta['action_type']) && !in($_page->targetaction, $panels_meta['action_type'])) return false;

			if(isset($panels_meta['parent_section'])) {
				$parent_section = strval($panels_meta['parent_section']);
				if(isset($this->layout_obj['editorblock']['form']['sections'][$parent_section])) {
					if(isset($panels_meta['panels'])) {
						if(!isset($this->layout_obj['editorblock']['form']['sections'][$parent_section]['panels']))
							$this->layout_obj['editorblock']['form']['sections'][$parent_section]['panels'] = array();

						$panels_obj = $this->layout_obj['editorblock']['form']['sections'][$parent_section]['panels'];
						reset($panels_obj);

						$panels = $panels_meta['panels'];
						$before = isset($panels_meta['before']) ? strtolower($panels_meta['before']) : null;
						if(is_null($before)) {
							// append
							$panels_obj_before = $panels_obj;
							$panels_obj_after = array();
						} elseif($before == key($panels_obj)) {
							// prepend
							$panels_obj_before = array();
							$panels_obj_after = $panels_obj;
						} else {
							// split
							list($panels_obj_before, $panels_obj_after) = splitArray($panels_obj, $before);
						}

						foreach($panels as $key => $panel) {
							if(!array_key_exists(strtolower($key), array_map('strtolower', array_keys($panels_obj)))) {
								$panels_obj_before[$key] = $panel;
								$ok = true;
							}
						}
						if($ok) {
							$this->layout_obj['editorblock']['form']['sections'][$parent_section]['panels'] = $panels_obj_before + $panels_obj_after;
						}
					} else {
						$_error->customErrorHandler(E_USER_NOTICE, "The 'panels' element of the \$panels_meta array not found.", $callfile, $callline);
					}
				} else {
					$_error->customErrorHandler(E_USER_NOTICE, "You can only add panels to an existing section ($parent_section).", $callfile, $callline);
				}
			} else {
				$_error->customErrorHandler(E_USER_NOTICE, "The \$panels_meta must include the 'parent_section' key", $callfile, $callline);
			}
		} else {
			$_error->customErrorHandler(E_USER_NOTICE, "The \$panels_meta must be an array", $callfile, $callline);
		}
		return $ok;
	}

	/**
	 * Adds one or more objects to a panel in the Render Layout Object,
	 * 		before a specific object (designated by key), or at end if $before is null
	 * @param array $objects_meta			An array containing objects meta data with required elements:
	 *											"objects" => objects,
	 *											"parent_section" => key of the containing section,
	 *											"parent_panel" => key of the containing panel within the section,
	 *											"before" => key before which the new objects will be added
	 *												or null to add to end
	 *											"data_type" => related to specific data type (optional)
	 *											"target_type" => related to specific target type (optional)
	 *											"action_type" => related to specific action type (optional)
	 * @return boolean
	 */
	function addObjects($objects_meta) {
		global $_error, $_page;

		$ok = false;
		$caller = debug_backtrace();
		$callfile = (isset($caller[0]['file']) ? $caller[0]['file'] : 'unknown');
		$callline = (isset($caller[0]['line']) ? $caller[0]['line'] : 0);
		if(is_array($objects_meta)) {
			if(isset($objects_meta['data_type']) && !in($_page->datatype, $objects_meta['data_type'])) return false;
			if(isset($objects_meta['target_type']) && !in($_page->targettype, $objects_meta['target_type'])) return false;
			if(isset($objects_meta['action_type']) && !in($_page->targetaction, $objects_meta['action_type'])) return false;

			if(isset($objects_meta['parent_section'])) {
				$parent_section = strval($objects_meta['parent_section']);
				if(isset($objects_meta['parent_panel'])) {
					$parent_panel = strval($objects_meta['parent_panel']);
					if(isset($this->layout_obj['editorblock']['form']['sections'][$parent_section]['panels'][$parent_panel])) {
						if(isset($objects_meta['objects'])) {
							if(!isset($this->layout_obj['editorblock']['form']['sections'][$parent_section]['panels'][$parent_panel]['objects']))
								$this->layout_obj['editorblock']['form']['sections'][$parent_section]['panels'][$parent_panel]['objects'] = array();

							$objects_obj = $this->layout_obj['editorblock']['form']['sections'][$parent_section]['panels'][$parent_panel]['objects'];
							reset($objects_obj);

							$objects = $objects_meta['objects'];
							$before = isset($objects_meta['before']) ? strtolower($objects_meta['before']) : null;
							if(is_null($before)) {
								// append
								$objects_obj_before = $objects_obj;
								$objects_obj_after = array();
							} elseif($before == key($objects_obj)) {
								// prepend
								$objects_obj_before = array();
								$objects_obj_after = $objects_obj;
							} else {
								// split
								list($objects_obj_before, $objects_obj_after) = splitArray($objects_obj, $before);
							}

							foreach($objects as $key => $object) {
								if(!array_key_exists(strtolower($key), array_map('strtolower', array_keys($objects_obj)))) {
									$objects_obj_before[$key] = $object;
									$ok = true;
								}
							}
							if($ok) {
								$this->layout_obj['editorblock']['form']['sections'][$parent_section]['panels'][$parent_panel]['objects'] = $objects_obj_before + $objects_obj_after;
							}
						} else {
							$_error->customErrorHandler(E_USER_NOTICE, "The 'objects' element of the \$objects_meta array not found.", $callfile, $callline);
						}
					} else {
						$_error->customErrorHandler(E_USER_NOTICE, "You can only add objects to an existing panel ($parent_section//$parent_panel).", $callfile, $callline);
					}
				} else {
					$_error->customErrorHandler(E_USER_NOTICE, "The \$objects_meta must include the 'parent_panel' key", $callfile, $callline);
				}
			} else {
				$_error->customErrorHandler(E_USER_NOTICE, "The \$objects_meta must include the 'parent_section' key", $callfile, $callline);
			}
		} else {
			$_error->customErrorHandler(E_USER_NOTICE, "The \$objects_meta must be an array", $callfile, $callline);
		}
		return $ok;
	}

	// ----------- PAGE/FORM FUNCTIONS ---------------

	/**
	 * Render the header contents
	 * @param array $scripts 					A comma-separated or array list of Foundry UI scripts
	 * @param array $uiwidgets 					A comma-separated or array list of jquery-ui widgets
	 *												i.e: datepicker+accordion>fade+dialog
	 */
	function showHeader($scripts = array(), $uiwidgets = array()){
		global $_events, $_plugins, $_themes;

		$_events->processTriggerEvent(__FUNCTION__);
		foreach($GLOBALS as $key => $val){
			if(!in_array($key, array('GLOBALS', '_SERVER', '_COOKIE', '_SESSION', '_GET', '_POST', '_FILES', '_REQUEST', '_ENV'))){
				$$key = $val;
			}
		}

		if(!empty($scripts) && !is_array($scripts))
			$scripts = explode(",", sanitizeText($scripts));
		elseif(empty($scripts))
			$scripts = array();
		$_plugins->scripts = $scripts;

		if(!empty($uiwidgets) && !is_array($uiwidgets))
			$uiwidgets = explode(",", sanitizeText($uiwidgets));
		elseif(empty($uiwidgets))
			$uiwidgets = array();
		$_plugins->uiwidgets = $uiwidgets;

		$_themes->showHeaderFile();
	}

	/**
	 * Render the footer contents
	 */
	function showFooter(){
		global $_events, $_plugins, $_error, $_themes;

		$_events->processTriggerEvent(__FUNCTION__);
		foreach($GLOBALS as $key => $val){
			if(!in_array($key, array('GLOBALS', '_SERVER', '_COOKIE', '_SESSION', '_GET', '_POST', '_FILES', '_REQUEST', '_ENV'))){
				$$key = $val;
			}
		}

		$_themes->showFooterFile();

		?>
		<script type="text/Javascript" language="javascript">
		<?php if(getRequestVar('qact') == 'settings'){ ?>
			jQuery('#settingsdialog').load(
				'<?php echo WEB_URL.ADMIN_FOLDER?>settingsdialog.php',
				{parentpage: '<?php echo $_SERVER['PHP_SELF']?>'},
				function(){
					$('#settingsdialog').dialog('open');
				}
			);
		<?php }

		$_error->showErrorMsg(CORE_ERR);
		$_error->showErrorMsg(DEBUGGER_ERR+RUNTIME_ERR);
		echo PHP_EOL;

		$footer_id = rand(1000, 9999);
		?>
		</script>
		<style>
			#footer<?php echo $footer_id ?>{border-top:1px solid gray!important;bottom:0!important;clear:both!important;color:#000!important;font-size:13px!important;font-weight:700!important;height:25px!important;margin:10px auto 0!important;padding:5px 0 10px!important;position:fixed!important;width:100%!important;z-index:999!important}
		</style>

		<div id="footer<?php echo $footer_id ?>" class="adm_footer">
		    <span class="alignleft">
		        Copyright &copy; <?php echo date("Y")?> <a href="<?php echo COPYRIGHT_WEB?>" target="_blank"><?php echo COPYRIGHT_NAME?></a>. The Web at Your Best.&nbsp;&nbsp;&nbsp;
		    </span>

		    <span class="alignright">
		        <span class="blue"><?php echo SYS_NAME?>: <?php echo CODE_VER; ?></span>&nbsp;|&nbsp;PHP: <?php echo phpversion()?><?php if(defined('DBHOST')) echo '&nbsp;|&nbsp;MySQL: '.mysqli_get_client_version().PHP_EOL; ?>
		    </span>
		</div>
    </div>
	<?php
	$_plugins->showScriptSourceLines(true, false);
	?>
</body>
</html>
		<?php
	}

	/**
	 * Output admin page title
	 */
	function showAdminTitle(){
		global $_page, $_events;

	    $urlpath = $_page->urlpath;
	    $_page->title = getIfSet($urlpath[0]);
	    if(isset($urlpath[1]) && !empty($urlpath[1])) $_page->title .= ' - '.$urlpath[1];

		list($title) = $_events->processTriggerEvent(__FUNCTION__, SITE_NAME.' Admin: '.ucwordsAdv($_page->title));
		echo $title;
	}

	/**
	 * Displays a Page Title DIV block
	 * @param array $atts 		Field parameters presented in array format
	 * @uses string $text
	 */
	function showPageTitle($atts){
		global $_page, $_error;

		$_page->title = getScalarIfSet($atts, 'title');
		$obj = (isset($atts['obj']) ? $atts['obj'] : 'h3');
		print "<".$obj." id=\"title\">".$_page->title."</".$obj.">\n";
		$_error->showErrorMsg();
	}

	/**
	 * Displays Admin information
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $text
	 * @uses string $labelclass
	 */
	function showInformationRow($atts){
		$label = getIfSet($atts['label']);
	    print "<div class=\"editlabel col-2\">{$label}";
	    print "</div>";
	}

	/**
	 * Begin the main area in which all other editor and button content is displayed
	 */
	function startContentArea(){
	    global $_page, $_events;

	    $this->showBlock(array("id" => "content-wrapper", "displaytype" => FLD_OPENROW));
	    $this->showBlock(array("id" => "display_core_msg"));
	    $this->showBlock(array("id" => "display_runtime_msg"));
		$this->showBlock(array("id" => "contentarea", "displaytype" => FLD_OPENROW));

		list($content, $continue, $passthru) = $_events->processTriggerEvent(__FUNCTION__, '');
	    $_page->createSection("_contentarea", array("htmlobj" => "div", "area" => "content", "handler" => "system"));
		if($continue) {
			if($passthru)
				return $content;
			else
				echo $content;
		}
	}

	/**
	 * End content area wrapper
	 */
	function endContentArea(){
	    global $_page;

	    if(isset($_page->sections["_contentarea"])) {
	        echo "</div>".PHP_EOL;
	        echo "</div>".PHP_EOL;
	        $_page->deleteSection("_contentarea");
	    }
	}

	/**
	 * Return brief version of contents
	 * @param string $content 					Content being shortened
	 * @param integer $length [optional] 		Length of result in words
	 * @param string $url [optional] 			URL of page where entire content is found
	 * @param string $finish [optional] 		Characters added to end (default = '...')
	 * @return string
	 */
	function getShortContent($content, $length = 20, $url = '', $finish = '...') {
	    global $_events;

		list($content) = $_events->processTriggerEvent(__FUNCTION__, $content);

	    // Clean and explode our content, Strip all HTML tags, and special charactors.
	    $words = explode(' ', strip_tags(preg_replace('/[^(\x20-\x7F)]*/','', $content)));

	    // Get a count of all words, and check we have less/more than our required amount of words.
	    $count = count($words);
	    $limit = ($count > $length) ? $length : $count;

	    // if we have more words than we want to show, add our ...
	    $end   = ($count > $length && $url != '') ? ' [<a href="'.$url.'">'.$finish.'</a>]' : '';

	    // create output
	    for($w = 0; $w <= $limit; $w++) {
	        $output .= $words[$w];
	        if($w < $limit) $output .= ' ';
	    }

	    // return end result.
	    return $output.$end;
	}

	/**
	 * Set page help contents (seen when the Help button is pressed)
	 * @param string $content 					Content of help panel
	 */
	function setPageHelp($content){
		global $_page, $_events;

		list($content) = $_events->processTriggerEvent(__FUNCTION__, $content);
		$_page->help = $content;
	}

	/**
	 * Starts a section
	 * @param array $atts       Field parameters presented in array format
	 * @uses string $id
	 * @uses string $class
	 * @uses string $style
	 * @uses string $js
	 */
	function startSection($atts){
	    global $_page;

	    $class = getFormatIfSet($atts['class'], "", " class=\"%s\"");
	    $id = getFormatIfSet($atts['id'], "", " id=\"%s\"");
	    $style = getFormatIfSet($atts['style'], "", " style=\"%s\"");
	    $js = getIfSet($atts['js']);
	    echo "<section{$id}{$class}{$style}{$js}>\n";
	    return true;
	}

	/**
	 * Ends section
	 */
	function endSection(){
	    echo "</section>\n";
	}

	/**
	 * Begin the button area which contains back, save, preview and status buttons
	 */
	function startButtonBlock(){
	    $this->showBlock(array("class" => "editor_buttonbox", "displaytype" => FLD_OPENROW));
	}

	/**
	 * Begin the editor area where user record data is entered
	 * @param string $id        The id value of the DOM object
	 */
	function startEditorBlock($id){
	    $this->showBlock(array("id" => $id, "class" => "fullwidth", "displaytype" => FLD_OPENROW));
	    return true;
	}

	/**
	 * Begin an open div/p/span block
	 * @param array $atts       Field parameters presented in array format
	 */
	function startBlock($atts){
	    $blocktype = getIfSet($atts['blocktype']);
	    $expand = getIfSet($atts['expander']);
	    if($expand == 'normal') $expand = '<a href="#" class="expand-button fa fa-arrow-up"></a>';
	    switch($blocktype){
	        case BLOCK_DIV:
	            $atts['class'] = "editdiv";
	            if(isset($atts['label'])) $atts['label'] .= $expand;
	            break;
	        case BLOCK_P:
	            $atts['class'] = "editpara";
	            if(isset($atts['label'])) $atts['label'] .= $expand;
	            break;
	        case BLOCK_SPAN:
	            $atts['class'] = "editspan";
	            break;
	        default:
	            return false;
	            break;
	    }
	    $atts['displaytype'] = FLD_OPENROW;
	    $this->showBlock($atts);
	    return true;
	}

	/**
	 * End an open div/p/span block
	 * @param string $blocktype     Type of object (DIV, SPAN, or P)
	 */
	function endBlock($blocktype = BLOCK_DIV){
	    if($blocktype == BLOCK_SPAN){
	        print "</span>\n";
	    }elseif($blocktype == BLOCK_DIV){
	        print "</div>\n";
	    }elseif($blocktype == BLOCK_P){
	        print "</p>\n";
	    }
	    return true;
	}

	/**
	 * Displays a DIV, SPAN or P block.  Options include: style, javascript, and whether or not to close block
	 * @param array $atts       Field parameters presented in array format
	 * @param string $text
	 * @param string $id
	 * @param string $class
	 * @param string $blocktype
	 * @param string $style
	 * @param string $js
	 * @param integer $displaytype
	 */
	function showBlock($atts){
	    $id = getFormatIfSet($atts['id'], "", " id=\"%s\"");
	    $label = getFormatIfSet($atts['label'], "", "<h2>%s</h2>");
	    $text = getIfSet($atts['text']);
	    $class = getIfSet($atts['class']);
	    $blocktype = getIfSet($atts['blocktype'], BLOCK_DIV);
	    $style = getFormatIfSet($atts['style'], "", " style=\"%s\"");
	    $js = getFormatIfSet($atts['js'], "", " %s");
	    $expandable = getBooleanIfSet($atts['expandable']);
	    $collapsed = getBooleanIfSet($atts['collapsed']);
	    $displaytype = getIfSet($atts['displaytype'], FLD_ALL);

	    if($expandable) $class .= " expandable";
	    if($collapsed) $class .= " collapsed";
	    if($class != "") $class = " class=\"{$class}\"";
	    if($blocktype == BLOCK_SPAN){
	        print "<span{$id}{$class}{$style}{$js}>\n".$label;
	        if($text != "") print $text."\n";
	        if($displaytype == FLD_ALL) print "</span>\n";
	    }elseif($blocktype == BLOCK_DIV){
	        print "<div{$id}{$class}{$style}{$js}>\n".$label;
	        if($text != "") print "<p>".$text."</p>\n";
	        if($displaytype == FLD_ALL) print "</div>\n";
	    }elseif($blocktype == BLOCK_P){
	        print "<p{$id}{$class}{$style}{$js}>\n".$label;
	        if($text != "") print $text."\n";
	        if($displaytype == FLD_ALL) print "</p>\n";
	    } else {
	        print $text;
	    }
	}

	/**
	 * Start page form wrapper
	 * @param string $action 				Destination URL for form
	 * @param string $method 				POST or GET
	 * @param boolean $enctype 				Encoding type of form
	 * @param array $additional_hidden_fields
	 */
	function startPageForm($action = "", $method = "post", $enctype = false, $additional_hidden_fields = null){
		global $_page, $_events, $_fields;

		list($content) = $_events->processTriggerEvent(__FUNCTION__);

		if(in_array($_page->targetaction, array('edit', 'add')) || in_array($_page->targettype, array('edit', 'add')))
			$page_type = 'editor';
		else
			$page_type = 'list';

		if (empty($this->form_id)) $this->form_id = 'list_form';
		if (empty($action)) $action = $_SERVER['REQUEST_URI'];
		($enctype) ? $enctype_code = ' enctype="multipart/form-data"' : $enctype_code = "";
		print "<form name=\"{$this->form_id}\" id=\"{$this->form_id}\" action=\"{$action}\" method=\"{$method}\"{$enctype_code}>".PHP_EOL;
		$_fields->showHiddenField(array("id" => "_n", "value" => $_page->nonce));
	    $_fields->showHiddenField(array("id" => "admin_url", "value" => WEB_URL.ADMIN_FOLDER));
	    $_fields->showHiddenField(array("id" => "page", "value" => $_page->pagenum));
		$_fields->showHiddenField(array("id" => "page_id", "value" => $_page->id));
		$_fields->showHiddenField(array("id" => "page_url", "value" => $_SERVER['REQUEST_URI']));
		$_fields->showHiddenField(array("id" => "page_subject", "value" => $_page->subject));
		$_fields->showHiddenField(array("id" => "page_childsubject", "value" => $_page->childsubject));
		$_fields->showHiddenField(array("id" => "page_ingroup", "value" => $_page->ingroup));
		$_fields->showHiddenField(array("id" => "page_parentgroup", "value" => $_page->parentgroup));
		$_fields->showHiddenField(array("id" => "page_type", "value" => $page_type));
		$_fields->showHiddenField(array("id" => "row_id", "value" => $_page->row_id));
		$_fields->showHiddenField(array("id" => "taxonomy_id", "value" => $_page->taxonomy_id));
		$_fields->showHiddenField(array("id" => "term_id", "value" => $_page->term_id));
	    $_fields->showHiddenField(array("id" => "full_delete", "value" => FULL_DELETE));
	    $_fields->showHiddenField(array("id" => "published", "value" => $_page->attributes['published']));
	    $_fields->showHiddenField(array("id" => "draft", "value" => $_page->attributes['draft']));
	    $_fields->showHiddenField(array("id" => "date_created", "value" => $_page->attributes['date_created']));
	    $_fields->showHiddenField(array("id" => "date_published", "value" => $_page->attributes['date_published']));
		$_fields->showHiddenField(array("id" => "date_updated", "value" => $_page->attributes['date_updated']));
		$_fields->showHiddenField(array("id" => "in_use", "value" => $_page->attributes['in_use']));
	    $_fields->showHiddenField(array("id" => SAVEBUTTONPRESSED));
		$_fields->showHiddenField(array("id" => "x_data"));
		$_fields->showHiddenField(array("id" => "cmd"));
		if(is_array($additional_hidden_fields)){
			foreach($additional_hidden_fields as $field => $value){
				$_fields->showHiddenField(array("id" => $field, "value" => $value));
			}
		}
		echo $content;

	    $_page->createSection($this->form_id, array("htmlobj" => "div", "area" => "form", "handler" => "system", "parentkey" => "_contentarea"));
	    return true;
	}

	/**
	 * End page form wrapper
	 */
	function endPageForm(){
	    global $_page, $_events;

	    if(isset($_page->sections[$this->form_id])) {
	    	list($content) = $_events->processTriggerEvent(__FUNCTION__);
	    	echo($content);
	    	echo "</form>".PHP_EOL;
	        $_page->deleteSection($this->form_id);
	    }
	}

	/**
	 * Output instructions block (between search and list areas)
	 * @param str $text
	 */
	function showInstructions($text){
	    global $_events;

		list($text) = $_events->processTriggerEvent(__FUNCTION__, $text);
	    if($text != ''){
			print "<div id=\"instruct_content\">".PHP_EOL;
	        print $text;
			print "</div>".PHP_EOL;
	    }
	}

	/**
	 * Output list block
	 * @param string $formname
	 * @param array $hiddenfields
	 * @param array $recset
	 * @param string $buttoncondindex [optional]
	 * @param string $buttontagfield [optional]
	 * @param boolean $allowsort [optional]
	 * @tutorial        // hover col (attr:hover)<br/>
	 *                  // file existence test (attr:fileexists)<br/>
	 *                  // quick edit col (attr:quickedit)<br/>
	 *                  // simple boolean test (attr:bool; trueval:yes; falseval:no)<br/>
	 *                  // conditional expression (attr:expr; compareusing:{=,>,<,>=,<=,!=}; compareval:value)<br/>
	 *                  // conditional expression (attr:expr; compareusing:{=,>,<,>=,<=,!=}; compareval:value; trueval:action; falseval:action; style:style; wrap:true|false)<br/>
	 *                  // image (attr:image; {thumbfield:name;} titlefield:name;)<br/>
	 *                  // indentation (attr:indent; {padstr:str}{countfield:name; }{checkfield:name; }{checkval:val;})<br/>
	 */
	function showList($formname, $hiddenfields, $recset, $buttoncondindex = "", $buttontagfield = "", $allowsort = true) {
		global $_db_control;
		global $_page, $_events, $_themes;
		global $cols, $colattr, $colsize, $sortcols, $buttons, $totalcols;

		list($params) = $_events->processTriggerEvent(__FUNCTION__, array($formname, $hiddenfields, $recset, $buttoncondindex, $buttontagfield, $allowsort));
		extract($params);

		if(is_bool($buttoncondindex)) die(__FUNCTION__.": Argument mismatch at \$buttoncondindex!");
		if(is_bool($buttontagfield)) die(__FUNCTION__.": Argument mismatch at \$buttontagfield!");

		if(is_array($cols)) {
			// button action setup
			if (!is_array($buttons)) $buttons = array(DEF_ACTION_EDIT, DEF_ACTION_DELETE);

			// columns setup
			$cols['actions'] = "";
			$colsize['actions'] = "";
	        $attr = array();
	        $style = "";

	        // persist list generation data in register
			$persist = array(
							'query_obj' => serialize($_db_control->getStorage()),
							'query' => $_db_control->lastquery,
							'cols' => $cols,
							'colattr' => $colattr,
							'colsize' => $colsize,
							'totalcols' => $totalcols,
							'buttons' => $buttons,
							'buttontagfield' => $buttontagfield,
							'buttoncondindex' => $buttoncondindex,
							'altparams' => $_page->altparams,
							'altgroups' => $_page->altgroups,
							'addqueries' => $_page->addqueries,
							'titlefld' => $_page->titlefld,
							'imagefld' => $_page->imagefld,
							'thumbfld' => $_page->thumbfld
			);
			$_SESSION[$_page->uri]['persist_data'] = $persist;

			// custom hidden fields
			if (isset($hiddenfields) && is_array($hiddenfields)) {
				foreach($hiddenfields as $key => $value) {
					if($key != "") {
						print "	<input type=\"hidden\" name=\"$key\" id=\"$key\" value=\"$value\" />".PHP_EOL;
					}
				}
			}

	        // move icon column (if present) to first element position
	        if(isset($cols['_icon'])) {
	            $icon = $cols['_icon'];
	            unset($cols['_icon']);
	            $cols = array('_icon' => $icon) + $cols;
	        }

	        // move checkbox column (if present) to first element position
	        if(isset($cols['_chk'])) {
	        	unset($cols['_chk']);
	        	$cols = array('_chk' => '') + $cols;
	        }

			// table start
	        print "<!-- TABLE -->".PHP_EOL;
	        print "<div class=\"listtable\">".PHP_EOL;

			// column header row
	        print "<!-- TABLE-HEADER -->".PHP_EOL;
			print "<div class=\"listheader\">".PHP_EOL;
			$colattr = $this->prepColAttr($cols, $colattr);

	        foreach ($cols as $key => $value) {
				$size = explode("|", getIfSet($colsize[$key]));
	            if($key == "_icon")
	                $style = ' style="width: 20px;"';
	            else{
	    			$width = ((!empty($size[0])) ? "width: ".$size[0] : "");
	    			$width = preg_replace("/;(.*);/i", "", $width);
	                $style = (($width != '') ? ' style="'.$width.'"' : "");
	            }
				print "<div{$style} class=\"listheader-cell\">";
				if($allowsort) {
	    			if($_page->sort_by == $key) {
	    				if(strtolower($_page->sort_dir) == "asc" || $_page->sort_dir == "") {
	    					$imgsrc = WEB_URL.$_themes->getThemePathUnder("admin")."images/icons/arrow-up.png";
	    					$imgdir = "desc";
	    					$class  = "fa fa-arrow-circle-up";
	    				} else {
	    					$imgsrc = WEB_URL.$_themes->getThemePathUnder("admin")."images/icons/arrow-dn.png";
	    					$imgdir = "asc";
	    					$class  = "fa fa-arrow-circle-down";
	    				}
	    				print $value;
	    				print "&nbsp;<a href=\"\" alt=\"Re-sort this column\" title=\"Re-sort this column\" rel=\"$imgdir\" class=\"listcol-sort\"><i class=\"{$class}\"></i></a>";
	    			}elseif(array_key_exists($key, $sortcols)){
	    				print "<a href=\"Javascript: $('#sort_by').val('$key'); $('#list_form').submit();\">$value</a>";
	    			}elseif($key == "_chk"){
	    				print "<img id=\"listrow-bulk-actions-check\" src=\"".WEB_URL.ADMIN_FOLDER.CORE_FOLDER."images/general/check.png\" alt=\"Click for bulk actions\" title=\"Click for bulk actions\" />&nbsp;";
	            		print "<div id=\"listrow-bulk-actions\">
	            					<em>Bulk Actions</em>
		            				<a href=\"#select_all\">Select All</a>
		            				<a href=\"#deselect_all\">Deselect All</a>";

	            		$blended_button_array = multiarray_unique($buttons);
	            		foreach($blended_button_array as $label){
	            			$actions = explode("::", $label);
	            			if(in_array($actions[0], array(DEF_ACTION_CLONE, DEF_ACTION_DELETE, DEF_ACTION_EXPORT, DEF_ACTION_PUBLISH, DEF_ACTION_SEND, DEF_ACTION_SUBSCRIBE, DEF_ACTION_UNPUBLISH, DEF_ACTION_UNSUBSCRIBE)) || ($actions[0] == DEF_ACTION_UNDELETE && !FULL_DELETE)){
			            		print "<a href=\"#{$actions[0]}\">".ucwords($actions[0])."</a>".PHP_EOL;
	            			}
	            		}
	            		print "		<a href=\"#advanced\">Advanced...</a>";
	            		print "</div>".PHP_EOL;
	            		print "<div id=\"listrow-bulk-actions-adv\">";
	            		$this->showCloseButton(true, true);
	            		print "<div class=\"listrow-bulk-actions-adv-content\"></div>";
	            		print "</div>".PHP_EOL;
	                }elseif($key == "_icon"){
	                    print "&nbsp;";
	    			}else {
	    				print $value;
					}
				}
				print "</div>".PHP_EOL;
			}
			print "</div>".PHP_EOL;
	        print "<!-- /TABLE-HEADER -->".PHP_EOL;

			// data rows
	        print "<!-- TABLE-BODY -->".PHP_EOL;
			print "<div id=\"listbody\">".PHP_EOL;
			$hoverdivs = $this->showListDataRows($recset, $cols, $colsize, $totalcols, $colattr, $buttons, $buttoncondindex, $buttontagfield);
			print "</div>".PHP_EOL;
	        print "<!-- /TABLE-BODY -->".PHP_EOL;

			// table end
			print "</div>".PHP_EOL;
	        print "<!-- /TABLE -->".PHP_EOL;

			if($hoverdivs != '') print $hoverdivs;
		}
	}

	/**
	 * Return the array of action buttons for the list row based on specific input conditions
	 * @param array $rec  					Data record
	 * @param array $buttons 				Array of buttons
	 * @param string $buttoncondindex 		Conditional key for a multi-dimensional button array
	 * @return array
	 */
	function getListButtonArray($rec, $buttons, $buttoncondindex = ""){
		$button_keys = array_keys($buttons);

		// assign button array to working array
		if($buttoncondindex != ""){
			// conditional index is set
			$recbuttonindex = getIfSet($rec[$buttoncondindex]);
			if(!empty($recbuttonindex)){
				// record includes value for conditional index column
				if(is_array($buttons[$recbuttonindex])){
					// conditional array prepared properly
					$button_array = $buttons[$recbuttonindex];
				} else {
					// conditional array not an array
					$button_array = array($buttons[$recbuttonindex]);
				}
			}elseif(is_array($buttons[0])){
				// record does not include conditional index value, use the first button array
				$button_array = $buttons[0];
			} else {
				$button_array = $buttons;
			}
		} else {
			// no conditional index set (single dimensional array)
			$button_array = $buttons;
		}
		return $button_array;
	}

	/**
	 * Process the list column attributes array ($colattr)
	 * @return array $colattr
	 */
	function prepColAttr($cols, $colattr){
		foreach ($cols as $key => $value) {
			if(isset($colattr[$key])) {
				// break colattr element value by semi-colons
				$attrstr = str_replace("\;", "¦", trim($colattr[$key]));
				$attrs = preg_split("(;|; )", $attrstr);
				foreach($attrs as $attrkey => $attrpair){
					// break attrpair (attr:hover) by colon
					$s = explode(":", $attrpair);
					$left_elem = $s[0];
					if(count($s) > 2){
						array_shift($s);
						$right_elem = join(":", $s);
					} else {
						$right_elem = $s[1];
					}
					$attr[trim($left_elem)] = str_replace("¦", ";", $right_elem);
				}

				$colattr[$key] = array('orig' => $colattr[$key]);
				switch($attr['attr']){
					case "hover":
						// hover col (attr:hover)
						$colattr[$key]['hover'] = true;
						break;
					case "fileexists":
						// file existence test (attr:fileexists)
						$colattr[$key]['fileexist'] = true;
						break;
					case "quickedit":
						// quick edit col (attr:quickedit)
						$colattr[$key]['xtra'] = DEF_ACTION_EDIT;
						break;
					case "boolean":
					case "bool":
						// simple boolean test (attr:bool; trueval:yes; falseval:no)
						// true if not zero, null or blank
						$colattr[$key]['boolean'] = true;
						$colattr[$key]['trueval'] = getIfSet($attr['trueval']);
						$colattr[$key]['falseval'] = getIfSet($attr['falseval']);
						break;
					case "expr":
					case "if":
						// conditional expression (attr:expr/if; compareusing:{=,>,<,>=,<=,!=}; compareval:value)
						$colattr[$key]['expr'] = true;
						$colattr[$key]['compareusing'] = getIfSet($attr['compareusing']);
						$colattr[$key]['compareval'] = getIfSet($attr['compareval']);
						break;
					case "advexpr":
					case "advif":
						// conditional expression (attr:advexpr/advif; compareusing:{=,>,<,>=,<=,!=}; compareval:value; trueval:action; falseval:action; style:style; wrap:true|false)
						$colattr[$key]['advexpr'] = true;
						$colattr[$key]['compareusing'] = getIfSet($attr['compareusing']);
						$colattr[$key]['compareval'] = getIfSet($attr['compareval']);
						$colattr[$key]['trueval'] = getIfSet($attr['trueval']);
						$colattr[$key]['falseval'] = getIfSet($attr['falseval']);
						$colattr[$key]['style'] = getIfSet($attr['style']);
						$colattr[$key]['wrap'] = getIfSet($attr['wrap']);
						break;
					case "image":
						// image (attr:image; {thumbfield:name;} titlefield:name;)
						$colattr[$key]['image'] = true;
						$colattr[$key]['thumbfield'] = ((empty($attr['thumbfield'])) ? 'thumb' : $attr['thumbfield']);
						$colattr[$key]['titlefield'] = ((empty($attr['titlefield'])) ? $key : $attr['titlefield']);
						break;
					case "indent":
						// indentation (attr:indent; {padstr:str}{countfield:name; }{checkfield:name; }{checkval:val;})
						$colattr[$key]['indent'] = true;
						$colattr[$key]['padstr'] = ((!empty($attr['padstr'])) ? str_replace("'", "", $attr['padstr']) : ' ');
						$colattr[$key]['countfield'] = getIfSet($attr['countfield']);
						$colattr[$key]['checkfield'] = getIfSet($attr['checkfield']);
						$colattr[$key]['checkval'] = getIfSet($attr['checkval']);
						break;
		            case "date":
		                // date formatting (attr:date; format:dateformat)
		                $colattr[$key]['date'] = true;
		                $colattr[$key]['format'] = getIfSet($attr['format']);
						break;
					case "format":
						// string formatting (attr:format; format:pattern)
						$colattr[$key]['format'] = true;
		                $colattr[$key]['format'] = getIfSet($attr['format']);
						break;
				}
			}
		}
		return $colattr;
	}

	/**
	 * Return the label for a specific action button
	 * @param string $butlabel 				The button label with or without alias (::tag)
	 * @return string
	 */
	function getListActionLabel($butlabel){
		/* button label can be:
		 * 	a. single value such as DEF_ACTION_EDIT, or
		 *  b. aliased value appended to label with ::
		*/

		$aliaspos  = strpos($butlabel, "::");
		$alias     = "";
		$buttontag = "";
		if ($aliaspos !== false){
			// aliased label
			$label = substr($butlabel, 0, $aliaspos);
			$alias = substr($butlabel, $aliaspos+2);
			if(strpos($alias, "|") !== false){
				$buttontag = substr($alias, strpos($alias, "|") + 1);
				$alias = substr($alias, 0, strpos($alias, "|"));
			}
		} else {
			$label = $butlabel;
		}
		return array($label, $buttontag, $alias);
	}

	/**
	 * Output all list data rows from data record set
	 * @param array $recset 				Data records
	 * @param array $cols 					Column headings array
	 * @param array $colsize 				Column size array
	 * @param array $totalcols 				Total columns
	 * @param array $colattr 				Column attributes array
	 * @param array $buttons 				Button actions array
	 * @param string $buttontagfield  		Button tag field
	 * @param object $_altpage (optional)
	 */
	function showListDataRows($recset, $cols, $colsize, $totalcols, $colattr,
						      $buttons, $buttoncondindex, $buttontagfield,
						      $_altpage = null){

		global $_page, $_db_control, $_filesys, $_themes;

		$coltotal = array();
		$rowcolor = LIST_ROWCOLOR1;
		$hoverdivs = '';
	    $themefolder = $_themes->getThemePathUnder('admin', THEME_PATH_ROOT, 0);

		if(is_null($_altpage)){
			$pagedata = $_page;
		} else {
			$pagedata = (object) $_altpage;
		}

		if(isset($recset) && is_array($recset)) {
			foreach($recset as $i => $rec) {
				// row field data
				$row_id = $rec['id'];
				$listhighlight = ((empty($listhighlight)) ? " listrow-highlight" : "");
				print "<div class=\"listrow{$listhighlight}\" id=\"listrow_{$row_id}\">".PHP_EOL;

				foreach($cols as $key => $value) {
					$size = explode("|", getIfSet($colsize[$key]));
					$width = (($size[0] != "") ? "width: ".$size[0]."; " : "");
					if(!isset($size[1])) $size[1] = "";
					$kval = getIfSet($rec[$key]);

					// calc totals (optional)
					if(isset($totalcols[$key])){
						$coltotal[$key] += floatval($kval);
					}

					if($key == "_chk"){
						// checkbox field
						print "<div class=\"listrow-cell listrow-check-cell\">";
						print "<input type=\"checkbox\" class=\"listrow-check\" name=\"listrow_check[]\" id=\"listrow-check-$i\" value=\"{$rec['id']}\" />";
						print "</div>".PHP_EOL;

	                }elseif($key == "_icon"){
	                    // icon field
	                    if(strpos($value, ".") !== false)
	                        $icon = "<img src=\"".WEB_URL.$themefolder."images/icons/".$value."\" alt=\"\" title=\"\" />&nbsp;";
	                    else
	                        $icon = "<i class=\"fa fa-".$value."\"></i>&nbsp;";
	                    print "<div class=\"listrow-cell listrow-icon-cell\">".$icon."</div>".PHP_EOL;

					}elseif(isset($colattr[$key]['hover']) && !empty($kval)) {
						// hover cell
						// (attr:hover)
						print "<div style=\"$width\" class=\"listrow-cell\">";
						print "<a class=\"listrow-hoverbox\" rel=\"".$key.$i."\" ";
						print "href=\"#\" class=\"action_edit\" rel=\"{$row_id}\">";
						print "Hover to View</a>";
						print "</div>".PHP_EOL;
						$hoverdivs .= "<div id=\"".$key.$i."\" class=\"box\">".nl2br(strip_tags($kval, '<br/><p><a><h1><h2><h3><h4><h5><h6><img><em><strong><b><i><u>'))."</div>";

					}elseif(isset($colattr[$key]['fileexist'])) {
						// file existence test
						// (attr:fileexists)
						$path = pathinfo($kval);
						$file = $path['basename'];
						if(!empty($kval)) {
							if(@file_exists($kval)){
								$dataexists = "Yes";
							} else {
								$dataexists = "Invalid";
							}
						} else {
							$dataexists = "No";
						}
						print "<div style=\"$width\" class=\"listrow-cell\">".$dataexists."</div>".PHP_EOL;

					}elseif(isset($colattr[$key]['boolean'])) {
						// simple boolean test
						// (attr:bool; trueval:yes; falseval:no)
						if(($kval != 0 && is_numeric($kval)) || ($kval != "" && !is_numeric($kval))) {
							$dataexists = $colattr[$key]['trueval'];
						} else {
							$dataexists = $colattr[$key]['falseval'];
						}
						print "<div style=\"$width\" class=\"listrow-cell\">".$dataexists."</div>".PHP_EOL;

					}elseif(isset($colattr[$key]['expr'])) {
						// expression test 1
						// conditional expression (attr:expr; compareusing:{=,>,<,>=,<=,!=}; compareval:value)
						$flag = "";
						$value1 = $kval;
						$value2 = str_replace("'", "", getIfSet($colattr[$key]['compareval']));
						switch (getIfSet($colattr[$key]['compareusing'])){
							case ">":
								if($value1 > $value2) $flag = '*';
								break;
							case "<":
								if($value1 < $value2) $flag = '*';
								break;
							case ">=":
								if($value1 >= $value2) $flag = '*';
								break;
							case "<=":
								if($value1 <= $value2) $flag = '*';
								break;
							case "!=":
								if($value1 != $value2) $flag = '*';
								break;
							default:
								if($value1 == $value2) $flag = '*';
								break;
						}
						print "<div style=\"$width\" class=\"listrow-cell\">{$value1} $flag</div>".PHP_EOL;

					}elseif(isset($colattr[$key]['advexpr'])) {
						// expression test 2
						// conditional expression (attr:advexpr; compareusing:{=,>,<,>=,<=,!=}; compareval:value; trueval:action; falseval:action; style:style; wrap:true|false)
						$flag = "";
						$value1 = $kval;
						$value2 = str_replace("'", "", getIfSet($colattr[$key]['compareval']));
						$fldcompareok  = false;
						$cellstyle = "";
						$wrap = "";
						switch (getIfSet($colattr[$key]['compareusing'])){
							case ">":
								if($value1 > $value2) $fldcompareok = true;
								break;
							case "<":
								if($value1 < $value2) $fldcompareok = true;
								break;
							case ">=":
								if($value1 >= $value2) $fldcompareok = true;
								break;
							case "<=":
								if($value1 <= $value2) $fldcompareok = true;
								break;
							case "!=":
								if($value1 != $value2) $fldcompareok = true;
								break;
							default:
								if($value1 == $value2) $fldcompareok = true;
								break;
						}

						$setting = (($fldcompareok) ? getIfSet($colattr[$key]['trueval']) : getIfSet($colattr[$key]['falseval']));

						if ($colattr[$key]['style'] != ''){
							$cellstyle = " ".$colattr[$key]['style'].";";
						}
						if (strpos(strtolower(getIfSet($colattr[$key]['wrap'])), 'val') !== false){
							$setting = str_replace('val', $setting, strtolower($colattr[$key]['wrap']));
						}
						if (substr($setting, 0, 2) == ">>"){
							$kval = substr($setting, 2);
						} else {
							$kval = $setting;
						}
						$cell_text = (($size[1] != "" && strlen($kval) > intval($size[1])) ? substr($kval, 0, intval($size[1]))."..." : $kval);
						print "<div style=\"".$width.$cellstyle."\" class=\"listrow-cell\">".$cell_text."</div>".PHP_EOL;

					}elseif(isset($colattr[$key]['image'])) {
						// show image instead of link
						// image (attr:image; {thumbfield:name;} titlefield:name;)
						if(empty($colattr[$key]['thumbfield'])) $colattr[$key]['thumbfield'] = 'thumb';
						if(empty($colattr[$key]['titlefield'])) $colattr[$key]['titlefield'] = $key;
						$title = $rec[$colattr[$key]['titlefield']];
						if(!empty($kval)){
							$folder = $_db_control->table;
							// if($folder == DB_TABLE_PREFIX."photos_cat") $folder = DB_TABLE_PREFIX."photos";
							$imgpath = $_filesys->checkImagePath($kval, MEDIA_FOLDER.IMG_UPLOAD_FOLDER.$folder, "");
							$thmpath = $_filesys->checkThumbPath($rec[$colattr[$key]['thumbfield']], MEDIA_FOLDER.THM_UPLOAD_FOLDER.$folder, "");
							if($thmpath != "" && $thmpath != MEDIA_FOLDER.THM_UPLOAD_FOLDER){
								list($imgwidth, $imgheight) = $_filesys->constrainImage(SITE_PATH.$thmpath, THM_MAX_WIDTH, THM_MAX_HEIGHT);
								$imgname = basename($imgpath);
								$img = "<a href=\"#\" class=\"action_edit\" rel=\"".$row_id."\">".$title."</a><br/>";
								$img.= "<div class=\"listimage\" style=\"width: ".($imgwidth+6)."px\">";
								$img.= "<a href=\"#\" class=\"action_edit\" rel=\"".$row_id."\">";
								$img.= "<img src=\"".WEB_URL.$thmpath."\" width=\"$imgwidth\" height=\"$imgheight\" alt=\"$imgname\" title=\"$imgname\" /></a>";
								$img.= "</div>";
							} else {
								$img = "<a href=\"#\" class=\"action_edit\" rel=\"".$row_id."\">".$title."</a><br/>";
							}
						} else {
							$img = "<div style=\"border: 1px solid #bbb; padding: 2px; width: 102px;\">$title</div>";
						}
						print "<div style=\"$width\" class=\"listrow-cell\">".$img."</div>".PHP_EOL;

					}elseif(isset($colattr[$key]['indent'])) {
						// indent data by various means
						// (attr:indent; {padstr:str}{countfield:name; }{checkfield:name; }{checkval:val;})
						$count = 0;
						$indent = '';
						if(!empty($colattr[$key]['countfield'])){
							$count = intval($rec[$colattr[$key]['countfield']]);
						}elseif(!empty($colattr[$key]['checkfield']) && !empty($colattr[$key]['checkval'])){
							$count = preg_match_all("/".$colattr[$key]['checkval']."/i", $rec[$colattr[$key]['checkfield']], $matches);
						}
						if($count > 0) $indent = str_repeat($colattr[$key]['padstr'], $count);
						print "<div style=\"$width\" class=\"listrow-cell\">".$indent.$kval."</div>".PHP_EOL;

	                }elseif(isset($colattr[$key]['date'])) {
	                    // format date
	                    print "<div style=\"$width\" class=\"listrow-cell\">";
	                    if(empty($kval)){
	                        print "--</div>";
	                    } else {
	                        if(!empty($colattr[$key]['format']))
	                            print date($colattr[$key]['format'], strtotime($kval))."</div>";
	                        else
	                            print $kval."</div>";
	                    }
	                }elseif(isset($colattr[$key]['format'])){
	                	// string format
	                    print "<div style=\"$width\" class=\"listrow-cell\">";
						if(!empty($colattr[$key]['format']))
                            print sprintf($colattr[$key]['format'], strtotime($kval))."</div>";
                        else
                            print $kval."</div>";
					}elseif($key != "actions") {
						// normal content
						($size[1] != "" && strlen($kval) > intval($size[1])) ? $cell_text = substr($kval, 0, intval($size[1]))."..." : $cell_text = $kval;
						if(!empty($colattr[$key]['xtra'])) {
							// quick edit link
							// (attr:quickedit)
							$cell_text = "<a href=\"#\" class=\"action_".$colattr[$key]['xtra']."\" title=\"Click to Edit\" rel=\"{$row_id}\">".$cell_text."</a>";
						}
						print "<div style=\"$width\" class=\"listrow-cell\">$cell_text</div>".PHP_EOL;

					} else {
						// row actions
						$altparams = $pagedata->altparams;
						$altgroups = $pagedata->altgroups;
						$addqueries = $pagedata->addqueries;
						$action = "";
						$buttontag = "";

						if(substr(getIfSet($cellstyle), 0, 2) == "; ") $cellstyle = substr($cellstyle, 2);

						// get the button array appropriate for this recset
						$button_array = $this->getListButtonArray($rec, $buttons, $buttoncondindex);

						print "<div class=\"list-actions\">";
						foreach($button_array as $butlabel) {
							list($label, $buttontag, $item) = $this->getListActionLabel($butlabel);
							$stub = "";
							$labeltask = preg_replace('/^(de(?!l)|un)/', '', $label);
							$rel = " rel=\"{$row_id}\" id=\"action_{$labeltask}_{$row_id}\"";
							$rel.= ((!empty($altgroups[$label])) ? " altgroup=\"{$altgroups[$label]}\"" : "");
							$rel.= ((!empty($altparams[$label])) ? " altparam=\"{$altparams[$label]}\"" : "");
							$rel.= ((!empty($addqueries[$label])) ? " addquery=\"{$addqueries[$label]}\"" : "");
							$tag = ((!empty($buttontag)) ? " tag=\"$buttontag\"" : "");

							$is_deleted = (getIntValIfSet($rec['deleted']) > 0);
							$is_published = (getIntValIfSet($rec['published']) > 0);
							$is_locked = (getIntValIfSet($rec['locked']) > 0);
							$is_protected = (getIntValIfSet($rec['protected']) > 0);
							$is_subscribed = (getIntValIfSet($rec['subscribed']) > 0);
							$is_replied = (getIntValIfSet($rec['replied']) > 0);
							$is_sent = (getIntValIfSet($rec['sent']) > 0);
							$is_gallerydef = (getIntValIfSet($rec['gallery_def']) > 0);
	                        $is_homepage = getBooleanIfSet($rec['homepage']);
	                        $alias = (($is_homepage) ? "" : getIfSet($rec['alias']));
	                        $pagename = getIfSet($rec['pagename']);
							switch ($label) {
								case DEF_ACTION_ADD:
									$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">Add".(($item == "") ? "" : $item)."</span></a>";
									break;
								case DEF_ACTION_EDIT:
								case DEF_ACTION_EDITFORM:
									if(!$is_deleted) $stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">Edit".(($item == "") ? "" : $item)."</span></a>";
									break;
								case DEF_ACTION_DELETE:
									if(!$is_deleted && !$is_locked && !$is_protected && ALLOW_DELETE) $stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"red\">Delete".(($item == "") ? "" : $item)."</span></a>";
									break;
								case DEF_ACTION_UNDELETE:
									if($is_deleted && !$is_locked && ALLOW_DELETE) $stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">Un-Delete".(($item == "") ? "" : $item)."</span></a>";
									break;
								case DEF_ACTION_PUBLISH:
									if(!$is_published && !$is_deleted && ALLOW_PUBLISH) $stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue bold\">Publish".(($item == "") ? "" : $item)."</span></a>";
									break;
								case DEF_ACTION_UNPUBLISH:
									if($is_published && !$is_deleted && ALLOW_PUBLISH) $stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue bold\">Un-Publish".(($item == "") ? "" : $item)."</span></a>";
									break;
								case DEF_ACTION_SUBSCRIBE:
									if(!$is_subscribed && !$is_deleted) $stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue bold\">Subscribe".(($item == "") ? "" : $item)."</span></a>";
									break;
								case DEF_ACTION_UNSUBSCRIBE:
									if($is_subscribed && !$is_deleted) $stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue bold\">Un-Subscribe".(($item == "") ? "" : $item)."</span></a>";
									break;
								case DEF_ACTION_VIEW:
	                                if(empty($pagename))
	                                    $stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\" rel=\"\">View";
	                                else
									    $stub = "<a href=\"".WEB_URL.$alias."\" target=\"_blank\" class=\"action_pre$label\"{$rel}{$tag}><span class=\"blue\" rel=\"\">Preview";
									$stub .= (!empty($butlabelstr[1])) ? $butlabelstr[1] : (($item == "") ? "" : $item);
									$stub.= "</span></a>";
									break;
								case DEF_ACTION_VIEWRECS:
									$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">View";
									$stub .= (!empty($butlabelstr[1])) ? $butlabelstr[1] : (($item == "") ? "List" : $item);
									$stub.= "</span></a>";
									break;
								case DEF_ACTION_VIEWPAGES:
									$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">View";
									$stub .= (!empty($butlabelstr[1])) ? $butlabelstr[1] : (($item == "") ? "Pages" : $item);
									$stub.= "</span></a>";
									break;
								case DEF_ACTION_OPEN:
									$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">Switch To".(($item == "") ? "" : $item)."</span></a>";
									break;
								case DEF_ACTION_REPLY:
									if(!$is_replied) {
										$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">".(($item == "") ? "View/Reply" : $item)."</span></a>";
									} else {
										$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">".(($item == "") ? "View" : $item)."</span></a>";
									}
									break;
								case DEF_ACTION_SEND:
									if(!$is_deleted && $is_published) {
										if (!$is_sent) {
											$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"bold blue\">".(($item == "") ? "Send Now" : $item)."</span></a>";
										} else {
											$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"bold blue\">".(($item == "") ? "Sent" : $item)."</span></a>";
										}
									}
									break;
								case DEF_ACTION_CLONE:
									$tag = " tag=\"".str_replace(array("&#34;","&#39;"), array("", ""), $rec[$pagedata->titlefld])."\"";
									$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">Clone</span></a>";
									break;
								case DEF_ACTION_EXPORT:
									$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">Export".(($item == "") ? "" : $item)."</span></a>";
									break;
								case DEF_ACTION_DEFAULT:
									if($is_published && !$is_deleted) {
										if(!$is_gallerydef) {
											$stub = "<a href=\"#\" class=\"action_$label\"{$rel}{$tag}><span class=\"blue\">Set as ".(($item == "") ? " Default" : $item)."</span></a>";
										} else {
											$stub = "<b>Is ".(($item == "") ? "Default" : $item)."</b>";
										}
									}
									break;
							}
							if($action != "" && $stub != "") $stub = " | ".$stub;
							$action .= $stub;
						}
						print "$action</div>".PHP_EOL;
					}
				}
				print "</div>".PHP_EOL;
				$rowcolor = (($rowcolor == LIST_ROWCOLOR1) ? LIST_ROWCOLOR2 : LIST_ROWCOLOR1);
			}
		}

		// show totals (optional)
		if(count($totalcols) > 0){
			$colnum = 0;
			print "<div id=\"listrow_totals\" class=\"listrow\">".PHP_EOL;
			foreach ($cols as $key => $value) {
				$size = explode("|", $colsize[$key]);
				$width = (($size[0] != "") ? "width: ".$size[0] : "");
				$width = preg_replace("/;(.*);/i", "", $width);
				print "<div style=\"$width\" class=\"listrow-cell\">";
				if($colnum == 0){
					print "Total:";
				}elseif($coltotal[$key] != 0){
					print number_format($coltotal[$key], 2);
				}
				print "</div>".PHP_EOL;
				$colnum++;
			}
			print "</div>".PHP_EOL;
		}

		return $hoverdivs;
	}

	function getListAdvancedActionDialog($row_ids, $pagedata, $x_data){
		global $_events, $_db_control, $_fields, $_users;

		$page_subject = getIfSet($pagedata['page_subject']);
		$page_ingroup = getIfSet($pagedata['page_ingroup']);
		$page_url = urldecode(getIfSet($pagedata['page_url']));
		$base_page_url = preg_replace("/((\?|\&).*)/i", "", $page_url);
		$page_flds = json_decode(getIfSet($param['page_flds']));

		$html = null;
        list($retn, $continue) = $_events->executeTrigger(__FUNCTION__, array($row_ids, $pagedata, $base_page_url), false);

        $html = '<h2>Advanced Bulk Actions</h2>';
        $html.= '<p>Select the data you want to modify and make your changes.</p>';
        $html.= '<p><label class="col-lg-2">Acting on:</label><span class="col-lg-10">'.count($row_ids).' '.$page_subject.' record(s)</span></p>';
    	$html.= '<input type="hidden" name="advaction_rowids" id="advaction_rowids" value="'.join(",", $row_ids).'" />';

        $arry = array(
			"visibility" => "Visibility",
			// "viewable_from" => "Viewable From",
			// "viewable_to" => "Viewable To",
			"parent" => "Parent",
			"user_id" => "Author",
			"searchable" => "Searchable?",
			"locked" => "Locked?",
			"protected" => "Protected?",
        	"metatitle" => "Meta Title",
			"metadescr" => "Meta Description",
			"metakeywords" => "Meta Keywords",
			"language" => "Language",
			"sitemap_state" => "Sitemap"
        );
        $recs = $_db_control->setTable($page_ingroup)->setWhere('id IN ('.join(",", $row_ids).')')->getData();

        ob_start();
        foreach($arry as $col => $label){
        	echo '<div class="col-lg-6 top-padding advaction_row">';
        	$label = '<input type="checkbox" class="advaction_check">'.$label;
        	switch($col){
        		case 'visibility':
        			$value = null;
        			$num_published = count(getArrayElements($recs, 'published', '1'));
        			$num_draft = count(getArrayElements($recs, 'draft', '1'));
        			if($num_published > 0 && $num_draft == 0) $value = "published";
        			if($num_published == 0 && $num_draft > 0) $value = "draft";
        			$atts = array("label" => $label, "id" => "advaction_".$col, "valuearray" => array("" => "- Not selected -", "published" => "Published", "draft" => "Draft"), "selectedvalue" => $value, "labelclass" => "col-lg-4 smallfont", "divclass" => "col-lg-8 smallfont", "fldclass" => "advaction_data");
        			$_fields->showMenu($atts);
        			break;
        		case 'parent':
        			break;
        		case 'viewable_from':
        		case 'viewable_to':
        			break;
        		case 'user_id':
        			$users = $_users->activelist;
        			$valuearray = array(0 => '--');
        			foreach($users as $user){
        				$valuearray[$user['id']] = $user['username'];
        			}
        			$atts = array("label" => $label, "id" => "advaction_".$col, "valuearray" => $valuearray, "selectedvalue" => $value, "labelclass" => "col-lg-4 smallfont", "divclass" => "col-lg-8", "fldclass" => "advaction_data");
        			$_fields->showMenu($atts);
        			break;
        		case 'searchable':
        		case 'locked':
        			$num_values = count(getArrayElements($recs, $col));
        			$chkstate = ($num_values == 1) ? $recs[0][$col] : 0;
        			$atts = array("label" => $label, "id" => "advaction_".$col, "value" => "1", "chkstate" => $chkstate, "labelclass" => "col-lg-4 smallfont", "divclass" => "col-lg-8 smallfont", "fldclass" => "advaction_data", "text" => "Yes");
        			$_fields->showCheckbox($atts);
        			break;
        		case 'protected':
        			$num_values = count(getArrayElements($recs, $col));
        			$chkstate = ($num_values == 1) ? $recs[0][$col] : 0;
        			$atts = array("label" => $label, "id" => "advaction_".$col, "value" => "1", "chkstate" => $chkstate, "labelclass" => "col-lg-4 smallfont", "divclass" => "col-lg-8 smallfont", "spanclass" => "col-lg-3", "fldclass" => "advaction_data", "text" => "Yes", "displaytype" => FLD_OPENROW);
        			$_fields->showCheckbox($atts);
        			$atts = array("before" => "Password", "id" => "advaction_password", "value" => "", "wrappertext" => array("before" => "<div class=\"col-lg-9\">Password:&nbsp;", "after" => "</div></div>"), "noac" => true, "fldclass" => "advaction_data", "displaytype" => FLD_DATA);
        			$_fields->showPasswordField($atts);
        			break;
        		case 'metatitle':
        		case 'metakeywords':
        			$num_values = count(getArrayElements($recs, null, true));
        			$value = ($num_values == 1) ? $recs[0][$col] : null;
        			$atts = array("label" => $label, "id" => "advaction_".$col, "value" => $value, "labelclass" => "col-lg-4 smallfont", "divclass" => "col-lg-8 smallfont", "fldclass" => "col-lg-12 advaction_data");
        			$_fields->showTextField($atts);
        			break;
        		case 'metadescr':
        			$num_values = count(getArrayElements($recs, null, true));
        			$value = ($num_values == 1) ? $recs[0][$col] : null;
        			$atts = array("label" => $label, "id" => "advaction_".$col, "value" => $value, "rows" => 3, "labelclass" => "col-lg-4 smallfont", "divclass" => "col-lg-8 smallfont", "fldclass" => "col-lg-12 advaction_data");
        			$_fields->showTextAreaField($atts);
        			break;
        		case 'language':
        			break;
        		case 'sitemap_state':
        			break;
        	}
        	echo '</div>'.PHP_EOL;
        }

        echo '<div class="alignright">';
        $atts = array("id" => "advaction_submit", "value" => "Save Changes", "displaytype" => FLD_DATA);
        $_fields->showButtonField($atts);
        echo '</div>';
        $html .= ob_get_contents();
        ob_end_clean();
		return $html;
	}

	/**
	 * Output search/sort block
	 * @param bool $allow_sort_override 	True to include sorting interface
	 */
	function showSearch($allow_sort_override = null) {
	    global $searchcols, $sortcols, $_page, $_events;

	    $search_text = $_page->search_text;
	    $search_by = $_page->search_by;
	    $sort_by = $_page->sort_by;
	    $sort_dir= $_page->sort_dir;
	    if(is_null($allow_sort_override)) $allow_sort_override = ALLOW_SORT;

	    if(!ALLOW_SEARCH) return;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes

		if(is_array($searchcols) && is_array($sortcols)) {
			$searchcols = array("" => "- No filter -", "all" => "All") + $searchcols;
			$sortcols = array("" => "- Not Sorted -") + $sortcols;

	        // print "<div id=\"search_header\"><div id=\"search_content_toggle\" class=\"toggleclose\">&nbsp;</div></div>".PHP_EOL;
			print " <div id=\"search_content\">".PHP_EOL;
			print "     <div class=\"search_leftdiv\">Search for:</div>";
			print "     <div class=\"search_rightdiv\">";
			print "         <input type=\"text\" id=\"search_text\" name=\"search_text\" value=\"$search_text\" size=\"70\" /> in ";
			print "         <select id=\"search_by\" name=\"search_by\" size=\"1\">";
			foreach($searchcols as $key => $value) {
				if($search_by == $key) {
					print "<option value=\"$key\" selected=\"selected\">$value</option>";
				} else {
					print "<option value=\"$key\">$value</option>";
				}
			}
			print "         </select>";
			print "     </div>".PHP_EOL;

			if (ALLOW_SORT && $allow_sort_override) {
	    		print "<div class=\"search_leftdiv\">Sort by:</div>";
	    		print "<div class=\"search_rightdiv\">";
	    		print "<select id=\"sort_by\" name=\"sort_by\" size=\"1\">";
	    		foreach($sortcols as $key => $value) {
	    			if($sort_by == $key) {
	    				print "<option value=\"$key\" selected=\"selected\">$value</option>";
	    			} else {
	    				print "<option value=\"$key\">$value</option>";
	    			}
	    		}
	    		print "</select>".PHP_EOL;
	    		print "<select id=\"sort_dir\" name=\"sort_dir\" size=\"1\">";
	    		$sortdircols = array("asc" => "Ascending", "desc" => "Descending");
	    		foreach($sortdircols as $key => $value) {
	    			if($sort_dir == $key) {
	    				print "<option value=\"$key\" selected=\"selected\">$value</option>";
	    			} else {
	    				print "<option value=\"$key\">$value</option>";
	    			}
	    		}
	    		print "</select>";
	    		print "</div>".PHP_EOL;
				print "<div id=\"search_buttons\">";
				print "<input id=\"page_search\" name=\"page_search\" type=\"submit\" value=\"Search/Sort Now\" />".PHP_EOL;
			} else {
				print "<div id=\"search_buttons\">";
				print "<input id=\"page_search\" name=\"page_search\" type=\"submit\" value=\"Search Now\" />".PHP_EOL;
			}

			print "</div>".PHP_EOL;
			print "</div>".PHP_EOL;
		}
	}

	/**
	 * Output pagination block
	 * @param integer $total
	 * @param boolean $showrecbuttons [optional]
	 * @param array $extrabuttons [optional] (array("id", "value", "JS"[, "alt"][, "title"][, "class"]))
	 * @param string $addnewbutname [optional]
	 * @param string $addnewbutparams [optional]
	 * @param string $appendquery [option]
	 */
	function showPagination($total, $recbuttons = 0, $extrabuttons = "", $addnewbutparams = "", $appendquery = ""){
	    global $_page;

	    $total_count = $total;
	    $num_of_pages = 0;
	    $display_pages = "";

	    if ($total_count > LIST_ROWLIMIT){
	        $num_of_pages = ceil($total_count/LIST_ROWLIMIT);

	        $prev = "";
	        $next = "";
	        $count = 0;

	        if(isset($_REQUEST['start']) && $_REQUEST['start'] != "")
	        	$start = $_REQUEST['start'];
	        else
	        	$start = 1;

	        //populate page numbers for easy page surfing
	        if(LIST_PAGESSHOWN > 0){
	            for($i = $start; $i <= $num_of_pages; $i++){
	            	//bold the current page
	            	if($i == $_page->pagenum){
	            		$page_uri = "<span class='page_current'>$i</span>";
	            		$display_pages .= $page_uri." ";
	            	} else {
	            		$page_uri = "<a href=\"?";
						if($appendquery != "") $page_uri .= $appendquery."&";
						$page_uri .= "start=$start&page=$i\">$i</a>";
	            		$display_pages .= $page_uri." ";
	            	}

	            	$count ++;
	            	if ($count >= LIST_PAGESSHOWN) $i = $num_of_pages + 1;  //exit the 'for' loop
	            }
	        }

	        //generate previous button
	        if ($_page->pagenum > 1){
	            if ($start > ($_page->pagenum - 1)){
	            	$new_start = $_page->pagenum - LIST_PAGESSHOWN;
	            } else {
	            	$new_start = $start;
	            }
				$prev = "<input type='button' class='list_button_page_prev' value='<' rel='".($_page->pagenum - 1)."' />";
	            $display_pages = $prev."&nbsp;&nbsp;".$display_pages;
	        }

	        //generate next button
	        if ($_page->pagenum != $num_of_pages){
	            if ((($_page->pagenum + 1) - $start) == LIST_PAGESSHOWN){
	            	$new_start = $_page->pagenum +1 ;
	            } else {
	            	$new_start = $start;
	            }
				$next = "<input type='button' class='list_button_page_next' value='>' rel='".($_page->pagenum + 1)."' />";
	            $display_pages = $display_pages."&nbsp;&nbsp;".$next;
	        }
	    }

		if($total_count == 0) $_page->offset = -1;

		$lastrow = $_page->offset + LIST_ROWLIMIT;
		if($lastrow > $total_count) $lastrow = $total_count;
	    print "<!-- PAGINATION -->".PHP_EOL;
	    print "<div class=\"paging_content\">".PHP_EOL;
	    print "<div class=\"paging_leftdiv\">";
	    print $display_pages;
		print "</div>".PHP_EOL;
		print "<div class=\"paging_rightdiv\">";
	    print "Displaying ".($_page->offset + 1)." to ".($lastrow)." of $total_count Item".(($total_count != 1) ? "s" : "");
		print "</div>".PHP_EOL;

		$this->showPaginationAreaButtons($recbuttons, $extrabuttons, $addnewbutparams);

	    print "</div>".PHP_EOL;
	    print "<!-- /PAGINATION -->".PHP_EOL;
	    return $display_pages;
	}

	/**
	 * Output pagination buttons such as "add new", "go back" or "organize"
	 * @param integer $recbuttons
	 * @param array $extrabuttons
	 * @param string $addnewbutparams
	 */
	function showPaginationAreaButtons($recbuttons = 0, $extrabuttons = "", $addnewbutparams = ""){
	    global $_page;

		// display add record button here
	    if($recbuttons > 0 || is_array($extrabuttons)) {
	        print "<!-- PAGINATION BUTTONS -->".PHP_EOL;
	        print "<div id=\"paging_recbuttons\">".PHP_EOL;
	        if(($recbuttons & DEF_PAGEBUT_ADDNEW) > 0){
	        	if(empty($addnewbutparams)) $addnewbutparams = $_page->cat_id;
	        	print "<input type=\"button\" name=\"addbutton\" id=\"addbutton\" value=\"Add New ".ucwordsAdv($_page->subject)."\" class=\"action_add\" rel=\"$addnewbutparams\" />".PHP_EOL;
	        }
	        if(($recbuttons & DEF_PAGEBUT_GOBACK) > 0){
	        	print "<input type=\"button\" name=\"gobackbutton\" id=\"gobackbutton\" value=\"Go Back to Parent\" class=\"action_goback\" />".PHP_EOL;
	        }
	        if(($recbuttons & DEF_PAGEBUT_ORGANIZER) > 0){
	        	print "<input type=\"button\" name=\"organizebutton\" id=\"organizebutton\" value=\"Organize\" class=\"action_organize\" />".PHP_EOL;
	        }
	        if(($recbuttons & DEF_PAGEBUT_EXPORT) > 0){
	        	print "<input type=\"button\" name=\"exportbutton\" id=\"exportbutton\" value=\"Export\" class=\"action_export\" />".PHP_EOL;
	        }

	        if(is_array($extrabuttons)) {
	            foreach($extrabuttons as $key => $value) {
	                /* 0 = id/name (req)
	                   1 = value (req)
	                   2 = javascript function (req)
	                   3 = alt tag
	                   4 = title tag
	                   5 = class
	                */
	                if($value[0] != "" && $value[1] != ""){
	                    print "&nbsp;&nbsp;<input type=\"button\" name=\"".$value[0]."\" id=\"".$value[0]."\" value=\"".$value[1]."\"";
	                    if($value[2] != "") print " onclick=\"javascript: ".$value[2]."\"";
	                    if($value[3] != "") print " alt=\"".$value[3]."\"";
	                    if($value[4] != "") print " title=\"".$value[4]."\"";
	                    if($value[5] != "") print " class=\"".$value[5]."\"";
	                    print " />".PHP_EOL;
	                }
	            }
	        }
	        print "</div>";
	        print "<!-- /PAGINATION BUTTONS -->".PHP_EOL;
	    }
	}

	/**
	 * Output editor action buttons
	 * @param integer $butset [optional]
	 * @param string $backquery [optional]
	 */
	function showEditorButtons($butset = 0) {
		global $_db_control, $infodialogtype, $_page, $_data;

		$the_page = str_replace("data_", "", $_db_control->table);
		$pad = "&nbsp;";
		$datatype = $_page->datatype;

	    // save button
		if (($butset & DEF_EDITBUT_SAVE) > 0) echo '<input type="button" name="'.DEF_POST_ACTION_SAVE.'" value="Save" class="editor_button_save splitbutton bold" />';
	    // save & add new button
		if (($butset & DEF_EDITBUT_SAVEADD) > 0) echo '<input type="button" name="'.DEF_POST_ACTION_SAVEADD.'" value="Save & Add" class="editor_button_save splitbutton bold" />';
	    // update button
		if (($butset & DEF_EDITBUT_UPDATE) > 0) echo $pad.'<input type="button" name="'.DEF_POST_ACTION_SAVE.'" value="Update" class="editor_button_save splitbutton bold" />';
	    // save settings button (old form)
	    if (($butset & DEF_SETBUT_SAVE) > 0) echo '<input type="submit" name="cfgsubmit" value="Save Settings" class="editor_button_save bold" />';

	    if (($butset & DEF_EDITBUT_UPDATE) > 0 || ($butset & DEF_EDITBUT_SAVE) > 0 || ($butset & DEF_EDITBUT_SAVEADD) > 0){
	        echo '<a href="#" class="editor_button_more fa fa-arrow-down fa-fw" title="View publication options"></a>';
	    }
		// save & reply button
	    if (($butset & DEF_EDITBUT_REPLY) > 0) echo $pad.'<input type="button" name="'.DEF_POST_ACTION_SAVEREPLY.'" value="Reply" class="editor_button_save bold" />';
		// delete button
	    if (ALLOW_DELETE && ($butset & DEF_EDITBUT_DELETE) > 0) echo $pad.'<input type="button" name="'.DEF_POST_ACTION_DELETE.'" value="Delete" class="editor_button_delete redbutton bold" />';
		// preview button
	    if (($butset & DEF_EDITBUT_PREVIEW) > 0) echo $pad.'<input type="button" class="editor_button_preview" value="Preview" />';
		// add new button
	    if (($butset & DEF_EDITBUT_ADD_NEW) > 0) echo $pad.'<input type="button" class="editor_button_add_new" value="Add New" rel="'.WEB_URL.$_data->getAdminDataAlias($datatype, "add").'" />';
		// status button
	    if (($butset & DEF_EDITBUT_STATS) > 0) echo $pad.'<input type="button" class="editor_button_status" value="View Stats" />';
		// info button
	    if (($butset & DEF_EDITBUT_INFO) > 0) {
			echo $pad.'<input type="button" class="editor_button_info" value="View Info" />';
			$infodialogtype = DEF_EDITBUT_INFO;
		}
	    if (($butset & DEF_EDITBUT_LASTINFO) > 0) {
			echo $pad.'<input type="button" class="editor_button_info" value="Last Added" />';
			$infodialogtype = DEF_EDITBUT_LASTINFO;
		}
		// go back button
	    if (($butset & DEF_EDITBUT_BACK) > 0) echo $pad.'<br/><br/><input type="button" class="editor_button_goback" value="Go Back" rel="'.$the_page.'" />';
	}

	/**
	 * Output previous page buttons
	 * @param array $buttonlist
	 */
	function showPrevPageButtons($buttonlist){
		if(is_array($buttonlist)){
			$count = 0;
			foreach($buttonlist as $key => $button){
				$count++;
				print "<input type=\"button\" rel=\"".WEB_URL.$button."\" class=\"editor_button_prev_page\" value=\"&lt; ".$key."\" />".PHP_EOL;
			}
		}
	}

	/**
	 * Output status box/block
	 * @param string $extraclass [optional]
	 */
	function showStats($extraclass = "") {
		global $date_created, $date_published, $date_subscribed, $date_updated;
		global $draft, $published, $subscribed, $deleted, $active;

		if ($extraclass != "") $extraclass = " class=\"$extraclass\"";
		print<<<EOT
		<div id="stats_content"{$extraclass}>
EOT;

		if (isDate($date_created)) {
	        $date = date("M j, Y g:i a", strtotime($date_created));
		print<<<EOT
		<div class="label">Created:</div>
		<div class="item">{$date}</div>
EOT;
		}
		if (isDate($date_updated)) {
	        $date = date("M j, Y g:i a", strtotime($date_updated));
		print<<<EOT
		<div class="label">Last Updated:</div>
		<div class="item">{$date}</div>
EOT;
		}
		if ($published == 1 && isDate($date_published)) {
	        $date = date("M j, Y g:i a", strtotime($date_published));
		print<<<EOT
		<div class="label">Date Published:</div>
		<div class="item">{$date}</div>
EOT;
		}
		if ($subscribed == 1 && isDate($date_subscribed)) {
	        $date = date("M j, Y g:i a", strtotime($date_subscribed));
		print<<<EOT
		<div class="label">Date Subscribed:</div>
		<div class="item">{$date}</div>
EOT;
		}
		print<<<EOT
		<div class="label">Current Status:</div>
		<div class="item">
EOT;
	    if(isset($published) && $published == 1){
			print "<span class=\"greenbutton bold\">Published</span> (visible to public)";
		}elseif(isset($deleted) && $deleted == 1){
			print "<span class=\"redbutton bold\">Deleted</span>";
		}elseif(isset($published) || isset($draft)){
			print "<span class=\"graybutton bold\">Updated</span> (not visible to public)";
		}elseif(isset($published) && $published == 0){
			print "<span class=\"redbutton bold\">Draft</span> (not visible to public)";
		}elseif(isset($subscribed) && $subscribed == 1){
			print "<span class=\"greenbutton bold\">Subscribed</span>";
		}elseif(isset($subscribed) && $subscribed == 0){
			print "<span class=\"redbutton bold\">NOT Subscribed</span>";
		}elseif(isset($active) && $active == 1){
			print "<span class=\"greenbutton bold\">Active</span>";
		}elseif(isset($active) && $active == 0){
			print "<span class=\"redbutton bold\">Not Active</span>";
		} else {
			print "Updated";
		}
		print<<<EOT
		</div>
		</div>
EOT;
	}

	/**
	 * Show update/save attribute fields (visibility, date published)
	 */
	function showUpdateOptions(){
	    global $_page;

	    $attr = $_page->attributes;
	    $published = getBooleanIfSet($attr['published']);
	    $draft = getBooleanIfSet($attr['draft']);
	    $deleted = getBooleanIfSet($attr['deleted']);
	    $date_created = $attr['date_created'];
	    $date_published = $attr['date_published'];
	    $date_updated = $attr['date_updated'];

	    echo '<div id="update_options">'.PHP_EOL;
	    $nostat = ((!$published && !$draft) ? ' selected="selected"' : '');
	    $pub = (($published) ? ' selected="selected"' : '');
	    $drf = (($draft) ? ' selected="selected"' : '');
	    $act = (($published) ? ' checked="checked"' : '');

	    if(!empty($attr['published'])){
		    if(isDate($date_published)){
		        $dmy = date(PHP_DATE_FORMAT, strtotime($date_published));
		        $hms = date(PHP_TIME_FORMAT, strtotime($date_published));
		        $date = date("F j, Y h:i:s", strtotime($date_published));
		    } else {
		        $dmy = $hms = null;
		        $date = "- Not published -";
		    }

		    preg_match("#(\-|\/)#", PHP_DATE_FORMAT, $sep);
		    $iso = convertPHPtoISODate(PHP_DATE_FORMAT, getIfSet($sep[0]));

		    print<<<EOT
	    <div class="label">Visibility Status:</div>
	    <div class="item">
	        <select name="content_status" id="content_status">
	            <option value=""{$nostat}>- Not set -</option>
	            <option value="published"{$pub}>Published</option>
	            <option value="draft"{$drf}>Draft</option>
	        </select>
	    </div>
	    <div class="label">Date Published:</div>
	    <div class="item">
	        <span class="date_published">{$date}</span>
	    </div>
	    <div class="label">Scheduled:</div>
	    <div class="item">
	        <i class="fa fa-calendar"></i>&nbsp;<input type="text" name="date_published_scheduled" id="date_published_scheduled" class="datepicker" title="Date" value="{$dmy}" rel="{$iso}" />
	        <i class="fa fa-clock-o"></i>&nbsp;<input type="text" name="date_published_scheduled_time" id="date_published_scheduled_time" title="Time" value="{$hms}" />
	        <span class="edithelp">yyyy-mm-dd  hh:mm:ss</span>
	    </div>
	    </div>
EOT;
		} else {
		    print<<<EOT
	    <div class="label">Visibility Status:</div>
	    <div class="item">
	        <input type="checkbox" name="content_status" id="content_status" value="published"{$act}> Active
	    </div>
	    </div>
EOT;
		}
	}

	/**
	 * Output info box/block
	 * @param string $itemval
	 * @param string $extraclass [optional]
	 */
	function showInfo($extraclass = "") {
	    global $_error;

		$row_id = $GLOBALS['lastitemid'];
		$itemval = $GLOBALS['lastitemval'];
		if($extraclass != "") $extraclass = " class=\"$extraclass\"";
		print<<<EOT
		<div title="Information"{$extraclass}>
EOT;
		if($GLOBALS['infodialogtype'] == DEF_EDITBUT_LASTINFO){
			if($_error->getErrorStatMsg(SUCCESS_CREATE)){
				if(intval($row_id)>0){
				print<<<EOT
		<div class="label">Last ID:</div>
		<div class="item">{$row_id}</div>
EOT;
				}
				if($itemval != ''){
				print<<<EOT
		<div class="label">Last Item:</div>
		<div class="item">'{$itemval}'</div>
EOT;
				}
			} else {
				print<<<EOT
		<div class="label">Last Item:</div>
		<div class="item">Not available</div>
EOT;
			}
		} else {
			if($itemval != ''){
			print<<<EOT
		<div class="label">Item:</div>
		<div class="item">'{$itemval}'</div>
EOT;
			}
		}
		print<<<EOT
		</div>
EOT;
	}

	/**
	 * Output send email block
	 * @param string $table
	 * @param string $msgtype
	 * @param string $msgfld
	 * @param string $crit
	 * @param string $choosesubs
	 */
	function showSendPanel($table, $msgtype, $msgfld, $crit="", $choosesubs=false){
	    global $_db_control;

		if($table != "" && $msgtype != ""){
	        $msgrec = $_db_control->setTable($table)->setWhere($crit)->setOrder($msgfld)->getRec();
			print "<input type=\"hidden\" name=\"sendmsg_subj\" id=\"sendmsg_subj\" value=\"\" />".PHP_EOL;
			print "<input type=\"hidden\" name=\"sendmsg_content\" id=\"sendmsg_content\" value=\"\" />".PHP_EOL;
			print "<input type=\"hidden\" name=\"sendmsg_file\" id=\"sendmsg_file\" value=\"\" />".PHP_EOL;
			print "<input type=\"hidden\" name=\"sendmsg_items\" id=\"sendmsg_items\" value=\"\" />".PHP_EOL;
			print "<div class=\"dialogpanel sendpanel\" title=\"Send Message\" style=\"display: none;\">".PHP_EOL;
			print "<p>Note: Select the topic or newsletter that you want to send to your subscribers.  Click ";
			print "<input type=\"button\" id=\"sendbut\" name=\"sendbut\" value=\"Send Now\" onclick=\"javascript: sendmessage('$msgtype');\"/>".PHP_EOL;
			print ".</p>".PHP_EOL;
			print "<div class=\"sendmsg_label\">Subject: *</div>";
			print "<div class=\"sendmsg_text\"><input type=\"text\" name=\"send_subj\" id=\"send_subj\" size=\"57\" value=\"\" /></div>".PHP_EOL;
			print "<div class=\"sendmsg_label\">Message to Send: *</div>";
			print "<div class=\"sendmsg_text\"><textarea name=\"send_content\" id=\"send_content\" rows=\"5\" cols=\"55\"></textarea></div>".PHP_EOL;
			print "<div class=\"sendmsg_label\">File (optional):</div>";
			print "<div class=\"sendmsg_text\"><input type=\"file\" name=\"send_file\" id=\"send_file\" size=\"40\" value=\"\" /></div>".PHP_EOL;
			print "<div class=\"sendmsg_label\">".ucwords($msgtype)."s:</div>";
			print "<div class=\"sendmsg_text\">".PHP_EOL;
			foreach($msgrec as $msgitem){
				(isset($msgitem['date_lastsent']) && isDate($msgitem['date_lastsent'])) ? $lastsent = " (last sent: ".date("M j, Y", strtotime($msgitem['date_lastsent'])).")" : $lastsent = " (Never sent)";
				print "<input type=\"checkbox\" class=\"send_item\" name=\"send_item[]\" value=\"".$msgitem['id']."\" />&nbsp;".$msgitem[$msgfld].$lastsent."<br/>".PHP_EOL;
			}
			print "</div>".PHP_EOL;
			print "</div>".PHP_EOL;

			print<<<EOT
	<script type="text/javascript" language="Javascript">
		function sendmessage(msgtype){
			// store data in sendmsg fields (which is outside hidden panel)
			var num = 0;
			var rtn = "";
			for(var i = 0; i < senditems.length; i++){
			$('.send_item').each(function(){
				if ($(this).is(':checked')) {
					num = $(this).val();
					if (rtn != "")
						rtn += ",";
					rtn += num;
				}
			});
			// validate now
			$('#sendmsg_subj').val($('#send_subj').val());
			$('#sendmsg_content').val($('#send_content').val());
			$('#sendmsg_file').val($('#send_file').val());
			$('#sendmsg_items').val(rtn);
			if(!checkRequiredField('sendmsg_subj', 'Please enter the subject.'))
				return false;

			if(!checkRequiredField('sendmsg_content', 'Please enter the message content.'))
				return false;

			if(!checkRequiredField('sendmsg_items', 'Please choose at least one '+msgtype+' to include.'))
				return false;

			$('#cmd').val('send');
			//document.list_form.submit();
			return false;
		}
	</script>

EOT;
		}
	}

	function showCloseButton($top = false, $right = false, $bottom = false, $left = false){
		$position = null;
		if($top) $position .= ' tophang';
		if($right) $position .= ' righthang';
		if($bottom) $position .= ' bottomhang';
		if($left) $position .= ' lefthang';

		if(!is_null($position))
			print "<i class=\"fa fa-close closebutton button".$position."\"></i>";
	}

	/**
	 * Create CSV from recordset
	 * @param string $title
	 * @param string $table
	 * @param string $crit
	 * @param string $sort
	 * @param string $headings
	 * @param string $fields
	 * @param string $sums
	 */
	function exportData($title, $table, $crit, $sort, $headings, $fields, $sums = ""){
	    global $_db_control;

		if($table != "" && $headings != "" && $fields != ""){
			$heading_array = explode(",", $headings);
			$field_array   = explode(",", $fields);
			$sum_array     = explode(",", $sums);

			if(is_array($heading_array) && is_array($field_array)){
	            $datarec = $_db_control->setTable($table)->setFields($fields)->setWhere($crit)->setOrder($sort)->getRec();

				$out  = '"'.$title.'"';
				$out .= "\n\r";

				// heading row
				foreach ($heading_array as $key => $cell) $out .= '"'.trim($cell).'",';
				$out .= "\n\r";

				// content rows
				$sumval_array = array();
				foreach($datarec as $dkey => $item){
					$row = "";
					foreach($field_array as $fkey => $field){
						$fld = strtolower(trim($field));
						if(strpos($fld, ' as ') !== false) {
							$as_fld = split(' as ', $fld);
							$fld = $as_fld[1];
						}
						if(is_array($sum_array)){
							if(in_array($fld, $sum_array)) $sumval_array[$fld] += floatval($item[$fld]);
						}

						if($row != "") $row .= ",";
						$row .= '"'.$item[$fld].'"';
					}
					$out .= $row."".PHP_EOL;
				}

				$out .= "".PHP_EOL;

				// optional sum row
				if(is_array($sum_array) && $sum_array[0] != ''){
					$row = "";
					$out .= '"TOTALS --->",';
					foreach($field_array as $field){
						$fld = trim($field);
						if(in_array($fld, $sum_array)){
							if($row != "") $row .= ",";
							$row .= '"'.$sumval[$fld].'"';
						}
					}
					$out .= $row."\n\r";
				}

				// finalize file
				$out .= "--END OF FILE--";
				$out .= "\n\r";

				$file = SITE_PATH.FILE_UPLOAD_FOLDER."export.csv";
				if(file_exists($file)) unlink($file);

				// Open file export.csv.
				$f = fopen ($file, 'w');

				// Put all values from $out to export.csv.
				fwrite($f, $out);
				fclose($f);

				// Call the CSV Exporter file which will output the CSV content
				echo "<script language=\"Javascript\">window.open('".WEB_URL.ADMIN_FOLDER.PLUGINS_FOLDER."exporters/exportcsv.php?title={$title}');</script>".PHP_EOL;
			}
		}
	}
}

?>