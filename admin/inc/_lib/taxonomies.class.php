<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("TAXLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/**
 * TAXONOMIES CLASS
 *
 */
class TaxonomiesClass {
	// overloaded data
	private $_keys = array(	);
	private $_subkeys = array("taxonomies" => array());
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$_events->processTriggerEvent(__FUNCTION__.'');                // alert triggered functions when function executes
		return $s;
	}

	/**
	 * Prepare the taxonomies object
	 */
	public function initTaxonomies(){
		$t = $this->getTaxonomies(array(), "name");
		$this->_keys['taxonomies'] = $t;
	}

	/**
	 * Get a taxonomy object based on id or code
	 * @param string|integer $val 			The id or code of the taxonomy
	 * @return array|null
	 */
	public function getTaxonomy($val){
		global $_db_control;

		if(is_numeric($val)){
			$id = intval($val);
			if($id == 0) return null;

			$t = $_db_control->setTable(TAXONOMIES_TABLE)->setWhere("id = '".$id."'")->getRec(true);
		}elseif(is_string($val)){
			if(empty($val)) return null;

			$t = $_db_control->setTable(TAXONOMIES_TABLE)->setWhere("code = '".$val."'")->getRec(true);
		}else{
			$t = null;
		}
		return $t;
	}

	/**
	 * Get multiple taxonomy objects.  Taxonomy objects include related attributes/aliases
	 * @param array $args 					Arguments list
	 * 											parent_id => id of common taxonomy parent
	 * 											type => type of taxonomy (category, faq, tag or link)
	 * 											active => active state (boolean)
	 * @return array
	 */
	public function getTaxonomies($args = array(), $sort = "id"){
		global $_db_control;

		$crit = '1=1';
		if(isset($args['parent_id'])) $crit .= ' AND '.TAXONOMIES_TABLE.'.`parent_id` = "'.getIntValIfSet($args['parent_id']).'"';
		if(isset($args['type'])) $crit .= ' AND '.TAXONOMIES_TABLE.'.`type` = "'.getIfSet($args['type']).'"';
		if(isset($args['active'])) $crit .= ' AND '.TAXONOMIES_TABLE.'.`active` = "'.getBooleanIfSet($args['active']).'"';
		if(empty($sort)) $sort = TAXONOMIES_TABLE.".id";
		$t = $_db_control
			->setTable(TAXONOMIES_TABLE)
			->setFields(TAXONOMIES_TABLE.".*, ".ATTRIBUTES_TABLE.".data_type, ".ATTRIBUTES_TABLE.".alias")
			->setJoins(
				array(
					array("type" => "LEFT JOIN", "table" => ATTRIBUTES_TABLE, "comp" => TAXONOMIES_TABLE.".data_alias = ".ATTRIBUTES_TABLE.".attribute_id")
				))
			->setWhere($crit)
			->setOrder($sort)
			->getRecJoin();
		return $t;
	}

	/**
	 * Save or update a taxonomy object
	 * @param array $args 				The values to save in and arguments list
	 * 										id => id of taxonomy to update.  Exclude if saving a new taxonomy
	 *										name => the name of the taxonomy
	 * 										code => the code of the taxonomy.  If omitted, will be derived from name
	 * 										parent_id => the id of the taxonomy parent.  Default is 0
	 * 										type => the type of taxonomy (category, link, tag, or faq)
	 *										active => the active state (boolean). Default is true
	 * @return id
	 */
	public function saveTaxonomy($args = array()){
		global $_db_control, $_data;

		$args = array_merge(
			array("id" => 0, "name" => null, "code" => null, "parent_id" => 0, "data_alias" => 0, "type" => 'category', "active" => true),
			$args
		);

		if(empty($args["code"]) && $args['id'] == 0) $args["code"] = codify($args["name"]);
		if($args["parent_id"] < 0) $args["parent_id"] = 0;
		if($args["data_alias"] < 0) $args["data_alias"] = 0;
		if(!in_array($args['type'], array('category','link','faq','tag')) && !empty($args['type'])) $args['type'] == 'category';
		unset($args["alias"]);
		if($args["id"] > 0){
			if($args["id"] == $args["parent_id"]) $args["parent_id"] = 0;
			$id = $args["id"];
			unset($args["id"]);
			$_db_control->setTable(TAXONOMIES_TABLE)->setFieldvals($args)->setWhere("id = '".$id."'")->updateRec();
		}else{
			unset($args["id"]);
			$id = $_db_control->setTable(TAXONOMIES_TABLE)->setFieldvals($args)->insertRec();
		}
		$this->initTaxonomies();
		return $id;
	}

	/**
	 * Set the active state of a taxonomy
	 * @param integer $id 					The id of taxonomy
	 * @param boolean $state 				True or false
	 */
	public function changeTaxonomyState($id, $state = true){
		global $_db_control;

		$state = (boolean)$state;
		$_db_control->setTable(TAXONOMIES_TABLE)->setFieldvals(array("active" => $state))->setWhere("id = '".$id."'")->updateRec();
		$this->initTaxonomies();
	}

	/**
	 * Delete a taxonomy, related alias, and all related terms
	 * @param integer $id 					The id of taxonomy
	 */
	public function deleteTaxonomy($id){
		global $_db_control, $_data;

		if($id == 0) return false;
		$alias_id = $_db_control->setTable(TAXONOMIES_TABLE)->setFields("data_alias")->setWhere("id = '".$id."'")->getRecItem();
		if($alias_id > 0) $_data->deleteAlias($alias_id, false);
		$_db_control->setTable(TAXONOMIES_TABLE)->setWhere("id = '".$id."'")->deleteRec();

		$this->purgeTaxonomy($id);
		$this->initTaxonomies();
		return true;
	}

	/**
	 * Delete all related terms of a taxonomy
	 * @param integer $id 					The id of taxonomy
	 */
	public function purgeTaxonomy($id){
		global $_db_control, $_data;

		if($id == 0) return false;
		$terms = $_db_control->setTable(TERMS_TABLE)->setFields("id")->setWhere("taxonomy_id = '".$id."'")->getRec();
		if(!empty($terms)){
			foreach($terms as $term){
				$this->deleteTerm($term['id']);
			}
		}
	}

	/**
	 * Get multiple terms.  Terms include related attributes/aliases
	 * @param array $args 					Arguments list
	 * 											parent_id => id of common taxonomy parent
	 * 											taxonomy_id => id of taxonomy
	 * 											active => active state (boolean)
	 * @return array
	 */
	public function getTerms($args = array()){
		global $_db_control;

		$crit = '1=1';
		if(isset($args['parent_id'])) $crit .= ' AND '.TERMS_TABLE.'.`parent_id` = "'.getIntValIfSet($args['parent_id']).'"';
		if(isset($args['taxonomy_id'])) $crit .= ' AND '.TERMS_TABLE.'.`taxonomy_id` = "'.getIntValIfSet($args['taxonomy_id']).'"';
		if(isset($args['active'])) $crit .= ' AND '.TERMS_TABLE.'.`active` = "'.getBooleanIfSet($args['active']).'"';
		if(empty($sort)) $sort = TERMS_TABLE.".id";
		$t = $_db_control
			->setTable(TERMS_TABLE)
			->setFields(TERMS_TABLE.".*, ".ATTRIBUTES_TABLE.".alias")
			->setJoins(
				array(
					array("type" => "LEFT JOIN", "table" => ATTRIBUTES_TABLE, "comp" => TERMS_TABLE.".id = ".ATTRIBUTES_TABLE.".data_id AND `data_type` = '".TERMS_TABLE."' AND `attribute_class` = '".ATTR_CLASS_TAX_ALIAS."'")
				))
			->setWhere($crit)
			->setOrder($sort)
			->getRecJoin();
		return $t;
	}

	/**
	 * Save or update a term
	 * @param array $args 				The values to save in and arguments list
	 * 										id => id of term to update.  Exclude if saving a new term
	 *										name => the name of the term
	 * 										code => the code of the term.  If omitted, will be derived from name
	 * 										parent_id => the id of the term parent.  Default is 0
	 * 										taxonomy_id => the id of the taxonomy
	 *										active => the active state (boolean). Default is true
	 * @return id
	 */
	public function saveTerm($args = array()){
		global $_db_control, $_data;

		$args = array_merge(
			array("id" => 0, "name" => null, "code" => null, "parent_id" => 0, "taxonomy_id" => 0, "active" => true),
			$args
		);

		if(empty($args["code"]) && $args['id'] == 0) $args["code"] = codify($args["name"]);
		if($args["parent_id"] < 0) $args["parent_id"] = 0;
		if($args["taxonomy_id"] < 0) $args["taxonomy_id"] = 0;
		if($args["id"] > 0){
			if($args["id"] == $args["parent_id"]) $args["parent_id"] = 0;
			$id = $args["id"];
			unset($args["id"]);
			$_db_control->setTable(TERMS_TABLE)->setFieldvals($args)->setWhere("id = '".$id."'")->updateRec();
		}else{
			unset($args["id"]);
			$id = $_db_control->setTable(TERMS_TABLE)->setFieldvals($args)->insertRec();
		}
		return $id;
	}

	/**
	 * Delete a term
	 * @param integer $id 				Id of the term
	 */
	public function deleteTerm($id){
		global $_db_control, $_data;

		if($id == 0) return false;
		$_db_control->setTable(ATTRIBUTES_TABLE)->setWhere("attribute_class = '".ATTR_CLASS_TAX_ALIAS."' AND data_type = '".TERMS_TABLE."' AND data_id = '".$id."'")->deleteRec();
		$_db_control->setTable(TERMS_TABLE)->setWhere("id = '".$id."'")->deleteRec();
		return true;
	}

	public function &__get($name){
		$v = null;
		if(array_key_exists($name, $this->_keys)){
			// return scalar value
			$v = $this->_keys[$name];
		}elseif(array_key_exists($name, $this->_subkeys)){
			// return subarray value
			$v = $this->_subkeys[$name];
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot get '$name'.  It is not a valid _TAX property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
		return $v;
	}

	public function __set($name, $value){
		if(array_key_exists($name, $this->_keys)){
			// set scalar value
			$this->_keys[$name] = $value;
		}elseif(array_key_exists($name, $this->_subkeys)){
			// set subarray value
			$this->_subkeys[$name][] = $value;
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot set '$name'.  It is not a valid _TAX property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
	}
}

?>