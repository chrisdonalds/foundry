<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

/* COLLECTOR GLOBAL CLASS
 *
 * An extendable class that stores shared system properties and variables
 */
define ("COLLECTLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

class CollectorClass {
	// overloaded data
	public $_db_control = null;
	protected static $_instance = null;

	public function __construct($db_control) {
		$this->checkCaller("init");
		$this->_db_control = $db_control;
	}

	private function __clone(){
	}

	private function checkCaller($action){
		$stack = debug_backtrace();
		$callerfile = $stack[1]['file'];
		if(strpos($callerfile, "_core") === false){
			die("Calling Collector::$action in $callerfile not allowed!");
		}
	}
}

?>