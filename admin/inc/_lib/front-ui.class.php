<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("FRONTLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/**
 * PAGECLASS
 * Stores and manages front page structure data
 * - header
 * - footer
 * - sectionid
 * - scriptlines[]
 */
class PageClass {
	// overloaded data
	private $_page = array();
	private $_keys = array(	"header" => "", "footer" => "", "sectionid" => 0,
							"file" => "", "name" => "", "alias" => "", "urlpath" => "",
                            "id" => 0, "code" => "", "title" => "",
							"parenttitle" => "", "data" => array(), "datatype" => "", "nonce" => "",
							"metatitle" => "", "metakeywords" => "", "metadescr" => "",
							"found" => false, "error" => "", "formTemplate" => "",
                            "ishomepage" => false, "islocked" => false, "issearchable" => false,
							"isprotected" => false, "ispublished" => false, "isdraft" => false,
                            "created" => "", "updated" => "", "published" => "",
                            "dbrec" => array(), "query" => "", "queryvars" => "",
                            "target" => null, "targetaction" => null, "targettype" => null,
                            "plugins_incl" => array());
	private $_subkeys = array("scriptlines" => array(),
							"cacheablescriptlines" => array(),
							"combinedscriptlines" => array(),
							"embedscripts" => array(),
							"embedstyles" => array());
	protected static $_instance = null;

	private function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_events;

		$s = new self;
		$_events->processTriggerEvent(__FUNCTION__.'_front_ui');				// alert triggered functions when function executes
		return $s;
	}

	public function __get($name){
		$v = null;
		if(array_key_exists($name, $this->_keys)){
			// return scalar value
			$v = $this->_keys[$name];
		}elseif(array_key_exists($name, $this->_subkeys)){
			// return subarray value
			$v = $this->_subkeys[$name];
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot get '$name'.  It is not a valid _PAGE property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
		return $v;
	}

	public function __set($name, $value){
		if(array_key_exists($name, $this->_keys)){
			// set scalar value
			$this->_keys[$name] = $value;
		}elseif(array_key_exists($name, $this->_subkeys)){
			// set subarray value
			$this->_subkeys[$name][] = $value;
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot set '$name'.  It is not a valid _PAGE property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
	}
}

?>