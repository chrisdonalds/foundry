<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("PLUGINSLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

define ("PLUGIN_SETTINGS_SAVE", 1);
define ("PLUGIN_SETTINGS_CLOSE", 2);
define ("PLUGIN_FULLDELETE", false);

define ("PLUGIN_CFGERR_NAMEDUP", 1);        // name duplication found
define ("PLUGIN_CFGERR_SYSVERBAD", 2);      // required system version invalid
define ("PLUGIN_CFGERR_USEDINBAD", 4);      // usage zone invalid
define ("PLUGIN_CFGERR_LICENSEBAD", 8);     // license type invalid
define ("PLUGIN_CFGERR_SETTINGSFUNC", 16);  // problem with settingsfunc function
define ("PLUGIN_CFGERR_HEADERFUNC", 32);    // problem with headerfunc function
define ("PLUGIN_CFGERR_INCLDUP", 64);       // inclusion verb duplication found
define ("PLUGIN_CFGERR_INCLBAD", 128);      // inclusion verb invalid
define ("PLUGIN_CFGERR_NAMENF", 256);       // name not specified
define ("PLUGIN_CFGERR_SYSVERNF", 512);     // required system version not specified
define ("PLUGIN_CFGERR_USEDINNF", 1024);    // usage zone not specified
define ("PLUGIN_CFGERR_INCLNF", 2048);      // inclusion verb not specified
define ("PLUGIN_CFGERR_INITFILENF", 4096);  // initializing file not found
define ("PLUGIN_CFGERR_INITFILEBAD", 8192); // initializing file invalid
define ("PLUGIN_CFGERR_REGPATHNF", 16384);  // registered path not found

define ("PLUGIN_SETTINGS_SAVETOSTD", "std");
define ("PLUGIN_SETTINGS_SAVETOSYS", "sys");

/* PLUGINS CLASS
 *
 * Maintains the Plugin API
 */
class PluginsClass {
	// overloaded data
	public $_keys = array("scripts" => array(), "uiwidgets" => array());
	public $jq_lines = array();
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'_plugins');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Load plugins and frameworks into system
	 * @internal Core function
	 */
	function initPluginsandFrameworks(){
	    global $_system, $incl, $_db_control, $_error, $_events, $_page;

	    /*  usedin = both or usedin = admin/front
	        is_framework = 0
	     */
	    $stack = debug_backtrace();
	    if(strpos($stack[0]['file'], "_core") === false && strpos($stack[0]['file'], "_lib") === false) die('Calling '.__FUNCTION__.' in '.$stack[0]['file'].' not allowed.');
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes

	    $plugins_array = array();       	// all plugins in system
	    $pluginsincl_array = array();   	// included plugins
	    $pluginsprob_array = array();   	// problem/deleted plugins
	    $pluginscontexts_array = array();	// context for plugins
	    $incl_zones = ((IN_ADMIN) ? array('admin', 'both') : array('front', 'both'));

	    // prepare inclusion verbs list
	    // the $incl string is processed and used to populate a system array, which is less susceptable to modification
	    $incl = trim($incl);
	    $incl_array = (($incl != '') ? preg_split("/( |\||,|, )/", $incl) : array());

	    // add 3rd-party universally-included (built-in) plugins
	    // or system extensions
	    $sys_built_in_plugins = $_db_control->setTable(PLUGINS_TABLE)->setFields("incl")->setWhere("builtin = 0 AND autoincl = 1")->getRec(true);
	    if(count($sys_built_in_plugins) > 0) $incl_array += $sys_built_in_plugins;

	    $incl_array = array_unique($incl_array);

	    // pending inclusion verbs adopts inclusion array
	    $rec_i = $incl_array;

	    // all currently registered plugins
	    $rec_p = $_db_control->setTable(PLUGINS_TABLE)->setWhere("is_framework = 0")->setOrder("`builtin` DESC, `group` ASC, `incl` ASC, `depends` ASC")->getRec();

	    if(count($rec_p) > 0){
	        // if plugin is dependent on another non-built-in plugin
	        // that is not in inclusion list, add parent to list
	        foreach($rec_p as $k => $the_plugin){
	            $depends = explode(",", trim($the_plugin['depends']));      // break multiple dependents
	            if(!empty($the_plugin['parent'])) $depends[] = $the_plugin['parent'];
	            foreach($depends as $depend){
	                $depend = trim($depend);
	                if(!in_array($depend, array('jquery', 'jqueryui')) && !empty($depend) && in_array($the_plugin['incl'], $incl_array)){
	                    if(!in_array($depend, $incl_array)){
	                        $incl_array[] = $depend;
	                    }
	                }
	            }
	        }

	        // run through registered plugins
	        foreach($rec_p as $the_plugin){
	            $incl_verb = $the_plugin['incl'];
	            $the_plugin['active'] = (boolean) $the_plugin['active'];
	            $the_plugin['is_deleted'] = (boolean) $the_plugin['is_deleted'];
	            $the_plugin['is_framework'] = (boolean) $the_plugin['is_framework'];
	            $the_plugin['is_editor'] = (boolean) $the_plugin['is_editor'];
	            $the_plugin['builtin'] = (boolean) $the_plugin['builtin'];
	            $the_plugin['autoincl'] = (boolean) $the_plugin['autoincl'];
	            $the_plugin['nodisable'] = (boolean) $the_plugin['nodisable'];
	            $the_plugin['nodelete'] = (boolean) $the_plugin['nodelete'];
	            $the_plugin_error_msg = (($the_plugin['builtin']) ? "Extension" : "Plugin")." inclusion problem: the ".(($the_plugin['builtin']) ? "extension" : "plugin")." (verb '".$incl_verb."')";

	            if($the_plugin['error_code'] == 0 && $the_plugin['is_deleted'] == 0){
	                // no errors.  save the plugin in the full plugins array
                	$the_plugin['initialized'] = false;
	                $plugins_array[$the_plugin['incl']] = $the_plugin;

	                // record initcontexts value for late-binding of this plugin if its context is satisfied
                    if(!empty($the_plugin['initcontexts']))
                    	$pluginscontexts_array[$the_plugin['incl']] = $this->parseInitContexts($the_plugin['initcontexts']);

	                $incl_key = array_search($incl_verb, $incl_array);
	                if(($incl_key !== false || $the_plugin['builtin']) && !isset($pluginsincl_array[$incl_verb])){
	                    // plugin hasn't already been included.  get ready to include it
	                    $reclist = trim(join("', '", $rec_i));
	                    if($the_plugin['active'] == 1){
	                        // plugin is active
	                        if(in_array($the_plugin['usedin'], $incl_zones)){
	                            // plugin is in right zone
	                            if(version_compare($the_plugin['sysver'], CODE_VER) <= 0){
	                                // plugin is compatible with this version of Foundry
	                                $initfile = $the_plugin['folder'].$the_plugin['initfile'];
	                                if(file_exists(SITE_PATH.$initfile)){
	                                    // file found. save plugin in the included plugins array
	                                    if(include (SITE_PATH.$initfile)){
	                                    	$the_plugin['initialized'] = $plugins_array[$the_plugin['incl']]['initialized'] = true;
	                                        $pluginsincl_array[$incl_verb] = $the_plugin;
	                                        unset($rec_i[$incl_key]);
	                                        $this->pluginUpdateCallers($the_plugin['id']);

	                                        if(isset($the_plugin['headerfunc'])){
	                                            // try to initiate any class __autoloaders
	                                            $public_method = explode("->", $the_plugin['headerfunc']);
	                                            // if(count($public_method) == 2 && method_exists($public_method[0], $public_method[1])){
	                                                // $obj = new $public_method[0]();
	                                            // }
	                                        }
	                                    }
	                                }else{
	                                    $_error->addErrorMsg((($the_plugin['builtin']) ? "Extension" : "Plugin")." inclusion problem: the ".(($the_plugin['builtin']) ? "extension" : "plugin")." control file, ".SITE_PATH.$initfile.", was not found.", CORE_ERR);
	                                }
	                            }else{
	                                if(!empty($reclist)) $_error->addErrorMsg($the_plugin_error_msg." not compatible with this version of ".SYS_NAME.".", CORE_ERR);
	                            }
	                        }else{
	                            if(!empty($reclist)) $_error->addErrorMsg($the_plugin_error_msg." cannot be used in the ".((IN_ADMIN) ? 'admin' : 'public site').".", CORE_ERR);
	                        }
	                    }else{
	                        if(!empty($reclist)) $_error->addErrorMsg($the_plugin_error_msg." is not activated.", CORE_ERR);
	                    }
	                }
	            }else{
	                $pluginsprob_array[$incl_verb] = $the_plugin;
	            }
	        }
	    }

	    // any plugins left in pending inclusion verbs array means they encountered problem
	    $rec_i = array_filter($rec_i);
	    if(count($rec_i) > 0) $_error->addErrorMsg("Plugin inclusion problem: the plugin".((count($rec_i) > 1) ? "s" : "")." (verb".((count($rec_i) > 1) ? "s" : "")." '".join("', '", $rec_i)."') could not be included.", CORE_ERR);

	    // prepare frameworks list
	    $frameworks_array = array();
	    $rec_f = $_db_control->setTable(PLUGINS_TABLE)->setWhere("is_framework = 1", "depends ASC")->getRec();
	    if(count($rec_f) > 0){
	        foreach($rec_f as $the_fw){
	            $frameworks_array[$the_fw['incl']] = $the_fw;
	        }
	        ksort($frameworks_array);
	    }

	    // register system properies
	    $_system->plugins = $plugins_array;
	    $_system->pluginsincl = $pluginsincl_array;
	    $_system->pluginsprob = $pluginsprob_array;
	    $_system->pluginscontexts = $pluginscontexts_array;
	    $_system->frameworks = $frameworks_array;
	    $_system->incl = $incl_array;
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Initialize one or more plugins, post-loader, adding the code to the PHP namespace.
	 * Unlike the similar method, loadPlugin which adds plugin script lines to the HTML head,
	 * includePlugins adds the plugin code to the PHP namespace.
	 * @param string $incl                      Comma-separated list of plugin verbs
	 * @internal Core function
	 */
	function includePlugins($incl, $suppressErrors = false){
	    global $_system, $_error, $_events;

	    /*  usedin = both or usedin = admin/front
	        is_framework = 0
	     */
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	    $pluginsincl_array = $_system->pluginsincl;   // already included plugins
	    $incl_zones = ((IN_ADMIN) ? array('admin', 'both') : array('front', 'both'));

	    // prepare inclusion verbs list
	    // the $incl string is processed and used to populate a system array, which is less susceptable to modification
	    if(!is_array($incl)){
	        $incl = trim($incl);
	        $incl_array = (($incl != '') ? preg_split("/( |\||,|, )/", $incl) : array());
	    }else{
	        $incl_array = $incl;
	    }
	    $incl_array = array_unique($incl_array);

	    // pending inclusion verbs adopts inclusion array
	    $rec_i = $incl_array;

	    // all currently registered plugins
	    $rec_p = $_system->plugins;

	    if(count($rec_p) > 0){
	        // if plugin is dependent on another non-built-in plugin
	        // that is not in inclusion list, add parent to list
	        foreach($rec_p as $k => $the_plugin){
	            $depends = explode(",", trim($the_plugin['depends']));      // break multiple dependents
	            if(!empty($the_plugin['parent'])) $depends[] = $the_plugin['parent'];
	            foreach($depends as $depend){
	                $depend = trim($depend);
	                if(!in_array($depend, array('jquery', 'jqueryui')) && !empty($depend) && in_array($the_plugin['incl'], $incl_array)){
	                    if(!in_array($depend, $incl_array)){
	                        $incl_array[] = $depend;
	                    }
	                }
	            }
	        }

	        // run through registered plugins
	        foreach($rec_p as $the_plugin){
	            $incl_verb = $the_plugin['incl'];
	            $the_plugin['active'] = (boolean) $the_plugin['active'];
	            $the_plugin['is_deleted'] = (boolean) $the_plugin['is_deleted'];
	            $the_plugin['is_framework'] = (boolean) $the_plugin['is_framework'];
	            $the_plugin['is_editor'] = (boolean) $the_plugin['is_editor'];
	            $the_plugin['builtin'] = (boolean) $the_plugin['builtin'];
	            $the_plugin['autoincl'] = (boolean) $the_plugin['autoincl'];
	            $the_plugin['nodisable'] = (boolean) $the_plugin['nodisable'];
	            $the_plugin['nodelete'] = (boolean) $the_plugin['nodelete'];
	            $the_plugin_error_msg = (($the_plugin['builtin']) ? "Extension" : "Plugin")." inclusion problem: the ".(($the_plugin['builtin']) ? "extension" : "plugin")." (verb '".$incl_verb."')";

	            $incl_key = array_search($incl_verb, $incl_array);
	            if($incl_key !== false && !$the_plugin['builtin'] && !isset($pluginsincl_array[$incl_verb])){
	                // plugin hasn't already been included.  get ready to include it
	                if($the_plugin['active'] == 1){
	                    // plugin is active
	                    if(in_array($the_plugin['usedin'], $incl_zones)){
	                        // plugin is in right zone
	                        if(version_compare($the_plugin['sysver'], CODE_VER) <= 0){
	                            // plugin is compatible with this version of Foundry
	                            $initfile = $the_plugin['folder'].$the_plugin['initfile'];
	                            if(file_exists(SITE_PATH.$initfile)){
	                                // file found. save plugin in the included plugins array
	                                if(include_once (SITE_PATH.$initfile)){
                                    	$the_plugin['initialized'] = true;
	                                    $pluginsincl_array[$incl_verb] = $the_plugin;
	                                    unset($rec_i[$incl_key]);
	                                    $this->pluginUpdateCallers($the_plugin['id']);
	                                }else{
	                                    if(!$suppressErrors) $_error->addErrorMsg($the_plugin_error_msg." was already included.", CORE_ERR);
	                                }
	                            }else{
	                                if(!$suppressErrors) $_error->addErrorMsg("Plugin inclusion problem: the plugin control file, ".SITE_PATH.$initfile.", was not found.", CORE_ERR);
	                            }
	                        }else{
	                            if(!$suppressErrors) $_error->addErrorMsg($the_plugin_error_msg."is not compatible with this version of ".SYS_NAME.".", CORE_ERR);
	                        }
	                    }else{
	                        if(!$suppressErrors) $_error->addErrorMsg($the_plugin_error_msg." cannot be used in the ".((IN_ADMIN) ? 'admin' : 'public site').".", CORE_ERR);
	                    }
	                }else{
	                    if(!$suppressErrors) $_error->addErrorMsg($the_plugin_error_msg." is not activated.", CORE_ERR);
	                }
	            }
	        }
	    }

	    // any plugins left in pending inclusion verbs array means they encountered problem
	    $rec_i = array_filter($rec_i);
	    if(count($rec_i) > 0 && !$suppressErrors) $_error->addErrorMsg("Plugin inclusion problem: the plugin".((count($rec_i) > 1) ? "s" : "")." (verb".((count($rec_i) > 1) ? "s" : "")." '".join("', '", $rec_i)."') could not be included.", CORE_ERR);

	    // register system properies
	    $_system->pluginsincl = $pluginsincl_array;
	    $_system->incl = $incl_array;
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Initialize one or more plugins that satisfy specific context criteria.
	 * Unlike the similar method, loadPlugin which adds plugin script lines to the HTML head,
	 * includePluginsMatchingContexts adds the plugin code to the PHP namespace.
	 * @internal Core function
	 */
	function includePluginsMatchingContexts(){
		global $_page, $_system, $_users;

		$usertype_arry = $_users->getUserTypes();
		if(count($_system->pluginscontexts) > 0){
			$incl_array = array();
			foreach($_system->pluginscontexts as $incl => $contexts){
				$ok = 0;
				foreach($contexts as $attr => $conditions){
					$attr = strtolower($attr);
					switch($attr){
						case 'targettype':
							// printr($_page);
							if(in_array($_page->targettype, $conditions)) $ok++;
							break;
						case 'datatype':
							if(in_array($_page->datatype, $conditions)) $ok++;
							break;
						case 'targetaction':
							if(in_array($_page->targetaction, $conditions)) $ok++;
							break;
						case 'url':
							// true if uri matches the uri regex
							if(preg_match('#'.$conditions[0].'#i', $_page->uri)) $ok++;
							break;
						case 'usertype':
							// true if users type is one of the conditions
							$conditions = array_change_key_case(array_flip($conditions), CASE_UPPER);
							$valid_uts = array_intersect_key($usertype_arry, $conditions);
							if(in_array($_users->level, $valid_uts) && !empty($valid_uts)) $ok++;
							break;
						case 'allowance':
							// true if user is allowed to do at least on of the conditions
							foreach($conditions as $condition)
								if($_users->userIsAllowedTo($condition)) {
									$ok++;
									break;
								}
							break;
						case 'queryvar':
							// using regex, matches uri query against list of conditional query fragments
							$query_vars = parse_url($_page->uri, PHP_URL_QUERY);
							$query_arry = explode("&", $query_vars);
							foreach($conditions as $condition)
								if(preg_grep('#'.$condition.'#i', $query_arry)) {
									$ok++;
									break;
								}
							break;
					}
				}

				if($ok == count($contexts)){
					$incl_array[] = $incl;
				}
			}

			if(!empty($incl_array)){
				$this->includePlugins($incl_array);
			}
		}
	}

	/**
	 * Return jQuery version number
	 * @return string
	 */
	function getJQueryVer(){
	    global $_db_control;

	    $ver = $_db_control->setTable(PLUGINS_TABLE)->setFields("ver")->setWhere("`incl` = 'jquery'")->getRecItem();
	    if(empty($ver)) $ver = JQUERY_FALLBACK_VER;
	    return $ver;
	}

	/**
	 * Return jQuery UI Core version number
	 * @return string
	 */
	function getJQueryUIVer(){
	    global $_db_control;

	    $ver = $_db_control->setTable(PLUGINS_TABLE)->setFields("ver")->setWhere("`incl` = 'jqueryui'")->getRecItem();
	    if(empty($ver)) $ver = JQUERYUI_FALLBACK_VER;
	    return $ver;
	}

	// ----------- PLUGIN SETTINGS FUNCTIONS ---------------

	/**
	 * Load plugin settings found in plugin.info files into database.
	 * This function does not make the plugins ready for use.  It
	 * processes any new or updated plugins.  Call $_themes->prepHeadSection,
	 * includePlugins or loadPlugin to put plugins into use.
	 * @param str $status
	 * @internal Core function
	 * @see loadPlugin
	 * @version 3.6
	 */
	function getInstalledPlugins($status = "all"){
	    global $_settings, $pluginslist, $_users, $_db_control, $_editors, $_events;

	    $stack = debug_backtrace();
	    if(strpos($stack[0]['file'], "_core") === false && strpos($stack[0]['file'], "_lib") === false) die('Calling '.__FUNCTION__.' in '.$stack[0]['file'].' not allowed.');
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes

	    if(!$_users->userIsAllowedTo('install_plugins') || !$_users->userIsAllowedTo('update_plugins'))
	        return false;

	    $_settings->clearSettingsIssues('plugins');
	    $_settings->clearSettingsIssues('plugins-info');
	    $_settings->clearSettingsIssues('editors');

	    $updflag = rand(0, 99999);
	    $numIns = 0;
	    $numUpd = 0;
	    $numDel = 0;

	    $maxtime = ini_get('max_execution_time');
	    set_time_limit(0);

	    $pluginpaths = $this->readInstalledPluginFolders(array(), ADMIN_FOLDER.PLUGINS_FOLDER);
	    $pluginpaths = $this->readInstalledPluginFolders($pluginpaths, ADMIN_FOLDER.EXTENSIONS_FOLDER, true);
	    $pluginslist = $this->readInstalledPluginInfoFiles($pluginpaths);

	    // loop through plugins updating/adding each to the database
	    foreach($pluginslist as $pluginslug => $pluginarry){
	        // find plugin record
	        $temp_updflag = $updflag;
	        $pluginid     = $pluginarry['id'];

	        // forced settings
	        if($pluginarry['builtin'] == 0 && $pluginarry['is_ext']) $pluginarry['builtin'] = 1;
	        $pluginarry['is_editor'] = 0;
	        $pluginarry['is_framework'] = 0;
	        unset($pluginarry['is_ext']);

	        // handle read cycle errors
	        if(!empty($pluginarry['errors'])){
	            // error detected...
	            $pluginarry['errors'] = join("<br/>", $pluginarry['errors']);
	            $temp_updflag = $pluginarry['mark'];
	        }elseif(getIfSet($pluginfromdb[0]['is_deleted'])){
	            // plugin was soft-deleted within Foundry...
	            $pluginsarry['is_deleted'] = true;
	            $temp_updflag = -1;
	        }else{
	            // no problems here...
	            $pluginarry['errors'] = '';
	            $pluginarry['error_code'] = 0;
	        }

	        unset($pluginarry['id']);
	        unset($pluginarry['mark']);
	        if($pluginid > 0){
	            // update
	            $fldvals = array("updflag" => $temp_updflag);
	            foreach($pluginarry as $key => $data) $fldvals[$key] = str_replace("'", "&#34;", $data);
	            if($_db_control->setTable(PLUGINS_TABLE)->setFieldvals($fldvals)->setWhere("id = {$pluginid}")->updateRec()) $numUpd++;
	        }else{
	            // insert
	            $fldvals = array("updflag" => $temp_updflag);
	            foreach($pluginarry as $key => $data) $fldvals[$key] = str_replace("'", "&#34;", $data);
	            $pluginid = $_db_control->setTable(PLUGINS_TABLE)->setFieldvals($fldvals)->insertRec();
	            if($pluginid > 0) $numIns++;
	        }
	    }

	    // mark plugins that were not found, or have config errors, as deleted/problematic
	    $_db_control->setTable(PLUGINS_TABLE)->setFieldvals(array("updflag" => -1))->setWhere("is_framework = 0 AND updflag != '{$updflag}' AND updflag != -2")->updateRec();
	    $_db_control->setTable(PLUGINS_TABLE)->setFieldvals(array("active" => 0, "error_code" => PLUGIN_CFGERR_REGPATHNF, "errors" => "The plugin was not found in its registered path, and has been deactivated."))->setWhere("is_framework = 0 AND is_editor = 0 AND updflag = -1 AND is_deleted = 0 AND active = 1 AND error_code = 0")->updateRec();
	    $numDel = $_db_control->setTable(PLUGINS_TABLE)->setWhere("is_framework = 0 AND updflag = -1 AND is_deleted = 0")->getRecNumRows();

	    // present issues
	    if($numIns > 0) $_settings->addSettingsIssue('plugins-info', $numIns." NEW plugin".(($numIns != 1) ? "s" : "")." ".(($numIns != 1) ? "were" : "was")." found and installed.");
	    if($numDel > 0) $_settings->addSettingsIssue('plugins', $numDel." plugin".(($numDel != 1) ? "s" : "")." ".(($numDel != 1) ? "were" : "was")." NOT found, ".(($numDel > 1) ? "have" : "has")." had ".(($numDel > 1) ? "their" : "its")." name changed, or ".(($numDel > 1) ? "have" : "has")." info file problems.");

	    $this->getInstalledFrameworks($updflag);
	    $_editors->getInstalledEditors($updflag);

	    set_time_limit($maxtime);
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	}

	/**
	 * Load frameworks into database.
	 * This function does not make the frameworks ready for use.
	 * Call $_themes->prepHeadSection, includePlugins or loadPlugin
	 * @internal Core function
	 * @return array
	 * @version 3.6
	 */
	function getInstalledFrameworks($updflag){
	    global $_settings, $_db_control, $_events;

	    $stack = debug_backtrace();
	    if(strpos($stack[0]['file'], "_core") === false && strpos($stack[0]['file'], "_lib") === false) die('Calling '.__FUNCTION__.' in '.$stack[0]['file'].' not allowed.');

	    // add built-in frameworks
	    $fwlist = array('jqueryui' => array(
	                            'name' => 'jQuery UI', 'author' => 'various', 'created' => BLANK_DATE, 'ver' => $this->getJQueryUIVer(),
	                            'revised' => BLANK_DATE, 'descr' => 'User Interface module for jQuery.  Includes UI Core, several interactions, widgets, and effects.',
	                            'sysver' => "2.00", 'website' => 'http://www.jqueryui.com', 'usedin' => 'both', 'incl' => 'jqueryui', 'initfile' => 'jquery-ui.min.js',
	                            'headerfunc' => '', 'settingsfunc' => '', 'depends' => 'jquery', 'parent' => '', 'is_framework' => 1, 'inline_settings' => 'ver',
	                            'nodelete' => 1, 'nodisable' => 1, 'builtin' => 1, 'autoincl' => 1, 'active' => 1
	                    )) +
	                    array('jquery' => array(
	                            'name' => 'jQuery', 'author' => 'John Resig', 'created' => '2010-11-11', 'ver' => $this->getJQueryVer(),
	                            'revised' => BLANK_DATE, 'descr' => 'jQuery core framework.', 'sysver' => "2.00",
	                            'website' => 'http://www.jquery.com', 'usedin' => 'both', 'incl' => 'jquery', 'initfile' => 'jquery.min.js',
	                            'headerfunc' => '', 'settingsfunc' => '', 'depends' => '', 'parent' => '', 'is_framework' => 1, 'inline_settings' => 'ver',
	                            'nodelete' => 1, 'nodisable' => 1, 'builtin' => 1, 'autoincl' => 1, 'active' => 1
	                    ));

	    // add custom-loaded frameworks (typically from Google APIs)
	    $fwlist += $this->readCustomFrameworkINI();

	    // loop through frameworks updating/adding each to the database
	    $numIns = 0;
	    $numUpd = 0;

	    foreach($fwlist as $fwslug => $fwarry){
	        // find framework record
	        $fwarry['errors'] = '';
	        $fwarry['error_code'] = 0;
	        $fwarry['updflag'] = $updflag;
	        $fwarry['is_framework'] = 1;
	        $fwarry['is_editor'] = 0;

	        $fwfromdb = $_db_control->setTable(PLUGINS_TABLE)->setWhere("name = '{$fwarry['name']}' AND is_framework = 1")->setLimit()->getRec();
	        $fwid = getIntValIfSet($fwfromdb[0]['id']);

	        // update the records
	        if($fwid > 0){
	            // update
	            $fldvals = array();
	            foreach($fwarry as $key => $data) $fldvals[$key] = str_replace("'", "&#34;", $data);
	            if($_db_control->setTable(PLUGINS_TABLE)->setFieldvals($fldvals)->setWhere("id = {$fwid}")->updateRec()) $numUpd++;
	        }else{
	            // insert (framework is initially disabled)
	            if(!isset($fwarry['active'])) $fwarry['active'] = 0;
	            $fldvals = array();
	            foreach($fwarry as $key => $data) $fldvals[$key] = str_replace("'", "&#34;", $data);
	            $fwid = $_db_control->setTable(PLUGINS_TABLE)->setFieldvals($fldvals)->insertRec();
	            if($fwid > 0) $numIns++;
	        }
	    }

	    // present issues
	    if($numIns > 0) $_settings->addSettingsIssue('plugins-info', $numIns." NEW framework".(($numIns != 1) ? "s" : "")." ".(($numIns != 1) ? "were" : "was")." found and installed.");
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	}

	/**
	 * Read plugins/extensions folders searching for valid plugin.info files for processing
	 * @param str $dir
	 * @param boolean $is_ext               True if search is for extensions; false for plugins
	 * @internal Core function
	 * @return array
	 * @version 3.6
	 */
	function readInstalledPluginFolders($pluginpaths, $dir, $is_ext = false, $mergearrays = true){
	    global $_settings, $_db_control, $_error, $_filesys, $_events;

	    $stack = debug_backtrace();
	    if(strpos($stack[0]['file'], "_core") === false && strpos($stack[0]['file'], "_lib") === false) die('Calling '.__FUNCTION__.' in '.$stack[0]['file'].' not allowed.');

	    // first, read folders and identify whether their contents
	    // are already in DB or are newly installed

	    $updpaths = array();    // plugins that are found in db and ready for update
	    $newpaths = array();    // plugins not found in db and deemed new
	    $errpaths = array();    // plugins that are found in db but were marked with an error
	    if(isset($dir)){
	        $this_dir = $dir;           // path/_plugins
	        $this_dir = $_filesys->addSlash($this_dir);
	        if(false !== ($handle = opendir(SITE_PATH.$dir))) {
	            while (false !== ($file = readdir($handle))) {
	                if($file !== '.' && $file !== '..'){
	                    if(is_dir(SITE_PATH.$this_dir.$file)){
	                        $sub_dir = $this_dir.$file."/";     // path/_plugins/folder
	                        if(file_exists(SITE_PATH.$sub_dir.'plugin.info')){
	                            // remove record if folder is relative
	                            // $_db_control->setTable(PLUGINS_TABLE)->setWhere("folder = '".$_filesys->getRelativePath($sub_dir)."'")->deleteRec();

	                            // get the plugin record where the folders match
	                            // - if matched, plugin is being updated, otherwise it is new
	                            $pluginfromdb = $_db_control->setTable(PLUGINS_TABLE)->setWhere("folder = '".$sub_dir."' OR folder = '".SITE_PATH.$sub_dir."'")->setLimit()->getRec();
	                            if(count($pluginfromdb) > 0){
	                                // matched
	                                if($pluginfromdb[0]['error_code'] == 0){
	                                    $updpaths[] = array("id" => $pluginfromdb[0]['id'], "path" => $sub_dir, "info" => $sub_dir."plugin.info", "tempname" => "", "is_ext" => $is_ext);
	                                }else{
	                                    $errpaths[] = array("id" => $pluginfromdb[0]['id'], "path" => $sub_dir, "info" => $sub_dir."plugin.info", "tempname" => "", "errorcode" => $pluginfromdb[0]['error_code'], "is_ext" => $is_ext);
	                                }
	                            }else{
	                                // not matched
	                                $newpaths[] = array("id" => 0, "path" => $sub_dir, "info" => $sub_dir."plugin.info", "tempname" => $file, "is_ext" => $is_ext);
	                            }
	                        }

	                        // dig into this directory for more plugins
	                        $pluginpaths = $this->readInstalledPluginFolders($pluginpaths, $sub_dir, $is_ext);
	                    }
	                }
	            }
	            closedir($handle);
	        }
	    }
	    if($mergearrays)
	       return array_merge($pluginpaths, $updpaths, $errpaths, $newpaths);
	    else
	       return array($pluginpaths, $updpaths, $errpaths, $newpaths);
	}

	/**
	 * Read plugin.info file data, populating pluginlist array
	 * @param str $dir
	 * @internal Core function
	 * @return array
	 * @version 3.6
	 */
	function readInstalledPluginInfoFiles($pluginpaths){
	    global $_settings, $_filesys;

	    $stack = debug_backtrace();
	    if(strpos($stack[0]['file'], "_core") === false && strpos($stack[0]['file'], "_lib") === false) die('Calling '.__FUNCTION__.' in '.$stack[0]['file'].' not allowed.');

	    // first, read folders and identify whether their contents
	    // are already in DB or are newly installed

	    $valid_setting_keys = array('name', 'author', 'ver', 'created', 'revised', 'descr', 'sysver', 'license',
	                                'website', 'usedin', 'incl', 'initfile', 'initcaller', 'initcontexts', 'headerfunc', 'settingsfunc',
	                                'depends', 'parent', 'nodelete', 'nodisable', 'builtin', 'autoincl', 'group', 'active');

	    if(is_null($pluginpaths)) return false;

	    $pluginslist = array();
	    foreach($pluginpaths as $pathdata){
	        $fhandle = @fopen(SITE_PATH.$pathdata['info'], 'r');
	        if($fhandle !== false){
	            $pluginname = null;
	            $pluginslug = null;
	            $pluginarry = array();
	            $pluginarry['id'] = $pathdata['id'];
	            $pluginerrs = array();
	            $pluginerrcode = 0;
	            $pluginmark = -1;
	            $pluginfolder = $_filesys->getLowestChildFolder($pathdata['path'])."/";
	            $pluginext = $pathdata['is_ext'];

	            while($setting = fscanf($fhandle, "%[#]%s\t%[^\t]")){
	                list(, $key, $val) = $setting;
	                $key = strtolower(str_replace(':', '', $key));
	                $val = trim($val);
	                if(in_array($key, $valid_setting_keys)){
	                    switch($key){
	                        case 'name':
	                            if(is_null($pluginname)){
	                                if($pathdata['tempname'] != '') $val = ucwordsAdv($pathdata['tempname']);
	                                $pluginslug = slugify($val);
	                                if(isset($pluginslist[$pluginslug])){
	                                    // oops! slug already in use
	                                    $pluginerrs[] = "The name '".$val."' in ".$pluginfolder."plugin.info has been registered to another plugin.";
	                                    $pluginerrcode += PLUGIN_CFGERR_NAMEDUP;
	                                    // use folder as temp name so data can be saved
	                                    $pluginslug = slugify($pluginfolder);
	                                    $pluginname = ucwordsAdv($pluginslug);
	                                    $pluginmark = -2;
	                                }else{
	                                    $pluginname = $val;
	                                }
	                            }
	                            break;
	                        case 'builtin':
	                            $val = (($pluginext) ? 1 : 0);
	                            $pluginarry['is_ext'] = $pathdata['is_ext'];    // a temporary marker that indicates an extension
	                            break;
	                        case 'autoincl':
	                        case 'nodelete':
	                        case 'nodisable':
	                        case 'active':
	                            $val = strtolower($val);
	                            $val = (($val == '1' || $val == 'yes' || $val == 'true') ? 1 : 0);
	                            break;
	                        case 'created':
	                        case 'revised':
	                            $val = ((isDate($val)) ? date(PHP_DATE_FORMAT, strtotime($val)) : BLANK_DATE);
	                            break;
	                        case 'usedin':
	                            if(!in_array(strtolower($val), array('admin', 'front', 'both'))) {
	                                $pluginerrs[] = "'".$val."' is not a valid USEDIN value in ".$pluginfolder."plugin.info.";
	                                $pluginerrcode += PLUGIN_CFGERR_USEDINBAD;
	                            }
	                            break;
	                        case 'license':
	                            if(!in_array(strtolower($val), array('free', 'trial', 'limited', 'full', 'subscription'))) {
	                                $pluginerrs[] = "'".$val."' is not a valid LICENSE value in ".$pluginfolder."plugin.info.";
	                                $pluginerrcode += PLUGIN_CFGERR_LICENSEBAD;
	                            }
	                            break;
	                        case 'website':
	                            if(strtolower(substr($val, 0, 4)) != 'http' && $val != '') $val = 'http://'.$val;
	                            break;
	                        case 'settingsfunc':
	                        case 'headerfunc':
	                            if(!empty($val))
	                                if(multiarray_search($val, $pluginslist) == $key){
	                                    $pluginerrs[] = "'".$val."' is already registered as the ".strtoupper($key)." to another plugin.";
	                                    $pluginerrcode += (($key == 'settingsfunc') ? PLUGIN_CFGERR_SETTINGSFUNC : PLUGIN_CFGERR_HEADERFUNC);
	                                }
	                            break;
	                        case 'incl':
	                            if(multiarray_search($val, $pluginslist) == $key){
	                                $pluginerrs[] = "'".$val."' is already assigned as the ".strtoupper($key)." to another plugin.";
	                                $pluginerrcode += PLUGIN_CFGERR_INCLDUP;
	                            }elseif(preg_match("/([^0-9a-z\-_])/i", $val)){
	                                $pluginerrs[] = "'".$val."' can only contain letters, numbers, hyphen or underscore.";
	                                $pluginerrcode += PLUGIN_CFGERR_INCLBAD;
	                            }
	                            break;
	                        case 'initfile':
	                            if(empty($val)){
	                                $pluginerrs[] = strtoupper($key)." cannot be blank.";
	                                $pluginerrcode += PLUGIN_CFGERR_INITFILENF;
	                            }elseif(!file_exists(SITE_PATH.$pathdata['path'].$val)){
	                                $pluginerrs[] = "The ".strtoupper($key)." was not found in the ".$pluginfolder." folder.";
	                                $pluginerrcode += PLUGIN_CFGERR_INITFILEBAD;
	                            }
	                            break;
	                        case 'group':
	                            break;
	                    }
	                    $pluginarry[$key] = $val;
	                }
	            }
	            fclose($fhandle);

	            if($pluginname == null) {
	                $pluginerrs[] = 'NAME is missing in '.$pluginfolder.'plugin.info.';
	                $pluginerrcode += PLUGIN_CFGERR_NAMENF;
	                continue;
	            }elseif(!isset($pluginarry['sysver'])){
	                $pluginerrs[] = 'SYSVER is required in '.$pluginfolder.'plugin.info.';
	                $pluginerrcode += PLUGIN_CFGERR_SYSVERNF;
	            }elseif(!isset($pluginarry['usedin'])){
	                $pluginerrs[] = 'USEDIN is required in '.$pluginfolder.'plugin.info.';
	                $pluginerrcode += PLUGIN_CFGERR_USEDINNF;
	            }elseif(!isset($pluginarry['incl'])){
	                $pluginerrs[] = 'INCL is required in '.$pluginfolder.'plugin.info.';
	                $pluginerrcode += PLUGIN_CFGERR_INCLNF;
	            }elseif(!isset($pluginarry['initfile'])){
	                $pluginerrs[] = 'INITFILE is required in '.$pluginfolder.'plugin.info.';
	                $pluginerrcode += PLUGIN_CFGERR_INITFILENF;
	            }

	            if(count($pluginerrs) > 0){
	                $pluginarry['errors'] = $pluginerrs;
	                $pluginarry['error_code'] = $pluginerrcode;
	                $pluginarry['mark'] = $pluginmark;
	                $pluginarry['active'] = 0;
	                $_settings->settings_issues['plugins'] = array_merge($_settings->settings_issues['plugins'], $pluginerrs);
	            }else{
	                $pluginarry['errors'] = "";
	                $pluginarry['error_code'] = "";
	                $pluginarry['mark'] = 0;
	                if($pluginext) $pluginarry['autoincl'] = 1;
	            }
	            $pluginarry['folder'] = $_filesys->getRelativePath($pathdata['path']);
	            $pluginslist[$pluginslug] = $pluginarry;
	        }
	    }
	    return $pluginslist;
	}

	/**
	 * Read the frameworks.ini file to load custom frameworks
	 * @internal Core function
	 * @return array
	 */
	function readCustomFrameworkINI() {
	    global $_settings, $_events;

	    $stack = debug_backtrace();
	    if(strpos($stack[0]['file'], "_core") === false && strpos($stack[0]['file'], "_lib") === false) die('Calling '.__FUNCTION__.' in '.$stack[0]['file'].' not allowed.');
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes

	    $valid_setting_keys = array('name', 'ver', 'descr', 'website',
	                                'depends', 'parent', 'nodelete', 'nodisable', 'active');

	    $fwinitfiles = array('angularjs' => 'angular.min.js',
	    					 'mootools' => 'mootools-yui-compressed.js',
	    					 'chrome-frame' => 'CFInstall.min.js',
	    					 'dojo' => 'dojo/dojo.xd.js',
	                         'ext-core' => 'ext-core.js',
	                         'prototype' => 'prototype.js',
	                         'scriptaculous' => 'scriptaculous.js',
	                         'swfobject' => 'swfobject.js',
	                         'yui' => 'build/yui/yui-min.js',
	                         'webfont' => 'webfont.js');

	    $fcontents = file_get_contents(SITE_PATH.ADMIN_FOLDER.CONFIG_FOLDER."frameworks.ini");
	    $flines = preg_split("/\\n/", $fcontents);
	    $fwname = null;
	    $fwslug = null;
	    $fwarry = array();
	    $fwlist = array();
	    $fwerrs = array();
	    $fwerrcode = 0;
	    $commentstarted = false;

	    foreach($flines as $fline){
	        $fline = trim($fline);
	        if($fline != ''){
	            if($fline == "/*" && !$commentstarted){
	                $commentstarted = true;
	            }elseif($fline == "*/" && $commentstarted){
	                $commentstarted = false;
	            }elseif(!$commentstarted){
	                if(substr($fline, 0, 1) == "#" && strlen($fline) > 1){
	                    if(count($fwarry) > 0 && $fwslug != '') $fwlist[$fwslug] = $fwarry;
	                    $fwslug = strtolower(substr($fline, 1));
	                    $fwarry = array("incl" => $fwslug);
	                }elseif(!is_null($fwslug)){
	                    $fwparts = explode("=", str_replace(";", "", $fline));
	                    if(in_array($fwparts[0], $valid_setting_keys)){
	                        $fwarry[$fwparts[0]] = $fwparts[1];
	                    }
	                }
	            }
	        }
	    }
	    if(count($fwarry) > 0 && $fwslug != '') $fwlist[$fwslug] = $fwarry;
	    foreach($fwlist as $slug => $fwarry){
	        $fwlist[$slug]['usedin'] = 'both';
	        $fwlist[$slug]['is_framework'] = 1;
	        $fwlist[$slug]['nodelete'] = 1;
	        $fwlist[$slug]['builtin'] = 0;
	        $fwlist[$slug]['initfile'] = $fwinitfiles[$slug];
	        $fwlist[$slug]['descr'] = getIfSet($fwarry['name'])." Framework.  ".getIfSet($fwarry['descr']);
	    }
	    return $fwlist;
	}

	/**
	 * Output the HTML for the settings area plugins (installed)
	 * @internal Core function
	 * @version 3.6
	 */
	function showSettingsPluginsInstalledList($param = array()){
	    global $_system, $_users, $_sec, $_settings;

	    $param['search'] = getIfSet($param['search'], null);
	    $param['active'] = getIfSet($param['active']);
	    $param['zone'] = getIfSet($param['zone']);
	    $count = 0;
	    $action = array('checkbox' => array(
	                            'id' => 'plugin_bulkcheck'),
	                    'menu' => array(
	                            'id' => 'plugin_bulkopt',
	                            'sel' => '',
	                            'options' => array('' => '--', 'deactivate' => '(De)activate', 'delete' => 'Delete', 'update' => 'Update')
	                    ),
	                    'buttons' => array(
	                            'plugin_bulkact' => 'Bulk Action')
	                );
	    if(!$_users->userIsAllowedTo('activate_plugins')) unset($action['menu']['options']['deactivate']);
	    if(!$_users->userIsAllowedTo('delete_plugins')) unset($action['menu']['options']['delete']);
	    ?>
	        <div class="settingsactions">
	            <?php if(count($action['menu']['options']) > 0) $_settings->showSettingsActions(0, $action); ?>
	            &nbsp;
	            <span>Search for:</span>
	            <input type="hidden" name="plugin_nonce" value="<?php echo $_sec->createNonce("plugin_search") ?>" />
	            <input type="text" class="plugin_search" name="plugin_search" value="<?php echo $param['search'] ?>" />&nbsp;
	            <input type="submit" class="plugin_search_submit" name="plugin_search_submit" value="Find" rel="plugin_installed" />
	            &nbsp;
	            <span>Filter:</span>
	            <?php
	            $action = array('menu' => array(
	                                'class' => 'plugin_filter_state',
	                                'rel' => 'plugin_installed',
	                                'sel' => $param['active'],
	                                'options' => array('' => '- Any Status -', 'active' => 'Active', 'inactive' => 'Inactive')
	                            )
	                    );
	            $_settings->showSettingsActions(0, $action);
	            $action = array('menu' => array(
	                                'class' => 'plugin_filter_zone',
	                                'rel' => 'plugin_installed',
	                                'sel' => $param['zone'],
	                                'options' => array('' => '- Any Zone -', 'admin' => 'Admin', 'front' => 'Front', 'both' => 'Both')
	                            )
	                    );
	            $_settings->showSettingsActions(0, $action);
	            ?>
	        </div>
	    <?php

	    $group = '--';
	    foreach($_system->plugins as $data){
	        $match = (preg_match("/(.*)".$param['search']."(.*)/i", $data['name']) || is_null($param['search']));
	        if(($param['active'] == 'active' && $data['active'] == 0) || ($param['active'] == 'inactive' && $data['active'] == 1))
	            $match = false;     // the filter state does not match the plugin active state
	        if($param['zone'] != '' && $data['usedin'] != $param['zone'])
	            $match = false;     // the filter zone does not match the plugin zone and filter zone is not both

	        if($data['builtin'] == 0 && $match){
	            if($group != $data['group']) {
	                if($group != '--') echo '</div>'.PHP_EOL;
	                if(empty($data['group']))
	                    echo '<div class="plugin_group expandable"><h2>Ungrouped<a href="" class="expand-button fa fa-arrow-up"></a></h2>'.PHP_EOL;
	                else
	                    echo '<div class="plugin_group expandable"><h2>'.$data['group'].'<a href="" class="expand-button fa fa-arrow-up"></a></h2>'.PHP_EOL;
	                $group = $data['group'];
	            }
	            $rtn = $this->getSettingsPluginRow($data, 'normal');
	            echo $rtn;
	            if($rtn != '') $count++;
	        }
	    }
	    echo '</div>'.PHP_EOL;
	    if($count == 0){ ?>
	        <p>No plugins are installed or your search yielded no results.</p>
	    <?php
	    }
	}

	/**
	 * Output the HTML for the settings area problem plugins (missing, deleted, error)
	 * @internal Core function
	 * @version 3.6
	 */
	function showSettingsPluginsProblemList(){
	    global $_system;

	    $count = 0;
	    ?>
	        <p class="settingsactions"></p>
	    <?php
	    $group = '--';
	    foreach($_system->pluginsprob as $data){
	        if(!empty($filter) && strpos(strtolower($data['name']), $filter) === false) continue;
	        if($group != $data['group']) {
	            if($group != '--') echo '</div>'.PHP_EOL;
	            if($data['builtin'] == 1)
	                echo '<div class="plugin_group expandable"><h2>System<a href="" class="expand-button fa fa-arrow-up"></a></h2>'.PHP_EOL;
	            elseif(empty($data['group']))
	                echo '<div class="plugin_group expandable"><h2>Ungrouped<a href="" class="expand-button fa fa-arrow-up"></a></h2>'.PHP_EOL;
	            else
	                echo '<div class="plugin_group expandable"><h2>'.$data['group'].'<a href="" class="expand-button fa fa-arrow-up"></a></h2>'.PHP_EOL;
	            $group = $data['group'];
	        }
	        $rtn = $this->getSettingsPluginRow($data, 'prob');
	        echo $rtn;
	        if($rtn != '') $count++;
	    }
	    echo '</div>'.PHP_EOL;

	    if($count == 0){ ?>
	        <p>No plugins are currently deleted or have plugin.info file configuration problems.</p>
	    <?php
	    }
	}

	/**
	 * Output the HTML for the settings area frameworks list
	 * @internal Core function
	 * @version 3.6
	 */
	function showSettingsPluginsFrameworks(){
	    global $_system, $_users, $_settings;

	    $count = 0;
	    $action = array('checkbox' => array(
	                            'id' => 'plugin_fw_bulkcheck'),
	                    'menu' => array(
	                            'id' => 'plugin_fw_bulkopt',
	                            'sel' => '',
	                            'options' => array('' => '--', 'deactivate' => '(De)activate')),
	                    'buttons' => array(
	                            'plugin_fw_bulkact' => 'Bulk Action')
	                    );
	    if(!$_users->userIsAllowedTo('activate_frameworks')) unset($action['menu']['options']['deactivate']);
	    ?>
	        <div class="settingsactions">
	            <?php if(count($action['menu']['options']) > 0) $_settings->showSettingsActions(0, $action); ?>
	        </div>
	    <?php
	    foreach($_system->frameworks as $data){
	        $rtn = $this->getSettingsPluginRow($data, 'framework');
	        echo $rtn;
	        if($rtn != '') $count++;
	    }

	    if($count == 0){ ?>
	        <p>No frameworks are installed.</p>
	    <?php
	    }
	}

	/**
	 * Output the HTML for the settings area extensions list
	 * @internal Core function
	 * @version 3.6
	 */
	function showSettingsPluginsExtensions($param = array()){
	    global $_system, $_users, $_sec, $_settings;

	    $param['search'] = getIfSet($param['search'], null);
	    $param['active'] = getIfSet($param['active']);
	    $param['zone'] = getIfSet($param['zone']);
	    $count = 0;
	    $action = array('checkbox' => array(
	                            'id' => 'plugin_ext_bulkcheck'),
	                    'menu' => array(
	                            'id' => 'plugin_ext_bulkopt',
	                            'sel' => '',
	                            'options' => array('' => '--', 'deactivate' => '(De)activate', 'update' => 'Update')
	                    ),
	                    'buttons' => array(
	                            'plugin_bulkact' => 'Bulk Action')
	                );
	    if(!$_users->userIsAllowedTo('activate_plugins')) unset($action['menu']['options']['deactivate']);
	    if(!$_users->userIsAllowedTo('delete_plugins')) unset($action['menu']['options']['delete']);
	    ?>
	        <div class="settingsactions">
	            <?php if(count($action['menu']['options']) > 0) $_settings->showSettingsActions(0, $action); ?>
	            &nbsp;
	            <span>Search for:</span>
	            <input type="hidden" name="plugin_ext_nonce" value="<?php echo $_sec->createNonce("plugin_search") ?>" />
	            <input type="text" class="plugin_search" name="plugin_ext_search" value="<?php echo $param['search'] ?>" />&nbsp;
	            <input type="submit" class="plugin_search_submit" name="plugin_ext_search_submit" value="Find" rel="plugin_extensions" />
	            &nbsp;
	            <span>Filter:</span>
	            <?php
	            $action = array('menu' => array(
	                                'class' => 'plugin_filter_state',
	                                'rel' => 'plugin_extensions',
	                                'sel' => $param['active'],
	                                'options' => array('' => '- Any Status -', 'active' => 'Active', 'inactive' => 'Inactive')
	                            )
	                    );
	            $_settings->showSettingsActions(0, $action);
	            $action = array('menu' => array(
	                                'class' => 'plugin_filter_zone',
	                                'rel' => 'plugin_extensions',
	                                'sel' => $param['zone'],
	                                'options' => array('' => '- Any Zone -', 'admin' => 'Admin', 'front' => 'Front', 'both' => 'Both')
	                            )
	                    );
	            $_settings->showSettingsActions(0, $action);
	            ?>
	        </div>
	    <?php

	    $group = '--';
	    foreach($_system->plugins as $data){
	        $match = (preg_match("/(.*)".$param['search']."(.*)/i", $data['name']) || is_null($param['search']));
	        if(($param['active'] == 'active' && $data['active'] == 0) || ($param['active'] == 'inactive' && $data['active'] == 1))
	            $match = false;     // the filter state does not match the plugin active state
	        if($param['zone'] != '' && $data['usedin'] != $param['zone'])
	            $match = false;     // the filter zone does not match the plugin zone and filter zone is not both

	        if($data['builtin'] == 1 && $match){
	            if($group != $data['group']) {
	                if($group != '--') echo '</div>'.PHP_EOL;
	                if(empty($data['group']))
	                    echo '<div class="plugin_group expandable"><h2>Ungrouped<a href="" class="expand-button fa fa-arrow-up"></a></h2>'.PHP_EOL;
	                else
	                    echo '<div class="plugin_group expandable"><h2>'.$data['group'].'<a href="" class="expand-button fa fa-arrow-up"></a></h2>'.PHP_EOL;
	                $group = $data['group'];
	            }
	            $rtn = $this->getSettingsPluginRow($data, 'extension');
	            echo $rtn;
	            if($rtn != '') $count++;
	        }
	    }
	    echo '</div>'.PHP_EOL;
	    if($count == 0){ ?>
	        <p>No extensions are installed or your search yielded no results.</p>
	    <?php
	    }
	}
	/**
	 * Return HTML for a single plugin row
	 * @param str $plugin_data
	 * @param boolean $is_prob
	 */
	function getSettingsPluginRow($data, $type = 'normal'){
	    global $_users;

	    if($type == 'normal'){
	        $act_link = '';
	        $del_link = '';
	        $act_class= '';
	        if($data['builtin'] == 0){
	            if($_users->userIsAllowedTo('activate_plugins')){
	                if($this->pluginCanActivate($data)){
	                    if($data['active'] == 0){
	                        $act_class = ' notactive';
	                        if($data['nodisable'] == 0) $act_link = '<a href="#" class="plugin_act" rel="'.$data['id'].'">Activate</a>';
	                    }else{
	                        $act_class = '';
	                        if($data['nodisable'] == 0) $act_link = '<a href="#" class="plugin_act" rel="'.$data['id'].'">Deactivate</a>';
	                    }
	                }else{
	                    if($data['nodisable'] == 0) $act_link = '<a href="#" class="plugin_act blocked" rel="'.$data['id'].'">Can\'t Activate</a>';
	                    $act_class = ' notactive red';
	                }
	            }
	            if($data['nodelete'] == 0 && $_users->userIsAllowedTo('delete_plugins')){
	                $del_link = (($act_link != '') ? '&nbsp;|&nbsp;' : '').'<a href="#" class="plugin_del" rel="'.$data['id'].'">Delete</a>';
	            }
	        }

	        ob_start();
	        ?>
	            <div id="plugin_row_<?php echo $data['id']?>" class="plugin_row settings_hover_row">
	                <input type="hidden" id="plugin_id_<?php echo $data['id']?>" name="plugin_id[]" class="plugin_id" value="<?php echo $data['id']?>"/>
	                <input type="hidden" id="plugin_slug_<?php echo $data['id']?>" name="plugin_slug[]" class="plugin_slug" value="<?php echo codify($data['name'])?>"/>
	                <input type="hidden" id="plugin_title_<?php echo $data['id']?>" name="plugin_title[]" class="plugin_title" value="<?php echo $data['name']?>"/>
	                <div class="plugin_leftside col-lg-2">
	                    <?php if(file_exists(SITE_PATH.$data['folder'].'pluginicon.png')) echo '<img src="'.WEB_URL.$data['folder'].'pluginicon.png'.'" alt="'.$data['name'].'" class="plugin_icon" />'; ?>
	                    <?php if($act_link.$del_link != '') { ?><input type="checkbox" id="plugin_check_<?php echo $data['id']?>" class="plugin_checks" /><?php } ?>
	                    <span id="plugin_name_<?php echo $data['id']?>" class="plugin_name<?php echo $act_class?>"><?php echo $data['name'].(($data['ver'] != '') ? ' v. '.$data['ver'] : '')?></span><br/>
	                    <span id="plugin_actions_<?php echo $data['id']?>" class="plugin_actions action_items"><?php echo $act_link.$del_link.(($data['builtin'] > 0) ? ' [Extension]' : '')?></span>
	                </div>
	                <div class="plugin_midside zone_<?php echo $data['usedin']?>">
	                    <img id="plugin_zone_<?php echo $data['id']?>" src="<?php echo WEB_URL.ADMIN_FOLDER.CORE_FOLDER."images/general/plugin_".$data['usedin'].".png" ?>" alt="<?php echo $data['usedin']?>" title="<?php echo $data['usedin']?>" />
	                </div>
	                <div class="plugin_rightside col-lg-9">
	                    <span id="plugin_descr_<?php echo $data['id']?>" class="plugin_descr"><?php echo $data['descr']?></span><br/>
	                    <a id="plugin_more_<?php echo $data['id']?>" href="#" class="plugin_more">More</a>
	                    <?php if($data['website'] != '') {?>&nbsp;|&nbsp;<a href="<?php echo $data['website']?>" class="plugin_links" target="_blank">Visit plugin website</a><?php } ?>
	                    <?php if(file_exists(SITE_PATH.$data['folder'].'readme.txt')) {?>&nbsp;|&nbsp;<a href="#" class="plugin_help_link" data-help-file="<?php echo str_replace(SITE_PATH, "", $data['folder']).'readme.txt'?>" rel="pluginhelp">Documentation</a><?php } ?>
	                    <?php if($data['settingsfunc'] != '' && $data['active'] > 0) {?>&nbsp;|&nbsp;<a href="#" class="plugin_settings_link" rel="<?php echo $data['settingsfunc']?>|<?php echo $data['folder'].$data['initfile'];?>|<?php echo $data['id'] ?>"><b>Settings</b></a><?php } ?>
	                    <br/>
	                    <div id="plugin_info_<?php echo $data['id']?>" class="plugin_info col-lg-10"></div>
	                </div>
	            </div>
	            <?php $rtn = ob_get_clean();

	    }elseif($type == 'prob'){
	        $fix_link   = '';
	        $undel_link = '';
	        $scrap_link = '';
	        $note       = '';
	        if($data['errors'] != ''){
	            $note = 'The <i>plugin.info</i> file has one or more configuration errors that prevents it from being fully installed.  Click "<strong>More</strong>" to view and/or repair the problem.';
	        }elseif($data['is_deleted'] == 1){
	            $note = 'This plugin was deleted from within '.SYS_NAME.'.';
	            if($_users->userIsAllowedTo('delete_plugins')){
	                $undel_link = '<a href="#" class="plugin_undel" rel="'.$data['id'].'">Restore</a>';
	                $scrap_link = '&nbsp;|&nbsp;<a href="#" class="plugin_scrap" rel="'.$data['id'].'">Scrap It</a>';
	            }
	        }elseif($data['updflag'] == -1){
	            $note = 'This plugin was not found in its registered installation folder.  If the <i>plugin.info</i> file cannot be recovered, you will have to re-install the plugin.';
	        }

	        ob_start();
	        ?>
	            <div id="plugin_row_<?php echo $data['id']?>" class="plugin_row settings_hover_row">
	                <input type="hidden" id="plugin_id_<?php echo $data['id']?>" name="plugin_id[]" class="plugin_id" value="<?php echo $data['id']?>"/>
	                <input type="hidden" id="plugin_slug_<?php echo $data['id']?>" name="plugin_slug[]" class="plugin_slug" value="<?php echo codify($data['name'])?>"/>
	                <input type="hidden" id="plugin_title_<?php echo $data['id']?>" name="plugin_title[]" class="plugin_title" value="<?php echo $data['name']?>"/>
	                <div class="plugin_leftside col-lg-2">
	                        <span id="plugin_name_prob_<?php echo $data['id']?>" class="plugin_name"><?php echo $data['name'].(($data['ver'] != '') ? ' v. '.$data['ver'] : '')?></span><br/>
	                        <span id="plugin_actions_prob_<?php echo $data['id']?>" class="plugin_actions action_items"><?php echo $fix_link.$undel_link.$scrap_link?></span>
	                </div>
	                <div class="plugin_rightside col-lg-9">
	                    <span id="plugin_descr_prob_<?php echo $data['id']?>" class="plugin_descr"><?php echo $note?></span><br/>
	                    <?php if(strpos($note, 'more') > 0){ ?>
	                    <a id="plugin_more_prob_<?php echo $data['id']?>" href="#" class="plugin_more prob">More</a>
	                    <?php } ?>
	                    <br/>
	                    <div id="plugin_info_prob_<?php echo $data['id']?>" class="plugin_info prob col-lg-10"></div>
	                </div>
	            </div>
	        <?php
	        $rtn = ob_get_clean();

	    }elseif($type == 'framework'){
	        $act_link = '';
	        $act_class= '';
	        if($_users->userIsAllowedTo('activate_frameworks')){
	            if($data['active'] == 0){
	                $act_class = ' notactive';
	                if($data['nodisable'] == 0) $act_link = '<a href="#" class="plugin_act">Activate</a>';
	            }else{
	                $act_class = '';
	                if($data['nodisable'] == 0) $act_link = '<a href="#" class="plugin_act">Deactivate</a>';
	            }
	        }
	        ob_start();
	        ?>
	        <div id="plugin_row_<?php echo $data['id']?>" class="plugin_row settings_hover_row">
	            <input type="hidden" id="plugin_id_<?php echo $data['id']?>" name="plugin_id[]" class="plugin_id" value="<?php echo $data['id']?>"/>
	            <input type="hidden" id="plugin_slug_<?php echo $data['id']?>" name="plugin_slug[]" class="plugin_slug" value="<?php echo codify($data['name'])?>"/>
	            <input type="hidden" id="plugin_title_<?php echo $data['id']?>" name="plugin_title[]" class="plugin_title" value="<?php echo $data['name']?>"/>
	            <div class="plugin_leftside col-lg-2">
	                <?php if($act_link != ''){?><input type="checkbox" id="plugin_check_fw_<?php echo $data['id']?>" class="plugin_checks" /><?php } ?>
	                <span id="plugin_name_fw_<?php echo $data['id']?>" class="plugin_name<?php echo $act_class?>"><?php echo $data['name'].(($data['ver'] != '') ? ' v. '.$data['ver'] : '')?></span><br/>
	                <span id="plugin_actions_fw_<?php echo $data['id']?>" class="plugin_actions action_items"><?php echo $act_link?></span>
	            </div>
	            <div class="plugin_midside">
	            </div>
	            <div class="plugin_rightside col-lg-9">
	                <span id="plugin_descr_fw_<?php echo $data['id']?>" class="plugin_descr"><?php echo $data['descr']?></span><br/>
	                <a id="plugin_more_fw_<?php echo $data['id']?>" href="#" class="plugin_more">More</a>
	                <?php if($data['website'] != '') {?>&nbsp;|&nbsp;<a href="<?php echo $data['website']?>" id="plugin_links_fw_<?php echo $data['id']?>" class="plugin_links" target="_blank">Visit framework website</a><?php } ?>
	                <br/>
	                <div id="plugin_info_fw_<?php echo $data['id']?>" class="plugin_info col-lg-10"></div>
	            </div>
	        </div>
	        <?php  $rtn = ob_get_clean();

	    }elseif($type == 'extension'){
	        $act_link = '';
	        $del_link = '';
	        $act_class= '';
	        if($data['is_editor'] == 0){
	            if($_users->userIsAllowedTo('activate_plugins')){
	                if($this->pluginCanActivate($data)){
	                    if($data['active'] == 0){
	                        $act_class = ' notactive';
	                        if($data['nodisable'] == 0) $act_link = '<a href="#" class="plugin_act" rel="'.$data['id'].'">Activate</a>';
	                    }else{
	                        $act_class = '';
	                        if($data['nodisable'] == 0) $act_link = '<a href="#" class="plugin_act" rel="'.$data['id'].'">Deactivate</a>';
	                    }
	                }else{
	                    if($data['nodisable'] == 0) $act_link = '<a href="#" class="plugin_act blocked" rel="'.$data['id'].'">Can\'t Activate</a>';
	                    $act_class = ' notactive red';
	                }
	            }
	            if($data['nodelete'] == 0 && $_users->userIsAllowedTo('delete_plugins')){
	                $del_link = (($act_link != '') ? '&nbsp;|&nbsp;' : '').'<a href="#" class="plugin_del" rel="'.$data['id'].'">Delete</a>';
	            }
	        }else{
	            $act_link = '[Switch through Editors tab]';
	        }

	        ob_start();
	        ?>
	            <div id="plugin_row_<?php echo $data['id']?>" class="plugin_row settings_hover_row">
	                <input type="hidden" id="plugin_id_<?php echo $data['id']?>" name="plugin_id[]" class="plugin_id" value="<?php echo $data['id']?>"/>
	                <input type="hidden" id="plugin_slug_<?php echo $data['id']?>" name="plugin_slug[]" class="plugin_slug" value="<?php echo codify($data['name'])?>"/>
	                <input type="hidden" id="plugin_title_<?php echo $data['id']?>" name="plugin_title[]" class="plugin_title" value="<?php echo $data['name']?>"/>
	                <div class="plugin_leftside col-lg-2">
	                    <?php if(file_exists(SITE_PATH.$data['folder'].'pluginicon.png')) echo '<img src="'.WEB_URL.$data['folder'].'pluginicon.png'.'" alt="'.$data['name'].'" class="plugin_icon" />'; ?>
	                    <?php if($act_link.$del_link != '' && $data['is_editor'] == 0) { ?><input type="checkbox" id="plugin_check_<?php echo $data['id']?>" class="plugin_checks" /><?php } ?>
	                    <span id="plugin_name_<?php echo $data['id']?>" class="plugin_name<?php echo $act_class?>"><?php echo $data['name'].(($data['ver'] != '') ? ' v. '.$data['ver'] : '')?></span><br/>
	                    <span id="plugin_actions_<?php echo $data['id']?>" class="plugin_actions action_items"><?php echo $act_link.$del_link ?></span>
	                </div>
	                <div class="plugin_midside zone_<?php echo $data['usedin']?>">
	                    <img id="plugin_zone_<?php echo $data['id']?>" src="<?php echo WEB_URL.ADMIN_FOLDER.CORE_FOLDER."images/general/plugin_".$data['usedin'].".png" ?>" alt="<?php echo $data['usedin']?>" title="<?php echo $data['usedin']?>" />
	                </div>
	                <div class="plugin_rightside col-lg-9">
	                    <span id="plugin_descr_<?php echo $data['id']?>" class="plugin_descr"><?php echo $data['descr']?></span><br/>
	                    <a id="plugin_more_<?php echo $data['id']?>" href="#" class="plugin_more">More</a>
	                    <?php if($data['website'] != '') {?>&nbsp;|&nbsp;<a href="<?php echo $data['website']?>" class="plugin_links" target="_blank">Visit plugin website</a><?php } ?>
	                    <?php if(file_exists(SITE_PATH.$data['folder'].'readme.txt')) {?>&nbsp;|&nbsp;<a href="#" class="plugin_help_link" data-help-file="<?php echo str_replace(SITE_PATH, "", $data['folder']).'readme.txt'?>" rel="pluginhelp">Documentation</a><?php } ?>
	                    <?php if($data['settingsfunc'] != '' && $data['active'] > 0) {?>&nbsp;|&nbsp;<a href="#" class="plugin_settings_link" rel="<?php echo $data['settingsfunc']?>|<?php echo $data['folder'].$data['initfile'];?>|<?php echo $data['id'] ?>"><b>Settings</b></a><?php } ?>
	                    <br/>
	                    <div id="plugin_info_<?php echo $data['id']?>" class="plugin_info col-lg-10"></div>
	                </div>
	            </div>
	            <?php $rtn = ob_get_clean();

	    }
	    return $rtn;
	}

	/**
	 * Output plugin settings tab contents
	 * @internal Core function
	 */
	function showSettingsPluginsSettings(){
	    global $_system;

	    $count = 0;
	    foreach($_system->plugins as $plugin_name => $data){
	        if($data['active'] > 0 && $data['settingsfunc'] != '' && $data['is_deleted'] == 0){
	            $rtn = $this->getSettingsPluginSettingsRow($data);
	            echo $rtn;
	            if($rtn != '') $count++;
	        }
	    }

	    if($count == 0){ ?>
	    <p>No plugin settings are available.</p>
	    <?php
	    }
	}

	/**
	 * Return single plugin settings block
	 * @internal Core function
	 * @param array $data
	 */
	function getSettingsPluginSettingsRow($data){
	    $imgfolder = str_replace(SITE_PATH, WEB_URL, $data['folder']);

	    ob_start();
	    ?>
	    <div class="plugin_tile" id="plugin_setting_<?php echo codify($data['name']) ?>">
	        <a href="#" class="plugin_settings_link" rel="<?php echo $data['settingsfunc']?>|<?php echo $data['folder'].$data['initfile'];?>">
	            <?php
	            if(file_exists($data['folder']."icon.png")){
	                echo '<img src="'.$imgfolder.'icon.png" alt="'.$data['name'].'" /><br/>';
	            }else{
	                echo '<img src="'.WEB_URL.ADMIN_FOLDER.CORE_FOLDER.'images/general/no-pic.gif" alt="" /><br/>';
	            }
	            echo $data['name'];
	            ?>
	        </a>
	    </div>
	    <?php
	    $rtn = ob_get_clean();
	    return $rtn;
	}

	/**
	 * Return plugins Updates contents
	 * @internal Core function
	 */
	function showSettingsPluginsUpdates(){

	}

	/**
	 * Return plugins Repository contents
	 * @internal Core function
	 */
	function showSettingsPluginsRepository(){

	}

	/**
	 * Return the "More" pop-out contents
	 * @param boolean $id               Plugin id
	 * @internal Core function
	 * @return boolean
	 */
	function getSettingsPluginsMoreInfo($id, $val = null){
	    global $_db_control, $_users;

	    $rtn = null;
	    if($id > 0){
	        $dataarry = $_db_control->setTable(PLUGINS_TABLE)->setWhere("id='{$id}'")->setLimit()->getRec();
	        $rtn = '';
	        $icon = '';
	        if(empty($val)){
	            if($dataarry[0]['is_framework'] == 0){
	                $keys = array('descr'=>'Description', 'author'=>'Author', 'incl'=>'Inclusion verb',
	                                'ver'=>'Version', 'sysver'=>'Min. system version required', 'license'=>'License',
	                                'created'=>'Created', 'revised'=>'Revised', 'website'=>'Website', 'usedin'=>'Usage zone',
	                                'depends'=>'Depends on', 'dependants'=>'Dependants', 'group'=>'Group',
	                                'parent'=>'Parent',
	                                'folder'=>'Located', 'initfile'=>'Core file', 'initcallers'=>'Initialized in', 'initcontexts'=>'Init. context',
	                                'headerfunc'=>'Header function', 'settingsfunc'=>'Settings function', 'nodelete'=>'Can be deleted?',
	                                'nodisable'=>'Can be disabled?', 'builtin'=>'System extension?', 'autoincl' => 'Auto-inclusion?',
	                                'active'=>'Status');
	            }else{
	                $keys = array('descr'=>'Description', 'incl'=>'Inclusion verb',
	                                'ver'=>'Version', 'sysver'=>'Min. system version required', 'website'=>'Website',
	                                'depends'=>'Depends on', 'dependants'=>'Dependants', 'initfile'=>'Loader file',
	                                'nodisable'=>'Can be disabled?', 'builtin'=>'System extension?', 'autoincl' => 'Auto-inclusion?',
	                                'active'=>'Status');
	            }
	            $dataarry[0]['dependants'] = $this->getPluginDependants($dataarry[0]['incl'], AS_STRING, true);

                if(file_exists(SITE_PATH.$dataarry[0]['folder'].'pluginicon.png')) $icon = '<img src="'.WEB_URL.$dataarry[0]['folder'].'pluginicon.png'.'" alt="'.$dataarry[0]['name'].'" class="plugin_icon" />&nbsp;';
	            $rtn .= '<h3 class="biggerfont blue">'.$icon.$dataarry[0]['name'].'</h3>';
	            foreach($keys as $key => $title){
	                if(isBlank($dataarry[0][$key])) $dataarry[0][$key] = null;
	                $data = $dataarry[0][$key];
	                $text = '';
	                switch($key){
	                    case 'created':
	                    case 'revised':
	                        if(isDate($data)) $text = date('M j, Y', strtotime($data));
	                        break;
	                    case 'sysver':
	                        if(version_compare($data, CODE_VER) <= 0)
	                            $text = $data . '&nbsp;[<span class="green">Ok</span>]';
	                        else
	                            $text = $data . '&nbsp;[<span class="red">Plugin Not Compatible</span>]';
	                        break;
	                    case 'builtin':
	                    case 'autoincl':
	                        $text = (($data == 1) ? 'Yes' : 'No');
	                        break;
	                    case 'usedin':
	                    case 'license':
	                        $text = ucwords($data);
	                        break;
	                    case 'active':
	                        $text = (($data == 1) ? 'Active' : 'Not Active');
	                        break;
	                    case 'nodelete':
	                    case 'nodisable':
	                        $text = (($data == 1) ? 'No' : 'Yes');
	                        break;
	                    case 'folder':
	                        $text = str_replace(SITE_PATH, '', $data);
	                        break;
	                    case 'incl':
	                        $text = ((empty($data) || $dataarry[0]['builtin'] == 1) ? 'Not required, built-in extension' : $data);
	                        break;
	                    case 'initcallers':
	                        $text = join("; ", explode(",", $data));
	                        break;
	                    case 'initcontexts':
	                    	$text = $this->parseInitContexts($data, 'string');
	                    	break;
	                    case 'depends':
	                        $depends = explode(",", $data);
	                        foreach($depends as $depend){
	                            $depend = trim($depend);
	                            $text .= (($text != '') ? ", " : "").((empty($depend)) ? 'None' : $depend);
	                            if($text != 'None'){
	                                $status = $this->getPluginStatus($depend);
	                                $act_link = (($this->pluginCanActivate($depend)) ? '<a class="plugin_act proxy" rel="'.$this->getPluginInfo($depend, 'id').'" href="#">activate</a>' : 'cannot be activated');
	                                if($status == 'active')
	                                    $text .= '&nbsp[<span class="green">'.$status.'</span>]';
	                                elseif($status == 'inactive')
	                                    $text .= '&nbsp[<span class="gray">'.$status.' - '.$act_link.'</span>]';
	                                elseif($status == 'problem')
	                                    $text .= '&nbsp[<span class="red">'.$status.'</span>]';
	                            }
	                        }
	                        break;
	                    case 'dependants':
	                        $text = ((empty($data)) ? 'None' : $data);
	                        break;
	                    case 'parent':
	                        $text = $data;
	                        $status = $this->getPluginStatus($data);
	                        $act_link = (($this->pluginCanActivate($data)) ? '<a class="plugin_act proxy" rel="'.$this->getPluginInfo($data, 'id').'" href="#">activate</a>' : 'cannot be activated');
	                        if($status == 'active')
	                            $text .= '&nbsp[<span class="green">'.$status.'</span>]';
	                        elseif($status == 'inactive')
	                            $text .= '&nbsp[<span class="gray">'.$status.' - '.$act_link.'</span>]';
	                        elseif($status == 'problem')
	                            $text .= '&nbsp[<span class="red">'.$status.'</span>]';
	                        break;
	                    case 'ver':
	                        if(!isBlank($dataarry[0]['inline_settings']) && $_users->userIsAllowedTo('update_plugins')){
	                            $cfgs = explode(",", $dataarry[0]['inline_settings']);
	                            if(in_array($key, $cfgs)){
	                                $text = '<input type="text" class="plugin_datamod smallfldsize" rel="'.$dataarry[0]['id'].'|'.$key.'" value="'.$data.'" />';
	                            }else{
	                                $text = $data;
	                            }
	                        }else{
	                            $text = $data;
	                        }
	                        break;
	                    case 'website':
	                        $text = '<a href="'.$data.'" target="_blank">'.$data.'</a>';
	                        break;
	                    default:
	                        $text = $data;
	                        break;
	                }
	                if(!empty($text)) $rtn .= ((!empty($rtn)) ? '<br/>' : '').'<strong>'.$keys[$key].':</strong>&nbsp;'.$text;
	            }
	        }elseif($val == 'prob'){
	            if($dataarry[0]['error_code'] != PLUGIN_CFGERR_REGPATHNF){
	                $rtn = "&bull;&nbsp;".str_replace("<br/>", "<br/>&bull;&nbsp;", $dataarry[0]['errors']);
	                $rtn.= "<br/>[PE".$dataarry[0]['error_code']."]";
	                if($_users->userIsAllowedTo('repair_plugins')){
	                    $rtn.= "<p>".SYS_NAME." will try to correct the following problematic entrie(s) in the <i>plugin.info</i> file. If the repair does not work, you will need to correct the issue(s) manually.</p>";
	                    $rtn.= $this->getSettingsPluginRepairForm($id);
	                }
	            }else{
	                $rtn = "&bull;&nbsp;".str_replace("<br/>", "<br/>&bull;&nbsp;", $dataarry[0]['errors']);
	                $rtn.= "<br/>[PE".$dataarry[0]['error_code']."]<br/><br/>";
	                $rtn.= "<span style=\"font-weight: bold\">The registered path, <em>".SITE_PATH.$dataarry[0]['folder']."</em>, no longer exists.  ";
	                if($_users->userIsAllowedTo('delete_plugins'))
	                    $rtn.= "You may either <a href=\"#\" class=\"plugin_scrap\" rel=\"{$dataarry[0]['id']}\">scrap</a> the plugin completely, or check the plugin path and files for any problems.</span>";
	                else
	                    $rtn.= "You should check the plugin path and files for any problems.</span>";
	            }
	        }
	    }
	    return $rtn;
	}

	/**
	 * Return the settings repair html form
	 * @param boolean $plugin_id
	 * @internal Core function
	 * @return boolean
	 */
	function getSettingsPluginRepairForm($plugin_id){
	    global $_db_control;
	    $html = '';
	    if($plugin_id > 0){
	        $plugin_data = $_db_control->setTable(PLUGINS_TABLE)->setWhere("id = '$plugin_id'")->setLimit()->getRec();
	        if(count($plugin_data) == 1){
	            $errcode = $plugin_data[0]['error_code'];
	            $html = '
	            <form id="plugin_repair_form" method="post" action="">
	                <input type="hidden" name="plugin_id" class="plugin_repair_plugin_id" value="'.$plugin_id.'"/>';

	            $line1  = '<p style="padding-left: 20px;"><b>Problem: %s</b><br/>';
	            $line2t = '%s: <input type="text" name="%s" id="%s" value="%s" />%s';
	            $line2m = '%s: <select name="%s" id="%s">';
	            $line2mo= '<option value="%s"%s>%s</option>';
	            $line2mc= '</select>%s';
	            if($errcode & PLUGIN_CFGERR_NAMEDUP){
	                $html .= sprintf($line1, "The NAME value '{$plugin_data[0]['name']}' already exists");
	                $html .= sprintf($line2t, "New Name", "name", "name", $plugin_data[0]['name'], '');
	            }
	            if($errcode & PLUGIN_CFGERR_USEDINBAD){
	                $html .= sprintf($line1, "The USEDIN value is invalid");
	                $html .= sprintf($line2m, "Usage Zone", "usedin", "usedin");
	                $html .= sprintf($line2mo, "admin", "", "Admin");
	                $html .= sprintf($line2mo, "front", "", "Front");
	                $html .= sprintf($line2mo, "both", "", "Both");
	                $html .= sprintf($line2mc, "");
	            }
	            if($errcode & PLUGIN_CFGERR_LICENSEBAD){
	                $html .= sprintf($line1, "The LICENCE value is invalid");
	                $html .= sprintf($line2m, "License", "license", "License");
	                $html .= sprintf($line2mo, "", "", "- n/a -");
	                $html .= sprintf($line2mo, "free", "", "Free");
	                $html .= sprintf($line2mo, "trial", "", "Trial");
	                $html .= sprintf($line2mo, "limited", "", "Limited");
	                $html .= sprintf($line2mo, "full", "", "Full");
	                $html .= sprintf($line2mo, "subscription", "", "Subscription");
	                $html .= sprintf($line2mc, "");
	            }
	            if($errcode & PLUGIN_CFGERR_SETTINGSFUNC){
	                $html .= sprintf($line1, "The SETTINGSFUNC value '{$plugin_data[0]['settingsfunc']}' already exists");
	                $html .= sprintf($line2t, "New Settings Function", "settingsfunc", "settingsfunc", $plugin_data[0]['settingsfunc'], '');
	            }
	            if($errcode & PLUGIN_CFGERR_HEADERFUNC){
	                $html .= sprintf($line1, "The HEADERFUNC value '{$plugin_data[0]['headerfunc']}' already exists");
	                $html .= sprintf($line2t, "New Header Function", "headerfunc", "headerfunc", $plugin_data[0]['headerfunc'], '');
	            }
	            if($errcode & PLUGIN_CFGERR_INCLDUP){
	                $html .= sprintf($line1, "The INCL value '{$plugin_data[0]['incl']}' already exists");
	                $html .= sprintf($line2t, "New Inclusion Verb", "incl", "incl", $plugin_data[0]['incl'], '');
	            }
	            if($errcode & PLUGIN_CFGERR_INCLBAD){
	                $html .= sprintf($line1, "The INCL value '{$plugin_data[0]['incl']}' contains invalid characters");
	                $html .= sprintf($line2t, "New Inclusion Verb", "incl", "incl", $plugin_data[0]['incl'], '');
	            }
	            if($errcode & PLUGIN_CFGERR_NAMENF){
	                $html .= sprintf($line1, "The NAME value is missing");
	                $html .= sprintf($line2t, "Plugin Name", "name", "name", '', '');
	            }
	            if($errcode & PLUGIN_CFGERR_SYSVERNF){
	                $html .= sprintf($line1, "The SYSVER value is missing");
	                $html .= sprintf($line2t, "Min. System Version", "sysver", "sysver", '', 'Current system version is '.CODE_VER);
	            }
	            if($errcode & PLUGIN_CFGERR_USEDINNF){
	                $html .= sprintf($line1, "The USEDIN value is invalid");
	                $html .= sprintf($line2m, "Usage Zone", "usedin", "usedin");
	                $html .= sprintf($line2mo, "admin", "", "Admin");
	                $html .= sprintf($line2mo, "front", "", "Front");
	                $html .= sprintf($line2mo, "both", "", "Both");
	                $html .= sprintf($line2mc, "");
	            }
	            if($errcode & PLUGIN_CFGERR_INCLNF){
	                $html .= sprintf($line1, "The INCL value is missing");
	                $html .= sprintf($line2t, "Inclusion Verb", "incl", "incl", '', '');
	            }
	            if($errcode & PLUGIN_CFGERR_INITFILENF){
	                $html .= sprintf($line1, "The INITFILE file is missing");
	                $html .= sprintf($line2t, "Core File", "initfile", "initfile", '', '');
	            }
	            if($errcode & PLUGIN_CFGERR_INITFILEBAD){
	                $html .= sprintf($line1, "The INITFILE file '{$plugin_data[0]['initfile']}' was not found");
	                $html .= sprintf($line2t, "Core File", "initfile", "initfile", $plugin_data[0]['initfile'], '');
	            }
	            if($errcode & PLUGIN_CFGERR_REGPATHNF){
	                $html .= sprintf($line1, "");
	            }

	            $html.= '<br/><br/><input type="submit" id="plugin_fix_submit" value="Repair" />
	                    </p>
	            </form>';
	        }else{
	            $html = '
	            Uh Oh!  The requested plugin data could not be retrieved from the database.';
	        }
	    }
	    return $html;
	}

	/**
	 * Repair the plugin using data from the settings repair html form
	 * @param boolean $plugin_id
	 * @internal Core function
	 * @return boolean
	 */
	function doSettingsPluginRepair($plugin_id){
	    global $_db_control, $_filesys, $_settings, $_events;

	    $rtnval = false;
	    $rtnstr = null;
		list($abort) = $_events->processTriggerEvent(__FUNCTION__, $plugin_id, false);				// alert triggered functions when function executes

	    if($plugin_id > 0 && !$abort){
	        $plugin_data = $_db_control->setTable(PLUGINS_TABLE)->setWhere("id = '{$plugin_id}'")->setLimit()->getRec();
	        $msg = array();
	        if(count($plugin_data) == 1){
	            // get plugins field array (name[], license[], incl[], sysver[]...)
	            $field_array = $_db_control->setTable(PLUGINS_TABLE)->setWhere("id != '{$plugin_id}'")->setLimit("")->getRecFieldArray();
	            foreach($field_array as $key => $sub_array){
	                foreach($sub_array as $sub_key => $value) {
	                    if(!empty($value) && !is_null($value)) {
	                        $field_array[$key][$sub_key] = strtolower($value);
	                    } else {
	                        unset($field_array[$key][$sub_key]);
	                    }
	                }
	            }

	            // get error code array (plugin_cfgerr_...)
	            $errcode = $plugin_data[0]['error_code'];
	            $errcode_array = getConsts('PLUGIN_CFGERR');
	            foreach($errcode_array as $code => $codeval){
	                $field_from_code = str_replace(array("plugin_cfgerr_", "dup", "nf", "bad"), "", strtolower($code));
	                $postvar = getIfSet($plugin_data[0][$field_from_code]);
	                if(($errcode & $codeval) > 0){
	                    switch(strtoupper($code)){
	                        case "PLUGIN_CFGERR_NAMEDUP":
	                        case "PLUGIN_CFGERR_NAMENF":
	                        case "PLUGIN_CFGERR_HEADERFUNC":
	                        case "PLUGIN_CFGERR_SETTINGSFUNC":
	                        case "PLUGIN_CFGERR_INCLDUP":
	                        case "PLUGIN_CFGERR_INCLNF":
	                        case "PLUGIN_CFGERR_INCLBAD":
	                            if(in_array(strtolower($postvar), $field_array[$field_from_code])){
	                                $msg[] = strtoupper($field_from_code)." '$postvar' already exists.\n";
	                            }elseif(preg_match('/([^a-z0-9\_\- ])/i', $postvar)){
	                                $msg[] = strtoupper($field_from_code)." can only consist of letters, numbers, space, hyphen and underscore.\n";
	                            }else{
	                                $errcode -= constant(strtoupper($code));
	                            }
	                            break;
	                        case "PLUGIN_CFGERR_SYSVERNF":
	                            $postvar_d = convertCodeVer2Dec($postvar);
	                            if(empty($postvar)){
	                                $msg[] = strtoupper($field_from_code)." cannot be blank.\n";
	                            }elseif(preg_match('/([^0-9\.])/i', $postvar)){
	                                $msg[] = strtoupper($field_from_code)." can only consist of numbers and decimal point.\n";
	                            }elseif($postvar_d < 2.06 or $postvar_d > getCodeVer()){
	                                // $msg[] = strtoupper($field_from_code)." must be between 2.6.0 and ".CODE_VER.".\n";
	                            }else{
	                                $errcode -= constant(strtoupper($code));
	                            }
	                            break;
	                        case "PLUGIN_CFGERR_LICENSEBAD":
	                            if(!in_array($postvar, array('free', 'trial', 'limited', 'full', 'subscription')) && !empty($postvar)){
	                                $msg[] = strtoupper($field_from_code)." '$postvar' is still invalid.\n\nThe only valid options are 'free', 'trial', 'limited', 'full', and 'subscription'.\n";
	                            }else{
	                                $errcode -= constant(strtoupper($code));
	                            }
	                            break;
	                        case "PLUGIN_CFGERR_USEDINBAD":
	                        case "PLUGIN_CFGERR_USEDINNF":
	                            if(empty($postvar)){
	                                $msg[] = strtoupper($field_from_code)." was not chosen.\n";
	                            }elseif(!in_array($postvar, array('admin', 'front', 'both')) && !empty($postvar)){
	                                $msg[] = strtoupper($field_from_code)." '$postvar' is still invalid.\n\nThe only valid zones are 'both', 'admin', or 'front'.\n";
	                            }else{
	                                $errcode -= constant(strtoupper($code));
	                            }
	                            break;
	                        case "PLUGIN_CFGERR_INITFILENF":
	                        case "PLUGIN_CFGERR_INITFILEBAD":
	                            if(empty($postvar)){
	                                $msg[] = strtoupper($field_from_code)." cannot be blank.\n";
	                            }elseif(!file_exists($plugin_data[0]['folder'].$postvar)){
	                                $msg[] = strtoupper($field_from_code)." '$postvar' was not found in plugin folder.\n\nEither enter the file name that exists or create an info\nfile with that name.";
	                            }else{
	                                $errcode -= constant(strtoupper($code));
	                            }
	                            break;
	                    }
	                }
	            }

	            $msg = array_unique($msg);
	            if($errcode > 0 || count($msg) > 0){
	                // we still have a problem
	                $rtnstr = "- ".join("- ", $msg);
	            }else{
	                // everything is ok now
	                // save data back to file
	                $file = $plugin_data[0]['folder']."plugin.info";
	                $fcontents = file_get_contents($file);
	                $flines = preg_split("/(\n)/i", $fcontents);
	                $fnewcontents = "";
	                foreach($flines as $fline){
	                    $fline_parts = explode(":", $fline, 2);
	                    $fline_off = ((substr($fline_parts[0], 0, 2) == '//') ? '//' : '');
	                    $field = strtolower(str_replace(array("#", "/"), "", $fline_parts[0]));
	                    $value = trim($fline_parts[1]);
	                    if(!empty($field) && !empty($value)){
	                        if(isset(${$field})) $value = ${$field};
	                        $fnewcontents .= $fline_off."#".$field.":".str_repeat(" ", (13-strlen($field))).$value.PHP_EOL;
	                    }
	                }
	                $_filesys->chmod2($file);
	                @file_put_contents($file, $fnewcontents);

	                // and update the database
	                $_settings->clearSettingsIssues();
	                $this->getInstalledPlugins();

	                // and return the plugin row to the js process
	                $plugin_data = $_db_control->setTable(PLUGINS_TABLE)->setWhere("id = '{$plugin_id}'")->setLimit()->getRec();
	                $rtn = array();
	                $rtn['row'] = $this->getSettingsPluginRow($plugin_data[0], 'normal');
	                $rtn['setting'] = ((!isBlank($plugin_data[0]['settingsfunc'])) ? $this->getSettingsPluginSettingsRow($plugin_data[0]) : '');
	                $rtn['plugins_issues'] = $_settings->showSettingsIssues('plugins', true);
	                $rtn['settings_issues'] = $_settings->showSettingsIssues('', true);
	                $rtnval = true;
	                $rtnstr = $rtn;
	            }
	        }else{
	            $rtnstr = "The plugin was not found in the database.";
	        }
	    }
		$_events->processTriggerEvent(__FUNCTION__.'_done', array($plugin_id, $rtnval));				// alert triggered functions when function executes
	    return array($rtnval, $rtnstr);
	}

	/**
	 * Return either array or comma-separated list of verbs of plugins that are dependant on subject plugin
	 * @param string $verb              The verb/id of the subject plugin (parent)
	 * @param integer $returnas         Form of return value (AS_ARRAY, AS_STRING)
	 * @param boolean $showstatus       Show active state
	 * @param boolean $onlyactive       Set to true to limit results to plugins that are currently active
	 * @return mixed                    Array or string
	 */
	function getPluginDependants($verb, $returnas = AS_ARRAY, $showstatus = false, $onlyactive = false) {
	    global $_db_control;

	    if(empty($verb)) return null;
	    $verb = trim($verb);
	    $dataarry = $_db_control->setTable(PLUGINS_TABLE)->setWhere("(`depends` REGEXP '(, |^){$verb}(, |$)' OR `parent` = '{$verb}')".(($onlyactive) ? ' AND `active` = 1' : ''))->getRec();
	    if($returnas == AS_ARRAY) return $dataarry;

	    $data = null;
	    if(!empty($dataarry)){
	        foreach($dataarry as $plugindata) {
	            if($showstatus){
	                $child_label = (($plugindata['parent'] = $verb) ? 'child -  ' : '');
	                $status = (($plugindata['error_code'] == 0) ? (($plugindata['active'] == 1) ? '&nbsp;['.$child_label.'<span class="green">active</span>]' : '&nbsp;['.$child_label.'<span class="gray">inactive</span>]') : '&nbsp;['.$child_label.'<span class="red">problem</span>]');
	            }else{
	                $status = null;
	            }
	            $data .= ((!is_null($data)) ? ', ' : '').$plugindata['incl'].$status;
	        }
	    }
	    return $data;
	}

	/**
	 * Return verbs of plugins that are the children of subject plugin
	 * @param string $verb              The verb of the subject plugin (parent)
	 * @param boolean $onlyactive       Set to true to limit results to plugins that are currently active
	 * @return array                    Array or string
	 */
	function getPluginChildren($verb, $onlyactive = false) {
	    global $_db_control;

	    if(empty($verb)) return null;
	    $verb = trim($verb);
	    $dataarry = $_db_control->setTable(PLUGINS_TABLE)->setWhere("(`parent` = '{$verb}')".(($onlyactive) ? ' AND `active` = 1' : ''))->getRec();
	    return $dataarry;
	}

	/**
	 * Return the state of a plugin (active, inactive, or problem)
	 * @param string $verb              The verb of the subject plugin
	 * @return string
	 */
	function getPluginStatus($verb){
	    global $_db_control;

	    if(empty($verb)) return null;
	    $verb = trim($verb);
	    $dataarry = $_db_control->setTable(PLUGINS_TABLE)->setWhere("`incl` = '{$verb}'")->setLimit()->getRec();
	    if(!empty($dataarry))
	        return (($dataarry[0]['error_code'] == 0) ? (($dataarry[0]['active'] == 1) ? 'active' : 'inactive') : 'problem');
	    else
	        return null;
	}

	/**
	 * Return whether or not a plugin can be activated
	 * @param mixed $data               Plugin's ID or plugin object
	 * @return boolean                  True if successful
	 * @return boolean
	 */
	function pluginCanActivate($data){
	    global $_system;

	    $ok = false;
	    if(!is_array($data)){
	        $val = $data;
	        $data = array();
	        foreach($_system->plugins as $key => $plugin){
	            if($plugin['id'] == $val && is_integer($val)){
	                $data = $plugin;
	                break;
	            }elseif($plugin['incl'] == $val){
	                $data = $plugin;
	                break;
	            }
	        }
	    }
	    if(!empty($data)){
	        if(version_compare($data['sysver'], CODE_VER) <= 0){
	            $ok = true;
	            $depends = array();
	            if(!empty($data['depends'])) $depends = explode(",", $data['depends']);
	            if(!empty($data['parent'])) $depends[] = $data['parent'];
	            if(!empty($depends)){
	                foreach($depends as $depend){
	                    $ok = $ok & ($this->getPluginStatus($depend) == 'active');
	                }
	            }
	        }
	    }
	    return $ok;
	}

	/**
	 * Activate or deactivate a plugin.
	 * @param mixed $key  	            Plugin verb or id
	 * @param boolean $activate         True to activate
	 * @param boolean $recurse          True to deactivate all child/dependent plugins if $activate is false
	 */
	function setPluginActivation($key, $activate = true, $recurse = true){
	    global $_db_control, $_events;

	    if($this->pluginCanActivate($key) || !$activate){
            $file = getDBDumpSQL(PLUGINS_TABLE);
	        if(!is_numeric($key)) $key = $this->getPluginInfo($key, 'id');
	        if($_db_control->setTable(PLUGINS_TABLE)->setFieldvals(array("active" => $activate))->setWhere("`id`='{$key}'")->updateRec()){
	            $deps = $this->getPluginDependants($key);
                $this->delegateAction($key, (($activate) ? 'activate_plugin' : 'deactivate_plugin'));
				$_events->processTriggerEvent(__FUNCTION__, array($key, $activate));				// alert triggered functions when function executes
	            return $file;
	        }else{
	            return false;
	        }
	    }else
	        return false;
	}

	/**
	 * Add/update the list of callers for a specific plugin with the URI or alias of the current page
	 * @param mixed $data               Plugin's ID or plugin object
	 * @return boolean                  True if successful
	 */
	function pluginUpdateCallers($data){
	    global $_page, $_db_control;

	    if(is_array($data)){
	        $id = $data['id'];
	    }else{
	        $id = $data;
	        $arry = $_db_control->setTable(PLUGINS_TABLE)->setWhere("`id` = '".$id."'")->setLimit()->getRec();
	        $data = $arry[0];
	    }
	    $id = intval($id);
	    if($id <= 0) return false;

	    $callers = explode(",", $data['initcallers']);
	    if(IN_ADMIN){
	        $page = substr($_page->uri, 0, strrpos($_page->uri, "?"));
	        if(!in_array($page, $callers)) $callers[] = $page;
	    }else{
	        $page = substr($_page->alias, 0, strrpos($_page->alias, "?"));
	        if(!in_array($page, $callers)) $callers[] = $page;
	    }
	    $callers_str = join(",", array_unique(array_filter($callers)));
	    $_db_control->setTable(PLUGINS_TABLE)->setFieldvals(array("initcallers" => $callers_str))->setWhere("`id` = '".$id."'")->updateRec();
	    return true;
	}

	// ----------- OPERATION FUNCTIONS ---------------

	/**
	 * Add script/style lines as a group based on a supplied feature. Unlike the similar method, includePlugins which adds
	 * the plugin code to the PHP namespace, loadPlugin adds plugin script lines to the HTML head.
	 * @param string $script_array				An array of feature script names
 	 * @param string $param_array 				A plugin array
	 * @param boolean $in_head
	 * @version 3.0
	 */
	function loadPluginScripts($script_array, $param_array = null, $in_head = true) {
	    global $_system, $_error, $_themes, $_events;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	    $pluginsfolder = WEB_URL.ADMIN_FOLDER.PLUGINS_FOLDER;
	    $incfolder = WEB_URL.ADMIN_FOLDER.INC_FOLDER;
	    $jsfolder = WEB_URL.ADMIN_FOLDER.JS_FOLDER;
	    $corefolder = WEB_URL.ADMIN_FOLDER.CORE_FOLDER;
	    $themefolder = WEB_URL.$_themes->getThemePathUnder("admin");
	    $editorsfolder = WEB_URL.ADMIN_FOLDER.EDITOR_FOLDER;
	    $front_cssfolder = WEB_URL.$_themes->getThemePathUnder("website").CSS_FOLDER;
	    $front_jsfolder = WEB_URL.JS_FOLDER;

	    $group = array();
	    if(is_array($param_array)){
	        $is_framework = getBooleanIfSet($param_array['is_framework']);
	        $ver = getIfSet($param_array['ver']);
	        $initfile = getIfSet($param_array['initfile']);
	        $folder = getIfSet($param_array['folder']);
	    }

	    // get the base script name, and all subscripts
	    // - eg: array("jqueryui", "uiwidgets" => "droppable>fold+datepicker", "multiselect")
	    if(!is_array($script_array)) $script_array = array($script_array);

	    foreach($script_array as $key => $script_name){
		    $widgets_array = array();
		    $effects_array = array();
	    	if(is_array($script_name)){
	    		$widgets_array = $script_name;
	    		$script_name = $key;
	    	}
		    $script_name = strtolower($script_name);

		    switch($script_name){
		        // built-in modules
		        case "jquery":
		        case "jquery-min":
		            if(!preg_match("/(.+)/", getIfSet($ver))) $ver = $this->getJQueryVer();
		            if(!preg_match("/(.+)/", getIfSet($initfile))) $initfile = "jquery.min.js";
		            $group[] = array("asType" => "script", "dir" => "https://ajax.googleapis.com/ajax/libs/jquery/$ver/", "file" => $initfile, "media" => "", "fallback" => "if (typeof jQuery == 'undefined') document.write(unescape(\"%3Cscript src='{$jsfolder}jquery.min.js' type='text/javascript'%3E%3C/script%3E%3Cscript src='{$jsfolder}jquery-ui.min.js' type='text/javascript'%3E%3C/script%3E\"))", "cacheskip" => true);
		            break;
		        case "jqueryui":
		        case "jqueryui-min":
		            if(!preg_match("/(.+)/", getIfSet($ver))) $ver = $this->getJQueryUIVer();
		            if(!preg_match("/(.+)/", getIfSet($initfile))) $initfile = "jquery-ui.min.js";
		            $group[] = array("asType" => "script", "dir" => "https://ajax.googleapis.com/ajax/libs/jqueryui/$ver/", "file" => $initfile, "media" => "", "cacheskip" => true);
		            $group[] = array("asType" => "style", "dir" => $jsfolder, "file" => "ui/ui.theme.css", "media" => "screen", "cacheskip" => false);
		          	break;
		       	case "uiwidgets":
		            if(count($widgets_array) > 0){
		                $valid_widgets = explode(",", "draggable,sortable,droppable,resizable,accordion,progressbar,dialog,tabs,datepicker,nestedsortable,hashtabs");
		                foreach($widgets_array as $widget){
		                	if(strpos($widget, ">") !== false){
		                		$w = explode(">", $widget);
		                		$widget = $w[0];
		                		$effects_array[] = explode(",", $w[1]);
		                	}
		                    if(in_array($widget, $valid_widgets)){
		                        $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "ui/ui.".$widget.".js", "media" => "", "cacheskip" => false);
		                    }
		                }
		            }
		            if(count($effects_array) > 0){
		                $valid_effects = explode(",", "blind,bounce,clip,drop,explode,fade,fold,highlight,pulsate,scale,shake,slide,transfer");
		                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "ui/effects.core.js", "media" => "", "cacheskip" => false);
		                foreach($effects_array as $effect){
		                    if(in_array($effect, $valid_effects)){
		                        $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "ui/effects.".$effect.".js", "media" => "", "cacheskip" => false);
		                    }
		                }
		            }
		            if(IN_ADMIN) $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "ui/ui.script.js", "media" => "", "cacheskip" => false);
		            break;
		        case "jquery-mobile":
		        case "mobile":
		            if(!preg_match("/(.+)/", getIfSet($initfile))) $initfile = "jquery.mobile.min.js";
		            $group[] = array("asType" => "script", "dir" => "https://ajax.googleapis.com/ajax/libs/jquerymobile/$ver/", "file" => $initfile, "media" => "", "cacheskip" => true);
		            $group[] = array("asType" => "style", "dir" => "https://ajax.googleapis.com/ajax/libs/jquerymobile/$ver/", "file" => "jquery.mobile.min.css", "media" => "screen", "cacheskip" => true);
		            break;
		        case "jquery-chosen":
		        case "chosen":
	                $group[] = array("asType" => "style", "dir" => $jsfolder, "file" => "jquery.chosen.css", "media" => "screen", "cacheskip" => false);
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.chosen.js", "media" => "", "cacheskip" => false);
		        	break;
		        case "jquery-chosen-min":
		        case "chosen-min":
	                $group[] = array("asType" => "style", "dir" => $jsfolder, "file" => "jquery.chosen.css", "media" => "screen", "cacheskip" => false);
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.chosen.min.js", "media" => "", "cacheskip" => false);
		        	break;
		        case "jquery-corners":
		        case "corners":
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.corners.js", "media" => "", "cacheskip" => false);
		        	break;
		        case "jquery-corners-min":
		        case "corners-min":
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.corners.min.js", "media" => "", "cacheskip" => false);
		        	break;
		        case "jquery-cycle":
		        case "jquery-cycle-min":
		        case "cycle":
		        case "cycle-min":
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.cycle.min.js", "media" => "", "cacheskip" => false);
		        	break;
		        case "masonry":
		        case "masonry-min":
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.masonry.min.js", "media" => "", "cacheskip" => false);
		        	break;
		        case "jquery-multiselect":
		        case "multiselect":
	                $group[] = array("asType" => "style", "dir" => $jsfolder, "file" => "jquery.multiselect.css", "media" => "screen", "cacheskip" => false);
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.multiselect.js", "media" => "", "cacheskip" => false);
		        	break;
		        case "jquery-multiselect-min":
	                $group[] = array("asType" => "style", "dir" => $jsfolder, "file" => "jquery.multiselect.css", "media" => "screen", "cacheskip" => false);
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.multiselect.min.js", "media" => "", "cacheskip" => false);
		        	break;
		        case "pngfix":
		        case "pngfix-min":
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.pngFix.pack.js", "media" => "", "cacheskip" => false);
		        	break;
		       	case "swfobject":
		       	case "swfobject-min":
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "swfobject.min.js", "media" => "", "cacheskip" => false);
		       		break;
		       	case "wookmark":
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.wookmark.js", "media" => "", "cacheskip" => false);
		       		break;
		       	case "wookmark-min":
	                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "jquery.wookmark.min.js", "media" => "", "cacheskip" => false);
		       		break;

		        // basic loading here
		        case "basic":
		            if(IN_ADMIN){
		                $group[] = array("asType" => "script", "dir" => $jsfolder."ui/", "file" => "ui.script.js", "media" => "", "in_head" => true, "cacheskip" => false);
		                if(defined("IN_SETTINGS")) $group[] = array("asType" => "script", "dir" => $jsfolder."ui/", "file" => "ui.script_settings.js", "media" => "", "in_head" => true, "cacheskip" => false);
		                $group[] = array("asType" => "style", "dir" => $corefolder."uploader/inc/", "file" => "uploader.css", "media" => "screen", "in_head" => true, "cacheskip" => false);
		                $group[] = array("asType" => "script", "dir" => $corefolder."uploader/inc/", "file" => "uploader.js", "media" => "", "in_head" => true, "cacheskip" => false);
		                $group[] = array("asType" => "script", "dir" => $jsfolder, "file" => "scripts.js", "media" => "", "in_head" => false, "cacheskip" => false);
		            }
		            break;

		        // editors
		        case (CMS_EDITOR):
		            if(function_exists($param_array['headerfunc']))
		                call_user_func($param_array['headerfunc'], $folder);
		            break;

		        // other frameworks
		        case ($param_array['is_framework'] == true):
		            $group[] = array("asType" => "script", "dir" => "https://ajax.googleapis.com/ajax/libs/$script_name/$ver/", "file" => $initfile, "media" => "", "cacheskip" => true);
		            break;

		        // custom plugin header
		        //  Included either programmatically or through an admin facility
		        default:
		            if(is_array($param_array)){
		                if(isset($param_array['headerfunc'])){
		                    $func = $param_array['headerfunc'];
		                    $static_method = explode("::", $func);
		                    $public_method = explode("->", $func);
		                    if(function_exists($func)){
		                        // call the custom plugin header initiator function
		                        $_system->currentexecplugin = $script_name;
		                        call_user_func($func, $script_name);
		                    }elseif(isset($func[0]) && function_exists($func[0])){
		                        // call the custom plugin header initiator function
		                        $_system->currentexecplugin = $script_name;
		                        call_user_func($func[0], $script_name);
		                    }elseif(count($static_method) == 2 && method_exists($static_method[0], $static_method[1])){
		                        // call the custom plugin header initiator class::method
		                        $_system->currentexecplugin = $script_name;
		                        call_user_func($static_method[0].'::'.$static_method[1], $script_name);
		                    }elseif(count($public_method) == 2 && method_exists($public_method[0], $public_method[1])){
		                        // call the custom plugin header initiator class->method
		                        $_system->currentexecplugin = $script_name;
		                        $obj = new $public_method[0]();
		                        call_user_func(array($obj, $public_method[1]), $script_name);
		                    }else{
		                        $_error->addErrorMsg("Plugin headerfunc problem: Function '".SITE_PATH."{$folder}{$param_array['initfile']} >> ".$func."' not found.", CORE_ERR);
		                    }
		                }
		            }
		            return;
		    }
		    $err = $this->addScriptSourceBlock($group, $script_name, $in_head);
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Add style/script block to scriptlines property of $_page class, from line-set object.
	 * Can be called from anywhere in HTML where <script> or <style> code is allowed
	 * @param array $lineset            (type [style/script], dir, file, media, charset, fallback)
	 * @param string $groupname
	 * @param boolean $in_head          true to limit block to <HEAD>
	 * @version 3.0
	 */
	function addScriptSourceBlock($lineset, $groupname = "", $in_head = true) {
	    global $_page, $_error, $_events;

	    $err = false;
	    // add comment line to scriptlines object
	    $_page->scriptlines = array("line" => "<!-- ".strtoupper($groupname)." -->", "filepath" => "", "cacheskip" => true, "in_head" => $in_head);
	    foreach($lineset as $item) {
	        if(!isset($item['charset'])) $item['charset'] = '';
	        $err = $this->addScriptSourceLine(
	                            $item['asType'],
	                            $item['dir'],
	                            $item['file'],
	                            getIfSet($item['media']),
	                            getIfSet($item['charset']),
	                            getIfSet($item['if']),
	                            ((isset($item['in_head'])) ? (bool) $item['in_head'] : $in_head),
	                            false,
	                            getIfSet($item['fallback']),
	                            getIfSet($item['cacheskip'])
	                        );
	        if($err) {
	            $_error->addErrorMsg("Header problem: Cannot prepare script line for '$groupname'.", CORE_ERR);
	            break;
	        }
	    }
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	}

	/**
	 * Store script/style line from either addScriptSourceBlock or direct call in scriptline property of $_page class.
	 * Can be called from anywhere in HTML where <script> or <style> code is allowed
	 * @param string $asType                    style, script, meta, meta_equiv, rss, favicon
	 * @param string $dir                       Path to file, or meta name
	 * @param string $file                      File to load, or meta content
	 * @param string $media [optional]
	 * @param string $charset [optional]
	 * @param string $if [optional]             The conditional tag (ie. if lt ie8)
	 * @param boolean $in_head [optional]       True limits line to <HEAD>
	 * @param boolean $immediate [optional]     Output line immediately rather than queued
	 * @param string $fallback [optional]       Script code to run should the file fail to load
	 * @return boolean
	 * @version 3.0
	 */
	function addScriptSourceLine($asType, $dir, $file, $media = "", $charset = "", $if = "", $in_head = true, $immediate = false, $fallback = null, $cacheskip = false) {
	    global $_page;

	    $asType = strtolower($asType);
	    if(strstr($media, "RSS") == "") $media  = strtolower($media);
	    $out = "";
	    if($asType != "" && $dir != "" && $file != "") {
	        $filepath = null;
	        switch($asType){
	            case "script":
	                //eg: <script type="text/javascript" language="javascript" src="WEB_URL.ckeditor/ckeditor.js"></script>
	                if($if != "") $out .= "<!--[$if]>\n";
	                $out.= "<script type=\"text/javascript\" language=\"javascript\" ";
	                if($charset != "") $out.= "charset=\"$charset\" ";
	                $out.= "src=\"".$dir.$file."\"></script>";
	                if(!is_null($fallback)) $out .= "\n<script type=\"text/javascript\">".$fallback."</script>";
	                if($if != "") $out .= "\n<![endif]-->";
	                $filepath = $dir.$file;
	                break;
	            case "style":
	                //eg: <link href=".WEB_URL.ADMIN_FOLDER.JS_FOLDER.jscal2/css/jscal2.css" rel="stylesheet" type="text/css" />
	                if($if != "") $out .= "<!--[$if]>\n";
	                $out.= "<link href=\"".$dir.$file."\" ";
	                $out.= "rel=\"stylesheet\" ";
	                $out.= "type=\"text/css\" ";
	                if($media != "") $out.= "media=\"$media\" ";
	                $out.= "/>";
	                if($if != "") $out .= "\n<![endif]-->";
	                $filepath = $dir.$file;
	                break;
	            case "meta":
	                //eg: <meta name="Revisit-after" content="7 Days" />
	                $out.= "<meta name=\"".$dir."\" ";
	                $out.= "content=\"".$file."\" />";
	                $filepath = $dir.">>".$file;
	                $cacheskip = true;
	                break;
	            case "meta_equiv":
	                //eg: <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	                $out.= "<meta http-equiv=\"".$dir."\" ";
	                $out.= "content=\"".$file."\" />";
	                $filepath = $dir.">>".$file;
	                $cacheskip = true;
	                break;
	            case "rss":
	                //eg: <link href="http://blogs.msdn.com/ie/rss.xml" rel="alternate" type="application/rss+xml" title="IEBlog (RSS 2.0)" />
	                //eg: <link href=".WEB_URL.RSS_FOLDER.showrss.php" rel="alternate" type="application/rss+xml" title=".BUSINESS." />
	                $out.= "<link href=\"http://blogs.msdn.com/ie/rss.xml\" ";
	                $out.= "rel=\"alternate\" ";
	                $out.= "type=\"application/rss+xml\" ";
	                $out.= "title=\"IEBlog (RSS 2.0)\" ";
	                $out.= "/>\n";
	                $cacheskip = true;
	                break;
	            case "favicon":
	                //eg: <link rel="icon" href=".WEB_URL.MEDIA_FOLDER.IMG_UPLOAD_FOLDER.favicon.ico" type="image/x-icon" />
	                $out.= "<link href=\"".$dir.$file."\" ";
	                $out.= "rel=\"icon\" ";
	                $out.= "type=\"image/x-icon\" />\n";
	                $filepath = $dir.$file;
	                break;
	        }
	        if($out != ''){
	            if($immediate){
	                echo $out.PHP_EOL;
	            }else{
	                // add line to scriptlines object
	                $_page->scriptlines = array("line" => $out, "filepath" => $filepath, "in_head" => $in_head, "cacheskip" => $cacheskip);
	            }
	        }
	        return false;
	    }else{
	        return true;
	    }
	}

	/**
	 * Output or return inline script/style code.  This function sanitizes the code.
	 * @param string $asType                    style or script
	 * @param string $script                    content without the <SCRIPT> or <STYLE> open/close tags
	 * @param boolean $to_jq_block [optional]   true to add script to head jQuery block object
	 * @version 3.0
	 */
	function addScript($asType, $script, $to_jq_block = false) {
	    global $_page, $_events;

	    $asType = strtolower($asType);
	    $out = "";
	    if($asType != "" && $script != "") {
	        switch($asType){
	            case "script":
	                $out = $script;
	                break;
	            case "style":
	                $out = $script;
	                break;
	        }
	        if($out != ''){
	            if($to_jq_block && $asType == "script"){
	                global $jq_lines;
	                $jq_lines[] = $out;
	            }else{
	                echo $out.PHP_EOL;
	            }
	        }
	    }
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	}

	/**
	 * Output contents of $_page->scriptline array
	 * @param boolean $sorted               true to sort JS first, CSS last
	 * @param boolean $for_head             True to output lines designated for the <HEAD>, or false for outside of <HEAD>
	 */
	function showScriptSourceLines($sorted = false, $for_head = true){
	    global $_page, $_filesys, $_cache;

	    echo PHP_EOL;
	    $js    		= array();
	    $js_cache	= array();
	    $css   		= array();
	    $css_cache  = array();
	    $jscss 		= array();
	    $scripts_readied = array();
	    $stime 		= time();

	    if(count($_page->scriptlines) > 0){
	        if(!$sorted){
	            foreach($_page->scriptlines as $set) {
	                if($set['in_head'] == $for_head)
	                    if(!in_array($set['line'], $scripts_readied)){
	                        $jscss[] = array("line" => $set['line'], "filepath" => $set['filepath'], "cacheskip" => $set['cacheskip']);
	                        $scripts_readied[] = $set['line'];
	                    }
	            }
	            foreach($jscss as $set) echo $set['line']."\n";
	        }else{
	            foreach($_page->scriptlines as $set){
	                if($set['in_head'] == $for_head){
	                    $line = $set['line'];
	                    if(substr($line, 0, 4) != '<!--'){
	                        if(!in_array($set['line'], $scripts_readied)){
	                            if(strpos($line, '.js') !== false){
	                            	if($set['cacheskip'] && substr($set['filepath'], 0, strlen(WEB_URL)) == WEB_URL)
	                                	$js[] = array("line" => $line, "filepath" => $set['filepath'], "cacheskip" => $set['cacheskip']);
	                                else
	                                	$js_cache[] = array("line" => $line, "filepath" => $set['filepath'], "cacheskip" => $set['cacheskip']);
	                            }else{
	                            	if($set['cacheskip'] && substr($set['filepath'], 0, strlen(WEB_URL)) == WEB_URL)
		                                $css[] = array("line" => $line, "filepath" => $set['filepath'], "cacheskip" => $set['cacheskip']);
		                           	else
		                                $css_cache[] = array("line" => $line, "filepath" => $set['filepath'], "cacheskip" => $set['cacheskip']);
	                            }
	                            $scripts_readied[] = $set['line'];
	                        }
	                    }
	                }
	            }

	            // pass array through caching mechanism
	            if(!empty($css_cache)){
		            $css_cache = $_cache->sortCSSPaths($css_cache);
		            $css_cache = $_cache->check($css_cache, ".css");
		            foreach($css_cache as $set) echo $set['line']."\n";
		        }
		        if(!empty($css)){
	            	foreach($css as $set) echo $set['line']."\n";
	            }

	            if(!empty($js_cache)){
		            $js_cache = $_cache->check($js_cache, ".js");
			        foreach($js_cache as $set) echo $set['line']."\n";
			    }
			    if(!empty($js)){
		        	foreach($js as $set) echo $set['line']."\n";
		        }
	        }
	    }
	}

	/**
	 * Checks if plugin is in section (admin, root, any)
	 * @param str $section
	 * @version 3.4
	 */
	function checkPluginHome($plugin_name, $section = 'any'){
	    global $_error;

	    $section = trim(strtolower($section));
	    switch($section){
	        case 'admin':
	            $validfolder1 = SITE_PATH.ADMIN_FOLDER.PLUGINS_FOLDER;
	            $validfolder2 = '';
	            break;
	        case 'root':
	        case 'front':
	            $validfolder1 = SITE_PATH.PLUGINS_FOLDER;
	            $validfolder2 = '';
	            break;
	        case 'any':
	        case 'both':
	            $validfolder1 = SITE_PATH.ADMIN_FOLDER.PLUGINS_FOLDER;
	            $validfolder2 = SITE_PATH.PLUGINS_FOLDER;
	            break;
	        default;
	            return false;
	    }

	    if($validfolder2 != ''){
	        if(!file_exists($validfolder1."/".strtolower($plugin_name)) && !file_exists($validfolder2."/".strtolower($plugin_name)))
	            if($validfolder2 != '') $validfolder2 = " or ".$validfolder2;
	                $_error->addErrorMsg("Plugin problem: Plugin '$plugin_name' was not installed in {$validfolder1}{$validfolder2}.", CORE_ERR);
	    }else{
	        if(!file_exists($validfolder1."/".strtolower($plugin_name)))
	            $_error->addErrorMsg("Plugin problem: Plugin '$plugin_name' was not installed in {$validfolder1}.", CORE_ERR);
	    }
	}

	/**
	 * Checks the minimum PHP version required to run plugin and halts if PHP too old
	 * @param str $plugin_name
	 * @param float $minver
	 * @version 3.4
	 */
	function checkPluginPHPVersion($plugin_name, $minver){
	    if(floatval(phpversion()) < $minver) die("PHP {$minver} or higher required for {$plugin_name}!");
	}

	/**
	 * Output plugin settings dialog contents as JSON data
	 * @param string $title
	 * @param string $contents
	 * @param string $func Calling function
	 * @version 3.7
	 */
	function pluginSettingsDialogContents($title, $contents, $func){
	    return array("title"=>$title, "contents"=>$contents, "func"=>$func);
	}

	/**
	 * Pass plugin settings dialog button pressed response back through AJAX to the API
	 * @param string $message           Message displayed in alert box
	 * @param boolean $closedialog      If set to false the dialog will be prevented from closing
	 * @version 3.7
	 */
	function pluginSettingsDialogButtonPressed($message = '', $closedialog = true){
	    if(empty($message)) $message = '';
	    return array("success" => true, "message" => $message, "closedialog" => $closedialog);
	}

	/**
	 * Return array containing all plugin objects installed in system
	 * @return array
	 */
	function getPluginsInstalled(){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return $_system->plugins;
	}

	/**
	 * Return an installed plugin object fetched by its inclusion verb
	 * @param string $verb
	 * @return array
	 */
	function getPluginInstalledbyVerb($verb){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return $_system->plugins[$verb];
	}

	/**
	 * Return array containing all plugin objects installed in system that belong to a specific group
	 * @param string $group
	 * @return array
	 */
	function getPluginsInstalledbyGroup($group){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    if(is_null($group)) return "A group is required when calling ".__FUNCTION__;
	    $group = strtolower($group);
	    $groupplugins = array();
	    foreach($p as $verb => $plugin) if(strtolower($plugin['group']) == $group) $groupplugins[$verb] = $plugin;
	    return $groupplugins;
	}

	/**
	 * Return true if plugin is installed in the system
	 * @param string $verb
	 * @return boolean
	 */
	function isPluginInstalled($verb){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return (isset($_system->plugins[$verb]));
	}

	/**
	 * Return array containing all plugin objects initiated in the $incl variable
	 * @return array
	 */
	function getPluginsIncluded(){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return $_system->pluginsincl;
	}

	/**
	 * Return plugin object fetched by its inclusion verb (initiated in the $incl variable)
	 * @param string $verb
	 * @return array or null if not found
	 */
	function getPluginIncludedbyVerb($verb){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return $_system->pluginsincl[$verb];
	}

	/**
	 * Return true if plugin is included (initiated in the $incl variable)
	 * @param string $verb
	 * @return boolean
	 */
	function isPluginIncluded($verb){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return (isset($_system->pluginsincl[$verb]));
	}

	/**
	 * Return true if plugin is both installed and included
	 * @param string $verb
	 * @return boolean
	 */
	function isPluginReady($verb){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return ($this->isPluginInstalled($verb) && $this->isPluginIncluded($verb));
	}

	/**
	 * Return array containing all plugin objects with problems or have been deleted internally
	 * @return array
	 */
	function getPluginsWithProblems(){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return $_system->pluginsprob;
	}

	/**
	 * Return problem plugin object fetched by its inclusion verb
	 * @param string $verb
	 * @return array or null if not found
	 */
	function getProblemPluginIncludedbyVerb($verb){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return $_system->pluginsprob[$verb];
	}

	/**
	 * Return true if problem plugin is included
	 * @param string $verb
	 * @return boolean
	 */
	function isProblemPluginIncluded($verb){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return (isset($_system->pluginsprob[$verb]));
	}

	/**
	 * Return array of inclusion verbs
	 * @return array
	 */
	function getInclusionVerbs(){
	    global $_system;

	    $p = $_system->plugins;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return $_system->incl;
	}

	/**
	 * Return list of built-in system plugin inclusion verbs
	 * @param boolean $asString
	 * @return array|string
	 */
	function getSystemBuiltinPlugins($asString = false){
	    global $_system;

	    $plugins = array();
	    foreach($_system->plugins as $data){
	        if($data['builtin'] == 1 || $data['autoincl'] == 1) $plugins[] = $data['incl'];
	    }
	    if($asString) $plugins = "'".join("','", $plugins)."'";
	    return $plugins;
	}

	/**
	 * Alias of getSystemBuiltinPlugins();
	 */
	function getExtensionsInstalled($asString = false){
		return $this->getSystemBuiltinPlugins($asString);
	}

	/**
	 * Return list of framework inclusion verbs
	 * @return array
	 */
	function getFrameworksInstalled(){
	    global $_system;

	    $p = $_system->frameworks;
	    if(is_null($p)) return __FUNCTION__." cannot be used within the global scope of a plugin.";
	    return $p;
	}

	/**
	 * Get the value of a plugin setting from either the plugins or settings (system) table
	 * @param string $verb          Plugin's registered verb (must by installed)
	 * @param string $fromwhere     Either PLUGIN_SETTINGS_SAVETOSTD or PLUGIN_SETTINGS_SAVETOSYS
	 * @return mixed                The value of the plugin setting
	 */
	function getPluginCustomSetting($verb, $fromwhere = PLUGIN_SETTINGS_SAVETOSTD){
	    global $_system, $_db_control;

	    $result = false;
	    if($verb != '' && in_array($fromwhere, array(PLUGIN_SETTINGS_SAVETOSTD, PLUGIN_SETTINGS_SAVETOSYS))){
	        if($this->isPluginInstalled($verb)){
	            switch($fromwhere){
	                case PLUGIN_SETTINGS_SAVETOSTD:
	                    // retrieve from plugin table
	                    $result = $_db_control->setTable(PLUGINS_TABLE)->setFields("custom_settings")->setWhere("incl = '$verb'")->getRecItem();
	                    break;
	                case PLUGIN_SETTINGS_SAVETOSYS:
	                    // retrieve from settings table
	                    $result = $_db_control->setTable(SETTINGS_TABLE)->setFields("value")->setWhere("`name` = 'PLUGIN_".strtoupper($verb)." AND `type` = 'plg'")->getRecItem();
	                    break;
	            }
	        }
	    }
	    return $result;
	}

	/**
	 * Save a plugin custom array, string, or object value to either the plugins or settings (system) table
	 * @param string $verb          Plugin's registered verb (must be installed)
	 * @param mixed $settings       Data to be saved
	 * @param string $savewhere     Either PLUGIN_SETTINGS_SAVETOSTD or PLUGIN_SETTINGS_SAVETOSYS
	 * @return boolean
	 */
	function savePluginCustomSettings($verb, $settings, $savewhere = PLUGIN_SETTINGS_SAVETOSTD){
	    global $_system, $_db_control, $_events;

	    $result = false;
	    if($verb != '' && in_array($savewhere, array(PLUGIN_SETTINGS_SAVETOSTD, PLUGIN_SETTINGS_SAVETOSYS))){
	        $setval = $settings;
	        if(is_array($setval) || is_object($setval)) $setval = json_encode($setval);

	        if($this->isPluginInstalled($verb)){
	            switch($savewhere){
	                case PLUGIN_SETTINGS_SAVETOSTD:
	                    // save to plugin table
	                    $result = $_db_control->setTable(PLUGINS_TABLE)->setFieldvals(array("custom_settings" => $setval))->setWhere("incl = '$verb'")->updateRec();
	                    break;
	                case PLUGIN_SETTINGS_SAVETOSYS:
	                    // save to settings table
	                    $result = ($_db_control->setTable(SETTINGS_TABLE)->setFieldvals(array("name" => "PLUGIN_".strtoupper($verb), "value" => $setval, "type" => "plg"))->setWhere("`name` = 'PLUGIN_".strtoupper($verb)."' AND `type` = 'plg'")->replaceRec() !== false);
	                    break;
	            }
	        }
	    }
		$_events->processTriggerEvent(__FUNCTION__, array($verb, $result));				// alert triggered functions when function executes
	    return $result;
	}

	/**
	 * Return the plugin verb using the id, folder, or initfile
	 * @param string|integer $val       id, folder or initfile to use as reference
	 * @param string $field             'id', 'folder', or 'initfile'
	 * @return string
	 */
	function getPluginVerb($val, $field = 'id'){
	    global $_db_control;

	    $verb = null;
	    $ok = true;
	    $field = strtolower($field);
	    if(!in_array($field, array('id', 'folder', 'initfile'))) return null;
	    if($field == 'id'){
	        $val = intval($val);
	        if($val <= 0) $ok = false;
	    }
	    if($ok) $verb = $_db_control->setTable(PLUGINS_TABLE)->setFields("incl")->setWhere("{$field}='{$val}'")->getRecItem();
	    return $verb;
	}

	/**
	 * Return specific information about a plugin using the plugin verb.
	 * @param string $verb          The inclusion verb of the subject plugin.  If blank and the type is 'url' or 'path',
	 *                              the info will be based on the folder in which the calling file is located
	 * @param string $item          The information requested
	 * @return mixed                The data retrieved
	 */
	function getPluginInfo($verb, $item = ''){
	    global $_system;

	    $verb = strtolower($verb);
	    $plugins = $_system->plugins;
	    $plugin = null;
	    $return = false;
	    if(isset($plugins[$verb])){
	        $plugin = $plugins[$verb];

	        switch(strtolower($item)){
	            case 'id':
	                $return = intval($plugin[$item]);
	                break;
	            case 'url':
	                $return = WEB_URL.$plugin['folder'];
	                break;
	            case 'path':
	                $return = SITE_PATH.$plugin['folder'];
	                break;
	            case 'name':
	            case 'ver':
	            case 'author':
	            case 'created':
	            case 'revised':
	            case 'license':
	            case 'website':
	            case 'usedin':
	            case 'depends':
	            case 'parent':
	            case 'folder':
	            case 'initfile':
	            case 'headerfunc':
	            case 'settingsfunc':
	            case 'group':
	                $return = $plugin[$item];
	                break;
	            case 'nodisable':
	            case 'nodelete':
	            case 'active':
	                $return = (bool) $plugin[$item];
	                break;
	            case '':
	                // return the plugin object
	                $return = $plugins[$verb];
	                break;
	            default:
	                break;
	        }
	    }
	    return $return;
	}

	/**
	 * Return the folder of a plugin via a plugin-based function (a calling function sourced from within a registered plugin folder)
	 *.@param string $verb              If provided, this procedure is similar to getPluginInfo($verb, 'folder')
	 * @return string
	 */
	function getPluginFolder($verb = ''){
	    global $_system;

	    $folder = null;
	    if(empty($verb)){
	        $stack = debug_backtrace();
	        if(count($stack) > 1 && isset($stack[0]['file'])){
	            $path = str_replace(DIRECTORY_SEPARATOR, "/", $stack[0]['file']);
	            foreach($_system->plugins as $plugin){
	                if(strpos($path, $plugin['folder']) !== false){
	                    $folder = $plugin['folder'];
	                    break;
	                }
	            }
	        }
	    }else{
	        $folder = $this->getPluginInfo($verb, 'folder');       // verb is provided
	    }
	    return $folder;
	}

	/**
	 * Delete contents of a plugin folder
	 * @param string $folder
	 */
	function deletePluginFolderContents($folder){
	    global $plugin_folder, $plugin_id, $_db_control, $_events;

		$_events->processTriggerEvent(__FUNCTION__, $folder);				// alert triggered functions when function executes
	    $rtn = '';
	    if(!empty($folder)){
	        if(file_exists($folder)){
	            if(false !== ($handle = opendir($folder))) {
	                while (false !== ($file = readdir($handle))) {
	                    if($file != '.' && $file != '..'){
	                        if(is_dir($folder.$file)){
	                            $rtn .= $this->deletePluginFolderContents($folder.$file."/");
	                            $files = @scandir($folder.$file);
	                            if($files && count($files) <= 2){
	                                // folder is empty
	                                closedir(opendir($folder.$file));   // close all connections to it
	                                chmod($folder.$file, 0777);
	                                @rmdir($folder.$file);
	                            }
	                        }else{
	                            if(strpos(strtolower($folder), strtolower($plugin_folder)) !== false){
	                                // ensure file is below absolute parent folder
	                                unlink($folder.$file);
	                            }
	                        }
	                    }
	                }
	            }
	            closedir($handle);

	            // lastly, remove plugin folder
	            if($folder == $plugin_folder){
	                $files = @scandir($plugin_folder);
	                if($files && count($files) <= 2){
	                    chmod($plugin_folder, 0777);
	                    @rmdir($plugin_folder);
	                    if($plugin_id > 0) $_db_control->setTable(PLUGINS_TABLE)->setWhere("id = '{$plugin_id}'")->deleteRec();
	                }else{
	                    $rtn .= 'Could not completely delete plugin folder because it still contains files/sub-folders.';
	                }
	            }
	        }
	    }
		$_events->processTriggerEvent(__FUNCTION__.'_done', $rtn);				// alert triggered functions when function executes
	    return $rtn;
	}

	/**
	 * Save the current activity state of all installed plugins to a specific location
	 * @param string $folder                    Path to save XML file
	 * @return boolean                          True if file was successfully saved
	 */
	function exportPluginStates($folder){
	    global $_system;

	    $ok = false;
	    if(file_exists($folder.".")){
	        $doc = new_xmldoc('1.0');
	        $root = $doc->add_root('plugins');

	        foreach($_system->plugins as $plugin){
	            $member = $root->new_child('plugin', '');

	            $member->new_child('name', $plugin['name']);
	            $member->new_child('incl', $plugin['incl']);
	            $member->new_child('active', $plugin['active']);
	        }

	        $fp = @fopen($folder.'pluginstate.xml', 'w+');
	        if($fp) {
	            fwrite($fp,$doc->dumpmem());
	            fclose($fp);
	            $ok = true;
	        }
	    }
	    return $ok;
	}

	/**
	 * Taking a context meta string, return either a context array or human-readable context phrase
	 * @param string $data 				The context meta string
	 * @param string $outputas 			Either array (default) or string
	 * @return array|string
	 */
	function parseInitContexts($data, $outputas = 'array'){
		$valid_contexts = explode(",", CONTEXTS);

		// plugin init contexts look like:
		//    context_set is context1==value1,value2; context2==value3
		//	  context is context1==value1,value2
		// 	  attr is context1
		//    condition_set is value1,value2
		//	  condition is value1

		$context_rules = array();
		if(!is_string($data)) return (($outputas == 'array') ? $context_rules : null);

		$context_set = explode(";", $data);
		foreach($context_set as $context){
			$context = explode("==", $context);
			if(count($context) == 2){
				$attr = trim(strtolower($context[0]));
				$conditions = trim($context[1]);
				if(in_array($attr, $valid_contexts)){
					$condition_set = explode(",", $conditions);
					if(!empty($condition_set)){
						$context_rules[$attr] = $condition_set;
					}
				}
			}
		}

		if($outputas == 'string'){
			$cstr = null;
			foreach($context_rules as $attr => $conditions){
				$cstr .= ((!is_null($cstr)) ? ' AND ' : '').ucwords($attr);
				if(count($conditions) > 1)
					$cstr .= ' is either '.join(' OR ', $conditions);
				else
					$cstr .= ' is '.join('', $conditions);
			}
			$context_rules = $cstr;
		}
		return $context_rules;
	}

	/**
	 * Delegate an action to a plugin function (first including the init file if the plugin has not been included yet)
	 * @param string|integer $key 				The inclusion verb or id of the plugin
	 * @param string $event 					The event that is part of the function name to hail
	 */
	function delegateAction($key, $event){
		global $_system, $_events;

		$plugin = null;
		$event = strtolower($event);
		if(is_numeric($key)){
			foreach($_system->plugins as $incl => $plugin_data){
				if($plugin_data['id'] == $key){
					$plugin = $plugin_data;
					break;
				}
			}
		}else{
			$key = strtolower($key);
			if(isset($_system->plugins[$key]))
				$plugin = $_system->plugins[$key];
		}

		if(!is_null($plugin)){
			$func = $plugin['incl'].'_'.$event;
			$ready = false;
			if(isset($_system->pluginsincl[$plugin['incl']])){
				// plugin has already been included.
				$ready = true;
			}else{
				// include init file
				if(include_once (SITE_PATH.$plugin['folder'].$plugin['initfile'])) $ready = true;
			}
			if($ready){
				if(function_exists($func)){
					// plugin delegate function found
					call_user_func($func);
				}
			}
		}
	}

	public function __get($name){
		$v = null;
		if(array_key_exists($name, $this->_keys)){
			// return scalar value
			$v = $this->_keys[$name];
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot get '$name'.  It is not a valid _PLUGINS property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
		return $v;
	}

	public function __set($name, $value){
		if(array_key_exists($name, $this->_keys)){
			// set scalar value
			$this->_keys[$name] = $value;
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot set '$name'.  It is not a valid _PLUGINS property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
	}
}

?>