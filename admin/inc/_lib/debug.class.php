<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("DEBUGLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/* KINT VISUAL DEBUGGER */

if(file_exists(SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."kint/Kint.class.php")){
    require SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."kint/Kint.class.php";
}

/**
 * Non-Class versions of specific methods
 */
function printr($array, $return = false){
	$d = new DebugClass();
	$d->printr($array, $return);
}

function _e($text, $is_error_msg = false){
	$d = new DebugClass();
	$d->_e($text, $is_error_msg);
}

/*
 * DEBUG CLASS
 * Provides several debugging methods for developers
 */
class DebugClass {
	// overloaded data
	public $debugger = false;
	public $debugger_on_error = false;
	protected static $_instance = null;

	public function __construct() {
		if(!isset($_SESSION['debug'])){
			$_SESSION['debug']['debugger'] = false;
			$_SESSION['debug']['debugger_on_error'] = false;
		}
	}

	private function __clone(){
	}

	public static function init(){
		global $_events;

		$s = new self;
		$_events->processTriggerEvent(__FUNCTION__.'_debug');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Toggle or set the debugger engine
	 * @param boolean $set 				True/false to activate/deactivate, or empty to toggle state
	 */
	public function toggle_debug($set = null){
		global $_users;

		if($set || is_null($set)){
			if(ALLOW_DEBUGGING && $_users->isUserLoggedin() && $_users->userIsAllowedTo(UA_VIEW_ERRORS)) {
				if(is_null($set)) $set = !$_SESSION['debug']['debugger'];
				$_SESSION['debug']['debugger'] = (bool) $set;
			}else{
				$_SESSION['debug']['debugger'] = false;
			}
		}else{
			$_SESSION['debug']['debugger'] = false;
		}
	}

	/**
	 * Toggle or set the debugger-on-error switch
	 * @param boolean $set 				True/false to activate/deactivate, or empty to toggle state
	 */
	public function toggle_debug_on_error($set = null){
		if(ALLOW_DEBUGGING && $_users->isUserLoggedin() && $_users->userIsAllowedTo(UA_VIEW_ERRORS)) {
			if(is_null($set)) $set = !$_SESSION['debug']['debugger_on_error'];
			$_SESSION['debug']['debugger_on_error'] = (bool) $set;
		}else{
			$_SESSION['debug']['debugger_on_error'] = false;
		}
	}

	/**
	 * Return state of debugger
	 * @return boolean
	 */
	public function isDebugging(){
		return ($_SESSION['debug']['debugger'] || $_SESSION['debug']['debugger_on_error']);
	}

	/**
	 * HTML formatted print_r
	 * @param array $array
	 * @param boolean $return 			Set to true to return instead of echoing output
	 */
	public function printr($array, $return = false){
		if(is_array($array) || is_object($array)){
			if($return){
	            return print_r($array, true);
	        }else{
	            print "<pre>".print_r($array, true)."</pre>";
	        }
		}
	}

	/**
	 * Debugger-aware echo
	 * Debugger-aware means that this function will only execute if the debugger system is active
	 * @param mixed $text
	 * @param boolean $is_error_msg
	 */
	public function _e($text, $is_error_msg = false){
		global $_error, $_users;

	    if($_SESSION['debug']['debugger'] || ($_SESSION['debug']['debugger_on_error'] && $is_error_msg)) {
	    	echo $text."<br/>".PHP_EOL;
	    	$_error->_log($text);
	    }
	}

	/**
	 * Debugger-aware printr
	 * Debugger-aware means that this function will only execute if the debugger system is active
	 * @param array $array
	 */
	public function _pr($array){
		global $_error;

	    if($this->isDebugging() && is_array($array)) {
	    	$this->printr($array);
	    	$_error->_log(print_r($array, true));
	    }
	}

	/**
	 * Debugger-aware var_dump
	 * Debugger-aware means that this function will only execute if the debugger system is active
	 * @param mixed $obj
	 */
	public function _vd($obj){
	    if($this->isDebugging()) {
	    	var_dump($obj);
	    }
	}

	/**
	 * Debugger-aware var_export
	 * Debugger-aware means that this function will only execute if the debugger system is active
	 * @param mixed $obj
	 */
	public function _ve($obj){
	    if($this->isDebugging()) {
	        var_export($obj);
	    }
	}

	/**
	 * Debugger-aware debug_backtrace
	 * Debugger-aware means that this function will only execute if the debugger system is active
	 */
	public function _dbt($index = -1){
	    if($this->isDebugging()) {
	    	$d = debug_backtrace();
	    	if($index >= 0 && isset($d[$index])) $d = $d[$index];
	    	$this->printr($d);
	    }
	}

	/**
	 * Debugger-aware ReflectionClass getStatic
	 * Debugger-aware means that this function will only execute if the debugger system is active
	 */
	public function _rc_getstatic(){
	    if($this->isDebugging()){
	        $reflection = new ReflectionClass('Static');
	        $this->printr($reflection->getStaticProperties());
	    }
	}

	/**
	 * Debugger-aware ReflectionClass getProperties
	 * Debugger-aware means that this function will only execute if the debugger system is active
	 * @param str $class
	 */
	public function _rc_getprops($class){
	    if($this->isDebugging()){
	        $reflect = new ReflectionClass($class);
	        $props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);

	        foreach ($props as $prop) {
	            print $prop->getName()."\n";
	        }
	    }
	}

	/**
	 * Debugger-aware ReflectionClass getMethods
	 * Debugger-aware means that this function will only execute if the debugger system is active
	 * @param str $class
	 */
	public function _rc_getmethods($class){
	    if($this->isDebugging()){
	        $reflect = new ReflectionClass($class);
	        $methods = $reflect->getMethods();
	        $this->printr($methods);
	    }
	}
}

?>