<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("RSSLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/**
 * RSSCLASS
 * Maintains and gives operation to the system for all RSS, ATOM, RDF, and Pingback requests
 */
class RSSClass {
    // channel vars
    var $channel_url;
    var $channel_title;
    var $channel_description;
    var $channel_lang;
    var $channel_copyright;
    var $channel_date;
    var $channel_creator;
    var $channel_subject;
    var $channel_generator;
    var $channel_subtitle;
    var $channel_id;

    // image
    var $image_url;

    // items
    var $items = array();
    var $nritems;

	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
		$this->nritems=0;
		$this->channel_url='';
		$this->channel_title='';
		$this->channel_description='';
		$this->channel_lang='';
		$this->channel_copyright='';
		$this->channel_date='';
		$this->channel_creator='';
		$this->channel_subject='';
		$this->channel_generator=null;
		$this->channel_subtitle='';
		$this->channel_id='';
		$this->image_url='';
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'');                // alert triggered functions when function executes
		return $s;
	}

    // set channel vars
    public function rss_setChannel($url, $title, $description, $lang, $copyright, $creator, $subject) {
        $this->channel_url = $url;
        $this->channel_title = $title;
        $this->channel_description = $description;
        $this->channel_lang = $lang;
        $this->channel_copyright = $copyright;
        $this->channel_date = date("Y-m-d").'T'.date("H:i:s").'+01:00';
        $this->channel_creator = $creator;
        $this->channel_subject = $subject;
    }

    // set atom feed vars
    public function atom_setChannel($id, $url, $title, $description, $lang, $copyright, $creator, $subtitle = null) {
        $this->channel_id = $id;
        $this->channel_url = $url;
        $this->channel_title = $title;
        $this->channel_description = $description;
        $this->channel_lang = $lang;
        $this->channel_copyright = $copyright;
        $this->channel_date = date("Y-m-d").'T'.date("H:i:s").'+01:00';
        $this->channel_creator = $creator;
        $this->channel_subtitle = $subtitle;
    }

    // set image
    public function rss_setImage($url) {
        $this->image_url = $url;
    }

    // set item
    public function rss_setItem($url, $title, $description) {
        $this->items[$this->nritems]['url'] = $url;
        $this->items[$this->nritems]['title'] = $title;
        $this->items[$this->nritems]['description'] = $description;
        $this->nritems++;
    }

    // set atom item
    public function atom_setItem($id, $url, $title, $description, $updated = null, $published = null, $author = null) {
        $this->items[$this->nritems]['id'] = $id;
        $this->items[$this->nritems]['url'] = $url;
        $this->items[$this->nritems]['title'] = $title;
        $this->items[$this->nritems]['description'] = $description;
        $this->items[$this->nritems]['updated'] = $updated;
        $this->items[$this->nritems]['published'] = $published;
        $this->items[$this->nritems]['author'] = $author;
        $this->nritems++;
    }

    // output feed
    public function rss_output() {
        $output =  '<?xml version="1.0" encoding="'.CHARSET.'"?>'."\n";
        $output .= '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://purl.org/rss/1.0/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" xmlns:taxo="http://purl.org/rss/1.0/modules/taxonomy/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:syn="http://purl.org/rss/1.0/modules/syndication/" xmlns:admin="http://webns.net/mvcb/" xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0">'."\n";
        $output .= '<channel rdf:about="'.$this->channel_url.'">'."\n";
        $output .= '<title>'.$this->channel_title.'</title>'."\n";
        $output .= '<link>'.$this->channel_url.'</link>'."\n";
        $output .= '<description>'.$this->channel_description.'</description>'."\n";
        $output .= '<dc:language>'.$this->channel_lang.'</dc:language>'."\n";
        $output .= '<dc:rights>'.$this->channel_copyright.'</dc:rights>'."\n";
        $output .= '<dc:date>'.$this->channel_date.'</dc:date>'."\n";
        $output .= '<dc:creator>'.$this->channel_creator.'</dc:creator>'."\n";
        $output .= '<dc:subject>'.$this->channel_subject.'</dc:subject>'."\n";

        $output .= '<items>'."\n";
        $output .= '<rdf:Seq>';
        for($k=0; $k<$this->nritems; $k++) {
            $output .= '<rdf:li rdf:resource="'.$this->items[$k]['url'].'"/>'."\n";
        };
        $output .= '</rdf:Seq>'."\n";
        $output .= '</items>'."\n";
        $output .= '<image rdf:resource="'.$this->image_url.'"/>'."\n";
        $output .= '</channel>'."\n";
        for($k=0; $k<$this->nritems; $k++) {
            $output .= '<item rdf:about="'.$this->items[$k]['url'].'">'."\n";
            $output .= '<title>'.$this->items[$k]['title'].'</title>'."\n";
            $output .= '<link>'.$this->items[$k]['url'].'</link>'."\n";
            $output .= '<description>'.$this->items[$k]['description'].'</description>'."\n";
            $output .= '<feedburner:origLink>'.$this->items[$k]['url'].'</feedburner:origLink>'."\n";
            $output .= '</item>'."\n";
        };
        $output .= '</rdf:RDF>'."\n";
        return $output;
    }

    // output feed
    public function rdf_output() {
        $output =  '<?xml version="1.0" encoding="'.CHARSET.'"?>'."\n";
        $output .= '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://purl.org/rss/1.0/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" xmlns:taxo="http://purl.org/rss/1.0/modules/taxonomy/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:syn="http://purl.org/rss/1.0/modules/syndication/" xmlns:admin="http://webns.net/mvcb/" xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0">'."\n";
        $output .= '<channel rdf:about="'.$this->channel_url.'">'."\n";
        $output .= '<title>'.$this->channel_title.'</title>'."\n";
        $output .= '<link>'.$this->channel_url.'</link>'."\n";
        $output .= '<description>'.$this->channel_description.'</description>'."\n";
        $output .= '<dc:language>'.$this->channel_lang.'</dc:language>'."\n";
        $output .= '<dc:rights>'.$this->channel_copyright.'</dc:rights>'."\n";
        $output .= '<dc:date>'.$this->channel_date.'</dc:date>'."\n";
        $output .= '<dc:creator>'.$this->channel_creator.'</dc:creator>'."\n";
        $output .= '<dc:subject>'.$this->channel_subject.'</dc:subject>'."\n";

        $output .= '<items>'."\n";
        $output .= '<rdf:Seq>';
        for($k=0; $k<$this->nritems; $k++) {
            $output .= '<rdf:li rdf:resource="'.$this->items[$k]['url'].'"/>'."\n";
        };
        $output .= '</rdf:Seq>'."\n";
        $output .= '</items>'."\n";
        $output .= '<image rdf:resource="'.$this->image_url.'"/>'."\n";
        $output .= '</channel>'."\n";
        for($k=0; $k<$this->nritems; $k++) {
            $output .= '<item rdf:about="'.$this->items[$k]['url'].'">'."\n";
            $output .= '<title>'.$this->items[$k]['title'].'</title>'."\n";
            $output .= '<link>'.$this->items[$k]['url'].'</link>'."\n";
            $output .= '<description>'.$this->items[$k]['description'].'</description>'."\n";
            $output .= '<feedburner:origLink>'.$this->items[$k]['url'].'</feedburner:origLink>'."\n";
            $output .= '</item>'."\n";
        };
        $output .= '</rdf:RDF>'."\n";
        return $output;
    }

    // output feed
    public function atom_output() {
    	$author = '';
    	$ok = false;
        if(is_string($this->channel_creator)){
        	$author = $this->channel_creator."\n";
        	$ok = true;
        }else{
        	if(isset($this->channel_creator['name'])){
        		$author .= '<name>'.$this->channel_creator['name'].'</name>'."\n";
        		$ok = true;
        	}
        	if(isset($this->channel_creator['email']) && !empty($this->channel_creator['email']))
        		$author .= '<email>'.$this->channel_creator['email'].'</email>'."\n";
        	if(isset($this->channel_creator['uri']) && !empty($this->channel_creator['uri']))
        		$author .= '<uri>'.$this->channel_creator['uri'].'</uri>'."\n";
        }
        if(!$ok) return false;

        $output =  '<?xml version="1.0" encoding="'.CHARSET.'"?>'."\n";
        $output .= '<feed xmlns="http://www.w3.org/2005/Atom">'."\n";
        $output .= '<title>'.$this->channel_title.'</title>'."\n";
        $output .= '<link href="'.$this->channel_url.'"/>'."\n";
        $output .= '<updated>'.$this->channel_date.'</updated>'."\n";
        $output .= '<author>'."\n".$author.'</author>'."\n";
        $output .= '<id>urn:uuid:'.trim(guid($this->channel_id, true), '{}').'</id>'."\n";
        $output .= '<generator version="'.CODE_VER.'">'.SYS_NAME.'</generator>'."\n";
        $output .= '<logo>'.$this->image_url.'</logo>'."\n";
        $output .= '<rights>'.$this->channel_copyright.'</rights>'."\n";
        if(!empty($this->channel_subtitle)) $output .= '<subtitle>'.$this->channel_subtitle.'</subtitle>'."\n";

        for($k=0; $k<$this->nritems; $k++) {
	        $output .= '<entry>'."\n";
            $output .= '<id>urn:uuid:'.trim(guid($this->items[$k]['id'], true), '{}').'</id>'."\n";
            $output .= '<title>'.$this->items[$k]['title'].'</title>'."\n";
            $output .= '<link href="'.$this->items[$k]['url'].'"/>'."\n";
            $output .= '<content>'.$this->items[$k]['description'].'</content>'."\n";
        	if(isset($this->items[$k]['updated']) && isDate($this->items[$k]['updated']))
        		$output .= '<updated>'.date("Y-m-d\Th:i:s\Z", strtotime($this->items[$k]['updated'])).'</updated>'."\n";
        	if(isset($this->items[$k]['published']) && isDate($this->items[$k]['published']))
        		$output .= '<published>'.date("Y-m-d\Th:i:s\Z", strtotime($this->items[$k]['published'])).'</published>'."\n";
        	$author = trim($this->items[$k]['author']);
        	if(isset($this->items[$k]['author']) && !empty($author)){
	        	$output .= '<author>'."\n";
	        	$output .= '<name>'.$author.'</name>'."\n";
	        	$output .= '</author>'."\n";
        	}
            $output .= '</entry>'."\n";
        };
        $output .= '</feed>'."\n";
        return $output;
    }

	public function rpc_pingback($text, $sourceURI, $sameSite = False) {
	    $targets = $this->rpc_getTargets($text);
	    foreach($targets as $targetURI) {
	        preg_match("/X-Pingback: (\S+)/i", $this->rpc_httpreq($targetURI, "HEAD"), $matches);

	        if(isset($matches[1])) {
	            $pingbackserver = $matches[1];
	        } else {
	            preg_match("/<link rel=\"pingback\" href=\"([^\"]+)\" ?\/?>/i", $this->rpc_httpreq($targetURI), $matches);

	            $pingbackserver = $matches[1];
	            if(!$pingbackserver) {
	                continue;
	            }
	        }
	        if ($sameSite !== False) {
	            preg_match("/^http:\/\/([^\/]+)(.*)$/", $sourceURI, $matches);
	            $hostname1 = $matches[1];
	            preg_match("/^http:\/\/([^\/]+)(.*)$/", $targetURI, $matches);
	            $hostname2 = $matches[1];
	            if ($hostname2 == $hostname) {
	                continue;
	            }
	        }
	        $this->rpc_ping($pingbackserver, $sourceURI, $targetURI);
	    }
	    return true;
	}

	public function rpc_getTargets($text) {
	    preg_match_all("/<a[^>]+href=.(http:\/\/[^'\"]+)/i", $text, $matches);
	    return array_unique($matches[1]);
	}

	public function rpc_httpreq($uri, $method = "GET", $add_header = '', $payload =  '') {
	    preg_match("/^http:\/\/([^\/]+)(.*)$/", $uri, $matches);
	    $hostname = $matches[1];
	    $script = $matches[2];
	    if(empty($hostname)) {
	        return;
	    }
	    $fp = fsockopen($hostname, 80, $errno, $errstr, 30);
	    if(!$fp) {
	        return;
	    }
	    fwrite($fp, "$method $script HTTP/1.1
	Host: $hostname
	User-Agent: pingback
	$add_header

	$payload
	");
	    stream_set_timeout($fp, 5);
	    $res = stream_get_contents($fp);
	    fclose($fp);
	    return $res;
	}

	public function rpc_ping($pingbackserver, $sourceURI, $targetURI) {
	    $payload = <<<ENDE
	    <?xml version="1.0"?>
	    <methodCall>
	        <methodName>pingback.ping</methodName>
	        <params>
	        <param>
	        <value>$sourceURI</value>
	        </param>
	        <param>
	        <value>$targetURI</value>
	        </param>
	        </params>
	    </methodCall>
ENDE;
	    $length = strlen($payload);
	    $request_head = "Content-Type: text/xml\r\nContent-length: $length";
	    return $this->rpc_httpreq($pingbackserver, "POST", $request_head, $payload);
	}
}

?>