<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("MAILLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

$path = SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."mail/lib/swift_required.php";

/* MAILER CLASSES
 *
 * A wrapper for a scalable mail system
 */
if(file_exists($path)){
	// Initiate the Swift Mailer SMTP class framework
	// designed (c) 2004-2009 Chris Corbyn

	define('MAILERCLASS', 'Swift');
	require ($path);
	require (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."mail/smtp-validate-email.php");

	// *** EXAMPLE ***
	// $mailer   = MailerClass::init();
	// $message  = MessageClass::init();
	// $message->setSubject('Subject')
	//     ->setFrom(array('cdonalds01@gmail.com' => SITE_NAME))
	//     ->setTo(array('email@domain.com' => 'Recipient Name'))
	//     ->setBody('Body of message')
	//     ;
	// $result = $mailer->send($message);

	class MailerClass extends Swift_Mailer {
		/**
		 * usage: MailerClass::init();
		 */
		public static function init(){
			global $_events;

			// Set up transport here
			if(SMTP_SECURE_MODE != '' && self::anySSLSocketsAvailable(SMTP_SECURE_MODE)){
				$transport = Swift_SmtpTransport::newInstance()
					->setHost(SMTP_HOST)
					->setPort(intval(SMTP_PORT))
					->setEncryption(SMTP_SECURE_MODE)
					->setUsername(SMTP_USERNAME)
					->setPassword(SMTP_PASSWORD)
					;
			}else{
				$transport = Swift_SmtpTransport::newInstance()
					->setHost(SMTP_HOST)
					->setPort(SMTP_PORT)
					->setUsername(SMTP_USERNAME)
					->setPassword(SMTP_PASSWORD)
					;
			}

			$s = self::newInstance($transport);
			$_events->processTriggerEvent(__FUNCTION__.'_mailer');				// alert triggered functions when function executes
			return $s;
		}

		/**
		 * usage: MailerClass::sendmail($to, $from, $subject, $message);
		 * @param string|array $to 				array(address => name)
		 * @param string|array $from			array(address => name)
		 * @param string $subject
		 * @param string $message
		 * @param string|array $headers
		 * @param string|array $params
		 * @return boolean
		 */
		public static function sendmail($to, $from, $subject, $message, $headers = null, $params = null){
			global $_events;

			list($abort) = $_events->processTriggerEvent(__FUNCTION__, array($to, $from, $subject), false);				// alert triggered functions when function executes
			if($abort) return false;

			$_mailer = self::init();
			$_message = MessageClass::init();
			$_message->setSubject($subject)
			    ->setFrom($from)
			    ->setTo($to)
			    ->setBody($message)
			    ;
			$result = $_mailer->send($_message);
			$_events->processTriggerEvent(__FUNCTION__.'_done', $result);				// alert triggered functions when function executes
			return $result;
		}

		/**
		 * Return whether or not the requested encrypted socket is in the list of registered PHP sockets
		 * @param string $sslsocket 			Requested socket (ssl or tls)
		 * @return boolean
		 */
		public static function anySSLSocketsAvailable($sslsocket = 'ssl'){
			$sockets = stream_get_transports();
			return in_array($sslsocket, $sockets);
		}

		public static function checkSMTPHost($smtpServer, $secureMode, $port){
			if($smtpServer == '' || intval($port) <= 0) return false;
			try {
				setErrorSuppression(true);
				$secureMode = strtolower($secureMode);
				if(in_array($secureMode, array('tls', 'ssl'))) $smtpServer = $secureMode . "://".$smtpServer;
				$smtpConnect = @fsockopen($smtpServer, intval($port), $errno, $errstr, 10);
				setErrorSuppression(false);
				if(empty($smtpConnect)) {
					return false;
				} else {
					return $smtpConnect;
				}
			} catch(Exception $e) {
				die("Failed to connect: ".$e->getMessage());
			}
		}

		public static function checkSMTPAuth($smtpConnect, $username, $password, $to, $from){
			$localhost = "localhost";
			$newLine = "\r\n";
			$output = "";

			//Say Hello to SMTP
			fputs($smtpConnect, "HELO $localhost" . $newLine);
			$smtpResponse = fgets($smtpConnect, 515);
			$output .= "HELO: $smtpResponse{$newLine}";

			fputs($smtpConnect, "HELO ".$localhost . $newLine);
			$smtpResponse = fgets($smtpConnect, 515);

			if(in_array(SMTP_SECURE_MODE, array('tls', 'ssl'))) {
			    fputs($smtpConnect, "STARTTLS" . $newLine);
			    $smtpResponse = fgets($smtpConnect, 515);
				stream_socket_enable_crypto($smtpConnect, true, STREAM_CRYPTO_METHOD_TLS_CLIENT);
				$output .= "SSL: $smtpResponse{$newLine}";

				//Say hello again!
			    fputs($smtpConnect, "HELO " . $localhost . $newLine);
			    $smtpResponse = fgets($smtpConnect, 515);
			}

			//Request Auth Login
			fputs($smtpConnect, "AUTH LOGIN" . $newLine);
			$smtpResponse = fgets($smtpConnect, 515);
			$output .= "AUTH LOGIN: $smtpResponse{$newLine}";

			//Send username
			fputs($smtpConnect, base64_encode($username) . $newLine);
			$smtpResponse = fgets($smtpConnect, 515);
			$output .= "USER: $smtpResponse{$newLine}";

			//Send password
			fputs($smtpConnect, base64_encode($password) . $newLine);
			$smtpResponse = fgets($smtpConnect, 515);
			$output .= "PASS: $smtpResponse{$newLine}";

			//Email From
			fputs($smtpConnect, "MAIL FROM:<$from>" . $newLine);
			$smtpResponse = fgets($smtpConnect, 515);
			$output .= "FROM: $smtpResponse{$newLine}";

			//Email To
			fputs($smtpConnect, "RCPT TO:<$to>" . $newLine);
			$smtpResponse = fgets($smtpConnect, 515);
			$output .= "TO: $smtpResponse{$newLine}";

			//Email Body
			fputs($smtpConnect, "DATA" . $newLine);
			$smtpResponse = fgets($smtpConnect, 515);
			$output .= "DATA: $smtpResponse{$newLine}";

			//Construct Headers
			$headers = "MIME-Version: 1.0" . $newLine;
			$headers .= "Content-type: text/html; charset=iso-8859-1" . $newLine;
			$headers .= "To: $to" . $newLine;
			$headers .= "From: $from" . $newLine;

			//Send Test
			$subject = "AUTH TEST";
			$message = "Auth test ".date("Y-m-d h:i:s");
			fputs($smtpConnect, "To: $to\nFrom: $from\nSubject: $subject\n$message\n.\n");
			$smtpResponse = fgets($smtpConnect, 515);
			$output = $output . "MSG: $smtpResponse{$newLine}";

			// Say Bye to SMTP
			fputs($smtpConnect,"QUIT" . $newLine);
			$smtpResponse = fgets($smtpConnect, 515);
			$output = $output . "QUIT: $smtpResponse{$newLine}";

			return $output;
		}

		public static function checkSMTPEmail($from, $to){
			$validator = new SMTP_Validate_Email($to, $from);
			$smtp_results = $validator->validate();

			return $smtp_results;
		}
	}

	class MessageClass extends Swift_Message {
		/**
		 * usage: MessageClass::init();
		 */
		public static function init(){
			$s = self::newInstance();
			return $s;
		}
	}
}else{
	// Fallback to basic PHP mailer class
	define('MAILERCLASS', 'PHPMail');

	class MailerClass {
		/**
		 * usage: MailerClass::sendmail($to, $from, $subject, $message);
		 * @param string|array $to 				array(address => name)
		 * @param string|array $from			array(address => name)
		 * @param string $subject
		 * @param string $message
		 * @param string|array $headers
		 * @param string|array $params
		 * @return boolean
		 */
		public static function sendmail($to, $from, $subject, $message, $headers = null, $params = null){
			global $_events;

			list($abort) = $_events->processTriggerEvent(__FUNCTION__, array($to, $from, $subject), false);				// alert triggered functions when function executes
			if($abort) return false;

			if(!empty($from)) {
				$headers .= "From: ";
				if(is_array($from)){
					reset($from);
					$headers .= current($from)." <".key($from).">";
				}else{
					$headers .= $From;
				}
			}
			$ok = mail($to, $subject, $message, $headers, $params);
			$_events->processTriggerEvent(__FUNCTION__.'_done', $ok);				// alert triggered functions when function executes
		}
	}
}
?>