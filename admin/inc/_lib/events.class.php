<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
define ("EVENTSLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

// Ajax events
define("TF_LISTACTION", "list_action");
define("TF_CONTENTMACRO", "content_macro");
define("TF_DEF_CONFIGS", "def_configs");

define("TF_ADMIN_HEAD", "admin_head");
define("TF_ADMIN_TITLE", "admin_title");
define("TF_ADMIN_HEAD_JQUERY", "admin_head_jquery");
define("TF_ADMIN_SCRIPT_CALLED", "admin_script_called");
define("TF_ADMIN_STYLE_CALLED", "admin_style_called");
define("TF_ADMIN_PLUGIN_INCLUDED", "admin_plugin_included");
define("TF_ADMIN_BODY", "admin_body");
define("TF_ADMIN_CONTENTAREA", "admin_contentarea");
define("TF_ADMIN_FOOT", "admin_foot");

define("TF_WEB_HEAD_JQUERY", "web_head_jquery");

/* EVENTS CLASS
 *
 * Maintains the various register, trigger, action and pulse objects (collectively Events) used by the site
 */
class EventsClass {
	// overloaded data
	private $_trigger_events = array();
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control;

		$s = new self;
		$s->_db = $_db_control;
		return $s;
	}

	// ----------- TRIGGER QUEUEING FUNCTIONS ---------------

	/*
	TRIGGERS:

	A trigger is a three-part process whereby a trigger function is associated with a
	target via a triggering event.  The trigger function, usually included in a custom-
	developed plugin or other contributed code is registered with the system by way of
	a specific event.  The trigger target, usually a system operation but also can be
	established in contributed code as a developer target, is the programming that will
	be affected by the trigger function.  The trigger event binds the function and target.

	1) create trigger event
	2) set trigger function
	3) execute trigger
	*/

	/**
	 * Creates a new trigger event.  Once an event occurs, trigger functions
	 * are executed using executeTrigger() and their results are returned to
	 * the target.  The next two steps are setTriggerFunction() then executeTrigger().
	 * @param string $event         Name of event
	 * @param string $indata        The value that is passed to trigger
	 * @param mixed $defaultdata    The default value of the parameter if no trigger function is waiting or nothing is returned
	 * @return boolean              True if successfully set or false if not
	 */
	function createTriggerEvent($event, $indata = null, $defaultdata = null){
	    /*
	    Events are created as a root array element as follows:

        _trigger_events[eventname] => array(
            'indata' => string,
            'defaultdata' => string,
            'active' => boolean,
            'functions' => array(
                'functionname' => array('triggercode' => string, 'priority' => integer, 'active' => boolean),
            )

        The event element is unique and only created once.
	    */
	    if(!empty($event)){
	        $event = strtolower($event);
	        if(!isset($this->_trigger_events[$event])){
	            if(is_null($defaultdata) && !is_null($indata)) $defaultdata = $indata;
	            $this->_trigger_events[$event] = array("indata" => $indata, "defaultdata" => $defaultdata, "active" => true, "functions" => array());
	        }
	        return true;
	    }
	    return false;
	}

	/**
	 * Update the in and default data of an existing trigger event.
	 * @param string $event         Name of event
	 * @param string $indata        The value that is passed to trigger
	 * @param mixed $defaultdata    The default value of the parameter if no trigger function is waiting or nothing is returned
	 * @return boolean              True if successfully set or false if not
	 */
	function updateTriggerEvent($event, $indata = null, $defaultdata = null){
	    /*
	    Events are created as a root array element as follows:

        _trigger_events[eventname] => array(
            'indata' => string,
            'defaultdata' => string,
            'active' => boolean,
            'functions' => array(
                'functionname' => array('triggercode' => string, 'priority' => integer, 'active' => boolean),
            )

        The event element is unique and only created once.
	    */
	    if(!empty($event)){
	        $event = strtolower($event);
	        if(isset($this->_trigger_events[$event])){
	            if(is_null($defaultdata) && !is_null($indata)) $defaultdata = $indata;
	            $functions = $this->_trigger_events[$event]['functions'];
	            $this->_trigger_events[$event] = array("indata" => $indata, "defaultdata" => $defaultdata, "active" => true, "functions" => $functions);
	        }
	        return true;
	    }
	    return false;
	}

	/**
	 * A combination of createTriggerEvent()/updateTriggerEvent() and executeTrigger()
	 * @param string $event  		Name of the event
	 * @param string $indata        The value that is passed to trigger function
	 * @param mixed $defaultdata    The default value used if no trigger function is waiting or nothing is returned
	 * @param string $triggercode   A phrase or code used to add a layer of specificity to the event
	 * @return boolean|mixed        Either the result of the execution or false if failed
	 */
	function processTriggerEvent($event, $indata = null, $defaultdata = null, $triggercode = '', $single = false){
	    $retn = false;
	    if(!empty($event)){
	        $event = strtolower($event);

	        if(!isset($this->_trigger_events[$event]))
	        	$this->createTriggerEvent($event, $indata, $defaultdata);
	        else
	        	$this->updateTriggerEvent($event, $indata, $defaultdata);
	        $retn = $this->executeTrigger($event, $triggercode);
	    }
	    return $retn;
	}

	/**
	 * A simplified version of processTriggerEvent that returns just $retn
	 * It creates and executes the triggered event in one step
	 * @param string $event         Name of event
	 * @param string $indata        The value that is passed to trigger
	 * @param mixed $defaultdata    The default value of the parameter if no trigger function is waiting or nothing is returned
	 * @return boolean|mixed        Either the result of the execution or false if failed
	 */
	function processQuickTriggerEvent($event, $indata = null, $defaultdata = null){
	    $retn = $this->processTriggerEvent($event, $indata, $defaultdata, '', true);
	    if(is_array($retn)) $retn = array_shift($retn);
	    return $retn;
	}

	/**
	 * Return array of all events registered so far
	 * @return array
	 */
	function getTriggerEvents(){
		return $this->_trigger_events;
	}

	/**
	 * Return true if the event requested exists in register
	 * @param string $event
	 * @return boolean
	 */
	function doesTriggerEventExist($event){
		$event = strtolower($event);
		return (isset($this->_trigger_events[$event]));
	}

	/**
	 * Adds a trigger function to an associated event.  A trigger function
	 * can be called during either a server-side operation or an AJAX process.  Note that an exit
	 * or die in AJAX-initiated triggers have no effect on client-side scripts.
	 * This function is the second step and precursor to executeTrigger();
	 * @param string $event         The type of event
	 * @param string $func          The function to execute
	 * @param string $triggercode   The unique code that identifies the function to be executed
	 * @param array $args           Array of event arguments.
	 *                                  'halt_normal_process'... true to stop standard event handler processing
	 *                                  'priority'... priority of function in call stack for this event (0 [sooner] to 99 [later])
	 * @return boolean
	 */
	function setTriggerFunction($event, $func, $triggercode = null, $args = null){
	    global $_error;
	    /*
	    Trigger functions are set within the functions array element:

        _trigger_events[eventname] => array(
            'indata' => string,
            'defaultdata' => string,
            'active' => boolean,
            'functions' => array(
                'functionname' => array('triggercode' => string, 'priority' => integer, 'active' => boolean),
            )

        The functionname is unique.
	    */

	    $ok = false;
		if(!empty($func) && !empty($event)){
	        $event = strtolower($event);

	        // Late registration of event.  Normally the event is created earlier
	        if(!isset($this->_trigger_events[$event]))
	        	$this->createTriggerEvent($event);

	        if(isset($this->_trigger_events[$event])) {
	        	if(!isset($this->trigger_events[$event]['functions'][$func])){
		            // is function name an array then convert to class::method
		            $func = stringifyClassMethodArray($func);
		            $function_callable = $method_callable = false;

		            if(is_string($args) && !empty($args)) {
		            	// query string (name=val&name2=val&name3=val)
		                parse_str($args, $aargs);
		                if(is_array($aargs))
		                	$args = $aargs;
		                else
		                	$args = array();
		            }
		            if(is_array($args) || empty($args)){
		                if(isset($args['priority'])){
		                    $priority = floatval($args['priority']);
		                    if($priority < 0 || $priority > 99) $priority = 50;
		                    unset($args['priority']);
		                }else{
		                    $priority = 50;
		                }
		                if(isset($args['halt_normal_process'])){
		                	$args['halt_normal_process'] = (bool) $args['halt_normal_process'];
		                }else{
		                	$args['halt_normal_process'] = false;
		                }
		                if(isset($args['passthru'])){
		                	$args['passthru'] = (bool) $args['passthru'];
		                }else{
		                	$args['passthru'] = false;
		                }

		                $this->_trigger_events[$event]['functions'][$func] = array("triggercode" => $triggercode, "priority" => $priority, "params" => $args, "active" => true);
		                $ok = true;
		            }else{
		                trigger_error(__FUNCTION__." accepts only array, queried string or null value allowed for trigger function parameters.");
		            }
		        }
	        }else{
	            // $f = $_error->getCallingMethod(1);
	            // trigger_error("The trigger event '".$event."' is not registered before being called.  Called from ".$f['function']." line ".$f['line']." in file ".$f['file']);
	        }
		}else{
			trigger_error(__FUNCTION__." requires a function and event.");
		}
	    return $ok;
	}

	/**
	 * Loads a system function, as trigger function, into the register.  A Trigger Action
	 * establishes a specific core function as the handler of a triggered event, instead of a typical custom function.
	 * @param string $event         The type of event
	 * @param array $args           Array of event arguments.
	 *                                  'halt_normal_process'... true to stop standard event handler processing
	 *                                  'priority'... priority of function in call stack for this event (0 [sooner] to 99 [later])
	 * @return boolean
	 */
	function setTriggerAction($event, $func, $args = null){
	    if(!empty($event) && !empty($func)){
	        $event = strtolower($event);
	        if(substr($event, 0, 8) != 'action::') $event = 'action::'.$event;
	        $this->setTriggerFunction($event, $func, "", array($args));
	    }
	}

	/**
	 * Alias of setTriggerFunction();
	 * @see setTriggerFunction
	 */
	function __sf($event, $func, $triggercode = null, $args = null){
	    $this->setTriggerFunction($event, $func, $triggercode, $args);
	}

	/**
	 * Unload a trigger function from the register
	 * @param string $event         The type of event
	 * @param string $func          Name of function
	 * @return boolean
	 */
	function unsetTriggerFunction($event, $func){
	    $ok = true;
	    $func = stringifyClassMethodArray($func);
		if(!empty($func) && !empty($event)){
	        $event = strtolower($event);
	        if(isset($this->_trigger_events[$event])){
	            if(isset($this->_trigger_events[$event]["functions"][$func])){
	                unset($this->_trigger_events[$event]["functions"][$func]);
	                $ok = true;
	            }
	        }
		}else{
			trigger_error("Function name and/or event missing for ".__FUNCTION__);
		}
	    return $ok;
	}

	/**
	 * Alias of unsetTriggerFunction();
	 * @see unsetTriggerFunction
	 */
	function __usf($event, $func){
	    return $this->unsetTriggerFunction($event, $func);
	}

	/**
	 * Unload all registered trigger functions associated with an event or array of events.
	 * This has the effect of killing pending queued functions before they can be triggered
	 * @param mixed $events          A single event or list of triggering events to dequeue
	 */
	function unsetAllTriggerFunctionsByEvent($events){
	    $ok = false;
	    if(is_array($events)){
	        if(!empty($events)){
	            foreach($events as $event){
	                $event = strtolower($event);
	                if(isset($this->_trigger_events[$event])) {
	                    unset($this->_trigger_events[$event]);
	                    $ok = true;
	                }
	            }
	        }else{
	            trigger_error("List of events is empty for ".__FUNCTION__);
	        }
	    }elseif(is_string($events)){
	        $events = strtolower($events);
	        if(isset($this->_trigger_events[$event])) {
	            unset($this->_trigger_events[$event]);
	            $ok = true;
	        }
	    }else{
	        trigger_error("An event or list of events is missing for ".__FUNCTION__);
	    }
	    return $ok;
	}

	/**
	 * Temporarily deactivate an active trigger function in the register
	 * @param string $event         The type of event
	 * @param string $func          Name of function
	 * @return boolean
	 */
	function suspendTriggerFunction($event, $func){
	    $ok = false;
	    $func = stringifyClassMethodArray($func);
		if(!empty($func) && !empty($event)){
	        $event = strtolower($event);
	        if(isset($this->_trigger_events[$event])){
	            if(isset($this->_trigger_events[$event]["functions"][$func])){
	                $this->_trigger_events[$event]["functions"][$func]["active"] = false;
	                $ok = true;
	            }
	        }
		}else{
			trigger_error("Function name and/or event missing for ".__FUNCTION__);
		}
	    return $ok;
	}

	/**
	 * Alias of suspendTriggerFunction();
	 * @see suspendTriggerFunction
	 */
	function __stf($event, $func){
	    return $this->suspendTriggerFunction($event, $func);
	}

	/**
	 * Reactivate a suspended trigger function in the register
	 * @param string $event         The type of event
	 * @param string $func          Name of function
	 * @return boolean
	 */
	function resumeTriggerFunction($event, $func){
	    $ok = false;
	    $func = stringifyClassMethodArray($func);
	    if(!empty($func) && !empty($event)){
	        $event = strtolower($event);
	        if(isset($this->_trigger_events[$event])){
	            if(isset($this->_trigger_events[$event]["functions"][$func])){
	                $this->_trigger_events[$event]["functions"][$func]["active"] = true;
	                $ok = true;
	            }
	        }
	    }else{
	        trigger_error("Function name and/or event missing for ".__FUNCTION__);
	    }
	    return $ok;
	}

	/**
	 * Alias of resumeTriggerFunction();
	 * @see resumeTriggerFunction
	 */
	function __rtf($event, $func){
	    return $this->resumeTriggerFunction($event, $func);
	}

	/**
	 * Retrieve an array containing function data (function, trigger, parameters (JSON), and priority)
	 * related to trigger command
	 * @param string $event         The type of event
	 * @param string $triggercode   The unique function code
	 * @param boolean $active       True of false
	 */
	function getTriggerFunctions($event, $triggercode = null, $active = true){
	    $functions = array();
	    if(!empty($event)){
	        $event = strtolower($event);
	        if(isset($this->_trigger_events[$event])){
	            if(!empty($this->_trigger_events[$event])){
	                $f = array();
	                foreach($this->_trigger_events[$event]["functions"] as $funcname => $func){
	                    if($func["active"] == $active){
	                        if($func["triggercode"] == $triggercode || empty($triggercode)){
	                            $priority = ((isset($func['priority'])) ? floatval($func['priority']) : 50);
	                            $incr = 1;
	                            while(isset($f[$priority])) {
	                            	$priority = intval($priority) + $incr / 100;
	                            	$incr++;
	                            }
	                            $f[$priority] = array($funcname, $func);
	                        }
	                    }
	                }
	                ksort($f);  // priority sort 0 to 99
	                foreach($f as $func) $functions[$func[0]] = $func[1];
	            }
	        }
	    }else{
	        trigger_error("Event is missing for ".__FUNCTION__);
	    }
		return $functions;
	}

	/**
	 * Alias of getTriggerFunctions();
	 * @see getTriggerFunctions
	 */
	function __gtf($event, $triggercode = null, $active = true){
	    $event = strtolower($event);
	    return $this->getTriggerFunctions($event, $triggercode, $active);
	}

	/**
	 * Trap and process a trigger function.  This function is the postcursor to setTriggerFunction()
	 * and createTriggerEvent().
	 * @param string $event         The type of event
	 * @param string $triggercode   The triggercode that is unique to the call function
	 *                                  (or blank for all functions registered to event)
	 * @param array $params         List of parameters in array form that will be passed to function
	 * @param string $callfile      Required if $triggercode is blank
	 * @return array                An array containing three components:
	 *                              - Returned data
	 *                              - Process continuation (default = true)
	 *                              - Data passthrough instruction (default = false)
	 *                              Note: some trigger events will always echo data here
	 */
	function executeTrigger($event, $triggercode = '', $params = array()){
	    global $_db_control;

		$retndata = null;
		$continue_normal_process = true;
	    $passthru = false;
	    $objs = array();

		$_eventdata = array(
			'retndata' => null
		);

	    if(empty($event)) {
	        trigger_error(__FUNCTION__." requires an event.");
	        return false;
	    }
	    $event = strtolower($event);

	    // Inherit global variables (not superglobals as they remain global) in preparation for the included file
	    foreach($GLOBALS as $key => $val){
	        if(!in_array($key, array('GLOBALS', '_SERVER', '_COOKIE', '_SESSION', '_GET', '_POST', '_FILES', '_REQUEST', '_ENV'))){
	            $$key = $val;
	        }
	    }

	    // Events are effected by one or more unique trigger functions,
	    // and can be called specifically by using the triggercode

        // _trigger_events[eventname] => array(
        //     'indata' => string,
        //     'defaultdata' => string,
        //     'active' => boolean,
        //     'functions' => array(
        //         'functionname' => array('triggercode' => string, 'priority' => integer, 'active' => boolean),
        //     )
        //
	    // event data prepared by the createTriggerEvent() function
	    //      indata = data passed to trigger function
	    //      defaultdata = data returned if no trigger function exists
	    //
	    // The retndata value is data that is returned to the event function (can be indata, defaultdata or
	    //		data modified by the trigger function).  By default it is set to the defaultdata value from the event
	    //
	    // Each trigger function must have already been included during normal loading (i.e. in a plugin, theme or app)

	    if(isset($this->_trigger_events[$event])){
		    $_eventdata = $this->_trigger_events[$event];
		    $_eventdata_arg_type = gettype($_eventdata['indata']);
		    $_eventdata['retndata'] = $retndata = $_eventdata['defaultdata'];

		    // get all active trigger function(s) sorted by priority
		    $functions = $this->getTriggerFunctions($event, $triggercode, true);

		    if(!empty($functions) && true == $_eventdata['active']){
		        // a trigger function was registered as respondent to event
		        foreach($functions as $function => $fn_args){
		            $function_callable = false;
		            $static_method_callable = false;
		            $public_method_callable = false;
		            if(strpos($function, "::") !== false){
		                $method = explode("::", $function);           // a trigger static class::method (class must be initiated)
		                $static_method_callable = (count($method) == 2 && method_exists($method[0], $method[1]));
		                if($static_method_callable) $_refl = new ReflectionMethod($method[0], $method[1]);
		            }elseif(strpos($function, "->") !== false){
		                $method = explode("->", $function);           // a trigger declared class->method (class must be initiated)
		                $public_method_callable = (count($method) == 2 && method_exists($method[0], $method[1]));
		                if($public_method_callable) $_refl = new ReflectionMethod($method[0], $method[1]);
		            }else{
		                $function_callable = is_callable($function);
		            }

		            if($function_callable || $static_method_callable || $public_method_callable){
			            $fn_params = $fn_args['params'];
		                if(is_array($fn_params)){
		                    // is the 'halt_normal_process' flag part of the function's parameters?
		                    // if so, developer wants this function to stop the normal processing from continuing.
		                    // it is up to the event function to honor this attribute.
		                    if(isset($fn_params['halt_normal_process'])){
		                        $continue_normal_process = !((bool) $fn_params['halt_normal_process']);
		                        unset($fn_params['halt_normal_process']);
		                    }

		                    // is the 'passthru' flag part of the function's parameters?
		                    // if so, developer wants this function to pass the data to the originating call file
		                    // i.e. if event is 'showcontentarea', which is handled by showContentArea,
		                    // the triggered function will return its modification of the event data back to showContentArea
		                    // but instead of showContentArea echoing it, it will pass the data through to the file that called
		                    // showContentArea (typically a list, edit or add page) for storage in a variable.
		                    // This saves the need to output buffer the data.
		                    if(isset($fn_params['passthru'])){
		                        $passthru = (bool) $fn_params['passthru'];
		                        unset($fn_params['passthru']);
		                    }
		                }

		                // add event data to the remaining fn_params
		                if(!is_array($_eventdata['indata'])) {
		                	$_eventdata['indata'] = array($_eventdata['indata']);
		                }
		                if(!empty($fn_params) && is_array($fn_params))
		                    $fn_params = array_merge($fn_params, $_eventdata['indata']);
		                else
		                    $fn_params = $_eventdata['indata'];

		                // if the indata was an array then the params will be new nested array
		                if($_eventdata_arg_type == 'array') $fn_params = array($fn_params);

		                // include function arguments with params from script_form js file
		                if(is_array($fn_params)) {
		                	$params = array_merge($params, $fn_params);
		                }

		                // prepend event name to params array
		                array_unshift($params, $event);

		                // call the trigger function/method and get the result
		                // params may be loaded with additional args
		                $obj = array();
		                $retn = null;
		                if($function_callable){
		                	// procedural function
		                    $obj['object'] = null;
		                    $obj['trigger_function'] = $function;
		                    $retn = call_user_func_array($function, $params);
		                }elseif($static_method_callable){
		                	// static method ($method is string like class::method)
		                    $obj['object'] = null;
		                    $obj['trigger_function'] = $method;
		                    $retn = call_user_func_array($method, $params);
		                }elseif($public_method_callable){
		                	// declared public method ($method is array like 0 => class, 1 => method)
		                    $obj['object'] = new $method[0]();
		                    $obj['trigger_function'] = $method[1];
		                    $retn = call_user_func_array(array($obj['object'], $method[1]), $params);
		                }
		                $objs[] = $obj;

		                // update the event data with the result of the function call (if applicable)
		                $retn_arg_type = gettype($retn);
		                if($_eventdata_arg_type != $retn_arg_type){
		                    // coerce typecasting of the return data to that of the event data
		                    switch($_eventdata_arg_type){
		                        case "boolean":
		                        case "bool":
		                            $retn = (bool) $retn;
		                            break;
		                        case "string":
		                            $retn = (string) $retn;
		                            break;
		                        case "integer":
		                            $retn = intval($retn);
		                            break;
		                        case "double":
		                            $retn = floatval($retn);
		                            break;
		                        case "array":
		                            //$retn = array($retn);
		                            break;
		                        case "object":
		                            $retn = (object) $retn;
		                            break;
		                        case "null":
		                            $retn = null;
		                            break;
		                        default:
		                            break;
		                    }
		                }

		                // merge current return data with saved return data
		                if(!empty($retn)){
			                if(empty($retndata)){
			                	// nothing saved so far
			                	$retndata = $retn;
			                }elseif(is_array($retn)){
			                	// new data is an array
			                	if(is_array($retndata)){
			                		$r = $retndata;
			                		$retndata = array($r, $retn);
			                	}else{
			                		$retndata = array_merge(array($retndata), $retn);
			                	}
			                }elseif(is_string($retn)){
			                	// new data is a string
			                	if(is_array($retndata)){
			                		$retndata = array_merge($retndata, array($retn));
			                	}else{
			                		$retndata = strval($retndata) . $retn;
			                	}
			                }else{
			                	// new data is a number
			                	if(is_array($retndata)){
			                		$retndata = array_merge($retndata, array($retn));
			                	}else{
			                		$retndata = floatval($retndata) + $retn;
			                	}
			                }
			            }
		            }
		        }
		    }
		}
        $_eventdata['retndata'] = $retndata;
	    return array($_eventdata['retndata'], $continue_normal_process, $passthru, $objs);
	}

	/**
	 * Alias of executeTrigger();
	 * @see executeTrigger
	 */
	function __xtf($event, $triggercode = '', $params = array()){
	    return $this->executeTrigger($event, $triggercode, $params);
	}

	// ----------- CONTENT MACRO QUEUING FUNCTIONS ---------------
	// Content Macros are a special group of Triggers that occur when content-displaying events occur

	/**
	 * Add a content macro function to the register
	 * @param string $function      The trigger function to register
	 * @param string $code          The unique code that identifies the function
	 * @param array $args           A list of arguments to pass to function
	 * @param integer $priority     Priority of execution (1=first...)
	 * @return boolean
	 */

	function setMacro($function, $code = null, $args = array(), $priority = 50){
		if(!is_array($args)) $args = (array) $args;
		$args['priority'] = $priority;
	    return $this->setTriggerFunction(TF_CONTENTMACRO, $function, $code, $args);
	}

	/**
	 * Alias of setMacro();
	 * @see setMacro
	 */
	function __sm($function, $code = null, $args = array(), $priority = 50){
		if(!is_array($args)) $args = (array) $args;
		$args['priority'] = $priority;
	    return $this->setMacro($function, $code, $args, $priority);
	}

	/**
	 * Remove a content macro function from the register
	 * @param string $function      The name of the function
	 * @return boolean
	 */
	function unsetMacro($function){
	    return $this->unsetTriggerFunction(TF_CONTENTMACRO, $function);
	}

	/**
	 * Alias of unsetMacro();
	 * @see unsetMacro
	 */
	function __usm($function){
	    return $this->unsetTriggerFunction(TF_CONTENTMACRO, $function);
	}

	/**
	 * Execute a registered content macro function
	 * @param string $triggercode   The unique code that identifies the function
	 * @param string $args          A list of arguments to pass to function
	 * @return string               Either the result of the call or <!-- CONTENTMACRO: ... --> if failed
	 */
	function executeMacro($triggercode, $args){
	    // function and args are derived originally from {function name=value, name=value,...}
	    // where args = parsed set of name/value pairs
	    if($triggercode != ''){
	        list($content) = $this->processTriggerEvent(TF_CONTENTMACRO, null, $args);
	        return (($content !== false) ? $content : '<!-- CONTENTMACRO: ERR -->');
	    }
	    return "<!-- CONTENTMACRO: FUNC MISSING -->";
	}

	/**
	 * Alias of executeMacro();
	 * @see executeMacro
	 */
	function __xm($triggercode, $args){
	    return $this->executeMacro($triggercode, $args);
	}
}

?>