<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

/* DATABASE CONNECTION CLASS - MySQL/MySQLi 3.3+ */

/**
 * DB_CONNECTION
 * Performs database engine connecting and database object loading
 */
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

class Connection {
	protected $db_conn;

    /**
     * Instantiate the connection class
     */
	public function __construct() {
  	}

	public function getConnection($db_username=null, $db_password=null, $db_database=null, $db_host=null, $db_port=null){
        // Get a new MySQLi connection object
		$this->db_conn = new mysqli($db_host, $db_username, $db_password, $db_database, $db_port);

		// Check for connection errors
		if (mysqli_connect_errno() || !$this->db_conn){
			$this->db_fatalError(mysqli_connect_error());
	  	}

		return $this->db_conn;
	}

	protected function db_fatalError($db_message) {
		die("Connection Fatal Error: " . $db_message);
  	}
}
?>