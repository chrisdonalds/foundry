<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

/* DATABASE WRAPPER CLASS - MySQL/MySQLi 3.3+ */

/**
 * DB_WRAPPER
 * Performs database operations (MySQL engine 3.3+)
 * - table
 * - child_table
 * - parent_table
 * - num_rows
 * - savequery
 * - lastquery
 */
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");
define ("DBWRAPLOADED", true);

require_once(SITE_PATH.ADMIN_FOLDER.LIB_FOLDER.'db_'.DB_ENGINE.'/db_connection.class.php');

class DB_wrapper extends Connection {
	/* variables */
	private $_keys = array("table" => "", "child_table" => "", "parent_table" => "",
						   "num_rows" => 0, "savequery" => false, "lastquery" => "",
						   "result_type" => DB_ARRAY);
	public $db;
	public $conn;
	private $callingClass;

	/* class object variables */

	/**
     * Instantiate the DB class object
     */
	public function __construct() {
   	}

   	public function setCallingClass($class){
   		$this->callingClass = $class;
   	}

	public function getQuery($query) {
		$result = $this->db->query($query);
		if ($result !== false) {
			$array = $this->loadDataArray($result);
			$this->_keys['num_rows'] = count($array);
			return $array;
		}elseif(!is_null($this->db->error)){
			$this->log_error($query);
			return false;
		}else{
			return array();
		}
	}

	public function executeQuery($query) {
		$result = $this->db->query($query);
		if ($result !== false) {
			return true;
		}elseif(!is_null($this->db->error)){
			$this->log_error($query);
			return false;
		}else{
			return false;
		}
	}

	// this would only work the best for a small amount of data.
	// use getTotalRows instead
	public function getNumRows($query = "") {
		if($query != ""){
			// execute a new query
			$array = $this->getQuery($query);
			return $this->_keys['num_rows'];
		}else{
			// return num rows of last query.  saves db engine workload
			return $this->_keys['num_rows'];
		}
	}

	//only return the 'total' data
	public function getTotalRows($query) {
		$array = $this->getQuery($query);
		return $array[0]['total'];
	}

	public function getTotalAffectedRows() {
		return parent::get_total_affected_rows($this->db);
	}

	public function updateQuery($query) {
		if($result = $this->executeQuery($query)){
			return 1;
		}else{
			$this->log_error($query);
			return 0;
		}
	}

	public function insertQuery($query) {
		if($result = $this->executeQuery($query)){
			return $this->db->insert_id;
		}else{
			$this->log_error($query);
			return 0;
		}
	}

	public function deleteQuery($query) {
		if($result = $this->executeQuery($query)){
			return 1;
		}else{
			$this->log_error($query);
			return 0;
		}
	}

	public function getDBInfo(){
		$info = $this->db->stat();
		if(!is_array($info)) $info = explode("  ", $info);
		return $info;
	}

	public function db_close() {
		$this->db->close();
	}

	public function loadDataArray($result) {
		$data_array = array();

		$row_count = 0;
		while ($row = $result->fetch_assoc()) {
			foreach ($row as $key => $value) {
				//collect the data to be exported
				$data_array[$row_count][$key] = $value;
			}
			$row_count ++;
		}

		$result->close();	/* free result set */
		return $data_array;
	}

	public function cleanQuery($sql){
		return $this->db->real_escape_string($sql);
	}

	public function dbError() {
		return $this->db->error();
	}

	public function log_error($query) {
		$stack = debug_backtrace();
		foreach($stack as $trace){
			$file = getIfSet($trace['file']);
			$line = getIfSet($trace['line']);
			if(strpos($file, "_lib\db") === false){
				// submitErrorLog(strucDBErrorMsg($file, $line, $query, $this->db->error));
				$errormsg = "DB ERROR in filename: ".$file." at Line: ".$line." (".str_replace(array("\\n", "\\r", "\\n\\r"), "", $query).") Problem: ".$this->db->error.PHP_EOL;
				error_log($errormsg, 0);
				return false;
			}
		}
	}

	// magic functions
	public function __get($name) {
		$v = null;
		if(array_key_exists($name, $this->_keys)){
			// return scalar value
			$v = $this->_keys[$name];
		}
		return $v;
	}

	public function __set($name, $value) {
		switch($name){
			case "table":
			case "child_table":
			case "parent_table":
				// record the data table(s) used by the calling page
				// a typical use is the "executelistaction" function in ajaxwrapper
				$this->_keys[$name] = $value;
				$alias = $_SERVER['REQUEST_URI'];
				if(strpos($alias, INC_FOLDER) === false){
					if(isset($_SESSION['register'][$alias])) unset($_SESSION['register'][$alias]);
					$_SESSION['register'][$alias] = array(
						"alias" => $alias,
						"db_".$name => $value
					);
					$this->callingClass->setTable(REGISTER_TABLE)->setFieldvals(array("type" => "db", "alias" => $alias, "db_".$name => $value))->setWhere("`type` = 'db' AND `alias` = '$alias'")->replaceRec();
				}
				break;
			case "num_rows":
				$this->_keys[$name] = $value;
				break;
			case "lastquery":
				$this->_keys[$name] = str_replace(array("\n", "\r", "\t"), array("", "", " "), $value);
				$this->_keys['savequery'] = false;
				break;
			case "savequery":
				if($value === false || $value === true) $this->_keys[$name] = $value;
				break;
			case "result_type":
				if(in_array($value, array(DB_ARRAY, DB_OBJECT))) $this->_keys[$name] = $value;
				break;
			default:
				break;
		}
	}
}
?>