<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

/* MENUS CLASS
 *
 * Handles all rendering and processing of admin and website menus
 */
define ("MENULIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

define ("DEF_IMG_LOGO_SYS", "system");
define ("DEF_IMG_LOGO_CUSTOM", "custom");
define ("MENU_MAIN_SLOT", 1);
define ("MENU_TARGETTYPE_DATA", "content");
define ("MENU_TARGETTYPE_TAXONOMY", "taxonomy");
define ("MENU_TARGETTYPE_EDITOR", "edit");
define ("MENU_TARGETTYPE_LIST", "list");
define ("MENU_TARGETTYPE_TAXTERM", "term");
define ("MENU_TARGETTYPE_TERM", "term");
define ("MENU_TARGETTYPE_FAQ", "faq");
define ("MENU_TARGETTYPE_USER", "user");
define ("MENU_TARGETTYPE_LINK", "link");
define ("MENU_TARGETTYPE_CUSTOM", "custom");

class MenuClass {
	// overloaded data
	private $_menus = array();
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'_menus');				// alert triggered functions when function executes
		return $s;
	}

	// ----------- WEBSITE MENU AND NAVIGATION FUNCTIONS ---------------

	// RECORDS

	/**
	 * Load menu records as a tree from the database
	 * @param integer $parent_id        ID of parent menu
	 * @param array $menus              Containing menu or blank array
	 */
	function getWebsiteMenuTree($menubar_key, $parent_id = -1, $skipself = false){
	    global $_db_control;

	    $menubar = $this->getWebsiteMenubars($menubar_key);
	    if(count($menubar) > 0)
	        $menu_bar_id = $menubar[0]['id'];
	    else
	        $menu_bar_id = 0;

	    $menurecs = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '0' AND `menu_bar_id` = '".$menu_bar_id."' AND `menu_class` = 'menu'".(($parent_id >=0) ? " AND `parent_id` = '$parent_id'" : ""))->getTree(array('skipself' => $skipself));

	    return $menurecs;
	}

	/**
	 * Load menu records from the database
	 * @param string|integer $menubar_key 	Key or id of menubar
	 * @param integer $parent_id        	ID of parent menu
	 * @param array $menus              	Containing menu or blank array
	 */
	function getWebsiteMenuRecords($menubar_key = null, $args = array(), $menus = array()){
	    global $_db_control;

	    if(is_numeric($menubar_key)){
	    	$menu_bar_id = $menubar_key;
	    }elseif(!empty($menubar_key)){
		    $menubar = $this->getWebsiteMenubars($menubar_key);
		    if(count($menubar) > 0)
		        $menu_bar_id = $menubar[0]['id'];
		    else
		        $menu_bar_id = 0;
		}else{
			$menu_bar_id = -1;
		}

		// arguments
		$argskeys = array(
			'parent_id' => 0,
			'parent_key' => '',
			'table' => '',
			'targettype' => '',
			'taxonomy_id' => 0,
			'active' => 1,
			'sites' => null
		);
		$argsvals = getIfSetArray($args, $argskeys);
		$argsstr = null;
		foreach($argskeys as $argskey => $v){
			if(!empty($argsvals[$argskey])) $argsstr .= " AND `".$argskey."` = '".$argsvals[$argskey]."'";
		}

		if($menu_bar_id >= 0)
		    $menurecs = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '0' AND `menu_bar_id` = '".$menu_bar_id."' AND `menu_class` = 'menu'".$argsstr)->setOrder("sort_order")->getRec();
		else
		    $menurecs = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '0' AND `menu_class` = 'menu'".$argsstr)->setOrder("sort_order")->getRec();

	    if(is_array($menus) && count($menurecs) > 0){
	        foreach($menurecs as $menurec){
	            $menus[$menurec['key']] = array(
	                "id" => $menurec['id'],
	                "menu_bar_id" => $menurec['menu_bar_id'],
	                "menu_class" => $menurec['menu_class'],
	                "parent_id" => $menurec['parent_id'],
	                "title" => $menurec['title'],
	                "key" => $menurec['key'],
	                "alias" => $menurec['alias'],
	                "target" => $menurec['target'],
	                "table" => $menurec['table'],
	                "taxonomy_id" => $menurec['taxonomy_id'],
	                "targettype" => $menurec['targettype'],
	                "restricted" => ($menurec['restricted'] == 1),
	                "window" => $menurec['window'],
	                "active" => ($menurec['active'] == 1),
	                "custom" => ($menurec['custom'] == 1),
	                "childmenus" => null
	            );
	            if($menurec['id'] > 0){
	                $childmenurecs = $this->getWebsiteMenuRecords($menubar_key, $menurec['id']);
	                if(!is_null($childmenurecs)) $menus[$menurec['key']]['childmenus'] = $childmenurecs;
	            }
	        }
	        return $menus;
	    }
	    return null;
	}

	/**
	 * Save menu collection back to menu record database
	 * @param integer $parent_id        ID of parent menu
	 * @param array $menus              Containing menu or blank array
	 */
	function saveWebsiteMenuRecords($parent_id = 0, $menus = array()){
	    global $_db_control;

	    if(is_array($menus) && count($menus) > 0){
	        foreach($menus as $key => $menu){
	            if(!isset($menu['parent_id'])) $menu['parent_id'] = 0;
	            if(!isset($menu['active'])) $menu['active'] = 1;
		        if(!isset($menu['table'])) $menu['table'] = null;
	            $_db_control->setTable(MENUS_TABLE)->setFieldvals(array("menu_bar_id" => $menu['menu_bar_id'], "parent_id" => $menu['parent_id'], "table" => $menu['table'], "title" => $menu['title'], "alias" => $menu['alias'], "taxonomy_id" => $menu['taxonomy_id'], "target" => $menu['target'], "targettype" => $menu['targettype'], "restricted" => $menu['restricted'], "window" => $menu['window'], "in_admin" => "0", "active" => $menu['active'], "custom" => $menu['custom']))->setWhere("`key` = '$key' AND `in_admin` = 0")->replaceRec();
	            if(is_array($menu['childmenus']) && count($menu['childmenus']) > 0){
	                $this->saveWebsiteMenuRecords($menu['parent_id'], $menu['childmenus']);
	            }
	        }
	    }
	    return true;
	}

	/**
	 * Save a single menu object back to menu record database
	 * @param string $key               Key of menu
	 * @param array $menus              Menu object
	 */
	function saveWebsiteMenuRecord($key, $menu){
	    global $_db_control, $_events;

	    if(is_array($menu)){
	        if(!isset($menu['parent_id'])) $menu['parent_id'] = 0;
	        if(!isset($menu['active'])) $menu['active'] = 1;
	        if(!isset($menu['table'])) $menu['table'] = null;
	        $_db_control->setTable(MENUS_TABLE)->setFieldvals(array("key" => $key, "menu_bar_id" => $menu['menu_bar_id'], "parent_id" => $menu['parent_id'], "table" => $menu['table'], "title" => $menu['title'], "alias" => $menu['alias'], "taxonomy_id" => $menu['taxonomy_id'], "target" => $menu['target'], "targettype" => $menu['targettype'], "restricted" => $menu['restricted'], "window" => $menu['window'], "in_admin" => "0", "active" => $menu['active'], "custom" => $menu['custom']))->setWhere("`key` = '$key' AND `in_admin` = 0")->replaceRec();
			$_events->processTriggerEvent(__FUNCTION__, $key);				// alert triggered functions when function executes
	        return true;
	    }
	}

	// MENUBARS

	/**
	 * Return an array of menuslots for the selected theme
	 * @param string $themeincl  		The slug of the theme, or the primary active theme if empty
	 * @return array
	 */
	function getWebsiteMenuSlots($themeincl = null){
	    global $_themes;

	    // get slots for selected theme
	    $slots = $_themes->getWebsiteThemeInfo($themeincl, 'menuslots');
	    if(!isJSONString($slots)) return array(); 		// DB slots list is not a JSON string
	    $slots = json_decode($slots, true);
	    return $slots;
	}

	/**
	 * Return the menubar object that has been assigned to a specific location slot
	 * @param string $themeincl  		The slug of the theme, or the primary active theme if empty
	 * @param string $slot 				The name of the menuslot
	 * @return mixed 					The menubar object or null if nothing has been assigned
	 */
	function getWebsiteMenubarInSlot($themeincl, $slot){
	    global $_db_control, $_themes;

	    // get slots for selected theme
	    $slots = $_themes->getWebsiteThemeInfo($themeincl, 'menuslots');
	    if(!isJSONString($slots)) return null; 		// DB slots list is not a JSON string
	    $slots = json_decode($slots, true);
	    $slot_id = array_search($slot, $slots);
	    if($slot_id === false) return null; 	// chosen slot is not in theme slots list

        $menubar = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '0' AND `menu_class` = 'menubar' AND `parent_id` = '".$slot_id."'")->setLimit()->getRec();
        if(count($menubar) > 0)
        	$menubar = $menubar[0];
       	else
       		$menubar = null;
	    return $menubar;
	}

	/**
	 * Return a specific website menubar object or all website menubars as a collection array
	 * @param string|integer $key       The menubar key or id
	 * @return array
	 */
	function getWebsiteMenubars($key = ''){
	    global $_db_control;

	    if(is_numeric($key))
	        $menubars = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '0' AND `menu_class` = 'menubar' AND `active` = 1".(($key > 0) ? " AND `id` = '$key'" : ""))->getRec();
	    else
	        $menubars = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '0' AND `menu_class` = 'menubar' AND `active` = 1".(($key != '') ? " AND `key` = '$key'" : ""))->getRec();
	    return $menubars;
	}

	/**
	 * Return menubar info
	 * @param integer $id 				The ID or key of the menubar
	 * @return mixed 					The menubar record or null if not found
	 */
	function getWebsiteMenubarInfo($id){
		global $_db_control;

		$key = $id;
		$id = intval($id);
		$info = null;
		if($id > 0){
			$info = $_db_control->setTable(MENUS_TABLE)->setFields("id, title, descr, parent_id")->setWhere("`in_admin` = '0' AND `id` = '".$id."'")->setLimit()->getRec();
			if(count($info) == 0) $info = null;
		}elseif(!empty($key)){
			$info = $_db_control->setTable(MENUS_TABLE)->setFields("id, title, descr, parent_id")->setWhere("`in_admin` = '0' AND `key` = '".$key."'")->setLimit()->getRec();
			if(count($info) == 0) $info = null;
		}
		return $info;
	}

	/**
	 * Saves the menubar data to the database
	 * @param array $args 				Data array. id (int), title (str), descr (str), parent_id (int)
	 * @return array(success, id, key) 	Returns -1 if saved, 1 if updated, and false if not successful
	 */
	function saveWebsiteMenubar($args = array()){
		global $_events, $_db_control;

		$id = getIntvalIfSet($args['id']);
		$title = sanitizeText(getIfSet($args['title']));
		$descr = sanitizeText(getIfSet($args['descr']));
		$parent_id = getIntvalIfSet($args['parent_id']);

        $save_array = array(
        	"menu_class" => "menubar",
        	"in_admin" => 0,
        	"targettype" => ""
        );
        if(isset($args['title'])) $save_array['title'] = $title;
        if(isset($args['descr'])) $save_array['descr'] = $descr;
        if(isset($args['parent_id'])) $save_array['parent_id'] = $parent_id;
		$key = null;

		$retn = false;
		if(!empty($title)){
	        if($id > 0){
	        	// update
	            if($_db_control->setTable(MENUS_TABLE)->setFieldvals($save_array)->setWhere("`id` = '".$id."'")->updateRec()){
	                $_events->processTriggerEvent('updateWebsiteMenubarInfo', $id);
	                $retn = 1;
	            }
	        }else{
	        	// save
		        $menubar_keys = $_db_control->flattenDBArray($_db_control->setTable(MENUS_TABLE)->setFields("`id`, `key`")->setWhere("`menu_class` = 'menubar' AND `in_admin` = 0")->getRec(), "id", "key");
	            $key = getUniqueKey(codify($title), $menubar_keys);
	            if($id = $_db_control->setTable(MENUS_TABLE)->setFieldvals($save_array)->insertRec()){
	                $_events->processTriggerEvent('saveWebsiteMenubarInfo', $id);
	                $retn = -1;
	            }
	        }
	  	}
	  	return array($retn, $id, $key);
	}

	// MENUS

	/**
	 * Return a specific website menu array object or entire website menu collection from the menu record array
	 * @param string|integer $menubar   The menubar key or id
	 * @param string $key               The menu key (optional)
	 * @param integer $parent_id        The parent menu id (optional)
	 * @return array
	 */
	function getWebsiteMenus($menubar = null, $key = '', $parent_id = 0){
	    global $_users;

	    $menus = $this->getWebsiteMenuRecords($menubar, $parent_id);
	    if($key != ''){
	        if(isset($menus[$key])){
	            return $menus[$key];
	        }else{
	            foreach($menus as $key => $menu){
	                if(isset($menu[$key])) return $menu[$key];
	            }
	        }
	    }
	    return $menus;
	}

	/**
	 * Return a specific website menu object by its key from a menu collection
	 * @param array $menus
	 * @param string $key
	 * @return array
	 */
	function getWebsiteMenuObject($menus, $key){
	    $retn = null;
	    if(is_array($menus) && !empty($key)){
	        if(array_key_exists($key, $menus)){
	            $retn = $menus[$key];
	        }else{
	            foreach($menus as $menu){
	                if(isset($menu['childmenus']) && is_array($menu['childmenus'])){
	                    $retn = $this->getWebsiteMenuObject($menu['childmenus'], $key);
	                    if(!is_null($retn)) break;
	                }
	            }
	        }
	    }
	    return $retn;
	}

	/**
	 * Return an Website Menu HTML
	 * @param string|integer $menubar   Menubar key or id
	 * @param array $args               Array of arguments (mainid, subclass, menuobj, itemwrap, itemwrapclass, before, after)
	 * @param array $menus              The parent menu object
	 * @return string
	 */
	function getWebsiteMenuHTML($slot = null, $menubar = null, $args = null, $menus = null){
	    global $_page, $_users, $_data;

	    // get the menubar, if not provided, from the slot
	    if(!empty($slot) && empty($menubar)){
	    	$menubarArry = $this->getWebsiteMenubarInSlot($slot);
	    	if(!empty($menubarArry)) $menubar = $menubarArry['key'];
	    }
	    if(empty($menubar)) return null;

	    // get the menus, if not provided, from the menubar
	    if(empty($menus)) {
	        $menus = $_page->menus = $this->getWebsiteMenus($menubar);
	    }

	    if(!is_array($args)) {
	        $args = array(
	            "mainid" => "navigation",
	            "subclass" => "subnavigation",
	            "menuclass" => "navmenu",
	            "menuobj" => "ul",
	            "itemwrap" => "a",
	            "itemwrapclass" => "",
	            "beforelink" => "",
	            "afterlink" => "",
	            "beforetext" => "",
	            "aftertext" => ""
	        );
	    }

	    $mainid = strtolower(getIfSet($args['mainid'], "navigation"));
	    $subclass = strtolower(getIfSet($args['subclass'], "subnavigation"));
	    $menuclass = strtolower(getIfSet($args['menuclass'], "navmenu"));
	    $menuobj = strtolower(getIfSet($args['menuobj'], "ul"));
	    $itemwrap = strtolower(getIfSet($args['itemwrap'], "a"));
	    $beforelink = getIfSet($args['beforelink'], "");
	    $afterlink = getIfSet($args['afterlink'], "");
	    $beforetext = getIfSet($args['beforetext'], "");
	    $aftertext = getIfSet($args['aftertext'], "");
	    $itemwrapclass = strtolower(getIfSet($args['itemwrapclass'], ""));

	    $ulclass = '';
        if(!empty($mainid)) $ulclass .= ' id="'.$mainid.'"';
        if(!empty($subclass)) $ulclass .= ' class="'.$subclass.'"';

	    switch($menuobj){
	        case "ul":
	            $itemobj = "li";
	            break;
	        case "div":
	            $itemobj = "div";
	            break;
	        case "nav":
	            $itemobj = "div";
	            break;
	        case "ol":
	            $itemobj = "li";
	            break;
	        default:
	            $menuobj = "ul";
	            $itemobj = "li";
	            break;
	    }

	    switch($itemwrap){
	        case "span":
	            $itemwrap_link = false;
	            break;
	        case "a":
	        default:
	            $itemwrap_link = true;
	            break;
	    }

	    if(count($menus) > 0){
	        $navmenu = "<{$menuobj}{$ulclass}>\n";
	        $p = parse_url($_SERVER['REQUEST_URI']);
	        $basepath = str_replace(VHOST, "", $p['path']);
	        foreach($menus as $key => $menu){
	            if($_users->userIsAllowedTo("view_locked_menus") || !$menu['restricted']){
	                $id = $menu['id'];
	                $table = str_replace(DB_TABLE_PREFIX, "", $menu['table']);
	                $activemenu = (($basepath == $menu['target']) ? " activemenu" : "");
	                $navmenu .= "\t<{$itemobj} class=\"{$menuclass}{$activemenu}\" id=\"menu_{$key}_{$id}\">";
	                $beforelink_str = str_replace(array("{key}", "{id}"), array($key, $menu['id']), $beforelink);
	                $afterlink_str = str_replace(array("{key}", "{id}"), array($key, $menu['id']), $afterlink);
	                $beforetext_str = str_replace(array("{data}"), array($table), $beforetext);
	                $aftertext_str = str_replace(array("{data}"), array($table), $aftertext);
	                $alias = intval($menu['alias']);
	                $window = $menu['window'];
	                if($window != '') $window = ' target="'.$window.'"';

	                if($itemwrap_link){
	                    if($alias > 0){
	                        $rec = $_data->data_liases(null, null, $alias);
	                        if(!empty($rec))
	                            $navmenu .= str_replace(array("{class}", "{link}", "{text}", "{key}"), array($itemwrapclass, WEB_URL.$rec[0]['alias'], $menu['title'], $key, $window), $beforelink_str.'<a href="{link}"{class} rel="{key}"{window}>'.$beforetext_str.'{text}'.$aftertext_str.'</a>'.$afterlink_str.PHP_EOL);
	                    }elseif($menu['target'] != ''){
	                        $navmenu .= str_replace(array("{class}", "{link}", "{text}", "{key}"), array($itemwrapclass, $target, $menu['title'], $key, $window), $beforelink_str.'<a href="{link}"{class} rel="{key}"{window}>'.$beforetext_str.'{text}'.$aftertext_str.'</a>'.$afterlink_str.PHP_EOL);
	                    }
	                }else{
	                    $navmenu .= str_replace(array("{class}", "{text}", "{key}"), array($itemwrapclass, $menu['title'], $key), $beforelink_str.'<span{class} rel="{key}">'.$beforetext_str.'{text}'.$aftertext_str.'</span>'.$afterlink_str.PHP_EOL);
	                }

	                if(is_array($menu['childmenus']) && count($menu['childmenus']) > 0){
	                    $navmenu .= $this->getWebsiteMenuHTML($slot, $menubar, $args, $menu['childmenus']);
	                }
	                $navmenu .= "\t</{$itemobj}>\n";
	            }
	        }
	        $navmenu .= "</{$menuobj}>\n";
	    }else{
	        $navmenu = "";
	    }
	    return $navmenu;
	}

	/**
	 * Generate target file paths for each website menu and submenu
	 * @param array $menus
	 * @return array $menus
	 */
	function setupWebsiteMenusTargets($menus){
	    foreach($menus as $key => $menu){
	        $folder = preg_replace("/(^".DB_TABLE_PREFIX."|_cat(s*))/i", "", $menu['table']);
	        $filesuffix = preg_replace("/^".DB_TABLE_PREFIX."/i", "", $menu['table']);
	        $menus[$key]['target'] = '';
	        $fileprefix = '';
	        if($menu['table'] != ''){
	            $fileprefix = 'list-';
	            if(!empty($menu['alias'])) $filesuffix = $menu['alias'];
	            $menus[$key]['target'] = $fileprefix.$filesuffix.".php";
	        }
	        if(getifSet($menu['tourl']) == true && !empty($menu['alias'])){
	            $menus[$key]['target'] = $menu['alias'];
	        }
	        if(!empty($menu['childmenus'])){
	            $menus[$key]['childmenus'] = $this->setupWebsiteMenusTargets($menus[$key]['childmenus']);
	        }
	    }
	    return $menus;
	}

	/**
	 * Return the target URL for the specified menu
	 * The target is
	 *              1) activetheme/datatype-targettype.php
	 *              2) activetheme/targettype.php
	 *              3) systemtheme/datatype-targettype.php
	 *              4) systemtheme/targettype.php
	 * @param string $datatype
	 * @param string $targettype
	 * @return string
	 */
	function getWebsiteMenuTarget($datatype, $targettype){
		global $_themes;

	    if(!empty($datatype) && $datatype != '- Not Bound -'){
	        $themefolder = $_themes->getThemePathUnder('website', THEME_PATH_ROOT, 0);
	        $systemfolder = $_themes->getWebsiteSystemThemePath();
	        $datatype = strtolower($datatype);
	        $targettype = strtolower($targettype);
	        $filetests = array();
	        $target = null;

	        $filetests[] = $themefolder.$datatype."-".$targettype.".php";
	        $filetests[] = $themefolder.$targettype.".php";
	        $filetests[] = $systemfolder.$datatype."-".$targettype.".php";
	        $filetests[] = $systemfolder.$targettype.".php";
	        foreach($filetests as $file){
	            if(file_exists(SITE_PATH.$file)){
	                $target = $file;
	                break;
	            }
	        }

	        return $target;
	    }else{
	        return null;
	    }
	}

	/**
	 * Return the website data alias for a specific datatype and targettype
	 * @param string $datatype
	 * @param string $targettype
	 * @param boolean $withwebsitefolder
	 * @return string
	 */
	function getWebsiteMenuAlias($datatype, $targettype, $withwebsitefolder = true){
	    global $_db_control;

	    $alias = $_db_control->setTable(MENUS_TABLE)->setFields("alias")->setWhere("`table` = '$datatype' AND `targettype` = '$targettype'")->getRecItem();
	    if(!$withwebsitefolder)
	        $alias = str_replace(ADMIN_FOLDER, "", $alias);
	    else
	        $alias = WEB_URL.$alias;
	    return $alias;
	}

	/**
	 * Return the derived file target for the current page alias
	 *      Aliases made up of datatype + targettype, derived order is:
	 *              1) activetheme/datatype-targettype.php
	 *              2) activetheme/targettype.php
	 *              3) activetheme/index.php
	 *
	 *      Aliases made up of datatype + targettype + action, derived order is:
	 *              1) activetheme/datatype-targettype-action.php
	 *              2) activetheme/datatype-targettype.php
	 *              3) activetheme/targettype.php
	 *              4) activetheme/index.php
	 */
	function getWebsiteMenuTargetFromAlias(){
	    global $_page, $_themes;

	    if(is_array($_page->urlpath)){
	        $themefolder = $_themes->getThemePathUnder('website', THEME_PATH_ROOT);
	        $datatype = $_page->datatype;
	        $targettype = null;
	        $action = null;
	        if(count($_page->urlpath) > 1){
	            $targettype = $_page->urlpath[1];
	            if(count($_page->urlpath) > 2)
	                $action = $_page->urlpath[2];
	        }

	        $filetests = array();
	        $target = null;
	        $found = false;
	        if(is_null($action)){
	            // datatype + targettype
	            if(!is_null($targettype)){
		            $filetests[] = $themefolder.$datatype."-".$targettype.".php";
		            $filetests[] = $themefolder.$targettype.".php";
		        }else{
		            $filetests[] = $themefolder.$datatype.".php";
		        }
	            $filetests[] = $themefolder."index.php";
	        }else{
	            // datatype + targettype + action
	            if(!is_null($targettype)){
		            $filetests[] = $themefolder.$datatype."-".$targettype."-".$action.".php";
		            $filetests[] = $themefolder.$datatype."-".$targettype.".php";
		            $filetests[] = $themefolder.$targettype.".php";
		        }else{
		            $filetests[] = $themefolder.$datatype.".php";
		        }
	            $filetests[] = $themefolder."index.php";
	        }
	        foreach($filetests as $file){
	            if(file_exists(SITE_PATH.$file)){
	                $target = $file;
	                $found = true;
	                break;
	            }
	        }
	    }
	    $_page->datatype = $datatype;
	    $_page->target = $target;
	    $_page->targettype = $targettype;
	    $_page->targetaction = $action;
	    $_page->found = $found;
	}

	/**
	 * Save website menus layout (for WME) and settings to database (triggered by menu sorting)
	 * @param string $newlayout         Array of menu ids describing the order set by the user (menuitem[id]=parentid)
	 * @return boolean
	 */
	function updateWebsiteMenuLayouts($newlayout){
	    global $_db_control, $_events;

	    $ok = false;
	    if(is_array($newlayout) && count($newlayout) > 0){
	        foreach($newlayout as $order => $menu){
	            preg_match("/([a-z0-9_-]+)\[(\d*)\]=([a-z0-9_-]*)/i", $menu, $data);
	            if(count($data) == 4){
	                $data[2] = intval($data[2]);
	                $data[3] = intval($data[3]);
	                $_db_control->setTable(MENUS_TABLE)->setFieldvals(array("parent_id" => $data[3], "sort_order" => $order))->setWhere("`id` = '".$data[2]."'")->updateRec();
	            }
	        }
	        if(!defined('IN_AJAX')) $_page->menus = $this->getWebsiteMenus();
	    }
		$_events->processTriggerEvent(__FUNCTION__, $newlayout);				// alert triggered functions when function executes
	    return $ok;
	}

	/**
	 * Return Website Menu Editor (WME) HTML form fields
	 * @param type $menubarkey
	 * @param type $menukey
	 * @param type $menus
	 * @return string
	 */
	function getWebsiteMenuEditorHTML($menubarkey, $menukey = null, $menus = null){
	    global $_page, $_data, $_tax;

	    $outp = '';
	    if(is_null($menus)){
	        if(defined('IN_AJAX')) {
	            $menus = $this->getWebsiteMenus($menubarkey);
	        }else{
	            $menus = $_page->menus;
	        }
	    }
	    if(!empty($menubarkey) && (count($menus) > 0 || empty($menukey))){
	        // prepare fields from saved data
	        $dataaliases = $_data->data_aliases;
	        array_unshift($dataaliases, array("alias" => "", "attribute_id" => 0));
	        $pagealiases = $_data->page_aliases;
	        array_unshift($pagealiases, array("alias" => "", "attribute_id" => 0));
	        $termaliases = $_data->term_aliases;
	        array_unshift($termaliases, array("alias" => "", "attribute_id" => 0));
	        $taxonomies = $_tax->taxonomies;
	        array_unshift($taxonomies, array("name" => "", "id" => 0));

	        $menubar = $this->getWebsiteMenubars($menubarkey);
	        if(count($menubar) == 0) $menubar = array(0 => array("key" => $menubarkey, "id" => 0));

	        $menubars = $this->getWebsiteMenubars();
	        $menubar = array(0 => array("key" => $menubarkey, "id" => 0));
	        foreach($menubars as $mb){
	            if($mb["key"] == $menubarkey){
	                $menubar[0] = $mb;
	                break;
	            }
	        }

	        $parent_menus = $this->getWebsiteMenuRecords($menubar[0]['key']);
	        $new_menu = array("id" => 0, "menu_bar_id" => $menubar[0]['id'], "parent_id" => 0, "title" => "", "table" => "", "alias" => "", "restricted" => 0, "target" => "", "targettype" => "", "taxonomy_id" => 0, "window" => "", "custom" => "", "active" => 1);

	        if(!empty($menukey)){
	            $clonemenu = (strpos($menukey, 'clone>>') !== false);
	            if($clonemenu) $menukey = str_replace('clone>>', '', $menukey);

	            $menu = $this->getWebsiteMenuObject($menus, $menukey);
		        $blocktitle = "Edit Website Menu Item - ".$menu['title'];
	            if(count($menu) == 0) {
	                $menu = $new_menu;
	                $blocktitle = "Create Website Menu Item";
	            }elseif($clonemenu){
	                $blocktitle = "Clone Website Menu Item";
	            }
	        }else{
	            $clonemenu = false;
	            $menu = $new_menu;
	            $blocktitle = "Create Website Menu Item";
	        }

	        // table, title, file alias
	        $outp = '<h3 class="header">'.$blocktitle.'</h3>';
	        $outp.= '<input type="hidden" id="websitemenu_dirty" value="" />'.PHP_EOL;
	        $outp.= '<input type="hidden" id="websitemenu_parent_id" name="websitemenu_parent_id" value="'.$menu['parent_id'].'" />'.PHP_EOL;
	        $outp.= '<input type="hidden" id="websitemenu_id" name="websitemenu_id" value="'.(($clonemenu) ? -$menu['id'] : $menu['id']).'" />'.PHP_EOL;

	        $outp.= '<div class="setlabel">Menu Title<span class="red">*</span>: <a href="#" class="hovertip" alt="The text displayed on the menu bar">[?]</a></div>';
	        $outp.= '<div class="setdata"><input type="text" id="websitemenu_title" name="websitemenu_title" value="'.$menu['title'].'" data-recall="'.$menu['title'].'" class="widefldsize" /></div>'.PHP_EOL;

	        $outp.= '<div class="setlabel">Path<span class="red">*</span>: <a href="#" class="hovertip" alt="The page, post, or other URL to which this menu links">[?]</a></div><div class="setdata">';
	        $outp.= '<p>Page Alias: <select id="websitemenu_alias" name="websitemenu_alias" class="advselect">'.PHP_EOL;
	        foreach($pagealiases as $alias){
	            $sel = (($alias['attribute_id'] == $menu['alias']) ? ' selected="selected"' : '');
	            $outp.= '<option value="'.$alias['attribute_id'].'"'.$sel.'>'.$alias['alias'].'</option>'.PHP_EOL;
	        }
	        $outp.= '</select></p>';
	        $outp.= '<p>or, Term Alias: <select id="websitemenu_termalias" name="websitemenu_termalias" class="advselect">'.PHP_EOL;
	        foreach($termaliases as $alias){
	            $sel = (($alias['attribute_id'] == $menu['alias']) ? ' selected="selected"' : '');
	            $outp.= '<option value="'.$alias['attribute_id'].'"'.$sel.'>'.$alias['alias'].'</option>'.PHP_EOL;
	        }
	        $outp.= '</select></p>';
	        $outp.= '<p>or, Taxonomy: <select id="websitemenu_tax" name="websitemenu_tax" class="advselect">'.PHP_EOL;
	        foreach($taxonomies as $taxonomy){
	            $sel = (($taxonomy['id'] == $menu['taxonomy_id']) ? ' selected="selected"' : '');
	            $outp.= '<option value="'.$taxonomy['id'].'"'.$sel.'>'.$taxonomy['name'].'</option>'.PHP_EOL;
	        }
	        $outp.= '</select></p>';

	        $outp.= 'or, Custom Link:&nbsp;<input type="text" id="websitemenu_target" name="websitemenu_target" value="'.$menu['target'].'" /><p></p>'.PHP_EOL;
	        $outp.= '</div>';
	        $outp.= '<div class="setlabel">Open in<span class="red">*</span>:</div><div class="setdata">';
	        $outp.= '<select id="websitemenu_window" name="websitemenu_window">'.PHP_EOL;
	        foreach(array("" => "Same window", "_blank" => "New window/tab", "_self" => "Current window") as $window_key => $window){
	            $sel = (($window_key == $menu['window']) ? ' selected="selected"' : '');
	            $outp.= '<option value="'.$window_key.'"'.$sel.'>'.$window.'</option>'.PHP_EOL;
	        }
	        $outp.= '</select></div>'.PHP_EOL;

	        $outp.= '<div class="setlabel">Menubar<span class="red">*</span>: <a href="#" class="hovertip" alt="Change to move menu item to another menubar">[?]</a></div><div class="setdata">';
	        $outp.= '<select id="websitemenu_menubar" name="websitemenu_menubar">'.PHP_EOL;
	        $outp.= '<option value="0:"'.(($menu['menu_bar_id'] == 0) ? ' selected="selected"' : '').'>- Not in menubar (Orphaned) -</option>'.PHP_EOL;
	        if(count($menubars) > 0){
	            foreach($menubars as $mb){
	                $sel = (($mb['id'] == $menu['menu_bar_id']) ? ' selected="selected"' : '');
	                $outp.= '<option value="'.$mb['id'].'"'.$sel.'>'.$mb['title'].'</option>'.PHP_EOL;
	            }
	        }
	        $outp.= '</select></div>'.PHP_EOL;

	        $outp.= '<div class="setlabel">Parent Menu Item:</div><div class="setdata">';
	        $outp.= '<select id="websitemenu_parent" name="websitemenu_parent">'.PHP_EOL;
	        $outp.= '<option value="0"'.(($menu['parent_id'] == 0) ? ' selected="selected"' : '').'>- No Parent -</option>'.PHP_EOL;
	        if(count($parent_menus) > 0){
	            foreach($parent_menus as $parent_menu_key => $parent_menu){
	                if($parent_menu['id'] != $menu['id']){
	                    $sel = (($parent_menu['id'] == $menu['parent_id']) ? ' selected="selected"' : '');
	                    $outp.= '<option value="'.$parent_menu['id'].'"'.$sel.'>'.$parent_menu['title'].'</option>'.PHP_EOL;
	                }
	            }
	        }
	        $outp.= '</select></div>'.PHP_EOL;

	        $outp.= '<div class="setlabel">Enabled?: <a href="#" class="hovertip" alt="If set, menu will be shown.">[?]</a></div><div class="setdata"><input type="checkbox" id="websitemenu_active" name="websitemenu_active" value="1"'.((getIfSet($menu['active'])) ? ' checked="checked"' : '').' /> Yes, menu is shown</div>'.PHP_EOL;
	        if(isset($menu['custom']) && $menu['custom'] == true) $outp.= '<div class="setlabel">Attention:</div><div class="setdata">This menu item was created by a custom program or plugin.  Changes here could impact performance.</div>'.PHP_EOL;
	        $delbtn = '&nbsp;&nbsp;<a href="#" class="websitemenu_delete" rel="'.$menu['id'].'">Delete Menu [Cannot be undone!]</a>';
	        $outp.= '<div class="setlabel"></div><div class="setdata"><input type="button" id="websitemenu_save" value="Save Changes" />'.$delbtn.'</div>'.PHP_EOL;
	    }
	    return $outp;
	}

	/**
	 * Save menu data to database
	 * @param string $id                Menu id
	 * @param string $args              Menu data (menu_bar_id, parent_id, table, title, alias, target, restricted, active, custom)
	 * @return array                    True if successful and menu key
	 */
	function saveWebsiteMenu($id, $args, $incrDupKey = true){
	    global $_db_control, $_events, $_tax;

	    $ok = false;
	    $key = null;
	    if(is_array($args)){
	        // prepare menu data
	        $menu_bar_id = getIntValIfSet($args['menu_bar_id']);
	        $new_menu_bar_id = getIntValIfSet($args['new_menu_bar_id']);
	        $title = getIfSet($args['title'], "");
	        $alias = getIntValIfSet($args['alias']);
	        $taxonomy_id = getIntValIfSet($args['taxonomy_id']);
	        $target = (($alias > 0) ? "" : getIfSet($args['target'], ""));
	        $parent_menu_id = getIntValIfSet($args['parent_id'], 0);

	        // if new menubar is different than current one and is not 0, deselect parent menu item
	        if($new_menu_bar_id != $menu_bar_id && $new_menu_bar_id > 0){
	            $parent_menu_id = 0;
	            $menu_bar_id = $new_menu_bar_id;
	        }

	        if($alias > 0) {
	        	$alias_rec = $_db_control->setTable(ATTRIBUTES_TABLE)->setWhere("attribute_id = '".$alias."'")->getRec();
	        	$data_type = $alias_rec[0]['data_type'];
	        	if($alias_rec[0]['attribute_class'] == ATTR_CLASS_TAX_ALIAS && $alias_rec[0]['data_type'] == TERMS_TABLE){
	        		$term = $_tax->getTerms(array("id" => $alias_rec[0]['data_id']));
	        		if(!empty($term)) $taxonomy_id = $term[0]['taxonomy_id'];
	        		$targettype = "term";
	        	}else{
	        		$targettype = "data";
	        	}
	        	$target = null;    // if alias chosen, target must be cleared
	        }elseif($taxonomy_id > 0) {
	        	$data_type = TAXONOMIES_TABLE;
	        	$targettype = "taxonomy";
	        	$target = null;    // if taxonomy chosen, target must be cleared
	        }elseif(!empty($target)){
	        	$targettype = "url";
	        }

	        $menu_data = array(
	            "menu_bar_id" => $menu_bar_id,
	            "parent_id" => $parent_menu_id,
	            "title" => $title,
	            "alias" => $alias,
	            "table" => $data_type,
	            "taxonomy_id" => $taxonomy_id,
	            "target" => $target,
	            "targettype" => $targettype,
	            "restricted" => false,
	            "window" => getIfSet($args['window'], ""),
	            "active" => getBooleanIfSet($args['active']),
	            "custom" => getBooleanIfSet($args['custom']),
	        );

	        // prepare menus
	        $menus = $this->getWebsiteMenus($menu_bar_id);

	        // check if key already exists for any other menu item
	        $menu_keys = $_db_control->flattenDBArray($_db_control->setTable(MENUS_TABLE)->setFields("`id`, `key`")->setWhere("`menu_class` = 'menu' AND `in_admin` = 0 AND `id` != '".$id."'")->getRec(), "id", "key");
	        $key = getUniqueKey(codify($title), $menu_keys);

	        if(!in_array($key, $menu_keys)) {
	            $ok = $this->saveWebsiteMenuRecord($key, $menu_data);
	        }else{
	            $key = 'The menu key is not unique.';
	        }
	    }
		$_events->processTriggerEvent(__FUNCTION__, $key);				// alert triggered functions when function executes
	    return array($ok, $key);
	}

	/**
	 * Save multiple menu objects to database
	 * @param integer $menu_bar_id 				The id of the menubar to which all menus will be added
	 * @param array $collection 				An array of data ids
	 * 												array("data type" => array(id1, id2, id3,...))
	 * @return boolean
	 */
	function addWebsiteMenuCollection($menu_bar_id, $collection){
		global $_db_control, $_events, $_data, $_tax;

		$ok = false;
		if(is_array($collection)){
	        $menu_keys = $_db_control->flattenDBArray($_db_control->setTable(MENUS_TABLE)->setFields("`id`, `key`")->setWhere("`menu_class` = 'menu' AND `in_admin` = 0")->getRec(), "id", "key");
			foreach($collection as $data_type => $ids){
				if(is_array($ids)){
					if(substr($data_type, 0, 9) == 'terms-tax'){
						$tax_id = intval(substr($data_type, 9));
						$data_type = 'terms';
						$recs = $_db_control->setTable($data_type)->setWhere("id IN (".join(",", $ids).")")->getRec();
					}else{
						$recs = $_db_control->setTable($data_type)->setWhere("id IN (".join(",", $ids).")")->getData();
					}
					foreach($recs as $rec){
						if($data_type == 'terms'){
							$title = $rec['name'];
							$key = getUniqueKey(codify($title), $menu_keys);

							// check tax aliases for pre-existing term alias
							$alias_id = 0;
							foreach($_data->taxonomy_aliases as $tax_alias){
								if($tax_alias['data_type'] == TERMS_TABLE && $tax_alias['data_id'] == $rec['id']){
									$alias_id = $tax_alias['attribute_id'];
									break;
								}
							}
							$taxonomy = $_tax->getTaxonomy($tax_id);
							if(empty($taxonomy)) return array(false, null);

							if($alias_id == 0){
								$args = array(
									"newalias" => $key,
									"data_type" => TERMS_TABLE,
									"data_id" => $rec['id'],
									"attribute_class" => ATTR_CLASS_TAX_ALIAS
								);
								$alias_id = $_data->saveDataAlias($args);
							}

					        $menu_data = array(
					            "menu_bar_id" => $menu_bar_id,
					            "in_admin" => false,
					            "menu_class" => "menu",
					            "parent_id" => 0,
					            "title" => $title,
					            "key" => $key,
					            "table" => $data_type,
					            "alias" => $alias_id,
					            "taxonomy_id" => $tax_id,
					            "target" => null,
					            "targettype" => "term",
					            "restricted" => false,
					            "window" => "",
					            "active" => true,
					            "custom" => false,
					        );
						}elseif($rec['attribute_id'] > 0){
							$title = $_db_control->getTitleField($rec);
							$key = getUniqueKey(codify($title), $menu_keys);

					        $menu_data = array(
					            "menu_bar_id" => $menu_bar_id,
					            "in_admin" => false,
					            "menu_class" => "menu",
					            "parent_id" => 0,
					            "title" => $title,
					            "key" => $key,
					            "table" => $data_type,
					            "alias" => $rec['attribute_id'],
					            "taxonomy_id" => 0,
					            "target" => null,
					            "targettype" => "data",
					            "restricted" => false,
					            "window" => "",
					            "active" => true,
					            "custom" => false,
					        );
					    }else{
					    	continue;
					    }

				        $ok = $this->saveWebsiteMenuRecord($key, $menu_data);
				    }
			   	}
			}
		}
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	    return array($ok, $key);
	}

	/**
	 * Delete an website menu
	 * @param string $id                Menu id
	 * @return boolean
	 */
	function deleteWebsiteMenu($id){
	    global $_db_control, $_events;

	    $ok = false;
	    if($id > 0){
	        $_db_control->setTable(MENUS_TABLE)->setWhere("`id` = '$id' AND `in_admin` = 0")->deleteRec();
	        $_db_control->setTable(MENUS_TABLE)->setFieldvals(array("parent_id" => 0))->setWhere("`parent_id` = '$id' AND `in_admin` = 0")->updateRec();
	        $ok = true;
	    }
		$_events->processTriggerEvent(__FUNCTION__, $ok);				// alert triggered functions when function executes
	    return $ok;
	}

	/**
	 * Delete all custom website menus (added programmatically to system and tagged as 'custom')
	 */
	function clearCustomWebsiteMenus($menu_bar_id){
	    global $_page, $_events;

	    $ok = false;
	    if(defined('IN_AJAX')) {
	        $menus = $this->getWebsiteMenus($menu_bar_id);
	    }else{
	        $menus = $_page->menus;
	        if(empty($menus)) $menus = $this->getWebsiteMenus($menu_bar_id);
	    }

	    if(count($menus) > 0){
	        foreach($menus as $menukey => $menu){
	            if(isset($menu['custom']) && $menu['custom'] == 1){
	                unset($menus[$menukey]);
	            }else if(isset($menu['childmenus']) && is_array($menu['childmenus'])){
	                foreach($menu as $submenukey => $submenu){
	                    if(isset($submenu['custom']) && $submenu['custom'] == 1){
	                        unset($menus[$menukey]['childmenus'][$submenukey]);
	                    }
	                }
	            }
	        }
			$_events->processTriggerEvent(__FUNCTION__, $menu_bar_id);				// alert triggered functions when function executes
	        $_db_control->setTable(MENUS_TABLE)->setWhere("`custom` = 1 AND `in_admin` = 0")->deleteRec();
	    }
	}

	// ----------- ADMIN MENU AND NAVIGATION FUNCTIONS ---------------

	/**
	 * Ensure that required admin menus, such as pages list, are in database and are structured correctly.
	 */
	function reinforceAdminMenus(){
		global $_db_control, $_data;

		// look for system menu records in any menubar

		$menubars = $this->getAdminMenubars();
		if(empty($menubars)){
			// no admin menubars.
			// create one and use it
			list($retn, $menu_bar_id, $key) = $this->saveAdminMenubar(array("title" => "Main", "parent_id" => 1));
		}else{
			$menu_bar_id = 0;
			foreach($menubars as $menubar){
				// find the menubar in the main slot (parent_id = 1)
				if($menubar['parent_id'] == 1) $menu_bar_id = $menubar['id'];
			}

			// if no menubar is in the main slot put the first one in it
			if($menu_bar_id == 0){
				$menu_bar_id = $menubars[0]['id'];
				$this->saveAdminMenubar(array("id" => $menu_bar_id, "parent_id" => 1));
			}
		}

		// get the system menus (if exists)
		$sys_menus = array(PAGES_TABLE, MEDIA_TABLE);
		foreach($sys_menus as $sys_menu){
			$menus = $this->getAdminMenus($menu_bar_id);
			$menu_id = 0;
			$the_menu = array();
			if(!empty($menus)){
				$the_key = codify($sys_menu);
				foreach($menus as $menu){
					if($menu['key'] == $the_key){
						$menu_id = $menu['id'];
						$the_menu = $menu;
						break;
					}
				}
			}

			// get pages alias
			$menu_alias = $_data->getDataAliases($sys_menu, null, $sys_menu."/list", 0, ATTR_CLASS_ADMIN_ALIAS);
			if(is_array($menu_alias)){
				// an alias was found.  save menu to this alias
				$menu_alias = current($menu_alias);
				if(empty($menu_alias['attribute_id']))
					$menu_alias['attribute_id'] = $_data->saveDataAlias(array("data_type" => $sys_menu, "attribute_class" => ATTR_CLASS_ADMIN_ALIAS, "newalias" => $sys_menu."/list"));
				$args = array(
		            "menu_bar_id" => $menu_bar_id,
		            "table" => $sys_menu,
		            "title" => ucwords($sys_menu),
		            "alias" => $menu_alias['attribute_id'],
		            "targettype" => "list",
		            "parent_id" => 0,
		            "active" => true,
		        );
				$this->saveAdminMenu($menu_id, $args, false);
			}
		}
	}

	// RECORDS

	/**
	 * Load menu records from the database
	 * @param string $menubar_key 		The key of the menubar in which the menus are organized
	 * @param integer $parent_id        ID of parent menu
	 * @param array $menus              Containing menu or blank array
	 */
	function getAdminMenuRecords($menubar_key = null, $args = array(), $menus = array()){
	    global $_db_control;

	    if(is_numeric($menubar_key)){
	    	$menu_bar_id = $menubar_key;
	    }elseif(!empty($menubar_key)){
		    $menubar = $this->getAdminMenubars($menubar_key);
		    if(count($menubar) > 0)
		        $menu_bar_id = $menubar[0]['id'];
		    else
		        $menu_bar_id = 0;
		}else{
			$menu_bar_id = -1;
		}

		// arguments
		$argskeys = array(
			'parent_id' => 0,
			'parent_key' => '',
			'table' => '',
			'targettype' => '',
			'taxonomy_id' => 0,
			'active' => 1,
			'sites' => null
		);
		$argsvals = getIfSetArray($args, $argskeys);
		$argsstr = null;
		foreach($argskeys as $argskey => $v){
			if(!empty($argsvals[$argskey])) $argsstr .= " AND `".$argskey."` = '".$argsvals[$argskey]."'";
		}

		if($menu_bar_id >= 0)
	    	$menurecs = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '1' AND `menu_bar_id` = '".$menu_bar_id."' AND `menu_class` = 'menu'".$argsstr)->setOrder("sort_order")->getRec();
	    else
	    	$menurecs = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '1' AND `menu_class` = 'menu'".$argsstr)->setOrder("sort_order")->getRec();

	    if(is_array($menus) && count($menurecs) > 0){
	        foreach($menurecs as $menurec){
	            $menus[$menurec['key']] = array(
	                "id" => $menurec['id'],
	                "menu_bar_id" => $menurec['menu_bar_id'],
	                "menu_class" => $menurec['menu_class'],
	                "parent_id" => $menurec['parent_id'],
	                "title" => $menurec['title'],
	                "key" => $menurec['key'],
	                "alias" => $menurec['alias'],
	                "target" => $menurec['target'],
	                "table" => $menurec['table'],
	                "taxonomy_id" => $menurec['taxonomy_id'],
	                "targettype" => $menurec['targettype'],
	                "restricted" => ($menurec['restricted'] == 1),
	                "window" => $menurec['window'],
	                "active" => ($menurec['active'] == 1),
	                "custom" => ($menurec['custom'] == 1),
	                "childmenus" => null
	            );
	            if($menurec['id'] > 0){
	                $childmenurecs = $this->getAdminMenuRecords($menubar_key, array('parent_id' => $menurec['id']));
	                if(!is_null($childmenurecs)) $menus[$menurec['key']]['childmenus'] = $childmenurecs;
	            }
	        }
	        return $menus;
	    }
	    return null;
	}

	/**
	 * Save menu collection back to menu record database
	 * @param integer $parent_id        ID of parent menu
	 * @param array $menus              Containing menu or blank array
	 */
	function saveAdminMenuRecords($parent_id = 0, $menus = array()){
	    global $_db_control;

	    if(is_array($menus) && count($menus) > 0){
	        foreach($menus as $key => $menu){
	            if(!isset($menu['parent_id'])) $menu['parent_id'] = 0;
	            if(!isset($menu['actiave'])) $menu['active'] = 1;
	            if(!isset($menu['table'])) $menu['table'] = null;
	            $_db_control->setTable(MENUS_TABLE)->setFieldvals(array("menu_bar_id" => $menu['menu_bar_id'], "parent_id" => $menu['parent_id'], "table" => $menu['table'], "title" => $menu['title'], "alias" => $menu['alias'], "target" => $menu['target'], "targettype" => $menu['targettype'], "restricted" => $menu['restricted'], "window" => $menu['window'], "in_admin" => "1", "active" => $menu['active'], "custom" => $menu['custom']))->setWhere("`key` = '$key' AND `in_admin` = 1")->replaceRec();
	            if(is_array($menu['childmenus']) && count($menu['childmenus']) > 0){
	                $this->saveAdminMenuRecords($menu['parent_id'], $menu['childmenus']);
	            }
	        }
	    }
	    return true;
	}

	/**
	 * Save a single menu object back to menu record database
	 * @param string $key               Key of menu
	 * @param array $menus              Menu object
	 */
	function saveAdminMenuRecord($key, $menu){
	    global $_db_control, $_events;

	    if(is_array($menu)){
	        if(!isset($menu['parent_id'])) $menu['parent_id'] = 0;
	        if(!isset($menu['active'])) $menu['active'] = 1;
	        if(!isset($menu['table'])) $menu['table'] = null;
	        $_db_control->setTable(MENUS_TABLE)->setFieldvals(array("key" => $key, "menu_bar_id" => $menu['menu_bar_id'], "parent_id" => $menu['parent_id'], "table" => $menu['table'], "title" => $menu['title'], "taxonomy_id" => $menu['taxonomy_id'], "alias" => $menu['alias'], "target" => $menu['target'], "targettype" => $menu['targettype'], "restricted" => $menu['restricted'], "window" => $menu['window'], "in_admin" => "1", "active" => $menu['active'], "custom" => $menu['custom']))->setWhere("`key` = '$key' AND `in_admin` = 1")->replaceRec();
			$_events->processTriggerEvent(__FUNCTION__, $key);				// alert triggered functions when function executes
	        return true;
	    }
	}

	// MENUBARS

	/**
	 * Return the menubar object that has been assigned to the main admin menu slot
	 * @return mixed 					The menubar object or null if nothing has been assigned
	 */
	function getAdminMenubarInSlot(){
	    global $_db_control;

	    $slot = MENU_MAIN_SLOT;
        $menubar = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '1' AND `menu_class` = 'menubar' AND `parent_id` = '".$slot."'")->setLimit()->getRec();
        if(count($menubar) > 0)
        	$menubar = $menubar[0];
       	else
       		$menubar = null;
	    return $menubar;
	}

	/**
	 * Return a specific admin menubar object or all admin menubars as a collection array
	 * @param string|integer $key       The menubar key or id
	 * @return array
	 */
	function getAdminMenubars($key = ''){
	    global $_db_control;

	    if(is_numeric($key))
	        $menubars = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '1' AND `menu_class` = 'menubar'".(($key > 0) ? " AND `id` = '$key'" : ""))->getRec();
	    else
	        $menubars = $_db_control->setTable(MENUS_TABLE)->setWhere("`in_admin` = '1' AND `menu_class` = 'menubar'".(($key != '') ? " AND `key` = '$key'" : ""))->getRec();
	    return $menubars;
	}

	/**
	 * Return menubar info
	 * @param integer $id 				The ID or key of the menubar
	 * @return mixed 					The menubar record or null if not found
	 */
	function getAdminMenubarInfo($id){
		global $_db_control;

		$key = $id;
		$id = intval($id);
		$info = null;
		if($id > 0){
			$info = $_db_control->setTable(MENUS_TABLE)->setFields("id, title, descr, parent_id")->setWhere("`in_admin` = '1' AND `id` = '".$id."'")->setLimit()->getRec();
			if(count($info) == 0) $info = null;
		}elseif(!empty($key)){
			$info = $_db_control->setTable(MENUS_TABLE)->setFields("id, title, descr, parent_id")->setWhere("`in_admin` = '1' AND `key` = '".$key."'")->setLimit()->getRec();
			if(count($info) == 0) $info = null;
		}
		return $info;
	}

	/**
	 * Saves the menubar data to the database
	 * @param array $args 				Data array. id (int), title (str), descr (str), parent_id (int)
	 * @return array(success, id, key) 	Returns -1 if saved, 1 if updated, and false if not successful
	 */
	function saveAdminMenubar($args = array()){
		global $_events, $_db_control;

		$id = getIntvalIfSet($args['id']);
		$title = sanitizeText(getIfSet($args['title']));
		$descr = sanitizeText(getIfSet($args['descr']));
		$parent_id = getIntvalIfSet($args['parent_id']);

        $save_array = array(
        	"menu_class" => "menubar",
        	"in_admin" => 1,
        	"targettype" => ""
        );
        if(isset($args['title'])) $save_array['title'] = $title;
        if(isset($args['descr'])) $save_array['descr'] = $descr;
        if(isset($args['parent_id'])) $save_array['parent_id'] = $parent_id;
		$key = null;

		$retn = false;
		if(!empty($title)){
	        if($id > 0){
	        	// update
	            if($_db_control->setTable(MENUS_TABLE)->setFieldvals($save_array)->setWhere("`id` = '".$id."'")->updateRec()){
	                $_events->processTriggerEvent('updateAdminMenubarInfo', $id);
	                $retn = 1;
	            }
	        }else{
	        	// save
		        $menubar_keys = $_db_control->flattenDBArray($_db_control->setTable(MENUS_TABLE)->setFields("`id`, `key`")->setWhere("`menu_class` = 'menubar' AND `in_admin` = 1")->getRec(), "id", "key");
	            $key = getUniqueKey(codify($title), $menubar_keys);
	            $save_array['key'] = $key;

	            if($id = $_db_control->setTable(MENUS_TABLE)->setFieldvals($save_array)->insertRec()){
	                $_events->processTriggerEvent('saveAdminMenubarInfo', $id);
	                $retn = -1;
	            }
	        }
	        if($retn !== false && $parent_id > 0){
	        	// unset all other admin menubars from the main location slot
	        	$_db_control->setTable(MENUS_TABLE)->setFieldVals(array("parent_id" => 0))->setWhere("`in_admin` = 1 AND `id` != '".$id."'")->updateRec();
	        }
	  	}
	  	return array($retn, $id, $key);
	}

	/**
	 * Delete a menubar
	 * @param integer $id 				The ID of the menubar
	 * @return boolean
	 */
	function deleteMenubar($id){
		global $_events;

        $id = intval($id);
        $ok = false;
        if($id > 0){
            $_events->processTriggerEvent('deleteAdminMenubar', $id);
            $_db_control->setTable(MENUS_TABLE)->setFieldvals(array("menu_bar_id" => 0))->setWhere("`menu_bar_id` = '".$id."'")->updateRec();
            if($_db_control->setTable(MENUS_TABLE)->setWhere("`id` = '".$id."'")->deleteRec()) $ok = true;
            $ok = true;
        }
        return $ok;
	}

	// MENUS

	/**
	 * Return a specific admin menu array object or entire admin menu collection from the menu record array
	 * @param string|integer $menubar   The menubar key or id
	 * @param string $key               The menu key (optional)
	 * @param integer $parent_id        The parent menu id (optional)
	 * @return array
	 */
	function getAdminMenus($menubar = null, $key = '', $parent_id = 0){
	    global $_users;

	    $menus = $this->getAdminMenuRecords($menubar, array('parent_id' => $parent_id));
	    if(!$_users->userIsAllowedTo(UA_VIEW_PAGES_LIST) && isset($menus["pages"])) unset($menus["pages"]);

	    if($key != ''){
	        if(isset($menus[$key])){
	            return $menus[$key];
	        }else{
	            foreach($menus as $key => $menu){
	                if(isset($menu[$key])) return $menu[$key];
	            }
	        }
	    }
	    return $menus;
	}

	/**
	 * Return a specific admin menu object by its key from a menu collection
	 * @param array $menus
	 * @param string $key
	 * @return array
	 */
	function getAdminMenuObject($menus, $key){
	    $retn = null;
	    if(is_array($menus) && !empty($key)){
	        if(array_key_exists($key, $menus)){
	            $retn = $menus[$key];
	        }else{
	            foreach($menus as $menu){
	                if(isset($menu['childmenus']) && is_array($menu['childmenus'])){
	                    $retn = $this->getAdminMenuObject($menu['childmenus'], $key);
	                    if(!is_null($retn)) break;
	                }
	            }
	        }
	    }
	    return $retn;
	}

	/**
	 * Return an Admin Menu HTML
	 * @param string|integer $menubar   Menubar key or id
	 * @param array $args               Array of arguments (mainid, subclass, menuobj, itemwrap, itemwrapclass, before, after)
	 * @param array $menus              The parent menu object
	 * @return string
	 */
	function getAdminMenuHTML($menubar = null, $args = null, $menus = null){
	    global $_page, $_users, $_data;

	    if(is_null($menubar)){
	    	// get the menubar that's assigned to the main slot
	    	$menubar = $this->getAdminMenubarInSlot(MENU_MAIN_SLOT);
	    	if(is_array($menubar)) $menubar = $menubar['id'];
	    }
	    if(is_null($menubar)) return null;

	    if(!is_array($args)) {
	        $args = array(
	            "mainid" => "navigation",
	            "subclass" => "subnavigation",
	            "menuclass" => "navmenu",
	            "menuobj" => "ul",
	            "itemwrap" => "a",
	            "itemwrapclass" => "",
	            "beforelink" => "",
	            "afterlink" => "",
	            "beforetext" => "",
	            "aftertext" => ""
	        );
	    }

	    $mainid = strtolower(getIfSet($args['mainid'], "navigation"));
	    $subclass = strtolower(getIfSet($args['subclass'], "subnavigation"));
	    $menuclass = strtolower(getIfSet($args['menuclass'], "navmenu"));
	    $menuobj = strtolower(getIfSet($args['menuobj'], "ul"));
	    $itemwrap = strtolower(getIfSet($args['itemwrap'], "a"));
	    $beforelink = getIfSet($args['beforelink'], "");
	    $afterlink = getIfSet($args['afterlink'], "");
	    $beforetext = getIfSet($args['beforetext'], "");
	    $aftertext = getIfSet($args['aftertext'], "");
	    $itemwrapclass = strtolower(getIfSet($args['itemwrapclass'], ""));

	    if(is_null($menus)) {
	        $menus = $_page->menus = $this->getAdminMenus($menubar);
	        $ulclass = ' id="'.$mainid.'"';
	    }else{
	        $ulclass = ' class="'.$subclass.'"';
	    }

	    switch($menuobj){
	        case "ul":
	            $itemobj = "li";
	            break;
	        case "div":
	            $itemobj = "div";
	            break;
	        case "nav":
	            $itemobj = "div";
	            break;
	        case "ol":
	            $itemobj = "li";
	            break;
	        default:
	            $menuobj = "ul";
	            $itemobj = "li";
	            break;
	    }

	    switch($itemwrap){
	        case "span":
	            $itemwrap_link = false;
	            break;
	        case "a":
	        default:
	            $itemwrap_link = true;
	            break;
	    }

	    $aliases = $_data->admin_aliases;
	    if(count($menus) > 0){
	        $navmenu = "<{$menuobj}{$ulclass}>\n";
	        $p = parse_url($_SERVER['REQUEST_URI']);
	        $basepath = str_replace(VHOST, "", $p['path']);
	        foreach($menus as $key => $menu){
	            if($_users->userIsAllowedTo("view_locked_menus") || !$menu['restricted']){
	                $id = $menu['id'];
	                $table = str_replace(DB_TABLE_PREFIX, "", $menu['table']);
	                $beforelink_str = str_replace(array("{key}", "{id}"), array($key, $menu['id']), $beforelink);
	                $afterlink_str = str_replace(array("{key}", "{id}"), array($key, $menu['id']), $afterlink);
	                $beforetext_str = str_replace(array("{data}"), array($table), $beforetext);
	                $aftertext_str = str_replace(array("{data}"), array($table), $aftertext);

	                if($itemwrap_link){
	                    $menualias_arry = array();
	                    $menualias_id = intval($menu['alias']);
	                    if($menualias_id > 0){
	                    	$menualias = ((isset($aliases[$menualias_id])) ? $aliases[$menualias_id]['alias'] : null);
		                    if(!is_null($menualias)){
		                    	$menualias_arry[] = $menualias;
		                    	if(strpos($menualias, "/list") !== false) $menualias_arry[] = str_replace("/list", "", $menualias);
		                    }
		                }
	                    if(!empty($menualias_arry)){
	                        $activemenu = ((in_array($basepath, $menualias_arry)) ? " activemenu" : "");
	                        $navmenu .= "\t<{$itemobj} class=\"{$menuclass}{$activemenu}\" id=\"menu_{$key}_{$id}\">";
	                        $navmenu .= str_replace(array("{class}", "{link}", "{text}", "{key}"), array($itemwrapclass, WEB_URL.ADMIN_FOLDER.$menualias, $menu['title'], $key), $beforelink_str.'<a href="{link}"{class} rel="{key}">'.$beforetext_str.'{text}'.$aftertext_str.'</a>'.$afterlink_str.PHP_EOL);
	                    }elseif($menu['target'] != ''){
	                        $activemenu = (($basepath == ADMIN_FOLDER.$table."/".$menu['target']) ? " activemenu" : "");
	                        $navmenu .= "\t<{$itemobj} class=\"{$menuclass}{$activemenu}\" id=\"menu_{$key}_{$id}\">";
	                        $navmenu .= str_replace(array("{class}", "{link}", "{text}", "{key}"), array($itemwrapclass, WEB_URL.ADMIN_FOLDER.$table."/".$menu['target'], $menu['title'], $key), $beforelink_str.'<a href="{link}"{class} rel="{key}">'.$beforetext_str.'{text}'.$aftertext_str.'</a>'.$afterlink_str.PHP_EOL);
	                    }else{
	                        $activemenu = (($basepath == ADMIN_FOLDER.$table."/".$menu['target']) ? " activemenu" : "");
	                        $navmenu .= "\t<{$itemobj} class=\"{$menuclass}{$activemenu}\" id=\"menu_{$key}_{$id}\">";
	                        $navmenu .= str_replace(array("{class}", "{link}", "{text}", "{key}"), array($itemwrapclass, WEB_URL.ADMIN_FOLDER.$table."/".$menu['target'], $menu['title'], $key), $beforelink_str.'<a href="{link}"{class} rel="{key}">'.$beforetext_str.'{text}'.$aftertext_str.'</a>'.$afterlink_str.PHP_EOL);
	                    }
	                }else{
                        $navmenu .= "\t<{$itemobj} class=\"{$menuclass}\" id=\"menu_{$key}_{$id}\">";
	                    $navmenu .= str_replace(array("{class}", "{text}", "{key}"), array($itemwrapclass, $menu['title'], $key), $beforelink_str.'<span{class} rel="{key}">'.$beforetext_str.'{text}'.$aftertext_str.'</span>'.$afterlink_str.PHP_EOL);
	                }

	                if(is_array($menu['childmenus']) && count($menu['childmenus']) > 0){
	                    $navmenu .= $this->getAdminMenuHTML($menubar, $args, $menu['childmenus']);
	                }
	                $navmenu .= "\t</{$itemobj}>\n";
	            }
	        }
	        $navmenu .= "</{$menuobj}>\n";
	    }else{
	        $navmenu = "";
	    }
	    return $navmenu;
	}

	/**
	 * Generate target file paths for each admin menu and submenu
	 * @param array $menus
	 * @return array $menus
	 */
	function setupAdminMenusTargets($menus){
	    foreach($menus as $key => $menu){
	        $folder = preg_replace("/(^".DB_TABLE_PREFIX."|_cat(s*))/i", "", $menu['table']);
	        $filesuffix = preg_replace("/^".DB_TABLE_PREFIX."/i", "", $menu['table']);
	        $menus[$key]['target'] = '';
	        $fileprefix = '';
	        if($menu['table'] != ''){
	            $fileprefix = 'list-';
	            if(!empty($menu['alias'])) $filesuffix = $menu['alias'];
	            $menus[$key]['target'] = $fileprefix.$filesuffix.".php";
	        }
	        if(getifSet($menu['tourl']) == true && !empty($menu['alias'])){
	            $menus[$key]['target'] = $menu['alias'];
	        }
	        if(!empty($menu['childmenus'])){
	            $menus[$key]['childmenus'] = $this->setupAdminMenusTargets($menus[$key]['childmenus']);
	        }
	    }
	    return $menus;
	}

	/**
	 * Return the target URL for the specified menu
	 * The target is
	 *              1) activetheme/datatype-targettype-action.php
	 *              2) activetheme/targettype-action.php
	 *              3) activetheme/datatype-targettype.php
	 *              4) activetheme/targettype.php
	 *              5) systemtheme/datatype-targettype-action.php
	 *              6) systemtheme/targettype-action.php
	 *              7) systemtheme/datatype-targettype.php
	 *              8) systemtheme/targettype.php
	 * @param string $datatype
	 * @param string $targettype
	 * @return string
	 */
	function getAdminMenuTarget($datatype, $targettype, $action = null){
		global $_themes;

	    if(!empty($datatype) && $datatype != '- Not Bound -'){
	        $themefolder = $_themes->getThemePathUnder('admin', THEME_PATH_ROOT, 0);
	        $systemfolder = $_themes->getAdminSystemThemePath();
	        $datatype = strtolower($datatype);
	        $targettype = strtolower($targettype);
	        $action = strtolower($action);
	        $filetests = array();
	        $target = null;

	        if(!empty($action)){
		        $filetests[] = $themefolder.$datatype."-".$targettype."-".$action.".php";
		        $filetests[] = $themefolder.$targettype."-".$action.".php";
	        }
	        $filetests[] = $themefolder.$datatype."-".$targettype.".php";
	        $filetests[] = $themefolder.$targettype.".php";
	        if(!empty($action)){
		        $filetests[] = $systemfolder.$datatype."-".$targettype."-".$action.".php";
		        $filetests[] = $systemfolder.$targettype."-".$action.".php";
	        }
	        $filetests[] = $systemfolder.$datatype."-".$targettype.".php";
	        $filetests[] = $systemfolder.$targettype.".php";
	        foreach($filetests as $file){
	            if(file_exists(SITE_PATH.$file)){
	                $target = $file;
	                break;
	            }
	        }

	        return $target;
	    }else{
	        return null;
	    }
	}

	/**
	 * Return all admin data aliases
	 * @param boolean $fullurl
	 * @return array
	 */
	function getAdminMenuAliases($targettype = null){
	    global $_db_control, $_data;

	    $menus = $_db_control->setTable(MENUS_TABLE)->setWhere("menu_class = 'menu'")->getRec();
	    $arry = array();
	    foreach($menus as $menu){
		    if(is_numeric($menu['alias']) && $menu['alias'] > 0){
		    	$aliasarry = $_data->admin_aliases;
		    	if(is_array($aliasarry) && !empty($aliasarry)){
		    		$aliasatts = reset($aliasarry);
		    		if(!is_null($targettype) && $targettype != $menu['targettype'])
		    			$aliasatts['alias'] = ADMIN_FOLDER.str_replace(DB_TABLE_PREFIX, "", $aliasatts['data_type'])."/".$targettype;
		        	$arry[$menu['id']] = array("title" => $menu['title'], "data_type" => $aliasatts['data_type'], "alias_id" => $menu['alias'], "alias_path" => $aliasatts['alias']);
		        }
		    }
		}
	    return $arry;
	}

	/**
	 * Return the derived file target for the current page alias
	 *      Aliases made up of datatype + targettype, derived order is:
	 *              1) activetheme/datatype-targettype.php
	 *              2) activetheme/targettype.php
	 *              3) activetheme/index.php
	 *              4) systemtheme/datatype-targettype.php
	 *              5) systemtheme/targettype.php
	 *              6) systemtheme/index.php
	 *
	 *      Aliases made up of datatype + targettype + action, derived order is:
	 *              1) activetheme/datatype-targettype-action.php
	 *              2) activetheme/targettype-action.php
	 *              3) activetheme/targettype.php
	 *              4) activetheme/action.php
	 *              5) activetheme/index.php
	 *              6) systemtheme/datatype-targettype-action.php
	 *              7) systemtheme/targettype-action.php
	 *              8) systemtheme/targettype.php
	 *              9) systemtheme/action.php
	 *              10) systemtheme/index.php
	 */
	function getAdminMenuTargetFromAlias(){
	    global $_page, $_themes;

	    if(is_array($_page->urlpath)){
	        $themefolder = $_themes->getThemePathUnder('admin', THEME_PATH_ROOT, 0);
	        $systemfolder = ADMIN_FOLDER.ADM_THEME_FOLDER.ADM_THEME;
	        $datatype = $_page->urlpath[0];
	        $targettype = null;
	        $action = null;
	        if(count($_page->urlpath) > 1){
	            $targettype = $_page->urlpath[1];
	            if(count($_page->urlpath) > 2)
	                $action = $_page->urlpath[2];
	        }
	        if(empty($targettype)) $targettype = 'list';

	        $filetests = array();
	        $target = null;
	        $found = false;
	        if(empty($action)){
	            // datatype + targettype
	            $filetests[] = $themefolder.$datatype."-".$targettype.".php";
	            $filetests[] = $themefolder.$targettype.".php";
	            $filetests[] = $themefolder."index.php";
	            $filetests[] = $systemfolder.$datatype."-".$targettype.".php";
	            $filetests[] = $systemfolder.$targettype.".php";
	            $filetests[] = $systemfolder."index.php";
	        }else{
	            // datatype + targettype + action
	            $filetests[] = $themefolder.$datatype."-".$targettype."-".$action.".php";
	            $filetests[] = $themefolder.$targettype."-".$action.".php";
	            $filetests[] = $themefolder.$targettype.".php";
	            $filetests[] = $themefolder.$action.".php";
	            $filetests[] = $themefolder."index.php";
	            $filetests[] = $systemfolder.$datatype."-".$targettype."-".$action.".php";
	            $filetests[] = $systemfolder.$targettype."-".$action.".php";
	            $filetests[] = $systemfolder.$targettype.".php";
	            $filetests[] = $systemfolder.$action.".php";
	            $filetests[] = $systemfolder."index.php";
	        }
	        foreach($filetests as $file){
	            if(file_exists(SITE_PATH.$file)){
	                $target = $file;
	                $found = true;
	                break;
	            }
	        }
	    }
	    $_page->datatype = $datatype;
	    $_page->target = $target;
	    $_page->targettype = $targettype;
	    $_page->targetaction = $action;
	    $_page->found = $found;
	}

	/**
	 * Save admin menus layout (for AME) and settings to database (triggered by menu sorting)
	 * @param string $newlayout 		Array of menu ids describing the order set by the user (menuitem[id]=parentid)
	 * @return boolean
	 */
	function updateAdminMenuLayouts($newlayout){
	    global $_db_control, $_events;

	    $ok = false;
	    if(is_array($newlayout) && count($newlayout) > 0){
	        foreach($newlayout as $order => $menu){
	            preg_match("/([a-z0-9_-]+)\[(\d*)\]=([a-z0-9_-]*)/i", $menu, $data);
	            if(count($data) == 4){
	                $data[2] = intval($data[2]);
	                $data[3] = intval($data[3]);
	                // $parent_key = $_db_control->setTable(MENUS_TABLE)->setFields("key")->setWhere("`id` = '".$data[3]."'")->getRecItem();
	                $_db_control->setTable(MENUS_TABLE)->setFieldvals(array("parent_id" => $data[3], "sort_order" => $order))->setWhere("`id` = '".$data[2]."'")->updateRec();
	            }
	        }
	        if(!defined('IN_AJAX')) $_page->menus = $this->getAdminMenus();
	    }
		$_events->processTriggerEvent(__FUNCTION__, $newlayout);				// alert triggered functions when function executes
	    return $ok;
	}

	/**
	 * Return Admin Menu Editor (AME) HTML form fields
	 * @param type $menubarkey
	 * @param type $menukey
	 * @param type $menus
	 * @return string
	 */
	function getAdminMenuEditorHTML($menubarkey, $menukey = null, $menus = null){
	    global $_page, $_data, $_tax, $_db_control;

	    $outp = '';
	    if(is_null($menus)){
	        if(defined('IN_AJAX')) {
	            $menus = $this->getAdminMenus($menubarkey);
	        }else{
	            $menus = $_page->menus;
	        }
	    }
	    if(!empty($menubarkey) && (count($menus) > 0 || empty($menukey))){
	        // prepare fields from saved data
	        $datatypes = $_data->datatables;
	        array_unshift($datatypes, '- Select -');
	        $datatypes[] = "terms";

	        $dataaliases = $_data->admin_aliases;
	        array_unshift($dataaliases, array("alias" => "", "attribute_id" => 0));

	        $taxonomies = $_tax->getTaxonomies(array("active" => true));
	        array_unshift($taxonomies, array("id" => "", "name" => '- Select Taxonomy -'));

	        $menubars = $this->getAdminMenubars();
	        $menubar = array(0 => array("key" => $menubarkey, "id" => 0));
	        foreach($menubars as $mb){
	            if($mb["key"] == $menubarkey){
	                $menubar[0] = $mb;
	                break;
	            }
	        }

	        $parent_menus = $this->getAdminMenuRecords($menubar[0]['key']);
	        $targettypes = array("list" => "List", "edit" => "Editor", "terms" => "Terms", "media" => "Media");
	        $new_menu = array("id" => 0, "menu_bar_id" => $menubar[0]['id'], "parent_id" => 0, "title" => "", "table" => "", "alias" => "", "restricted" => 0, "target" => "", "targettype" => "", "taxonomy_id" => 0, "custom" => "", "active" => 1);

	        if(!empty($menukey)){
	            $clonemenu = (strpos($menukey, 'clone>>') !== false);
	            if($clonemenu) $menukey = str_replace('clone>>', '', $menukey);

	            $menu = $this->getAdminMenuObject($menus, $menukey);
		        $blocktitle = "Edit Admin Menu Item - ".$menu['title'];
	            if(count($menu) == 0) {
	                $menu = $new_menu;
	                $blocktitle = "Create Admin Menu Item";
	            }elseif($clonemenu){
	                $blocktitle = "Clone Admin Menu Item";
	            }
	            if('terms' != $menu['targettype'])
	            	unset($targettypes['terms']); 			// terms menus are created in the taxonomy settings
	        }else{
	            $clonemenu = false;
	            $menu = $new_menu;
	            $blocktitle = "Create Admin Menu Item";
	            unset($targettypes['terms']); 			// terms menus are created in the taxonomy settings
	        }

	        $s = array('title' => '', 'targettype' => '', 'datatype' => '', 'alias' => '', 'menubar' => '', 'parent' => '', 'options' => '');
	        if('terms' == $menu['targettype']) {
	        	$s['datatype'] = ' hidden';
	        	$s['alias'] = ' hidden';
	        }

	        // table, title, file alias
	        if(($menu['table'] != PAGES_TABLE && $menu['table'] != MEDIA_TABLE) || $menu['targettype'] != 'list') {
	            $outp = '<h3 class="header">'.$blocktitle.'</h3>';
	            $outp.= '<input type="hidden" id="adminmenu_dirty" value="" />'.PHP_EOL;
	            $outp.= '<input type="hidden" id="adminmenu_parent_id" name="adminmenu_parent_id" value="'.$menu['parent_id'].'" />'.PHP_EOL;
	            $outp.= '<input type="hidden" id="adminmenu_id" name="adminmenu_id" value="'.(($clonemenu) ? -$menu['id'] : $menu['id']).'" />'.PHP_EOL;

	            // title
	            $outp.= '<div class="setlabel'.$s['title'].'">Menu Title<span class="red">*</span>: <a href="#" class="hovertip" alt="The text displayed on the menu bar">[?]</a></div>';
	            $outp.= '<div class="setdata'.$s['title'].'"><input type="text" id="adminmenu_title" name="adminmenu_title" value="'.$menu['title'].'" data-recall="'.$menu['title'].'" class="widefldsize" /></div>'.PHP_EOL;

	           	// target type (and taxonomy)
	            $outp.= '<div class="setlabel'.$s['targettype'].'">Target Type<span class="red">*</span>: <a href="#" class="hovertip" alt="Either a list or edit page">[?]</a></div>';
	            $outp.= '<div class="setdata'.$s['targettype'].'"><select id="adminmenu_targettype" name="adminmenu_targettype" class="advselect"'.(($menu['targettype'] == 'terms') ? ' disabled="disabled"' : '').'>'.PHP_EOL;
	            foreach($targettypes as $typekey => $type){
	                $sel = (($typekey == $menu['targettype']) ? ' selected="selected"' : '');
	                $outp.= '<option value="'.$typekey.'"'.$sel.'>'.$type.'</option>'.PHP_EOL;
	            }
	            $outp.= '</select>';
	            $outp.= '<span id="adminmenu_taxdiv"'.(($menu['targettype'] != 'terms') ? ' class="hidden"' : '').'>';
	            $outp.= '&nbsp;Taxonomy: <select id="adminmenu_taxonomy" name="adminmenu_taxonomy" class="advselect">'.PHP_EOL;
	            foreach($taxonomies as $tax){
	                $sel = (($tax['id'] == $menu['taxonomy_id']) ? ' selected="selected"' : '');
	                $outp.= '<option value="'.$tax['id'].'"'.$sel.'>'.$tax['name'].'</option>'.PHP_EOL;
	            }
	            $outp.= '</select></span>';
	            if('terms' == $menu['targettype']) $outp .= '<p>Note: This menu\'s data type and alias were set when the related taxonomy was created.</p>';
	            $outp.= '</div>'.PHP_EOL;

	            // data type
	            $outp.= '<div class="setlabel'.$s['datatype'].'">Data Type<span class="red">*</span>:</div>';
	            $outp.= '<div class="setdata'.$s['datatype'].'"><select id="adminmenu_datatype" name="adminmenu_datatype" class="advselect">'.PHP_EOL;
	            foreach($datatypes as $type){
	                $sel = (($type == $menu['table']) ? ' selected="selected"' : '');
	                $outp.= '<option value="'.$type.'"'.$sel.'>'.ucwords(str_replace(DB_TABLE_PREFIX, "", $type)).'</option>'.PHP_EOL;
	            }
	            $outp.= '</select></div>'.PHP_EOL;

	            // alias
	            $outp.= '<div class="setlabel'.$s['alias'].'">Alias<span class="red">*</span>: <a href="#" class="hovertip" alt="The admin page to which this menu links">[?]</a></div>';
	            $outp.= '<div class="setdata'.$s['alias'].'"><p>Existing Alias: '.ADMIN_FOLDER.'<select id="adminmenu_alias" name="adminmenu_alias">'.PHP_EOL;
	            foreach($dataaliases as $alias){
	                $sel = (($alias['attribute_id'] == $menu['alias']) ? ' selected="selected"' : '');
	                $outp.= '<option value="'.$alias['attribute_id'].'"'.$sel.'>'.$alias['alias'].'</option>'.PHP_EOL;
	            }
	            $outp.= '</select></p>';
	            $outp.= 'Or, New Alias:&nbsp;'.ADMIN_FOLDER.'<input type="text" id="adminmenu_target" name="adminmenu_target" value="'.$menu['target'].'" />&nbsp;<input type="button" id="adminmenu_generate_target" value="Generate" /><br/>i.e.: pages/list<p></p>'.PHP_EOL;
	            $outp.= '</div>'.PHP_EOL;

	            // menubar
	            $outp.= '<div class="setlabel'.$s['menubar'].'">Menubar<span class="red">*</span>: <a href="#" class="hovertip" alt="Change to move menu item to another menubar">[?]</a></div>';
	            $outp.= '<div class="setdata'.$s['menubar'].'"><select id="adminmenu_menubar" name="adminmenu_menubar">'.PHP_EOL;
	            $outp.= '<option value="0:"'.(($menu['menu_bar_id'] == 0) ? ' selected="selected"' : '').'>- Not in menubar (Orphaned) -</option>'.PHP_EOL;
	            if(count($menubars) > 0){
	                foreach($menubars as $mb){
	                    $sel = (($mb['id'] == $menu['menu_bar_id']) ? ' selected="selected"' : '');
	                    $outp.= '<option value="'.$mb['id'].'"'.$sel.'>'.$mb['title'].'</option>'.PHP_EOL;
	                }
	            }
	            $outp.= '</select></div>'.PHP_EOL;

	            // parent menu
	            $outp.= '<div class="setlabel'.$s['parent'].'">Parent Menu Item:</div>';
	            $outp.= '<div class="setdata'.$s['parent'].'"><select id="adminmenu_parent" name="adminmenu_parent">'.PHP_EOL;
	            $outp.= '<option value="0"'.(($menu['parent_id'] == 0) ? ' selected="selected"' : '').'>- No Parent -</option>'.PHP_EOL;
	            if(count($parent_menus) > 0){
	                foreach($parent_menus as $parent_menu_key => $parent_menu){
	                    if($parent_menu['id'] != $menu['id']){
	                        $sel = (($parent_menu['id'] == $menu['parent_id']) ? ' selected="selected"' : '');
	                        $outp.= '<option value="'.$parent_menu['id'].'"'.$sel.'>'.$parent_menu['title'].'</option>'.PHP_EOL;
	                    }
	                }
	            }
	            $outp.= '</select></div>'.PHP_EOL;

	            // options
	            $outp.= '<div class="setlabel'.$s['options'].'">Enabled?: <a href="#" class="hovertip" alt="If set, menu will be shown.">[?]</a></div><div class="setdata"><input type="checkbox" id="adminmenu_active" name="adminmenu_active" value="1"'.((getIfSet($menu['active'])) ? ' checked="checked"' : '').' /> Yes, menu is shown</div>'.PHP_EOL;
	            $outp.= '<div class="setlabel'.$s['options'].'">Restricted Access?: <a href="#" class="hovertip" alt="If set, users must be allowed to \'view restricted menus\' to access it.">[?]</a></div><div class="setdata"><input type="checkbox" id="adminmenu_restricted" name="adminmenu_restricted" value="1"'.((getIfSet($menu['restricted'])) ? ' checked="checked"' : '').((!getBooleanIfSet($menu['active'])) ? ' disabled="disabled"' : '').' /> Yes, viewable only to users allowed to \'view restricted menus\'</div>'.PHP_EOL;
	            if(isset($menu['custom']) && $menu['custom'] == true) $outp.= '<div class="setlabel">Attention:</div><div class="setdata">This menu item was created by a custom program or plugin.  Changes here could impact performance.</div>'.PHP_EOL;
	            $delbtn = '&nbsp;&nbsp;<a href="#" class="adminmenu_delete" rel="'.$menu['id'].'">Delete Menu [Cannot be undone!]</a>';

	            // save
	            $outp.= '<div class="setlabel"></div><div class="setdata"><input type="button" id="adminmenu_save" value="Save Changes" />'.$delbtn.'</div>'.PHP_EOL;
	        }else{
	        	if($menu['alias'] == 0){
	        		$menu['alias'] = $_db_control->setTable(ATTRIBUTES_TABLE)->setFields("attribute_id")->setWhere("`data_type` = '".$menu['table']."' AND `attribute_class` = '".ATTR_CLASS_ADMIN_ALIAS."'")->getRecItem();
	        	}
	            $outp = '<h3 class="header">Edit Admin Menu Item - '.ucwords($menu['table']).'</h3>';
	            $outp.= '<input type="hidden" id="adminmenu_dirty" value="" />'.PHP_EOL;
	            $outp.= '<input type="hidden" id="adminmenu_parent" name="adminmenu_parent" value="0" />'.PHP_EOL;
	            $outp.= '<input type="hidden" id="adminmenu_parent_id" name="adminmenu_parent_id" value="0" />'.PHP_EOL;
	            $outp.= '<input type="hidden" id="adminmenu_id" name="adminmenu_id" value="'.$menu['id'].'" />'.PHP_EOL;
	            $outp.= '<input type="hidden" id="adminmenu_title" name="adminmenu_title" value="'.ucwords($menu['table']).'" data-recall="'.$menu['table'].'" />';
	            $outp.= '<input type="hidden" id="adminmenu_code" name="adminmenu_code" value="'.$menukey.'" />';
	            $outp.= '<input type="hidden" id="adminmenu_target" name="adminmenu_target" value="" />';
	            $outp.= '<input type="hidden" id="adminmenu_targettype" name="adminmenu_targettype" value="list" />';
	            $outp.= '<input type="hidden" id="adminmenu_datatype" name="adminmenu_datatype" value="'.$menu['table'].'" />';
	            $outp.= '<input type="hidden" id="adminmenu_alias" name="adminmenu_alias" value="'.$menu['alias'].'" />';
	            $outp.= '<input type="hidden" id="adminmenu_active" name="adminmenu_active" value="1" checked="checked" />';
	            $outp.= '<div class="setlabel">Menu Title<span class="red">*</span>: <a href="#" class="hovertip" alt="The unique text displayed on the menu bar">[?]</a></div><div class="setdata"> '.ucwords($menu['table']);
	            $outp.= '</div>'.PHP_EOL;

	            $outp.= '<div class="setlabel">Menubar<span class="red">*</span>: <a href="#" class="hovertip" alt="Change to move menu item to another menubar">[?]</a></div>';
	            $outp.= '<div class="setdata"><select id="adminmenu_menubar" name="adminmenu_menubar">'.PHP_EOL;
	            $outp.= '<option value="0:"'.(($menu['menu_bar_id'] == 0) ? ' selected="selected"' : '').'>- Not in menubar (Orphaned) -</option>'.PHP_EOL;
	            if(count($menubars) > 0){
	                foreach($menubars as $mb){
	                    $sel = (($mb['id'] == $menu['menu_bar_id']) ? ' selected="selected"' : '');
	                    $outp.= '<option value="'.$mb['id'].'"'.$sel.'>'.$mb['title'].'</option>'.PHP_EOL;
	                }
	            }
	            $outp.= '</select></div>'.PHP_EOL;

	            $outp.= '<div class="setlabel">Restricted Access?: <a href="#" class="hovertip" alt="If set, users must be allowed to \'view restricted menus\' to access it.">[?]</a></div><div class="setdata"><input type="checkbox" id="adminmenu_restricted" name="adminmenu_restricted" value="1"'.((getIfSet($menu['restricted'])) ? ' checked="checked"' : '').((!getBooleanIfSet($menu['active'])) ? ' disabled="disabled"' : '').' /> Yes, viewable only to users allowed to \'view restricted menus\'</div>'.PHP_EOL;
	            if(isset($menu['custom']) && $menu['custom'] == true) $outp.= '<div class="setlabel">Attention:</div><div class="setdata">This menu item was created by a custom program or plugin.  Changes here could impact performance.</div>'.PHP_EOL;
	            $outp.= '<div class="setlabel"></div><div class="setdata"><input type="button" id="adminmenu_save" value="Save Changes" /></div>'.PHP_EOL;
	        }
	    }
	    return $outp;
	}

	/**
	 * Save menu data to database
	 * @param string $id 				Menu id
	 * @param string $args              Menu data (menu_bar_id, parent_id, table, title, alias_id, targettype, restricted, active, custom)
	 * @return array 					True if successful and menu key
	 */
	function saveAdminMenu($id, $args, $incrDupKey = true){
	    global $_page, $_db_control, $_data, $_events;

	    $ok = false;
	    $key = null;
	    if(is_array($args)){
	        // prepare menu data
	        $menu_bar_id = getIntValIfSet($args['menu_bar_id']);
	        $new_menu_bar_id = getIntValIfSet($args['new_menu_bar_id']);
	        $table = getIfSet($args['table']);
	        $title = getIfSet($args['title']);
	        $targettype = getIfSet($args['targettype']);
	        $taxonomy_id = getIntValIfSet($args['taxonomy_id']);
	        $target = getIfSet($args['target']);
	        $parent_menu_id = getIntValIfSet($args['parent_id'], 0);

	        // if new menubar is different than current one and is not 0, deselect parent menu item
	        if($new_menu_bar_id != $menu_bar_id && $new_menu_bar_id > 0){
	            $parent_menu_id = 0;
	            $menu_bar_id = $new_menu_bar_id;
	        }

	        // update/add alias and use the id
            $alias_id = $_data->saveDataAlias(array("data_type" => $table, "attribute_class" => ATTR_CLASS_ADMIN_ALIAS, "newalias" => $target, "alias_id" => getIntValIfSet($args['alias'], 0)));
            if($alias_id > 0) {
	        	$data_type = $_db_control->setTable(ATTRIBUTES_TABLE)->setFields("data_type")->setWhere("attribute_id = '".$alias_id."'")->getRecItem();
            	$target = null;
            }

	        $menu_data = array(
	            "menu_bar_id" => $menu_bar_id,
	            "parent_id" => $parent_menu_id,
	            "table" => $table,
	            "title" => $title,
	            "alias" => $alias_id,
	            "target" => $target,
	            "targettype" => $targettype,
	            "taxonomy_id" => $taxonomy_id,
	            "restricted" => getBooleanIfSet($args['restricted']),
	            "window" => "",
	            "active" => getBooleanIfSet($args['active']),
	            "custom" => getBooleanIfSet($args['custom']),
	        );

	        // prepare menus
	        $menus = $this->getAdminMenus($menu_bar_id);

	        // check if key already exists for any other menu item
	        $menu_keys = $_db_control->flattenDBArray($_db_control->setTable(MENUS_TABLE)->setFields("`id`, `key`")->setWhere("`menu_class` = 'menu' AND `in_admin` = 1 AND `id` != '".$id."'")->getRec(), "id", "key");
	        if($incrDupKey)
	        	$key = getUniqueKey(codify($title), $menu_keys);
	        else{
	        	$key = codify($title);
	        	$_db_control->setTable(MENUS_TABLE)->setWhere("`key` = '".$key."' AND `table` = '".$table."' AND `in_admin` = 1 AND `id` != '".$id."'")->deleteRec();
	        }

	        if(!in_array($key, $menu_keys)) {
	            $ok = $this->saveAdminMenuRecord($key, $menu_data);
	        }else{
	            $key = 'The menu key is not unique.';
	        }
	    }
		$_events->processTriggerEvent(__FUNCTION__, $key);				// alert triggered functions when function executes
	    return array($ok, $key);
	}

	/**
	 * Delete an admin menu
	 * @param string $id                Menu id
	 * @return boolean
	 */
	function deleteAdminMenu($id){
	    global $_page, $_db_control, $_events;

	    $ok = false;
	    if($id > 0){
	        $_db_control->setTable(MENUS_TABLE)->setWhere("`id` = '$id' AND `in_admin` = 1")->deleteRec();
	        $_db_control->setTable(MENUS_TABLE)->setFieldvals(array("parent_id" => 0))->setWhere("`parent_id` = '$id' AND `in_admin` = 1")->updateRec();
	        $ok = true;
	    }
		$_events->processTriggerEvent(__FUNCTION__, $ok);				// alert triggered functions when function executes
	    return $ok;
	}

	/**
	 * Delete all custom admin menus (added programmatically to system and tagged as 'custom')
	 */
	function clearCustomAdminMenus(){
	    global $_page, $_events;

	    $ok = false;
	    if(defined('IN_AJAX')) {
	        $menus = $this->getAdminMenus('admin');
	    }else{
	        $menus = $_page->menus;
	        if(empty($menus)) $menus = $this->getAdminMenus('admin');
	    }

	    if(count($menus) > 0){
	        foreach($menus as $menukey => $menu){
	        	if(isset($menu['custom']) && $menu['custom'] == 1){
	        		unset($menus[$menukey]);
	        	}else if(isset($menu['childmenus']) && is_array($menu['childmenus'])){
	        		foreach($menu as $submenukey => $submenu){
	    				if(isset($submenu['custom']) && $submenu['custom'] == 1){
	    					unset($menus[$menukey]['childmenus'][$submenukey]);
	    				}
	        		}
	        	}
	        }
	        $_db_control->setTable(MENUS_TABLE)->setWhere("`custom` = 1 AND `in_admin` = 1")->deleteRec();
			$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	    }
	}
}

?>