<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("FIELDSLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

define ("FLD_DATA", 0);
define ("FLD_LABEL", 1);
define ("FLD_OPENROW", 2);
define ("FLD_CLOSEROW", 4);
define ("FLD_ALL", 8);
define ("FLD_SIMPLE", 16);

define ("BLOCK_DIV", 1);
define ("BLOCK_SPAN", 2);
define ("BLOCK_P", 3);

define ("MENU_DAY", "day");
define ("MENU_DOW", "dow");
define ("MENU_SHORTDOW", "sdow");
define ("MENU_MONTH", "month");
define ("MENU_SHORTMONTH", "smonth");
define ("MENU_YEAR", "year");
define ("MENU_ALPHA", "alpha");
define ("MENU_NUM", "number");
define ("MENU_ONOFF", "onoff");
define ("MENU_YESNO", "yesno");
define ("MENU_STATUS", "status");

/* FIELDS CLASS
 *
 * provides the system with functions and properties collectively classed as the Field API
 */
class FieldsClass {
	// overloaded data
	private $_fields = array();
	private $form_id = null;
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'fields');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * NEW: Display field using parameter array
	 * @param string $type Field type
	 * @param array $atts Field parameters presented in array format
	 * @version 3.9.5
	 */
	function showObject($form_id, $type, $atts){
		global $_error, $_events, $_render;

	    // trigger
	    list($retn, ) = $_events->processTriggerEvent(__FUNCTION__, array($form_id, $type, $atts), null);

	    $this->form_id = $form_id;
		foreach($atts as $key => $attr) ${strtolower($key)} = $attr;

		switch (strtolower($type)){
			case "address":
				$this->showAddressBox($atts);
				break;
			case "block":
				$_render->showBlock($atts);
				break;
			case "divblock":
				$atts['blocktype'] = BLOCK_DIV;
				$_render->startBlock($atts);
				break;
			case "parablock":
				$atts['blocktype'] = BLOCK_P;
				$_render->startBlock($atts);
				break;
			case "button":
				$this->showButtonField($atts);
				break;
			case "checkbox":
				$this->showCheckbox($atts);
				break;
			case "checkboxlist":
				$this->showCheckboxList($atts);
				break;
			case "custom":
				$this->showCustomField($atts);
				break;
			case "hidden":
				$this->showHiddenField($atts);
				break;
			case "hiddenlabel":
				$this->showHiddenlabelField($atts);
				break;
			case "cmseditor":
			case "htmleditor":
			case "html":
			case "editor":
				$this->showHTMLEditorField($atts);
				break;
			case "image":
				$this->showImageField($atts);
				break;
			case "file":
				$this->showFileField($atts);
				break;
			case "instructions":
				$_render->showInstructions($atts);
				break;
			case "information":
				$_render->showInformationRow($atts);
				break;
			case "label":
				$this->showLabel($atts);
				break;
			case "map":
			case "mapbox":
				$this->showMapBox($atts);
				break;
			case "menu":
			case "list":
			case "select":
			case "selectmenu":
				$this->showMenu($atts);
				break;
			case "pagetitle":
				$_render->showPageTitle($atts);
				break;
			case "password":
			case "cpassword":
				$atts['fldclass'] = getIfSet($atts['fldclass'])." ".((strtolower($type) == "cpassword") ? "confirm" : "")."passwordclass";
				$this->showPasswordField($atts);
				break;
			case "radiolist":
				$this->showRadioList($atts);
				break;
			case "textarea":
				$this->showTextareaField($atts);
				break;
			case "text":
			case "textbox":
				$this->showTextField($atts);
				break;
			case "pluginsincl":
				$this->showPluginsIncluded($atts);
				break;
			default:
				$more = ((function_exists("show".$type)) ? "  Perhaps you should try just 'show{$type}'." : "");
				$_error->addErrorMsg("ShowObject: Unknown form object type '$type'.{$more}");
				break;
		}
	}

	/**
	 * Displays Admin label box.  Usually followed with showRowStart or other show{Field} call
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $text
	 * @uses string $help
	 */
	function showLabel($atts){
	    $label = getIfSet($atts['label']);
	    $labelclass = getIfSet($atts['labelclass'], "editlabel");
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
	    (substr($label, -1, 1) == "*") ? $label = str_replace("*", "", $label).": ".REQD_ENTRY : $label .= ": ";

	    print "<div class=\"{$labelclass}\">{$label}";
		if(!empty($atts['help'])) print "<span class=\"hovertip\" alt=\"".$atts['help']."\">[?]</span>\n";
	    print "</div>";
	    if(!is_null(getIfSet($atts['text']))) print "<div{$divclass}>".$atts['text']."</div>\n";
	}

	/**
	 * Displays the 'Required Entry' text
	 */
	function showReqdTextLine($text = " = Required Entry"){
		echo REQD_ENTRY.$text."\n";
	}

	/**
	 * Displays Admin field row start tags
	 */
	function showRowStart(){
		print "<div class=\"editfield\">\n";
	}

	/**
	 * Closes a previously opened Admin field row (</div>)
	 */
	function showRowEnd(){
		print "</div>\n";
	}

	/**
	 * Displays Admin text input field
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $id
	 * @uses string $value
	 * @uses integer $maxlen
	 * @uses string $aftertext
	 * @uses string $js
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showTextField($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$value = getIfSet($atts['value']);
		$maxlen = getIntValIfSet($atts['maxlen']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
		$validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
		$fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$readonly = (getBooleanIfSet($atts['readonly']) ? " readonly=\"readonly\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		if($maxlen > 0) {
			$maxlen = " maxlength=\"{$maxlen}\" onKeyUp=\"countChars('{$id}', '{$id}_count', false, {$maxlen})\" onFocus=\"countChars('{$id}', '{$id}_count')\"";
			$maxlen_countfld = "<input type=\"text\" id=\"{$id}_count\" size=\"2\" value=\"".strlen($value)."\" disabled=\"disabled\" />\n";
		}elseif($maxlen < 0) {
			$maxlen = " maxlength=\"".abs($maxlen)."\"";
			$maxlen_countfld = "";
		}else{
			$maxlen = "";
			$maxlen_countfld = "";
		}
		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));

		if($label != "") $this->showLabel(array("label" => $label, "labelclass" => $labelclass, "help" => $wrapperElems['help']));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL) {
			print $wrapperElems['before'];
			if(strpos($id, "price") !== false) print "$ ";
			print "<input type=\"text\" id=\"{$id}\" name=\"{$name}\" value=\"{$value}\"{$maxlen}{$fldclass}{$js}{$disabled}{$readonly} />\n";
			print $maxlen_countfld.$wrapperElems['after'];
		}
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Displays Admin textarea input field
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $id
	 * @uses string $value
	 * @uses integer $cols
	 * @uses integer $rows
	 * @uses integer $height
	 * @uses string $toolbar
	 * @uses integer $maxlen
	 * @uses string $js
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses integer $displaytype 			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showTextareaField($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$value = getIfSet($atts['value']);
		$maxlen = getIntValIfSet($atts['maxlen']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$readonly = (getBooleanIfSet($atts['readonly']) ? " readonly=\"readonly\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);
		$cols = getIfSet($atts['cols'], 75);
		$rows = getIfSet($atts['rows'], 7);

		if($maxlen > 0) {
			$maxlen = " onKeyUp=\"countChars('{$id}', '{$id}_count', false, {$maxlen})\" onFocus=\"countChars('{$id}', '{$id}_count')\"";
			$maxlen_countfld = "<input type=\"text\" id=\"{$id}_count\" size=\"2\" value=\"".strlen($value)."\" disabled=\"disabled\" />";
		}else{
			$maxlen = "";
			$maxlen_countfld = "";
		}
		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));

		if($label != "") $this->showLabel(array("label" => $label, "labelclass" => $labelclass, "help" => $wrapperElems['help']));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL) {
			print<<<EOT
	{$wrapperElems['before']}<textarea id="{$id}" name="{$name}" cols="{$cols}" rows="{$rows}"{$maxlen}{$fldclass}{$js}{$disabled}{$readonly}>{$value}</textarea>{$wrapperElems['after']}
	{$maxlen_countfld}
EOT;
		}
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Displays CMS HTML editor box
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $id
	 * @uses string $value
	 * @uses integer $cols
	 * @uses integer $rows
	 * @uses array $dim
	 * @uses string $toolbar
	 * @uses integer $maxlen
	 * @uses string $aftertext
	 * @uses string $js
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showHTMLEditorField($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$value = getIfSet($atts['value']);
		$maxlen = getIntValIfSet($atts['maxlen']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
	    $editorclass = " ".CMS_EDITOR;
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate.$editorclass."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);
		$cols = getIfSet($atts['cols'], 20);
		$rows = getIfSet($atts['rows'], 15);
		$dim = getIfSet($atts['dim']);
		$ht = "";
		$wd = "";

		if(is_array($dim)) {
			$ht = ((!empty($dim[1])) ? "height  : '".$dim[1]."'" : "");
			$wd = ((!empty($dim[0])) ? "width   : '".$dim[0]."'" : "");
			if(!empty($ht) && !empty($wd)) $ht .= ",".PHP_EOL;
		}
		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));

		if($label != "") $this->showLabel(array("label" => $label, "labelclass" => $labelclass, "help" => $wrapperElems['help']));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL) print "{$wrapperElems['before']}<textarea id=\"{$id}\" name=\"{$name}\" cols=\"{$cols}\" rows=\"{$rows}\"{$fldclass}{$js}>{$value}</textarea>{$wrapperElems['after']}\n";

		if(CMS_EDITOR != '' && function_exists(CMS_EDITOR."_render")) {
			call_user_func(CMS_EDITOR."_render", $atts, $id, $ht, $wd, $maxlen);
		}

		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Displays Admin password input field
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $id
	 * @uses string $value
	 * @uses integer $maxlen
	 * @uses string $aftertext
	 * @uses string $js
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses boolean $noac
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showPasswordField($atts){
		global $_themes;

		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$value = getIfSet($atts['value']);
		$maxlen = getIntValIfSet($atts['maxlen']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
		$noac = (getBooleanIfSet($atts['noac'])) ? " readonly=\"readonly\"" : "";
		$noacclass = ($noac != "") ? " noac" : "";
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
	    $fldclass = getIfSet($atts['fldclass']).$validate.$noacclass;
		$js = getFormatIfSet($atts['js'], "", " %s");
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$readonly = (getBooleanIfSet($atts['readonly']) ? " readonly=\"readonly\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		$event = explode(":", getIfSet($atts['event']));
		$event[0] = strtolower($event[0]);

		$elemspair = getIfSet($event[1]);
		$elems = explode(",", $elemspair);
		if(count($elems) == 2 && $event[0] == "match") $fldclass .= " passmatch";
		($maxlen > 0) ? $maxlen = " maxlength=\"{$maxlen}\"" : $maxlen = "";
		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));

		$fldclass = trim($fldclass);
		if(!empty($fldclass)) $fldclass = ' class="'.$fldclass.'"';

		if($label != "") $this->showLabel(array("label" => $label, "labelclass" => $labelclass, "help" => $wrapperElems['help']));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL) {
			(strpos($id, "*") !== false) ? $imgid = "pass" : $imgid = "cpass";
			$id = str_replace("*", "", $id);
			print $wrapperElems['before'];
			print "<input type=\"password\" id=\"{$id}\" name=\"{$name}\" value=\"{$value}\"{$maxlen}{$fldclass}{$js}{$noac}{$validatefld}{$disabled}{$readonly} rel=\"$elemspair\" />";

			if(count($elems) == 2 && $event[0] == "match" && $elems[0] == $id){
				print "&nbsp;<img id=\"{$imgid}ok_{$elems[0]}\" class=\"{$imgid}ok\" src=\"".WEB_URL.$_themes->getThemePathUnder('admin')."images/icons/passok.png\" height=\"16\" width=\"16\" alt=\"\" />";
				print "&nbsp;<img id=\"{$imgid}bad_{$elems[0]}\" class=\"{$imgid}bad\" src=\"".WEB_URL.$_themes->getThemePathUnder('admin')."images/icons/passbad.png\" height=\"16\" width=\"16\" alt=\"\" />";
			}
			print $wrapperElems['after']."\n";
		}
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Displays Admin button
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $id
	 * @uses string $value
	 * @uses string $js
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showButtonField($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$value = getIfSet($atts['value']);
		$maxlen = getIntValIfSet($atts['maxlen']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_DATA);

		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));

		if($label != "") $this->showLabel(array("label" => $label, "help" => $wrapperElems['help']));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL) print $wrapperElems['before']."<input type=\"button\" id=\"{$id}\" name=\"{$name}\" value=\"{$value}\"{$fldclass}{$js}{$disabled} />".$wrapperElems['after']."\n";
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Displays Admin image field.  Prepares field as a FileUploader image field if FileUploader plugin initialized
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $ids
	 * @uses array $values
	 * @uses array $displayed
	 * @uses string $folder
	 * @uses string $aftertext
	 * @uses string $js
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showImageField($atts){
		global $_filesys;

		$ids = getIfSet($atts['ids']);
		if(empty($ids)) return false;

		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$value = getIfSet($atts['value']);
		$values = getIfSet($atts['values']);
		$maxlen = getIntValIfSet($atts['maxlen']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
		$displayed = getIfSet($atts['displayed']);
		$folder = getIfSet($atts['folder']);
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		$sublabel = str_replace("*", "", $label);
		if($label != '') $label .= " (JPG, GIF, PNG Only)";
		$fs_image = getifset($values[0]);
		$fs_thumb = getifset($values[1]);
		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));

		if($displaytype != FLD_SIMPLE){
			if($label != "") $this->showLabel(array("label" => $label, "labelclass" => $labelclass, "help" => $wrapperElems['help']));
			if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
			if($displaytype != FLD_LABEL) {
				print "<input type=\"hidden\" id=\"{$ids[1]}\" name=\"{$ids[1]}\" value=\"{$fs_image}\" />\n";
				if ($fs_thumb != "") print "<input type=\"hidden\" id=\"{$ids[2]}\" name=\"{$ids[2]}\" value=\"{$fs_thumb}\" />\n";

				if ($fs_image != "" && $displayed[0]) {
					if(substr($folder, -1, 1) != "/") $folder .= "/";
					$photo_pic	= $_filesys->checkImagePath($fs_image, MEDIA_FOLDER.IMG_UPLOAD_FOLDER.$folder, "");
					if(!empty($photo_pic)){
						list($width, $height, $origwidth, $origheight) = $_filesys->constrainImage(SITE_PATH.$photo_pic, 480);
						print " - Existing {$sublabel}: ".basename($fs_image);
						if($ids[3] != "") print "&nbsp;&nbsp;<input type=\"checkbox\" id=\"{$ids[3]}\" name=\"{$ids[3]}\" /> Delete (Cannot be undeleted)";
						print "<br/>- Size: (".intval($origwidth)." x ".intval($origheight)." pixels)";
						print "<div id=\"gallery\">";
						print "<a href=\"".WEB_URL.$photo_pic."\" title=\"$fs_image\">";
						print "<img src=\"".WEB_URL."$photo_pic\" border=\"1\" width=\"$width\" height=\"$height\" />";
						print "</a>";
						print "</div>\n";
					}
				}
				if ($fs_thumb != "" && $displayed[1]) {
					$photo_pic	= $_filesys->checkThumbPath($fs_thumb, MEDIA_FOLDER.THM_UPLOAD_FOLDER.$folder, "");
					if(!empty($photo_pic)){
						list($width, $height) = @getimagesize(SITE_PATH.$photo_pic);
						print "<br/> - Thumbnail";
						if($ids[3] != "" && !$displayed[0]) print "&nbsp;&nbsp;<input type=\"checkbox\" id=\"{$ids[3]}\" name=\"{$ids[3]}\" /> Delete (Cannot be undeleted)";
						print "<br/><img src=\"".WEB_URL.$photo_pic."\" border=\"1\" width=\"{$width}\" height=\"{$height}\" />";
					}
				}

				// fileuploader advanced input container
				$inputfld = '<input type="file" id="'.$ids[0].'" name="'.$ids[0].'"'.$fldclass.$js.' /><br/>'.IMG_WARNING;
				$width = "";
				$height = "";
				if(file_exists(WEB_URL.$fs_image)) list($width, $height) = @getimagesize(SITE_PATH.$fs_image);
				print <<<EOT
				<input type="hidden" name="{$ids[0]}_fld" id="{$ids[0]}_fld" class="qq-upload-field" value="{$fs_image}" />
				<input type="hidden" name="{$ids[0]}_mod" id="{$ids[0]}_mod" class="qq-upload-mod" value="" />
				<input type="hidden" name="{$ids[0]}_dim" id="{$ids[0]}_dim" class="qq-upload-mod" value="{$width}|{$height}" />
				<div id="{$ids[0]}">
					<noscript>
						<p>Please enable JavaScript to use file uploader.</p>
					</noscript>
				</div>
EOT;
			}
			if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
		}else{
			if($displaytype != FLD_LABEL) {
				print "<input type=\"hidden\" id=\"{$ids[1]}\" name=\"{$ids[1]}\" value=\"{$fs_image}\" />\n";
				if ($fs_thumb != "") print "<input type=\"hidden\" id=\"{$ids[2]}\" name=\"{$ids[2]}\" value=\"{$fs_thumb}\" />\n";
				if ($fs_image != "" && $displayed[0]) {
					if(substr($folder, -1, 1) != "/") $folder .= "/";
					$photo_pic = $_filesys->checkImagePath($fs_image, MEDIA_FOLDER.IMG_UPLOAD_FOLDER.$folder, "");
					list($width, $height, $origwidth, $origheight) = $_filesys->constrainImage(SITE_PATH.$photo_pic, 80);
					print "<img src=\"".WEB_URL."$photo_pic\" border=\"1\" width=\"$width\" height=\"$height\" />";
					if($ids[3] != "") print "&nbsp;<input type=\"checkbox\" id=\"{$ids[3]}\" name=\"{$ids[3]}\" /> Delete<br/>";
				}elseif ($fs_thumb != "" && $displayed[1]) {
					$photo_pic = $_filesys->checkThumbPath($fs_thumb, MEDIA_FOLDER.THM_UPLOAD_FOLDER.$folder, "");
					list($width, $height) = @getimagesize(SITE_PATH.$photo_pic);
					print "<img src=\"".WEB_URL.$photo_pic."\" border=\"1\" width=\"{$width}\" height=\"{$height}\" />";
					if($ids[3] != "") print "&nbsp;<input type=\"checkbox\" id=\"{$ids[3]}\" name=\"{$ids[3]}\" /> Delete<br/>";
				}
				$inputfld = '<input type="file" id="'.$ids[0].'" name="'.$ids[0].'"'.$fldclass.$js.' />';
				// simplified file browse input field
				print $wrapperElems['before'].$inputfld.$wrapperElems['after']."<br/>".IMG_WARNING."\n";
			}
		}
	}

	/**
	 * Displays Admin file browser input field
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $ids
	 * @uses string $fs_file
	 * @uses string $folder
	 * @uses string $type
	 * @uses string $aftertext
	 * @uses string $js
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showFileField($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;
		$ids = explode(",", $id);

		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$value = getIfSet($atts['value']);
		$maxlen = getIntValIfSet($atts['maxlen']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$folder = getIfSet($atts['folder']);
		$type = getIfSet($atts['type']);
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		if(strpos($label, "*") !== false) $fldclass = trim("required ".$fldclass);
		$sublabel = str_replace("*", "", $label);
		switch($type){
			case "doc":
				$label .= " (DOC or DOCX Only)";
				break;
			case "pdf":
				$label .= " (PDF Only)";
				break;
			case "audio":
				$label .= " (Audio File Only)";
				break;
			case "video":
				$label .= " (Video File Only)";
				break;
		}
		if($label != '') (substr($label, -1, 1) == "*") ? $label = str_replace("*", "", $label).": ".REQD_ENTRY : $label .= ": ";
		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));

		if($label != "") $this->showLabel(array("label" => $label, "labelclass" => $labelclass, "help" => $wrapperElems['help']));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL) {
			if (!empty($ids[1])) print "<input type=\"hidden\" id=\"{$ids[1]}\" name=\"{$ids[1]}\" value=\"{$value}\" />\n";

			$inputfld = '<input type="file" id="'.$ids[0].'" name="'.$ids[0].'"'.$fldclass.$js.$disabled.' /><br/>';
			// fileuploader advanced input container
			print <<<EOT
			<input type="hidden" name="{$ids[0]}_fld" id="{$ids[0]}_fld" class="qq-upload-field" value="{$value}" />
			<input type="hidden" name="{$ids[0]}_mod" id="{$ids[0]}_mod" class="qq-upload-mod" />
			<div id="{$ids[0]}">
				<noscript>
					<p>Please enable JavaScript to use file uploader.</p>
					{$inputfld}
				</noscript>
			</div>
EOT;
		}
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Displays Admin hidden field
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $id
	 * @uses string $value
	 * @uses boolean $noname   				True to exclude the 'name' attribute
	 */
	function showHiddenField($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$value = getIfSet($atts['value']);
		$noname = getBooleanIfSet($atts['noname']);

		echo "<input type=\"hidden\" id=\"{$id}\"".((!$noname) ? " name=\"{$name}\"" : "")." value=\"{$value}\" />\n";
	}

	/**
	 * Displays Admin hidden field and label text
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $id
	 * @uses string $label
	 * @uses string $value
	 * @uses boolean $noname   				True to exclude the 'name' attribute
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $help
	 */
	function showHiddenLabelField($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$value = getIfSet($atts['value']);
		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
		$noname = getBooleanIfSet($atts['noname']);
		$help = getIfSet($atts['help']);
		$fldtext = getIfSet($atts['fldtext']);

		if($labelclass != "") $labelclass = " {$labelclass}";
	    (substr($label, -1, 1) == "*") ? $label = str_replace("*", "", $label).": ".REQD_ENTRY : $label .= ": ";

	    echo "<div class=\"editlabel{$labelclass}\">{$label}";
		if($help != '') echo "<span class=\"hovertip\" alt=\"".$help."\">[?]</span>\n";
	    echo "</div>";
	    echo "<div{$divclass}>$fldtext\n";
		echo "<input type=\"hidden\" id=\"{$id}\"".((!$noname) ? " name=\"{$name}\"" : "")." value=\"{$value}\" />";
		echo "</div>\n";
	}

	/**
	 * displays Admin checkbox field
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $id
	 * @uses string $value
	 * @uses string $chkstate
	 * @uses string $text
	 * @uses string $wrappertext
	 * @uses string $separator
	 * @uses array $js
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $spanclass
	 * @uses string $fldclass
	 * @uses string $validate
	 * @uses string $validatefld
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showCheckbox($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$value = getIfSet($atts['value']);
		$chkstate = getIfSet($atts['chkstate']);
		$text = getIfSet($atts['text']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
		$spanclass = getFormatIfSet($atts['spanclass'], "", " class=\"%s\"");
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$separator = getIfSet($atts['separator']);
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		($chkstate == "" || intval($chkstate) == 0 || $chkstate != $value) ? $selected = "" : $selected = " checked=\"checked\"";
		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));

		if($label != "") $this->showLabel(array("label" => $label, "labelclass" => $labelclass, "help" => $wrapperElems['help']));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL) print $wrapperElems['before']."<span{$spanclass}><input type=\"checkbox\" id=\"{$id}\" name=\"{$name}\" value=\"{$value}\"{$selected}{$fldclass}{$js}{$disabled} />{$text}</span>".$wrapperElems['after'].$wrapperElems['help']."\n";
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * displays Admin checkbox list fields
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $id
	 * @uses array $selectionarray
	 * @uses array $chosenvaluesarray
	 * @uses string $separator
	 * @uses array $jsarray
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showCheckboxList($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$selectionarray = getIfSet($atts['selectionarray']);
		$chosenvaluesarray = getIfSet($atts['chosenvaluesarray']);
		$separator = getIfSet($atts['separator'], "&nbsp;");
		$spanclassarray = getIfSet($atts['spanclassarray']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$jsarray = getIntValIfSet($atts['jsarray']);
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

	    if(strpos($label, "*") !== false) $fldclass = trim("required ".$fldclass);
	    if($labelclass != "") $labelclass = " class=\"{$labelclass}\"";
	    (substr($label, -1, 1) == "*") ? $label = str_replace("*", "", $label).": ".REQD_ENTRY : $label .= ": ";

		if($label != "") $this->showLabel(array("label" => $label));
	    if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
	    if(is_array($selectionarray)){
	        foreach($selectionarray as $key => $value){
	            if(is_array($chosenvaluesarray)){
	                (in_array($key, $chosenvaluesarray)) ? $selected = " checked=\"checked\"" : $selected = "";
	            }elseif(is_numeric($chosenvaluesarray[$key])){
	                (($chosenvaluesarray[$key] > 0 || $value == $chosenvaluesarray[$key])) ? $selected = " checked=\"checked\"" : $selected = "";
	            }else{
	                (($chosenvaluesarray[$key] != '')) ? $selected = " checked=\"checked\"" : $selected = "";
	            }
	            ($spanclassarray[$key] != "") ? $spanclass = " class=\"{$spanclassarray[$key]}\"" : $spanclass = "";
	            if($displaytype != FLD_LABEL) print "<span{$spanclass}><input type=\"checkbox\" id=\"{$id}{$key}\" name=\"{$id}[{$key}]\" value=\"{$key}\"{$selected}{$fldclass}{$jsarray[$key]}{$disabled}/>{$value}</span>{$separator}\n";
	        }
	    }
	    if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Displays Admin radio button list fields
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $id
	 * @uses array $selectionarray
	 * @uses string $chosenvaluesarray
	 * @uses array $textarray
	 * @uses string $separator
	 * @uses string $js
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showRadioList($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$selectionarray = getIfSet($atts['selectionarray']);
		$chosenvaluesarray = getIfSet($atts['chosenvaluesarray']);
		$separator = getIfSet($atts['separator'], "&nbsp;");
		$spanclassarray = getIfSet($atts['spanclassarray']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
		$textarray = getIntValIfSet($atts['textarray']);
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		if($label != "") $this->showLabel(array("label" => $label, "labelclass" => $labelclass));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		foreach($selectionarray as $key => $value){
			($chosenvaluesarray == $value) ? $selected = " checked=\"checked\"" : $selected = "";
			($spanclassarray[$key] != "") ? $spanclass = " class=\"{$spanclassarray[$key]}\"" : $spanclass = "";
			if($displaytype != FLD_LABEL) print "<span{$spanclass}><input type=\"radio\" id=\"{$id}\" name=\"{$name}\" value=\"{$value}\"{$selected}{$fldclass}{$js}{$disabled} />{$textarray[$key]}</span>{$separator}\n";
		}
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Displays Admin select menu field
	 * @param array $atts 				Field parameters presented in array format
	 * @uses str $label
	 * @uses str $id
	 * @uses array $valuearray
	 * @uses str $selectedvalue
	 * @uses int $size
	 * @uses bool $multiple
	 * @uses str $js
	 * @uses string $optjs
	 * @uses string $aftertext
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses integer $displaytype		FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showMenu($atts){
		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$label = getIfSet($atts['label']);
		$valuearray = getIfSet($atts['valuearray']);
		$selectedvalue = getIfSet($atts['selectedvalue']);
		$disablevalue = getIfSet($atts['disablevalue']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
		$labelclass = getIfSet($atts['labelclass']);
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$optjs = getIfSet($atts['optjs']);
		$size = getIfSet($atts['size'], 1);
		$multiple = getBooleanIfSet($atts['multiple']);
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$readonly = (getBooleanIfSet($atts['readonly']) ? " readonly=\"readonly\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		if(!is_array($valuearray)) $valuearray = array();

		($multiple) ? $multiple_tag = " multiple=\"multiple\"" : $multiple_tag = "";
		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));

		if($label != "") $this->showLabel(array("label" => $label, "labelclass" => $labelclass, "help" => $wrapperElems['help']));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL) {
			echo $wrapperElems['before']."<select id=\"{$id}\" name=\"{$name}\" size=\"{$size}\" {$multiple_tag}{$js}{$fldclass}{$disabled}{$readonly}>\n";
			$optgroupstarted = false;
			foreach($valuearray as $key => $value){
				if(substr($key, 0, 1) == '-'){
					// opt group
					if($optgroupstarted) print "</optgroup>\n";
					print "<optgroup label=\"{$value}\">\n";
					$optgroupstarted = true;
				}elseif($disablevalue == $key && $disablevalue != ""){
					print "<option value=\"{$key}\" disabled=\"disabled\"{$optjs}>{$value}</option>\n";
	            }else{
					// option
					($selectedvalue == $key || ($selectedvalue == $value && !is_numeric($selectedvalue))) ? $selected = " selected=\"selected\"" : $selected = "";
					print "<option value=\"{$key}\"{$selected}{$optjs}>{$value}</option>\n";
				}
			}
			if($optgroupstarted) print "</optgroup>\n";
			print "</select>".$wrapperElems['after']."\n";
		}
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Displays Admin "Plugins Included" selector
	 * @param array $atts 				Field parameters presented in array format
	 * @uses str $label
	 * @uses str $id
	 * @uses str $data_type
	 * @uses str $js
	 * @uses string $optjs
	 * @uses string $aftertext
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses integer $displaytype		FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showPluginsIncluded($atts){
		global $_system, $_db_control;

		$id = getIfSet($atts['id']);
		if(empty($id)) return false;

		$name = ((!empty($this->form_id)) ? $this->form_id."[".$id."]" : $id);
		$label = getIfSet($atts['label']);
		$zone = strtolower(getIfSet($atts['zone']));
		$data_type = getIfSet($atts['data_type']);
		$value = getIfSet($atts['value']);
		$disablevalue = getIfSet($atts['disablevalue']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
		$labelclass = getIfSet($atts['labelclass']);
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s\"");
		$js = getFormatIfSet($atts['js'], "", " %s");
		$optjs = getIfSet($atts['optjs']);
		$size = getIfSet($atts['size'], 1);
		$multiple = getBooleanIfSet($atts['multiple']);
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$readonly = (getBooleanIfSet($atts['readonly']) ? " readonly=\"readonly\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		if(!is_array($zone)) $zone = explode(",", $zone);
		$zone = array_map("trim", $zone);
		$z = array_intersect($zone, array("admin", "front", "both"));
		if(empty($z)) {
			echo "Plugins_incl: Invalid zone '$zone'";
			return;
		}

		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));
		$pluginsIncl = array();
		if(!empty($value)) $pluginsIncl = json_decode($value, true);
		if(!is_array($pluginsIncl)) $pluginsIncl = array();

		if($label != "") $this->showLabel(array("label" => $label, "labelclass" => $labelclass, "help" => $wrapperElems['help']));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL) {
			echo $wrapperElems['before']."<select id=\"{$id}\" name=\"{$name}[]\" multiple=\"multiple\"{$js}{$fldclass}{$disabled}{$readonly}>\n";
			foreach($_system->plugins as $key => $plugin){
				// option
				if(in_array($plugin['usedin'], $zone)){
					$selected = "";
					if($plugin['builtin'] == 0 && $plugin['autoincl'] == 0){
						if(in_array($key, $pluginsIncl)) $selected = " selected=\"selected\"";
						print "<option value=\"{$key}\"{$selected}{$optjs}>{$plugin['name']}</option>\n";
					}
				}
			}
			print "</select>".$wrapperElems['after']."\n";
		}
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Prepares array containing standard menu data such as months, dates, etc.
	 * @param array $atts 		Field parameters presented in array format
	 * @uses integer $menutype
	 */
	function prepStandardMenu($menutype, $moredata = null){
		$data = array();
		switch($menutype){
			case MENU_DAY:
				for($i=1; $i<32; $i++) $data[sprintf("%02d", $i)] = $i;
				break;
			case MENU_SHORTDOW:
				$data = array("Mon", "Tues", "Wed", "Thurs", "Fri", "Sat", "Sun");
				break;
			case MENU_DOW:
				$data = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
				break;
			case MENU_MONTH:
				if($moredata == null){
					for($i=1; $i<13; $i++) $data[sprintf("%02d", $i)] = date("F", mktime(0,0,0,$i,1,2000));
				}elseif($moredata == MENU_NUM){
					for($i=1; $i<13; $i++) $data[sprintf("%02d", $i)] = $i." - ".date("F", mktime(0,0,0,$i,1,2000));
				}
				break;
			case MENU_SHORTMONTH:
				if($moredata == null){
					for($i=1; $i<13; $i++) $data[sprintf("%02d", $i)] = date("M", mktime(0,0,0,$i,1,2000));
				}elseif($moredata == MENU_NUM){
					for($i=1; $i<13; $i++) $data[sprintf("%02d", $i)] = $i." - ".date("M", mktime(0,0,0,$i,1,2000));
				}
				break;
			case MENU_YEAR:
				$moredata = intval($moredata);
				if ($moredata < 1990 || $moredata > 2029) $moredata = date("Y");
				for($i = date("Y")+2; $i >= $moredata; $i--) $data[$i] = $i;
				break;
			case MENU_ALPHA:
				for($i=1; $i<27; $i++) $data[chr($i+64)] = chr($i+64);
				break;
			case MENU_NUM:
				for($i=0; $i<10; $i++) $data[$i] = $i;
				break;
			case MENU_ONOFF:
				$data = array("1" => "On", "0" => "Off");
				break;
			case MENU_YESNO:
				$data = array("1" => "Yes", "0" => "No");
				break;
			case MENU_STATUS:
				$data = array("open" => "Open", "closed" => "Closed", "complete" => "Completed", "inprocess" => "In-Process");
				break;
		}
		if(is_array($moredata)) $data += $moredata;
		return $data;
	}

	/**
	 * Displays GoogleMap-powered Admin Map Box.  Requires initialization of GoogleMap Plugin
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses float $lat
	 * @uses float $lon
	 * @uses string $titlerowid
	 * @uses string $boxrowid
	 * @uses string $coordhint
	 * @uses string $js
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showMapBox($atts){
		$label = getIfSet($atts['label']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
		$lat = getIfSet($atts['lat']);
		$lon = getIfSet($atts['lon']);
		$titlerowid = getIfSet($atts['titlerowid'], "maptitlerow");
		$boxrowid = getIfSet($atts['boxrowid'], "mapboxrow");
		$coordhint = getIfSet($atts['coordhint']);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		if($label != "") $this->showLabel(array("label" => $label));
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL){
			print<<<EOT
			<tr id="{$boxrowid}"><td>
			{$coordhint}<br/>
			<div id="mapbox"{$js}></div>
			<script type="text/javascript" language="JavaScript">
				updateMapFromRegion({$lat}, {$lon});
			</script>
EOT;
		}
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Displays address entry box (address, street, street direction, city...)
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses array $colids
	 * @uses array $collabels
	 * @uses array $colvals
	 * @uses string $js
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses string $fldclass
	 * @uses string $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showAddressBox($atts){
		$colids = getIfSet($atts['colids']);
		if(empty($colids)) return false;

		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$value = getIfSet($atts['value']);
		$collabels = getIfSet($atts['collabels']);
		$colvals = getIfSet($atts['colvals']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
	    $validate = (getBooleanIfSet($atts['validate']) ? " validate" : "");
	    $validatefld = ($validate != "") ? " data-validate=\"".getIfSet($atts['validatemessage'])."\"" : "";
	    $fldclass = getFormatIfSet($atts['fldclass'], "", " class=\"%s".$validate."\"".$validatefld);
		$js = getFormatIfSet($atts['js'], "", " %s");
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$readonly = (getBooleanIfSet($atts['readonly']) ? " readonly=\"readonly\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		if(strpos($label, "*") !== false) $fldclass = trim("required ".$fldclass);
		if($labelclass != "") $labelclass = " class=\"{$labelclass}\"";
		if($label != '') (substr($label, -1, 1) == "*") ? $label = str_replace("*", "", $label).": ".REQD_ENTRY : $label .= ": ";
		$deflabels = array("Apt (eg. 203):", "Street # (...1950):", "Street Name (...Gordon):", "Type (...Drive):", "Dir.:", "City (...Vancouver):");
		$defids = array("aptnumber", "streetnumber", "streetname", "type", "dir", "city");
		if(is_array($collabels)) {
			if(count($collabels) == 0) $collabels = $deflabels;
		}else{
			$collabels = $deflabels;
		}
		if(is_array($colids)) {
			if(count($colids) == 0) $colids = $defids;
		}else{
			$colids = $defids;
		}
		$streettype_list = $this->buildDataList("streettype", $streettype, "address_streettype", "name", "name", "", "", "name", "", $js);
		$streetdir_list = $this->buildDataList("streetdir", $streetdir, "address_streetdir", "abbrev", "abbrev", "", "", "abbrev", "", $js);

		if($displaytype != FLD_DATA && $displaytype != FLD_CLOSEROW) print "<tr id=\"{$titlerowid}\"><td class=\"editlabel {$labelclass}\">{$label}</div>\n";
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL){
			foreach($colids as $idkey => $colid){
				switch ($colid){
					case "type":
						print "<div style=\"float: left; margin-right: 5px;\">{$collabels[$idkey]}<br/>{$streettype_list}</div>\n";
						break;
					case "dir":
						print "<div style=\"float: left; margin-right: 5px;\">{$collabels[$idkey]}<br/>{$streetdir_list}</div>\n";
						break;
					default:
						print "<div style=\"float: left; margin-right: 5px;\">{$collabels[$idkey]}<br/><input type=\"text\" id=\"{$colid}\" name=\"{$colid}\" value=\"{$colvals[$idkey]}\" size=\"25\"{$js} /></div>\n";
						break;
				}
			}
		}
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Display custom contents under label
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $label
	 * @uses string $contents
	 * @uses string $labelclass
	 * @uses string $divclass
	 * @uses integer $displaytype			FLD_DATA, FLD_LABEL, FLD_OPENROW, FLD_CLOSEROW, FLD_ALL, FLD_SIMPLE
	 */
	function showCustomField($atts){
		$label = getIfSet($atts['label']);
		$labelclass = getIfSet($atts['labelclass']);
		$divclass = getFormatIfSet($atts['divclass'], " class=\"editfield\"", " class=\"%s\"");
		$contents = getIfSet($atts['contents']);
		$wrappertext = getIfSet($atts['wrappertext']);
		$disabled = (getBooleanIfSet($atts['disabled']) ? " disabled=\"disabled\"" : "");
		$readonly = (getBooleanIfSet($atts['readonly']) ? " readonly=\"readonly\"" : "");
		$displaytype = getIfSet($atts['displaytype'], FLD_ALL);

		$wrapperElems = parseAttributes($wrappertext, array("before", "after", "help"));

		if($label != "") $this->showLabel($label, $labelclass, "", $wrapperElems['help']);
		if($displaytype == FLD_ALL || $displaytype == FLD_OPENROW) print "<div{$divclass}>";
		if($displaytype != FLD_LABEL) print $wrapperElems['before'].$contents.$wrapperElems['after'];
		if($displaytype == FLD_ALL || $displaytype == FLD_CLOSEROW) print "</div>\n";
	}

	/**
	 * Build a <select> object based on database data
	 * @param array $atts 					Field parameters presented in array format
	 * @uses string $selname
	 * @uses string $in_value
	 * @uses string $table
	 * @uses string/array $id_fld
	 * @uses string $data_fld
	 * @uses string $data_sep
	 * @uses string $crit
	 * @uses string $sort
	 * @uses string $limit
	 * @return string
	 */
	function buildDataList($selname, $in_value, $table, $id_fld, $data_fld, $data_sep, $crit, $sort, $limit, $js = "", $ignoreemptytable = false){
		global $_db_control;

		if(is_array($data_fld)){
			$fields = implode(", ", $data_fld);
			$header_elem = array($id_fld => "");
			foreach($data_fld as $fld) $header_elem[$fld] = "";
		}else{
			$fields = $data_fld;
			$header_elem = array($id_fld => "", $data_fld => "");
		}
		$fields = $id_fld.", ".$fields;
		$tblchk = $_db_control->dbGetQuery("show tables where tables_in_".DBNAME." = '%s'", array($table));
		if(count($tblchk) == 0 && !$ignoreemptytable) die ("{$table} table missing!");
		$datarec = $_db_control->setTable($table)->setFields($fields)->setWhere($crit)->setOrder($sort)->setLimit($limit)->getRec();
		array_unshift($datarec, $header_elem);
		$data_list = "<select name=\"{$selname}\" id=\"{$selname}\" size=\"1\"{$js}>\n";
		foreach($datarec as $item){
			($in_value == $item[$id_fld]) ? $selected = " selected=\"selected\"" : $selected = "";
			if(is_array($data_fld)){
				$text = "";
				foreach($data_fld as $fldkey => $fld) {
					if($text != "") $text .= $data_sep;
					(is_numeric($fldkey)) ? $key = $fld : $key = $fldkey;
					$text .= $item[$key];
				}
			}else{
				$text = $item[$data_fld];
			}
			$data_list .= "<option value=\"".$item[$id_fld]."\"$selected>".$text."</option>\n";
		}
		$data_list .= "</select>\n";
		return $data_list;
	}

	/**
	 * Return the HTML checked attribute if state is non-zero, true, or non-blank string
	 * @param $state 					1/True or 0/False
	 * @return html
	 */
	function showCheckedState($state){
		echo (((bool) $state == true) ? ' checked="checked"' : '');
	}

	/**
	 * Return the HTML selected attribute if state is non-zero, true, or non-blank string
	 * @param $state 					1/True or 0/False
	 * @return html
	 */
	function showSelectedState($state){
		echo (((bool) $state == true) ? ' selected="selected"' : '');
	}
}

?>