<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("THEMESLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

define ("THEME_SETTINGS_SAVE", 1);
define ("THEME_SETTINGS_CLOSE", 2);
define ("THEME_CFGERR_NAMEDUP", 1);
define ("THEME_CFGERR_SYSVERBAD", 2);
define ("THEME_CFGERR_TYPEBAD", 4);
define ("THEME_CFGERR_NAMENF", 256);
define ("THEME_CFGERR_SYSVERNF", 512);

define ("THEME_PATH_ROOT", "__root__");
define ("THEME_PATH_ALL", "");

/**
 * THEMES CLASS
 * Stores and manages theme-related data
 */
class ThemeClass {
	// overloaded data
	protected static $_instance = null;

	private function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_events;

		$s = new self;
		$_events->processTriggerEvent(__FUNCTION__.'_themes');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Read admin themes folders searching for valid theme.info files for processing
	 * @param string $type 				Theme type (admin or website)
	 * @internal Core function
	 * @return array
	 * @version 3.6
	 */
	function readInstalledThemeFolders($type){
		global $_settings, $_db_control, $_filesys, $_events;

		$type = strtolower($type);
		if($type != 'admin' && $type != 'website') return false;

		// first, read folders and identify whether their contents
		// are already in DB or are newly installed

		$updpaths = array();	// themes that are found in db and ready for update
		$newpaths = array();	// themes not found in db and deemed new
		$errpaths = array();	// themes that are found in db but were marked with an error
		$base_dir = (($type == "admin") ? SITE_PATH.ADMIN_FOLDER.ADM_THEME_FOLDER : SITE_PATH.THEME_FOLDER);
		$base_dir = $_filesys->addSlash($base_dir);
		if(false !== ($handle = opendir($base_dir))) {
			while (false !== ($folder = readdir($handle))) {
				if($folder != '.' && $folder != '..' && $folder != '.sys'){
					if(is_dir($base_dir.$folder)){
						$sub_dir = $base_dir.$folder."/";		// .../_themes/folder
						if(file_exists($sub_dir.'theme.info')){
	                        // remove record if folder is relative
	                        // $_db_control->setTable(THEMES_TABLE)->setWhere("folder = '".$folder."'")->deleteRec();

							// get the theme record where the folders match
							// - if matched, theme is being updated, otherwise it is new
							$themefromdb = $_db_control->setTable(THEMES_TABLE)->setWhere("folder = '".$folder."/'")->setLimit()->getRec();
							if(count($themefromdb) > 0){
								// matched
								if($themefromdb[0]['error_code'] == 0){
									$updpaths[] = array("id" => $themefromdb[0]['id'], "folder" => $folder."/", "info" => $folder."/theme.info", "tempname" => "");
								}else{
									$errpaths[] = array("id" => $themefromdb[0]['id'], "folder" => $folder."/", "info" => $folder."/theme.info", "tempname" => "");
								}
							}else{
								// not matched
								$newpaths[] = array("id" => 0, "folder" => $folder."/", "info" => $folder."/theme.info", "tempname" => $folder);
							}
						}
					}
				}
			}
			closedir($handle);
		}
		return array_merge($updpaths, $errpaths, $newpaths);
	}

	/**
	 * Read theme.info file data, populating themelist array
	 * @param string $type 				Theme type (admin or website)
	 * @param array $themepaths
	 * @internal Core function
	 * @return array
	 * @version 4.0
	 */
	function readInstalledThemeInfoFiles($type, $themepaths){
		global $_settings;

		$type = strtolower($type);
		if($type != 'admin' && $type != 'website') return false;

		// first, read folders and identify whether their contents
		// are already in DB or are newly installed

		$valid_setting_keys = array('name', 'author', 'ver', 'descr', 'sysver', 'website',
									'usedin', 'type', 'settingsfunc', 'headerfunc', 'parent',
									'stylefiles', 'scriptfiles', 'menuslots', 'locations',
									'layoutfolder', 'sites');

		if(is_null($themepaths)) return false;
		$base_dir = (($type == 'admin') ? SITE_PATH.ADMIN_FOLDER.ADM_THEME_FOLDER : SITE_PATH.THEME_FOLDER);
		$themeslist = array();

		foreach($themepaths as $pathdata){
			$fhandle = @fopen($base_dir.$pathdata['info'], 'r');
			if($fhandle !== false){
				$themename = null;
				$themeslug = null;
				$themearry = array();
				$themearry['id'] = $pathdata['id'];
				$themeerrs = array();
				$themeerrcode = 0;
				$themefolder = $pathdata['folder']."/";

				while($setting = fscanf($fhandle, "%[#]%s\t%[^\t]")){
					list($n, $key, $val) = $setting;
					$key = strtolower(str_replace(':', '', $key));
					$val = trim($val);
					if(in_array($key, $valid_setting_keys)){
						switch($key){
							case 'name':
								if(is_null($themename)){
									if($pathdata['tempname'] != '') $val = ucwordsAdv($pathdata['tempname']);
									$themeslug = slugify($val);
									if(isset($themeslist[$themeslug])){
										// oops! slug already in use
										$themeerrs[] = "The name '".$val."' in ".$themefolder."theme.info has been registered to another theme.";
										$themeerrcode += THEME_CFGERR_NAMEDUP;
										// use folder as temp name so data can be saved
										$themeslug = slugify($themefolder);
										$themename = ucwordsAdv($themeslug);
									}else{
										$themename = $val;
									}
								}
								break;
							case 'sysver':
								$val_d = convertCodeVer2Dec($val);
								if($val_d < 2.06 or $val_d > getCodeVer()){
									$themeerrs[] = "SYSVER must be between 2.6.0 and ".CODE_VER." in ".$themefolder."theme.info.";
									$themeerrcode += THEME_CFGERR_SYSVERBAD;
								}
								break;
							case 'website':
								if(strtolower(substr($val, 0, 4)) != 'http' && $val != '') $val = 'http://'.$val;
								break;
							case 'menuslots':
								$val = explode(",", $val);
								$val = array_combine(array_keys(array_fill(1, count($val), 0)), array_values($val));
								$val = json_encode($val);
								break;
						}
						$themearry[$key] = $val;
					}
				}
				fclose($fhandle);

				if($themename == null) {
					$themeerrs[] = 'NAME is missing in '.$themefolder.'/theme.info.';
					$themeerrcode += THEME_CFGERR_NAMENF;
					continue;
				}elseif(!isset($themearry['sysver'])){
					$themeerrs[] = 'SYSVER is required in '.$themefolder.'/theme.info.';
					$themeerrcode += theme_CFGERR_SYSVERNF;
				}

				if(count($themeerrs) > 0){
					$themearry['errors'] = $themeerrs;
					$themearry['error_code'] = $themeerrcode;
					$_settings->$settings_issues['themes'] = array_merge($_settings->$settings_issues['themes'], $themeerrs);
				}
				$themearry['folder'] = $pathdata['folder'];
				$themeslist[$themeslug] = $themearry;
			}
		}
		return $themeslist;
	}

	/**
	 * Return an array containing the file and proper names of all layout files found in folder
	 * @param string $type 					Either 'admin' or 'website'
	 * @param string $themefolder 			The folder of the designated theme
	 * @param string $layoutfolder 			The folder, under themefolder, where layout files are stored.  If empty, the themefolder is assumed.
	 * @return array
	 */
	function getThemeLayouts($type, $themefolder, $layoutfolder = ""){
		global $_filesys, $_events;

		$layouts = array();
		if(!empty($themefolder) && in_array($type, array('admin', 'website'))){
			$themefolder = (($type == 'admin') ? SITE_PATH.ADMIN_FOLDER.ADM_THEME_FOLDER : SITE_PATH.THEME_FOLDER).$_filesys->addSlash($themefolder);
			if(!empty($layoutfolder)) $themefolder .= $_filesys->addSlash($layoutfolder);
			if(false !== ($handle = opendir($themefolder))) {
				while (false !== ($file = readdir($handle))) {
					if(!is_dir($themefolder.$file) && substr($file, -11) == '.layout.php'){
						$ini = parse_ini_file($themefolder.$file);
						$basefile = substr($file, 0, -11);
						if(!empty($ini['layout_name']))
							$layouts[$basefile] = $ini['layout_name'];
						else
							$layouts[$basefile] = ucwords($basefile);
					}
				}
				closedir($handle);
			}
		}
		return $layouts;
	}

	/**
	 * Load theme settings found in theme.info files into database.
	 * This function does not activate a theme.  It simply
	 * processes any new or updated themes.
	 * @param string $type 			Type of theme (admin, website, both)
	 * @internal Core function
	 * @version 3.6
	 */
	function getInstalledThemes($type = "both", $refresh = true){
		global $_system, $_settings, $_users, $_db_control, $_filesys, $_events;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
		$type = strtolower($type);
		if($type == "both")
			$types = array("admin", "website");
		else
			$types = array($type);

		$updflag = rand(0, 99999);
		$_settings->clearSettingsIssues('themes');
		$_settings->clearSettingsIssues('themes-info');

		$themes = array();
		$paths = array();

		if($refresh){
			foreach($types as $the_type){
				if($_users->userIsAllowedTo('view_themes') && ($the_type != 'admin' || $the_type != 'website')){
					$themepaths = $this->readInstalledThemeFolders($the_type);
					$themeslist = $this->readInstalledThemeInfoFiles($the_type, $themepaths);

					// loop through themes updating/adding each to the database
					foreach($themeslist as $themeslug => $themearry){
						// find theme record
						$themeid = $themearry['id'];

						// handle read cycle errors
						if(isset($themearry['errors'])){
							// error detected...
							$themearry['errors'] = join("<br/>", $themearry['errors']);
						}else{
							// no problems here...
							$themearry['errors'] = '';
							$themearry['error_code'] = 0;
						}

						// get list of theme layout files
						$themearry['layouts'] = json_encode($this->getThemeLayouts($the_type, $themearry['folder'], getIfSet($themearry['layoutfolder'])));
						unset($themearry['id']);
						if($themeid > 0){
							// update
							$fldvals = array("type" => $the_type, "updflag" => $updflag);
							foreach($themearry as $key => $data) $fldvals[$key] = str_replace("'", "&#34;", $data);
							$_db_control->setTable(THEMES_TABLE)->setFieldvals($fldvals)->setWhere("`id` = '{$themeid}'")->updateRec();
						}else{
							// insert
							$fldvals = array("type" => $the_type, "updflag" => $updflag);
							foreach($themearry as $key => $data) $fldvals[$key] = str_replace("'", "&#34;", $data);
							$themeid = $_db_control->setTable(THEMES_TABLE)->setFieldvals($fldvals)->insertRec();
						}
					}
					$_db_control->setTable(THEMES_TABLE)->setWhere("(updflag = '-1' OR updflag != '{$updflag}') AND `type` = '{$the_type}'")->deleteRec();

					// create system themes object
					$themes[$the_type] = array('active' => array(), 'inactive' => array());
					$themelist = $_db_control->setTable(THEMES_TABLE)->setWhere("`type` = '{$the_type}' AND `error_code` = 0")->setOrder("active ASC, name ASC")->getRec();
					foreach($themelist as $themearry){
						$incl = $_filesys->getLowestChildFolder($themearry['folder']);
						if($themearry['active'] == 1)
							$themes[$the_type]['active'][$incl] = $themearry;
						else
							$themes[$the_type]['inactive'][$incl] = $themearry;
					}

					$paths[$the_type] = $this->getActiveThemePaths($the_type, $themes[$the_type]['active'], null, array());
				}
			}

		    // if zone is admin and no active themes are set, ensure that the .sys theme is set
		    if(!isset($paths['admin'][THEME_PATH_ROOT])){
		    	$paths['admin'][THEME_PATH_ROOT][] = ADMIN_FOLDER.THEME_FOLDER.".sys/";
		    }

			$_system->themepaths = $paths;
			$_system->themes = $themes;
			$_db_control->setTable(SETTINGS_TABLE)->setFieldvals(array("name" => "THEME_PATHS", "value" => json_encode($paths), "type" => "str"))->setWhere("`name` = 'THEME_PATHS'")->replaceRec();
		}else{
			$paths = $_db_control->setTable(SETTINGS_TABLE)->setFields('value')->setWhere("`name` = 'THEME_PATHS'")->getRecItem();
			if($paths != '')
				$_system->themepaths = json_decode($paths, true);
			else
				$_system->themepaths = array();
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Initialize theme settings
	 * @param string $type 				Either admin or website
	 * @internal Core function
	 * @return boolean
	 */
	function getActiveThemePaths($type, $themearry, $parentincl = null, $paths = array()){
		global $_db_control, $_system;

		if(count($themearry) > 0 || !in_array($type, array("admin", "website"))){
			foreach($themearry as $incl => $theme){
				if($theme['parent'] == $parentincl){
					$key = ((is_null($parentincl)) ? THEME_PATH_ROOT : $parentincl);
					$paths[$key][] = (($type == "admin") ? ADMIN_FOLDER.ADM_THEME_FOLDER : THEME_FOLDER).$theme['folder'];
					$paths = $this->getActiveThemePaths($type, $themearry, $incl, $paths);
				}
			}
		}
		return $paths;
	}

	/**
	 * Return a specific theme path under the root or another theme
	 * @param string $type 				Either admin or website
	 * @param string $parentincl 		The inclusion verb of the parent theme, or THEME_PATH_ROOT to focus on the primary themes
	 * @param string $indx 				The zero-based array index of the theme path to return
	 * @param string 					The path or null if nothing found
	 */
	function getThemePathUnder($type, $parentincl = THEME_PATH_ROOT, $indx = 0){
		global $_system;

		$themearry = $_system->themepaths;
		$path = null;
		if(count($themearry) > 0 && in_array($type, array("admin", "website"))){
			if(isset($themearry[$type][$parentincl][$indx]))
				$path = $themearry[$type][$parentincl][$indx];
		}
		return $path;
	}

	/**
	 * Return the theme path from the calling theme file or specific theme inclusion verb.
	 * This function will return null if called from a non-theme file and $themeincl is null
	 * @param string $type 				Either admin or website
	 * @param string $themeincl 		The inclusion verb of the theme
	 */
	function getThemePath($type, $themeincl = null){
		global $_system;

		$themearry = $_system->themepaths;
		$path = null;
		if(count($themearry) > 0 && in_array($type, array("admin", "website"))){
			if(is_null($themeincl)){
				$stack = debug_backtrace();
				$caller = str_replace("\\", "/", $stack[0]['file']);
				$rootpath = (($type == "admin") ? ADMIN_FOLDER.THEME_FOLDER : THEME_FOLDER);
				if(strpos($caller, SITE_PATH.$rootpath) !== false){
					// the calling function was from a theme file
					$callerpath = explode("/", str_replace(SITE_PATH.$rootpath, "", $caller));
					$path = $rootpath.$callerpath[0];
				}
			}else{
				$path = (($type == "admin") ? ADMIN_FOLDER.THEME_FOLDER : THEME_FOLDER).$themeincl."/";
			}
		}
		return $path;
	}

	/**
	 * Return the path to the admin system theme
	 * @return string
	 */
	function getAdminSystemThemePath(){
		return ADMIN_FOLDER.ADM_THEME_FOLDER.".sys/";
	}

	/**
	 * Get information (other than folder) about an active theme.
	 * @param string $type 			Theme type (admin or website)
	 * @param string $themeincl 	The slug of the theme
	 * @param string $attr 			An optional array element to retrieve.  Assumes 'name'.
	 *								Valid attr: "name", "ver", "sysver", "descr", "author", "website", "settingfunc", "headerfunc",
	 * 									"scriptfiles", "stylefiles", "errors", "menuslots", "locations", "sites", "layouts"
	 * @return mixed 				The value of the themes setting.  Calling the function with the 'folder' attribute will
	 * 								return the path to the theme from the site root.
	 */
	function getThemeInfo($type, $themeincl, $attr = "name"){
		global $_db_control, $_system;

	    $result = false;
	    $type = strtolower($type);
	    $themeincl = strtolower($themeincl);
	    if($type == 'admin' || $type == 'website'){
	        // retrieve from themes table
	        if(in_array($attr, array("name", "folder", "ver", "sysver", "descr", "author", "website", "settingfunc", "headerfunc", "scriptfiles", "stylefiles", "errors", "locations", "sites", "menuslots", "layouts"))) {
	        	if(!is_null($themeincl)){
	        		if(isset($_system->themes[$type]['active'][$themeincl][$attr]))
	        			$result = $_system->themes[$type]['active'][$themeincl][$attr];
	        		else
	        			$result = $_db_control->setTable(THEMES_TABLE)->setFields($attr)->setWhere("`type` = '$type' AND `folder` = '{$themeincl}/' AND `active` = 1")->getRecItem();
	        	}else{
	        		$result = $_db_control->setTable(THEMES_TABLE)->setFields($attr)->setWhere("`type` = '$type' AND `active` = 1")->getRecItem();
	        	}
		    }
	    }
	    return $result;
	}

	/**
	 * Get information (other than folder) about an active admin theme.
	 * @param string $themeincl 	The slug of the theme or, if empty, the slug of the primary active theme
	 * @param string $attr 			An optional array element to retrieve.  Assumes 'name'
	 * @return mixed 				The value of the themes setting
	 */
	function getAdminThemeInfo($themeincl = null, $attr = "name"){
		global $_db_control;

		if(empty($themeincl)){
			// no incl provided, get primary active theme
			$themeincl = $_db_control->setTable(THEMES_TABLE)->setFields('folder')->setWhere("parent IS NULL AND active = 1 AND type = 'admin'")->getRecItem();
			$themeincl = trim($themeincl, "/");
		}
		if(!empty($themeincl))
			return $this->getThemeInfo("admin", $themeincl, $attr);
		else
			return null;
	}

	/**
	 * Get information (other than folder) about an active website theme.
	 * @param string $themeincl 	The slug of the theme or, if empty, the slug of the primary active theme
	 * @param string $attr 			An optional array element to retrieve.  Assumes 'name'
	 * @return mixed 				The value of the themes setting
	 */
	function getWebsiteThemeInfo($themeincl = null, $attr = "name"){
		global $_db_control;

		if(empty($themeincl)){
			// no incl provided, get primary active theme
			$themeincl = $_db_control->setTable(THEMES_TABLE)->setFields('folder')->setWhere("parent IS NULL AND active = 1 AND type = 'website'")->getRecItem();
			$themeincl = trim($themeincl, "/");
		}
		if(!empty($themeincl))
			return $this->getThemeInfo("website", $themeincl, $attr);
		else
			return null;
	}

	/**
	 * Execute the theme_ops (or theme control) file for the active theme
	 * @param string $type 				Either admin or website
	 * @param string $parentincl 		The inclusion verb of the parent theme,
	 * 										THEME_PATH_ROOT to focus on the themes at root/primary level, or
	 *										THEME_PATH_ALL to process all path levels
	 * @param string $indx 				The array index of the theme path (-1 = all paths at level, 0+ = specific path value)
	 */
	function initThemeOps($type = "admin", $parentincl = "", $indx = -1){
		global $_system, $_events;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
		if(count($_system->themepaths) > 0 && isset($_system->themepaths[$type]) && in_array($type, array("admin", "website"))){
			if($parentincl == THEME_PATH_ALL){
				// process all theme paths at all levels
				foreach($_system->themepaths[$type] as $level => $pathobj){
					foreach ($pathobj as $i => $path) {
						if(file_exists(SITE_PATH.$path."theme_ops.php"))
							include_once (SITE_PATH.$path."theme_ops.php");
					}
				}
			}else{
				if($indx == -1){
					// process all theme paths at specific level
					if(isset($_system->themepaths[$type][$parentincl])){
						foreach($_system->themepaths[$type][$parentincl] as $path){
							if(file_exists(SITE_PATH.$path."theme_ops.php"))
								include_once (SITE_PATH.$path."theme_ops.php");
						}
					}
				}else{
					include_once (SITE_PATH.$this->getThemePathUnder($type, $parentincl, $indx)."theme_ops.php");
				}
			}
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * CORE: Setup plugins, jQuery and execute any triggers
	 * @version 3.0
	 */
	function prepHeadSection(){
	    global $_system, $jq_lines, $_events, $_plugins, $_events;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes

	    // Jquery is always loaded
	    $_plugins->loadPluginScripts("jquery-min");
	    $loadedplugins = array("jquery");

	    // Initiate built-in framework/tool header processes
	    if(IN_ADMIN) {
	        // admin pages always include dialog and tabs as well as other on-demand UIs
		    $uiwidgets = array("dialog");
		    foreach($_plugins->uiwidgets as $w) $uiwidgets[] = $w;		// append input widgets
		    $scripts_to_load = array("jqueryui");
		    foreach($_plugins->scripts as $s) $scripts_to_load[] = $s;	// append input scripts
		    $scripts_to_load["uiwidgets"] = $uiwidgets;					// join widgets to scripts
	        $_plugins->loadPluginScripts($scripts_to_load);
	       	$loadedplugins[] = "jqueryui";

	        if(CMS_EDITOR != '' && isset($_system->plugins[CMS_EDITOR])){
	            $the_plugin = $_system->plugins[CMS_EDITOR];
	            $_plugins->loadPluginScripts(CMS_EDITOR, array("folder" => $the_plugin['folder'], "initfile" => $the_plugin['initfile'], "headerfunc" => $the_plugin['headerfunc']));
	        }

		    // Initiate basic theme scripts
		    $this->loadThemeScripts("admin");

		    // Initiate basic plugin scripts
		    $_plugins->loadPluginScripts("basic");

		    // Initiate theme headerfunc function, if present
		    $this->callThemeHeaderFunc("admin");
	    }else{
	        // front-end on-demand UIs
		    $uiwidgets = array();
		    foreach($_plugins->uiwidgets as $w) $uiwidgets[] = $w;		// append input widgets
		    $scripts_to_load = array("jqueryui");
		    foreach($_plugins->scripts as $s) $scripts_to_load[] = $s;	// append input scripts
		    $scripts_to_load["uiwidgets"] = $uiwidgets;					// join widgets to scripts
	        $_plugins->loadPluginScripts($scripts_to_load);
	        $loadedplugins[] = "jqueryui";

		    // Initiate basic theme scripts
		    $this->loadThemeScripts("website");

		    // Initiate theme headerfunc function, if present
		    $this->callThemeHeaderFunc("website");
	    }

	    // Initiate additional framework header processes
	    if(count($_system->frameworks) > 0){
	        foreach($_system->frameworks as $the_framework){
	            if($the_framework['active'] != 0){
	                $param = array("is_framework" => true, "ver" => $the_framework['ver'], "initfile" => $the_framework['initfile']);
	                $_plugins->loadPluginScripts($the_framework['incl'], $param);
	            }
	        }
	    }

	    // Initiate included p1ugin header processes
	    if(count($_system->pluginsincl) > 0){
	        foreach($_system->pluginsincl as $the_plugin){
	            if(!empty($the_plugin['headerfunc']) && $the_plugin['is_editor'] == false && !in_array($the_plugin['incl'], $loadedplugins)){
	            	$param = array("folder" => $the_plugin['folder'], "initfile" => $the_plugin['initfile'], "headerfunc" => $the_plugin['headerfunc']);
	                $_plugins->loadPluginScripts($the_plugin['incl'], $param);
	            }
	        }
	    }

	    // Output head script/style lines
	    $_plugins->showScriptSourceLines(true);

	    // JQuery block
	    if(IN_ADMIN){
	        list($jq_x) = $_events->executeTrigger(TF_ADMIN_HEAD_JQUERY);
	        if(!empty($jq_x)) $jq_lines[] = $jq_x;
	    }else{
	        list($jq_x) = $_events->executeTrigger(TF_WEB_HEAD_JQUERY);
	        if(!empty($jq_x)) $jq_lines[] = $jq_x;
	    }

	    if(count($jq_lines) > 0){
	        echo "
	<script type=\"text/javascript\">
	    jQuery(document).ready(function($) {
	";
	        foreach ($jq_lines as $line) if(isStringable($line)) echo "\t\t".$line."\n";
	        echo "\t});\n</script>\n";
	    }
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Add theme script/style lines as a group.
	 * @param boolean $in_head
	 * @version 3.0
	 */
	function loadThemeScripts($type = "admin", $in_head = true) {
	    global $_system, $_filesys, $_plugins, $_events;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	    $scripts = array();
	    $styles = array();
	    $group = array();
	    if($type == "admin"){
	    	$basefolder = ADMIN_FOLDER.THEME_FOLDER;
	    }else{
	    	$basefolder = THEME_FOLDER;
	    }

		foreach($_system->themepaths[$type] as $level => $pathobj){
			foreach ($pathobj as $i => $path) {
				$folder = $_filesys->getLowestChildFolder($path);
				$f = $_filesys->addSlash($folder);
				if($f == '.sys/' && $type == 'admin'){
					$styles[] = array("folder" => $basefolder.$f, "file" => 'css/master.css');
				}else{
					$scripts[] = array("folder" => $basefolder.$f, "file" => $this->getThemeInfo($type, $folder, 'scriptfiles'));
					$styles[] = array("folder" => $basefolder.$f, "file" => $this->getThemeInfo($type, $folder, 'stylefiles'));
				}
			}
		}

		if(!empty($styles)){
			foreach($styles as $style){
				if(!empty($style['file']))
		        	$group[] = array("asType" => "style", "dir" => WEB_URL.$style['folder'], "file" => $style['file'], "media" => "screen", "in_head" => true, "cacheskip" => false);
			}
		}
		if(!empty($scripts)){
			foreach($scripts as $script){
				if(!empty($script['file']))
			        $group[] = array("asType" => "script", "dir" => WEB_URL.$script['folder'], "file" => $script['file'], "media" => "", "in_head" => true, "cacheskip" => false);
			}
		}
	    $err = $_plugins->addScriptSourceBlock($group, "Theme", $in_head);
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Call the headerfunc function of all active themes in the zone
	 * @param string $type 						Either 'admin' or 'website'
	 **/
	function callThemeHeaderFunc($type = 'admin'){
		global $_system, $_filesys, $_events;

		// process all theme paths at all levels
		foreach($_system->themepaths[$type] as $level => $pathobj){
			foreach ($pathobj as $i => $path) {
				$folder = $_filesys->getLowestChildFolder($path);
				$headerfunc = $this->getThemeInfo('admin', $folder, 'headerfunc');
				if(!empty($headerfunc) && function_exists($headerfunc)){
					call_user_func($headerfunc);
				}
			}
		}
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	}

	/**
	 * Delete a theme by id
	 * @param integer $id
	 * @return boolean
	 */
	function deleteTheme($id){
		global $_db_control, $_events, $_filesys;

		$id = intval($id);
		list($abort) = $_events->processTriggerEvent(__FUNCTION__, $id, false);				// alert triggered functions when function executes
		if($abort) return false;

		$ok = false;
		if($id > 0){
			$theme = $_db_control->setTable(THEMES_TABLE)->setWhere("id = '".$id."'")->getRec();
			if(isBlank($theme[0]['folder']) && !isBlank($theme[0]['folder'])){
				if($_db_control->setTable(THEMES_TABLE)->setWhere("id = '".$id."'")->deleteRec()){
					$folder = $theme[0]['folder'];
					$path = (($theme[0]['type'] == 'admin') ? SITE_PATH.ADMIN_FOLDER.THEME_FOLDER.$folder : SITE_PATH.THEME_FOLDER.$folder);
					$ok = $_filesys->deleteFolderTree($path);
				}
			}
		}
		return $ok;
	}

	/**
	 * Return the contents of the theme header file
	 */
	function showHeaderFile(){
	    $file = $this->getValidAdminThemeFile("header.php");
	    if($file !== false) include (SITE_PATH.$file);
	}

	/**
	 * Return the contents of the theme footer file
	 */
	function showFooterFile(){
	    $file = $this->getValidAdminThemeFile("footer.php");
	    if($file !== false) include (SITE_PATH.$file);
	}

	/**
	 * Set theme to inactive or active
	 * @param integer $id 						Theme id
	 * @param boolean $val 						True to activate
	 * @internal Core function
	 * @return mixed
	 */
	function setThemeActiveState($id, $val = null){
		global $_db_control, $_events;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
		$state = null;
		$ok = false;
		if($id > 0){
			if(is_null($val)) {
				$state = $_db_control->setTable(THEMES_TABLE)->setFields(array("active"))->setWhere("id = '{$id}'")->setLimit()->getRecItem();
				$state = !((bool) $state);
			}else{
				$state = (bool) $val;
			}
			$state = abs($state);
			$ok = ($_db_control->setTable(THEMES_TABLE)->setFieldvals(array("active" => $state))->setWhere("id = '{$id}'")->updateRec());
		}
		return array("state" => $state, "success" => $ok);
	}

	/**
	 * Output the HTML for the settings area themes
	 * @internal Core function
	 * @version 4.0
	 */
	function showSettingsThemesList($type){
		global $_system, $_debug;

		if($type != "admin" && $type != "website") return null;

		$activetitle = false;
		$othertitles = false;

		if(!isset($_system->themes) || !isset($_system->themes[$type])) $this->getInstalledThemes($type);

		foreach($_system->themes[$type] as $state => $themesarry){
			echo '<h3 class="header theme_'.$state.'">'.ucwords($state).' Themes</h3>'.PHP_EOL;
			if(!empty($themesarry)){
				foreach($themesarry as $theme){
					echo $this->getSettingsThemeRow($theme, $type, $state);
				}
			}else{
				?>
				<p>
					No <?php echo $state.' '.$type ?> themes are installed.<?php if($type == 'admin' && $state == 'active') echo '  The core admin theme, <strong>.sys</strong>, is in effect.' ?>
				</p>
				<?php
			}
		}
	}

	/**
	 * Return HTML for a single theme row
	 * @param string $theme_data
	 * @param boolean $is_prob
	 * @internal Core function
	 */
	function getSettingsThemeRow($data, $type, $state){
		global $_users;

		$type = strtolower($type);
		if($type != "admin" && $type != "website") return null;

		$link = '';
		$folder = prependSlash((($type == 'admin') ? ADMIN_FOLDER.ADM_THEME_FOLDER.$data['folder'] : THEME_FOLDER.$data['folder']));
		$theme_primary = ((empty($data['parent'])) ? " theme_primary" : "");
		$theme_zone = " theme_".$data['type'];
		if($data['active'] == 1){
			if($data['settingsfunc'] != '') $link = '<a href="#" class="theme_settings_link'.$theme_primary.$theme_zone.'" rel="'.$data['settingsfunc'].'">Theme Settings</a>';
			if($_users->userIsAllowedTo('activate_'.$type.'_theme')){
				$link .= (($link != '') ? '&nbsp;|&nbsp;' : '').'<a href="#" class="theme_act'.$theme_primary.$theme_zone.'">Deactivate</a>';
			}
		}else{
			if($_users->userIsAllowedTo('activate_'.$type.'_theme')){
				$link .= (($link != '') ? '&nbsp;|&nbsp;' : '').'<a href="#" class="theme_act'.$theme_primary.$theme_zone.'">Activate</a>';
			}
			if($_users->userIsAllowedTo('delete_'.$type.'_theme')){
				$link .= (($link != '') ? '&nbsp;|&nbsp;' : '').'<a href="#" class="theme_del'.$theme_primary.$theme_zone.'">Delete</a>';
			}
		}

		ob_start();
		?>
			<div class="theme_row theme_state_<?php echo $state ?> settings_hover_row">
				<input type="hidden" name="theme_id[]" class="theme_id" value="<?php echo $data['id']?>"/>
				<input type="hidden" name="theme_name[]" class="theme_name" value="<?php echo $data['name']?>"/>
				<div class="theme_leftside">
					<?php
					$theme_logos = array($folder."images/themelogo.png", $folder."images/themelogo.jpg");
					$theme_logo_tag = '<div style="width: 120px; height: 50px;" class="theme_logo">- No Logo -</div>';
					foreach($theme_logos as $theme_logo){
						if(!file_exists(SITE_PATH.$theme_logo)) continue;
						$theme_logo_tag = '<img src="'.WEB_URL.THUMB_GEN_ALIAS.'?src='.$theme_logo.'&w=120" alt="" title="" class="theme_logo" /><br/>';
					}
					echo $theme_logo_tag;
					?>
					<span class="theme_actions action_items"><?php echo $link?></span>
				</div>
				<div class="theme_rightside">
					<strong class="theme_name"><?php echo $data['name'].(($data['ver'] != '') ? ' v. '.$data['ver'] : '')?></strong>
					<div class="theme_descr"><?php echo $data['descr'] ?></div>
					<a href="#" class="theme_more">More</a>
					<?php if($data['website'] != '') {?>&nbsp;|&nbsp;<a href="<?php echo $data['website']?>" class="theme_links" target="_blank">Visit theme website</a><?php } ?>
					<?php if(file_exists($data['folder'].'readme.txt')) {?>&nbsp;|&nbsp;<a href="#" class="theme_help_link" rel="<?php echo str_replace(SITE_PATH, "", $data['folder']).'readme.txt'?>">Documentation</a><?php } ?>
					<br/>
					<div class="theme_info"></div>
				</div>
			</div>
		<?php 	$rtn = ob_get_clean();
		return $rtn;
	}

	/**
	 * Return HTML for a single theme
	 * @param string $id
	 * @internal Core function
	 * @return string
	 */
	function getSettingsThemeData($id){
		global $_db_control;

		$rtn = false;
		if($id > 0){
			$dataarry = $_db_control->setTable(THEMES_TABLE)->setWhere("id='{$id}'")->setLimit()->getRec();
			$rtn = '';
			if(empty($val)){
				$keys = array(	'name' => 'Name', 'author' => 'Author', 'ver'=>'Version', 'folder' => 'Theme location',
								'sysver' => 'System version required', 'website' => 'Website', 'parent' => 'Parent theme',
								'settingsfunc' => 'Theme settings', 'headerfunc' => 'Theme header function');
				foreach($dataarry[0] as $key => $data){
					if(!empty($keys[$key])){
						$text = '';
						switch($key){
							case 'folder':
								if($dataarry[0]['type'] == 'admin'){
									$text = ADMIN_FOLDER.ADM_THEME_FOLDER.$data;
								}else{
									$text = THEME_FOLDER.$data;
								}
								break;
	                        case 'ver':
	                            $text = $data;
	                            break;
	                        case 'website':
	                            $text = '<a href="'.$data.'" target="_blank">'.$data.'</a>';
	                            break;
	                        case 'settingsfunc':
	                        case 'headerfunc':
	                            if($dataarry[0]['active'] == 1 && !empty($data)) $text = '<a href="#" class="theme_settings_link" rel="'.$data.'">Click to view settings</a>';
	                            break;
							default:
								$text = $data;
								break;
						}
						if(!empty($text)) $rtn .= ((!empty($rtn)) ? '<br/>' : '').'<strong>'.$keys[$key].':</strong>&nbsp;'.$text;
					}
				}
			}
		}
		return $rtn;
	}

	/**
	 * Try to find the requested file in the 1) theme folder, or if not present; 2) system folder
	 * @param string $file 			Requested file
	 * @return mixed 				The path to the requested file or false if not found
	 */
	function getValidAdminThemeFile($file, $fallbackfile = "index.php"){
		$path = false;
		if(!empty($file)){
	        $themefolder = $this->getThemePathUnder('admin', THEME_PATH_ROOT, 0);
	        $systemfolder = ADMIN_FOLDER.ADM_THEME_FOLDER.ADM_THEME;

	        if(file_exists(SITE_PATH.$themefolder.$file)){
	        	$path = $themefolder.$file;
	        }elseif(file_exists(SITE_PATH.$systemfolder.$file)){
	        	$path = $systemfolder.$file;
	        }elseif(!is_null($fallbackfile)){
		        if(file_exists(SITE_PATH.$themefolder.$fallbackfile)){
		        	$path = $themefolder.$fallbackfile;
		        }elseif(file_exists(SITE_PATH.$systemfolder.$fallbackfile)){
		        	$path = $systemfolder.$fallbackfile;
		        }
		    }
	    }
	    return $path;
	}

	/**
	 * Get the value of a theme's setting from either the themes table.  This assumes the active theme.
	 * @param string $type 			Theme type (admin or website)
	 * @param string $arrayelem 	An optional array element to retrieve
	 * @return mixed 				The value of the themes setting
	 */
	function getThemeCustomSetting($type, $arrayelem = null){
		global $_db_control;

	    $result = false;
	    $type = strtolower($type);
	    if($type == 'admin' || $type == 'website'){
	        // retrieve from themes table
	        $result = $_db_control->setTable(THEMES_TABLE)->setFields("custom_settings")->setWhere("`type` = '$type' AND `active` = 1")->getRecItem();
	        $json_result = json_decode($result, true);
	        if(!empty($arrayelem)){
	        	if(isset($json_result[$arrayelem])) $result = $json_result[$arrayelem];
	        }
	    }
	    return $result;
	}

	/**
	 * Save a theme's custom array, string, or object value to the themes table
	 * @param string $type      	Theme type (admin or website)
	 * @param mixed $settings 		The value to save
	 * @return boolean
	 */
	function saveThemeCustomSettings($type, $settings){
		global $_db_control, $_events;

	    $result = false;
	    $type = strtolower($type);
	    if($type == 'admin' || $type == 'website'){
	        $setval = json_encode($settings);

		    // save to themes table
		    $result = $_db_control->setTable(THEMES_TABLE)->setFieldvals(array("custom_settings" => $setval))->setWhere("`type` = '$type' AND `active` = 1")->updateRec();
	    }
		$_events->processTriggerEvent(__FUNCTION__, $result);				// alert triggered functions when function executes
	    return $result;
	}

	/**
	 * Update a theme's custom array, string, or object value with an element value
	 * @param string $type      	Theme type (admin or website)
	 * @param mixed $setting 		The value to save
	 * @param string $arrayelem 	The element that will be updated
	 * @return boolean
	 */
	function updateThemeCustomSettings($type, $setting, $arrayelem){
		global $_db_control, $_events;

	    $result = false;
	    $type = strtolower($type);
	    if(($type == 'admin' || $type == 'website') && $arrayelem != ''){
	    	$curval = $_db_control->setTable(THEMES_TABLE)->setFields("custom_settings")->setWhere("`type` = '$type' AND `active` = 1")->getRecItem();
	        $json_val = json_decode($curval, true);
	        $json_val[$arrayelem] = $setting;
	        $setval = json_encode($json_val);

		    // save to themes table
		    $result = $_db_control->setTable(THEMES_TABLE)->setFieldvals(array("custom_settings" => $setval))->setWhere("`type` = '$type' AND `active` = 1")->updateRec();
	    }
		$_events->processTriggerEvent(__FUNCTION__, $result);				// alert triggered functions when function executes
	    return $result;
	}

	/**
	 * Delete an element value from the theme's custom settings
	 * @param string $type      	Theme type (admin or website)
	 * @param string $arrayelem 	The element that will be deleted
	 * @return boolean
	 */
	function deleteThemeCustomSettings($type, $arrayelem){
		global $_db_control, $_events;

	    $result = false;
	    $type = strtolower($type);
	    if(($type == 'admin' || $type == 'website') && $arrayelem != ''){
	    	$curval = $_db_control->setTable(THEMES_TABLE)->setFields("custom_settings")->setWhere("`type` = '$type' AND `active` = 1")->getRecItem();
	        $json_val = json_decode($curval, true);
	        if(isset($json_val[$arrayelem])) unset($json_val[$arrayelem]);
	        $setval = json_encode($json_val);

		    // save to themes table
		    $result = $_db_control->setTable(THEMES_TABLE)->setFieldvals(array("custom_settings" => $setval))->setWhere("`type` = '$type' AND `active` = 1")->updateRec();
	    }
		$_events->processTriggerEvent(__FUNCTION__, $result);				// alert triggered functions when function executes
	    return $result;
	}

	/**
	 * Return attribute of currently loaded theme
	 * @param string $attr
	 */
	function getAdminThemeAttr($attr = 'theme'){
	    $css_file = SITE_PATH.$this->getThemePathUnder("admin").ADM_CSS_FOLDER."admin.css";
	    if(file_exists($css_file)){
	        $fdata = file_get_contents($css_file, false, null, -1, 512);
	        $fdata = str_replace(" *  ", "", $fdata);
	        $flines = explode("\n", $fdata);
	        foreach($flines as $line){
	            $line = trim($line);
	            if(strpos($line, "@".$attr) !== false){
	                $chunk = trim(substr($line, strlen($attr)+1));
	                return $chunk;
	            }
	        }
	    }
	}

	/**
	 * Output theme settings dialog contents as JSON data
	 * @param string $contents
	 * @param string $func Calling function
	 * @version 4.0
	 */
	function themeSettingsDialogContents($title, $contents, $func){
		return json_encode(array("title"=>$title, "contents"=>$contents, "func"=>$func));
	}

	/**
	 * Pass theme settings dialog button pressed response back through AJAX
	 * @param string $message
	 * @param boolean $closedialog
	 * @version 4.0
	 */
	function themeSettingsDialogButtonPressed($message = '', $closedialog = true){
		return json_encode(array("success"=>true, "message"=>$message, "closedialog"=>$closedialog));
	}

	/**
	 * Show the theme icon
	 * @param string $type 				Type of theme (admin, front)
	 * @param string $icon 				Icon filename
	 * @return string $html
	 */
	function showIcon($type, $icon, $atts_arry = array()){
		$html = '';
		$file = $this->getThemePathUnder($type).IMG_UPLOAD_FOLDER.'icons/'.$icon.'.png';
		if(file_exists(SITE_PATH.$file)){
			$atts = '';
			if(is_array($atts_arry)){
				foreach($atts_arry as $param => $val){
					$atts .= (($atts != '') ? ' ' : '').$param.'="'.$val.'"';
				}
			}
			$html = '<img src="'.WEB_URL.$file.'" alt="'.$icon.'" '.$atts.'/>';
		}
		return $html;
	}

	function checkCaller($action){
		$stack = debug_backtrace();
		$callerfile = $stack[1]['file'];
		if(strpos($callerfile, "_core") === false){
			die("Calling Media::$action in $callerfile not allowed!");
		}
	}
}
?>