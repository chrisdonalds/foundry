<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// ---------------------------
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

/* DATABASE CONTROLLER CLASS */

if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");
define ("DBLOADED", true);

if(!defined('DB_ARRAY')) define ("DB_ARRAY", 1);
if(!defined('DB_OBJECT')) define ("DB_OBJECT", 2);

require_once(SITE_PATH.ADMIN_FOLDER.LIB_FOLDER.'db_'.DB_ENGINE.'/db_wrapper.class.php');

class DB_controller extends DB_wrapper {
	private $table = null;
	private $fields = "*";
	private $field_obj = array();
	private $fieldvals = null;
	private $crit = null;
	private $order = null;
	private $group = null;
	private $having = null;
	private $joins = null;
    private $limit = null;
	private $lock_requested = false;
	private $data_obj = array();
	private $data = null;
	private $attr_obj = array();
	private $atts = array();
	private $data_id = 0;
	private $storage = array();
    private $save = false;
    private $_debug = null;

	const CRIT = 1;
	const ORDER = 2;
	const LIMIT = 4;
	const GROUP = 8;

	public function __construct($db_host=null, $db_database=null, $db_username=null, $db_password=null, $db_port=null) {
        global $_events;

        $this->_debug = new DebugClass();
        if(empty($db_host) && defined(DBHOST)) $db_host = DBHOST;
        if(empty($db_database) && defined(DBNAME)) $db_database = DBNAME;
        if(empty($db_username) && defined(DBUSER)) $db_username = DBUSER;
        if(empty($db_password) && defined(DBPASS)) $db_host = DBPASS;
        if(empty($db_port) && defined(DBPORT)) $db_host = DBPORT;
		if(empty($db_host) || empty($db_database) || empty($db_username)) die('DB_control requires host, database and username parameters to initiate.');
        $this->db = $this->getConnection($db_username, $db_password, $db_database, $db_host, $db_port);
		$this->setCallingClass($this);
        $_events->processTriggerEvent('init_db');                // alert triggered functions when function executes
    }

	// ----------- DATABASE PREPARATORY FUNCTIONS ---------------

    public function list_functions(){
    	$f = get_class_methods($this);
    	unset($f[array_search('__construct', $f)]);
    	unset($f[array_search('list_functions', $f)]);
    	return $f;
    }

    public function setDebug(){
        $this->_debug->toggle_debug(true);
        return $this;
    }

    /**
     * Setter: Table(s)
     * @param mixed $table 				One or more tables (comma-separated or array)
     */
    public function setTable($table){
    	if(is_array($table)){
    		$this->table = join(",", $table);
    	}else{
    		$this->table = trim($table);
    	}
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->table);
    	return $this;
    }

    /**
     * Add a table alias to the PHP constants
     * @param string $table
     * @param string $alias
     */
    public function setTableAlias($table, $alias = null){
        $table = trim($table, "_");
        $alias = trim($alias, "_");
        if(is_string($table) && !empty($table)){
            if(empty($alias)) $alias = strtoupper($table)."_TABLE";
            if(substr($table, 0, strlen(DB_TABLE_PREFIX)) == DB_TABLE_PREFIX && !defined($alias)){
                define($alias, $table, true);
            }
        }
    }

    /**
     * Return the table name referenced by its alias
     * @param string $alias
     * @return string null/tablename
     */
    public function getTableFromAlias($alias){
        $alias = trim($alias, "_");
        $table = null;
        if(is_string($alias) && !empty($alias)){
            $const = get_defined_constants();
            if(in_array($alias, $const)) $table = $const[$alias];
        }
        return $table;
    }

    /**
     * Setter: Field(s)
     * @param mixed $fields 			One or more fields (comma-separated or array). If blank, "*" will be assumed
     */
    public function setFields($fields = "*"){
    	if(is_array($fields)){
    		$this->fields = join(",", $fields);
    		$this->field_obj = $fields;
    	}else{
    		$this->fields = trim($fields);
    		$this->field_obj = explode(",", $fields);
    	}
    	if(empty($this->fields)) $this->fields = "*";
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->fields);
    	return $this;
    }

    /**
     * Setter: Fieldval(s)
     * @param array $fieldvals 			One or more field-value pairs (field is the key of the pair)
     */
    public function setFieldvals($fieldvals = array()){
    	if(is_array($fieldvals)){
    		$fieldvalset = array();
    		foreach($fieldvals as $field => $value){
    			$fieldvalset[] = "`".$field."` = '".sanitizeText($value, false)."'";
    		}
    		$this->fieldvals = join(",", $fieldvalset);
    		$this->field_obj = $fieldvals;
	        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->fieldvals);
    	}else{
			_e(MISSING_ARG.__FUNCTION__, true);
    	}
    	return $this;
    }

    /**
     * Setter: Where criteria
     * @param string $crit
     */
    public function setWhere($crit = ""){
    	$this->crit = trim($crit);
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->crit);
    	return $this;
    }

    /**
     * Setter: Joins(s)
     * @param array $joins 				One or more array joins (element => array(join type, join table, column relations))
     */
    public function setJoins($joins = ""){
    	if(is_array($joins)){
    		$join = array();
			$mysql_joins = array("JOIN", "INNER JOIN", "LEFT JOIN", "RIGHT JOIN", "LEFT OUTER JOIN", "RIGHT OUTER JOIN");
    		foreach($joins as $joinparts){
    			if(is_array($joinparts) && count($joinparts) == 3){
    				$jointype = getIfSet($joinparts['type']);
    				$jointbl = getIfSet($joinparts['table']);
    				$joincomp = getIfSet($joinparts['comp']);
    				if(in_array($jointype, $mysql_joins)){
    					$join[] = strtoupper(trim($jointype))." ".trim($jointbl)." ON ".trim($joincomp);
    				}
    			}
    		}
    		$this->joins = join(" ", $join);
    	}else{
    		$this->joins = trim($joins);
    	}
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->joins);
    	return $this;
    }

    /**
     * Setter: Order
     * @param string $order 			An order clause (without the ORDER BY)
     */
    public function setOrder($order = ""){
    	$this->order = trim($order);
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->order);
    	return $this;
    }

    /**
     * Setter: Group
     * @param string $group 			A group by clause (without the GROUP BY)
     */
    public function setGroup($group = ""){
    	$this->group = trim($group);
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->group);
    	return $this;
    }

    /**
     * Setter: Having
     * @param string $having 			A having criteria (without the HAVING)
     */
    public function setHaving($having = ""){
    	$this->having = trim($having);
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->having);
    	return $this;
    }

    /**
     * Setter: Limit
     * @param string $limit 			A limit clause (without the LIMIT).  If blank, "1" is assumed
     */
    public function setLimit($limit = "1"){
    	$this->limit = trim($limit);
    	if(empty($this->limit)) $this->limit = "1";
    	if($this->limit == "all") $this->limit = "";
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->limit);
    	return $this;
    }

    /**
     * Setter: Data set
     * @param array $data_obj 			An array containing field-value pairs
     */
    public function setData($data_obj){
		if(is_array($data_obj)){
			$data = "";
			foreach($data_obj as $field => $val) $data .= (($data != "") ? ", " : "")."`".trim($field)."` = '".$val."'";
			$this->data = $data;
			$this->data_obj = $data_obj;
	        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->data_obj);
	    }else{
			_e(MISSING_ARG.__FUNCTION__, true);
		}
    	return $this;
    }

    /**
     * Setter: Attributes Data set
     * @param array $attr_obj			An array containing field-value pairs
     */
    public function setAttributeData($attr_obj){
		if(is_array($attr_obj)){
			$data = "";
			foreach($attr_obj as $field => $val) $data .= (($data != "") ? ", " : "")."`".trim($field)."` = '".$val."'";
		}else{
			$data = $attr_obj;
		}
		$this->attr_obj = $data;
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->attr_obj);
    	return $this;
    }

	/**
	 * Setter: Attributes array values from parameters (allows merging up to two attributes arrays)
	 * @param array $atts1
	 * @param array $atts2
	 */
	public function setAttributes($atts1, $atts2 = array()){
        global $_events;

		if(is_array($atts1)){
			$atts = $atts1;
			if(is_array($atts2)) $atts = $atts + $atts2;
            $aliasdefault = getIfSet($atts['aliasdefault']);
            if(empty($aliasdefault)) {
                if(isset($atts['alias']))
                    $aliasdefault = $atts['alias'];
            }
            if(!empty($aliasdefault)){
                $this->atts = array();
                if(isset($atts['viewable_from'])) $this->atts['viewable_from'] = date(PHP_DATE_FORMAT." ".PHP_TIME_FORMAT, strtotime($atts['viewable_from']));
                if(isset($atts['viewable_to'])) $this->atts['viewable_to'] = date(PHP_DATE_FORMAT." ".PHP_TIME_FORMAT, strtotime($atts['viewable_to']));
    			if(isset($atts['date_created'])) $this->atts['date_created'] = date(PHP_DATE_FORMAT." ".PHP_TIME_FORMAT, strtotime($atts['date_created']));
                if(isset($atts['date_published'])) $this->atts['date_published'] = date(PHP_DATE_FORMAT." ".PHP_TIME_FORMAT, strtotime($atts['date_published']));
    			if(isset($atts['date_updated'])) $this->atts['date_updated'] = date(PHP_DATE_FORMAT." ".PHP_TIME_FORMAT, strtotime($atts['date_updated']));
                if(isset($atts['alias'])) $this->atts['alias'] = (empty($atts['alias']) ? $aliasdefault : $atts['alias']);
                if(isset($atts['path'])) $this->atts['path'] = $atts['path'];
    			if(isset($atts['metatitle'])) $this->atts['metatitle'] = $atts['metatitle'];
    			if(isset($atts['metadescr'])) $this->atts['metadescr'] = $atts['metadescr'];
    			if(isset($atts['metakeywords'])) $this->atts['metakeywords'] = $atts['metakeywords'];
                if(isset($atts['searchable'])) $this->atts['searchable'] = intval((bool)$atts['searchable']);
    			if(isset($atts['displayed'])) $this->atts['displayed'] = intval((bool)$atts['displayed']);
                if(isset($atts['language'])) $this->atts['language'] = $atts['language'];
    			if(isset($atts['sitemap_state'])) $this->atts['sitemap_state'] = $atts['sitemap_state'];
                if(isset($atts['homepage'])) $this->atts['homepage'] = intval((bool)$atts['homepage']);
    			if(isset($atts['protected'])) $this->atts['protected'] = intval((bool)$atts['protected']);
    			if(isset($atts['locked'])) $this->atts['locked'] = intval((bool)$atts['locked']);
    			if(isset($atts['published'])) $this->atts['published'] = intval((bool)$atts['published']);
    			if(isset($atts['draft'])) $this->atts['draft'] = intval((bool)$atts['draft']);
                if(isset($atts['deleted'])) $this->atts['deleted'] = $atts['deleted'];
    			if(isset($atts['password'])) $this->atts['password'] = $atts['password'];
    			if(isset($atts['user_editing'])) $this->atts['user_editing'] = intval($atts['user_editing']);
                if(isset($atts['user_id'])) $this->atts['user_id'] = intval($atts['user_id']);
                if(isset($atts['parent_id'])) $this->atts['parent_id'] = intval($atts['parent_id']);
    			if(isset($atts['plugins_incl'])) $this->atts['plugins_incl'] = $atts['plugins_incl'];

                $this->atts[SAVEBUTTONPRESSED] = (isset($atts[SAVEBUTTONPRESSED])) ? $atts[SAVEBUTTONPRESSED] : null;
            }
		}
        _e(__CLASS__.'::'.__FUNCTION__.' => '.print_r($this->atts, true));
    	return $this;
	}

    /**
     * Getter: Returns merged Data Attributes and Database Query properties.  DOES NOT alter the Attributes array.
     * @return array
     */
    public function getAttributesSuperset(){
        $atts = $this->getProperties() + $this->atts;
        return $atts;
    }

    /**
     * Getter: Returns Database Query properties as array.
     * @return array
     */
    public function getProperties(){
        $atts = array(
            "table" => $this->table,
            "fields" => $this->fields,
            "field_obj" => $this->field_obj,
            "fieldvals" => $this->fieldvals,
            "crit" => $this->crit,
            "order" => $this->order,
            "group" => $this->group,
            "having" => $this->having,
            "joins" => $this->joins,
            "limit" => $this->limit
        );
        return $atts;
    }

    /**
     * Setter: Attribute-Data ID
     * @param string $id
     */
    public function setDataID($id){
    	$this->data_id = intval($id);
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->data_id);
    	return $this;
    }

    /**
     * Setter: Data lock
     * @param boolean $locked
     */
    public function requestLock($locked = true){
        $this->lock_requested = $locked;
        _e(__CLASS__.'::'.__FUNCTION__.' => '.$this->lock_requested);
        return $this;
    }

    /**
     * Setter: Clear values
     */
    public function clear(){
    	$this->table = null;
    	$this->crit = null;
    	$this->fields = "*";
    	$this->field_obj = array();
    	$this->fieldvals = null;
    	$this->joins = null;
    	$this->order = null;
    	$this->group = null;
    	$this->having = null;
    	$this->limit = null;
    	$this->atts = array();
    	$this->data_obj = array();
    	$this->data = null;
        $this->_debug->toggle_debug(false);

    	return $this;
    }

    /**
     * Activate storage of the current db query object before final operation is executed.
     */
    public function saveQuery(){
        $this->save = true;
        return $this;
    }

    /**
     * Save query object properties
     */
    public function store($method = null, $endsaving = false){
    	$this->storage = array(
	    	"table" => $this->table,
	    	"crit" => $this->crit,
	    	"fields" => $this->fields,
	    	"field_obj" => $this->field_obj,
	    	"fieldvals" => $this->fieldvals,
	    	"joins" => $this->joins,
	    	"order" => $this->order,
	    	"group" => $this->group,
	    	"having" => $this->having,
	    	"limit" => $this->limit,
	    	"atts" => $this->atts,
	    	"data_obj" => $this->data_obj,
	    	"data" => $this->data,
            "operation" => $method
	    );
        if($endsaving) $this->save = false;
    }

    /**
     * Retrieve query object properties from storage
     */
    public function recall(){
        $this->table = $this->storage['table'];
        $this->crit = $this->storage['crit'];
        $this->fields = $this->storage['fields'];
        $this->field_obj = $this->storage['field_obj'];
        $this->fieldvals = $this->storage['fieldvals'];
        $this->joins = $this->storage['joins'];
        $this->order = $this->storage['order'];
        $this->group = $this->storage['group'];
        $this->having = $this->storage['having'];
        $this->limit = $this->storage['limit'];
        $this->atts = $this->storage['atts'];
        $this->data_obj = $this->storage['data_obj'];
        $this->data = $this->storage['data'];
    }

    /**
     * Retrieve value of storage
     * @return array
     */
    public function getStorage(){
        return $this->storage;
    }

    public function redo($store = array()){
        $this->storage = $store;
        $this->recall();
        $retn = array();
        switch($store['operation']){
            case 'getTree':
                $retn = $this->getTree();
                break;
            case 'getDataTree':
                $retn = $this->getDataTree();
                break;
        }
        return $retn;
    }

    /**
     * Prepare the where, group by, order by and limit part of SQL statement from values
     */
    private function prepSQL($ignore = false){
    	$sql = "";
		if(!empty($this->crit) && ($ignore & self::CRIT) == false) $sql .= " WHERE {$this->crit}";
		if(!empty($this->group) && ($ignore & self::GROUP) == false) $sql .= " GROUP BY {$this->groupby}";
		if(!empty($this->order) && ($ignore & self::ORDER) == false) $sql .= " ORDER BY {$this->order}";
		if(!empty($this->limit) && ($ignore & self::LIMIT) == false) $sql .= " LIMIT {$this->limit}";
		return $sql;
    }

	// ----------- UNIVERSAL DATABASE FUNCTIONS ---------------

    /**
     * Universal wrapper for the db::executeQuery method
     * @param string $sql
     * @return array
     */
    function univExecuteQuery($sql){
        _e("EXECUTE QUERY: ".$sql."<br/>");
        return $this->executeQuery($sql);
    }

	/**
	 * Universal wrapper for the db::getQuery method
	 * @param string $sql
	 * @return array
	 */
	function univGetQuery($sql){
		_e("SELECT QUERY: ".$sql."<br/>");
		return $this->getQuery($sql);
	}

	/**
	 * Universal wrapper for the db::updateQuery method
	 * @param string $sql
	 * @return boolean
	 */
	function univUpdateQuery($sql){
		_e("UPDATE QUERY: ".$sql."<br/>");
		return $this->updateQuery($sql);
	}

	/**
	 * Universal wrapper for the db::insertQuery method
	 * @param string $sql
	 * @return int 					Index of inserted record
	 */
	function univInsertQuery($sql){
		_e("INSERT QUERY: ".$sql."<br/>");
		return $this->insertQuery($sql);
	}

	/**
	 * Universal wrapper for the db::deleteQuery method
	 * @param string $sql
	 * @return boolean
	 */
	function univDeleteQuery($sql){
		_e("DELETE QUERY: ".$sql."<br/>");
		return $this->deleteQuery($sql);
	}

	/**
	 * Universal wrapper for the db::getNumRows method
	 * @param string $sql
	 * @return int
	 */
	function univGetNumRows($sql){
		_e("NUMROWS QUERY: ".$sql."<br/>");
		return $this->getNumRows($sql);
	}

	/**
	 * Universal wrapper for the db::getTotalRows method
	 * @param string $sql
	 * @return int
	 */
	function univGetTotalRows($sql){
		_e("TOTALS QUERY: ".$sql."<br/>");
		return $this->getTotalRows($sql);
	}

	/**
	 * Universal wrapper for the db::getAffectedRows method
	 * @return int
	 */
	function univGetAffectedRows(){
		_e("AFFECTED ROWS QUERY: ".$sql."<br/>");
		return $this->getTotalAffectedRows();
	}

    /**
     * Universal wrapper for the db::dbError method
     * @return mixed            Error
     */
	function univGetError(){
		_e("DB ERRORS QUERY: ".$sql."<br/>");
		return $this->dbError();
	}

    /**
     * Universally set the return object type
     * @param mixed             DB_ARRAY or DB_OBJECT
     */
	function univSetRecResultType($type = DB_ARRAY){
		if(in_array($type, array(DB_ARRAY, DB_OBJECT)))
			$this->result_type = $type;
		else
			die('DB Result type cannot be set to "'.$type.'"');
	}

    /**
     * Return the universal return object type
     * @return string
     */
	function univGetRecResultType(){
		switch($this->result_type){
			case DB_ARRAY:
				return "Array";
				break;
			case DB_OBJECT:
				return "Object";
				break;
		}
	}

    // ----------- DATABASE SAFE QUERYING FUNCTIONS ---------

    /**
     * Execute a database-safe query
     * @param string $sql               A sprintf-like query (i.e. SELECT * FROM %s WHERE %s = %d)
     * @param array $args               A parameter array with each element corresponding to placeholders in query
     * @return array|null
     */
    function dbExecuteQuery($sql, $args){
        $r = null;
        if(is_string($sql) && is_array($args) && count($args) > 0){
            $s = vsprintf($sql, $args);
            $s = $this->cleanQuery($s);
            $r = $this->univExecuteQuery($s);
        }
        return $r;
    }

    /**
     * Return recordset array using database-safe functions
     * @param string $sql               A sprintf-like query (i.e. SELECT * FROM %s WHERE %s = %d)
     * @param array $args               A parameter array with each element corresponding to placeholders in query
     * @return array|null
     */
    function dbGetQuery($sql, $args){
        $r = null;
        if(is_string($sql) && is_array($args) && count($args) > 0){
            $s = vsprintf($sql, $args);
            $s = $this->cleanQuery($s);
            $r = $this->univGetQuery($s);
        }
        return $r;
    }

    /**
     * Insert data into database using database-safe functions
     * @param string $sql               A sprintf-like query (i.e. SELECT * FROM %s WHERE %s = %d)
     * @param array $args               A parameter array with each element corresponding to placeholders in query
     * @return integer|false
     */
    function dbInsertQuery($sql, $args){
        $r = false;
        if(is_string($sql) && is_array($args) && count($args) > 0){
            $s = vsprintf($sql, $args);
            $s = $this->cleanQuery($s);
            $r = $this->univInsertQuery($s);
        }
        return $r;
    }

    /**
     * Update data using database-safe functions
     * @param string $sql               A sprintf-like query (i.e. SELECT * FROM %s WHERE %s = %d)
     * @param array $args               A parameter array with each element corresponding to placeholders in query
     * @return boolean
     */
    function dbUpdateQuery($sql, $args){
        $r = false;
        if(is_string($sql) && is_array($args) && count($args) > 0){
            $s = vsprintf($sql, $args);
            $s = $this->cleanQuery($s);
            $r = $this->univUpdateQuery($s);
        }
        return $r;
    }

    /**
     * Delete recordset using database-safe functions
     * @param string $sql               A sprintf-like query (i.e. SELECT * FROM %s WHERE %s = %d)
     * @param array $args               A parameter array with each element corresponding to placeholders in query
     * @return boolean
     */
    function dbDeleteQuery($sql, $args){
        $r = false;
        if(is_string($sql) && is_array($args) && count($args) > 0){
            $s = vsprintf($sql, $args);
            $s = $this->cleanQuery($s);
            $r = $this->univDeleteQuery($s);
        }
        return $r;
    }

	// ----------- DATABASE WRAPPER FUNCTIONS ---------------

	/**
	 * Return recordset array from SELECT query
	 * @param boolean $flatten
	 * @return array
	 */
	function getRec($flatten = false, $keepalive = false) {
        global $_events;

		if(!empty($this->table)) {
			$sql = "SELECT {$this->fields} FROM {$this->table}".$this->prepSQL();
			if($this->save) $this->store(__FUNCTION__, true);
			$rec = $this->univGetQuery($sql);
			if(!$keepalive) $this->clear();
			if($flatten && count($rec) == 1){
	            // reduce multidimension array with a single element to a single dimension array
	            $rec = $rec[0];
	        }
            if($this->result_type == DB_OBJECT) $rec = json_decode(json_encode($rec));

            $atts = $this->getProperties();
            $_events->processTriggerEvent('getrec', $atts);
            return $rec;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Return recordset array and attributes from data table
	 * @param boolean $flatten
	 * @return array
	 */
	function getData($flatten = false, $request_lock = false, $keepalive = false) {
        global $_events;

		if(!empty($this->table) && !empty($this->fields)) {
			if(strpos($this->table, " ") !== false){
				$tbl = explode(" ", $this->table);
				$alias = $tbl[1];
			}else{
                $tbl = $alias = $this->table;
            }

			$sql = "SELECT `".$alias."`.{$this->fields}, `".ATTRIBUTES_TABLE."`.* FROM `{$tbl}` LEFT JOIN `".ATTRIBUTES_TABLE."` ON `$alias`.id = `".ATTRIBUTES_TABLE."`.data_id";
            if(!empty($this->joins)) $sql .= " ".$this->joins;
			$this->crit = "(`".ATTRIBUTES_TABLE."`.data_type = '{$this->table}' OR `".ATTRIBUTES_TABLE."`.data_type IS NULL)".((!empty($this->crit)) ? ' AND '.$this->crit : '');
			$sql.= $this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
			$rec = $this->univGetQuery($sql);
            list($in_use, $rec) = $this->checkRecLocked($rec);

            if(!$in_use && $request_lock) $rec = $this->lockRec($rec);
			if(!$keepalive) $this->clear();
			if($flatten && isset($rec[0])){
	            // reduce multidimension array with a single element to a single dimension array
                $temp = $rec[0];
                $rec = $temp;
	        }
            if($this->result_type == DB_OBJECT) $rec = json_decode(json_encode($rec));

            $atts = $this->getAttributesSuperset();
            $_events->processTriggerEvent('getdata', $atts);
            return $rec;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Return recordset array from SELECT JOIN query of two or more tables
	 * @return array
	 */
	function getRecJoin() {
		//select fields from table1 join table2 on field1 = field2
		if(!empty($this->table) && !empty($this->joins)) {
			$sql = "SELECT {$this->fields} FROM {$this->table} {$this->joins}".$this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
			$rec = $this->univGetQuery($sql);
            if($this->result_type == DB_OBJECT) $rec = json_decode(json_encode($rec));
			$this->clear();
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
		return $rec;
	}

	/**
	 * Return recordset array from SELECT JOIN query of 2 tables and attributes
	 * @return array
	 */
	function getDataJoin() {
		//select fields from table1 join table2 on field1 = field2
		if(!empty($this->table) && !empty($this->fields) && !empty($this->joins)) {
			if(strpos($this->table, " ") !== false){
				$tbl = explode(" ", $this->table);
				$alias = $tbl[1];
			}else
				$alias = $this->table;
			$this->joins = "LEFT JOIN `attributes` ON {$alias}.id = `attributes`.data_id ".$this->joins;
			$sql = "SELECT {$this->fields}, `attributes`.* FROM {$this->table} {$this->joins}".$this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
			$rec = $this->univGetQuery($sql);
            if($this->result_type == DB_OBJECT) $rec = json_decode(json_encode($rec));
			$this->clear();
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
		return $rec;
	}

	/**
	 * Return recordset array from SELECT DISTINCT query
	 * @param boolean $flatten
	 * @return array
	 */
	function getRecDistinct($flatten = false) {
		if(!empty($this->table) && !empty($this->fields)) {
			$sql = "SELECT DISTINCT {$this->fields} FROM {$this->table}".$this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
			$rec = $this->univGetQuery($sql);
			$this->clear();
			if($flatten and count($rec) > 0){
	            // reduce multidimension array with a single element to a single dimension array
	            $temp = $rec[0];
	            $rec = $remp;
	        }
            if($this->result_type == DB_OBJECT) $rec = json_decode(json_encode($rec));
	        return $rec;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Return recordset array from SELECT UNION query
	 * @return array
	 */
	function getRecUnion() {
		$sql = "";
		$tables = explode(",", $this->table);
		if(count($tables) > 1) {
			$clause = $this->prepSQL(self::LIMIT);
			foreach($tables as $table) {
				// bind sqls
				if($sql != "") $sql .= " UNION ";
				// create a select statement for each tableset
				$table = trim($table);
				$sql .= "(SELECT {$this->fields} FROM {$table}".$clause.")";
			}
			if(!empty($this->limit)) $sql = "SELECT uni.* FROM (".$sql.") AS uni LIMIT {$this->limit}";
            if($this->save) $this->store(__FUNCTION__, true);
			$rec = $this->univGetQuery($sql);
            if($this->result_type == DB_OBJECT) $rec = json_decode(json_encode($rec));
			$this->clear();
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
		return $rec;
	}

	/**
	 * Return number of rows from SELECT query
	 * @return integer
	 */
	function getRecNumRows() {
		if(!empty($this->table)) {
			$sql = "SELECT {$this->fields} FROM {$this->table}".$this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
			$num = $this->univGetNumRows($sql);
			$this->clear();
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
		return $num;
	}

	/**
	 * Return number of rows from SELECT JOIN query
	 * @return integer
	 */
	function getRecJoinNumRows() {
		//select fields from table1 join table2 on field1 = field2
		if(!empty($this->table) && !empty($this->joins)) {
			$sql = "SELECT {$this->fields} FROM {$this->table} {$this->joins}".$this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
			$rec = $this->univGetNumRows($sql);
			$this->clear();
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
		return $num;
	}

	/**
	 * Return the total rows from SELECT query (better for large result sets)
	 * @return integer
	 */
	function getRecTotal() {
		if(!empty($this->table)) {
			$sql = "SELECT {$this->fields} FROM {$this->table}".$this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
			$num = $this->univGetTotalRows($sql);
			$this->clear();
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
		return $num;
	}

	/**
	 * Return the total affected rows from most recent insert or delete query
	 * @return integer
	 */
	function getRecTotalAffectedRows() {
		return $this->univGetAffectedRows();
	}

	/**
	 * Return field value of a record
	 * @return string
	 */
	function getRecItem() {
		if(!empty($this->table) && !empty($this->fields) && !empty($this->crit)) {
			if(strpos($this->fields, "(") === false){
				$this->fields = "`".$this->fields."`";
			}
			$item_rec = $this->setLimit()->getRec();
			if(isset($item_rec[0])){
				_e(current($item_rec[0]))."<br/>";
				return current($item_rec[0]);
			}else{
	            return null;
	        }
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Return array of field values from a recordset
	 * @return array
	 */
	function getRecItemList() {
		if(!empty($this->table) && !empty($this->fields) && !empty($this->crit)) {
			$field_array = explode(",", $this->fields);
			$item_rec = $this->setLimit()->getRec();
			$data_array = array();
			foreach($field_array as $field){
				$data_array[] = $item_rec[0][trim($field)];
			}
			_pr($data_array);
            if($this->result_type == DB_OBJECT) $data_array = json_decode(json_encode($data_array));
			return $data_array;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Return an array of arrays containing field data
	 * @return array
	 */
	function getRecFieldArray(){
		if(!empty($this->table) && !empty($this->fields)) {
			$rec = $this->getRec();
			$data_array = array();
			foreach($rec as $row){
				foreach($row as $field => $data){
					$data_array[$field][] = $data;
				}
			}
			_pr($data_array);
            if($this->result_type == DB_OBJECT) $data_array = json_decode(json_encode($data_array));
			return $data_array;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Return the lowest autoindex value
	 * @return integer
	 */
	function getFirstID() {
		if(!empty($this->table)) {
			$this->setOrder($this->fields." ASC");
			$this->setLimit();
			$sql = "SELECT * FROM {$this->table} ".$this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
		 	$rec = $this->univGetQuery($sql);
		 	$this->clear();

            $indx = false;
            if(!empty($rec)){
                $idfld = $this->getIDField($rec[0]);
                if(!empty($idfld))
                    $indx = intval($rec[0][$idfld]);
            }
            return $indx;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Return the last autoindex value
	 * @return integer
	 */
	function getLastID() {
		if(!empty($this->table)) {
			$this->setOrder($this->fields." DESC");
			$this->setLimit();
			$sql = "SELECT * FROM {$this->table} ".$this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
		 	$rec = $this->univGetQuery($sql);
		 	$this->clear();

            $indx = false;
            if(!empty($rec)){
                $idfld = $this->getIDField($rec[0]);
                if(!empty($idfld))
                    $indx = intval($rec[0][$idfld]);
            }
            return $indx;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Return the parent id of the current record
	 * @param integer $row_id
	 * @param string $parent field
	 */
	function getRecCatID($row_id, $cat_field = "parent_id"){
	    $id = intval($row_id);
	    $cat_id = 0;
	    if(!empty($this->table) && $id > 0){
	        if(empty($cat_field)) $cat_field = (($this->table == PAGES_TABLE) ? "ppage_id" : "parent_id");
	        $cat_id = intval($this->setFields($cat_dield)->setWhere("id = '$id'")->setLimit()->getRecItem());
	    }
	    return $cat_id;
	}

	/**
	 * Update a record
	 * @return boolean
	 */
	function updateRec() {
        global $_events;

		if(!empty($this->table) && !empty($this->fieldvals) && (!empty($this->crit) || intval($this->limit) > 0)) {
			$sql = "UPDATE {$this->table} SET {$this->fieldvals}".$this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
			$this->univUpdateQuery($sql);
			$this->clear();

            $atts = $this->getProperties();
            $_events->processTriggerEvent('saverec', $atts);
			return true;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Insert a record
	 * @return integer 				index value
	 */
	function insertRec($rememberdata = null) {
        global $_events;

		if(!empty($this->table) && !empty($this->fieldvals)) {
			$sql = "INSERT INTO {$this->table} SET {$this->fieldvals}";
            if($this->save) $this->store(__FUNCTION__, true);
			$id = $this->univInsertQuery($sql);
			if($rememberdata != null) {
				$GLOBALS['lastitemval'] = $rememberdata;
				$GLOBALS['lastitemid'] = $id;
				unset($_REQUEST);
			}
			$this->clear();

            $atts = $this->getProperties();
            $_events->processTriggerEvent('insertrec', $atts);
	        return $id;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Update a data record including attributes
	 * @return boolean
	 */
	function updateData() {
		global $_users, $_events;

		if(!empty($this->table) && !empty($this->fieldvals) && $this->data_id > 0 && (!empty($this->crit) || $this->limit > 0)) {
			// remember certain properties for the attribute-update phase
			$atts = $this->atts;
			$data_id = $this->data_id;
			$data_table = $this->table;
            $savebuttonpressed = $atts[SAVEBUTTONPRESSED];
            unset($atts[SAVEBUTTONPRESSED]);

			// update data
			$this->updateRec();

			// reset last record this user was editing
			$this->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("user_editing" => 0))->setWhere("`user_editing` = '".$_users->id."'")->updateRec();

			// set new update attributes
			$atts["date_updated"] = date(PHP_DATE_FORMAT." ".PHP_TIME_FORMAT);
			$atts["user_editing"] = $_users->id;

			// update data attributes (or insert new attributes if it does not exist)
            $attr_rec_exists = ($this->setTable(ATTRIBUTES_TABLE)->setWhere("`data_id` = '".$data_id."' AND `data_type` = '".$data_table."'")->getRecNumRows() > 0);
            $atts["data_id"] = $data_id;
            $atts["data_type"] = $data_table;
            if($attr_rec_exists) {
                $this->setTable(ATTRIBUTES_TABLE)->setAttributes($atts)->setFieldvals($atts)->setWhere("`data_id` = '".$data_id."' AND `data_type` = '".$data_table."'")->updateRec();
			}else{
				if(isset($atts["published"]) && $atts["published"] == 1)
					$atts["date_published"] = date(PHP_DATE_FORMAT." ".PHP_TIME_FORMAT);
				$this->setTable(ATTRIBUTES_TABLE)->setFieldvals($atts)->insertRec();
			}

            if(!empty($savebuttonpressed))
                $_events->processTriggerEvent('savedata', $atts);
			return true;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Update a data record including attributes
	 * @return boolean
	 */
	function insertData($rememberdata = null) {
		global $_users, $_events;

		if(!empty($this->table) && !empty($this->fieldvals)) {
			// remember certain properties for the attribute-insertion phase
			$atts = $this->atts;
			$data_table = $this->table;
            $savebuttonpressed = $atts[SAVEBUTTONPRESSED];
            unset($atts[SAVEBUTTONPRESSED]);

			// insert data (get the inserted id)
			$data_id = $this->insertRec();

			// reset last record this user was editing
			$this->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("user_editing" => 0))->setWhere("`user_editing` = '".$_users->id."'")->updateRec();

			// set new insertion attributes
			if(isset($atts["published"]) && $atts["published"] == 1)
				$atts["date_published"] = date(PHP_DATE_FORMAT." ".PHP_TIME_FORMAT);
			$atts["date_updated"] = $atts["date_created"] = date(PHP_DATE_FORMAT." ".PHP_TIME_FORMAT);
			$atts["user_editing"] = $_users->id;
			$atts["data_id"] = $data_id;
			$atts["data_type"] = $data_table;

			// insert data attributes
			$this->setTable(ATTRIBUTES_TABLE)->setFieldvals($atts)->insertRec();

			if($rememberdata != null) {
				$GLOBALS['lastitemval'] = $rememberdata;
				$GLOBALS['lastitemid'] = $data_id;
				unset($_REQUEST);
			}

            if(!empty($savebuttonpressed))
                $_events->processTriggerEvent('savedata', $atts);
	        return $data_id;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Update a record if exists, otherwise insert a new record
	 * @return boolean
	 */
	function replaceRec() {
        global $_events;

		if(!empty($this->table) && !empty($this->fieldvals) && !empty($this->crit)) {
            $sql = "SELECT {$this->fields} FROM {$this->table}".$this->prepSQL();
            $nr = $this->univGetNumRows($sql);
			if($nr > 0){
				return $this->updateRec();
			}else{
				return $this->insertRec();
			}
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Delete a record
	 * @return boolean
	 */
	function deleteRec() {
        global $_events;

		if(!empty($this->table) && !empty($this->crit)) {
			$sql = "DELETE FROM {$this->table}".$this->prepSQL();
            if($this->save) $this->store(__FUNCTION__, true);
			$result = $this->univDeleteQuery($sql);
			$this->clear();
			if(!$result) $fnerr = DB_DELETE_ERROR;

            $atts = $this->getProperties();
            $_events->processTriggerEvent('deleterec', $atts);
	    	return true;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

	/**
	 * Copy a record
	 * @return integer 				index value
	 */
	function cloneRec(){
        global $_events;

		if(!empty($this->table) && intval($this->data_id) > 0 && !empty($this->fieldvals)){
			$table = $this->table;
			$newfldvals = $this->field_obj;		// this holds the field/values to clone
			$data_id = $this->data_id;

			// get data from original record
			$srcrec = $this->setTable($table)->setWhere("id = $data_id")->setLimit()->getRec();

			foreach($srcrec[0] as $key => $value){
				if($key != $clonenewfld && $key != "id" && !is_int($key)){
					// skip passed param which includes the new record title provided by user
					$incl_fld = true;
					if($key == "rank"){
						$value = $this->setTable($table)->setFields("rank")->getLastID() + 1;
					}elseif(substr($key, 0, 5) == 'date_' && $value == ''){
						$value = date('Y-m-d');
					}
					if($incl_fld){
						$newfldvals[$key] = $value;
					}
				}
			}
			$id = $this->setTable($table)->setFieldvals($newfldvals)->insertRec();

            $atts = $this->getProperties();
            $_events->processTriggerEvent('clonerec', $atts);
			if($id > 0){
				return $id;
			}else{
				return false;
			}
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

    /**
     * Check if another user is editing record (single record only)
     * @param array $rec
     * @return array $rec
     */
    function checkRecLocked($rec){
        global $_users, $_error;

        if(!is_array($rec) || empty($rec)) return array(false, $rec);
        if(count($rec) > 1) return array(false, $rec);

        $dims = 1;
        if(isset($rec[0])) {
            $rec = $rec[0];                     // flatten array to rec
            $dims = 2;
        }
        $rec['in_use'] = $in_use = false;
        if($rec['user_editing'] != $_users->id && $rec['user_editing'] > 0) {
            $rec['in_use'] = $in_use = true;
            $_error->addErrorMsg(DATA_LOCKED, RUNTIME_ERR);
        }
        if($dims == 2) $rec = array($rec);      // redim array as rec[0]
        return array($in_use, $rec);
    }

    /**
     * Lock one or more records to the currently logged in user
     * @param array $recs
     * @return array $recs                  Modified with 'user-editing' field set to user_id
     */
    function lockRecs($recs){
        global $_users;

        $rec_attr_ids = array();
        if(!isset($recs[0])) $recs = array($recs);
        foreach($recs as $rec){
            $rec_attr_ids[] = $rec['attribute_id'];
            $rec['user_editing'] = $_users->id;
        }

        $sql = "UPDATE `".ATTRIBUTES_TABLE."` SET `user_editing` = '".$_users->id."' WHERE `attribute_id` IN (".join(",", $rec_attr_ids).")";
        $this->univUpdateQuery($sql);
        return $recs;
    }

    /**
     * Unlock one or more records
     * @param string $data_type (optional)
     * @param string $class (optional)
     * @param integer $id (optional)
     * @param integer $user_id (optional)
     */
    function unlockRecs($data_type = null, $class = null, $id = 0, $user_id = 0){
        $id = intval($id);
        $where = "1=1";
        if(!empty($data_type)) $where .= " AND `data_type` = '".$data_type."'";
        if(!empty($class)) $where .= " AND `attribute_class` = '".$class."'";
        if($id > 0) $where .= " AND `attribute_id` = '".$id."'";
        if($user_id == 0){
            $sql = "UPDATE `".ATTRIBUTES_TABLE."` SET `user_editing` = 0 WHERE ".$where;
        }else{
            $sql = "UPDATE `".ATTRIBUTES_TABLE."` SET `user_editing` = 0 WHERE ".$where." AND `user_editing` = '".$user_id."'";
        }
        $this->univUpdateQuery($sql);
    }

	/**
	 * Reduce an array derived from a 'get' function into an array of id:value-paired elements
	 * @param array $arry
	 * @param string $idfld 			Set to blank to reduce array even further to an indexed array of values (val1, val2...)
	 * @param string $valfld
	 * @return array
	 */
	function flattenDBArray($arry, $idfld, $valfld) {
        $retnarry = array();
        if(is_array($arry) && !empty($valfld)){
            foreach($arry as $elem){
                $val = null;
                if(is_array($valfld)){
                    foreach($valfld as $valelem){
                        if(isset($elem[$valelem])) $val .= $elem[$valelem];
                    }
                }else{
                    if(isset($elem[$valfld])) $val = $elem[$valfld];
                }
                if(empty($idfld))
                    $retnarry[] = $val;
                else
                    $retnarry[$elem[$idfld]] = $val;
            }
            return $retnarry;
        }else{
            _e(MISSING_ARG.__FUNCTION__, true);
            return false;
        }
	}

    /**
     * Reduce an array derived from the 'getTree' function into an array of id:value-paired elements
     * @param array $arry
     * @param string $idfld             Set to blank to reduce array even further to an indexed array of values (val1, val2...)
     * @param string $valfld
     * @return array
     */
    function flattenDBTreeArray($arry, $idfld, $valfld, $padstr = "&nbsp;") {
        $retnarry = array();
        if(is_array($arry) && !empty($valfld)){
            foreach($arry as $elem){
                $val = null;
                if(is_array($valfld)){
                    foreach($valfld as $valelem){
                        if(isset($elem[$valelem])) $val .= $elem[$valelem];
                    }
                }else{
                    if(isset($elem[$valfld])) $val = $elem[$valfld];
                }
                if(isset($elem['__depth']))
                    $val = str_repeat($padstr, $elem['__depth']).$val;
                if(empty($idfld))
                    $retnarry[] = $val;
                else
                    $retnarry[$elem[$idfld]] = $val;
            }
            return $retnarry;
        }else{
            _e(MISSING_ARG.__FUNCTION__, true);
            return false;
        }
    }

    /**
     * Return array containing all table names
     * @return array
     */
    function getTables(){
        $rtn = array();
        if(defined('DBNAME')){
            $tables = $this->univGetQuery("SHOW TABLES");
            if(count($tables) > 0){
                foreach($tables as $table){
                    $rtn[] = $table['Tables_in_'.DBNAME];
                }
            }
        }
        return $rtn;
    }

	/**
	 * Return array containing all data table names
	 * @param boolean $inclpages
	 * @return array
	 */
	function getDataTables($inclpages = true, $stripdbprefix = false){
		$rtn = array();
	    if(defined('DBNAME')){
	        $tables = $this->univGetQuery("SHOW TABLES WHERE `tables_in_".DBNAME."` LIKE '".DB_TABLE_PREFIX."%'".(($inclpages) ? " OR `tables_in_".DBNAME."` = '".PAGES_TABLE."'" : ""));
	        if(count($tables) > 0){
	            foreach($tables as $table){
	                $rtn[] = ($stripdbprefix) ? str_replace(DB_TABLE_PREFIX, "", $table['Tables_in_'.DBNAME]) : $table['Tables_in_'.DBNAME];
	            }
	        }
	    }
		return $rtn;
	}

    /**
     * Return array containing all data tables (as keys) and all columns of each table (as value arrays)
     * @param boolean $inclpages
     * @return array
     */
    function getDataTableFields($inclpages = true){
        $rtn = array();
        $t = $this->getDataTables($inclpages);
        if(!empty($t)){
            foreach($t as $table){
                $sql = "SHOW COLUMNS FROM `{$table}`";
                $recs = $this->univGetQuery($sql);
                $cols = array();
                foreach($recs as $rec){
                    $cols[$rec['Field']] = $rec;
                }
                $rtn[$table] = $cols;
                $this->clear();
            }
        }
        return $rtn;
    }

	/**
	 * Return all or specific field(s) schema from a table
	 * @param string $find_field
	 * @return boolean
	 */
	function getTableFields($find_field = ''){
		if(!empty($this->table)) {
			$sql = "SHOW COLUMNS FROM `{$this->table}`".(($find_field != '') ? " WHERE `field` = '$find_field'" : "");
			$rec = $this->univGetQuery($sql);
            if($this->result_type == DB_OBJECT) $rec = json_decode(json_encode($rec));
			$this->clear();
	        return $rec;
		}else{
			_e(MISSING_ARG.__FUNCTION__, true);
			return false;
		}
	}

    /**
     * Return the most likely title or name field from a recordset
     * @param array $rec
     * @return string
     */
    function getTitleField($rec){
        if(isset($rec['title']))
            $val = $rec['title'];
        elseif(isset($rec['pagetitle']))
            $val = $rec['pagetitle'];
        elseif(isset($rec['name']))
            $val = $rec['name'];
        elseif(isset($rec['firstname']) && isset($rec['lastname']))
            $val = $rec['firstname']." ".$rec['lastname'];
        elseif(isset($rec['fname']) && isset($rec['lname']))
            $val = $rec['fname']." ".$rec['lname'];
        elseif(isset($rec['address']))
            $val = $rec['address'];
        else
            $val = null;
        return $val;
    }

    /**
     * Return the most likely ID field from a recordset
     * @param array $rec
     * @return string
     */
    function getIDField($rec){
        if(isset($rec['id']))
            $val = $rec['id'];
        elseif(isset($rec['ID']))
            $val = $rec['ID'];
        elseif(isset($rec['attribute_id']))
            $val = $rec['attribute_id'];
        elseif(isset($rec['index']))
            $val = $rec['index'];
        elseif(isset($rec['indx']))
            $val = $rec['indx'];
        else
            $val = null;
        return $val;
    }

	/**
	 * Return whether or not table exists
	 * @return boolean
	 */
	function findTable(){
	    if(!empty($this->table)){
	        $tables = $this->univGetQuery("SHOW TABLES WHERE `tables_in_".DBNAME."` = '".$this->table."'");
	        return (count($tables) > 0);
	    }
	    return false;
	}

    /**
     * Return information about the database environment
     * @return array
     */
    function getDatabaseServerInfo(){
        $info = array();
        $client_info = (isset($this->db->client_info) ? $this->db->client_info : mysqli_get_client_version());
        if(!is_null($client_info)){
            if(strpos($client_info, 'mysql') !== false)
                $info['Engine'] = "MySQLi";
        }
        if(isset($this->db->client_version)) $info['Client Version'] = $this->db->client_version;
        if(isset($this->db->host_info)) $info['Host'] = $this->db->host_info;
        if(isset($this->db->stat)) $info['Statistics'] = $this->db->stat;
        return $info;
    }

    /**
     * Return database info
     */
    function getDatabaseInfo(){
        return $this->getDBInfo();
    }

    /**
     * Return the size of the current database
     * @return string
     */
    function getDatabaseSize(){
        $sql = "SHOW TABLE STATUS";
        $recs = $this->univGetQuery($sql);
        $dbsize = 0;
        foreach($recs as $rec){
            $dbsize += $rec['Data_length'] + $rec['Index_length'];
        }
        return formatFileSize($dbsize);
    }

	// ----------- ANCESTRY FUNCTIONS ---------------

	/**
	 * Retrieves array of multiple-depth categories (if a single-depth list is desired, use getRec).
	 * @param array $atts 					parent_id (int), skipself (boolean)
	 * @return array
	 */
	function getTree($atts = array()){
	    if(!empty($this->table) && $this->data_id >= 0){
            if($this->save) $this->store(__FUNCTION__, true);
	    	// arguments
	    	$skipself = getBooleanIfSet($atts['skipself'], false);
            $saved_crit = $this->crit;
            $parent_id = getIntValIfSet($atts['parent_id'], 0);

            $tree_arry = $this->getBranches($parent_id, $this->crit, $skipself);
	        return $tree_arry;
	    }else{
	        _e(MISSING_ARG.__FUNCTION__, true);
	        return null;
	    }
	}

    /**
     * Retrieves data array of multiple-depth categories (if a single-depth list is desired, use getRec).
     * @param array $atts                   parent_id (int), skipself (boolean)
     * @return array
     */
    function getDataTree($atts = array()){
        if(!empty($this->table) && $this->data_id >= 0){
            if($this->save) $this->store(__FUNCTION__, true);
            // arguments
            $skipself = getBooleanIfSet($atts['skipself'], false);
            $saved_crit = $this->crit;
            $parent_id = getIntValIfSet($atts['parent_id'], 0);

            $tree_arry = $this->getBranches($parent_id, $this->crit, $skipself, -1, true);
            return $tree_arry;
        }else{
            _e(MISSING_ARG.__FUNCTION__, true);
            return null;
        }
    }

    /**
     * Get all branches below the designated attributes parent_id and return as a hierarchical array (includes __depth element)
     * @param integer $parent_id        The root parent id to start at
     * @param string $crit              An optional criteria for the WHERE clause
     * @param boolean $skipself         True to skip the record designated by ->data_id
     * @param integer $depth            Used internally as a depth tracker
     * @return array
     *
     */
    function getBranches($parent_id = 0, $crit = null, $skipself = true, $depth = -1, $inclattrs = false){
        $arry = array();
        if(!empty($this->table) && $this->data_id >= 0){
            if($inclattrs){
                $this->crit = ATTRIBUTES_TABLE.".`parent_id` = '".$parent_id."'";
                if($skipself) $this->crit .= " AND ".ATTRIBUTES_TABLE.".`data_id` != '".$this->data_id."'";
                if(!empty($crit)) $this->crit .= " AND ".$crit;
                $branch_nodes = $this->getData(false, false, true);
            }else{
                $this->crit = $this->table.".`parent_id` = '".$parent_id."'";
                if($skipself) $this->crit .= " AND ".$this->table.".`id` != '".$this->data_id."'";
                if(!empty($crit)) $this->crit .= " AND ".$crit;
                $branch_nodes = $this->getRec(false, true);
            }
            if(!empty($branch_nodes)){
                $depth++;
                foreach($branch_nodes as $branch_node){
                    $branch_node['__depth'] = $depth;
                    $new_parent_id = (($inclattrs) ? $branch_node['data_id'] : $branch_node['id']);
                    $arry[$new_parent_id] = $branch_node;
                    $branch_arry = $this->getBranches($new_parent_id, $crit, $skipself, $depth, $inclattrs);
                    if(!empty($branch_arry)) $arry = $arry + $branch_arry;
                }
            }
        }
        return $arry;
    }

	/**
	 * Returns 1) an array of ancestor ids, 2) the highest ancestor id reached, 3) if we reached
	 * the top of the ancestor tree.
	 * Reaching the treetop is useful especially if we want to know if the entire
	 * $code_array chain is valid, otherwise the process will only collect ancestor ids
	 * @param array $atts 					cat_field, code_array
	 * @return array 						Ancestor array, highest ancestor, if treetop was reached
	 */
	function getRecAncestors(){
	    $ancestors = array();
	    $highest_parent = -1;
	    $cat_id = 0;
	    $treetop_reached = false;

	    if(!empty($this->table) && $this->data_id > 0){
	    	// arguments
	    	$code_array = getIfSet($atts['code_array'], null);

	        $table = strtolower($this->table);
	        $from_id = $id = $this->data_id;
	        if($table == PAGES_TABLE){
	        	$cat_field = "ppage_id";
	        }elseif(empty($cat_field)){
		    	$cat_field = getIfSet($atts['cat_field'], null);
	            if(empty($cat_field)) $cat_field = "cat_id";
	        }
	        $code_field = (($table == PAGES_TABLE) ? "pagename" : "code");

            // cat_table is either the current table (category-based records) or
            // {$table}_cat (standard records)
            $cat_table = $table;
            if(substr($table, -4, 4) != '_cat'){
                if($this->findTable($table."_cat")){
                    $cat_table = $table."_cat";
                }
            }

            // get the category index of this record
            $cat_id = intval($this->setTable($cat_table)->setFields($cat_field)->setWhere("id = '$id'")->getRecItem());
            $limiter = 0;
            while($cat_id > 0 && $limiter < 255){
                // a parent exists
                $ancestors[] = $cat_id;
                $id = $cat_id;

                // traverse by ids
                $cat_id = intval($this->setWhere("id = '$id'")->getRecItem());
                $limiter++;         	// just in case we enter a loop
            }
            $highest_parent = $id;  	// highest parent is the last id reached
	    }
	    $treetop_reached = ($highest_parent > 0 && $cat_id == 0);
	    return array($ancestors, $highest_parent, $treetop_reached);
	}
}
?>