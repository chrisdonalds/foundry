<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("EDITORSLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/* EDITORS CLASS
 *
 * Maintains the CMS Editors API
 */
class EditorsClass {
	// overloaded data
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'_editors');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Return desired CMS editor version number
	 * @return string
	 */
	function getCMSEditorVer($editor){
	    global $_db_control;

	    $ver = $_db_control->setTable(PLUGINS_TABLE)->setFields("ver")->setWhere("`incl` = '$editor' && `is_editor` = 1")->getRecItem();
	    return $ver;
	}

	/**
	 * Return list active CMSes as a plugins sub-object
	 * @return array
	 */
	function getListofCMSEditors(){
	    global $_system, $_db_control;

	    $active_cmses = array();
	    $arry = $_db_control->setTable(PLUGINS_TABLE)->setFields("incl")->setWhere("`is_editor` = 1")->getRec();
	    if(count($arry) > 0){
	        $recognized_cmses = array();
	        foreach($arry as $elem){
	            $recognized_cmses[] = $elem['incl'];
	        }
	        foreach($_system->plugins as $plugin){
	            if(in_array($plugin['incl'], $recognized_cmses))
	                $active_cmses[$plugin['incl']] = $plugin;
	        }
	    }
	    return $active_cmses;
	}

	/**
	 * Load editor settings found in editor.info files into database.
	 * This function does not make the editors ready for use.  It
	 * processes any new or updated editors.  Call $_themes->prepHeadSection,
	 * includePlugins or loadPlugin to put editors into use.
	 * @internal Core function
	 * @see loadPlugin
	 * @version 3.6
	 */
	function getInstalledEditors($updflag){
	    global $_settings, $_users, $_db_control;

	    $stack = debug_backtrace();
	    if(strpos($stack[0]['file'], "_core") === false && strpos($stack[0]['file'], "_lib") === false) die('Calling '.__FUNCTION__.' in '.$stack[0]['file'].' not allowed.');

	    if(!($_users->userIsAllowedTo('install_editors') && $_users->userIsAllowedTo('update_editors')))
	        return false;

	    $numIns = 0;
	    $numUpd = 0;
	    $numDel = 0;

	    $maxtime = ini_get('max_execution_time');
	    set_time_limit(0);

	    $editorpaths = $this->readInstalledEditorFolders(array(), ADMIN_FOLDER.EDITOR_FOLDER);
	    $editorslist = $this->readInstalledEditorInfoFiles($editorpaths);

	    // loop through editors updating/adding each to the database
	    foreach($editorslist as $editorslug => $editorarry){
	        // find editor record
	        $temp_updflag = $updflag;
	        $editorid     = $editorarry['id'];

	        // forced settings
	        $editorarry['builtin'] = 1;
	        $editorarry['is_editor'] = 1;
	        $editorarry['is_framework'] = 0;
	        unset($editorarry['is_ext']);

	        // handle read cycle errors
	        if(!empty($editorarry['errors'])){
	            // error detected...
	            $editorarry['errors'] = join("<br/>", $editorarry['errors']);
	            $temp_updflag = $editorarry['mark'];
	        }else{
	            // no problems here...
	            $editorarry['errors'] = '';
	            $editorarry['error_code'] = 0;
	        }

	        unset($editorarry['id']);
	        unset($editorarry['mark']);
	        if($editorid > 0){
	            // update
	            $fldvals = array("updflag" => $temp_updflag);
	            foreach($editorarry as $key => $data) $fldvals[$key] = str_replace("'", "&#34;", $data);
	            if($_db_control->setTable(PLUGINS_TABLE)->setFieldvals($fldvals)->setWhere("id = {$editorid}")->updateRec()) $numUpd++;
	        }else{
	            // insert
	            $fldvals = array("updflag" => $temp_updflag);
	            foreach($editorarry as $key => $data) $fldvals[$key] = str_replace("'", "&#34;", $data);
	            $editorid = $_db_control->setTable(PLUGINS_TABLE)->setFieldvals($fldvals)->insertRec();
	            if($editorid > 0) $numIns++;
	        }
	    }

	    // mark editors that were not found, or have config errors, as deleted/problematic
	    $_db_control->setTable(PLUGINS_TABLE)->setFieldvals(array("updflag" => -1))->setWhere("is_editor = 1 AND updflag != '{$updflag}' AND updflag != -2")->updateRec();
	    $_db_control->setTable(PLUGINS_TABLE)->setFieldvals(array("active" => 0, "error_code" => PLUGIN_CFGERR_REGPATHNF, "errors" => "The editor was not found in its registed path, and has been deactivated."))->setWhere("is_editor = 1 AND updflag = -1 AND is_deleted = 0 AND active = 1 AND error_code = 0")->updateRec();
	    $numDel = $_db_control->setTable(PLUGINS_TABLE)->setWhere("is_editor = 1 AND updflag = -1 AND is_deleted = 0")->getRecNumRows();

	    // present issues
	    if($numIns > 0) $_settings->addSettingsIssue('editors-info', $numIns." NEW editor".(($numIns != 1) ? "s" : "")." ".(($numIns != 1) ? "were" : "was")." found and installed.");
	    if($numDel > 0) $_settings->addSettingsIssue('editors', $numDel." editor".(($numDel != 1) ? "s" : "")." ".(($numDel != 1) ? "were" : "was")." NOT found, ".(($numDel > 1) ? "have" : "has")." had ".(($numDel > 1) ? "their" : "its")." name changed, or ".(($numDel > 1) ? "have" : "has")." info file problems.");

	    set_time_limit($maxtime);
	}

	/**
	 * Read editor folders searching for valid plugin.info files for processing
	 * @param str $dir
	 * @param boolean $is_ext               True if search is for extensions; false for plugins
	 * @internal Core function
	 * @return array
	 * @version 3.6
	 */
	function readInstalledEditorFolders($editorpaths, $dir, $is_ext = false, $mergearrays = true){
	    global $_settings, $_db_control, $_filesys, $_events;

	    $stack = debug_backtrace();
	    if(strpos($stack[0]['file'], "_core") === false && strpos($stack[0]['file'], "_lib") === false) die('Calling '.__FUNCTION__.' in '.$stack[0]['file'].' not allowed.');

	    // first, read folders and identify whether their contents
	    // are already in DB or are newly installed

	    $updpaths = array();    // editors that are found in db and ready for update
	    $newpaths = array();    // editors not found in db and deemed new
	    $errpaths = array();    // editors that are found in db but were marked with an error
	    if(isset($dir)){
	        $this_dir = $dir;           // path/_editors
	        $this_dir = $_filesys->addSlash($this_dir);
	        if(false !== ($handle = opendir(SITE_PATH.$dir))) {
	            while (false !== ($file = readdir($handle))) {
	                if($file != '.' && $file != '..'){
	                    if(is_dir(SITE_PATH.$this_dir.$file)){
	                        $sub_dir = $this_dir.$file."/";     // path/_editors/folder
	                        if(file_exists(SITE_PATH.$sub_dir.'editor.info')){
	                            // remove record if folder is relative
	                            // $_db_control->setTable(PLUGINS_TABLE)->setWhere("folder = '".getRelativePath($sub_dir)."'")->deleteRec();

	                            // get the editor record where the folders match
	                            // - if matched, editor is being updated, otherwise it is new
	                            $editorfromdb = $_db_control->setTable(PLUGINS_TABLE)->setWhere("folder = '".$sub_dir."' OR folder = '".SITE_PATH.$sub_dir."'")->setLimit()->getRec();
	                            if(count($editorfromdb) > 0){
	                                // matched
	                                if($editorfromdb[0]['error_code'] == 0){
	                                    $updpaths[] = array("id" => $editorfromdb[0]['id'], "path" => $sub_dir, "info" => $sub_dir."editor.info", "tempname" => "", "is_ext" => $is_ext);
	                                }else{
	                                    $errpaths[] = array("id" => $editorfromdb[0]['id'], "path" => $sub_dir, "info" => $sub_dir."editor.info", "tempname" => "", "errorcode" => $editorfromdb[0]['error_code'], "is_ext" => $is_ext);
	                                }
	                            }else{
	                                // not matched
	                                $newpaths[] = array("id" => 0, "path" => $sub_dir, "info" => $sub_dir."editor.info", "tempname" => $file, "is_ext" => $is_ext);
	                            }
	                        }

	                        // dig into this directory for more editors
	                        $editorpaths = $this->readInstalledEditorFolders($editorpaths, $sub_dir, $is_ext);
	                    }
	                }
	            }
	            closedir($handle);
	        }
	    }
	    if($mergearrays)
	       $r = array_merge($editorpaths, $updpaths, $errpaths, $newpaths);
	    else
	       $r = array($editorpaths, $updpaths, $errpaths, $newpaths);
		$_events->processTriggerEvent(__FUNCTION__.'_done', $r);				// alert triggered functions when function executes
	   	return $r;
	}

	/**
	 * Read editor.info file data, populating editorlist array
	 * @param str $dir
	 * @internal Core function
	 * @return array
	 * @version 3.6
	 */
	function readInstalledEditorInfoFiles($editorpaths){
	    global $_settings, $_filesys;

	    $stack = debug_backtrace();
	    if(strpos($stack[0]['file'], "_core") === false && strpos($stack[0]['file'], "_lib") === false) die('Calling '.__FUNCTION__.' in '.$stack[0]['file'].' not allowed.');

	    // first, read folders and identify whether their contents
	    // are already in DB or are newly installed

	    $valid_setting_keys = array('name', 'author', 'ver', 'created', 'revised', 'descr', 'sysver', 'license',
	                                'website', 'usedin', 'incl', 'initfile', 'initcaller', 'headerfunc', 'settingsfunc',
	                                'depends', 'parent', 'nodelete', 'nodisable', 'builtin', 'autoincl', 'group', 'active');

	    if(is_null($editorpaths)) return false;

	    $editorslist = array();
	    foreach($editorpaths as $pathdata){
	        $fhandle = @fopen(SITE_PATH.$pathdata['info'], 'r');
	        if($fhandle !== false){
	            $editorname = null;
	            $editorslug = null;
	            $editorarry = array();
	            $editorarry['id'] = $pathdata['id'];
	            $editorerrs = array();
	            $editorerrcode = 0;
	            $editormark = -1;
	            $editorfolder = $_filesys->getLowestChildFolder($pathdata['path'])."/";
	            $editorext = $pathdata['is_ext'];

	            while($setting = fscanf($fhandle, "%[#]%s\t%[^\t]")){
	                list(, $key, $val) = $setting;
	                $key = strtolower(str_replace(':', '', $key));
	                $val = trim($val);
	                if(in_array($key, $valid_setting_keys)){
	                    switch($key){
	                        case 'name':
	                            if(is_null($editorname)){
	                                if($pathdata['tempname'] != '') $val = ucwordsAdv($pathdata['tempname']);
	                                $editorslug = slugify($val);
	                                if(isset($editorslist[$editorslug])){
	                                    // oops! slug already in use
	                                    $editorerrs[] = "The name '".$val."' in ".$editorfolder."editor.info has been registered to another editor.";
	                                    $editorerrcode += PLUGIN_CFGERR_NAMEDUP;
	                                    // use folder as temp name so data can be saved
	                                    $editorslug = slugify($editorfolder);
	                                    $editorname = ucwordsAdv($editorslug);
	                                    $editormark = -2;
	                                }else{
	                                    $editorname = $val;
	                                }
	                            }
	                            break;
	                        case 'builtin':
	                            $editorarry['is_ext'] = 0;    // a temporary marker that indicates an extension
	                            break;
	                        case 'autoincl':
	                        case 'nodelete':
	                        case 'nodisable':
	                        case 'active':
	                            $val = strtolower($val);
	                            $val = (($val == '1' || $val == 'yes' || $val == 'true') ? 1 : 0);
	                            break;
	                        case 'created':
	                        case 'revised':
	                            $val = ((isDate($val)) ? date(PHP_DATE_FORMAT, strtotime($val)) : BLANK_DATE);
	                            break;
	                        case 'usedin':
	                            if(!in_array(strtolower($val), array('admin', 'front', 'both'))) {
	                                $editorerrs[] = "'".$val."' is not a valid USEDIN value in ".$editorfolder."editor.info.";
	                                $editorerrcode += PLUGIN_CFGERR_USEDINBAD;
	                            }
	                            break;
	                        case 'license':
	                            if(!in_array(strtolower($val), array('free', 'trial', 'limited', 'full', 'subscription'))) {
	                                $editorerrs[] = "'".$val."' is not a valid LICENSE value in ".$editorfolder."editor.info.";
	                                $editorerrcode += PLUGIN_CFGERR_LICENSEBAD;
	                            }
	                            break;
	                        case 'website':
	                            if(strtolower(substr($val, 0, 4)) != 'http' && $val != '') $val = 'http://'.$val;
	                            break;
	                        case 'settingsfunc':
	                        case 'headerfunc':
	                            if(!empty($val))
	                                if(multiarray_search($val, $editorslist) == $key){
	                                    $editorerrs[] = "'".$val."' is already registered as the ".strtoupper($key)." to another editor.";
	                                    $editorerrcode += (($key == 'settingsfunc') ? PLUGIN_CFGERR_SETTINGSFUNC : PLUGIN_CFGERR_HEADERFUNC);
	                                }
	                            break;
	                        case 'incl':
	                            if(multiarray_search($val, $editorslist) == $key){
	                                $editorerrs[] = "'".$val."' is already assigned as the ".strtoupper($key)." to another editor.";
	                                $editorerrcode += PLUGIN_CFGERR_INCLDUP;
	                            }elseif(preg_match("/([^0-9a-z\-_])/i", $val)){
	                                $editorerrs[] = "'".$val."' can only contain letters, numbers, hyphen or underscore.";
	                                $editorerrcode += PLUGIN_CFGERR_INCLBAD;
	                            }
	                            break;
	                        case 'initfile':
	                            if(empty($val)){
	                                $editorerrs[] = strtoupper($key)." cannot be blank.";
	                                $editorerrcode += PLUGIN_CFGERR_INITFILENF;
	                            }elseif(!file_exists(SITE_PATH.$pathdata['path'].$val)){
	                                $editorerrs[] = "The ".strtoupper($key)." was not found in the ".$editorfolder." folder.";
	                                $editorerrcode += PLUGIN_CFGERR_INITFILEBAD;
	                            }
	                            break;
	                        case 'group':
	                            break;
	                    }
	                    $editorarry[$key] = $val;
	                }
	            }
	            fclose($fhandle);

	            if($editorname == null) {
	                $editorerrs[] = 'NAME is missing in '.$editorfolder.'editor.info.';
	                $editorerrcode += PLUGIN_CFGERR_NAMENF;
	                continue;
	            }elseif(!isset($editorarry['sysver'])){
	                $editorerrs[] = 'SYSVER is required in '.$editorfolder.'editor.info.';
	                $editorerrcode += PLUGIN_CFGERR_SYSVERNF;
	            }elseif(!isset($editorarry['usedin'])){
	                $editorerrs[] = 'USEDIN is required in '.$editorfolder.'editor.info.';
	                $editorerrcode += PLUGIN_CFGERR_USEDINNF;
	            }elseif(!isset($editorarry['incl'])){
	                $editorerrs[] = 'INCL is required in '.$editorfolder.'editor.info.';
	                $editorerrcode += PLUGIN_CFGERR_INCLNF;
	            }elseif(!isset($editorarry['initfile'])){
	                $editorerrs[] = 'INITFILE is required in '.$editorfolder.'editor.info.';
	                $editorerrcode += PLUGIN_CFGERR_INITFILENF;
	            }

	            if(count($editorerrs) > 0){
	                $editorarry['errors'] = $editorerrs;
	                $editorarry['error_code'] = $editorerrcode;
	                $editorarry['mark'] = $editormark;
	                $editorarry['active'] = 0;
	                $_settings->settings_issues['editors'] = array_merge($_settings->settings_issues['editors'], $editorerrs);
	            }else{
	                $editorarry['errors'] = "";
	                $editorarry['error_code'] = "";
	                $editorarry['mark'] = 0;
	                // $editorarry['active'] = 1;
	                $editorarry['autoincl'] = 1;
	            }
	            $editorarry['folder'] = $_filesys->getRelativePath($pathdata['path']);
	            $editorslist[$editorslug] = $editorarry;
	        }
	    }
	    return $editorslist;
	}
}

?>