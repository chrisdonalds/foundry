<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("MEDIALIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

if(!defined("MT_IMAGE")) define("MT_IMAGE", "image");
if(!defined("MT_ICON")) define("MT_ICON", "icon");
if(!defined("MT_DOC")) define("MT_DOC", "text/document");
if(!defined("MT_WEB")) define("MT_WEB", "web");
if(!defined("MT_GEO")) define("MT_GEO", "geological");
if(!defined("MT_AV")) define("MT_AV", "audio/video");
if(!defined("MT_GENERIC")) define("MT_GENERIC", "generic");
if(!defined("MT_CUSTOM")) define("MT_CUSTOM", "custom");

/* MEDIA CLASS
 *
 * Stores and manages media-related files and data
 */
class MediaClass {
	// overloaded data
	private $_keys = array( "formats" => array(), "systemformats" => array(), "pluginformats" => array(), "themeformats" => array()
                            );
	private $_subkeys = array();
	public $_collect = null;
	protected static $_instance = null;

	private function __construct() {
	}

	private function __clone(){
	}

	public static function init(CollectorClass $collect){
		global $_events;

		$s = new self;
		$s->_collect = $collect;
		$_events->processTriggerEvent(__FUNCTION__.'_media');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Save image file to disk.  Alias of $_filesys->saveImage()
	 */
	public function saveImage($imgfields, $destfolder, $destthumbfolder = "", $createThumb = false, $thumbdim = false, $altdim = false, $altfile = null){
		global $_filesys;
		$r = $_filesys->saveImage($imgfields, $destfolder, $destthumbfolder, $createThumb, $thumbdim, $altdim, $altfile);
		return $r;
	}

	/**
	 * Save file to disk.  Alias of $_filesys->saveFile()
	 */
	public function saveFile($filefields, $destfolder, $altfile = null){
		global $_filesys;
		$r = $_filesys->saveFile($filefields, $destfolder, $altfile);
		return $r;
	}

	/**
	 * Return media types from a specific type group or all types
	 * @param string $type_groups 			A specific group or null/blank to return all types
	 * 											Image, icon, text/document, web, geological, audio/video, generic, custom
	 *											For multiple groups, either pass an array of types or comma-separate them
	 * @return array
	 */
	public function getMediaTypes($type_groups = array()){
		global $_events;

		$types = array(
			'image' => array(
				'cgm',
				'bmp',
				'gif',
				'jp2',
				'jpeg',
				'jpg',
				'jpm',
				'jpx',
				'pcx',
				'pic',
				'png',
				'svg',
				'tif',
				'tiff',
			),
			'icon' => array(
				'ico',
			),
			'text/document' => array(
				'csv',
				'doc',
				'docx',
				'pdf',
				'rtf',
				'rtx',
				'txt',
				'wps',
				'xls',
				'xlsx',
			),
			'web' => array(
				'css',
				'htm',
				'html',
				'js',
				'json',
				'log',
				'markdown',
				't',
				'ttl',
				'vcf',
				'xml',
				'xsl',
			),
			'geological' => array(
				'geojson',
				'kml',
				'kmz',
				'dxf',
			),
			'audio/video' => array(
				'3gp',
				'3g2',
				'aac',
				'flv',
				'f4v',
				'h261',
				'h263',
				'h264',
				'jpgv',
				'mid',
				'mj2',
				'mp4',
				'mp4a',
				'mpeg',
				'mpg',
				'mpga',
				'mseq',
				'nv',
				'oga',
				'ogg',
				'ogv',
				'qt',
				'ram',
				'raw',
				'rtx',
				'swf',
				'wav',
				'weba',
				'webm',
			),
			'generic' => array(
				'azw',
				'gz',
				'gzip',
				'mxf',
				'ma',
				'mbox',
				'msh',
				'mdb',
				'rar',
				'tar',
				'zip',
			),
			'custom' => array(
			)
		);
		list($more_types) =  $_events->processTriggerEvent(__FUNCTION__);
		if(!empty($more_types) && is_array($more_types)){
			$types['custom'] = $types['custom'] + $more_types;
		}

		$rtn = array();
		if(!is_array($type_groups)) $type_groups = explode(",", $type_groups);
		if(!empty($type_groups)){
			foreach($type_groups as $type_group){
				$type_group = trim(strtolower($type_group));
				if(isset($types[$type_group]))
					$rtn = array_merge($rtn, $types[$type_group]);
			}
		}else{
			$rtn = $types;
		}

		return $rtn;
	}

	/**
	 * Return media types from a specific type group or all types as HTML select menu options
	 * @param string|array $values 			Current selections as a comma-separated string or array
	 * @param string $type_groups 			A specific group or null/blank to return all types
	 * 											Image, icon, text/document, web, geological, audio/video, generic, custom
	 *											For multiple groups, either pass an array of types or comma-separate them
	 * @return string
	 */
	public function getMediaTypesMenu($values, $type_groups = array()){
		$html = '';
		if(!is_array($values)) $values = explode(",", $values);
		$mt = $this->getMediaTypes($type_groups);
		foreach($mt as $group => $types){
			if($html != '') $html .= '</optgroup>';
			$html .= '<optgroup label="'.ucwords($group).'">';
			foreach($types as $type){
				$sel = ((in_array($type, $values)) ? ' selected="selected"' : '');
				$html .= '<option value="'.$type.'"'.$sel.'>'.$type.'</option>';
			}
		}
		return $html;
	}

	/**
	 * Return the maximum file upload limit, based on file type, from a specific media format
	 * @param string $format 					Code of format
	 * @param string $type 						MT_IMAGE or MT_ICON for image files, any other for non-image files
	 * @return integer 							The lesser of the format size limit or system size limit
	 */
	public function getFileSizeLimit($format, $type){
		global $_filesys;

		if($type == MT_IMAGE || $type == MT_ICON)
			$sizelimit = IMG_MAX_UPLOAD_SIZE;
		else
			$sizelimit = FILE_MAX_UPLOAD_SIZE;
		$sizelimit = $_filesys->evaluateFileSize($sizelimit);
		if(!empty($format)){
			$format = strtolower($format);
			$rec = $this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setWhere("`code` = '".$format."'")->getRec();
			if(!empty($rec)){
				$sizelimit2 = $_filesys->evaluateFileSize($rec[0]['filelimit']);
				if($sizelimit2 < $sizelimit) $sizelimit = $sizelimit2;
			}
		}
		return $sizelimit;
	}

	/**
	 * Retrieve list of media formats from database
	 * @param array 					array(id, source_type, code)
	 * 										source_type must be one of 'system', 'plugin', or 'theme'
	 * @return array
	 */
	public function getFormats($params = array()){
		if(isset($params['id']) && intval($params['id']) <= 0) unset($params['id']);
		if(isset($params['source_type']) && !in_array($params['source_type'], array('system', 'plugin', 'theme'))) unset($params['source_type']);
		if(isset($params['code']) && is_null($params['code'])) unset($params['code']);
		$crit = "mf.source_type != 'backup'";
		foreach($params as $field => $val){
			$crit .= " AND mf.".$field." = '".$val."'";
		}
		$joinarray = array(array("type" => "LEFT JOIN", "table" => MEDIA_FORMATS_TABLE." mf1", "comp" => "mf.id = mf1.ref_id"));
		$retn = $this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE." mf")->setFields(array("mf.*", "mf1.id AS backup_id"))->setJoins($joinarray)->setWhere($crit)->getRecJoin();
		return $retn;
	}

	/**
	 * Change the active state of a media format
	 * @param integer $id 				ID of media format
	 * @param integer|boolean $state 	The new state of the media format or null to switch state
	 * @param array 					array($success, $state)
	 */
	public function setMediaFormatActiveState($id, $state = null){
		global $_events;

		$success = false;
		$id = intval($id);
		if($id > 0){
			if(is_null($state)){
				$state = $this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setFields("active")->setWhere("id = '".$id."'")->getRecItem();
				$state = abs(!((bool) $state));
			}

			if($this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setFieldVals(array("active" => $state))->setWhere("id = '".$id."'")->updateRec()){
	            $_events->processTriggerEvent(__FUNCTION__);
	            $success = true;
			}
		}
		return array("success" => $success, "state" => $state);
	}

	/**
	 * Override or restore a saved media format.  If overriding, a backup of the selected media format will be made.
	 * @param integer $id 				ID of the media format
	 * @param integer|boolean $override Set to true to override, false to restore, or null to switch state.
	 * @return boolean
	 */
	public function overrideMediaFormat($id, $override = null){
		global $_events;

		$id = intval($id);
		if($id == 0) return array("success" => false);
		$rec = $this->getFormats(array("id" => $id));
		$backup_id = ((isset($rec[0]['backup_id'])) ? $rec[0]['backup_id'] : 0);
		if(is_null($override)){
			$override = ($backup_id == 0);  // no backup yet created
		}

		unset($rec[0]['id']);
		unset($rec[0]['backup_id']);
		$rec[0]['source_type'] = 'backup';
		$rec[0]['ref_id'] = $id;

        $_events->processTriggerEvent(__FUNCTION__);
		if($override){
			// create/update backup
			if($backup_id == 0){
				$this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setFieldVals($rec[0])->insertRec();
			}else{
				$this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setFieldVals($rec[0])->setWhere("ref_id = '".$id."'")->updateRec();
			}
		}else{
			// delete backup
			$this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setWhere("ref_id = '".$id."'")->deleteRec();
		}

		return true;
	}

	/**
	 * Renames a media format
	 * @param integer $id 				The ID of the media format
	 * @param string $name 				The new name
	 * @return array 					array(new name, error)
	 */
	public function renameMediaFormat($id, $name){
		global $_events;

		$id = intval($id);
		$name = strtolower(trim($name));
		$result = null;
		$error = null;
		if($id == 0){
			$result = ucwords($name);
		}else{
			if($name != ''){
				$numrows = $this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setWhere("`code` = '".$name."' AND `id` != '".$id."' AND `source_type` != 'backup'")->getRecNumRows();
				if($numrows == 0){
					$oldcode = $this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setFields("code")->setWhere("`id` = '$id'")->getRecItem();
					if($oldcode != $name){
						$_events->processTriggerEvent(__FUNCTION__);
						$this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setFieldVals(array("code" => $name, "name" => ucwords($name)))->setWhere("`code` = '".$oldcode."'")->updateRec();
						$result = ucwords($name);
					}
				}else{
					$error = 'The media format title must be unique.';
				}
			}else{
				$error = 'The media format title cannot be blank.';
			}
		}
		return array($result, $error);
	}

	/**
	 * Delete a media format
	 * @param integer $id 				The ID of the media format
	 * @return boolean
	 */
	public function deleteMediaFormat($id){
		global $_events;

		$id = intval($id);
		if($id == 0) return array("success" => false);

        $_events->processTriggerEvent(__FUNCTION__);
		return ($this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setWhere("id = '".$id."' OR ref_id = '".$id."'")->deleteRec());
	}

	/**
	 * Returns the media format action types
	 * @return array
	 */
	public function getMediaFormatActionTypes(){
		$retn = $this->_collect->_db_control->setTable(MEDIA_FORMATS_ACTIONS_TABLE)->setWhere("active = 1")->getRec();
		return $retn;
	}

	/**
	 * Returns the media format action attributes
	 * @param string $val 				Action code
	 * @return string 					Options in JSON format
	 */
	public function getMediaFormatActionAttributes($val){
		$retn = $this->_collect->_db_control->setTable(MEDIA_FORMATS_ACTIONS_TABLE)->setFields("options")->setWhere("code = '$val' AND active = 1")->getRecItem();
		if(!empty($retn)) $retn = json_decode($retn, true);
		return $retn;
	}

	/**
	 * Removes a media format action from a media format
	 * @param string $id_action 		A colon-separated pair containing the id of the media format and action
	 * @return boolean
	 */
	public function deleteMediaFormatAction($id_action){
		global $_events;

		$id_action = explode(":", $id_action);
		if(count($id_action) < 2) return array("success" => false);

		$ok = false;
		$actions = $this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setWhere("id = '".$id_action[0]."'")->setFields("actions")->getRecItem();
		if($actions){
			$actions = json_decode($actions, true);
			if(isset($actions[$id_action[1]])) unset($actions[$id_action[1]]);
			$actions = json_encode($actions);
			$_events->processTriggerEvent(__FUNCTION__);
			$this->_collect->_db_control->setTable(MEDIA_FORMATS_TABLE)->setWhere("id = '".$id_action[0]."'")->setFieldvals(array("actions" => $actions))->updateRec();
			$ok = true;
		}
		return $ok;
	}

	/**
	 * Return flexible thumbnail URL
	 * @param string $src 	Path to full size image
	 * @param integer $width
	 * @param integer $height
	 * @param array $attr
	 * @return string
	 */
	public function getFlexThumbnail($src, $width = 0, $height = 0, $attr = null){
		if($src != ''){
			if($width < 0) $width = 0;
			if($height < 0) $height = 0;
	        if(!preg_match("/^((http|https):\/\/|\\".DIRECTORY_SEPARATOR.")/", $src)) $src = "/".$src;
			$path = WEB_URL.THUMB_GEN_ALIAS."?src=".$src;
			if($width > 0) $path .= "&w=".$width;
			if($height > 0) $path .= "&h=".$height;
			return $path;
		}else{
			return false;
		}
	}

	/**
	 * Output flexible thumbnail IMG tag
	 * @param string $src 	Path to full size image
	 * @param integer $width
	 * @param integer $height
	 * @param array $attr
	 * @return string
	 */
	public function showFlexThumbnail($src, $width = 0, $height = 0, $attr = null){
		$path = $this->getFlexThumbnail($src, $width, $height, $attr);
		if($path !== false) {
			$obj = '<img src="'.$path.'"';
			if(isset($attr['id'])) $obj .= ' id="'.$attr['id'].'"';
			if(isset($attr['class'])) $obj .= ' class="'.$attr['class'].'"';
			$obj.= ' />'.PHP_EOL;
			echo $obj;
		}
	}

	/**
	 * Return an array containing all login images
	 * @return array
	 */
	public function getLoginImageSet(){
		if(IMG_LOGIN_LOGOS != ''){
			$imgs = explode(",", IMG_LOGIN_LOGOS);
	        $imgtype = DEF_IMG_LOGO_CUSTOM;
	    }elseif(IMG_LOGIN_LOGO){
	        $imgs = array(IMG_LOGIN_LOGO);
	        $imgtype = DEF_IMG_LOGO_CUSTOM;
		}else{
	        $imgs = array(ADMIN_FOLDER.CORE_FOLDER.IMG_UPLOAD_FOLDER.strtolower(SYS_NAME)."_std_logo_2.png");
	        $imgtype = DEF_IMG_LOGO_SYS;
	    }
		return array($imgs, $imgtype);
	}

	/**
	 * Return a random login image
	 * @return string 			Image path or blank string
	 */
	public function getLoginImage(){
		$imgs = $this->getLoginImageSet();
		$k = array_rand($imgs[0]);
		return array($imgs[0][$k], $imgs[1]);
	}

	public function __get($name){
		$v = null;
		if(array_key_exists($name, $this->_keys)){
			// return scalar value
			$v = $this->_keys[$name];
		}elseif(array_key_exists($name, $this->_subkeys)){
			// return subarray value
			$v = $this->_subkeys[$name];
		}else{
			trigger_error("Cannot get '$name'.  It is not a valid _MEDIA property. ");
		}
		return $v;
	}

	public function __set($name, $value){
		if(array_key_exists($name, $this->_keys)){
			// set scalar value
			$this->_keys[$name] = $value;
		}elseif(array_key_exists($name, $this->_subkeys)){
			// set subarray value
			$this->_subkeys[$name][] = $value;
		}else{
			trigger_error("Cannot set '$name'.  It is not a valid _MEDIA property. ");
		}
	}
}

?>