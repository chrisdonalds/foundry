<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("USERSLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

// Admin user levels
define ("USERTYPE_PREFIX", "ADMLEVEL_");
define (USERTYPE_PREFIX."DEVELOPER", 0);
define (USERTYPE_PREFIX."SYSADMIN", 1);
define (USERTYPE_PREFIX."OWNER", 2);
define (USERTYPE_PREFIX."USER", 4);
define (USERTYPE_PREFIX."AUTHOR", 8);
define (USERTYPE_PREFIX."GUEST", 16);

// Allowances
// - Users
define("UA_VIEW_USERS", "view_users");
define("UA_CREATE_USER", "create_user");
define("UA_CREATE_LOWER_USER", "create_lower_user");
define("UA_EDIT_USER", "edit_user");
define("UA_EDIT_PROFILE", "edit_profile");
define("UA_DELETE_USER", "delete_user");
define("UA_DELETE_LOWER_USER", "delete_lower_user");
define("UA_ACTIVATE_USER", "activate_user");
define("UA_ACTIVATE_LOWER_USER", "activate_lower_user");
define("UA_BAN_USER", "ban_user");
define("UA_BAN_LOWER_USER", "ban_lower_user");

// - Themes
define("UA_VIEW_THEMES", "view_themes");
define("UA_INSTALL_WEBSITE_THEME", "install_website_theme");
define("UA_INSTALL_ADMIN_THEME", "install_admin_theme");
define("UA_EDIT_WEBSITE_THEME", "edit_website_theme");
define("UA_EDIT_ADMIN_THEME", "edit_admin_theme");
define("UA_DELETE_WEBSITE_THEME", "delete_website_theme");
define("UA_DELETE_ADMIN_THEME", "delete_admin_theme");
define("UA_ACTIVATE_WEBSITE_THEME", "activate_website_theme");
define("UA_ACTIVATE_ADMIN_THEME", "activate_admin_theme");

// - Menus/Tabs
define("UA_VIEW_MENU_SETTINGS", "view_menu_settings");
define("UA_VIEW_CUSTOM_SETTINGS_TABS", "view_custom_settings_tabs");
define("UA_EDIT_WEBSITE_MENUS", "edit_website_menus");
define("UA_EDIT_ADMIN_MENUS", "edit_admin_menus");
define("UA_VIEW_LOCKED_MENUS", "view_locked_menus");

// - Plugins
define("UA_VIEW_PLUGINS", "view_plugins");
define("UA_INSTALL_PLUGINS", "install_plugins");
define("UA_UPDATE_PLUGINS", "update_plugins");
define("UA_REPAIR_PLUGINS", "repair_plugins");
define("UA_DELETE_PLUGINS", "delete_plugins");
define("UA_ACTIVATE_PLUGINS", "activate_plugins");

// - Editors
define("UA_VIEW_EDITOR_SETTINGS", "view_editor_settings");
define("UA_INSTALL_EDITORS", "install_editors");
define("UA_UPDATE_EDITORS", "update_editors");
define("UA_DELETE_EDITORS", "delete_editors");
define("UA_ACTIVATE_EDITORS", "activate_editors");
define("UA_EDIT_EDITOR_SETTINGS", "edit_editor_settings");

// - Frameworks
define("UA_ACTIVATE_FRAMEWORKS", "activate_frameworks");

// - Media
define("UA_VIEW_MEDIA_SETTINGS", "view_media_settings");
define("UA_EDIT_MEDIA_SETTINGS", "edit_media_settings");

// - Taxonomies
define("UA_CREATE_TAXONOMIES", "create_taxonomies");

// - General
define("UA_VIEW_GENERAL_SETTINGS", "view_general_settings");
define("UA_EDIT_GENERAL_SETTINGS", "edit_general_settings");

// - Database
define("UA_MANAGE_DATABASE", "manage_database");

// - Advanced
define("UA_VIEW_ADVANCED_SETTINGS", "view_advanced_settings");
define("UA_EDIT_ADVANCED_SETTINGS", "edit_advanced_settings");
define("UA_MANAGE_DATA_TYPES", "manage_aliases");
define("UA_MANAGE_DATA_ALIASES", "manage_aliases");
define("UA_MANAGE_ALIASES", "manage_aliases");
define("UA_MANAGE_SEO", "manage_seo");
define("UA_MANAGE_VISIBILITY", "manage_visibility");
define("UA_MANAGE_ERROR_HANDLER", "manage_error_handler");
define("UA_VIEW_ERRORS", "view_errors");
define("UA_MANAGE_REPORTS", "manage_reports");
define("UA_MANAGE_USER_SYSTEM", "manage_user_system");
define("UA_MANAGE_CORE", "manage_core_components");
define("UA_MANAGE_FILE_MAIL_SYS", "manage_file/mail_system");
define("UA_MANAGE_FILE_SYS", "manage_file_system");
define("UA_MANAGE_MAIL_SYS", "manage_mail_system");
define("UA_MANAGE_CRON_SYS", "manage_cron_system");
define("UA_VIEW_REPORTS", "view_reports");

// - Records and Files
define("UA_UPLOAD_FILES", "upload_files");
define("UA_VIEW_LIST", "view_list");
define("UA_EDIT_RECORD", "edit_record");
define("UA_ADD_RECORD", "add_record");
define("UA_DELETE_RECORD", "delete_record");
define("UA_RENAME_RECORD", "rename_record");
define("UA_VIEW_RECORD", "view_record");
define("UA_PUBLISH_RECORD", "publish_record");
define("UA_ACTIVATE_RECORD", "activate_record");
define("UA_CLONE_RECORD", "clone_record");
define("UA_ORGANIZE_RECORDS", "organize_records");
define("UA_EXPORT_RECORDS", "export_records");
define("UA_SEND_EMAILS", "send_emails");

// - Pages
define("UA_VIEW_PAGES_LIST", "view_pages_list");
define("UA_EDIT_PAGE", "edit_page");
define("UA_ADD_PAGE", "add_page");
define("UA_DELETE_PAGE", "delete_page");
define("UA_RENAME_PAGE", "rename_page");
define("UA_VIEW_PAGE", "view_page");
define("UA_PUBLISH_PAGE", "publish_page");
define("UA_ACTIVATE_PAGE", "activate_page");
define("UA_CLONE_PAGE", "clone_page");
define("UA_EDIT_PAGE_META", "edit_page_meta");
define("UA_VIEW_LOCKED_PAGES", "view_locked_pages");

/**
 * USERS CLASS
 * Processes all user login, allowance, and role functions for the system
 */
class UsersClass {
	// overloaded data
	private $_keys = array(	"activelist" => array(), "types" => array(), "allowances" => array(),
							"allowances_custom" => array(), "allowance_groups" => array(),
							"isloggedin" => false, "logintimestamp" => "",
							"id" => 0, "level" => 0, "isactivated" => false, "iphash" => "",
							"username" => "", "firstname" => "", "lastname" => "",
							"email" => "", "twitter" => "", "googleplus" => "",
							"facebook" => "", "image" => "", "thumb" => "", "avatar" => "");
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$s->_keys['types'] = getConsts(USERTYPE_PREFIX);
		$_events->processTriggerEvent(__FUNCTION__.'_users');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Find a user account record by username/password pair,
	 * or other supported social network identity
	 * @param string $user
	 * @param string $pwd
	 * @internal Core function
	 */
	function getAccountbyLogin($user, $pwd){
		global $_sec, $_events;

	    // first try internal username/passwords
	    $acct = $_events->processQuickTriggerEvent(__FUNCTION__, array($user, $pwd), false);
	    if($acct !== false) return $acct;

	    $acct = $_sec->checkPassword($user, $pwd, false);
	    if(!is_null($acct)) return $acct;

	    $accts = $this->_db->setTable(ACCOUNTS_TABLE)->getRec();
	    if(FACEBOOKLOGIN){
	        // try asking Facebook
	        $url = 'https://login.facebook.com/login.php?m&next=http%3A%2F%2Fm.facebook.com%2Feditprofile.php';
	        $fields = 'email='.urlencode($user).'&pass='.urlencode($pwd).'&login=Login';
	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $url);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_HEADER, 0);
	        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
	        $v = strtolower(curl_exec($ch));
	        curl_close($ch);

	        if((strpos($v, "your password was incorrect") === false || strpos($v, "you used an old password") === false) && count($accts) > 0){
	            foreach($accts as $acct){
	                if($acct['facebook_link'] != ''){
	                    if(strpos($v, "facebook.com/".strtolower($acct['facebook_link'])) !== false){
	                        return $acct;
	                    }
	                }
	            }
	        }
	    }
	}

	function getAccountByField($field, $data){
	    $acct = $this->_db->setTable(ACCOUNTS_TABLE)->setWhere("`$field` = '$data'")->setLimit()->getRec();
	    if(count($acct) == 1)
	        return $acct[0];
	    else
	        return false;
	}

	/**
	 * Complete the login process and return to the calling page
	 * @param object $acct          User account object (use getAccountbyLogin() to test login)
	 * @param string $iser          The username
	 */
	function completeLogin($acct, $user, $rurl){
		global $_filesys, $_events;

	    // trigger
	    list($retn, ) = $_events->processTriggerEvent(__FUNCTION__, $acct['id'], true);
	    if($acct['activated'] == 1 && $acct['blocked'] == 0 && $retn === true){
	        // log user into system
	        // set session cookie for a maximum life of 24 hours (the session timer may expire it sooner)
	        $_SESSION['admlogin'] = true;
	        $_SESSION['admuserid'] = $acct['id'];
	        $_SESSION['admuserlevel'] = $acct['level'];
	        setcookie('admlogin', date("Y-m-d H:i:s"), time()+60*60*24, '/');

	        // set user persistence data
	        $_SESSION['timestamp'] = time();
	        $_SESSION['userdata'] = $acct;
	        $remoteiphash = $this->getClientIPHash();
	        $this->_db->setTable(SESSION_TABLE)->setFieldvals(array("user_id" => $acct['id'], "ip_hash" => $remoteiphash, "username" => $user, "section" => "admin", "logged_in" => 1, "logged_in_date" => date("Y-m-d H:i:s")))->setWhere("`ip_hash` = '$remoteiphash'")->replaceRec();

	        setcookie("admuser", getRequestVar('admuser'), time() + 60*60*24*365);
	        setcookie("admpwd", getRequestVar('admpwd'), time() + 60*60*24*365);

	        // rebuild admin url that got us here
	        $rurl = ltrim($rurl, "/");
	        $row_id = intval(getRequestVar('row_id'));
	        $cat_id = intval(getRequestVar('cat_id'));
	        if($row_id > 0) $rurl .= '?row_id='.$row_id;

	        // return to calling page
	        $_filesys->gotoPage (WEB_URL.$rurl);
	        exit;
	    }else{
	        if($acct['blocked'] != 0){
	            $msg = "Your account is blocked.  Please contact the system administrator.";
	        }elseif($acct['activated'] != 1){
	            $msg = "Your account has not been activated.  Please contact the system administrator.";
	        }else{
	            $msg = "Your account is not able to login.  Please contact the system administrator.";
	        }
	        return $msg;
	    }
	}

	/**
	 * Log a specific user out of system
	 * @param integer $id               User ID (0 to logout current user)
	 * @return boolean
	 */
	function terminateLogin($id = 0){
		global $_events;

	    $id = intval($id);

	    // trigger
	    list($retn, ) = $_events->processTriggerEvent(__FUNCTION__, $id, true);

	    if($retn == true){
	        if($id == 0){
	            if($this->_db->setTable(SESSION_TABLE)->setWhere("`ip_hash` = '".$this->_keys['iphash']."' AND `section` = 'admin'")->deleteRec()){
	                unset($_SESSION['admlogin']);
	                $this->_keys['isloggedin'] = false;
	                setcookie('admlogin', '', time()+3600*24*(-100), '/');
	                return true;
	            }
	        }else{
	            if($this->_db->setTable(SESSION_TABLE)->setWhere("`user_id` = '$id' AND `section` = 'admin'")->deleteRec()){
	                return true;
	            }
	        }
	    }
	    return false;
	}

	/**
	 * Check if user (IP) is logged in
	 * @param integer $user_id      The ID of the user to query, if provided, or the log in state of the user viewing the page.
	 * @return boolean
	 */
	function isUserLoggedin($user_id = 0) {
	    if($user_id == 0){
	    	$remoteiphash = $this->getClientIPHash();
	        $id = $this->_db->setTable(SESSION_TABLE)->setFields("id")->setWhere("ip_hash = '$remoteiphash' AND logged_in = 1 AND section = 'admin'")->getRecItem();
	    	return (intval($id) > 0);
	    }elseif(is_numeric($user_id)){
	        $id = $this->_db->setTable(SESSION_TABLE)->setFields("id")->setWhere("user_id = '$user_id' AND logged_in = 1 AND section = 'admin'")->getRecItem();
	        return (intval($id) > 0);
	    }
	}

	/**
	 * Return whether or not user is currently in the process of logging out
	 * @return boolean
	 */
	function isLoggingOut(){
	    return (getRequestVar('admsubmit') == 'Logout');
	}

	/**
	 * Return the number of times a remote user attempted to login to the admin system since $ago
	 * @param string $ago       Number of time units to look back (default = "-1 day")
	 * @return integer
	 */
	function getLoginAttempts($ago = "-5 minutes"){
	    $datetime_ago = date("Y-m-d H:i:s", strtotime($ago));
	    $datetime_now = date("Y-m-d H:i:s");
	    $remoteiphash = $this->getClientIP();
	    $rows = $this->_db->setTable(EVENT_LOG_TABLE)->setWhere("`ip_address` = '".$remoteiphash."' AND `date` >= '".$datetime_ago."' AND `date` < '".$datetime_now."' AND `source` = 'Core' AND `category` = 'Login'")->getRecNumRows();
	    return $rows;
	}

	/**
	 * Return the maximum login attempts allowed by the system or overridden by related trigger function
	 * @return integer
	 */
	function getMaxLoginAttemptsAllowed(){
		global $_events;

		list($max) = $_events->processTriggerEvent(__FUNCTION__, MAX_LOGIN_TRIES, MAX_LOGIN_TRIES);				// alert triggered functions when function executes
		return $max;
	}

	/**
	 * Purge any login session data if time is beyond specific days, hours, minutes, or seconds since last login time
	 * @param integer $days
	 * @param integer $hrs
	 * @param integer $mins
	 * @param integer $secs
	 * @internal Core function
	 */
	function purgeLoginSessions($days, $hrs, $mins, $secs) {
	    global $_users;

	    $this->_db->setTable(SESSION_TABLE)->setWhere("logged_in_date < '".date("Y-m-d H:i:s", mktime(date("H")-$hrs, date("i")-$mins, date("s")-$secs, date("m"), date("d")-$days, date("Y")))."'")->deleteRec();
	    if (!$this->isUserLoggedin()) {
	        unset($_SESSION['admlogin']);
	        unset($_SESSION['admuserid']);
	        unset($_SESSION['admuserlevel']);
	        $this->_keys['isloggedin'] = false;
	    }
	}

	/**
	 * Return whether or not the website visitor is actively logged into Private-Viewing mode
	 * @internal
	 * @return boolean
	 */
	function isPrivatelyLoggedin(){
	    if(getBooleanIfSet($_SESSION['pvtlogin'])) {
	        return true;
	    }else{
	        return false;
	    }
	}

	/**
	 * Try to authenticate visitor for Private-Viewing mode
	 * @param string $password
	 * @internal
	 * @return boolean
	 */
	function getPrivateLogin($password){
		global $_sec;

	    $phash = $_sec->createHash('password', true);
	    // $sec = new SecurityClass(8, true);
	    return $_sec->checkPassword($password, $phash);
	}

	/**
	 * Return the IP address of the remote client (HTTP, HTTP_X, HTTP_FORWARDED, REMOTE_ADDR)
	 * @return string
	 */
	function getClientIP() {
	    $ipaddress = '';
	    if (function_exists('apache_request_headers'))
			$headers = apache_request_headers();
	    else
	    	$headers = $_SERVER;

	    // checking headers/$_SERVER
	    if (array_key_exists('X-Forwarded-For', $headers) && filter_var($headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
	    	$ipaddress = $headers['X-Forwarded-For'];
	    else if (array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers) && filter_var($headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
	    	$ipaddress = $headers['HTTP_X_FORWARDED_FOR'];

	    // checking environment variables
	    else if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	        $ipaddress = getenv('HTTP_FORWARDED');

	    // using default
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';

	    return $ipaddress;
	}

	/**
	 * Return hash key of remote client IP including user agent
	 * @return string;
	 */
	function getClientIPHash(){
		global $_sec;

	    $ip = $this->getClientIP();
	    $ua = getIfSet($_SERVER['HTTP_USER_AGENT']);
	    return $_sec->createUniqueID($ip.$ua);
	}

	/**
	 * Return number of concurrently logged-in users
	 * @param integer $id                   User ID to check (if 0 will exclude current user from count)
	 * @return integer
	 */
	function getConcurrentUserLogins($id = 0){
	    $id = intval($id);

	    if($id == 0){
	        // return number of logged-in users other than current one
	        $remoteiphash = $this->getClientIPHash();
	        $id = $this->_keys['id'];
	        return $this->_db->setTable(SESSION_TABLE)->setFields("id")->setWhere("`user_id` = '".$id."' AND `ip_hash` != '".$remoteiphash."'")->getRecNumRows();
	    }else{
	        return $this->_db->setTable(SESSION_TABLE)->setFields("id")->setWhere("`user_id` = '".$id."'")->getRecNumRows();
	    }
	}

	/**
	 * Post an error alerting logged-in user if anyone else has logged-in with their account
	 * @return integer
	 */
	function getConcurrentLoginsUsingAcct(){
		global $_error;

		$numotherlogins = 0;
        if($this->isloggedin && !$this->isLoggingOut()){
            $numotherlogins = $this->getConcurrentUserLogins();
            if($numotherlogins > 0) $_error->addErrorMsg($numotherlogins." other user".(($numotherlogins == 1) ? ' is' : 's are')." currently logged in with your account.");
        }
        return $numotherlogins;
	}

	/**
	 * Give the currently logged in user editorial control of a specific data record
	 * @param string $data_type
	 * @param integer $data_id
	 * @return boolean
	 */
	function giveCurrentUserDataControl($data_type, $data_id){
		global $_db_control;

		$ok = false;
		$data_id = intval($data_id);
		if(!empty($data_type) && $data_id > 0) {
			if($_db_control->setTable($data_type)->setFieldvals(array("user_editing" => $this->_keys['id']))->setWhere("data_type = '".$data_type."' AND data_id = '".$data_id."'")->updateRec()) {
				$ok = true;
			}
		}
		return $ok;
	}

	// USERS

	/**
	 * Return array containing all users registered in system
	 * @return array
	 */
	function getUsers(){
		return $this->_keys['activelist'];
	}

	/**
	 * Return logged-in user's level
	 * @return int
	 */
	function getUserLevel(){
	    return $this->_keys['level'];
	}

	/**
	 * Return logged-in (no parameters) or any user's account ID
	 * @param string $user_find_using       'id', 'username' or 'email'
	 * @param string|integer $user_find_var Value that is used to find the user record
	 * @return int
	 */
	function getUserID($user_find_using = "", $user_find_var = ""){
	    if(empty($user_find_using) || empty($user_find_var)){
	        return $this->_keys['id'];
	    }else{
	        $user_query = null;
	        switch(strtolower($user_find_using)){
	            case 'id':
	                $user_query = "id = '".intval($user_find_var)."' ";
	                break;
	            case 'username':
	            case 'user name':
	            case 'user':
	                $user_query = "LOWER(username) = '".strtolower($user_find_var)."' ";
	                break;
	            case 'email':
	                $user_query = "LOWER(email) = '".strtolower($user_find_var)."' ";
	                break;
	        }

	        if(!empty($user_query)){
	            return intval($this->_db->setTable(ACCOUNTS_TABLE)->setFields("id")->setWhere($user_query)->getRecItem());
	        }else{
	            return false;
	        }
	    }
	}

	/**
	 * Return specific info about the logged-in user (id = 0) or any other user (id > 0)
	 * @param int $user_id              0 for currently logged-in user, >0 for any other user
	 * @param string $item              username, email, firstname, lastname, image (not gravatar),
	 *                                  googleplus, facebook, twitter, lastlogin
	 * @return string|object            returns string if $item is one of the valid parameters, or entire current user object
	 */
	function getUserInfo($user_id = 0, $item = ''){
	    $u = (array) $this->_keys;       	// convert _users object to array
	    $user_id = intval($user_id);
	    if($user_id > 0){
	        $a = $this->_keys['activelist'];
	        $found = false;
	        foreach($a as $data){
	            if($data['id'] == $user_id){
	                $u = $data;
	                $u['facebook'] = $u['facebook_link'];
	                $u['googleplus'] = $u['google_plus_link'];
	                $u['twitter'] = $u['twitter_link'];
	                $u['logintimestamp'] = (($user_id == $this->_keys['id']) ? $this->_keys['logintimestamp'] : null);
	                $found = true;
	            }
	        }
	        if(!$found) return false;
	    }

	    switch($item){
	        case 'username':
	            return $u['username'];
	            break;
	        case 'email':
	            return $u['email'];
	            break;
	        case 'firstname':
	            return $u['firstname'];
	            break;
	        case 'lastname':
	            return $u['lastname'];
	            break;
	        case 'image':
	            return $u['image'];
	            break;
	        case 'avatar':
	        case 'gravatar':
	            return $u['avatar'];
	            break;
	        case 'googleplus':
	            return $u['googleplus'];
	            break;
	        case 'facebook':
	            return $u['facebook'];
	            break;
	        case 'twitter':
	            return $u['twitter'];
	            break;
	        case 'lastlogin':
	            return $u['logintimestamp'];
	            break;
	        default:
	            return $u;
	            break;
	    }
	}

	/**
	 * Update a specific user's data
	 * @param string $user_find_using       'id', 'username' or 'email'
	 * @param string|integer $user_find_var Value that is used to find the user record
	 * @param array                         Array of fields and data to update
	 *                                      'username', 'password', 'level', 'email', 'firstname',
	 *                                      'lastname', 'twitter_link', 'google_plus_link', 'facebook_link',
	 *                                      'activated', 'blocked'
	 * @return
	 */
	function setUserInfo($user_find_using, $user_find_var, $user_data){
		global $_error, $_sec;

	    $id = $this->getUserID($user_find_using, $user_find_var);
	    if($id > 0) {
	        if(is_array($user_data)){
	            $user_levels = $this->getAllUserTypes();
	            $fieldset = array();
	            foreach($user_data as $field => $data){
	                $field = strtolower(str_replace(array(" ", "_"), "", $field));

	                switch(strtolower($field)){
	                    case 'username':
	                        if(!empty($data))
	                            if($this->_db->setTable(ACCOUNTS_TABLE)->setFields("username")->setWhere("username = '$data' and id != $id")->getRecItem() == "")
	                                $fieldset['username'] = $data;
	                        break;
	                    case 'password':
	                        if(!empty($data)){
	                            $fieldset['password'] = $_sec->createHash($data, false);
	                            $fieldset['phash'] = '';
	                            $fieldset['pcle'] = md5($this->getUserInfo($id, 'username').$data);
	                        }
	                        break;
	                    case 'level':
	                        $data = intval($data);
	                        if(in_array($data, $user_levels))
	                            $fieldset['level'] = $data;
	                        break;
	                    case 'email':
	                        if(validateEmail($data))
	                            $fieldset['email'] = $data;
	                        break;
	                    case 'firstname':
	                        if(!empty($data))
	                            $fieldset['firstname'] = $data;
	                        break;
	                    case 'lastname':
	                        if(!empty($data))
	                            $fieldset['lastname'] = $data;
	                        break;
	                    case 'twitterlink':
	                    case 'twitter':
	                        $fieldset['twitter_link'] = $data;
	                        break;
	                    case 'googlepluslink':
	                    case 'googlelink':
	                    case 'google':
	                        $fieldset['google_plus_link'] = $data;
	                        break;
	                    case 'facebooklink':
	                    case 'facebook':
	                        $fieldset['facebook_link'] = $data;
	                        break;
	                    case 'activated':
	                        $fieldset['activated'] = ((bool) $data) ? 1 : 0;
	                        break;
	                    case 'blocked':
	                        $fieldset['blocked'] = ((bool) $data) ? 1 : 0;
	                        break;
	                }
	            }

	            if(count($fieldset) > 0){
	                $fieldsetstr = array();
	                foreach($fieldset as $k => $v) $fieldsetstr[$k] = $v;
	                return $this->_db->setTable(ACCOUNTS_TABLE)->setFieldvals($fieldsetstr)->setWhere("id = '$id'")->updateRec();
	            }else{
	                $_error->_e("Nothing to update");
	                return false;
	            }
	        }else{
	            $_error->_e("User data not an array");
	            return false;
	        }
	    }else{
	        $_error->_e("User not found");
	        return false;
	    }
	}

	/**
	 * Check if user is at least at level ADMLEVEL_???
	 * @param int $minlevel
	 * @return boolean
	 */
	function userIsAtleast($minlevel = ADMLEVEL_DEVELOPER){
	    return ($this->_keys['level'] <= $minlevel);
	}

	function isAtLeast($minLevel = ADMLEVEL_DEVELOPER){
		return $this->userIsAtleast($minlevel);
	}

	/**
	 * Return whether or not user can perform or gain access to a particular action or area
	 * (i.e. user's type contains the desired allowance)
	 * (References Allowances and Custom Allowances settings)
	 * @param string $action                An allowance action to check
	 * @return boolean
	 */
	function userIsAllowedTo($action, $test = false){
		$ok = false;
		$a = $this->_keys['allowances'] + $this->_keys['allowances_custom'];
		if($test) {
			// echo $action;
			// printr($a);
		}
		if(isset($a[$action]) && is_array($a[$action])){
			if($this->_keys['level'] > 0){
				$this->_keys['userlevel'] = log($this->_keys['level']) / log(2) + 1;
			}else{
				$this->_keys['userlevel'] = 0;
			}
			if($a[$action][$this->_keys['userlevel']] == 1) $ok = true;
		}
	    return $ok;
	}

	/**
	 * Alias of userIsAllowedTo();
	 * @param string $action                An allowance action to check
	 * @return boolean
	 */
	function isAllowedTo($action){
		return $this->userIsAllowedTo($action);
	}

	/**
	 * Return array containing all allowances for current user
	 * @return array
	 */
	function getUserAllowances(){
	    return getUserTypeAllowances($this->_keys['level']);
	}

	/**
	 * Return array containing all allowances for specific usertype or, if usertype is 0, the usertype of the current user
	 * @param integar $usertype             0 for usertype of currently logged-in user, or specific usertype
	 * @return array
	 */
	function getUserTypeAllowances($usertype = 0){
	    $usertype = intval($usertype);
	    if($usertype == 0) $usertype = $this->_keys['level'];
	    if($usertype > 0){
	        $this->_keys['userlevel'] = log($usertype) / log(2) + 1;
	    }else{
	        $this->_keys['userlevel'] = 0;
	    }

	    $a = $this->_keys['allowances'] + $this->_keys['allowances_custom'];
	    $ula = array();
	    foreach($a as $action => $aa) {
	        if($aa[$this->_keys['userlevel']] == 1) $ula[] = $action;
	    }
	    return $ula;
	}

	/**
	 * Stop further access to the page if user does not have proper allowance
	 * @param string $action An allowance action
	 * @param boolean $is_page
	 * @param boolean $hardstop             End execution if allowance failed
	 */
	function haltIfUserCannot($action, $is_page = true, $hardstop = true){
	    global $_error, $_events;

		if(!$this->userIsAllowedTo($action)){
			$_events->processTriggerEvent(__FUNCTION__, $action);				// alert triggered functions when function executes
			$e = (($is_page) ? ACCESS_PAGE_FAIL : ACCESS_FUNC_FAIL);
			if($hardstop){
				die($e);
			}else{
				$_error->addErrorMsg($e);
			}
		}
	}

	/**
	 * Filter the list page record actions by the user allowances
	 * @param string $class                 'page' or 'record'
	 * @param array $buttonarray            Button actions you want to filter (edit, publish, unpublish, delete, undelete, clone).  Not used on Pages list.
	 * @return array                        buttons, addbut, orgbut, expbut
	 */
	function getUserAllowedListActions($class, $buttonarray = array()){
	    $class = strtolower($class);
	    $arry = array();
	    if($class == 'page'){
	        $edit = (($this->userIsAllowedTo(UA_EDIT_PAGE)) ? DEF_ACTION_EDIT.":: Page" : "");
	        $publish = (($this->userIsAllowedTo(UA_PUBLISH_PAGE)) ? DEF_ACTION_PUBLISH : "");
	        $unpublish = (($this->userIsAllowedTo(UA_PUBLISH_PAGE)) ? DEF_ACTION_UNPUBLISH : "");
	        $delete = (($this->userIsAllowedTo(UA_DELETE_PAGE)) ? DEF_ACTION_DELETE : "");
	        $undelete = (($this->userIsAllowedTo(UA_DELETE_PAGE)) ? DEF_ACTION_UNDELETE : "");
	        $clone = (($this->userIsAllowedTo(UA_CLONE_PAGE)) ? DEF_ACTION_CLONE : "");
	        $view = DEF_ACTION_VIEW;
	        $arry = array (
	            "1" => array (
	                $edit,
	                $publish,
	                $unpublish,
	                $view,
	                $clone,
	                $delete,
	                $undelete
	            ),
	            "2" => array (
	                $edit,
	                $publish,
	                $unpublish,
	                $view,
	                $delete,
	                $undelete
	            ),
	            "3" => array (
	                $edit,
	                $publish,
	                $unpublish,
	                $view,
	                $delete,
	                $undelete
	            )
	        );

	        $addbut = (($this->userIsAllowedTo(UA_ADD_PAGE)) ? DEF_PAGEBUT_ADDNEW : "");
	        $orgbut = "";
	        $expbut = "";
	    }else{
	        $edit = (($this->userIsAllowedTo(UA_EDIT_RECORD) && in_array(DEF_ACTION_EDIT, $buttonarray)) ? DEF_ACTION_EDIT.":: ".ucwordsAdv($class) : "");
	        $publish = (($this->userIsAllowedTo(UA_PUBLISH_RECORD) && in_array(DEF_ACTION_PUBLISH, $buttonarray)) ? DEF_ACTION_PUBLISH : "");
	        $unpublish = (($this->userIsAllowedTo(UA_PUBLISH_RECORD) && in_array(DEF_ACTION_UNPUBLISH, $buttonarray)) ? DEF_ACTION_UNPUBLISH : "");
	        $delete = (($this->userIsAllowedTo(UA_DELETE_RECORD) && in_array(DEF_ACTION_DELETE, $buttonarray)) ? DEF_ACTION_DELETE : "");
	        $undelete = (($this->userIsAllowedTo(UA_DELETE_RECORD) && in_array(DEF_ACTION_UNDELETE, $buttonarray)) ? DEF_ACTION_UNDELETE : "");
	        $clone = (($this->userIsAllowedTo(UA_CLONE_RECORD) && in_array(DEF_ACTION_CLONE, $buttonarray)) ? DEF_ACTION_CLONE : "");
	        $arry = array (
	                $edit,
	                $publish,
	                $unpublish,
	                $clone,
	                $delete,
	                $undelete
	            );

	        $addbut = (($this->userIsAllowedTo(UA_ADD_RECORD)) ? DEF_PAGEBUT_ADDNEW : "");
	        $orgbut = "";
	        $expbut = "";
	    }
	    return array($arry, $addbut);
	}

	/**
	 * Define all custom user types
	 * @internal Core function
	 */
	function defineCustomUserTypes(){
	    $ut = $this->_db->setTable(SETTINGS_TABLE)->setFields("value")->setWhere("`name` = 'CUSTOM_USER_TYPES'")->getRecItem();
	    if($ut != ''){
	        $ut_arry = json_decode($ut, true);
	        foreach($ut_arry as $k => $u){
	            define(USERTYPE_PREFIX.strtoupper($u), $k);
	        }
	        $this->_keys['types'] = getConsts(USERTYPE_PREFIX);
	    }
	}

	/**
	 * Return array of standard user types
	 * @return array  			[type name] = mask value;
	 */
	function getUserTypes(){
	    $ut = getConsts(USERTYPE_PREFIX);
	    $ut_arry = array();
	    foreach($ut as $k => $v){
	        if($v <= ADMLEVEL_GUEST) $ut_arry[str_replace(USERTYPE_PREFIX, "", $k)] = $v;
	    }
	    return $ut_arry;
	}

	/**
	 * Return array of custom user types
	 * @return array
	 */
	function getCustomUserTypes(){
	    $ut = getConsts(USERTYPE_PREFIX);
	    $ut_arry = array();
	    foreach($ut as $k => $v){
	        if($v > ADMLEVEL_GUEST) $ut_arry[str_replace(USERTYPE_PREFIX, "", $k)] = $v;
	    }
	    return $ut_arry;
	}

	function getAllUserTypes(){
	    return $this->getUserTypes() + $this->getCustomUserTypes();
	}

	/**
	 * Return a display-ready version of user type
	 * @param string $type
	 * @return string
	 */
	function getFriendlyUserType($type){
	    return trim(ucwordsAdv(str_replace(array(USERTYPE_PREFIX, "_"), array("", " "), strtoupper($type))));
	}

	/**
	 * Add a new User Type to the Custom User Types object, and optionally set its Allowances
	 * @param string $usertypename          Name of new User Type (must be unique)
	 * @param string $usertypekey           (optional) Codified version of name
	 * @param string $copyAllowancesFrom    (optional) User Type key of the User Type whose Allowances will be adopted
	 * @param array $enabledAllowances      (optional) Array of Allowance keys that will be enabled for this user type
	 * @return boolean                      True if user type is successfully created
	 */
	function createCustomUserType($usertypename, $usertypekey = "", $copyAllowancesFrom = "", $enabledAllowances = array()){
	    $ut_array = $this->getAllUserTypes();
	    $usertypename = trim($usertypename);
	    $usertypekey = trim($usertypekey);

	    if($usertypename == "") return false;
	    if($usertypekey == ""){
	        $usertypekey = trim(strtoupper(preg_replace("/(".USERTYPE_PREFIX."|[^a-z0-9_])/i", "", $usertypename)));
	    }

	    if(array_key_exists($usertypekey, $ut_array)) {
	        return false;
	    }elseif($usertypekey == ""){
	        return false;
	    }

	    // create the user type
	    $ut_key_last = 2 ^ $this->getAllowanceDepth();  // next key bit
	    $ut_array[$ut_key_last] = $usertypekey;
	    if(!defined(USERTYPE_PREFIX.$ut_newname)) define(USERTYPE_PREFIX.$usertypekey, $ut_key_last);

	    // if copying allowances
	    $ua_changed = false;
	    $ua = $this->_keys['allowances'];
	    if($copyAllowancesFrom != ""){
	        $copyAllowancesFrom = USERTYPE_PREFIX.trim(strtoupper($copyAllowancesFrom));
	        if(defined($copyAllowancesFrom)){
	            $ut_key = getBinaryPower(constant($copyAllowancesFrom), true);
	            foreach($ua as $ua_key => $values){
	                $ua[$ua_key][] = $ua[$ut_key];
	            }
	            $ua_changed = true;
	        }
	    }elseif(is_array($enabledAllowances) && count($enabledAllowances) > 0){
	        // enabling specific allowances
	        foreach($ua as $ua_key => $values){
	            if(in_array($ua_key, $enabledAllowances)){
	                $ua[$ua_key][] = 1;
	            }else{
	                $ua[$ua_key][] = 0;
	            }
	        }
	        $ua_changed = true;
	    }

	    if($ua_changed) $this->_keys['allowances'] = $ua;
	    return true;
	}

	/**
	 * Return or output the current user menu HTML
	 * @param boolean $returnhtml              True to return the HTML, false to echo the HTML
	 * @return html if $returnhtml is true
	 */
	function showUserMenu($returnhtml = true){
	    if($returnhtml) ob_start();
	    $avatar = $this->getUserGravatar(0, true, 80, array('id' => 'admuser_avatar'));
	    echo $avatar;
	    echo '<div id="admuser_info">';
	    echo '<p><strong>Welcome, '.$this->getUserInfo(0, 'username').'</strong></p>';
	    echo '<p>'.$this->getUserInfo(0, 'email').'<br/>Login @ '.date(PHP_DATE_FORMAT." g:i a", $this->getUserInfo(0, 'lastlogin')).'</p>';
	    echo '<p><a href="'.WEB_URL.ADMIN_FOLDER.'settings?tab=users&tch=.user_editprofile,0">Edit Profile</a></p>';
	    echo '</div>';
	    echo '<div id="admuser_actions">';
	    echo '<input type="button" id="admuser_logout" value="Log Out" />';
	    echo '<input type="button" id="admuser_close" value="Close" />';
	    echo '</div>';
	    if($returnhtml){
	        $html = ob_get_clean();
	        return $html;
	    }
	}

	// ALLOWANCES

	/**
	 * Initialize User Allowances and return them in JSON format
	 * @internal Core function
	 * @return string
	 */
	function initAllowances(){
		global $_events;

		// these are the default values
		$defaults = '{"view_pages_list":[1,1,1,1,1,1],
			"edit_page":[1,1,1,1,0,0],
			"add_page":[1,1,1,0,0,0],
			"delete_page":[1,1,1,0,0,0],
			"rename_page":[1,1,1,0,0,0],
			"view_page":[1,1,1,1,1,1],
			"publish_page":[1,1,1,0,0,0],
			"activate_page":[1,1,1,0,0,0],
			"clone_page":[1,1,1,0,0,0],
			"edit_page_meta":[1,1,0,0,0,0],
			"view_locked_pages":[1,0,0,0,0,0],
			"edit_advanced_bulk_options":[1,1,1,1,0,0],

			"view_users":[1,1,1,1,1,1],
			"create_user":[1,1,0,0,0,0],
			"create_lower_user":[1,1,1,1,0,0],
			"edit_user":[1,1,0,0,0,0],
			"edit_profile":[1,1,1,1,1,1],
			"delete_user":[1,1,0,0,0,0],
			"delete_lower_user":[1,1,1,1,0,0],
			"activate_user":[1,1,0,0,0,0],
			"activate_lower_user":[1,1,1,1,0,0],
			"ban_user":[1,1,0,0,0,0],
			"ban_lower_user":[1,1,1,1,0,0],

			"view_themes":[1,1,1,1,0,0],
			"install_website_theme":[1,1,0,0,0,0],
			"install_admin_theme":[1,0,0,0,0,0],
			"edit_website_theme":[1,1,1,0,0,0],
			"edit_admin_theme":[1,0,0,0,0,0],
			"delete_website_theme":[1,1,0,0,0,0],
			"delete_admin_theme":[1,0,0,0,0,0],
			"activate_website_theme":[1,1,0,0,0,0],
			"activate_admin_theme":[1,0,0,0,0,0],

			"view_menu_settings":[1,1,0,0,0,0],
	        "view_locked_menus":[1,0,0,0,0,0],
	        "view_custom_settings_tabs":[1,1,1,1,0,0],
			"edit_website_menus":[1,0,0,0,0,0],
			"edit_admin_menus":[1,0,0,0,0,0],

			"view_plugins":[1,1,1,1,0,0],
			"install_plugins":[1,0,0,0,0,0],
			"update_plugins":[1,1,1,0,0,0],
			"repair_plugins":[1,0,0,0,0,0],
			"delete_plugins":[1,0,0,0,0,0],
			"activate_plugins":[1,1,0,0,0,0],
			"activate_frameworks":[1,1,0,0,0,0],

	        "view_editor_settings":[1,1,1,1,0,0],
	        "edit_editor_settings":[1,1,1,0,0,0],
	        "install_editors":[1,0,0,0,0,0],
	        "update_editors":[1,1,1,0,0,0],
	        "delete_editors":[1,0,0,0,0,0],
	        "activate_editors":[1,1,0,0,0,0],

			"view_general_settings":[1,1,1,1,0,0],
			"edit_general_settings":[1,1,1,0,0,0],

	        "view_media_settings":[1,1,1,1,0,0],
	        "edit_media_settings":[1,1,1,0,0,0],

	        "create_taxonomies":[1,1,1,1,0,0],

			"view_advanced_settings":[1,1,0,0,0,0],
			"edit_advanced_settings":[1,1,0,0,0,0],
			"manage_aliases":[1,1,0,0,0,0],
			"manage_user_system":[1,1,0,0,0,0],
			"manage_seo":[1,1,0,0,0,0],
	        "manage_visibility":[1,1,0,0,0,0],
	        "manage_file/mail_system":[1,1,0,0,0,0],
	        "manage_database":[1,0,0,0,0,0],
	        "manage_core_components":[1,0,0,0,0,0],
	        "manage_file_system":[1,0,0,0,0,0],
	        "manage_mail_system":[1,0,0,0,0,0],
	        "manage_cron_system":[1,1,0,0,0,0],
	        "manage_error_handler":[1,1,0,0,0,0],
			"view_errors":[1,1,0,0,0,0],
	        "view_reports":[1,1,0,0,0,0],
	        "manage_reports":[1,1,0,0,0,0],
	        "view_debug_data":[1,1,0,0,0,0],

			"upload_files":[1,1,1,1,1,1],
			"view_list":[1,1,1,1,1,1],
			"edit_record":[1,1,1,1,0,0],
			"add_record":[1,1,1,0,0,0],
			"delete_record":[1,1,1,0,0,0],
			"rename_record":[1,1,1,0,0,0],
			"view_record":[1,1,1,1,1,1],
			"publish_record":[1,1,1,0,0,0],
			"activate_record":[1,1,1,0,0,0],
			"clone_record":[1,1,1,0,0,0],
			"organize_records":[1,1,1,1,0,0],
			"export_records":[1,1,1,0,0,0],
			"send_emails":[1,1,1,0,0,0]}';

		$defaults = preg_replace("/\s*/", "", $defaults);
		$defaults_array = json_decode($defaults, true);

		// get allowances from settings table.  if missing assign defaults
	    $a = $this->_db->setTable(SETTINGS_TABLE)->setFields("value")->setWhere("`name` = 'ALLOWANCES'")->getRecItem();
		if(empty($a)){
			// aa inherits defaults
	        $this->_db->setTable(SETTINGS_TABLE)->setWhere("`name` = 'ALLOWANCES'")->deleteRec();
	        $this->_db->setTable(SETTINGS_TABLE)->setFieldvals(array("name" => "ALLOWANCES", "value" => $defaults, "type" => "str"))->insertRec();
			$aa = $defaults_array;
		}else{
			// aa inherits any missing allowances
			$aa = json_decode($a, true);
			foreach($defaults_array as $key => $allowances){
				if(!isset($aa[$key])) $aa[$key] = $allowances;
			}
	        // save missing allowances to db
			if(json_encode($aa) != $a){
	            $this->_db->setTable(SETTINGS_TABLE)->setFieldvals(array("value" => json_encode($aa)))->setWhere("`name` = 'ALLOWANCES'")->updateRec();
			}
		}

	    // get custom allowances from settings table.  if key is missing, create blank record
	    $a = $this->_db->setTable(SETTINGS_TABLE)->setFields("value")->setWhere("`name` = 'ALLOWANCES_CUSTOM'")->getRecItem();
	    if(empty($a)) {
	        $ca = array(
	            // "c_test1" => array(1,1,1,1,0,0),
	            // "c_test2" => array(1,1,1,0,0,0)
	            );
	        $this->_db->setTable(SETTINGS_TABLE)->setFieldvals(array("name" => "ALLOWANCES_CUSTOM", "value" => json_encode($ca)))->insertRec();
	    }else{
	        $ca = json_decode($a, true);
	    }

	    // store allowances (standard and custom) to class
		$this->_keys['allowances'] = $aa;
	    $this->_keys['allowances_custom'] = $ca;

	    $this->groupAllowances();
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	}

	/**
	 * Organize Allowances ($this->_keys['allowances']) by category
	 */
	function groupAllowances(){
	    // prepare names of the custom allowances
	    $ca = $this->_keys['allowances_custom'];
	    $ca_name = array();
	    if(count($ca) > 0){
	        foreach($ca as $key => $value){
	            $ca_name[] = $this->getAllowanceNameFromKey($key);
	        }
	    }

	    $groups = array(
	        "Pages" => array("View Pages List", "Edit Page", "Add Page", "Delete Page", "Rename Page", "View Page", "Publish Page", "Activate Page", "Clone Page", "Edit Page Meta", "View Locked Pages"),
	        "Custom Data" => array("View List", "Edit Record", "Add Record", "Delete Record", "Rename Record", "View Record", "Publish Record", "Activate Record", "Clone Record", "Organize Records", "Export Records", "Send Emails", "Edit Advanced Bulk Options"),
	        "General Settings" => array("View General Settings", "Edit General Settings"),
	        "Media Settings" => array("View Media Settings", "Edit Media Settings"),
	        "Editor Settings" => array("View Editor Settings", "Edit Editor Settings", "Install Editors", "Update Editors", "Delete Editors", "Activate Editors"),
	        "Theme Settings" => array("View Themes", "Install Website Theme", "Install Admin Theme", "Edit Website Theme", "Edit Admin Theme", "Delete Website Theme", "Delete Admin Theme", "Activate Website Theme", "Activate Admin Theme"),
	        "Menu Settings" => array("View Menu Settings", "View Locked Menus", "Edit Website Menus", "Edit Admin Menus", "View Custom Settings Tabs"),
	        "Taxonomies" => array("Create Taxonomies"),
	        "Plugins" => array("View Plugins", "Install Plugins", "Update Plugins", "Repair Plugins", "Delete Plugins", "Activate Plugins", "Activate Frameworks"),
	        "Users and User Manager" => array("View Users", "Create User", "Create Lower User", "Edit User", "Edit Profile", "Delete User", "Delete Lower User", "Activate User", "Activate Lower User", "Ban User", "Ban Lower User", "Manage User System"),
	        "Advanced Settings" => array("View Advanced Settings", "Edit Advanced Settings", "Manage Aliases", "Manage SEO", "Manage Visibility", "Manage File/Mail System", "Upload Files", "Manage Cron System", "Manage Database", "Manage Error Handler", "View Errors", "View Reports", "Manage Reports", "View Debug Data", "Manage Core Components"),
	        "Custom Allowances" => $ca_name
	    );

	    $this->_keys['allowance_groups'] = $groups;
	}

	/**
	 * Create a new custom allowance with optional values.
	 * @param string $key                       The unique key of the new allowance item
	 * @param array $allowance_values_array     An array list of allowance values (1 or 0) for each User Type
	 *                                          indicating whether related User Type is allowed action
	 * @param boolean $refresh                  Refresh the allowance class
	 * @return string                           Success (blank string) or fail (error string) of addition
	 */
	function addCustomAllowance($key, $allowance_values_array = array(), $refresh = true){
	    global $_users;

	    $ua = $this->_keys['allowances'] + $this->_keys['allowances_custom'];
	    if($key == '') return "`$key` is invalid or cannot be used.";
	    if(!array_key_exists($key, $ua)){
	        // array key is unique
	        foreach($allowance_values_array as $k => $v){
	            $v = (boolean) $v;
	            $allowance_values_array[$k]  = (($v) ? 1 : 0);      // ensure array values are 1 or 0 only
	        }

	        // if array size of new allowance values is less than allowance/user depth
	        // fill remaining elements with zeroes
	        $ua_diff = $this->getAllowanceDepth() - count($allowance_values_array);
	        if($ua_diff > 0){
	            $ua_fill = array_fill(0, $ua_diff, 0);
	            $allowance_values_array = $allowance_values_array + $ua_fill;
	        }

	        // save to database
	        $ca = $this->_keys['allowances_custom'];
	        $ca[$key] = $allowance_values_array;
	        if($this->_db->setTable(SETTINGS_TABLE)->setFieldvals(array("value" => json_encode($ca), "name" => "ALLOWANCES_CUSTOM"))->setWhere("`name` = 'ALLOWANCES_CUSTOM'")->replaceRec() !== false) {
	            if($refresh) $this->initAllowances();
	            return $allowance_values_array;
	        }else{
	            return "Unable to create new custom allowance.";
	        }
	    }else{
	        return "`$key` is not unique.";
	    }
	}

	/**
	 * Delete a Custom Allowance from the system (Standard Allowances cannot be deleted only disabled per User Type).
	 * @param string $key
	 * @param boolean $refresh              Refresh the allowance class
	 * @return boolean                      Success status of deletion
	 */
	function deleteAllowance($key, $refresh = true){
	    global $_users, $_events;

	    $ca = $this->_keys['allowances_custom'];
	    $ok = false;
	    if(array_key_exists($key, $ca)){
	        unset($ca[$key]);
	        $ok = ($this->_db->setTable(SETTINGS_TABLE)->setFieldvals(array("name" => "ALLOWANCES_CUSTOM", "value" => json_encode($ca)))->setWhere("`name` = 'ALLOWANCES_CUSTOM'")->replaceRec() !== false);
	        if($ok && $refresh) $this->initAllowances();
	    }
		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	    return $ok;
	}

	/**
	 * Return an array of allowances for a specific User Type and optional Allowance Group
	 * @param integer $usertype
	 * @param string $group_name
	 */
	function getAllowancesByUserType($usertype, $group_name = ''){
	    global $_users;

	    $ua_retn = array();
	    $ua = $this->_keys['allowances'] + $this->_keys['allowances_custom'];
	    $usertype_indx = getBinaryPower($usertype);
	    if($usertype_indx != intval($usertype_indx)) return false;
	    if($group_name != ''){
	        if(isset($this->_keys['allowance_groups'][$group_name])){
	            $group = $this->_keys['allowance_groups'][$group_name];
	            foreach($group as $ua_name){
	                $ua_key = $this->getAllowanceKeyFromName($ua_name);
	                $ua_retn[$ua_key] = ((isset($ua[$ua_key][$usertype_indx])) ? $ua[$ua_key][$usertype_indx] : 0);
	            }
	        }else{
	            return false;
	        }
	    }else{
	        foreach($ua as $ua_key => $ua_values){
	            $ua_retn[$ua_key] = ((isset($ua_values[$usertype_indx])) ? $ua_values[$usertype_indx] : 0);
	        }
	    }
	    return $ua_retn;
	}

	/**
	 * Return number of user types registered with allowances
	 */
	function getAllowanceDepth(){
	    $ua = $this->_keys['allowances'];
	    reset($ua);
	    return count(current($ua));
	}

	/**
	 * Return key (slug) from name
	 * @param string $name
	 * @return string
	 */
	function getAllowanceKeyFromName($name){
	    return str_replace(" ", "_", strtolower($name));
	}

	/**
	 * Derive and return capitalized name from key (slug)
	 * @param string $key
	 * @return string
	 */
	function getAllowanceNameFromKey($key){
	    return ucwordsAdv(str_replace("_", " ", $key));
	}

	/**
	 * Return the URL or image object for a user's Gavatar
	 * @param integer $user_id              user id to retrieve or 0 for currently logged in user
	 * @param boolean $return_as_img        true to return complete img tag, false to return url
	 * @param integer $size                 size in pixels (default = 80px)
	 * @param array $atts                   optional attributes to add to image tag
	 */
	function getUserGravatar($user_id = 0, $return_as_img = true, $size = 80, $atts = array()){
	    $retn = '';
	    $email = '';
	    $img = '';
	    $user_id = intval($user_id);
	    $size = intval($size);
	    if($size == 0) $size = 80;
	    if($user_id == 0) {
	        // logged in user
	        $rec = $this->_db->setTable(ACCOUNTS_TABLE)->setWhere("id = '".$this->_keys['id']."'")->setLimit()->getRec(true);
	    }else{
	        // another user
	        $rec = $this->_db->setTable(ACCOUNTS_TABLE)->setWhere("id = '$user_id'")->setLimit()->getRec(true);
	    }
	    if(count($rec) > 0){
	        $email = $rec['email'];
	        $img = $rec['image'];
	        $gravatar = $rec['avatar'];
	    }
	    if($email != ''){
	        // email is required
	        if(!empty($img) && file_exists(SITE_PATH.$img)){
	            $retn = WEB_URL.$img;
	        }else{
	            $img = $gravatar;        // if uploaded image not found, use gravatar
	            if(empty($img)) $img = 'mm';             // if gravatar not set, use "no user" gravatar
	            $retn = 'http://www.gravatar.com/avatar/'.md5(strtolower(trim($email))).'?d='.$img.'&s='.$size.'&r=g';
	        }
	        if($return_as_img){
	            $retn = '<img src="'.$retn.'" title="'.$img.'" width="'.$size.'"';
	            if(is_array($atts)){
	                foreach($atts as $k => $v) $retn .= ' '.$k.'="'.$v.'"';
	            }
	            $retn.= ' />';
	        }
	    }

	    return $retn;
	}

	/**
	 * Return the URL or image object for a Gavatar
	 * @param string $email
	 * @param string $gravatar          mm, identicon, monsterid, wavatar
	 * @param boolean $return_as_img    true to return complete img tag, false to return url
	 * @param integer $size             size in pixels (default = 80px)
	 * @param array $atts               optional attributes to add to image tag
	 */
	function getGravatar($email, $gravatar, $return_as_img = true, $size = 80, $atts = array()){
	    $retn = '';
	    $size = intval($size);
	    if($size == 0) $size = 80;
	    if(empty($email))
	        $hash_email = '';
	    else
	        $hash_email = md5(strtolower(trim($email)));

	    // email is required
	    if($gravatar == '') $gravatar = 'mm';             // if gravatar not set, use "no user" gravatar
	    $retn = 'http://www.gravatar.com/avatar/'.$hash_email.'?d='.$gravatar.'&amp;s='.$size.'&amp;r=g';
	    if($return_as_img){
	        $retn = '<img src="'.$retn.'" alt="'.$gravatar.'" title="'.$gravatar.'" width="'.$size.'"';
	        if(is_array($atts)){
	            foreach($atts as $k => $v) $retn .= ' '.$k.'="'.$v.'"';
	        }
	        $retn.= ' />';
	    }

	    return $retn;
	}

	public function __get($name){
		$v = null;
		if(array_key_exists($name, $this->_keys)){
			// return scalar value
			$v = $this->_keys[$name];
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot get '$name'.  It is not a valid _USERS property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
		return $v;
	}

	public function __set($name, $value){
		if(array_key_exists($name, $this->_keys)){
			// set scalar value
			$this->_keys[$name] = $value;
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot set '$name'.  It is not a valid _USERS property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
	}
}

?>