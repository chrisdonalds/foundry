<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("SETTINGSLIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

define ("SETTINGS_TAB_GENERAL", "General");
define ("SETTINGS_TAB_MEDIA", "Media");
define ("SETTINGS_TAB_EDITORS", "Editors");
define ("SETTINGS_TAB_THEMES", "Themes");
define ("SETTINGS_TAB_MENUS", "Menus");
define ("SETTINGS_TAB_PLUGINS", "Plugins");
define ("SETTINGS_TAB_TAX", "Taxonomies");
define ("SETTINGS_TAB_USERS", "Users");
define ("SETTINGS_TAB_ADVANCED", "Advanced");
define ("SETTINGS_SUBTAB_MEDIA_UNIVERSAL", "Universal Settings");
define ("SETTINGS_SUBTAB_MEDIA_FORMATS", "Media Formats");
define ("SETTINGS_SUBTAB_THEMES_WEBSITE", "Website Themes");
define ("SETTINGS_SUBTAB_THEMES_ADMIN", "Admin Themes");
define ("SETTINGS_SUBTAB_MENUS_WEBSITE", "Website Menus");
define ("SETTINGS_SUBTAB_MENUS_ADMIN", "Admin Menus");
define ("SETTINGS_SUBTAB_PLUGINS_INSTALLED", "Installed Plugins");
define ("SETTINGS_SUBTAB_PLUGINS_EXTENSION", "Extensions");
define ("SETTINGS_SUBTAB_PLUGINS_PROBLEM", "Problem/Deleted Plugins");
define ("SETTINGS_SUBTAB_PLUGINS_FRAMEWORKS", "Frameworks");
define ("SETTINGS_SUBTAB_PLUGINS_SETTINGS", "Settings");
define ("SETTINGS_SUBTAB_PLUGINS_UPDATES", "Updates");
define ("SETTINGS_SUBTAB_PLUGINS_REPOSITORY", "Repository");
define ("SETTINGS_SUBTAB_ADVANCED_ALIASES", "Aliases");
define ("SETTINGS_SUBTAB_ADVANCED_USERS_MANAGER", "Users Manager");
define ("SETTINGS_SUBTAB_ADVANCED_SEO", "SEO");
define ("SETTINGS_SUBTAB_ADVANCED_VISIBILITY", "Site Visibility");
define ("SETTINGS_SUBTAB_ADVANCED_FILES_EMAIL", "Files/Email");
define ("SETTINGS_SUBTAB_ADVANCED_TUNING", "Tuning");
define ("SETTINGS_SUBTAB_ADVANCED_CRON", "Cron");
define ("SETTINGS_SUBTAB_ADVANCED_ERRORS", "Error Handling");
define ("SETTINGS_SUBTAB_ADVANCED_REPORTS", "Reports");
define ("SETTINGS_SUBTAB_ADVANCED_CORE", "Core");

/* SETTINGS CLASS
 *
 * Provides methods to settings operations
 */
class SettingsClass {
	// overloaded data
	private $settings_errors = array();
	private $tabs = array();
	private $custom_tabs = array();
	public $settings_issues = array();
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'_settings');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Clear all settings issues, errors and custom tabs
	 */
	function clearAll(){
		$this->clearSettingsErrors();
		$this->clearSettingsIssues();
		$this->clearCustomSettingsTabs();
	}

	/**
	 * Clear all settings issues for specific tab
	 * @param string $settingtab
	 */
	function clearSettingsIssues($settingtab = null){
		if(is_null($settingtab))
			$this->settings_issues = array();
		else
			$this->settings_issues[$settingtab] = array();
	}

	/**
	 * Clear all settings errors
	 */
	function clearSettingsErrors(){
		$this->settings_errors = array();
	}

	/**
	 * Delete all custom settings tabs (added programmatically to system)
	 */
	function clearCustomSettingsTabs(){
		$this->custom_tabs = array();
	}

	/**
	 * Add an issue to the settings issues array
	 * @param string $settingtab
	 * @param string $issue
	 */
	function addSettingsIssue($settingtab, $issue){
		if(isset($this->settings_issues[$settingtab])){
			$this->settings_issues[$settingtab][] = $issue;
		}
	}

	/**
	 * Show the reset link
	 */
	function showResetLink($key1, $key2 = null){
	    global $defcfg, $config, $_themes;

	    $themepath = $_themes->getThemePathUnder("admin");
	    if($key2 == null){
	        print "<input type=\"hidden\" id=\"reset_{$key1}\" value=\"".getIfSet($defcfg[$key1])."\"/>";
	        print "<input type=\"hidden\" id=\"undo_{$key1}\" value=\"".getConstIfSet($key1)."\"/>";
	        print "&nbsp;&nbsp;<a href=\"#\" onclick=\"$('#$key1').val($('#reset_{$key1}').val());\" class=\"fa fa-fast-backward\" title=\"Reset\"></a>";
	        print "&nbsp;<a href=\"#\" onclick=\"$('#$key1').val($('#undo_{$key1}').val());\" class=\"fa fa-undo\" title=\"Undo\"></a>";
	    }else{
	        print "<input type=\"hidden\" id=\"reset_{$key1}\" value=\"".getIfSet($defcfg[$key1])."\"/>";
	        print "<input type=\"hidden\" id=\"undo_{$key1}\" value=\"".getConstIfSet($key1)."\"/>";
	        print "<input type=\"hidden\" id=\"reset_{$key2}\" value=\"".getIfSet($defcfg[$key2])."\"/>";
	        print "<input type=\"hidden\" id=\"undo_{$key2}\" value=\"".getConstIfSet($key2)."\"/>";
	        print "&nbsp;&nbsp;<a href=\"#\" onclick=\"$('#$key1').val($('#reset_{$key1}').val()); $('#$key2').val($('#reset_{$key2}').val());\" class=\"fa fa-fast-backward\" title=\"Reset\"></a>";
	        print "&nbsp;<a href=\"#\" onclick=\"$('#$key1').val($('#undo_{$key1}').val()); $('#$key2').val($('#undo_{$key2}').val());\" class=\"fa fa-undo\" title=\"Undo\"></a>";
	    }
	}

	/**
	 * Output the basic reset link
	 * @param string $keyl
	 * @param string @origkey
	 */
	function showBasicResetLink($key1, $origkey){
		global $_themes;

	    $themepath = $_themes->getThemePathUnder("admin");
	    print "&nbsp;&nbsp;<a href=\"#\" onclick=\"$('#$key1').val($('#".$origkey."').val())\" class=\"fa fa-fast-backward\" title=\"Reset\"></a>";
	}

	/**
	 * Show timezone settings
	 */
	function showSettingsTimeZoneData(){
	    global $_system;

	    $tz_array = getTimezones();
	    ?>
	    <select name="newcfg[TIMEZONE]" id="TIMEZONE">
	        <?php
	        $tzgroup = "";
	        $cfgtimezone = getIfSet($_system->configs['TIMEZONE']);
	        foreach($tz_array as $tzkey => $tz){
	            $sel = (($tzkey == $cfgtimezone) ? ' selected="selected"' : '');
	            $tzkey_parts = explode("/", $tzkey);
	            if($tzgroup != $tzkey_parts[0]) {
	                echo (($tzgroup != '') ? '</optgroup>'.PHP_EOL : '').'<optgroup label="'.ucwords($tzkey_parts[0]).'">'.PHP_EOL;
	                $tzgroup = $tzkey_parts[0];
	            }
	            echo '<option value="'.$tzkey.'"'.$sel.'>'.$tz.'</option>'.PHP_EOL;
	        }
	        ?>
	        </optgroup>
	    </select> <?php $this->showResetLink('TIMEZONE')?>
	    <p class="settingsdescr">Current Server Time: <?php echo date("Y-m-d g:i:s a T"); ?><?php if(date("I") == 1) echo " (Daylight Savings Time)"?></p>
	    <?php
	}

	/**
	 * Show users list
	 */
	function showSettingsUsersList(){
	    global $_users, $fu_obj, $_system, $_db_control, $_fields, $_media;

	    ?>
	    <p class="settingsactions">
	    <?php
	    $action = array('checkbox' => array(
	                            'id' => 'user_bulkcheck'),
	                    'menu' => array(
	                            'id' => 'user_bulkopt',
	                            'sel' => '',
	                            'options' => array('deactivate' => '(De)activate', 'delete' => 'Delete')),
	                    'buttons' => array(
	                            'user_bulkact' => 'Go', 'admnewuser' => 'Add User')
	                   );
	    $this->showSettingsActions(0, $action);
	    ?>
	    </p>
	    <p class="user_you">Your account (Cannot be deleted by you)</p>
	    <?php
	    $users_array = $_db_control->setTable(ACCOUNTS_TABLE)->setOrder("username")->getRec();
	    $user_levels = $_users->types;
	    $user_count  = 0;
	    $user_admins = 0;
	    $current_loggedin_user_id = $_users->getUserID();
	    $current_loggedin_user_level = $_users->getUserLevel();
	    foreach($users_array as $this_user){
	        $this_user_id = $this_user['id'];
	        $user_count++;
	        if($this_user['level'] <= ADMLEVEL_SYSADMIN && $this_user['activated'] == 1) $user_admins++;
	        $current_user_can_edit_user = ($current_loggedin_user_level < $this_user['level'] || $current_loggedin_user_level == ADMLEVEL_DEVELOPER || $current_loggedin_user_id == $this_user_id);
	    ?>
	    <div class="user_row settings_hover_row<?php echo (($this_user_id == $current_loggedin_user_id) ? ' user_you' : '')?>">
	        <input type="hidden" class="user_id" name="users_id[<?php echo $user_count?>]" value="<?php echo $this_user_id?>" />
	        <div class="user_leftside col-lg-2">
	            <?php if($current_user_can_edit_user) { ?><input type="checkbox" name="users_check[<?php echo $user_count?>]" id="users_check<?php echo $user_count?>" class="user_checks" /><?php } ?>
	            <span class="user_name<?php echo (($this_user['activated'] == 0) ? " notactive" : "")?>"><?php echo ucwordsAdv($this_user['username'])?></span><br/>
	            <div class="user_actions action_items">
	                <?php
	                if($current_user_can_edit_user) {
	                    if($_users->userIsAllowedTo('activate_user')){?><a href="#" class="user_act"><?php echo (($this_user['activated'] == 1) ? "Dea" : "A")?>ctivate</a> | <?php }
	                    if($this_user_id != $current_loggedin_user_id && $_users->userIsAllowedTo('delete_user')){?><a href="#" class="user_del">Delete</a><br/><?php }
	                    if($_users->userIsAllowedTo('edit_profile')){?><a href="#" class="user_editprofile">Edit Profile</a><?php }
	                }
	                ?>
	            </div>
	            <input type="hidden" name="users_active[<?php echo $user_count?>]" id="users_active<?php echo $user_count?>" value="<?php echo $this_user['activated']?>" />
	            <input type="hidden" name="users_cur[<?php echo $user_count?>]" class="user_cur" value="<?php echo (($this_user_id == $current_loggedin_user_id) ? 'yes' : '');?>" />
	        </div>
	        <div class="user_rightside col-lg-6">
	            <div class="row"><span class="col-lg-4">Email Address:</span><span class="col-lg-8"><?php echo $this_user['email']?></span></div>
	            <div class="row"><span class="col-lg-4">Role:</span><span class="col-lg-8">
	            <?php
	            $key = array_search($this_user['level'], $user_levels);
	            echo $_users->getFriendlyUserType($key)."\n";
	            ?>
	            </span></div>

	        <?php if($current_user_can_edit_user) { ?>
	            <div class="user_profile row">
	                <div class="row"><span class="col-lg-12 clearfix"><strong><i class="fa fa-user"></i> User Profile</strong></span></div>
	                <div class="row"><span class="col-lg-4">Username:</span><span class="col-lg-8"><input type="text" name="users_name[<?php echo $user_count?>]" id="users_name<?php echo $user_count?>" size="20" value="<?php echo $this_user['username']?>"/></span></div>
	                <div class="row"><span class="col-lg-4">Password:</span><span class="col-lg-8">
	                	<input type="password" name="users_pass[<?php echo $user_count?>]" id="users_pass<?php echo $user_count?>" size="20" class="users_pass" value="" />&nbsp;<span class="users_pass_strength"></span>
	                	<a href="" class="users_pass_generate_open"><i class="fa fa-key"></i> Generate</a></span>
	                	<div class="small-box hidden"><input type="checkbox" value="a-z" name="user_pass_generate_opts[]">A-Z, <input type="checkbox" value="0-9" name="user_pass_generate_opts[]">0-9, <input type="checkbox" value="upper/lower" name="user_pass_generate_opts[]">Upper/Lower, <input type="checkbox" value="symbols" name="user_pass_generate_opts[]">@#%&amp; <input type="button" class="user_pass_generate_ok" value="Ok"> <input type="button" class="user_pass_generate_cancel" value="Cancel"></div>
	                </div>
	                <div class="row"><span class="col-lg-4">Confirm Password:</span><span class="col-lg-8"><input type="password" name="users_pass_confirm[<?php echo $user_count?>]" id="users_pass_confirm<?php echo $user_count?>" size="20" class="users_pass" value="" /></div>
	                <div class="row"><span class="col-lg-4">First Name:</span><span class="col-lg-8"><input type="text" name="users_firstname[<?php echo $user_count?>]" size="20" value="<?php echo $this_user['firstname']?>" /></span></div>
	                <div class="row"><span class="col-lg-4">Last Name:</span><span class="col-lg-8"><input type="text" name="users_lastname[<?php echo $user_count?>]" size="20" value="<?php echo $this_user['lastname']?>" /></span></div>
	                <div class="row"><span class="col-lg-4">Email:</span><span class="col-lg-8"><input type="text" name="users_email[<?php echo $user_count?>]" id="users_email<?php echo $user_count?>" size="30" value="<?php echo $this_user['email']?>"/></span></div>
	                <div class="row"><span class="col-lg-4">Role:</span><span class="col-lg-8"><select name="users_level[<?php echo $user_count?>]" id="users_level<?php echo $user_count?>">
	                <?php foreach($user_levels as $key => $lvl){
	                    if($lvl >= $this_user['level'] || $lvl >= $current_loggedin_user_level){
	                        $sel = (($this_user['level'] == $lvl) ? ' selected="selected"' : '');
	                        echo '<option value="'.$lvl.'"'.$sel.'>'.$_users->getFriendlyUserType($key).'</option>';
	                    }
	                } ?>
	                </select></span></div>

	                <div class="row top-padding"><span class="col-lg-12 clearfix"><strong><i class="fa fa-share-square-o"></i> Social Networks</strong></span></div>
	                <div class="row"><span class="col-lg-4">Facebook URL:[<a href="#" class="hovertip" alt="i.e.: facebook.com/&lt;b&gt;your.name&lt;/b&gt;.  <?php echo SYS_NAME?> uses this name to verify your identity. (The 'Allow User to Login with Their Facebook Account' setting, in Advanced>Users, must be on)">?</a>]</span><span class="col-lg-8"><input type="text" name="users_facebook_link[<?php echo $user_count?>]" size="30" value="<?php echo $this_user['facebook_link']?>" /></span></div>
	                <div class="row"><span class="col-lg-4">Twitter Link:</span><span class="col-lg-8"><input type="text" name="users_twitter_link[<?php echo $user_count?>]" size="30" value="<?php echo $this_user['twitter_link']?>" /></span></div>
	                <div class="row"><span class="col-lg-4">Google+ Link:</span><span class="col-lg-8"><input type="text" name="users_google_plus_link[<?php echo $user_count?>]" size="30" value="<?php echo $this_user['google_plus_link']?>" /></span></div>

	                <div class="row top-padding"><span class="col-lg-12 clearfix"><strong><i class="fa fa-eye"></i> Avatars</strong></span></div>
	                <div class="row"><span class="col-lg-4">Gravatar:</span><span class="col-lg-8">
	                <?php
	                $gr_arry = array('mm', 'identicon', 'wavatar', 'monsterid');
	                foreach($gr_arry as $gr){
	                    echo '<input type="radio" name="users_avatar['.$user_count.']" value="'.$gr.'"'.(($this_user['avatar'] == $gr) ? ' checked="checked"' : '').' />';
	                    echo $_users->getGravatar($this_user['email'], $gr, true, 40, array('style' => 'border: 1px solid black !important')).' ';
	                }
	                ?></span></div>
	                <div class="row"><span class="col-lg-4"><?php echo SYS_NAME?> Avatar:</span><span class="col-lg-8"></div>
	                <?php
	                $fu_obj['elems'][] = "users_image".$user_count;
	                $fu_obj['lastimg'][] = $this_user['image'];
	                $fu_obj['allowedexts'][] = $_media->getMediaTypes(MT_IMAGE);
	                $_fields->showImageField("", array("users_image".$user_count, "users_lastimg".$user_count, "users_lastthm".$user_count, "users_delimg".$user_count), array($this_user['image']), array(false), "", "", "", "", "", FLD_DATA);
	                ?>
	                </span></div>
	            </div>
	        <?php } else { ?>
	            <input type="hidden" name="users_name[<?php echo $user_count?>]" id="users_name<?php echo $user_count?>" value="<?php echo $this_user['username']?>"/>
	            <input type="hidden" name="users_email[<?php echo $user_count?>]" id="users_email<?php echo $user_count?>" value="<?php echo $this_user['email']?>"/>
	        <?php } ?>
	        </div>
	    </div>
	    <?php } ?>
	    <input type="hidden" id="users_count" value="<?php echo $user_count?>" />
	    <input type="hidden" id="users_admins" value="<?php echo $user_admins?>" />
	    <?php
	}

	/**
	 * Show website menu settings
	 */
	function showSettingsMenusforWebsite(){
	    global $_menus, $_data, $_tax, $_db_control, $_inflect;

	    // build the initial samples
	    $menubars = $_menus->getWebsiteMenubars();
	    $data_aliases = $_data->data_aliases;
	    $page_aliases = $_data->page_aliases;
	    $data_tables = $_db_control->getDataTables(false, false);
	    $taxonomies = $_tax->getTaxonomies(array('active' => 1));
	    $slots = $_menus->getWebsiteMenuSlots();

	    array_unshift($menubars, array('id' => 0, 'key' => '-', 'title' => '- Orphaned Menus -'));
	    $first_menubar = $menubars[0];
	    $menus = $_menus->getWebsiteMenus($first_menubar['key']);
	    $menu_html = $_menus->getWebsiteMenuHTML(null, $first_menubar['key'], array("mainid" => "websitemenu_mainnav", "subclass" => "websitemenu_subnav", "menuclass" => "websitemenu_menuitem bold", "itemwrap" => "span", "aftertext" => "&nbsp;({data})", "afterlink" => '<a href="#" class="websitemenu_menuitem_delete fa fa-trash" rel="{id}" title="Delete"></a><a href="#" class="websitemenu_menuitem_clone fa fa-files-o" rel="{key}" title="Clone"></a>'));
	    ?>
	    <p>With the Website Menu Editor you can create both menubars and menu items.  Menubars, which contain one or more menu items, are display objects that are added to pages.  Menu items are the actual links to aliased content or off-site addresses.</p>
	    <p>To reposition a menu item, just click and drag it to a new place.  The editor automatically updates the system.</p>
	    <div id="websitemenu_sample">
	        <div id="websitemenu_menubars">
	            <strong>Choose a Menubar:</strong>
	            <select id="websitemenu_menubar_select">
	                <?php
	                foreach($menubars as $menubar){
	                    echo '<option value="'.$menubar['id'].':'.$menubar['key'].'">'.$menubar['title'].'</option>'.PHP_EOL;
	                }
	                ?>
	            </select>
	            <?php if(count($menubars) > 1){ ?>
	            <input type="button" value="Edit" id="websitemenu_menubar_edit" class="hidden" />
	            <input type="button" value="Delete" id="websitemenu_menubar_delete" class="hidden" />
	            <?php } ?>
	            <input type="button" value="Create" id="websitemenu_menubar_create" />
	            <input type="hidden" value="<?php echo $first_menubar['id'] ?>" id="websitemenu_menubar_id" />
	        </div>
	        <div class="row">
	            <div id="websitemenu_menubar_editor" class="col-lg-11">
	                <h3 class="header">Edit Website Menubar</h3>
	                <div class="setlabel">Menubar Title:</div><div class="setdata"><input type="text" id="websitemenu_menubar_title" name="websitemenu_menubar_title" value="" /></div>
	                <div class="setlabel">Menubar Description:</div><div class="setdata"><textarea id="websitemenu_menubar_descr" name="websitemenu_menubar_descr"></textarea></div>
	                <div class="setlabel">Menubar Slot:</div><div class="setdata">
	                	<select id="websitemenu_menubar_slot" name="websitemenu_menubar_slot">
	                		<?php
	                		foreach($slots as $id => $slot){
	                			echo '<option value="'.$id.'">'.$slot.'</option>';
	                		}
	                		?>
	                	</select>
	            	</div>
	                <div class="setbuttons row">
	                    <input type="hidden" value="" id="websitemenu_menubar_editor_id" />
	                    <input type="button" value="Cancel" id="websitemenu_menubar_cancel" />
	                    <input type="button" value="Save" id="websitemenu_menubar_save" />
	                </div>
	            </div>
	        </div>
	        <div class="row">
		        <div id="websitemenu_menuitems" class="col-lg-4">
	                <h3 class="header">Website Menu Items <input type="button" value="Add Menu(s)" id="websitemenu_menuitems_add" class="hidden" /></h3>
		        	<div id="websitemenu_menuitems_add_list">
		        		<p>
		        			Add one or more menu items to this menubar that link to a:
		        		</p>
		        		<div class="row">
			        		<div class="col-lg-3">Page:</div>
			        		<div class="col-lg-8">
				        		<select id="websitemenu_menuitems_datatype_pages" class="multiselect normalfldsize" multiple="multiple" data-placeholder="Select a Page" rel="pages">
				        		<?php echo $_data->getPageAliases("published = 1", "asc", "all", "menu", false); ?>
				        		</select>
				        	</div>
			        	</div>
			        	<?php
			        	if(!empty($data_tables)){
			        		foreach($data_tables as $table){
			        			$recs = $_db_control->setTable($table)->setWhere("published = 1")->getData();
			        			if(!empty($recs)){
			        				$tablename = ucwords(str_replace(DB_TABLE_PREFIX, "", $table));
			        				$tablename_single = $_inflect->singularize($tablename);
			        				?>
		        		<div class="row">
			        		<div class="col-lg-3"><?php echo $tablename_single ?>:</div>
			        		<div class="col-lg-8">
				        		<select id="websitemenu_menuitems_datatype_<?php echo $table ?>" class="multiselect normalfldsize" data-placeholder="Select <?php echo $tablename_single ?>" multiple="multiple" rel="<?php echo $table ?>">
				        			<?php
				        			foreach($recs as $rec){
				        				$val = $_db_control->getTitleField($rec);
				        				echo '<option value="'.$rec['id'].'">'.$val.'</option>';
				        			}
				        			?>
				        		</select>
				        	</div>
			        	</div>
			        				<?php
			        			}
			        		}
			        	}

			        	if(!empty($taxonomies)){
			        		foreach($taxonomies as $tax){
			        			$recs = $_db_control->setTable(TERMS_TABLE)->setWhere("taxonomy_id = '".$tax['id']."'")->getRec();
			        			if(!empty($recs)){
			        				$taxname_single = $_inflect->singularize($tax['name']);
			        				?>
		        		<div class="row">
			        		<div class="col-lg-3"><?php echo $taxname_single ?> Terms:</div>
			        		<div class="col-lg-8">
				        		<select id="websitemenu_menuitems_terms_<?php echo $tax['code'] ?>" class="multiselect normalfldsize" data-placeholder="Select a Term" multiple="multiple" rel="terms-tax<?php echo $tax['id']?>">
				        			<?php
				        			foreach($recs as $rec){
				        				echo '<option value="'.$rec['id'].'">'.$rec['name'].'</option>';
				        			}
				        			?>
				        		</select>
				        	</div>
			        	</div>
			        				<?php
			        			}
			        		}
			        	}
			        	?>
		        		<p class="row">
		        			<input type="button" value="Add Selection(s)" id="websitemenu_menuitems_add_items" class="gray" disabled="disabled">... Or, <input type="button" value="Create a Custom Menu" id="websitemenu_menuitems_add_blank" />
		        		</p>
		        		<p class="row">
		        			<input type="button" value="Close" id="websitemenu_menuitems_add_close" />
		        		</p>
		        	</div>
		            <div id="websitemenu_menuitems_list">
		                <p class="websitemenu_title">
		                    <?php
		                    if($first_menubar['key'] != '-'){
		                    	if(empty($menu_html)) {
		                    		echo "No menu items are in the ".$first_menubar['title']." website menubar.  Click 'Add Menu(s)' to add one.";
		                    	}else{
		                    		echo "Menu items in the '".$first_menubar['title']."' website menubar:";
		                    	}
		                    } else {
		                    	if(empty($menu_html)) {
		                    		echo "Great! No menus are currently orphaned.";
		                    	}else{
		                    		echo "The following menu items are disassociated from any website menubars:";
		                    	}
		                    }
		                    ?>
		                </p>
		                <?php echo $menu_html?>
		            </div>
		        </div>
		        <div id="websitemenu_editor" class="col-lg-7">
		            Select a menu from left or create a new menu item...
		        </div>
		    </div>
	    </div>
	    <?php
	}

	/**
	 * Show admin menu settings
	 */
	function showSettingsMenusforAdmin(){
	    global $_menus;

	    // build the initial samples
	    $menubars = $_menus->getAdminMenubars();

	    array_unshift($menubars, array('id' => 0, 'key' => '-', 'parent_id' => 0, 'title' => '- Orphaned Menus -'));
	    $first_menubar = $menubars[0];
	    $menus = $_menus->getAdminMenus($first_menubar['key']);
	    $menu_html = $_menus->getAdminMenuHTML($first_menubar['key'], array("mainid" => "adminmenu_mainnav", "subclass" => "adminmenu_subnav", "menuclass" => "adminmenu_menuitem", "itemwrap" => "span", "aftertext" => "&nbsp;({data})", "afterlink" => '<a href="#" class="adminmenu_menuitem_delete fa fa-trash" rel="{id}" title="Delete"></a><a href="#" class="adminmenu_menuitem_clone fa fa-files-o" rel="{key}" title="Clone"></a>'));
	    ?>
	    <p>The Admin Menu Editor provides a painless yet powerful mechanism for modifying the admin area menus.  New menus can be created easily -- create the menubar and create the menu item.</p>
	    <p>To reposition a menu, just click and drag it to a new place.  The editor automatically updates the system.</p>
	    <div id="adminmenu_sample">
	        <div id="adminmenu_menubars">
	            <strong>Choose a Menubar:</strong>
	            <select id="adminmenu_menubar_select">
	                <?php
	                foreach($menubars as $menubar){
	                    echo '<option value="'.$menubar['id'].':'.$menubar['key'].'">'.$menubar['title'].(($menubar['parent_id'] == MENU_MAIN_SLOT) ? ' (Main)' : '').'</option>'.PHP_EOL;
	                }
	                ?>
	            </select>
	            <?php if(count($menubars) > 1){ ?>
	            <input type="button" value="Edit" id="adminmenu_menubar_edit" class="hidden" />
	            <input type="button" value="Delete" id="adminmenu_menubar_delete" class="hidden" />
	            <?php } ?>
	            <input type="button" value="Create" id="adminmenu_menubar_create" />
	            <input type="hidden" value="<?php echo $first_menubar['id'] ?>" id="adminmenu_menubar_id" />
	        </div>
        	<div class="row">
	            <div id="adminmenu_menubar_editor" class="col-lg-11">
	                <h3 class="header">Edit Admin Menubar</h3>
	                <div class="setlabel">Menubar Title:</div><div class="setdata"><input type="text" id="adminmenu_menubar_title" name="adminmenu_menubar_title" value="" /></div>
	                <div class="setlabel">Menubar Description:</div><div class="setdata"><textarea id="adminmenu_menubar_descr" name="adminmenu_menubar_descr"></textarea></div>
	                <div class="setlabel">Menubar Slot:</div><div class="setdata"><input type="checkbox" id="adminmenu_menubar_slot" name="adminmenu_menubar_slot" value="1" /> Main Slot<br/>Note: Only one menubar can be assigned to this menubar slot.</div>
	                <div class="setbuttons">
	                    <input type="hidden" value="" id="adminmenu_menubar_editor_id" />
	                    <input type="button" value="Cancel" id="adminmenu_menubar_cancel" />
	                    <input type="button" value="Save" id="adminmenu_menubar_save" />
	                </div>
	            </div>
	        </div>
        	<div class="row">
		        <div id="adminmenu_menuitems" class="col-lg-4">
	                <h3 class="header">Admin Menu Items <input type="button" value="Create" id="adminmenu_menuitems_add" class="hidden" /></h3>
		        	<div id="adminmenu_menuitems_add_list">
		        	</div>
		            <div id="adminmenu_menuitems_list">
		                <p class="adminmenu_title">
		                    <?php
		                    if($first_menubar['key'] != '-'){
		                    	if(empty($menu_html)) {
		                    		echo "No menu items are in the ".$first_menubar['title']." website menubar.  Click 'Add Menu(s)' to add one.";
		                    	}else{
		                    		echo "Menu items in the '".$first_menubar['title']."' website menubar:";
		                    	}
		                    } else {
		                    	if(empty($menu_html)) {
		                    		echo "Great! No menus are currently orphaned.";
		                    	}else{
		                    		echo "The following menu items are disassociated from any website menubars:";
		                    	}
		                    }
		                    ?>
		                </p>
		                <?php echo $menu_html?>
		            </div>
		        </div>
		        <div id="adminmenu_editor" class="col-lg-7">
		            Select a menu from left or create a new menu item...
		        </div>
		    </div>
	    </div>
	    <?php
	}

	/**
	 * Show admin alias settings
	 */
	function showSettingsAdminAliases(){
	    global $_system, $_page, $_data, $_db_control;

	    $aliases = $_data->admin_aliases;
    	if(!empty($aliases)){
	        foreach($aliases as $alias){
 				$num_linked_menus = $_db_control->setTable(MENUS_TABLE)->setWhere("alias = '{$alias['attribute_id']}'")->getRecNumRows();
 				$num_linked_taxonomies = $_db_control->setTable(TAXONOMIES_TABLE)->setWhere("data_alias = '{$alias['attribute_id']}'")->getRecNumRows();
 				$delete = (($num_linked_menus == 0 && $num_linked_taxonomies == 0) ? '<i class="fa fa-trash red dataalias_delete clickable left-padding"></i>' : '<i class="fa fa-trash gray left-padding" title="Cannot delete while alias is associated with a menu or term.  Delete the menu or term first."></i>');
	            ?>
	            <div class="row top-padding" rel="<?php echo $alias['attribute_id'] ?>">
	            	<div class="col-lg-4">
		                <?php
		                echo ADMIN_FOLDER;
		                if($alias['data_type'] != TERMS_TABLE && $alias['data_type'] != PAGES_TABLE){
		                	echo '<input type="text" name="admin_dataalias_meta['.$alias['attribute_id'].']" class="admin_dataalias_meta bigfldsize" value="'.getIfSet($alias['alias']).'" />';
		                }else{
		                	echo '<input type="text" name="admin_dataalias_meta['.$alias['attribute_id'].']" class="admin_dataalias_meta bigfldsize" value="'.getIfSet($alias['alias']).'" readonly="readonly" disabled="disabled" />';
		                }
		                ?>
		            </div>
		            <div class="col-lg-8">
		            	<?php
		            	echo str_replace(DB_TABLE_PREFIX, "", $alias['data_type']);
			            if($alias['data_type'] != PAGES_TABLE) echo $delete;
		            	?>
		            </div>
	            </div>
	        <?php
	        }
	    }
    }

	/**
	 * Show data alias settings
	 */
	function showSettingsDataAliases(){
	    global $_system, $_page, $_data, $_db_control;

	    $aliases = $_data->data_aliases;
	    $metas = array(
	        DA_TERM => "A single taxonomy term",
	        DA_TERM_ID => "A single taxonomy term ID",
	        DA_CODE => "A data record code",
	        DA_ID => "A data record ID",
	        DA_TYPE => "A data type code",
	        DA_YEAR => "Four-digit year",
	        DA_MONTH => "Two-digit month",
	        DA_DAY => "Two-digit day-of-the-month",
	        DA_HOUR => "Two-digit hour",
	        DA_MINUTE => "Two-digit minutes",
	        DA_SECOND => "Two-digit seconds",
	        );
	    $meta_text = '<a href="#" class="close"></a><br/>';
	    foreach($metas as $key => $phrase) $meta_text .= $key." = ".$phrase."&nbsp;[<a href=\"#\" class=\"dataalias_meta_add\" rel=\"".$key."\">Paste</a>]<br/>";
	    ?>
	    <div id="dataaliasmetas" class="help_div smallfont"><?php echo $meta_text ?></div>
	    <?php

    	if(!empty($aliases)){
	        foreach($aliases as $alias){
	            ?>
	            <div class="row top-padding" rel="<?php echo $alias['attribute_id'] ?>">
	            	<div class="col-lg-4">
		                <input type="text" name="dataalias_meta[<?php echo $alias['attribute_id']?>]" class="dataalias_meta bigfldsize" value="<?php echo getIfSet($alias['alias'])?>" />
		                <span class="dataalias_notice"></span>
		                <a href="#" class="triggerhelp align_bottom fa fa-tags" rel="#dataaliasmetas" title="Data Alias Meta Tags"></a>
		            </div>
		            <div class="col-lg-8">
		            	<?php
		            	echo str_replace(DB_TABLE_PREFIX, "", $alias['data_type']);
		            	?>
		            </div>
	            </div>
	        <?php
	        }
	    }
	}

	/**
	 * Show taxonomy settings
	 */
	function showSettingsTaxonomyAliases(){
	    global $_system, $_page, $_data, $_db_control;

	    $aliases = $_data->taxonomy_aliases;
	    $metas = array(
	        DA_TERM => "A single taxonomy term",
	        DA_TERM_ID => "A single taxonomy term ID",
	        DA_YEAR => "Four-digit year",
	        DA_MONTH => "Two-digit month",
	        DA_DAY => "Two-digit day-of-the-month",
	        DA_HOUR => "Two-digit hour",
	        DA_MINUTE => "Two-digit minutes",
	        DA_SECOND => "Two-digit seconds",
	        );
	    $meta_text = '<a href="#" class="close"></a><br/>';
	    foreach($metas as $key => $phrase) $meta_text .= $key." = ".$phrase."&nbsp;[<a href=\"#\" class=\"taxalias_meta_add\" rel=\"".$key."\">Paste</a>]<br/>";
	    ?>
	    <div id="taxaliasmetas" class="help_div smallfont"><?php echo $meta_text ?></div>
	    <?php
    	if(!empty($aliases)){
	        foreach($aliases as $alias){
	            ?>
	            <div class="row top-padding" rel="<?php echo $alias['attribute_id'] ?>">
	            	<div class="col-lg-12">
		                <input type="text" id="taxalias_meta_<?php echo $alias['attribute_id']?>" name="taxalias_meta[<?php echo $alias['attribute_id']?>]" class="taxalias_meta bigfldsize" value="<?php echo getIfSet($alias['alias'])?>" />
		                <span class="taxalias_notice"></span>
		                <a href="#" class="triggerhelp align_bottom fa fa-tags" rel="#taxaliasmetas" data-rel="#taxalias_meta_<?php echo $alias['attribute_id']?>" title="Taxonomy Alias Meta Tags"></a>
		            </div>
	            </div>
	        <?php
	        }
	    }
	}

	/**
	 * Show SEO settings
	 */
	function showSettingsLinksArea(){
		global $_themes;

	    $themepath = $_themes->getThemePathUnder("admin");
	    ?>
	        <p><b>CAUTION: Invalid settings may cause page loading or server problems!</b>  If you are unable to access the site, the only recourse will be to modify the .htaccess file directly.</p>
	    <?php
	    $cur_htfile = SITE_PATH.".htaccess";
	    $ht_linedata = array();
	    $ht_sectdata = array("301" => array(), "seo" => array(), "www" => array(), "img" => array());
	    $ht_section = "";

	    $fp = @fopen($cur_htfile, "r");
	    if($fp){
	        $domain = str_replace(array(".", "www"), array("\.", ""), $_SERVER['HTTP_HOST']);
	        while(($line = fgets($fp)) !== false){
	            $line = trim(addslashes($line));
	            if(substr($line, 0, 8) == '# ----- '){
	                switch(strtolower(substr($line, 8))){
	                    case "301 redirects":
	                        $ht_section = "301";
	                        break;
	                    case "seo":
	                        $ht_section = "seo";
	                        break;
	                    case "www rewrites":
	                        $ht_section = "www";
	                        $ht_sectdata['www']['set_www'] = 'RewriteCond %{HTTP_HOST} ^([a-z.]+)?'.$domain.'$ [NC]
	RewriteCond %{HTTP_HOST} !^www\. [NC]
	RewriteRule .? http://www.%1'.$_SERVER['HTTP_HOST'].'%{REQUEST_URI} [R=301]';
	                        $ht_sectdata['www']['unset_www'] = 'RewriteCond %{HTTP_HOST} ^www\.([a-z.]+)?'.$domain.'$ [NC]
	RewriteRule .? http://%1'.$_SERVER['HTTP_HOST'].'%{REQUEST_URI} [R=301]';
	                        $ht_sectdata[$ht_section]['www_active'] = true;
	                        break;
	                    case "image hotlinking":
	                        $ht_section = "img";
	                        $ht_sectdata['img']['basic_img'] = 'RewriteCond %{HTTP_REFERER} !^$
	RewriteCond %{HTTP_REFERER} !^http://(www\.)?'.$domain.'/.*$ [NC]
	RewriteRule \.(gif|jpg|png)$ - [F]';
	                        $ht_sectdata['img']['adv_img'] = 'RewriteCond %{REQUEST_FILENAME} .*(jpg|jpeg|gif|png)$ [NC]
	RewriteCond %{HTTP_REFERER} !^$
	RewriteCond %{HTTP_REFERER} !'.$domain.' [NC]
	RewriteCond %{HTTP_REFERER} !google\. [NC]
	RewriteCond %{HTTP_REFERER} !search\?q=cache [NC]
	RewriteRule (.*) /'.substr(VHOST, 1).'_imghl.php?pic=$1';
	                        $ht_sectdata[$ht_section]['img_active'] = true;
	                        break;
	                    default:
	                        $ht_section = '';
	                        $ht_linedata[] = $line;
	                        break;
	                }
	                if($ht_section != '') $ht_linedata[] = "#< ".$ht_section." >";
	            }elseif($line != '' && $ht_section != ''){
	                $ht_active = true;
	                if(substr($line, 0, 1) == '#') {
	                    $ht_active = false;
	                    if($ht_section == '301' || $ht_section == 'seo') $line = substr($line, 1);
	                }
	                switch($ht_section){
	                    case "301":
	                        $chunks = preg_split("/[\s]+/i", $line);
	                        if(strtolower($chunks[0]) == 'redirect' && $chunks[1] == '301' && count($chunks) >= 4){
	                            $ht_sectdata[$ht_section][] = array("from" => $chunks[2], "to" => $chunks[3], "flags" => $chunks[4], "active" => $ht_active);
	                        }
	                        break;
	                    case "seo":
	                        $chunks = preg_split("/[\s]+/i", $line);
	                        if(strtolower($chunks[0]) == 'rewriterule' && count($chunks) >= 3){
	                            $ht_sectdata[$ht_section][] = array("from" => $chunks[1], "to" => $chunks[2], "active" => $ht_active);
	                        }
	                        break;
	                    case "www":
	                        $ht_sectdata[$ht_section]['www_data'] .= ((!isBlank($ht_sectdata[$ht_section]['www_data'])) ? PHP_EOL : "").stripslashes($line);
	                        $ht_sectdata[$ht_section]['www_active'] &= $ht_active;
	                        break;
	                    case "img":
	                        $ht_sectdata[$ht_section]['img_data'] .= ((!isBlank($ht_sectdata[$ht_section]['img_data'])) ? PHP_EOL : "").stripslashes($line);
	                        $ht_sectdata[$ht_section]['img_active'] &= $ht_active;
	                        break;
	                }
	            }else{
	                $ht_linedata[] = $line;
	            }
	        }
	        fclose($fp);
	        if($ht_linedata[0] == '') unset($ht_linedata[0]);
	    }
	    ?>
	        <textarea id="ht_line_data" name="ht_line_data" style="display: none" cols="1" rows="1"><?php echo join("\n", str_replace("<", "&lt;", $ht_linedata));?></textarea>
	        <input type="hidden" id="ht_mod1" name="ht_mod1" value="" />
	        <input type="hidden" id="ht_mod2" name="ht_mod2" value="" />

	        <h3 class="header">Custom SEO Links  <a href="http://www.yourhtmlsource.com/sitemanagement/urlrewriting.html" target="_blank" class="fa fa-external-link" title="Learn more about SEO page redirects"></a></h3>
	        <p>These page redirects enhance page links by making URLs more user-friendly and maximize search engine ranking.</p>
	        [RewriteRule <i>url_from</i> <i>url_to</i>]
	        <input type="button" id="ht_seo_addrow" value="Add New" />
	        <br/>
	        <?php
	        $n = 0;
	        foreach($ht_sectdata['seo'] as $line){
	            echo "<div class=\"ht_seo\">\n";
	            echo "<input type=\"checkbox\" name=\"ht_seo_active[$n]\" class=\"ht_seo_active\" value=\"1\"".(($line['active'] == 1) ? ' checked="checked" rel="y"' : ' rel=""')." title=\"Check to enable\" /> ";
	            echo "<input type=\"text\" name=\"ht_seo_from[$n]\" class=\"ht_seo_from\" size=\"28\" value=\"{$line['from']}\" rel=\"{$line['from']}\" /> ";
	            echo "<input type=\"text\" name=\"ht_seo_to[$n]\" class=\"ht_seo_to\" size=\"28\" value=\"{$line['to']}\" rel=\"{$line['to']}\" /> ";
	            echo "<a href=\"#\" class=\"ht_delete fa fa-trash\" rel=\"$n\" title=\"Delete\"></a> ";
	            echo "</div>\n";
	            $n++;
	        }
	        ?>
	        <h3 class="header">301 Redirects  <a href="http://en.wikipedia.org/wiki/HTTP_301" target="_blank" class="fa fa-external-link" title="Learn more about 301 redirects"></a></h3>
	        <p>A 301 Redirect instructs the server to access a different web page when a specific address is requested.</p>
	        [Redirect 301 <i>url_from</i> <i>url_to</i>]
	        <input type="button" id="ht_301_addrow" value="Add New" />
	        <br/>
	        <?php
	        $n = 0;
	        foreach($ht_sectdata['301'] as $line){
	            echo "<div class=\"ht_301\">\n";
	            echo "<input type=\"checkbox\" name=\"ht_301_active[$n]\" class=\"ht_301_active\" value=\"1\"".(($line['active'] == 1) ? ' checked="checked" rel="y"' : ' rel=""')." title=\"Check to enable\" /> ";
	            echo "<input type=\"text\" name=\"ht_301_from[$n]\" class=\"ht_301_from\" size=\"28\" value=\"{$line['from']}\" rel=\"{$line['from']}\" /> ";
	            echo "<input type=\"text\" name=\"ht_301_to[$n]\" class=\"ht_301_to\" size=\"28\" value=\"{$line['to']}\" rel=\"{$line['to']}\" /> ";
	            echo "<a href=\"#\" class=\"ht_delete fa fa-trash\" rel=\"$n\" title=\"Delete\"></a> ";
	            echo "</div>\n";
	            $n++;
	        }
	        ?>

	        <h3 class="header">WWW Checking  <a href="http://en.wikipedia.org/wiki/URL_redirection" target="_blank" class="fa fa-external-link" title="Learn more about www rewriting"></a></h3>
	        <p>Choose one of the provided options that will determine how to handle www prefixes.</p>
	        <span class="ht_toggle" id="ht_www_toggle">Less</span>
	        <textarea id="ht_www_data" name="ht_www_data" cols="70" rows="5"><?php echo $ht_sectdata['www']['www_data'];?></textarea>
	        <?php
	        $www1 = "";
	        $data = strtolower($ht_sectdata['www']['www_data']);
	        $sel = ' selected="selected"';
	        if(strpos($data, strtolower($ht_sectdata['www']['set_www'])) !== false){
	            $www1 = "set_www";
	        }elseif(strpos($data, strtolower($ht_sectdata['www']['unset_www'])) !== false){
	            $www1 = "unset_www";
	        }
	        foreach($ht_sectdata['www'] as $key => $val) echo '<input type="hidden" id="'.$key.'" value="'.$val.'" />'.PHP_EOL;
	        ?>
	        <p>
	            <select id="ht_www1" name="ht_www1">
	                <option value="">Original/Customized</option>
	                <option value="disable"<?php echo ((!$ht_sectdata['www']['www_active']) ? $sel : '')?>>Disable 'www' checking</option>
	                <option value="set_www"<?php echo (($www1 == "set_www") ? $sel : '')?>>Add 'www' to URLs</option>
	                <option value="unset_www"<?php echo (($www1 == "unset_www") ? $sel : '')?>>Remove 'www' from URLs</option>
	            </select>
	        </p>

	        <h3 class="header">Image Hotlinking  <a href="http://en.wikipedia.org/wiki/Inline_linking" target="_blank" class="fa fa-external-link" title="Learn more about preventing image hotlinking"></a></h3>
	        <p>Prevent web designers from directly requesting images, located on this server, for use on their sites by activating the following option.
	        Note: you may need to disable image hotlinking before moving this site to another domain or images will not be displayed.</p>
	        <span class="ht_toggle" id="ht_img_toggle">Less</span>
	        <textarea id="ht_img_data" name="ht_img_data" cols="70" rows="5"><?php echo $ht_sectdata['img']['img_data'];?></textarea>
	        <?php
	        $img1 = "";
	        $data = strtolower($ht_sectdata['img']['img_data']);
	        $sel = ' selected="selected"';
	        if(strpos($data, strtolower($ht_sectdata['img']['basic_img'])) !== false){
	            $img1 = "basic_img";
	        }elseif(strpos($data, strtolower($ht_sectdata['img']['adv_img'])) !== false){
	            $img1 = "adv_img";
	        }
	        foreach($ht_sectdata['img'] as $key => $val) echo '<input type="hidden" id="'.$key.'" value="'.$val.'" />'.PHP_EOL;
	        ?>
	        <p>
	            <select id="ht_img1" name="ht_img1">
	                <option value="">Original/Customized</option>
	                <option value="disable"<?php echo ((!$ht_sectdata['img']['img_active']) ? $sel : '')?>>Disable image hotlink blocking</option>
	                <option value="basic_img"<?php echo (($img1 == "basic_img") ? $sel : '')?>>Basic protection</option>
	                <option value="adv_img"<?php echo (($img1 == "adv_img") ? $sel : '')?>>Advanced protection</option>
	            </select>
	        </p>
	    <?php
	}

	/**
	 * Show system visibility settings
	 */
	function showSettingsSiteVisibilityArea(){
	    $files = scandir(SITE_PATH.ADMIN_FOLDER.REV_FOLDER, 1);
	    ?>
	        <h3 class="header">Access Rules List:</h3>
	        <p>The robots.txt file contains a set of rules that dictate which automated search engine scripts, colloquially called 'robots' or 'spiders'
	        are allowed to index parts or the entire site.  You can learn more about how to construct the robots.txt file at
	        <a href="http://www.outfront.net/tutorials_02/adv_tech/robots.htm" target="_blank">www.outfront.net/tutorials_02/adv_tech/robots.htm</a>.</p>
	        <p class="settingsdescr">Simply, select an agent (or * for all) and enter the list of folders, one per line, starting and ending with a forward-slash (/), that you <em>do not want</em> the agent to see.  System folders are already blocked.</p>
	        <?php if (count($files) > 2){ ?>
	        <div class="setlabel"><strong>Previous Versions</strong></div>
	        <div class="setdata">
	            <select id="robots_revfile">
	                <?php
	                foreach($files as $file) if($file != '..' && $file != '.') echo '<option value="'.REV_FOLDER.$file.'">'.$file.'</option>';
	                ?>
	            </select>
	            &nbsp;<input type="button" id="robots_revision" value="Revert" />
	        </div>
	        <?php } ?>
	    <?php
	    echo "<div class=\"setlabel\"><strong>Agent</strong> <span class=\"hovertip\" alt=\"List of search crawlers, bots, scrapers, and spiders that you can select\">[?]</span></div>\n";
	    echo "<div class=\"setdata\"><strong>Affected Path (eg. /, /folder/, /events/today/)</strong> <span class=\"hovertip\" alt=\"The path(s) that will be blocked\">[?]</span></div>\n";
	    $bots = array(  ""=>"",
	                    "*" => "All",
	                    "AbachoBOT"=>"Abacho",
	                    "Acoon"=>"Acoon",
	                    "AESOP_com_SpiderMan"=>"Aesop",
	                    "ah-ha.comcrawler"=>"Ah-ha",
	                    "ia_archiver"=>"Alexa",
	                    "FAST-WebCrawler"=>"AlltheWeb",
	                    "Scooter"=>"<b>AltaVista",
	                    "Atomz"=>"Atomz",
	                    "DeepIndex"=>"DeepIndex",
	                    "Arachnoidea"=>"Euroseek",
	                    "EZResult"=>"EZResults",
	                    "Gigabot"=>"Gigablast",
	                    "Googlebot"=>"<b>Google",
	                    "KIT-Fireball/2.0"=>"Fireball 2.0 (GermanSEatwww.fireball.de)",
	                    "Slurp.so/1.0"=>"Inktomi 1.0",
	                    "Slurp/2.0"=>"Inktomi 2.0",
	                    "Slurp/2.0j"=>"Inktomi 2.0j",
	                    "Slurp/3.0"=>"<b>Inktomi 3.0",
	                    "LNSpiderguy"=>"Lexis-Nexis",
	                    "MantraAgent"=>"LookSmart",
	                    "HenryTheMiragoRobot"=>"Mirago",
	                    "MSN"=>"<b>MSN (MicrosoftPrototypeCrawler)",
	                    "NationalDirectory-SuperSpider"=>"<b>National Directory",
	                    "Openbot"=>"Openfind",
	                    "Fido"=>"PlanetSearch",
	                    "Openfindpiranha,Shark"=>"Openfind",
	                    "Scrubby"=>"ScrubTheWeb",
	                    "Fluffythespider"=>"SearchHippo",
	                    "Teoma_agent1"=>"Teoma",
	                    "ESISmartSpider"=>"TravelFinder",
	                    "UKSearcherSpider"=>"UKSearcher",
	                    "appie"=>"Walhello",
	                    "WebCrawler"=>"WebCrawler",
	                    "Nazilla"=>"Websmostlinked",
	                    "Winona"=>"WhatUSeek",
	                    "ZyBorg"=>"Wisenut",
	                    "Gulper"=>"Yuntis",
	                );

	    $curfilename = SITE_PATH."robots.txt";
	    $fp = @fopen($curfilename, "r");
	    if($fp){
	        $rules = array();
	        $rule_robot = "";
	        $sysrules = false;
	        $currobot = "";
	        while($line = fgets($fp)){
	            $line = trim($line);
	            $currobot .= $line."\n";
	            if($line == "# System folders"){
	                $sysrules = true;
	            }elseif($line == "# End of System folders"){
	                $sysrules = false;
	            }
	            if(!$sysrules){
	                if (substr(strtolower($line), 0, 11) == "user-agent:"){
	                    $rule_robot = substr($line, 12);
	                    $rules[$rule_robot] = "";
	                }elseif (substr(strtolower($line), 0, 9) == "disallow:"){
	                    $rules[$rule_robot] .= substr($line, 10)."\n";
	                }
	            }
	        }
	        fclose($fp);
	    }

	    reset($rules);
	    for($i = 0; $i < 20; $i++){
	        list($ua_i, $da_i) = each($rules);
	        $list = "<select name=\"ua[{$i}]\" id=\"ua{$i}\" size=\"1\">";
	        foreach($bots as $bot => $se){
	            ($ua_i == $bot) ? $sel = " selected=\"selected\"" : $sel = "";
	            ($bot == "*") ? $bottag == " (All)" : $bottag = "";
	            if(strpos($se, '<b>') !== false) {
	                $style = ' style="font-weight: bold;"';
	                $se = substr($se, 3);
	            }else
	                $style = "";
	            $list .= "<option value=\"{$bot}\"{$sel}{$style}>{$se}{$bottag}</option>";
	        }
	        $list .= "</select>";
	        print "<div class=\"setlabel\">$list</div>\n";
	        print "<div class=\"setdata\"><textarea name=\"da[{$i}]\" id=\"da{$i}\" cols=\"42\" rows=\"2\">".$da_i."</textarea></div>\n";
	    }
	    ?>
	        <input type="hidden" id="currules" name="currules" value="<?php echo str_replace(array('"', '\\'), array('&#34;', '&#92;&#92;'), json_encode($rules)) ?>"/>
	    <?php
	}

	/**
	 * Show settings reports
	 * @param string $report
	 */
	function showSettingsReports($report){
	    global $_error, $_db_control, $_filesys;

	    $html = array();
	    $columns = null;
	    $columnatts = array();
	    $actions = array();
	    switch($report){
	        case 'system_health':
	            $reqd_tables = checkRequiredTables(false);
	            $html = array(
	                "System Versions" => array(
	                    SYS_NAME => CODE_VER,
	                    "PHP Requirements" => "",
	                    " -- Memory Limit" => ini_get('memory_limit')." ".((intval(ini_get('memory_limit')) < intval(F_MEMORY_LIMIT)) ? '<span class="bad">'.F_MEMORY_LIMIT.' or more required.</span>' : '<i class="fa fa-check fa-lg green"></i>'),
	                    " -- Safe mode" => ((ini_get('safe_mode')) ? 'Enabled&nbsp;<span class="bad">Should not be set</span>' : 'Disabled&nbsp;<i class="fa fa-check fa-lg green"></i>'),
	                    " -- Register globals" => ((ini_get('register_globals')) ? 'Enabled <span class="bad">Should not be set</span>' : 'Disabled&nbsp;<i class="fa fa-check fa-lg green"></i>'),
	                    "Database Requirements" => "",
	                    " -- Required Tables" => ((count($reqd_tables) == 0) ? 'All present&nbsp;<i class="fa fa-check fa-lg green"></i>' : '<span class="bad">Missing: '.join(", ", $reqd_tables).'</span>'),
	                )
	            );
	            break;
	        case 'php_config':
	            $columns = array("Setting", "Global Value", "Local Value");
	            $columnatts = array("setting" => " col-lg-3");
	            $data = ini_get_all();
	            $html = array();
	            foreach($data as $setting => $values){
	                $html[] = array("setting" => $setting, "global-value" => $values['global_value'], "local-value" => $values['local_value']);
	            }
	            $html = array("Configuration" => $html);
	            break;
	        case 'system_environ':
	            $dbinfoarray = $_db_control->getDatabaseServerInfo();
	            $dbinfo = null;
	            foreach($dbinfoarray as $k => $v) $dbinfo .= $k.": ".$v."<br/>";
	            $html = array(
	                "Configuration" => array(
	                    "PHP System" => phpversion()." ".((version_compare(phpversion(), '5.2.0', '<')) ? '<span class="bad">Minimum version 5.2.0 required</span>' : '<i class="fa fa-check fa-lg green"></i>'),
	                    "Database Server" => (defined('DBHOST')) ? $dbinfo : 'Unknown',
	                    "Web Server" => getenv('SERVER_SOFTWARE')
	                )
	            );
	            break;
	        case 'updates_monitor':
	            break;
	        case 'error_log':
	            if(file_exists(SITE_PATH.ADMIN_FOLDER.CACHE_FOLDER."error.log")){
	                $columns = array("date", "time", "error");
	                $columnatts = array("error" => " col-lg-3");
	                $arry = $_filesys->parseTextFile(SITE_PATH.ADMIN_FOLDER.CACHE_FOLDER."error.log", PHP_EOL, " - ");
	                $html = array(
	                    "Events" => array()
	                );
	                foreach($arry as $event){
	                    if(count($event) == 2){
	                        $html["Events"][] = array("date" => date("Y-m-d", strtotime($event[0])), "time" => date("H:i:s", strtotime($event[0])), "error" => $event[1]);
	                    }
	                }
	                $actions = array("clearallevents" => "<a class=\"reportclearerrorevents\">Clear All Events</a>");
	            }
	            break;
	        case 'audit_log':
	            $columns = array("date", "source", "category", "type", "username", "notes", "actions");
	            $events = $_error->getEventLog();
	            $html = array(
	                "Events" => $events
	            );
	            break;
	        case 'seo_errors':
	            break;
	    }

	    if(is_array($html)){
	        foreach($html as $label => $section){
	            echo '<div><strong>'.$label.'</strong></div>';
	            if(!is_null($columns)){
	                echo '<div class="setreportrow">';
	                foreach($columns as $column){
	                    $key = codify($column);
	                    echo '<span class="setreportcol'.((isset($columnatts[$key])) ? $columnatts[$key] : ' col-lg-3').'"><strong>'.ucwords($column).'</strong></span>';
	                }
	                echo '</div>'.PHP_EOL;
	            }
	            if(is_array($section)){
	                foreach($section as $label => $info){
	                    if(!is_array($info)){
	                        echo '<span class="setreportlabel col-lg-4">'.$label.'</span>';
	                        echo '<span class="setreportinfo col-lg-6">'.$info.'</span>';
	                    }elseif(count($info) == 1){
	                        echo '<span class="setreportlabel col-lg-4">'.$label.'</span>';
	                        foreach($info as $sublabel => $subinfo){
	                            echo '<span class="setreportlabel col-lg-4">'.$sublabel.'</span>';
	                            echo '<span class="setreportinfo col-lg-8">'.$subinfo.'</span>';
	                        }
	                    }elseif($report == 'audit_log'){
	                        echo '<div class="setreportrow" rel="'.$info['user_id'].'|'.$info['category'].'">';
	                        foreach($columns as $column){
	                            $key = codify($column);
	                            if($column == 'actions'){
	                                if(intval($info['user_id']) > 0) echo '<a class="setreportcol reportresetuseraudit" rel="'.$info['user_id'].'|'.$info['category'].'">Reset '.$info['category'].' Activity for This User</a>';
	                            }else{
	                                echo '<span class="setreportcol'.((isset($columnatts[$key])) ? $columnatts[$key] : ' col-lg-3').'">'.ucwords(getIfSet($info[$column])).'&nbsp;</span>';
	                            }
	                        }
	                        echo '</div>'.PHP_EOL;
	                    }else{
	                        echo '<div class="setreportrow">';
	                        foreach($columns as $column){
	                            $key = codify($column);
	                            echo '<span class="setreportcol'.((isset($columnatts[$key])) ? $columnatts[$key] : ' col-lg-3').'">'.getIfSet($info[$key]).'&nbsp;</span>';
	                        }
	                        echo '</div>'.PHP_EOL;
	                    }
	                }
	            }
	            if(count($actions) > 0){
	                echo '<div class="setreportactions">';
	                foreach($actions as $key => $value){
	                    echo $value;
	                }
	                echo '</div>'.PHP_EOL;
	            }
	        }
	    }

	    return true;
	}

	/**
	 * List taxonomies with HTML controls
	 * @return html
	 */
	function showSettingsTaxonomyEditor($return = false){
		global $_tax, $_events, $_data, $_menus, $_db_control;

		$taxonomies = $_tax->taxonomies;
		$tax_aliases = $_data->taxonomy_aliases;

		if($return) ob_start();
		?>
            <div class="table-head col-lg-12 gap-5 dottedborder-bottom row">
                <div class="col-lg-2">Taxonomy Name</div>
                <div class="col-lg-2">Code</div>
                <div class="col-lg-2">Parent Taxonomy</div>
                <div class="col-lg-2">Alias</div>
                <div class="col-lg-1">Type</div>
                <div class="col-lg-2">Linked Menu(s)</div>
                <div class="col-lg-1">Status</div>
            </div>
            <div id="tax_edit_row" class="col-lg-12 gap-5 dottedborder smallerfont bottom-margin hidden row">
            	<?php
            	$tax_parents_menu = '<select id="tax_edit_parent_id" name="tax[parent_id]"><option value="">- None -</option>';
            	foreach($taxonomies as $tax) $tax_parents_menu .= '<option value="'.$tax['id'].'">'.$tax['name'].'</option>';
            	$tax_parents_menu .= '</select>';
            	$tax_aliases_menu = '<select id="tax_edit_data_alias" name="tax[data_alias]"><option value="">- Select -</option>';
            	foreach($tax_aliases as $id => $alias) $tax_aliases_menu .= '<option value="'.$id.'">'.$alias['alias'].'</option>';
            	$tax_aliases_menu .= '</select>';
            	$tax_term_menu = '<span id="tax_edit_create_menu"><input type="checkbox" id="tax_create_menu" value="1" name="tax[create_menu]" /> Create an Admin Terms Menu</span>&nbsp;';
 				echo <<<EOT
 				<input type="hidden" name="tax_edit_id" id="tax_edit_id" value="" />
                <div class="tax-edit-name col-lg-2"><input type="text" id="tax_edit_name" name="tax[name]" value="" style="width: 90%" /></div>
                <div class="tax-edit-code col-lg-2"><input type="text" id="tax_edit_code" name="tax[code]" value="" style="width: 90%" disabled="disabled" /></div>
                <div class="tax-edit-parent-id col-lg-2">{$tax_parents_menu}</div>
                <div class="tax-edit-alias col-lg-2">Existing alias: {$tax_aliases_menu}<br/><div class="top-padding">Or, new alias: <input type="text" id="tax_edit_alias" name="tax[alias]" value="" /></div></div>
                <div class="tax-edit-type col-lg-1"><select id="tax_edit_type" name="tax[type]"><option value="category">Category</option><option value="tag">Tag List</option><option value="faq">FAQ List</option><option value="link">Link List</option></select></div>
                <div class="tax-edit-term-menus col-lg-2">{$tax_term_menu}</div>
                <div class="tax-edit-status col-lg-1"><input type="button" id="tax_edit_save" value="Save" />&nbsp;<input type="button" id="tax_edit_close" value="Close" /></div>
EOT;
            	?>
            </div>
 		<?php
 		if(!empty($taxonomies)){
 			foreach($taxonomies as $taxonomy){
 				$num_linked_menus = $_db_control->setTable(MENUS_TABLE)->setWhere("taxonomy_id = '{$taxonomy['id']}'")->getRecNumRows();
 				$status = (($taxonomy['active']) ? '<span class="tax-state">Active</span> <a href="#" class="tax-disable fa fa-pause left-gutter" title="Deactivate"></a>' : '<span class="tax-state">Inactive</span> <a href="#" class="tax-enable fa fa-play green left-gutter" title="Activate"></a>');
 				$delete = (($num_linked_menus == 0) ? '<a class="tax-delete fa fa-trash red left-gutter" title="Delete" href="#"></a>' : '<i class="fa fa-trash gray left-gutter" title="Cannot delete while taxonomy contains terms or associate with menu"></i>');
 				$parent = $_tax->getTaxonomy($taxonomy['parent_id']);
 				if(empty($parent)) $parent['name'] = null;
 				if($taxonomy['data_alias'] > 0){
 					$data_alias = $taxonomy['alias'];
 					$data_alias_id = $taxonomy['data_alias'];
 				}else{
 					$data_alias = '- Not set -';
 					$data_alias_id = 0;
 				}
				$menus = $_menus->getAdminMenuRecords(-1, array('taxonomy_id' => $taxonomy['id'], 'targettype' => 'terms'));
				$tax_term_menus = null;
				$tax_term_menus_rel = null;
				if(!empty($menus)){
					$tax_term_menus_rel = ' has-menus';
					foreach($menus as $menu)
						$tax_term_menus .= ((!is_null($tax_term_menus)) ? ', ' : '').$menu['title'];
				}
				// if(empty($tax_term_menus)) $tax_term_menus = '<input type="checkbox" value="1" name="tax[create_menu]" class="tax_menus" /> Create an Admin Terms Menu';
				$tax_term_menus .= '&nbsp;';
 				echo <<<EOT
 			<div class="tax-row table-body col-lg-12 gap-5 settings_hover_row dottedborder-bottom smallerfont row">
 				<input type="hidden" name="tax_id[]" class="tax_id" value="{$taxonomy['id']}" />
                <div class="tax-name col-lg-2"><a href="#" class="tax_edit" title="Click to edit">{$taxonomy['name']}&nbsp;<i class="fa fa-pencil"></i></a><input type="hidden" name="tax_name[]" class="tax_name" value="{$taxonomy['name']}" />&nbsp;</div>
                <div class="tax-code col-lg-2">{$taxonomy['code']}<input type="hidden" name="tax_code[]" class="tax_code" value="{$taxonomy['code']}" />&nbsp;</div>
                <div class="tax-parent-id col-lg-2">{$parent['name']}<input type="hidden" name="tax_parent_id[]" class="tax_parent_id" value="{$taxonomy['parent_id']}" />&nbsp;</div>
                <div class="tax-alias col-lg-2">{$data_alias}<input type="hidden" name="tax_data_alias_id[]" class="tax_data_alias_id" value="{$data_alias_id}" />&nbsp;</div>
                <div class="tax-type col-lg-1">{$taxonomy['type']}<input type="hidden" name="tax_type[]" class="tax_type" value="{$taxonomy['type']}" />&nbsp;</div>
                <div class="tax-term-menus col-lg-2{$tax_term_menus_rel}">{$tax_term_menus}</div>
                <div class="tax-status col-lg-1">{$status}&nbsp;{$delete}</div>
            </div>
EOT;
 			}
 		}
 		if($return){
 			$html = ob_get_clean();
 			return $html;
 		}
	}

	/**
	 * Return number of settings issues (critical, warning or informational)
	 * @param string $section               Settings section
	 * @return integer
	 */
	function getSettingsIssuesCount($section = ""){
	    if($section == ""){
	        // count from all sections
	        $count = 0;
	        foreach($this->settings_issues as $section => $section_issues){
	            $count += count($this->settings_issues[$section]);
	        }
	        return $count;
	    }else{
	        // count from single section
	        $count = countIfSet($this->settings_issues[$section]);
	        if(strpos($section, "-info") === false) $count += countIfSet($this->settings_issues[$section."-info"]);
	        return $count;
	    }
	}

	/**
	 * Output settings issues (notices at top of dialog or top of tabs)
	 * @param string $section
	 * @param boolean $rtn True to return HTML code rather than outputting it
	 */
	function showSettingsIssues($section = "", $rtn = false){
		global $_themes;

	    $themepath = $_themes->getThemePathUnder("admin");
	    $html = '';
	    $bullet = "&nbsp;&nbsp;&bull;&nbsp;";
	    if($section == ""){
	        if($this->getSettingsIssuesCount() == 0) return false;

	        $warn = 0;
	        $info = 0;
	        foreach($this->settings_issues as $section => $section_issues){
	            if(substr($section, -5) == '-info') { $info += count($section_issues); } else { $warn += count($section_issues); }
	        }
	        $html = (($warn > 0) ? "<img src=\"".WEB_URL.$themepath."images/icons/warning.png\" border=\"0\" alt=\"\" />&nbsp;{$warn} critical issue(s).  " : "");
	        $html.= (($info > 0) ? "<img src=\"".WEB_URL.$themepath."images/icons/info_button_16.png\" border=\"0\" alt=\"\" />&nbsp;{$info} notice(s)." : "");
	    }else{
	        if(count($this->settings_issues[$section]) > 0) $html = $bullet.join("<br/>".$bullet, $this->settings_issues[$section]);
	        if(strpos($section, '-info') === false && count($this->settings_issues[$section.'-info']) > 0) $html .= (($html != '') ? '<br/><br/>' : '').$bullet.join("<br/>".$bullet, $this->settings_issues[$section.'-info']);
	    }
	    if($rtn) {
	        return $html;
	    }else{
	        echo $html;
	    }
	}

	/**
	 * Output settings indicator icon (shown on tabs when issues are present)
	 * @param unknown_type $section
	 */
	function showSettingsIssuesIndicator($section){
		global $_themes;

		if(!isset($this->settings_issues[$section])){
			$this->settings_issues[$section] = array();
			$this->settings_issues[$section."-info"] = array();
		}

	    $themepath = $_themes->getThemePathUnder("admin");

	    $html = "";
	    $crit_issues = getIfSet($this->settings_issues[$section]);
	    $info_issues = getIfSet($this->settings_issues[$section."-info"]);
	    if(count($crit_issues) > 0 && is_array($crit_issues)){
	        $alt = count($crit_issues)." critical issue(s)";
	        $html = "&nbsp;<img src=\"".WEB_URL.$themepath."images/icons/warning.png\" border=\"0\" title=\"{$alt}\" alt=\"{$alt}\" />";
	    }elseif(count($info_issues) > 0 && is_array($info_issues)){
	        $alt = count($info_issues)." information notice(s)";
	        $html = "&nbsp;<img src=\"".WEB_URL.$themepath."images/icons/info_button_16.png\" border=\"0\" title=\"{$alt}\" alt=\"{$alt}\" />";
	    }
	    echo $html;
	}

	/**
	 * Output action objects (buttons, links, menus)
	 * @param integer $curindex
	 * @param array $objects Type of buttons, links, menu.  ['type']['id']['options']
	 */
	function showSettingsActions($curindex = 0, $objects = null){
	    foreach($objects as $type => $obj){
	        if(is_array($obj)){
	            $type = strtolower($type);
	            if($type == 'links'){
	                $index = 0;
	                foreach($obj as $id => $val){
	                    if($index > 0) echo "|&nbsp;";
	                    if($index == $curindex) $val = "<b>{$val}</b>";
	                    if(substr($id, 0, 4) == 'url:') {
	                        $attr = "href=\"".substr($id, 4)."\"";
	                    }else{
	                        $attr = "id=\"{$id}\"";
	                    }
	                    echo "<a {$attr}>{$val}</a>\n";
	                    $index++;
	                }
	            }elseif($type == 'buttons'){
	                foreach($obj as $id => $val){
	                    echo "<input type=\"button\" id=\"{$id}\" value=\"{$val}\" />\n";
	                }
	            }elseif($type == 'menu'){
	                // id
	                // options ---- val => text
	                $id = ((isset($obj['id'])) ? ' id="'.$obj['id'].'"' : '');
	                $class = ((isset($obj['class'])) ? ' class="'.$obj['class'].'"' : '');
	                $rel = ((isset($obj['rel'])) ? ' rel="'.$obj['rel'].'"' : '');
	                echo "<select{$id}{$class}{$rel}>\n";
	                $obj['options'] = getIfSet($obj['options']);
	                foreach($obj['options'] as $val => $text){
	                    $sel = (($val == $obj['sel']) ? ' selected="selected"' : '');
	                    echo "<option value=\"{$val}\"{$sel}>{$text}</option>\n";
	                }
	                echo "</select>\n";
	            }elseif($type == 'checkbox'){
	                $id = ((isset($obj['id'])) ? ' id="'.$obj['id'].'"' : '');
	                $class = ((isset($obj['class'])) ? ' class="'.$obj['class'].'"' : '');
	                $rel = ((isset($obj['rel'])) ? ' rel="'.$obj['rel'].'"' : '');
	                echo "<input type=\"checkbox\"{$id}{$class}{$rel} value=\"".getIfSet($obj['val'])."\" />\n";
	            }
	        }
	    }
	}

	/**
	 * Return array of user-allowed settings manager tabs
	 * @return array            Name => URL
	 */
	function getSettingsTabs(){
	    global $_users, $_events, $_db_control;

	    $tabs = $this->tabs;
        $tabs[SETTINGS_TAB_GENERAL] = array(WEB_URL.ADMIN_FOLDER.'settings?tab=general');
        $tabs[SETTINGS_TAB_MEDIA] = array(WEB_URL.ADMIN_FOLDER.'settings?tab=media');
        $tabs[SETTINGS_TAB_MEDIA]['Universal Settings'] = WEB_URL.ADMIN_FOLDER.'settings?tab=media&sub=media_basic';
        $tabs[SETTINGS_TAB_MEDIA]['Media Formats'] = WEB_URL.ADMIN_FOLDER.'settings?tab=media&sub=media_formats';
        $tabs[SETTINGS_TAB_EDITORS] = array(WEB_URL.ADMIN_FOLDER.'settings?tab=editors');
        $tabs[SETTINGS_TAB_THEMES] = array(WEB_URL.ADMIN_FOLDER.'settings?tab=themes');
        $tabs[SETTINGS_TAB_THEMES]['Website Themes'] = WEB_URL.ADMIN_FOLDER.'settings?tab=themes&sub=theme_website';
        $tabs[SETTINGS_TAB_THEMES]['Admin Themes'] = WEB_URL.ADMIN_FOLDER.'settings?tab=themes&sub=theme_admin';
        $tabs[SETTINGS_TAB_MENUS] = array(WEB_URL.ADMIN_FOLDER.'settings?tab=menus');
        $tabs[SETTINGS_TAB_MENUS]['Website Menus'] = WEB_URL.ADMIN_FOLDER.'settings?tab=menus&sub=menus_website';
        $tabs[SETTINGS_TAB_MENUS]['Admin Menus'] = WEB_URL.ADMIN_FOLDER.'settings?tab=menus&sub=menus_admin';
        $tabs[SETTINGS_TAB_TAX] = array(WEB_URL.ADMIN_FOLDER.'settings?tab=taxonomies');
        $tabs[SETTINGS_TAB_PLUGINS] = array(WEB_URL.ADMIN_FOLDER.'settings?tab=plugins');
        $tabs[SETTINGS_TAB_PLUGINS]['Installed'] = WEB_URL.ADMIN_FOLDER.'settings?tab=plugins&sub=plugin_installed';
        $tabs[SETTINGS_TAB_PLUGINS]['Extensions'] = WEB_URL.ADMIN_FOLDER.'settings?tab=plugins&sub=plugin_extensions';
        $tabs[SETTINGS_TAB_PLUGINS]['Problem/Deleted'] = WEB_URL.ADMIN_FOLDER.'settings?tab=plugins&sub=plugin_problem';
        $tabs[SETTINGS_TAB_PLUGINS]['Frameworks'] = WEB_URL.ADMIN_FOLDER.'settings?tab=plugins&sub=plugin_frameworks';
        $tabs[SETTINGS_TAB_PLUGINS]['Settings'] = WEB_URL.ADMIN_FOLDER.'settings?tab=plugins&sub=plugin_settings';
        $tabs[SETTINGS_TAB_PLUGINS]['Updates'] = WEB_URL.ADMIN_FOLDER.'settings?tab=plugins&sub=plugin_updates';
        $tabs[SETTINGS_TAB_PLUGINS]['Foundry Repository'] = WEB_URL.ADMIN_FOLDER.'settings?tab=plugins&sub=plugin_repo';
        $tabs[SETTINGS_TAB_USERS] = array(WEB_URL.ADMIN_FOLDER.'settings?tab=users');
        $tabs[SETTINGS_TAB_ADVANCED] = array(WEB_URL.ADMIN_FOLDER.'settings?tab=advanced');
        $tabs[SETTINGS_TAB_ADVANCED]['Aliases'] = WEB_URL.ADMIN_FOLDER.'settings?tab=advanced&sub=adv_aliases';
        $tabs[SETTINGS_TAB_ADVANCED]['SEO'] = WEB_URL.ADMIN_FOLDER.'settings?tab=advanced&sub=adv_links';
        $tabs[SETTINGS_TAB_ADVANCED]['Users Manager'] = WEB_URL.ADMIN_FOLDER.'settings?tab=advanced&sub=adv_users';
        $tabs[SETTINGS_TAB_ADVANCED]['Site Visibility'] = WEB_URL.ADMIN_FOLDER.'settings?tab=advanced&sub=adv_visibility';
        $tabs[SETTINGS_TAB_ADVANCED]['Files and Mail'] = WEB_URL.ADMIN_FOLDER.'settings?tab=advanced&sub=adv_filesys';
        $tabs[SETTINGS_TAB_ADVANCED]['Tuning'] = WEB_URL.ADMIN_FOLDER.'settings?tab=advanced&sub=adv_tuning';
        $tabs[SETTINGS_TAB_ADVANCED]['Cron'] = WEB_URL.ADMIN_FOLDER.'settings?tab=advanced&sub=adv_cron';
        $tabs[SETTINGS_TAB_ADVANCED]['Error Handling'] = WEB_URL.ADMIN_FOLDER.'settings?tab=advanced&sub=adv_debugger';
        $tabs[SETTINGS_TAB_ADVANCED]['Reports'] = WEB_URL.ADMIN_FOLDER.'settings?tab=advanced&sub=adv_reports';
        $tabs[SETTINGS_TAB_ADVANCED]['Core'] = WEB_URL.ADMIN_FOLDER.'settings?tab=advanced&sub=adv_core';

	    list($triggered_tabs) = $_events->processTriggerEvent("setCustomSettingsTab");

	    if(!empty($triggered_tabs) && is_array($triggered_tabs)) {
	    	$new_tabs = array();
	    	$this->setCustomSettingsTab($triggered_tabs);
	    	$custom_tabs = $this->custom_tabs;

	    	// cycle through tabs and insert custom tabs at key points
	    	foreach($tabs as $key => $tab){
	    		if(isset($custom_tabs[$key])){
	    			foreach($custom_tabs[$key] as $custom_key => $custom_tab){
		    			if(!empty($custom_tab[0]['aftersubkey']) && isset($tab[$custom_tab[0]['aftersubkey']])){
		    				// custom tab is to be added after existing, matched subtab
		    				$new_sub_tabs = array();
		    				$after_sub_key = $custom_tab[0]['aftersubkey'];
		    				unset($custom_tab[0]['aftersubkey']);
		    				foreach($tab as $sub_key => $sub_tab){
		    					$new_sub_tabs[$sub_key] = $sub_tab;
		    					if($sub_key === $after_sub_key)
		    						$new_sub_tabs[$custom_key] = $custom_tab;  // add it now
		    				}
		    				$new_tabs[$key] = $new_sub_tabs;
		    			}elseif(!empty($custom_tab[0]['aftersubtabs']) && $custom_tab[0]['aftersubtabs'] == true){
		    				// custom tab is to be added after all subtabs
		    				$new_sub_tabs = $tab;
		    				unset($custom_tab[0]['aftersubtabs']);
    						$new_sub_tabs[$custom_key] = $custom_tab;  // add it now
		    				$new_tabs[$key] = $new_sub_tabs;
		    			}else{
		    				// custom tab is to be added after the top tab
				    		$new_tabs[$key] = $tab;
			    			$new_tabs = $new_tabs + $custom_tabs[$key];
		    			}
		    		}
	    		}else{
	    			$new_tabs[$key] = $tab;
	    		}
	    	}
	    }else{
	    	$new_tabs = $tabs;
	    }
	    $this->tabs = $new_tabs;
	    return $this->tabs;
	}

	/**
	 * Return array of custom settings tabs (tabs added by a plugin or other program)
	 * @return array
	 */
	function getCustomSettingsTabs(){
	    return $this->custom_tabs;
	}

	/**
	 * Save settings tab data to database, at specific location in or at end of collection.
	 * Usable in a plugin or theme to create custom settings tabs programmatically
	 * @param array $args 				Custom tab arguments:
	 * 										$key               	Tab key
	 *                                      					Tab will be updated if key exists,
	 *                                      					otherwise it will be created and a new key will be generated and returned
	 *                                      					Tab key cannot be one of the system tab keys
	 * 										$parent            	Tab key of parent tab
	 * 										$afterkey          	Tab key that precedes (is to the left of) this tab
	 * 										$title				Tab title
	 * 										$content_callback  	The function that returns the tab content HTML
	 * 										$shared_content_callback
	 *															The function that returns the shared tab content HTML
	 * 										$validator_callback	The function that handles tab field validation and saving
	 * 										$url          		The URL that will set this tab as current
	 * 										$allowance   		The allowance (standard or custom) that will be tested in order to display tab
	 * @return array                    True if successful and tab key
	 */
	function setCustomSettingsTab($args){
	    global $_events;

		$_events->processTriggerEvent(__FUNCTION__, $args);

	    $ok = false;
	    if(!is_array($args)) return array($ok, null);
        $tabs = $this->getCustomSettingsTabs();

        foreach($args as $tabset){
		    $key = getIfSet($tabset['key']);
		    $parent = getIfSet($tabset['parent']);
		    $title = getIfSet($tabset['title']);
		    $url = getIfSet($tabset['url']);
		    $afterkey = getIfSet($tabset['afterkey']);
		    $aftersubkey = getIfSet($tabset['aftersubkey']);
		    $aftersubtabs = getIfSet($tabset['aftersubtabs']);
		    $content_callback = getIfSet($tabset['content_callback']);
		    $shared_content_callback = getIfSet($tabset['shared_content_callback']);
		    $validator_callback = getIfSet($tabset['validator_callback']);
		    $allowance = getIfSet($tabset['allowance']);

		    if(!empty($title) && !empty($key) && (!empty($content_callback) || !empty($shared_content_callback))) {
		        $updated = false;
		        if($afterkey == "") $afterkey = SETTINGS_TAB_ADVANCED;

		        if(isset($tabs[$key])){
		            // update tab object
		            $tabobj = array(
		            	array(
		            		"key" => $key,
		            		"title" => $title,
		                	"content_callback" => $content_callback,
		                	"shared_content_callback" => $shared_content_callback,
		                	"validator_callback" => $validator_callback,
			                "allowance" => $allowance,
			                "url" => $url,
			                "aftersubkey" => $aftersubkey,
			                "aftersubtabs" => $aftersubtabs
			            )
		            );
		            $tabs[$afterkey][$title] = $tabobj;
		        }else{
		            // create tab object
		            if(empty($key)) $key = codify($title);
		            $std_tab_keys = array(
		                SETTINGS_TAB_ADVANCED, SETTINGS_TAB_USERS, SETTINGS_TAB_PLUGINS, SETTINGS_TAB_MENUS,
		                SETTINGS_TAB_THEMES, SETTINGS_TAB_MEDIA, SETTINGS_TAB_GENERAL.
		                SETTINGS_SUBTAB_ADVANCED_CORE, SETTINGS_SUBTAB_ADVANCED_REPORTS, SETTINGS_SUBTAB_ADVANCED_ERRORS,
		                SETTINGS_SUBTAB_ADVANCED_CRON, SETTINGS_SUBTAB_ADVANCED_TUNING, SETTINGS_SUBTAB_ADVANCED_VISIBILITY,
		                SETTINGS_SUBTAB_ADVANCED_SEO, SETTINGS_SUBTAB_ADVANCED_ALIASES, SETTINGS_SUBTAB_MENUS_ADMIN,
		                SETTINGS_SUBTAB_MENUS_WEBSITE, SETTINGS_SUBTAB_THEMES_ADMIN, SETTINGS_SUBTAB_THEMES_WEBSITE,
		                SETTINGS_SUBTAB_MEDIA_UNIVERSAL, SETTINGS_SUBTAB_MEDIA_FORMATS, SETTINGS_SUBTAB_PLUGINS_INSTALLED,
		                SETTINGS_SUBTAB_PLUGINS_EXTENSION, SETTINGS_SUBTAB_PLUGINS_PROBLEM, SETTINGS_SUBTAB_PLUGINS_FRAMEWORKS,
		                SETTINGS_SUBTAB_PLUGINS_SETTINGS, SETTINGS_SUBTAB_PLUGINS_UPDATES, SETTINGS_SUBTAB_PLUGINS_REPOSITORY
		            );
		            foreach($std_tab_keys as $kv) if(strtolower($key) == $kv) {
		                trigger_error(__FUNCTION__.": Cannot use key '$key'. Already a system key");
		                return false;
		            }

		            $tabobj = array(
		            	array(
		            		"key" => $key,
		            		"title" => $title,
		                	"content_callback" => $content_callback,
		                	"shared_content_callback" => $shared_content_callback,
		                	"validator_callback" => $validator_callback,
			                "allowance" => $allowance,
			                "url" => $url,
			                "aftersubkey" => $aftersubkey,
			                "aftersubtabs" => $aftersubtabs
			            )
		            );

		            $tabs[$afterkey][$title] = $tabobj;
		        }
		    }
		}
    	$this->custom_tabs = $tabs;
	    return array($ok, $key);
	}

	/**
	 * Display a custom settings tab
	 * @param string $key
	 * @param array $tabarry 			The custom tab arguments
	 * 										$title				Tab title
	 * 										$content_callback  	The function that returns the tab content HTML
	 * 										$allowance   		The allowance (standard or custom) that will be tested in order to display tab
	 */
	function showCustomSettingsTab($key, $tabarry){
		global $_users;

	    if(!is_array($tabarry)) return false;
        if(empty($tabarry['content_callback']) || !empty($tabarry['shared_content_callback']) || empty($tabarry['key'])) return false;

		$allowed = true;
		if(isset($tabarry['allowance']))
			$allowed = $_users->userIsAllowedTo($tabarry['allowance']);

	    if($allowed) {
	    	$title = getIfSet($tabarry['title']);
	    	$tabkey = getIfSet($tabarry['key']);
	    	echo '<li><a href="#tabs-'.$tabkey.'" class="settingstabs_link settingstab_custom">'.$title.$this->showSettingsIssuesIndicator($key).'</a></li>';
	    	echo PHP_EOL;
	    }
	}

	/**
	 * Display a custom settings tab's content via content_callback function
	 * @param array $tabarry 			The custom tab contents arguments
	 * 										$key               	Tab key
	 *                                      					Tab will be updated if key exists,
	 *                                      					otherwise it will be created and a new key will be generated and returned
	 *                                      					Tab key cannot be one of the system tab keys
	 * 										$parent            	Tab key of parent tab
	 * 										$content_callback  	The function that returns the tab content HTML
	 * 										$shared_content_callback
	 *															Must be null, empty or omitted
	 * 										$allowance   		The allowance (standard or custom) that will be tested in order to display tab
	 */
	function showCustomSettingsTabContent($tabarry){
	    global $_users;

	    if(!is_array($tabarry)) return false;
        if(empty($tabarry['content_callback']) || !empty($tabarry['shared_content_callback']) || empty($tabarry['key'])) return false;

	    $allowed = true;
	    if(isset($tabarry['allowance']) && !empty($tabarry['allowance']))
	    	$allowed = $_users->userIsAllowedTo($tabarry['allowance']);

	    $content_callback = $tabarry['content_callback'];
	    if(!empty($content_callback) && function_exists($content_callback)){
	        $html = $content_callback();
	        echo '<div id="tabs-'.$tabarry['key'].'">';
	        echo $html;
	        echo '</div>';
	    }
	    return true;
	}

	/**
	 * Display all custom subtabs, under the selected tab $aftertab
	 * @param string $afterkey              Key of tab in which the sub tabs will be displayed
	 * @param boolean $echo 				True to output results, false to return it
	 */
	function showCustomSettingsSubTabs($afterkey, $echo = true){
		global $_users, $_debug;

	    if(empty($this->custom_tabs)) $this->getSettingsTabs();
	    $tabs = $this->custom_tabs;

		$html = null;
	    if(isset($tabs[$afterkey]) && is_array($tabs[$afterkey])){
	        foreach($tabs[$afterkey] as $tabkey => $tabobj){
	        	if(isset($tabobj[0]['content_callback'])){
	                // check if user has allowance to view tab content_callbacks
	                $allowed = false;
	                if(!isBlank($tabobj[0]['allowance'])){
	                    if($_users->userIsAllowedTo($tabobj[0]['allowance'])) $allowed = true;
	                }else{
	                    $allowed = true;
	                }
	                if($allowed){
	                    $html .= '<li><a href="#'.$tabobj[0]['key'].'" class="settingstabs_sublink">'.$tabobj[0]['title'].'</a></li>'.PHP_EOL;
	                }
	            }
	        }
	    }
	    if($echo)
	    	echo $html;
	    else
			return $html;
	}

	/**
	 * Display the custom tab content in selected tab, positioned below selected tab content.
	 * @param string $afterkey              Key of tab in which the content_callback function's contents will be displayed
	 * @param boolean $echo 				True to output results, false to return it
	 */
	function showCustomSettingsSubTabsContent($afterkey, $echo = true){
	    global $_users, $_debug;

	    if(empty($this->custom_tabs)) $this->getSettingsTabs();
	    $tabs = $this->custom_tabs;

	    $html = null;
	    if(isset($tabs[$afterkey]) && is_array($tabs[$afterkey])){
	        foreach($tabs[$afterkey] as $tabkey => $tabobj){
	        	if(isset($tabobj[0]['content_callback'])){
		            $content_callback = $tabobj[0]['content_callback'];
		            if(!empty($content_callback) && function_exists($content_callback)) {
		                // check if user has allowance to view tab content_callbacks
		                $allowed = false;
		                if(!isBlank($tabobj[0]['allowance'])){
		                    if($_users->userIsAllowedTo($tabobj[0]['allowance'])) $allowed = true;
		                }else{
		                    $allowed = true;
		                }
		                if($allowed){
		                    if(function_exists($content_callback))
		                        if($echo)
		                        	echo $content_callback();
		                       	else
		                       		$html .= $content_callback();
		                }
		            }
		        }
	        }
	    }
	    if(!$echo) return $html;
	}

	/**
	 * Display the custom tab content in selected tab, positioned below selected tab content.
	 * @param string $afterkey              Key of tab in which the content_callback function's content will be displayed
	 * @param string $aftersubkey           Key of subtab in which the content_callback function's contents will be displayed
	 * 											Leave null to append content to main tab
	 * @param boolean $echo 				True to output results, false to return it
	 */
	function showCustomSettingsTabContentAppended($afterkey, $aftersubkey = null, $echo = true){
	    global $_users, $_debug;

	    if(empty($this->custom_tabs)) $this->getSettingsTabs();
	    $tabs = $this->custom_tabs;

	    $html = null;
	    if(isset($tabs[$afterkey]) && is_array($tabs[$afterkey])){
	        foreach($tabs[$afterkey] as $tabkey => $tabobj){
	        	$show = false;
	        	if(is_null($aftersubkey) || $tabobj[0]['aftersubkey'] == $aftersubkey) $show = true;

	        	if(isset($tabobj[0]['shared_content_callback']) && $show){
		            $content_callback = $tabobj[0]['shared_content_callback'];
		            if(!empty($content_callback) && function_exists($content_callback)) {
		                // check if user has allowance to view tab content_callbacks
		                $allowed = false;
		                if(!isBlank($tabobj[0]['allowance'])){
		                    if($_users->userIsAllowedTo($tabobj[0]['allowance'])) $allowed = true;
		                }else{
		                    $allowed = true;
		                }
		                if($allowed){
		                    if(function_exists($content_callback))
		                        if($echo)
		                        	echo $content_callback();
		                       	else
		                       		$html .= $content_callback();
		                }
		            }
		        }
	        }
	    }
	    if(!$echo) return $html;
	}

	/**
	 * Verify settings changes before saving to system
	 * @param boolean $is_adminsys
	 */
	function validateChanges($is_adminsys){
		global $_db_control, $_filesys, $_system, $_events;

		list($abort) = $_events->processTriggerEvent(__FUNCTION__, null, false);				// alert triggered functions when function executes
		if($abort) return false;

	    $loadpage = $_SERVER['HTTP_REFERER'];
		$errstr = array();
		$activetabs = $this->getSettingsTabs();

	    // Get the current settings values from the database
	    $settings = $_db_control->flattenDBArray($_db_control->setTable(SETTINGS_TABLE)->getRec(), "name", "value");

		// Config settings
		if(!$is_adminsys){
	        if(isset($_POST['newcfg'])){
	    		$newcfg = $_POST['newcfg'];
	            if(!isset($newcfg['CACHE_CSS'])) {
	            	$newcfg['CACHE_CSS'] = 0;
	            	$newcfg['COMPRESS_CSS'] = 0;
	            }
	            if(!isset($newcfg['CACHE_JS'])) $newcfg['CACHE_JS'] = 0;
	            if(!isset($newcfg['COMPRESS_CSS'])) $newcfg['COMPRESS_CSS'] = 0;
	            if(isset($newcfg['CACHE_LIFESPAN'])) $newcfg['CACHE_LIFESPAN'] = intval($newcfg['CACHE_LIFESPAN']);
	            $max_file_size = $_filesys->evaluateFileSize(ini_get('upload_max_filesize'));

	    		foreach($newcfg as $key => $value){
	    			$err = "";
	                $value = trim($value);
	    			switch ($key){
	    				case "BUSINESS":
	    				case "SITE_NAME":
	    				case "DB_TABLE_PREFIX":
	    					if(empty($value)) $err = "is missing";
	    					break;
	    				case "OWNER_EMAIL":
	    				case "ADMIN_EMAIL":
	    					if(empty($value)) $err = "is missing";
	    					break;
	    				case "IMG_MAX_UPLOAD_SIZE":
	    				case "THM_MAX_UPLOAD_SIZE":
	    				case "FILE_MAX_UPLOAD_SIZE":
	    					if(intval($value) > $max_file_size && $max_file_size > 0) $err = "is greater than ".$_filesys->formatFileSize($max_file_size);
	    					break;
	    				case "THM_UPLOAD_FOLDER":
	    				case "IMG_UPLOAD_FOLDER":
	    				case "FILE_UPLOAD_FOLDER":
	    					if(empty($value) || $value == "/") $err = "is missing";
	                        if(preg_match("/[\*\?\|\>\<]/i", $value)) $err = "contains invalid characters.";
	    					break;
	                    case "FTPPASS":
	                        if(!empty($newcfg['FTPHOST']) && !empty($newcfg['FTPUSER']) && !empty($newcfg['FTPPASS'])){
	                            $ftp = new Ftp;
	                            $ftp->connect($newcfg['FTPHOST']);
	                            $rtn = $ftp->login($newcfg['FTPUSER'], $newcfg['FTPPASS']);
	                            if($rtn !== true)
	                                $err = "FTP Connection Failed";
	                            else
	                                $ftp->close();
	                        }
	                        break;
	                    case "TEMPFOLDER":
	                        if(empty($value)) $value = $_filesys->getTempFolder();
	                        break;
	                    case "UPLOADTEMPFOLDER":
	                        if(empty($value)) $value = ini_get('upload_tmp_dir');
	                        break;
	                    case "SMTP_VALID":
	                        if($value == 'validated'){

	                        }
	                    case "SMTP_PORT":
	                    case "SMTP_SECURE_MODE":
	                        // if($newcfg['SMTP_HOST'] == '')
	                        //     $err = "is missing";
	                        // else if(intval($newcfg['SMTP_PORT']) < 1 || intval($newcfg['SMTP_PORT']) > 65336)
	                        //     $err = "is invalid or not a number between 1 and 65336";
	                    case "SMTP_VALID":
	                        // if($newcfg['SMTP_VALID'] != 'valid'){
	                        //     $err = "The SMTP settings haven't been checked.  Please click 'Test Connection'";
	                        // }
	                        // break;
	    			}

	                // Save standard config settings
	    			if(!empty($err)){
	    				// revert to previously saved value
	    				if(isset($_system->configs[$key]))
	    					$value = $_system->configs[$key];
	    				else
	    					$value = '';
	    				$errstr[] = str_replace("_", " ", $key)." ".$err;
	    			}else{
	    				// save changes
	                    $value = trim(str_replace("\\", "\\\\", $value));
	                    if(isset($_system->configs[$key])){
	                        $_db_control->setTable(SETTINGS_TABLE)->setFieldvals(array("name" => $key, "value" => $value))->setWhere("name = '$key'")->replaceRec();
	                    }else{
	                        $_db_control->setTable(SETTINGS_TABLE)->setFieldvals(array("name" => $key, "value" => $value))->insertRec();
	                    }
	                    $_system->configs[$key] = str_replace("\\\\", "\\", $value);
	    			}
	    		}
	        }

	        // Save extended config settings using settings tabs form process functions
	        foreach($activetabs as $tabkey => $tabarry){
	        	$tabkey = strtolower($tabkey);
	        	$errstrincl = array();
                if(isset($tabarry[0])){
                	// system tabs
                    if(!is_array($tabarry[0])){
                    	// system tab validator overrides
                    	list($errstrincl) = $_events->processTriggerEvent("settings_override_".$tabkey."_tab_validator");
                    	if(is_null($errstrincl)){
                    		// standard system tab form processors
				            $func = "settings_".$tabkey."_form_process";
				            if(function_exists($func)) $errstrincl = $func();
				        }
				    }

				    // custom tab validators
				    foreach($tabarry as $tabobj){
				    	if(is_array($tabobj)){
					    	foreach($tabobj as $tabelem){
						        if(isset($tabelem['validator_callback'])){
						        	$func = $tabelem['validator_callback'];
						        	if(function_exists($func)) $errstrincl = $func($tabelem);
						        }
						    }
						}
				    }
		            if(!is_array($errstrincl) && !empty($errstrincl))
		            	$errstrincl = array($errstrincl);
		            else
		            	$errstrincl = array();
		        }
	            $errstr = $errstr + $errstrincl;
	        }

	        // Process any trigger functions attached to the validatechanges event
	        list($errstrincl) = $_events->processTriggerEvent(__FUNCTION__);
			if(!is_array($errstrincl) && !empty($errstrincl)) {
				$errstrincl = array($errstrincl);
	            $errstr = $errstr + $errstrincl;
	        }
		}

		if(count($errstr) > 0){
	        $nl = "\\r\\n";
			print "<script type=\"text/javascript\">alert(\"One or more problems occurred:{$nl}{$nl}- ".join("$nl- ", $errstr)."{$nl}{$nl}The missing or invalid setting".((count($errstr) == 1) ? ' has' : 's have')." been reset.\");</script>";
		}else{
			$_filesys->gotoPage($loadpage);
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Return or output the settings menu HTML
	 * @param boolean $returnhtml              True to return the HTML, false to echo the HTML
	 * @return html if $returnhtml is true
	 */
	function showSettingsMenu($returnhtml = true){
	    $opt = $this->getSettingsTabs();
	    if($returnhtml) ob_start();
	    echo '<div id="admsettings_info">'.PHP_EOL;
	    echo '<h2>'.SYS_NAME.' Settings</h2>'.PHP_EOL;
	    echo '<div id="admsettings_quick_menu">'.PHP_EOL;
	    foreach($opt as $label => $url){
	        echo '<div class="admsettings_menu_item">';
	        if(!is_array($url)){
	            echo '<a class="admsettings_quick_menu_item" href="'.$url.'">'.$label.'</a>';
	        }else{
	            echo '<a class="admsettings_quick_menu_item" href="'.$url[0].'">'.$label.'</a>';
	            echo '<ul class="admsettings_quick_menu_sub">';
	            unset($url[0]);
	            foreach($url as $sublabel => $suburl){
	                echo '<li><a class="admsettings_quick_menu_sub_item" href="'.$suburl.'">'.$sublabel.'</a></li>';
	            }
	            echo '</ul>';
	        }
	        echo '</div>'.PHP_EOL;
	    }
	    echo '</div>'.PHP_EOL;
	    echo '</div>'.PHP_EOL;
	    if($returnhtml){
	        $html = ob_get_clean();
	        return $html;
	    }
	}

	/**
	 * Show an overlay with message for users who do not have permission to access tab
	 * @param boolean $permitted
	 */
	function showSettingsTabOverlay($permitted){
		if(!$permitted){
			echo '<div class="settingstabs-overlay">'.PHP_EOL;
			echo '<p>You do not have the appropriate permission to edit these settings.</p>'.PHP_EOL;
			echo '</div>'.PHP_EOL;
		}
	}
}

?>