<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("REMOTELIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

/* REMOTE SERVICES GLOBAL CLASS
 *
 * Abstract class that handles Remote Svc API calls
 */
abstract class RemoteSvcClass {
	protected $method = '';
    protected $endpoint = '';
    protected $verb = '';
    protected $args = Array();
    protected $file = null;
	protected static $_instance = null;

    public function __construct($request) {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");

        // $request should be set to either
        //      "endpoint/verb/args" or
        //      array("endpoint", "verb", "args") or
        //      array("__rest", "endpoint", "verb", "args") where the "__rest" loader redirector  is discarded

        if(!is_array($request)){
            $this->args = explode('/', rtrim($request, '/'));
        }else{
            if($request[0] == '__rest') array_shift($request);
            $this->args = $request;
        }
        $this->endpoint = array_shift($this->args);

        if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
            $this->verb = array_shift($this->args);
        }

        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }

        switch($this->method) {
            case 'DELETE':
            case 'POST':
                $this->request = $this->_cleanInputs($_POST);
                break;
            case 'GET':
                $this->request = $this->_cleanInputs($_GET);
                break;
            case 'PUT':
                $this->request = $this->_cleanInputs($_GET);
                $this->file = file_get_contents("php://input");
                break;
            default:
                $this->_response('Invalid Method', 405);
                break;
        }
    }

    public function processAPI() {
        if ((int)method_exists($this, $this->verb) > 0) {
            die('verb '.$this->verb.' found in endpoint '.$this->endpoint);
            return $this->_response($this->{$this->endpoint}($this->args));
        }
        return $this->_response('', 400);
    }

    private function _response($data, $status = 200) {
        header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
        return json_encode($data);
    }

    private function _cleanInputs($data) {
        $clean_input = Array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->_cleanInputs($v);
            }
        } else {
            $clean_input = trim(strip_tags($data));
        }
        return $clean_input;
    }

    private function _requestStatus($code) {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$code]) ? $status[$code] : $status[500];
    }
}

function initRestEndpoint(){
    global $_page;
    restTestInit($_page->urlpath);
}

function restTestInit($urlpath){
    global $_page;

    $incomingOrigin = array_key_exists('HTTP_ORIGIN', $_SERVER) ? $_SERVER['HTTP_ORIGIN'] : $_SERVER['HTTP_HOST'];
    $allowOrigin    = $_SERVER['HTTP_HOST'];
    if(is_array($_page->urlpath) && isset($_page->urlpath[1])){
        switch($_page->urlpath[1]){
            case 'page':
                $API = new PageAPI($urlpath, $incomingOrigin);
                break;
            case 'user':
                $API = new UserAPI($urlpath, $incomingOrigin);
                break;
        }
    }else{
        die('Error rendering urlpath attribute for _page object.');
    }
}


/* This abstraction would be in a third-party plugin/code that is registered in REST Endpoint Manager */
class UserAPI extends RemoteSvcClass{
    public function __construct($request, $origin) {
        parent::__construct($request);

        // $APIKey = new Models\APIKey();
        // $User = new Models\User();

        // if (!array_key_exists('apiKey', $this->request)) {
        //     throw new Exception('No API Key provided');
        // } else if (!$APIKey->verifyKey($this->request['apiKey'], $origin)) {
        //     throw new Exception('Invalid API Key');
        // } else if (array_key_exists('token', $this->request) &&
        //      !$User->get('token', $this->request['token']))

        //     throw new Exception('Invalid User Token');
        // }

        // $this->User = $User;

        try {
            echo $this->processAPI();
        } catch (Exception $e) {
            echo json_encode(Array('error' => $e->getMessage()));
        }
    }

    private function user($args){
        printr($args);
    }

    private function delete($args){
        printr($args);
    }
}

class PageAPI extends RemoteSvcClass{
    public function __construct($request, $origin) {
        parent::__construct($request);

        // $APIKey = new Models\APIKey();
        // $User = new Models\User();

        // if (!array_key_exists('apiKey', $this->request)) {
        //     throw new Exception('No API Key provided');
        // } else if (!$APIKey->verifyKey($this->request['apiKey'], $origin)) {
        //     throw new Exception('Invalid API Key');
        // } else if (array_key_exists('token', $this->request) &&
        //      !$User->get('token', $this->request['token']))

        //     throw new Exception('Invalid User Token');
        // }

        // $this->User = $User;

        try {
            echo $this->processAPI();
        } catch (Exception $e) {
            echo json_encode(Array('error' => $e->getMessage()));
        }
    }

    private function page($args){
        printr($args);
    }

    private function info($args){
        printr($args);
    }
}

?>