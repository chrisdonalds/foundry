<?php
// XML SITEMAP GENERATOR
//
// Author: Chris Donalds <cdonalds01@gmail.com>
// Date: January 13, 2010
// License: GPL
// -----------------------------------------------------
// NOTE: ONLY EDIT AFTER THE 'ONLY EDIT BELOW HERE' MARK
// -----------------------------------------------------
//
// *** Setting the Inclusions and Exclusions:
// *** (Lines 76 to 121)
//  dirs must either begin with http://, https:// or /
//  extensions must start with .
//
//  - Excluding (skipping) a file -
//		eg. excludeit('filename.php');
//  - Excluding (skipping) a folder and all subfolders -
//		eg. excludeit($dir.'/folder');
//  - Excluding (skipping) an extension -
//		eg. excludeit('.jpg');
//
//  - Including a file -
//		eg. includeit('filename.php');
//  - Including a folder and all contents -
//		eg. includeit($dir.'/folder');
//  - Including an extension -
//		eg. includeit('.jpg');
//
// *** Calling it from your browser:
//
//	www.website.com/inc/_plugins/sitemapgen/xmlsitemapgen.php{?option1=value{&option2=value}...}
//  TYPICAL CALL: www.website.com/inc/_plugins/sitemapgen/xmlsitemapgen.php?verbose=1
//
// Options List:
//  - Verbosity (DEFAULT = off) -
//	 verbose=1 (turns verbosity on -- outputs processing information to screen)
//	 verbose=0 (turns verbosity off -- no output to browser screen)
//
//	- Change Frequency (DEFAULT = weekly) -
//	 chgfreq=string (set to daily, weekly, monthly, quarterly, yearly to tell search engines how often to update)
//
//	- Priority (DEFAULT = 50%) -
//	 priority=number (set to number between 0.1 (10%) and 1.0 (100%))
//
//	- First Index Required (DEFAULT = off) -
//	 firstindex=1 (tells xmlsitemapgen to save first xml file as sitemap1.xml)
//	 firstindex=0 (tells xmlsitemapgen to save first sml file as sitemap.xml)
//
//	- Page Name Parameter (DEFAULT = ignored) -
//	 pagenamevar=string (tells xmlsitemapgen where to look for the page title in the PHP pages
//						 eg. $pagename = 'Real Page Name';)
//
//	- Database Parser (DEFAULT = ignored) -
//	 dbfile=1 (forces xmlsitemapgen to process the database as specified in xmlsitemapgen-dbfile.php.  a separate inc file for the output is created)
//	 dbfile=0 (xmlsitemapgen will NOT process database records)
//
// *** Including it from another PHP file:
//
//	$_GET['chgfreq'] = "monthly"; $_GET['priority'] = 1; $_GET['getsitepage'] = 0; $_GET['dbfile'] = 1; $_GET['verbose'] = 0;
//	require(SITE_PATH.PLUGINS_FOLDER."sitemapgen/xmlsitemapgen.php");

function sm_header(){
	global $_events;

    $_events->setTriggerFunction('addpanels', 'sm_pagepanel');
}

function sm_pagepanel($event){
    global $_page;

    $sitemap_state = isset($_page->attributes['sitemap_state']) ? $_page->attributes['sitemap_state'] : null;

    $sitemapoptions = array(
        "0:0" => "- Excluded -",
        "1:1.0" => "1.0 (Top)",
        "1:0.9" => "0.9",
        "1:0.8" => "0.8",
        "1:0.7" => "0.7",
        "1:0.6" => "0.6",
        "1:0.5" => "0.5",
        "1:0.4" => "0.4",
        "1:0.3" => "0.3",
        "1:0.2" => "0.2",
        "1:0.1" => "0.1",
        "1:0.0" => "0.0 (Bottom)",
    );

    return array(
        "parent_section" => "right",
        "panels" => array(
            "sitemap" => array(
                "type" => "accordion",
                "label" => "XML Site Map",
                "expandable" => true,
                "expander" => "normal",
                "collapsed" => false,
                "objects" => array(
                    "sitemap_state" => array("type" => "menu", "label" => "Sitemap Priority", "selectedvalue" => $sitemap_state, "valuearray" => $sitemapoptions),
                )
            )
        ),
    );
}

function sm_pageRLO($event, $layoutarry){
    print "<!-- RLO: ";
	var_dump($layoutarry);
    print " -->";
	return $layoutarry;
}
?>
