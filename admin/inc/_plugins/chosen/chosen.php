<?php
/*
CHOSEN PLUG-IN
Web Template 3.0
========================================
Select Box Enhancer for jQuery and Protoype
PHP Integration to Foundry: Chris Donalds
Chosen JS/CSS: Patrick Filler (for Harvest)
*/

/**
 * Initialize the plugin scripts and styles
 */
function chosen_headerprep($attr = null){
	global $_plugins;

	$_plugins->addScript("script", "$(\".chzn-select\").chosen();", true);

	$_plugins->addScriptSourceLine(
		'style',
		WEB_URL.ADMIN_FOLDER.PLUGINS_FOLDER."chosen/",
		"chosen.css",
		"screen"
	);
	$_plugins->addScriptSourceLine(
		'script',
		WEB_URL.ADMIN_FOLDER.PLUGINS_FOLDER."chosen/",
		"chosen.jquery.js",
		""
	);

	$path = $_plugins->getPluginInfo('', 'path');
}

function chosen_trigger($configs){
	return "test";
}

function chosen_title($content){
	return $content.' | test title';
}

function chosen_footer(){
	echo 'test footer';
}

class chosen_class {
	static function chosen_class_trigger(){
		return "class test";
	}
}

function chosen_target(){
	global $_events;

	list($data) = $_events->processTriggerEvent(__FUNCTION__, 'initial');
	echo $data;
}

function chosen_trigger2($data){
	return $data.' test';
}

$_events->setTriggerFunction('showadmintitle', 'chosen_title');
$_events->setTriggerFunction('showfooter', 'chosen_footer');
$_events->setTriggerFunction('chosen_target', 'chosen_trigger2');
chosen_target();
?>