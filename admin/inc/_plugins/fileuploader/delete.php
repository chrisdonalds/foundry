<?php
/*
FILEUPLOADER PLUG-IN
Web Template 3.0
====================
*/

define("BASIC_GETINC", true);
include("../../_core/loader.php");

$file = getRequestVar("file");
$delrec = intval(getRequestVar("delrec"));
$row_id = intval(getRequestVar("row_id"));
if($file != ''){
	if($delrec == 1 && $row_id > 0){
		// image was not selected during current page session
		// get the table related to the page
		$page_url = getRequestVar("page_url");
		$base_page_url = preg_replace("/((\?|\&).*)/i", "", $page_url);
		$table = $_db_control->setTable(REGISTER_TABLE)->setFields("db_table")->setWhere("alias = '$base_page_url' AND type = 'db'")->getRecItem();
		if($table != ''){
			// clear the image data from the page record
			$_db_control->updateRec($table, "image='', thumb=''", "id='$row_id'");
		}
	}
	$thm_file = SITE_PATH.dirname(str_replace(IMG_UPLOAD_FOLDER, THM_UPLOAD_FOLDER, $file))."/thm_".basename($file);
	$img_file = SITE_PATH.$file;
	if(file_exists($thm_file)) @unlink($thm_file);
	if(file_exists($img_file)) {
		@unlink($img_file);
		echo json_encode(array('success' => true));
	}else{
		echo json_encode(array('success' => true, 'value' => 'The file `'.basename($file).'` was not found', ''));
	}
}else{
	echo json_encode(array('success' => false));
}
?>
