// LightOpenID Javascript file

$jq(document).ready(function($){
    $('#admlogin_alt').click(function(e){
        e.preventDefault();
        $('#admlogininnerbox').hide();
        $('#admlogininnerbox_openid').show();
    });

    $('.admsubmit_openid_button').click(function(e){
        var elem = $(this);
        elem.parent().attr('action', elem.attr('rel')).submit();
    });

    $('#admlogin_normal').click(function(e){
        e.preventDefault();
        $('#admlogininnerbox_openid').hide();
        $('#admlogininnerbox').show();
    });
});
