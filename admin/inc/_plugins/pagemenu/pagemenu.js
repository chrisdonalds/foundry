/**
 * Pagemenu Plugin
 * ---------------
 * v 1.0
 * Javascript file
 */
jQuery(document).ready(function($){
    $('#page_menu_enabled').click(function(){
        if($(this).is(':checked')){
            $('#page_menu_title').removeAttr('disabled');
            $('#page_menu_bar').removeAttr('disabled');
            $('#page_menu_parent_id').removeAttr('disabled');
        }else{
            $('#page_menu_title').attr('disabled', 'disabled');
            $('#page_menu_bar').attr('disabled', 'disabled');
            $('#page_menu_parent_id').attr('disabled', 'disabled');
        }
    });

    $('#page_menu_bar').change(function(){
        var val = $(this).val();
        $.post(
            $.foundry.ajax_url,
            {'op':'pagemenu_ajax', 'action':'menubar_change', 'val':val},
            function(html){
                if(html != null) {
                    $('#page_menu_parent_id').parent().html(html);
                }
            },
            "html"
        )
    });
});