<?php
// PAGE MENU
//
// Author: Chris Donalds <cdonalds01@gmail.com>
// Date: July 26, 2015
// License: GPL

/**
 * Occurs when plugin is first being rendered.
 * @uses Loading script/stylesheets, registering trigger functions, preparing plugin
 */
function pagemenu_header(){
    global $_events, $_plugins;

    $_plugins->addScriptSourceLine("script", WEB_URL.ADMIN_FOLDER.PLUGINS_FOLDER."pagemenu/", "pagemenu.js");
    $_events->setTriggerFunction('addpanels', 'pagemenu_panel');
    $_events->setTriggerFunction('savedata', 'pagemenu_save');
}

/**
 * Occurs when editor page panels are being rendered
 * @uses Adding a new panel to Layout Render Object
 */
function pagemenu_panel($event){
    global $_db_control, $_menus, $_page;

    if(in_array("terms", $_page->urlpath)) return null;

    $rec = $_db_control->setTable($_page->datatype)->setWhere("id = '{$_page->row_id}'")->getData();
    $m = array();
    $menubar_list = $_db_control->flattenDBArray($_menus->getWebsiteMenubars(), "id", "title");
    if(!empty($rec)){
        $m = $_db_control->setTable(MENUS_TABLE)->setWhere("alias = '".$rec[0]['attribute_id']."' AND in_admin = 0")->getRec();
    }

    if(count($m) > 0){
        $page_menu_id = $m[0]['id'];
        $page_menu_enabled = true;
        $page_menu_title = $m[0]['title'];
        $page_menu_bar = $m[0]['menu_bar_id'];
        $page_menu_parent_id = $m[0]['parent_id'];
    }elseif(count($rec) > 0){
        $page_menu_id = 0;
        $page_menu_enabled = false;
        $page_menu_title = $_db_control->getTitleField($rec[0]);
        $page_menu_bar = ((!empty($menubar_list)) ? key($menubar_list) : 0);
        $page_menu_parent_id = 0;
    }else{
        $page_menu_id = 0;
        $page_menu_enabled = false;
        $page_menu_title = "";
        $page_menu_bar = ((!empty($menubar_list)) ? key($menubar_list) : 0);
        $page_menu_parent_id = 0;
    }
    $menu_list = array("" => "- No Parent -") + $_db_control->flattenDBTreeArray($_menus->getWebsiteMenuTree($page_menu_bar, -1, true), "id", "title", "-- ");

    return array(
        "parent_section" => "right",
        "panels" => array(
            "menu" => array(
                "type" => "accordion",
                "label" => "Menu",
                "expandable" => true,
                "expander" => "normal",
                "collapsed" => false,
                "objects" => array(
                    "page_menu_id" => array("type" => "hidden", "value" => $page_menu_id),
                    "page_menu_enabled" => array("type" => "checkbox", "label" => "Menu is Active", "value" => 1, "chkstate" => $page_menu_enabled, "text" => "Page is accessible via menu"),
                    "page_menu_title" => array("type" => "text", "label" => "Menu Title", "value" => $page_menu_title, "fldclass" => "col-lg-11", "disabled" => (!(boolean)$page_menu_enabled)),
                    "page_menu_bar" => array("type" => "menu", "label" => "Menubar", "valuearray" => $menubar_list, "selectedvalue" => $page_menu_bar, "disabled" => (!(boolean)$page_menu_enabled)),
                    "page_menu_parent_id" => array("type" => "menu", "label" => "Parent Menu", "valuearray" => $menu_list, "selectedvalue" => $page_menu_parent_id, "disabled" => (!(boolean)$page_menu_enabled))
                )
            )
        ),
    );
}

/**
 * Occurs when an AJAX call is requested from the plugin Javascript
 * @uses Handling plugin-specific AJAX processes
 */
function pagemenu_ajax(){
    global $_db_control, $_fields, $_menus;

    extractVariables($_REQUEST);
    $html = null;
            echo 'change';
    if(!empty($action)){
        switch($action){
            case 'menubar_change':
                $val = intval($val);
                if($val > 0){
                    $menu_list = array("" => "- No Parent -") + $_db_control->flattenDBTreeArray($_menus->getWebsiteMenuTree($val, -1, true), "id", "title", "-- ");
                    $html = $_fields->showMenu(array("type" => "menu", "label" => "Parent Menu", "valuearray" => $menu_list, "selectedvalue" => 0, "disabled" => false));
                }
                echo $html;
                break;
        }
    }
}

/**
 * Occurs when a plugin delegation task is requested (in this case an activation task)
 * @uses Allows the plugin to respond to non-initialized events
 */
function pagemenu_activate_plugin(){
    // echo 'activating';
}

/**
 * Occurs when an editor data attribute save event is started
 * @uses Allows plugin to hook onto the record saving process
 */
function pagemenu_save($event, $atts){
    global $_db_control, $error;

    // get the attribute_id of this record (if not set then the record did not update correctly)
    if(empty($atts['data_type'])) return false;

    $at_id = $_db_control->setTable(ATTRIBUTES_TABLE)->setFields("attribute_id")->setWhere("data_id = '".$atts['data_id']."'")->getRecItem();
    if($at_id < 1) return false;

    // get all posted data
    $args = array(
        'page_menu_id',
        'page_menu_bar',
        'page_menu_parent_id',
        'page_menu_title',
        'page_menu_enabled',
    );
    $vars = getRequestData($args, 'edit_form');
    $page_menu_id = intval($vars['page_menu_id']);

    // build menu fieldvals
    $datafields = array(
        "in_admin" => 0,
        "menu_bar_id" => $vars['page_menu_bar'],
        "parent_id" => $vars['page_menu_parent_id'],
        "title" => $vars['page_menu_title'],
        "key" => codify($vars['page_menu_title']),
        "table" => $atts['data_type'],
        "alias" => $at_id,
        "targettype" => MENU_TARGETTYPE_DATA,
        "active" => $vars['page_menu_enabled']
    );

    // save the menu
    $ok = false;
    if($page_menu_id == 0){
        // no pre-existing menu
        $page_menu_id = $_db_control->setTable(MENUS_TABLE)->setFieldvals($datafields)->insertRec();
        if($page_menu_id > 0) $ok = true;
    }else{
        // update current menu
        $ok = $_db_control->setTable(MENUS_TABLE)->setFieldvals($datafields)->setWhere("id = '".$page_menu_id."'")->updateRec();
    }
    return true;
}
?>
