<?php
/*
WEATHER PLUG-IN
Web Template 3.0
Chris Donalds <cdonalds01@gmail.com>
========================================
*/

include_once(SITE_PATH.PLUGINS_FOLDER."weather/weather.class.php");
$weather = new getWeather();
$weather->dbUser = DBUSER;
$weather->dbPass = DBPASS;
$weather->dbName = DBNAME;

$tempkey = array_search("Temperature", $weather_data[0]);
$condkey = array_search("Condition", $weather_data[0]);
$weather_icon_file = $_db_control->setTable("weather")->setFields("img_num")->setWhere("ec_name = '".strtolower($weather_data[1][$condkey])."'")->getRecItem();
//if($weather_icon_file == "") $weather_icon_file = 37;

function getCondIcon($weather_data, $condkey){
    global $_db_control;
    $file = $_db_control->setTable("weather")->setFields("img_num")->setWhere("ec_name = '".strtolower($weather_data[1][$condkey])."'")->getRecItem();
	//if($file == "") $file = 37;
	return $file;
}
?>
