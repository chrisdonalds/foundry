<?php
/*
MY PLUG-IN
Web Template 3.0
Chris Donalds <cdonalds01@gmail.com>
========================================
*/

function myplugin_headerprep(){
    die("HELLO 1");
}

function myplugin_settings($action = null){
    global $_plugins;

    if($action == null){
        return $_plugins->pluginSettingsDialogContents("My Plugin", "Settings details go here.", __FUNCTION__);
    }elseif($action == PLUGIN_SETTINGS_SAVE){
        return $_plugins->pluginSettingsDialogButtonPressed("Saved!", true);
    }elseif($action == PLUGIN_SETTINGS_CLOSE){
        return $_plugins->pluginSettingsDialogButtonPressed("Closed!", true);
    }
}

?>