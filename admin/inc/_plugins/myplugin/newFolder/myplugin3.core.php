<?php
/*
MY PLUG-IN3
Web Template 3.0
Chris Donalds <cdonalds01@gmail.com>
========================================
*/

function myplugin3_headerprep(){
    die("HELLO 1");
}

function myplugin3_settings($action = null){
    global $_plugins;

    if($action == null){
        return $_plugins->pluginSettingsDialogContents("My Plugin", "Settings details go here.", __FUNCTION__);
    }elseif($action == PLUGIN_SETTINGS_SAVE){
        return $_plugins->pluginSettingsDialogButtonPressed("Saved!", true);
    }elseif($action == PLUGIN_SETTINGS_CLOSE){
        return $_plugins->pluginSettingsDialogButtonPressed("Closed!", true);
    }
}

?>