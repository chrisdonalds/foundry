<?php

//  ------------------------------------------------------------------------------------
//  LIST CATEGORY TERMS
//  ------------------------------------------------------------------------------------

$_render->showHeader();
$_render->startContentArea();
$_users->haltIfUserCannot(UA_VIEW_LIST);

// build search query
$_page->prepSearch(array(
		"sort_by" => "name",
		"sort_dir" => "ASC",
		"search_list" => array("name")
));

// build query
$taxonomy_id = 0;
$alias = $_db_control->setTable(ATTRIBUTES_TABLE)->setWhere("alias = '".$_page->name."'")->getRec(true);
if(!empty($alias)){
	$menu = $_db_control->setTable(MENUS_TABLE)->setWhere("alias = '".$alias['attribute_id']."'")->getRec(true);
	if(!empty($menu))
		$taxonomy_id = $menu['taxonomy_id'];
}
$recset = $_db_control->saveQuery()->setTable(TERMS_TABLE)->setWhere("taxonomy_id = '".$taxonomy_id."'")->getTree();
$rowcount = count($recset);

$_page->ingroup = $_page->datatype."/".TERMS_TABLE;
$_page->subject = "term";
$_page->titlefld = "name";
$_page->taxonomy_id = $taxonomy_id;
$_render->showPageTitle(array("title" => ucwords($_inflect->singularize($_page->datatype))." Terms"));

// col names
$cols 		= array (	"_chk" => "",
						"name" => "Term Name",
                        "code" => "Code",
						"description" => "Description",
						"active"	=> "Active"
					);
// column attributes
$colattr	= array (	"name" => "attr:indent; padstr:'----&nbsp\;'; countfield:__depth",
						"active" => "attr:boolean; trueval:Yes; falseval:No"
					);
// adding a pipe (|) and number after size will limit cell contents to that many characters
$colsize 	= array (	"name" => "20%",
                        "code" => "15%",
						"description" => "35%",
						"active" => "20%",
					);
$sortcols 	= array (	"name" => "Name",
                        "active" => "Active State",
					);
$searchcols = array (	"name" => "Name",
					);
// buttons may be further limited by "ALLOW_" constants in config.php
// single-dimensional button array: $buttons = array ( but1, but2, ... )
// multiple-dimensional button array: $buttons = array ( index1 => array(but1, but2...), index2 => array(but1, but2...) ... )
// aliased button labels in the form DEF_ACTION_LABEL."::alias"
list($buttons, $addbut)	= $_users->getUserAllowedListActions('term', array(DEF_ACTION_EDIT, DEF_ACTION_DELETE, DEF_ACTION_CLONE));

$_render->startPageForm();
$_render->showSearch();
$_render->showPagination($rowcount, $addbut);
$_render->showList(TERMS_TABLE, array(), $recset, null, "name", ALLOW_SORT);
$_render->showPagination($rowcount);
$_render->endPageForm('list_form');
$_render->endContentArea();
$_render->showFooter();
?>