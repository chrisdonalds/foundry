<?php

//  ------------------------------------------------------------------------------------
//  LIST PAGES
//  ------------------------------------------------------------------------------------

$_render->setPageHelp("");

$_render->showHeader();
$val = $_render->startContentArea();
$_users->haltIfUserCannot(UA_VIEW_PAGES_LIST);

// build search query
$_page->prepSearch(array(
		"sort_by" => "lineage",
		"sort_dir" => "ASC",
		"search_list" => array("p.pagename", "p.pagetitle")
));

// build query
$_page->where_clause .= $_page->concat." p.pagetypeid > 0";
$recset = $_db_control->saveQuery()->setTable(PAGES_TABLE)->setWhere()->getDataTree();
$rowcount = count($recset);

$_page->ingroup = PAGES_TABLE;
$_page->subject = "page";
$_page->titlefld = "pagetitle";
$_page->addqueries = array(DEF_ACTION_PUBLISH => "pub_id=1", DEF_ACTION_UNPUBLISH => "unpub_id=1");
$_page->altgroups = array(DEF_ACTION_DELETE => "test");
$_page->altparams = array(DEF_ACTION_EDIT => "test_id");
$_render->showPageTitle(array("title" => "Pages"));

// col names
$cols 		= array (	"_chk" => "",
						"_icon" => "file-text-o",
						"pagetitle" => "Page",
                        "date_updated" => "Updated",
						"metatitle" => "Meta Title",
						"metakeywords" => "Keywords",
						"metadescr" => "Meta Description",
						"homepage" => "Homepage?",
						"published"	=> "Status"
					);
// column attributes
$colattr	= array (	"pagetitle" => "attr:indent; padstr:'----&nbsp\;'; countfield:__depth",
						"date_updated" => "attr:date; format: M j, Y g:i a",
                        "metatitle" => "attr:boolean; trueval:Completed; falseval:Incomplete",
						"metakeywords" => "attr:boolean; trueval:Completed; falseval:Incomplete",
						"metadescr" => "attr:boolean; trueval:Completed; falseval:Incomplete",
						"homepage" => "attr:advexpr; compareusing:=; compareval:1; trueval:Yes; falseval:No",
						"published" => "attr:boolean; trueval:Published; falseval:Draft"
					);
// adding a pipe (|) and number after size will limit cell contents to that many characters
$colsize 	= array (	"pagetitle" => "25%",
                        "date_updated" => "15%",
						"metatitle" => "10%",
						"metakeywords" => "10%",
						"metadescr" => "10%",
						"homepage" => "10%",
						"published" => "8%"
					);
$sortcols 	= array (	"pagetitle" => "Page",
                        "date_updated" => "Date Updated",
						"published" => "Status"
					);
$searchcols = array (	"pagetitle" => "Page",
					);
// buttons may be further limited by "ALLOW_" constants in config.php
// single-dimensional button array: $buttons = array ( but1, but2, ... )
// multiple-dimensional button array: $buttons = array ( index1 => array(but1, but2...), index2 => array(but1, but2...) ... )
// aliased button labels in the form DEF_ACTION_LABEL."::alias"
list($buttons, $addbut)	= $_users->getUserAllowedListActions('page');

$_render->startPageForm();
$_render->showSearch();
$_render->showPagination($rowcount, $addbut);
$_render->showList(PAGES_TABLE, array("ppage_id" => ""), $recset, "pagetypeid", "pagename", ALLOW_SORT);
$_render->showPagination($rowcount);
$_render->endPageForm('list_form');
$_render->endContentArea();
$_render->showFooter();
?>