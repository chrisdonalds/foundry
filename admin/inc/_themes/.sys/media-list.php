<?php

//  ------------------------------------------------------------------------------------
//  LIST PAGES
//  ------------------------------------------------------------------------------------

$_render->setPageHelp("");

$_render->showHeader();
$val = $_render->startContentArea();
$_users->haltIfUserCannot(UA_VIEW_LIST);

// build search query
$_page->prepSearch(array(
		"sort_by" => "name",
		"sort_dir" => "ASC",
		"search_list" => array("p.name")
));

// build query
$recset = $_db_control->saveQuery()->setTable(MEDIA_TABLE)->setWhere()->getData();
$rowcount = count($recset);

$_page->ingroup = MEDIA_TABLE;
$_page->subject = "media";
$_page->titlefld = "name";
$_page->addqueries = array(DEF_ACTION_PUBLISH => "pub_id=1", DEF_ACTION_UNPUBLISH => "unpub_id=1");
$_render->showPageTitle(array("title" => "Media"));

// col names
$cols 		= array (	"_chk" => "",
						"_icon" => "file-text-o",
						"file_path" => "Media",
						"file_type" => "Type",
						"file_size" => "Size",
						"width" => "Width",
						"height" => "Height",
						"data_id" => "Associated?",
						"date_created" => "Created",
                        "date_updated" => "Updated",
					);
// column attributes
$colattr	= array (	"file_path" => "attr:image; thumbfield:file_path; titlefield:name",
						"file_size" => "attr:format; format:%d Kb",
						"width" => "attr:format; format:%d px",
						"height" => "attr:format; format:%d px",
						"data_id" => "attr:advexpr; compareusing:>; compareval:0; trueval:Yes; falseval:No",
						"date_created" => "attr:date; format: M j, Y g:i a",
						"date_updated" => "attr:date; format: M j, Y g:i a"
					);
// adding a pipe (|) and number after size will limit cell contents to that many characters
$colsize 	= array (	"file_path" => "10%",
						"file_type" => "10%",
						"file_size" => "10%",
						"width" => "10%",
						"height" => "10%",
						"data_id" => "15%",
                        "date_created" => "12%",
                        "date_updated" => "12%",
					);
$sortcols 	= array (	"name" => "Name",
						"file_type" => "Type",
						"file_size" => "Size",
						"width" => "Width",
						"height" => "Height",
                        "date_updated" => "Date Updated",
					);
$searchcols = array (	"name" => "Name",
						"file_path" => "Path",
						"file_type" => "Type",
						"data_type" => "Associated Data Type"
					);
// buttons may be further limited by "ALLOW_" constants in config.php
// single-dimensional button array: $buttons = array ( but1, but2, ... )
// multiple-dimensional button array: $buttons = array ( index1 => array(but1, but2...), index2 => array(but1, but2...) ... )
// aliased button labels in the form DEF_ACTION_LABEL."::alias"
list($buttons, $addbut)	= $_users->getUserAllowedListActions('page');

$_render->startPageForm();
$_render->showSearch();
$_render->showPagination($rowcount, $addbut);
$_render->showList(MEDIA_TABLE, array(), $recset);
$_render->showPagination($rowcount);
$_render->endPageForm('list_form');
$_render->endContentArea();
$_render->showFooter();
?>