<?php

//  ------------------------------------------------------------------------------------
//  CREATE PAGE
//  ------------------------------------------------------------------------------------

$_render->showHeader("datepicker");
$_render->startContentArea();
$_users->haltIfUserCannot(UA_ADD_PAGE);

// process POST DATA
if($_page->formDataIsReadyForSaving()){
    if($_sec->validateNonce()){
        // collect posted data
        $args = array(
            'pagetitle',
            'pagename',
            'alias',
            'content',
            'parent_id',
            'locked',
            'homepage',
            'metatitle',
            'metadescr',
            'metakeywords',
            'password',
            'plugins_incl',
            'sitemap_state',
            'layout'
        );
        $vars = getRequestData($args, 'edit_form');

        $vars['pagename'] = codify($vars['pagetitle']);
        $vars['locked'] = intval($vars['locked']);
        $vars['homepage'] = intval($vars['homepage']);
        $vars['parent_id'] = intval($vars['parent_id']);
        $vars['plugins_incl'] = json_encode($vars['plugins_incl']);

        if($_data->checkDataAliasExists(PAGES_TABLE, $vars['pagename'])){
            // save data
            $data = array("pagename" => $vars['pagename'], "pagetitle" => $vars['pagetitle'], "content" => $vars['content'], "layout" => $vars['layout']);
            $atts = array("parent_id" => $vars['parent_id'], "metatitle" => $vars['metatitle'], "metakeywords" => $vars['metakeywords'], "metadescr" => $vars['metadescr'], "language" => DEF_LANG_ISO, "alias" => $vars['alias'], "aliasdefault" => $vars['pagename'], "homepage" => $vars['homepage'], "locked" => $vars['locked'], "plugins_incl" => $vars['plugins_incl'], "sitemap_state" => $vars['sitemap_state']);

            // $_page->savebuttonpressed holds type of save action that was requested
            if($_page->savebuttonpressed == DEF_POST_ACTION_SAVE || $_page->savebuttonpressed == DEF_POST_ACTION_SAVEPUB)
                $atts["published"] = true;

            $_page->row_id = $_db_control->setTable(PAGES_TABLE)->setFieldvals($data)->setAttributes($_POST, $atts)->insertData();
			if($_page->row_id > 0){
				$_error->addErrorStatMsg(SUCCESS_CREATE);
			}else{
				$_error->addErrorStatMsg(FAILURE_CREATE);
			}

            // if this is the home page update the home page alias token
            if((boolean) $vars['homepage']) $_data->switchHomePageTo(PAGES_TABLE, $vars['pagename']);
		}

		if($_error->getErrorStatMsg(SUCCESS_CREATE)) $_filesys->gotoEditPage(PAGES_TABLE);
    }
}

// build data
$pagetitle = getRequestVar('pagetitle');
$pagename = getRequestVar('pagename');
$alias = getRequestVar('alias');
$alias_after = getRequestVar('alias_after');
$content = getRequestVar('content');
$parent_id = getRequestVar('parent_id');
$searchable = intval(getRequestVar('searchable'));
$locked = intval(getRequestVar('locked'));
$homepage = intval(getRequestVar('homepage'));
$metatitle = getRequestVar('metatitle');
$metadescr = getRequestVar('metadescr');
$metakeywords = getRequestVar('metakeywords');
$password = getRequestVar('password');
$plugins_incl = getRequestVar('plugins_incl');
$sitemap_state = getRequestVar('sitemap_state');
$layout = getRequestVar('layout');

// get other info such as title, and list of other terms for parent term menu
$parents_arry = $_db_control->setTable(PAGES_TABLE)->setDataId($_page->row_id)->getDataTree(array("skipself" => false));
$parents_arry = array("" => "- No Parent -") + $_db_control->flattenDBTreeArray($parents_arry, "data_id", "pagetitle", "-- ");
$_page->ingroup = PAGES_TABLE;

// prepare layout object
$themelayouts = json_decode($_themes->getAdminThemeInfo(null, "layouts"), true);
$themelayouts = array('' => '- Select a layout -') + $themelayouts;

$layout = array(
    "buttonblock" => array(
        "prevpagebuttons" => array("Pages List" => $_data->getAdminDataAlias(PAGES_TABLE, "list")),
        "editorbuttons" => DEF_EDITBUT_SAVE
    ),
    "editorblock" => array(
        "id" => "edit_content",
        "pagetitle" => "Create New Page",
        "showreqdtextline" => true,
        "form" => array(
            "id" => "edit_form", "method" => "POST", "action" => "", "addenctype" => true, "additionalfields" => null,
            "sections" => array(
                "left" => array(
                    "class" => "col-lg-8 col-offset-right-1 col-offset-left-1",
                    "panels" => array(
                        "basic" => array("type" => "accordion",
                            "objects" => array(
                                "pagetitle" => (
                                    ($_users->userIsAllowedTo(UA_RENAME_PAGE))
                                    ? array("type" => "text", "label" => "Title*", "value" => $pagetitle, "fldclass" => "bigfldtext col-lg-8", "validate" => true, "validatemessage" => "Page title")
                                    : array("type" => "hiddenfield", "label" => "Title", "value" => $pagetitle, "help" => null)
                                ),
                                "alias" => (
                                    ($_users->userIsAllowedTo(UA_MANAGE_DATA_ALIASES))
                                    ? array("type" => "text", "label" => "Page Alias", "value" => (($homepage == 1) ? '' : $alias), "fldclass" => "bigfldsize", "wrappertext" => array("after" => $alias_after, "before" => WEB_URL, "help" =>"Spoiler: The page alias can be anything."))
                                    : array("type" => "hidden", "value" => $alias)
                                ),
                            )
                        ),
                        "features" => array("type" => "accordion", "label" => "Features", "expandable" => true, "expander" => "normal", "collapsed" => false,
                            "objects" => array(
                                "parent_id" => array("type" => "list", "label" => "Parent Page", "valuearray" => $parents_arry, "selectedvalue" => $parent_id, "wrappertext" => array("help" => "Tip: You can make any top level page the home page."), "disablevalue" => $_page->row_id),
                                "content" => array("type" => "editor", "label" => "Content*", "value" => $content, "dim" => array("95%", "600"), "validate" => true, "validatemessage" => "Page content"),
                            )
                        ),
                        "seo" => array("type" => "accordion", "label" => "Basic SEO", "expandable" => true, "expander" => "normal", "collapsed" => false,
                            "objects" => array(
                                "metainstructions" => array("type" => "information", "label" => "Meta-data (Meta Title, Meta Keywords, and Meta Description) are used by search engines such as Google and Yahoo to list the page and help aid with search-engine optimization (SEO).  If this data is not provided, the page will be supplied with the site's default data."),
                                "metatitle" => array("type" => "text", "label" => "Meta Title", "value" => $metatitle, "fldclass" => "col-lg-8", "wrappertext" => array("help" => "This is the value used in the &lt;title>&lt;/title> tag"), "displaytype" => FLD_OPENROW),
                                "metatitle_copy" => array("type" => "button", "value" => "Same as Page title", "fldclass" => "editpage_copydata", "js" => "rel=\"pagetitle,metatitle\"", "wrappertext" => array("help" => "The site name is already provided"), "displaytype" => FLD_CLOSEROW),
                                "metadescr" => array("type" => "textarea", "label" => "Meta Description", "value" => $metadescr, "fldclass" => "col-lg-12", "wrappertext" => array("after" => "<span class='edithelp'>Leave blank to use a shortened version of the page contents.</span>")),
                                "metakeywords" => array("type" => "textarea", "label" => "Meta Keywords", "value" => $metakeywords, "fldclass" => "col-lg-12", "wrappertext" => array("after" => "<span class='edithelp'>Comma-separated. Up to 512 characters.</span>")),
                            )
                        ),
                    ),
                ),
                "right" => array(
                    "class" => "col-lg-3",
                    "panels" => array(
                        "attributes" => array("type" => "accordion", "label" => "Attributes", "expandable" => true, "expander" => "normal", "collapsed" => false,
                            "objects" => array(
                                "advinstructions" => array("type" => "information", "label" => "The following options offer robust enhancements to the page."),
                                "layout" => array("type" => "selectmenu", "label" => "Layout", "valuearray" => $themelayouts, "selectedvalue" => $layout),
                                "locked" => (
                                    ($_users->userIsAllowedTo(UA_VIEW_LOCKED_PAGES))
                                    ? array("type" => "checkbox", "label" => "Page is Locked?", "value" => 1, "chkstate" => $locked, "text" => "If checked, this page will be locked, and only users with the ability to view locked pages will be able to access it.")
                                    : array("type" => "hidden", "value" => $locked)
                                ),
                                "searchable" => array("type" => "checkbox", "label" => "Page is Searchable?", "value" => 1, "chkstate" => $searchable, "text" => "If checked, this page and its contents and field values may be searched using search plugins."),
                                "password" => array("type" => "password", "label" => "Password Protection?", "value" => $password, "wrappertext" => array("help" => "If provided visitors will be asked to enter the correct password to view the page.", "after" => "<span class='edithelp'>If provided visitors will be asked to enter the correct password to view the page.</span>")),
                                "homepage" => array("type" => "checkbox", "label" => "Page is the Homepage?", "value" => 1, "chkstate" => (($homepage == 1 && $_page->row_id != ERROR_404_PAGE) ? '1' : '0'), "text" => "Yes, make this page the homepage.", "wrappertext" => array("after" => "<span class='edithelp'>Setting this attribute will automatically unset any page currently designated as the homepage.</span>")),
                                "404page" => (
                                    ($homepage == 1)
                                    ? array("type" => "label", "label" => "Use as the 404 Page?", "text" => "This page is currently the homepage.  To use it as the 404 page, uncheck the 'Set as Homepage' setting.", "wrappertext" => array("after" => "<span class='edithelp'>When the page is updated a blank alias will be automatically generated.</span>"))
                                    : array("type" => "checkbox", "label" => "Use as the 404 Page?", "value" => 1, "chkstate" => (($_page->row_id == ERROR_404_PAGE && ERROR_404_PAGE > 0) ? '1' : '0'), "text" => "Yes, use this page as the 404 page")
                                ),
                            )
                        ),
                        "advanced" => array(
                            "type" => "accordion", "label" => "Advanced", "expandable" => true, "expander" => "normal", "collapsed" => false,
                            "objects" => array(
                                "plugins_incl" => array("type" => "pluginsincl", "label" => "Plugins Included on Page", "value" => $plugins_incl, "zone" => "both,front", "data_type" => "pages", "fldclass" => "multiselect", "wrappertext" => array("after" => "<span class='edithelp'>You can select one or more front-zone plugins that will be initiated specifically for this page.</span>")),
                            )
                        )
                    ),
                ),
            ),
        )
    ),
);

// show page
$_render->renderAdminPage($layout);
$_render->endContentArea();
$_render->showFooter();
?>
