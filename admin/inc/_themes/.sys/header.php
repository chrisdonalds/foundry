<?php
// ---------------------------
//
// HEADER
//
// ---------------------------
// The preferred method is:
//     include('../../_core/loader.php');
//     $_render->showHeader();
//
// However, if header is included directly, it will automatically include loader.php thus
// kickstarting the system.
global $_users, $_filesys, $_render, $_themes, $_fields, $_page, $_events;

if(!defined('LOADER_DOCUMENT_ROOT')) include("loader.php");

// handle admin system re-login and redirection
if ((!$_users->isloggedin) && !isset($rurl)) {
	$_filesys->gotoPage (WEB_URL.ADMIN_FOLDER."admlogin.php?rurl=".urlencode($_SERVER['REQUEST_URI']));
}

header('Content-Type: text/html; charset='.CHARSET);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php $_render->showAdminTitle(); ?></title>
<meta name="copyright" content="Copyright <?php echo date("Y")." ".COPYRIGHT_NAME?>"/>
<meta name="author" content="<?php echo COPYRIGHT_NAME?>" />
<meta name="distribution" content="Global" />
<meta name="content-language" content="<?php echo LANGUAGE?>" />
<meta name="generator" content="<?php echo SYS_NAME?> <?php echo CODE_VER?> (<?php echo CODE_VER_NAME?>)" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET?>" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="-1"/>
<?php
$_themes->prepHeadSection();
?>
</head>

<body data-alias="<?php echo codify(trim($_page->name, "/")); ?>">
    <div id="wrapper">
        <?php
        include (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."menu.php");
        $_fields->showHiddenField(array("id" => "base_url", "value" => WEB_URL.ADMIN_FOLDER));
        $_fields->showHiddenField(array("id" => "admin_folder", "value" => WEB_URL.ADMIN_FOLDER.CORE_FOLDER));
        ?>
