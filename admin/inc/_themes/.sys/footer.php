        <?php
        $footer_id = rand(1000, 9999);
        ?>
        <style type="text/css">
            #footer<?php echo $footer_id ?>{border-top:1px solid gray!important;bottom:0!important;clear:both!important;color:#000!important;font-size:13px!important;font-weight:700!important;height:25px!important;margin-left:-10px !important;padding:5px 0 10px!important;position:fixed!important;width:100%!important;z-index:999!important}
        </style>

        <div id="footer<?php echo $footer_id ?>" class="adm_footer">
            <span class="alignleft">
                Copyright &copy; <?php echo date("Y")?> <a href="<?php echo COPYRIGHT_WEB?>" target="_blank"><?php echo COPYRIGHT_NAME?></a>. The Web at Your Best.&nbsp;&nbsp;&nbsp;
            </span>

            <span class="alignright">
                <span class="blue"><?php echo SYS_NAME?>: <?php echo CODE_VER; ?></span>&nbsp;|&nbsp;PHP: <?php echo phpversion()?><?php if(defined('DBHOST')) echo '&nbsp;|&nbsp;MySQL: '.mysqli_get_client_version().PHP_EOL; ?>
            </span>
        </div>
    </div>
