<?php

//  ------------------------------------------------------------------------------------
//  ADD TERM
//  ------------------------------------------------------------------------------------

$_render->showHeader("datepicker");
$_render->startContentArea();
$_users->haltIfUserCannot(UA_ADD_RECORD);

// process POST DATA
if($_page->formDataIsReadyForSaving()){
    if($_sec->validateNonce()){
        // collect posted data
        $args = array(
            'name',
            'code',
            'description',
            'parent_id',
            'taxonomy_id',
            'published'
        );
        $vars = getRequestData($args, 'edit_form');

        if(empty($vars['code'])) $vars['code'] = codify($vars['name']);
        $vars['parent_id'] = intval($vars['parent_id']);
        $vars['taxonomy_id'] = intval($vars['taxonomy_id']);
        $vars['published'] = (bool)(getRequestVar('published'));    // published is one of the form control values

        if($_data->checkAliasExists(TERMS_TABLE, $vars['code'], $_page->row_id) && !empty($vars['taxonomy_id']) && !empty($vars['name'])){
            // save data
            $data = array("code" => $vars['code'], "name" => $vars['name'], "description" => $vars['description'], "parent_id" => $vars['parent_id'], "taxonomy_id" => $vars['taxonomy_id'], "active" => $vars['published']);

            // $_page->savebuttonpressed holds type of save action that was requested
            if($_page->savebuttonpressed == DEF_POST_ACTION_SAVE || $_page->savebuttonpressed == DEF_POST_ACTION_SAVEPUB)
                $atts["published"] = true;

            $_page->row_id = $_db_control->setTable(TERMS_TABLE)->setFieldvals($data)->insertRec();
            if($_page->row_id > 0){
                $_error->addErrorStatMsg(SUCCESS_CREATE);
            }else{
                $_error->addErrorStatMsg(FAILURE_CREATE);
            }
        }

        if($_error->getErrorStatMsg(SUCCESS_CREATE)) $_filesys->gotoEditPage($_page->datatype."/".TERMS_TABLE, 'add');
    }
}

// build data
$name = getRequestVar('name');
$code = getRequestVar('code');
$description = getRequestVar('description');
$parent_id = intval(getRequestVar('parent_id'));
$active = getBooleanIfSet($_REQUEST['active'], true);

// get the taxonomy info
$taxonomy = $_tax->getTaxonomy($_page->datatype);
if(!empty($taxonomy)){
    $taxonomy_id = $taxonomy['id'];
    $taxonomy_name = $taxonomy['name'];
}else{
    $taxonomy_id = 0;
    $taxonomy_name = null;
}

// get other info such as title, and list of other terms for parent term menu
$parent = $_db_control->setTable(TERMS_TABLE)->setFields("name")->setWhere("id = {$parent_id}")->getRecItem();
$parents_array = $_db_control->setTable(TERMS_TABLE)->setDataId($_page->row_id)->getTree(array("skipself" => false));
$parents_array = array("" => "- No Parent -") + $_db_control->flattenDBTreeArray($parents_array, "id", "name", "-- ");

$tax_array = $_db_control->setTable(TAXONOMIES_TABLE)->setWhere("active = 1")->setOrder("name ASC")->getRec();
$tax_array = array("" => "- Select Taxonomy -") + $_db_control->flattenDBArray($tax_array, "id", "name");
$term_data_type = ucwords($_inflect->singularize($_page->datatype));

// prepare layout object
$layout = array(
    "buttonblock" => array(
        "prevpagebuttons" => array($term_data_type." Terms List" => $_data->getAdminDataAlias($_page->datatype."/".TERMS_TABLE, "list")),
        "editorbuttons" => DEF_EDITBUT_SAVEADD
    ),
    "editorblock" => array(
        "id" => "edit_description",
        "pagetitle" => "Create a ".$term_data_type." Term",
        "showreqdtextline" => true,
        "form" => array(
            "id" => "edit_form", "method" => "POST", "action" => "", "addenctype" => true, "additionalfields" => null,
            "sections" => array(
                "left" => array(
                    "class" => "col-lg-8 col-offset-right-1 col-offset-left-1",
                    "panels" => array(
                        "basic" => array("type" => "accordion",
                            "objects" => array(
                                "name" => array("type" => "text", "label" => "Term Name*", "value" => $name, "fldclass" => "bigfldtext col-lg-8", "validate" => true, "validatemessage" => "Term name"),
                                "code" => (
                                    ($_users->userIsAllowedTo(UA_MANAGE_DATA_ALIASES))
                                    ? array("type" => "text", "label" => "Term Code*", "value" => $code, "fldclass" => "bigfldsize", "wrappertext" => array("after" => "<span class='edithelp'>The term's code, comprising of lowercase letters, numbers, the underscore, and hyphen, is used as part of the URL alias for the term.<br/>If left blank the code will be automatically generated.</span>"))
                                    : array("type" => "hidden", "value" => $code)
                                ),
                            )
                        ),
                        "features" => array("type" => "accordion", "label" => "Details", "expandable" => true, "expander" => "normal", "collapsed" => false,
                            "objects" => array(
                                "taxonomy_id" => (
                                    (!empty($taxonomy))
                                    ? array("type" => "hiddenlabel", "label" => "Taxonomy*", "value" => $taxonomy_id, "fldtext" => $taxonomy_name)
                                    : array("type" => "list", "label" => "Taxonomy*", "valuearray" => $tax_array, "selectedvalue" => $taxonomy_id, "validate" => true, "validatemessage" => "Taxonomy")
                                ),
                                "parent_id" => array("type" => "list", "label" => "Parent Term", "valuearray" => $parents_array, "selectedvalue" => $parent_id, "disablevalue" => $_page->row_id, "wrappertext" => array("after" => "<span class='edithelp'>Terms can have other terms as parents, in a hierarchical relationship.</span>")),
                                "description" => array("type" => "editor", "label" => "Description", "value" => $description, "dim" => array("95%", "600")),
                            )
                        ),
                    ),
                ),
                "right" => array(
                    "class" => "col-lg-3",
                    "panels" => array(
                    ),
                ),
            ),
        )
    ),
);

// show page
$_render->renderAdminPage($layout);
$_render->enddescriptionArea();
$_render->showFooter();
?>
