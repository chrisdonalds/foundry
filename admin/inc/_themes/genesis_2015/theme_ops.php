<?php
// ---------------------------
//
// ADMIN AREA COMMON FUNCTIONS
//  - CUSTOM FUNCTIONS FOR THEME -
//
// ---------------------------

define ("ADMINCUSTOM", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

// ----------- CUSTOM FUNCTIONS ---------------
global $_events, $_settings, $_cron, $_db_control, $_menus;

// extra head items
function genesis_header(){
    global $_plugins, $_themes;

    $path = WEB_URL.$_themes->getThemePath("admin");
    $_plugins->loadPluginScripts("wookmark-min");
    $_plugins->loadPluginScripts("jquery-chosen");
    $_plugins->addScriptSourceLine("script", $path."/js/", "script.js");
    $_plugins->addScriptSourceLine("meta_equiv", "X-UA-Compatible", "IE=edge,chrome=1");
}

function genesis_settings($action = null, $data = ''){
    global $_users, $_themes;

    if($action == null){
        // return dialog HTML
        $data = $_themes->getThemeCustomSetting('admin');
    	$html = '
    	<h3 class="header">Genesis 4 Theme Settings</h3>
    	<form action="" method="POST" id="themesettingsform">
    	<p class="clearfix" style="float: right"><input type="button" id="theme_settings_dialog_save" rel="'.__FUNCTION__.'" value="Save"/></p>
    	</form>
    	';
        // $_users->setUserInfo("id", 1, array("username" => "admin", "password" => "password", "level" => ADMLEVEL_DEVELOPER, "firstname" => "My", "lastname" => "Name", "facebooklink" => "", "google" => "", "twitter_link" => "", "activated" => "yes", "blocked" => false));
        return $_themes->themeSettingsDialogContents('Genesis 4 Theme Settings', $html, __FUNCTION__);
    }elseif($action == THEME_SETTINGS_SAVE){
        // save data to database
        $result = $_themes->saveThemeCustomSettings('admin', $data);
        return $_themes->themeSettingsDialogButtonPressed((($result) ? 'Settings saved successfully' : 'Settings were not saved'), true);
    }elseif($action == THEME_SETTINGS_CLOSE){
        // closed
        return $_themes->themeSettingsDialogButtonPressed("Closed!", true);
    }
}

// settings tab and contents
// $key = $_menus->setAdminMenu("top", "chimera2", "", 0, "projects", "Chimera 2", "data_chosen", "admin/chimera2", "list");

function genesis_add_tabs(){
    return array(
        array("key" => "custom1", "title" => "Custom1", "afterkey" => SETTINGS_TAB_MEDIA, "content_callback" => "genesis_tab_contents", "validator_callback" => "genesis_tab_validator", "url" => WEB_URL.ADMIN_FOLDER."settings?tab=chimera"),
        array("key" => "custom2", "title" => "Custom2", "afterkey" => SETTINGS_TAB_MEDIA, "content_callback" => "genesis_tab_contents", "validator_callback" => "genesis_tab_validator", "url" => WEB_URL.ADMIN_FOLDER."settings?tab=chimera2"),
        array("key" => "custom3", "title" => "Custom3", "afterkey" => SETTINGS_TAB_MEDIA, "shared_content_callback" => "genesis_tab_shared_content", "validator_callback" => "genesis_tab_validator"),
    );
}
$_events->setTriggerFunction("setCustomSettingsTab", "genesis_add_tabs");

function genesis_tab_contents(){
    return "This is the tab's content.";
}

function genesis_tab_shared_content(){
    return "<p class='clearfix'>This is shared content!</p>";
}

function genesis_tab_validator($info){
    // echo 'caught it!';
    // var_dump($info);
    // var_dump($_POST);
    // die();
}

function genesis_login_intercept($arg1){
    echo $arg1;
    return true;
}
// $_events->setTriggerFunction('getaccountbylogin', 'genesis_login_intercept');

function genesis_alt_toolbar($array1){
    // unset($array1['about']);
    unset($array1['settings']);
    $array1['usertool'] = false;
    return $array1;
}
// $_events->setTriggerFunction('adminToolbar', 'genesis_alt_toolbar');
// $_events->unsetTriggerFunction('adminToolbar', 'genesis_alt_toolbar');

function genesis_tabs($event, $attr){
    $retn = null;
    switch($event){
        case 'settings_general_menu':
            $retn = '<i class="fa fa-asterisk"></i>';
            break;
        case 'settings_media_menu':
            $retn = '<i class="fa fa-picture-o"></i>';
            break;
        case 'settings_editors_menu':
            $retn = '<i class="fa fa-edit"></i>';
            break;
        case 'settings_menus_menu':
            $retn = '<i class="fa fa-th-list"></i>';
            break;
        case 'settings_themes_menu':
            $retn = '<i class="fa fa-paint-brush"></i>';
            break;
        case 'settings_plugins_menu':
            $retn = '<i class="fa fa-plug"></i>';
            break;
        case 'settings_taxonomies_menu':
            $retn = '<i class="fa fa-cube"></i>';
            break;
        case 'settings_users_menu':
            $retn = '<i class="fa fa-users"></i>';
            break;
        case 'settings_advanced_menu':
            $retn = '<i class="fa fa-sliders"></i>';
            break;
    }
    return $retn;
}
$_events->setTriggerFunction('settings_general_menu', 'genesis_tabs');
$_events->setTriggerFunction('settings_media_menu', 'genesis_tabs');
$_events->setTriggerFunction('settings_editors_menu', 'genesis_tabs');
$_events->setTriggerFunction('settings_themes_menu', 'genesis_tabs');
$_events->setTriggerFunction('settings_menus_menu', 'genesis_tabs');
$_events->setTriggerFunction('settings_plugins_menu', 'genesis_tabs');
$_events->setTriggerFunction('settings_taxonomies_menu', 'genesis_tabs');
$_events->setTriggerFunction('settings_users_menu', 'genesis_tabs');
$_events->setTriggerFunction('settings_advanced_menu', 'genesis_tabs');

function genesis_cron($cron, $params = null){
    // echo 'task #'.$cron['id'].' started at '.date("h:i:s", time())." and will recur next at ".date("Y-m-d H:i:s", $cron['scheduled'])."<br>";
}
// $_cron->delete_cron("genesis_cron");
$args = array(
    array(
        "function" => "genesis_cron",
        "parameters" => array(),
        "interval" => "30,0",
        "priority" => 25,
        "final_datetime" => "Dec 31, 2015",
    ),
    array(
        "function" => "genesis_cron",
        "parameters" => array(),
        "interval" => "0,5",
        "recurrence" => "3",
        "priority" => 5,
        "final_datetime" => "Aug 30, 2015",
    )
);
$crons = $_cron->get_crons(array("function" => "genesis_cron"));
if(count($crons) == 0) {
    $_cron->create_crons($args);
}

function genesis_addons(){
    return '<div class="admLaunchpadItem"><strong>More</strong><br/>Items</div>';
}
// $_events->setTriggerFunction('launchpadToolbarAddons', 'genesis_addons');
/**
 * This function displays the tab button
 * @return string $html
 */
function sample_tab_menu(){
    $html = <<<EOT
    <li><a href="#tabs-plugins" class="settingstabs_link">Sample Tab</a></li>
EOT;
    return $html;
}

/**
 * The tab content shell goes in here
 * @return string $html
 */
function sample_tab_content_wrapper(){
    $html  = '<div id="tabs-plugins">';
    $html .= '</div>'.PHP_EOL;
    return $html;
}

/**
 * The contents function...
 * @return string $html
 */
function sample_tab_content(){
    global $_settings;

    $html  = '<div id="tabs-plugins">';
    $html .= '<h3 class="header">Sample Tab</h3>';
    $html .= '<div class="setlabel">Label: <a href="#" class="hovertip" alt="This is a sample hovertip)">[?]</a>'.REQD_ENTRY.'</div>';
    $html .= '<div class="setdata">Sample Information</div>';
    $html .= '</div>'.PHP_EOL;

    $html .= $_settings->showCustomSettingsTabContentAppended(SETTINGS_TAB_PLUGINS, false);        // The parameter is one of the SETTINGS_TAB_??? constants
    return $html;
}

/**
 * And finally this function handles the data validation
 * @return array $errstr                Return an empty array to indicate that there were no validation errors
 */
function sample_tab_form_validate(){
    global $_db_control;

    $errstr = array();

    return $errstr;
}
// $_events->setTriggerFunction('settings_override_plugins_tab_menu', 'sample_tab_menu');
// $_events->setTriggerFunction('settings_override_plugins_tab_shell', 'sample_tab_content_wrapper');
// $_events->setTriggerFunction('settings_override_plugins_tab_contents', 'sample_tab_content');
// $_events->setTriggerFunction('settings_override_plugins_tab_validator', 'sample_tab_form_validate');
?>