jQuery(document).ready(function($){
    // jQuery theme script code . . .
    $('.hovertip').append('<i class="left-margin fa fa-info-circle"></i>').each(function(){
        $(this).html($(this).html().replace('[?]', ''));
    });

    if(undefined == $.foundry.extenders.grid){
        // ensures that this theme does not everride an already established grid extender
        $.foundry.extenders.grid = function(options){
            $(options.selector).wookmark();
        }
    }
    // $.foundry.extenders.hovertip = false;

    // $('.multiselect').multiselect({
    //     checkAllText: 'Select All',
    //     unCheckAllText: 'Deselect All',
    //     selectedList: 3,
    //     showHeader: true
    // });
    // $('.multiselect').chosen({
    //     inherit_select_classes: true,
    //     search_contains: true,
    //     width: "60%"
    // });
    // $('.advselect').chosen({
    //     inherit_select_classes: true,
    //     search_contains: false,
    //     width: "60%"
    // });
});