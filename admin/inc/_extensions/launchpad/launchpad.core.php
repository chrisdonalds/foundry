<?php
// LAUNCHPAD
//
// Author: Chris Donalds <cdonalds01@gmail.com>
// Date: April 2015
// Version: 1.0
// License: GPL
// ----------------------------------------------
// Foundry admin access bar

function launchpad_headerprep(){
    global $_events, $_plugins;

    $path = WEB_URL.$_plugins->getPluginFolder();
    $_plugins->addScriptSourceLine("script", $path, "launchpad.js");
    $_plugins->addScriptSourceLine("style", $path, "launchpad.css");

}

function launchpad_settings(){
    return "hello!";
}

function launchpad_toolbar(){
    $html = <<<EOT
                <div id="admlaunchpad" class="admtoolbaricon">
                    <a href="#" title="Launchpad Toolbar" class="fa fa-rocket"></a>
                </div>
EOT;
    return $html;
}
$_events->setTriggerFunction("adminToolbarAddons", "launchpad_toolbar");

function launchpad_toolbar_popup(){
    global $_menus, $_inflect, $_themes, $_plugins, $_events;

    // get items to add by menu entry
    $menu_add_items = $_menus->getAdminMenuAliases("add");
    $menu = '';
    foreach($menu_add_items as $item){
        $menu .= '<li><a href="'.WEB_URL.$item['alias_path'].'">'.$_inflect->singularize($item['title']).'</a></li>';
    }

    // get the current theme configuration
    $atheme = $_themes->getAdminThemeInfo();
    $wtheme = $_themes->getWebsiteThemeInfo();

    // get the number of plugins
    $plugins_installed = count($_plugins->getPluginsInstalled());
    $plugins_with_problems = count($_plugins->getPluginsWithProblems());
    $extensions_installed = count($_plugins->getExtensionsInstalled());
    $frameworks_installed = count($_plugins->getFrameworksInstalled());

    // 3rd-party notices
    $other_items = $_events->processQuickTriggerEvent("launchpadToolbarAddons");

    $adminpath = WEB_URL.ADMIN_FOLDER;

    $html = <<<EOT
    <h2>Foundry LaunchPad <i class="fa fa-rocket"></i></h2>
    <div id="admLaunchpadContainer">
        <div class="admLaunchpadItem">
            <strong><i class="fa fa-check-circle"></i> Create New</strong>
            <ul>
            {$menu}
            </ul>
        </div>
        <div class="admLaunchpadItem">
            <strong><i class="fa fa-paint-brush"></i> Themes</strong>
            <ul>
                <li><em>Admin Theme:</em> {$atheme} <a href="{$adminpath}settings?tab=themes&sub=theme_admin">Change</a><li>
                <li><em>Website Theme:</em> {$wtheme} <a href="{$adminpath}settings?tab=themes&sub=theme_website">Change</a><li>
            </ul>
        </div>
        <div class="admLaunchpadItem">
            <strong><i class="fa fa-plug"></i> Plugins</strong>
            <ul>
                <li><em>Installed:</em> <a href="{$adminpath}settings?tab=plugins&sub=plugins_installed">{$plugins_installed}</a></li>
                <li><em>With Problems:</em> <a href="{$adminpath}settings?tab=plugins&sub=plugin_problem">{$plugins_with_problems}</a></li>
                <li><em>Extensions:</em> <a href="{$adminpath}settings?tab=plugins&sub=plugins_extensions">{$extensions_installed}</a></li>
                <li><em>Frameworks:</em> <a href="{$adminpath}settings?tab=plugins&sub=plugin_frameworks">{$frameworks_installed}</a></li>
            </ul>
        </div>
        {$other_items}
    </div>
EOT;
    return $html;
}
?>
