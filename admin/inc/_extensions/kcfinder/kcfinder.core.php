<?php
/*
MY PLUG-IN
Web Template 3.0
Chris Donalds <cdonalds01@gmail.com>
========================================
*/

function kcfinder_headerprep(){
}

function kcfinder_settings($action = null, $data = ''){
    global $_plugins;

    if($action == null){
        return $_plugins->pluginSettingsDialogContents("My Plugin", "Settings details go here.", __FUNCTION__);
    }elseif($action == PLUGIN_SETTINGS_SAVE){
        return $_plugins->pluginSettingsDialogButtonPressed("Saved!", true);
    }elseif($action == PLUGIN_SETTINGS_CLOSE){
        return $_plugins->pluginSettingsDialogButtonPressed("Closed!", true);
    }
}

?>