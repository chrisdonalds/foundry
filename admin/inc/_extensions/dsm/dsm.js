/**
 * @author Chris Donalds (cdonalds01@gmail.com)
 */
/* Foundry Dataset Manager Extension script
--------------------------------------------------------------------------------------- */
jQuery(document).ready(function($){
    $(document).delegate('#dsm_create_table', 'click', function(){
        var db_prefix = $(this).attr('rel');
        var name = prompt("Enter a unique name for the new table.\n\nPrefix the table name with '"+db_prefix+"' to associate it with attributes.\nUnprefixed tables are not accessible by the Dataset Manager.");
        if(name != null) name = name.trim();
        if(name != null && name != ''){
            $.ajax({
                type: "POST",
                url: $.foundry.ajax_url,
                data: "op=dsm_ajax_handler&task=check-table-name&new_name="+name,
                dataType: "json",
                async: false,
                success: function(jsondata){
                    if(jsondata.success){
                        alert('The new table was created with a default ID field.');
                        $.post(
                            $.foundry.ajax_url,
                            {op: "dsm_ajax_handler", task: "reload"},
                            function(jsondata){
                                if(jsondata.success){
                                    $('#dsm_settings').html(jsondata.rtndata);
                                }
                            },
                            'json'
                        );
                    }else{
                        alert('The new table name was not accepted.');
                        $('#dsm_create_table').trigger('click');
                    }
                }
            });
        }
    });

    $(document).delegate('.dsm_table_rename', 'click', function(e){
        e.preventDefault();
        var elem = $(this).closest('div');
        elem.find('.dsm_table_name').addClass('hidden');
        elem.find('.dsm_table_name_field').removeClass('hidden');
    });

    $(document).delegate('.dsm_table_delete', 'click', function(e){
        e.preventDefault();
        var table = $(this).attr('rel');
        if(confirm("Delete table '"+table+"'?")){
            $.post(
                $.foundry.ajax_url,
                {op: "dsm_ajax_handler", task: "delete table", table:table},
                function(jsondata){
                    if(jsondata.success){
                        alert('The table was deleted.');
                        $('#dsm_settings').html(jsondata.rtndata);
                    }else{
                        alert('The table could not be deleted.  '+jsondata.rtndata);
                    }
                },
                'json'
            );
        }
    });

    $(document).delegate('.dsm_table_name_field', 'blur', function(){
        var div = $(this).closest('div');
        var new_name = $(this).val().trim();
        var old_name = div.find('.dsm_table_name').text();

        if(new_name != ''){
            $.post(
                $.foundry.ajax_url,
                {op: "dsm_ajax_handler", task: "rename table", old_name: old_name, new_name: new_name},
                function(jsondata){
                    if(jsondata.success){
                        alert('The table was renamed.');
                    }else{
                        alert('The table could not be renamed.  '+jsondata.rtndata);
                    }
                    div.find('.dsm_table_name').text(new_name).removeClass('hidden');
                    div.find('.dsm_table_name_field').addClass('hidden');
                },
                'json'
            );
        }
    });

    $(document).delegate('.dsm_edit_table_cols', 'click', function(e){
        e.preventDefault();
        $(this).parent().find('.dsm_table_cols').slideDown(200);
    });

    $(document).delegate('.dsm_add_col', 'click', function(e){
        e.preventDefault();
        var btn = $(this);
        var add_row = btn.closest('.dsm_table_add_col_row');
        var div = btn.closest('.dsm_table_col_row').find('.dsm_table_new_col_row').clone();
        var table = btn.closest('.dsm_table_col');
        var new_count = btn.attr('rel');
        if(isNaN(new_count)) new_count = 1;
        btn.attr('rel', new_count + 1);
        div.removeClass('hidden');
        div.find('.dsm_new_col').each(function(){
            var item_name = $(this).attr('name');
            $(this).attr('name', item_name.replace('_new_', 'new_'+new_count));
        });
        add_row.before(div);
        div.find('.dsm_table_col_name').focus();
    });

    $('.dsm_table_col_row.row').hover(function(){
        $(this).addClass('dsm-highlight-row');
    }, function(){
        $(this).removeClass('dsm-highlight-row');
    });

    $('.dsm_table_col_pri').click(function(){
        var elem = $(this);
        if(elem.is(':checked')){
            elem.closest('.dsm_table_cols').find('.dsm_table_col_pri').removeAttr('checked');
            elem.attr('checked', 'checked');
        }
    });

    $(document).delegate('.dsm_table_col_type', 'change', function(){
        var autofield = $(this).closest('.dsm_table_col_row').find('.dsm_table_col_auto');
        switch($(this).val()){
            case 'CHAR':
            case 'VARCHAR':
            case 'FLOAT':
            case 'DOUBLE':
            case 'DECIMAL':
            case 'DATE':
            case 'DATETIME':
            case 'TIMESTAMP':
            case 'TIME':
            case 'YEAR':
            case 'CHAR':
            case 'VARCHAR':
            case 'BLOB':
            case 'TEXT':
            case 'TINYTEXT':
            case 'MEDIUMTEXT':
            case 'LONGTEXT':
            case 'ENUM':
                autofield.removeAttr('checked').attr('disabled', 'disabled');
                break;
            default:
                autofield.removeAttr('disabled');
                break;
        }
    });

    $(document).delegate('.dsm_table_col_delete', 'click', function(e){
        e.preventDefault();
        if(confirm('Delete this column?  Doing so will remove all data stored in this column.\n\nIf this is not intended, reload the page before saving.')){
            var row = $(this).closest('.dsm_table_col_row');
            row.addClass('hidden');
            row.find('.dsm_table_col_state').val('deleted');
        }
    });

    $('#cfgform').submit(function(){
        var msg = null;
        var ok = true;
        $('.dsm_table_col_name').each(function(){
            if(!$(this).hasClass('dsm_new_col')){
                $(this).val($(this).val().trim());
                if($(this).val() == '') {
                    msg = 'All fields must have names.';
                    console.log($(this));
                }
            }
        });
        if(msg == null){
            $('.dsm_table_col_type').each(function(){
                var elem = $(this);
                if(!$(this).hasClass('dsm_new_col') && !$(this).closest('.dsm_table_col_row').hasClass('hidden')){
                    var row = elem.closest('.dsm_table_col_row');
                    var field = row.find('.dsm_table_col_name').val();
                    var size = elem.parent().find('.dsm_table_col_typesize').val();
                    switch($(this).val()){
                        case 'INT':
                        case 'TINYINT':
                        case 'SMALLINT':
                        case 'MEDIUMINT':
                        case 'BIGINT':
                        case 'CHAR':
                        case 'VARCHAR':
                            size = parseInt(size);
                            if(isNaN(size) || size < 1) msg = 'Field size for "'+field+'" must be a positive integer (1 ... n).';
                            break;
                        case 'FLOAT':
                        case 'DOUBLE':
                            size = parseFloat(size.replace(/,/, '.'));
                            if(isNaN(size) || size <= 0) msg = 'Field size for "'+field+'" must be one or two positive integers (1 ... n) separated by a comma.';
                            break;
                        case 'TEXT':
                        case 'TINYTEXT':
                        case 'MEDIUMTEXT':
                        case 'LONGTEXT':
                        case 'BLOB':
                            if(size != '') msg = 'Field size for "'+field+'" must empty.';
                            break;
                    }
                }
            });
        }
        if(msg != null){
            alert(msg);
            ok = false;
        }
        return ok;
    });
});