<?php
// DATASET MANAGER
//
// Author: Chris Donalds <cdonalds01@gmail.com>
// Date: October 18, 2015
// Version: 1.0
// License: GPL
// ----------------------------------------------
// Extends Foundry's database controller by allowing users to create and modify data tables.

function dsm_headerprep(){
    global $_events, $_plugins;

    $path = WEB_URL.$_plugins->getPluginFolder();
    $_plugins->addScriptSourceLine("script", $path, "dsm.js");
    $_plugins->addScriptSourceLine("style", $path, "dsm.css");
}

function dsm_settings(){
    return "hello!";
}

/**
 * Trigger callback for setCustomSettingsTab event
 */
function dsm_add_tabs(){
    return array(
        array(
            "key" => "dsm_settings",
            "title" => "Dataset Manager",
            "afterkey" => SETTINGS_TAB_ADVANCED,
            "aftersubkey" => SETTINGS_SUBTAB_ADVANCED_CORE,
            "shared_content_callback" => "dsm_tab_shared_content",
            "validator_callback" => "dsm_tab_validator"
        ),
    );
}
$_events->setTriggerFunction("setCustomSettingsTab", "dsm_add_tabs");

/**
 * Output shared tab content
 * @param boolean $contents_only
 */
function dsm_tab_shared_content($contents_only = false){
    global $_db_control;

    $tables = $_db_control->getDataTables(false);
    $tables_html = '<div id="dsm_top"><div class="setlabel dsm_header">Tables</div><div class="setdata dsm_header">Columns</div></div>'.PHP_EOL;
    $types = array("int", "tinyint", "smallint", "mediumint", "bigint", "float", "double", "decimal", "date", "datetime",
                   "timestamp", "time", "year", "char", "varchar", "blob", "text", "tinytext", "mediumtext", "longtext", "enum");

    foreach($tables as $table){
        $tables_html .= '<div class="dsm_table">'.PHP_EOL;
        $tables_html .= '<div class="setlabel"><span class="dsm_table_name">'.$table.'</span><input type="text" name="dsm_table_name['.$table.']" value="'.$table.'" class="dsm_table_name_field hidden" /><br/><a href="#" class="dsm_table_rename"><i class="fa fa-pencil"></i>&nbsp;Rename</a> | <a href="#" class="dsm_table_delete" rel="'.$table.'"><i class="fa fa-trash"></i>&nbsp;Delete</a></div>'.PHP_EOL;
        $tables_html .= '<div class="setdata"><a href="#" class="dsm_edit_table_cols"><i class="fa fa-pencil"></i> Edit/Add Columns</a>';
        $sql = "SHOW COLUMNS FROM `%s`";
        $args = array($table);
        $tablecols = array();
        $tablecols_rs = $_db_control->dbGetQuery($sql, $args);

        $tables_html .= '<div class="dsm_table_cols">'.PHP_EOL;
        $tables_html .= '<div class="dsm_table_header row top-padding">'.PHP_EOL;
        $tables_html .= '<div class="col-lg-2 dsm_header">Name</div><div class="col-lg-2 dsm_header">Type/Size</div><div class="col-lg-2 dsm_header">Default Value</div><div class="col-lg-4 dsm_header">Attributes</div><div class="col-lg-2 dsm_header">Actions</div>';
        $tables_html .= '</div>'.PHP_EOL;
        if(!empty($tablecols_rs)){
            foreach($tablecols_rs as $col){
                preg_match("/([a-z0-9]+)\(([0-9]+)\)/", $col['Type'], $type);
                if(isset($type[1]))
                    $type[1] = strtoupper($type[1]);
                else
                    $type = array(1 => $col['Type'], 2 => 0);
                $disabled = ((strpos($type[1], 'INT') === false) ? ' disabled="disabled"' : '');

                $typesel = '<select class="dsm_table_col_type" name="dsm_table['.$table.']['.$col['Field'].'][Type]">';
                foreach($types as $t){
                    $t = strtoupper($t);
                    $typesel .= '<option value="'.$t.'"'.(($t == $type[1]) ? ' selected="selected"' : '').'>'.$t.'</option>';
                }
                $typesel .= '</select>';
                $tables_html .= '<div class="dsm_table_col_row row"><input type="hidden" class="dsm_table_col_state" name="dsm_table['.$table.']['.$col['Field'].'][State]" value="active" />'.PHP_EOL;
                $tables_html .= '<div class="col-lg-2"><input type="text" class="dsm_table_col_name" name="dsm_table['.$table.']['.$col['Field'].'][Field]" value="'.$col['Field'].'" /></div>';
                $tables_html .= '<div class="col-lg-2">'.$typesel.'<input type="text" class="dsm_table_col_typesize" name="dsm_table['.$table.']['.$col['Field'].'][Typesize]" value="'.$type[2].'" /></div>';
                $tables_html .= '<div class="col-lg-2"><input type="text" class="dsm_table_col_default" name="dsm_table['.$table.']['.$col['Field'].'][Default]" value="'.$col['Default'].'" /></div>';
                $tables_html .= '<div class="col-lg-4">';
                $tables_html .= ' <input type="checkbox" class="dsm_table_col_null" name="dsm_table['.$table.']['.$col['Field'].'][Null]" value="1"'.(($col['Null'] == 'YES') ? ' checked="checked"' : '').' /> Null';
                $tables_html .= ' <input type="checkbox" class="dsm_table_col_pri" name="dsm_table['.$table.']['.$col['Field'].'][Pri]" value="1"'.(($col['Key'] == 'PRI') ? ' checked="checked"' : '').' /> Primary';
                $tables_html .= ' <input type="checkbox" class="dsm_table_col_auto" name="dsm_table['.$table.']['.$col['Field'].'][Auto]" value="1"'.(($col['Extra'] == 'auto_increment' && $disabled == '') ? ' checked="checked"' : '').$disabled.' /> Auto-increment';
                $tables_html .= '</div>';
                $tables_html .= '<div class="col-lg-2"><a href="#" class="dsm_table_col_delete"><i class="fa fa-trash"></i> Delete</a></div>';
                $tables_html .= '</div>'.PHP_EOL;
            }
        }

        $tables_html .= dsm_new_column_html($table, $types);
        $tables_html .= '</div></div></div></div>'.PHP_EOL;
    }

    $db_prefix = DB_TABLE_PREFIX;
    $html = <<<EOT
        <h3 class="header">Dataset Manager</h3>
        <p>With the Dataset Manager, you can create or modify data tables.  Only tables prefixed with '{$db_prefix}' are editable with this tool.</p>
        <p><input type="button" id="dsm_create_table" value="Create Table" rel="{$db_prefix}" /></p>
        <div id="dsm_tables">
            {$tables_html}
        </div>
EOT;
    if(!$contents_only) $html = '    <div id="dsm_settings">'.$html.'    </div>'.PHP_EOL;

    return $html;
}

/**
 * Output table column fields for a new column
 * @param string $table
 * @param array $types
 */
function dsm_new_column_html($table, $types){
    $tables_html = '';
    $typesel = '<select class="dsm_table_col_type" name="dsm_table['.$table.'][_new_][Type]">';
    foreach($types as $t){
        $t = strtoupper($t);
        $typesel .= '<option value="'.$t.'">'.$t.'</option>';
    }
    $typesel .= '</select>';

    $tables_html .= '<div class="dsm_table_col_row dsm_table_add_col_row top-padding">'.PHP_EOL;
    $tables_html .= '<div class="col-lg-12"><input type="button" class="dsm_add_col" rel="'.$table.'" value="Add Column" />';
    $tables_html .= '</div>'.PHP_EOL;
    $tables_html .= '<div class="dsm_table_col_row row dsm_table_new_col_row hidden '.$table.'"><input type="hidden" class="dsm_table_col_state dsm_new_col" name="dsm_table['.$table.'][_new_][State]" value="new" />'.PHP_EOL;
    $tables_html .= '<div class="col-lg-2"><input type="text" class="dsm_table_col_name dsm_new_col" name="dsm_table['.$table.'][_new_][Field]" value="" /></div>';
    $tables_html .= '<div class="col-lg-2">'.str_replace("dsm_table_col_type", "dsm_table_col_type dsm_new_col", $typesel).'<input type="text" class="dsm_table_col_typesize dsm_new_col" name="dsm_table['.$table.'][_new_][Typesize]" value="" /></div>';
    $tables_html .= '<div class="col-lg-2"><input type="text" class="dsm_table_col_default dsm_new_col" name="dsm_table['.$table.'][_new_][Default]" value="" /></div>';
    $tables_html .= '<div class="col-lg-4">';
    $tables_html .= ' <input type="checkbox" class="dsm_table_col_null dsm_new_col" name="dsm_table['.$table.'][_new_][Null]" value="1" /> Null';
    $tables_html .= ' <input type="checkbox" class="dsm_table_col_pri dsm_new_col" name="dsm_table['.$table.'][_new_][Pri]" value="1" /> Primary';
    $tables_html .= ' <input type="checkbox" class="dsm_table_col_auto dsm_new_col" name="dsm_table['.$table.'][_new_][Auto]" value="1" /> Auto-increment';
    $tables_html .= '</div>';
    $tables_html .= '<div class="col-lg-2"></div>';
    $tables_html .= '</div>'.PHP_EOL;
    return $tables_html;
}

/**
 * Validate data posted from a settings save routine
 * @param string $info
 */
function dsm_tab_validator($info){
    global $_db_control;

    $data = $_POST['dsm_table'];
    $errors = array();
    if(is_array($data) && !empty($data)){
        $tablecols = $_db_control->getDataTableFields(false);

        foreach($data as $table => $cols){
            foreach($cols as $curfield => $atts){
                $sql = null;
                $args = array();
                if($curfield != '_new_'){
                    $field = trim($atts['Field']);
                    $type = strtoupper(getIfSet($atts['Type']));
                    $size = intval(getIfSet($atts['Typesize']));
                    $default = trim(addslashes(getIfSet($atts['Default'])));
                    $pri = (boolean)getIfSet($atts['Pri']);
                    $null = (boolean)getIfSet($atts['Null']);
                    $auto = (boolean)getIfSet($atts['Auto']);
                    switch($atts['State']){
                        case 'active':
                            if(!empty($field) && !empty($type)){
                                $update = true;
                                if(isset($tablecols[$table][$curfield])){
                                    // check if this column has a pending change
                                    preg_match("/([a-zA-Z]+)\(([0-9]+)\)/", $tablecols[$table][$curfield]['Type'], $curtypesize);
                                    $curtype = strtoupper($curtypesize[1]);
                                    $cursize = ((isset($curtypesize[2])) ? $curtypesize[2] : '');
                                    $curnull = (strtoupper($tablecols[$table][$curfield]['Null']) == 'YES');
                                    $curpri = (strtoupper($tablecols[$table][$curfield]['Key']) == 'PRI');
                                    $curdefault = $tablecols[$table][$curfield]['Default'];
                                    $curauto = (strtoupper($tablecols[$table][$curfield]['Extra']) == 'AUTO_INCREMENT');
                                    if(strtoupper($curfield) == strtoupper($field) && $curtype == $type && $cursize == $size && $curdefault == $default && $curpri == $pri && $curnull == $null && $curauto == $auto) $update = false;
                                }
                                if($update){
                                    $sql = "ALTER TABLE `$table` CHANGE COLUMN `%s` `%s` $type";
                                    $sql = dsm_col_sql($sql, $type, $size, $default, $null, $pri, $auto);
                                    $args = array($curfield, $field);
                                }
                            }
                            break;
                        case 'deleted':
                            $sql = "ALTER TABLE `$table` DROP COLUMN `%s`";
                            $args = array($curfield);
                            break;
                        case 'new':
                            if(!empty($field) && !empty($type)){
                                $sql = "ALTER TABLE `$table` ADD COLUMN `%s` $type";
                                $sql = dsm_col_sql($sql, $type, $size, $default, $null, $pri, $auto);
                                $args = array($field);
                            }
                            break;
                    }
                        // printr($atts);
                    if(!empty($sql)) {
                        $result = $_db_control->dbUpdateQuery($sql, $args);
                        if(!$result){
                            switch($atts['State']){
                                case 'active':
                                    $error[] = "change '$curfield'";
                                    break;
                                case 'deleted':
                                    $error[] = "delete '$curfield'";
                                    break;
                                case 'new':
                                    $error[] = "add '$field'";
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
    $errors = join(", ", $error);
    return $errors;
}

/**
 * SQL query preparation
 * @param string $sql
 * @param string $type
 * @param string $size
 * @param string $default
 * @param boolean $null
 * @param boolean $pri
 * @param boolean $auto
 */
function dsm_col_sql($sql, $type, $size, $default, $null, $pri, $auto){
    if($size > 0){
        $sql .= "($size)";
    }
    if($default != '' && !is_null($default)) {
        if(!is_numeric($default)) $default = "'$default'";
        $sql .= " DEFAULT $default";
    }
    if($null){
        if($default == '' || is_null($default)) $sql .= " DEFAULT";
        $sql .= " NULL";
    }else{
        $sql .= " NOT NULL";
    }
    if($pri) $sql .= " PRIMARY";
    if($auto) $sql .= " AUTO_INCREMENT";
    $sql .= ";";
    return $sql;
}

/**
 * Process incoming AJAX calls
 */
function dsm_ajax_handler(){
    global $task, $_db_control;

    switch($task){
        case 'rename table':
            $ok = false;
            $result = null;
            $old_name = strtolower(getRequestVar('old_name'));
            $new_name = trim(strtolower(getRequestVar('new_name')));
            $new_name = preg_replace("/([^a-z0-9_]+)/", "", $new_name);
            if($old_name != $new_name){
                $tables = $_db_control->getDataTables(true);
                if(!in_array($new_name, $tables)){
                    $sql = "ALTER TABLE `%s` RENAME `%s`;";
                    $args = array($old_name, $new_name);
                    $ok = $_db_control->dbExecuteQuery($sql, $args);
                }else{
                    $result = 'The new name already exists.';
                }
            }else{
                $result = 'The new name is the same as the old one.';
            }
            exitAjax($ok, $result);
            break;
        case 'check-table-name':
            $ok = false;
            $result = null;
            $name = strtolower(getRequestVar('new_name'));
            $name = preg_replace("/([^a-z0-9_]+)/", "", $name);
            if(!empty($name)){
                $tables = $_db_control->getTables();
                if(!in_array($name, $tables)){
                    $sql = "CREATE TABLE `%s` (`id` bigint(11) unsigned NOT NULL auto_increment, PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1";
                    if($_db_control->dbExecuteQuery($sql, array($name))) $ok = true;
                }
            }
            exitAjax($ok, $result);
            break;
        case 'reload':
            exitAjax(true, dsm_tab_shared_content(true));
            break;
        case 'delete table':
            $table = trim(strtolower(getRequestVar('table')));
            $ok = false;
            $rtn = null;
            if(!empty($table)){
                $sql = "DROP TABLE IF EXISTS `%s`";
                $args = array($table);
                $ok = $_db_control->dbDeleteQuery($sql, $args);
                if($ok)
                    $rtn = dsm_tab_shared_content(true);
            }
            exitAjax($ok, $rtn);
            break;
    }
}
?>