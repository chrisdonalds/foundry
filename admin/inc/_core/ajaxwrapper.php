<?php
// ---------------------------
//
// AJAX WRAPPER
//
//  - Handles specific operations
//  - Use Ajaxprocessor for generic SQL requests
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define("IN_AJAX", true);
define("BASIC_GETINC", true);
if(isset($_REQUEST['op']) && $_REQUEST['op'] == 'trytoaccessdb') {
    // This tells the loader and db_config not to try to start the database early
    define("BASICDB", true);
    define("DB_USED", false);
    define("QUICK_LOAD", true);
}
if(!defined("LOADER_DOCUMENT_ROOT")) include("loader.php");

$op = strtolower(getRequestVar("op"));
$val = getRequestVar("val");
extractVariables($_REQUEST);
$_events->processTriggerEvent('ajaxcall', $op);                // alert triggered functions when function executes

// FUNCTION
if(function_exists($op)){
    echo $op();
    exit;
}

switch($op){
    // GENERAL

    case 'pulseaction':
        $val = explode(",", $val);
        $retn = array();
        if(in_array('logincheck', $val)){
            $retn['logincheck'] = $_users->isUserLoggedin();
        }
        exitAjax(true, $retn);
        break;

    // SETTINGS/TOOLBARS

    case 'getphpinfo':
        echo phpinfo();
        break;
    case 'getadmuserpopup':
        echo $_users->showUserMenu();
        break;
    case 'getadmsettingspopup':
        echo $_settings->showSettingsMenu();
        break;
    case 'getadmhelppopup':
        if(isset($data)){
            echo urldecode($data);
        }
        break;
    case 'getadminsettingstabcontents':
        if(!empty($val)){
            if(filter_var($custom, FILTER_VALIDATE_BOOLEAN) == false){
                list($overhtml) = $_events->processTriggerEvent("settings_override_".$val."_tab_contents");
                if(empty($overhtml)){
                    $content_func = "settings_".$val."_content";
                    if(!function_exists("settings_".$val."_menu")) $_filesys->includeFile(SITE_PATH.ADMIN_FOLDER.SETTINGS_FOLDER."settings-".$val.".php");
                    if(function_exists($content_func)) {
                        $content_func($val);
                    }
                }else
                    echo $overhtml;
            }else{
                $tabs = $_settings->getSettingsTabs();
                foreach($tabs as $key => $tabarry){
                    $t = $tabarry;
                    if(isset($t[0])) $t = $t[0];
                    if(isset($t['content_callback']) && isset($t['key'])){
                        if(strtolower($t['key']) == $val){
                            $content_func = $t['content_callback'];
                            if(function_exists($content_func)) {
                                echo $content_func($t);
                            }
                        }
                    }
                }
            }
        }
        break;

    // MEDIA

    case 'addmediaformat':
        if(!function_exists('settings_show_media_format_new_row')) include(SITE_PATH.ADMIN_FOLDER.SETTINGS_FOLDER."settings-media.php");
        ob_start();
        settings_show_media_format_new_row();
        $html = ob_get_contents();
        ob_end_clean();
        exitAjax(true, $html);
        break;
    case 'setmediaformatactivestate':
        $id = intval(getRequestVar('id'));
        $retn = $_media->setMediaFormatActiveState($id);
        if($retn["success"])
            exitAjax(true, $retn["state"]);
        else
            exitAjax(false, null);
        break;
    case 'overridemediaformat':
        $id = intval(getRequestVar('id'));
        if($_media->overrideMediaFormat($id)){
            if(!function_exists('settings_show_media_formats_list')) include(SITE_PATH.ADMIN_FOLDER.SETTINGS_FOLDER."settings-media.php");
            ob_start();
            settings_show_media_formats_list();
            $html = ob_get_contents();
            ob_end_clean();
            exitAjax(true, $html);
        }else{
            exitAjax(false, null);
        }
        break;
    case 'renamemediaformattitle':
        $id = intval(getRequestVar('id'));
        list($result, $error) = $_media->renameMediaFormat($id, $val);
        if($error == null)
            exitAjax(true, $result);
        else
            exitAjax(false, $error);
        break;
    case 'deletemediaformat':
        $id = intval(getRequestVar('id'));
        if($_media->deleteMediaFormat($id))
            exitAjax(true);
        else
            exitAjax(false);
        break;
    case 'addmediaformataction':
        if($val != ''){
            $name = getRequestVar('name');
            $id = getRequestVar('id');
            $media_formats = $_media->getMediaFormatActionAttributes($val);
            if(is_array($media_formats) && !empty($media_formats)){
                $attshtml = '';
                foreach($media_formats as $attr => $format){
                    switch(true){
                        case ($format == 'text'):
                            $attshtml .= ucwords($attr).' <input class="media_format_val" type="text" value="" size="3" maxlength="6" name="media_format['.$id.'][action]['.$val.'|'.time().']['.$attr.']"> ';
                            break;
                        case preg_match("/^menu:(.*)/i", $format):
                            $attshtml .= ucwords($attr).' <select class="media_format_val" name="media_format['.$id.'][action]['.$val.'|'.time().']['.$attr.']">';
                            $menuoptions = explode(",", substr($format, 5));
                            if(!empty($menuoptions)){
                                foreach($menuoptions as $menu) $attshtml .= '<option value="'.$menu.'">'.$menu.'</option>';
                            }
                            $attshtml .= '</select>'.PHP_EOL;
                            break;
                    }
                }
                if($attshtml != ''){
                    $attshtml = '<div class="gap-5 media_formats_action"><strong>'.$name.'</strong><br/>'.$attshtml.' '.$_themes->showIcon('admin', 'delete', array('class' => 'media_formats_actions_delete clickable', 'title' => 'Delete this format action', 'rel' => $id)).'</div>';
                    exitAjax(true, $attshtml);
                }
            }
            exitAjax(false, "Unable to retrieve action attributes for '".$val."'");
        }
        break;
    case 'deletemediaformataction':
        if($_media->deleteMediaFormatAction($val))
            exitAjax(true);
        else
            exitAjax(false);
        break;

	// THEMES

	case 'setthemeactivestate':
		$id = intval(getRequestVar('id'));
        $retn = $_themes->setThemeActiveState($id);
        if($retn["success"])
            exitAjax(true, $retn["state"]);
        else
            exitAjax(false, null);
		break;
    case 'getthemetabcontents':
        $zone = getRequestVar('zone');
        $_themes->showSettingsThemesList($zone);
        break;
	case 'getthemedata':
		$id = intval(getRequestVar('id'));
        $retn = $_themes->getSettingsThemeData($id);
        if($retn !== false)
            exitAjax(true, $retn);
        else
            exitAjax(false, null);
		break;
	case 'runthemesettingsfunc':
        // execute a registered theme trigger function called 'settingsfunc' for the active theme
		if(!empty($val)){
			$func = explode('|', $val);
			if(function_exists($func[0])){
				// theme init file already included
				echo call_user_func($func[0]);
			}
		}
		break;
	case 'runthemesettingsaction':
		// run the active theme's settingsfunc action
		$action = getRequestVar('action');
		$data = getRequestVar('data');
		if(!empty($val)){
			$func = explode('|', $val);
			if(function_exists($func[0])){
				// theme init file already included
				$rtn = call_user_func($func[0], $action, $data);
			}
			echo $rtn;
		}
		break;
    case 'deletetheme':
        $id = intval(getRequestVar('id'));
        $retn = $_themes->deleteTheme($id);
        if($retn !== false)
            exitAjax(true, $retn);
        else
            exitAjax(false, null);
        break;
	case 'getcolorpickercontents':
		$html = getThemePaletteBox();
		echo $html;
		break;

    // MENUS

    case 'updateadminmenulayout':
        if(!empty($val)) {
            $val = explode("|", str_replace("menu_", "", $val));
            $ok = $_menus->updateAdminMenuLayouts($val);
            exitAjax($ok, '');
        }else{
            exitAjax(false, '');
        }
        break;
    case 'getadminmenueditorhtml':
        if(!empty($menubar)){
            $html = $_menus->getAdminMenuEditorHTML($menubar, $val);
            echo $html;
        }
        break;
    case 'getadminmenubarinfo':
        $val = intval($val);
        if($val > 0){
            $data = $_menus->getAdminMenubarInfo($val);
            if(!is_null($data))
                exitAjax(true, $data[0]);
            else
                exitAjax(false, null);
        }else{
            exitAjax(false, null);
        }
        break;
    case 'saveadminmenubarinfo':
        $id = intval($val);
        $parent_id = intval($parent_id);
        $descr = getIfSet($descr);
        $title = getIfSet($title);
        if(!empty($title)){
            list($success, $new_id, $key) = $_menus->saveAdminMenubar(array('id' => $id, 'title' => $title, 'descr' => $descr, 'parent_id' => $parent_id));
            if($success > 0){
                // updated
                exitAjax(true, null);
            }elseif($success < 0){
                // saved
                exitAjax(true, array('id' => $new_id, 'key' => $key));
            }else{
                exitAjax(false, "There was a problem saving the menubar.");
            }
        }else{
            exitAjax(false, "The menubar title was not provided, and thus the changes were not saved.");
        }
        break;
    case 'refreshadminmenulist':
        if(!empty($val)){
            $menu_html = $_menus->getAdminMenuHTML($val, array("mainid" => "adminmenu_mainnav", "subclass" => "adminmenu_subnav", "menuclass" => "adminmenu_menuitem", "itemwrap" => "span", "aftertext" => "&nbsp;({data})", "afterlink" => '<a href="#" class="adminmenu_menuitem_delete fa fa-trash" rel="{id}" title="Delete"></a><a href="#" class="adminmenu_menuitem_clone fa fa-files-o" rel="{key}" title="Clone"></a>'));
            if(empty($menu_html))
                exitAjax(true, "");
            else
                exitAjax(true, $menu_html);
        }
        exitAjax(false, "");
        break;
    case 'deleteadminmenubar':
        $id = intval($val);
        if($_menus->deleteMenubar($id)){
            exitAjax(true, null);
        }else{
            exitAjax(false, "There was a problem deleting the menubar.");
        }
        break;

    case 'saveadminmenu':
        $menu_bar_id = intval(getRequestVar('menu_bar_id'));
        $new_menu_bar_id = intval(getRequestVar('new_menu_bar_id'));
        $title = getRequestVar('title');
        $data_type = getRequestVar('datatype');
        $taxonomy_id = intval(getRequestVar('taxonomy_id'));
        $ok = false;
        if(($menu_bar_id > 0 || $new_menu_bar_id > 0) && !empty($title) && !empty($data_type)){
            if($targettype == 'terms') $data_type = 'terms';
            if($data_type == '- Not Bound -') $data_type = null;
            $args = array(
                "parent_id" => $parent_id,
                "menu_bar_id" => $menu_bar_id,
                "new_menu_bar_id" => $new_menu_bar_id,
                "title" => $title,
                "alias" => $alias,
                "target" => $target,
                "table" => $data_type,
                "taxonomy_id" => $taxonomy_id,
                "targettype" => $targettype,
                "restricted" => $restricted,
                "active" => $active,
                "custom" => $custom
            );
            list($ok, $key) = $_menus->saveAdminMenu($val, $args, false);
        }elseif($menu_bar_id == 0 || $new_menu_bar_id == 0){
            $key = "Menu items cannot be added or moved to the orphaned menubar.";
        }
        exitAjax($ok, $key);
        break;
    case 'getadminmenutarget':
        $html = '';
        if(!empty($table) && $table != '- Unknown -'){
            $html = $_menus->getAdminMenuTarget($table, $alias);
        }
        echo $html;
        break;
    case 'deleteadminmenu':
        $ok = false;
        $val= intval($val);
        if($val > 0){
            $ok = $_menus->deleteAdminMenu($val);
        }
        exitAjax($ok);
        break;

    case 'addwebsitemenucollection':
        $menu_bar_id = intval(getRequestVar('menu_bar_id'));
        $ok = false;
        if(!empty($val)){
            $arry = array();
            foreach($val as $collection){
                $c = explode("=", $collection);
                if(count($c) == 2){
                    $arry[$c[0]] = explode(",", $c[1]);
                }
            }
            $ok = $_menus->addWebsiteMenuCollection($menu_bar_id, $arry);
        }
        exitAjax($ok);
        break;
    case 'updatewebsitemenulayout':
        if(!empty($val)) {
            $val = explode("|", str_replace("menu_", "", $val));
            $ok = $_menus->updateWebsiteMenuLayouts($val);
            exitAjax($ok, '');
        }else{
            exitAjax(false, '');
        }
        break;
    case 'getwebsitemenueditorhtml':
        if(!empty($menubar)){
            $html = $_menus->getWebsiteMenuEditorHTML($menubar, $val);
            echo $html;
        }
        break;
    case 'getwebsitemenubarinfo':
        $val = intval($val);
        if($val > 0){
            $data = $_menus->getWebsiteMenubarInfo($val);
            if(!is_null($data))
                exitAjax(true, $data[0]);
            else
                exitAjax(false, null);
        }else{
            exitAjax(false, null);
        }
        break;
    case 'savewebsitemenubarinfo':
        $id = intval($val);
        $parent_id = intval($parent_id);
        $descr = getIfSet($descr);
        $title = getIfSet($title);
        if(!empty($title)){
            list($success, $new_id, $key) = $_menus->saveWebsiteMenubar(array('id' => $id, 'title' => $title, 'descr' => $descr, 'parent_id' => $parent_id));
            if($success > 0){
                // updated
                exitAjax(true, null);
            }elseif($success < 0){
                // saved
                exitAjax(true, array('id' => $new_id, 'key' => $key));
            }else{
                exitAjax(false, "There was a problem saving the menubar.");
            }
        }else{
            exitAjax(false, "The menubar title was not provided, and thus the changes were not saved.");
        }
        break;
    case 'refreshwebsitemenulist':
        if(!empty($val)){
            $menu_html = $_menus->getWebsiteMenuHTML(null, $val, array("mainid" => "websitemenu_mainnav", "subclass" => "websitemenu_subnav", "menuclass" => "websitemenu_menuitem", "itemwrap" => "span", "aftertext" => "&nbsp;({data})", "afterlink" => '<a href="#" class="websitemenu_menuitem_delete fa fa-trash" rel="{id}" title="Delete"></a><a href="#" class="websitemenu_menuitem_clone fa fa-files-o" rel="{key}" title="Clone"></a>'));
            if(empty($menu_html))
                exitAjax(true, "");
            else
                exitAjax(true, $menu_html);
        }
        exitAjax(false, "");
        break;
    case 'deletewebsitemenubar':
        $id = intval($val);
        if($_menus->deleteMenubar($id)){
            exitAjax(true, null);
        }else{
            exitAjax(false, "There was a problem deleting the menubar.");
        }
        break;

    case 'savewebsitemenu':
        $menu_bar_id = intval(getRequestVar('menu_bar_id'));
        $new_menu_bar_id = intval(getRequestVar('new_menu_bar_id'));
        $title = getRequestVar('title');
        $ok = false;
        if(($menu_bar_id > 0 || $new_menu_bar_id > 0) && !empty($title)){
            $args = array(
                "parent_id" => $parent_id,
                "menu_bar_id" => $menu_bar_id,
                "new_menu_bar_id" => $new_menu_bar_id,
                "title" => $title,
                "alias" => $alias,
                "taxonomy_id" => $taxonomy_id,
                "target" => $target,
                "window" => $wind,
                "active" => $active,
                "custom" => $custom
            );
            list($ok, $key) = $_menus->saveWebsiteMenu($val, $args, false);
        }elseif($menu_bar_id == 0 || $new_menu_bar_id == 0){
            $key = "Menu items cannot be added or moved to the orphaned menubar.";
        }
        exitAjax($ok, $key);
        break;
    case 'getwebsitemenutarget':
        $html = '';
        if(!empty($table) && $table != '- Unknown -'){
            $html = $_menus->getWebsiteMenuTarget($table, $alias);
        }
        echo $html;
        break;
    case 'deletewebsitemenu':
        $ok = false;
        $val= intval($val);
        if($val > 0){
            $ok = $_menus->deleteWebsiteMenu($val);
        }
        exitAjax($ok);
        break;

    // TAXONOMIES

    case 'savetaxonomy':
        $id = intval($val);
        $alias = getRequestVar('alias');
        $data_alias = intval(getRequestVar('data_alias'));
        $create_menu = (boolean)getRequestVar('create_menu');
        if($data_alias == 0 && !empty($alias)) {
            // add a new data alias
            $data_alias = $_data->saveDataAlias(array("data_type" => TAXONOMIES_TABLE, "newalias" => $alias, "attribute_class" => ATTR_CLASS_TAX_ALIAS));
        }
        if($data_alias > 0){
            $code = getRequestVar('code');
            $name = getRequestVar('name');
            $args = array(
                "id" => $id,
                "name" => $name,
                "code" => $code,
                "parent_id" => intval(getRequestVar('parent_id')),
                "type" => getRequestVar('type'),
                "data_alias" => $data_alias,
            );

            // save taxonomy
            $id = $_tax->saveTaxonomy($args);
            $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldVals(array("data_id" => $id))->setWhere("attribute_id='".$data_alias."'")->updateRec();

            // if creating an admin terms editor menu
            if($create_menu){
                // $alias_id = $_data->saveDataAlias(array('data_type' => TERMS_TABLE, 'attribute_class' => ATTR_CLASS_ADMIN_ALIAS, 'newalias' => $code.'/terms/list'));
                $menu_bar = $_menus->getAdminMenubarInSlot();
                $_menus->saveAdminMenu(0, array('menu_bar_id' => $menu_bar['id'], 'title' => $name.' Terms', 'table' => TERMS_TABLE, 'targettype' => 'terms', 'taxonomy_id' => $id, 'target' => $code.'/terms/list', 'active' => true), false);
            }
            $html = $_settings->showSettingsTaxonomyEditor(true);
            exitAjax(true, $html);
        }else{
            exitAjax(false, null);
        }
        break;
    case 'changetaxonomystate':
        $id = intval($val);
        if($id > 0){
            $state = getRequestVar('state');
            $_tax->changeTaxonomyState($id, $state);
            exitAjax(true);
        }else{
            exitAjax(false);
        }
        break;
    case 'deletetaxonomy':
        $id = intval($val);
        if($_tax->deleteTaxonomy($id)){
            $html = $_settings->showSettingsTaxonomyEditor(true);
            exitAjax(true, $html);
        }else{
            exitAjax(false);
        }
        break;

	// PLUGINS

	case 'setpluginactivestate':
		$id = intval(getRequestVar('id'));
		if($id > 0){
            $retn = $_plugins->setPluginActivation($id, $val);
            if($retn !== false){
                exitAjax(true, array('result' => $val, 'file' => $retn));
            }else{
                exitAjax(false);
            }
		}
		break;
    case 'getplugindependants':
        $id = intval($val);
        if($id > 0){
            $deps = $_plugins->getPluginDependants($_plugins->getPluginVerb($id), AS_ARRAY, false, true);
            echo count($deps);
        }else{
            echo false;
        }
        break;
	case 'getplugindata':
		$id = intval(getRequestVar('id'));
		exitAjax(true, $_plugins->getSettingsPluginsMoreInfo($id, $val));
		break;
    case 'updateplugindata':
        $rel = getRequestVar('rel');
        if(!empty($val) && !empty($rel)){
            $rel = explode('|', $rel);
            if($_db_control->setTable(PLUGINS_TABLE)->setFieldvals(array($rel[1] => $val))->setWhere("`id` = '".$rel[0]."'")->updateRec()){
                $_events->processTriggerEvent('updatePluginData', $rel[0]);
                exitAjax(true, '');
            }else{
                exitAjax(false, '');
            }
        }else{
            exitAjax(false, '');
        }
        break;
	case 'deleteplugin':
	case 'scrapplugin':
		$plugin_id = intval(getRequestVar('id'));
		if($plugin_id > 0){
			if(PLUGIN_FULLDELETE || $op == 'scrapplugin'){
                $plugin_data = $_db_control->setTable(PLUGINS_TABLE)->setWhere("id='{$plugin_id}'")->setLimit()->getRec();
				$plugin_folder = $plugin_data[0]['folder'];
                $_events->processTriggerEvent('deletePlugin_'.$plugin_folder, 'full');
				$rtn = $_plugins->deletePluginFolderContents($plugin_folder);
                $_db_control->setTable(PLUGINS_TABLE)->setWhere("id='{$plugin_id}'")->deleteRec();
				exitAjax((empty($rtn)), $rtn);
			}else{
                if($_db_control->setTable(PLUGINS_TABLE)->setFieldvals(array("is_deleted" => 1))->setWhere("`id`='{$plugin_id}'")->updateRec()){
                    $plugin_data = $_db_control->setTable(PLUGINS_TABLE)->setWhere("id='{$plugin_id}'")->getRec();
                    $plugin_folder = $plugin_data[0]['folder'];
                    $_events->processTriggerEvent('deletePlugin_'.$plugin_folder, 'temp');
					$rtn = array();
					$rtn['row'] = $_plugins->getSettingsPluginRow($plugin_data[0], 'prob');
					$rtn['setting'] = ((!isBlank($plugin_data[0]['settingsfunc'])) ? codify($plugin_data[0]['name']) : '');
					exitAjax(true, $rtn);
				}else{
					exitAjax(false, 'Problem marking plugin as deleted in the database.');
				}
			}
		}
		break;
	case 'undeleteplugin':
		$plugin_id = intval(getRequestVar('id'));
		if($plugin_id > 0){
			if(!PLUGIN_FULLDELETE){
                if($_db_control->setTable(PLUGINS_TABLE)->setFieldvals(array("is_deleted" => 0))->setWhere("id='{$plugin_id}'")->updateRec()){
                    $plugin_data = $_db_control->setTable(PLUGINS_TABLE)->setWhere("id='{$plugin_id}'")->getRec();
                    $plugin_folder = $plugin_data[0]['folder'];
                    $_events->processTriggerEvent('undeletePlugin_'.$plugin_folder);
                    $rtn = array();
                    $rtn['row'] = $_plugins->getSettingsPluginRow($plugin_data[0], 'normal');
                    $rtn['setting'] = ((!isBlank($plugin_data[0]['settingsfunc'])) ? $_plugins->getSettingsPluginSettingsRow($plugin_data[0]) : '');
                    exitAjax(true, $rtn);
                }else{
                    exitAjax(false, 'Problem marking plugin as un-deleted in the database.');
                }
			}
		}
		break;
	case 'runpluginsettingsfunc':
        // execute a registered plugin trigger function called 'settingsfunc'
		if(!empty($val)){
			$func = explode('|', $val);
			if(function_exists($func[0])){
				// plugin init file already included
				$rtn = call_user_func($func[0]);
			}else{
				// include plugin and try again
				if(substr($func[1], 0, strlen(SITE_PATH)) != SITE_PATH) $func[1] = SITE_PATH.$func[1];
				if(file_exists($func[1])){
					include($func[1]);
					if(function_exists($func[0])){
						$rtn = call_user_func($func[0]);
					}else{
						// passthru with error
						$rtn = $_plugins->pluginSettingsDialogContents('', '', $func[0]);
					}
				}else{
					$rtn = array('success' => false, 'contents' => '', 'func' => $func[0], 'id' => 0);
				}
			}
            if(is_array($rtn)) if(isset($func[2])) $rtn['id'] = $func[2];
		}
        if(!is_array($rtn)) $rtn = array('success' => true, 'contents' => '', 'func' => $func[0], 'id' => 0);
        echo json_encode($rtn);
		break;
	case 'runpluginsettingsaction':
		$action = getRequestVar('action');
		$data = getRequestVar('data');
        $rtn = null;
		if(!empty($val)){
			$func = explode('|', $val);
			if(function_exists($func[0])){
				// plugin init file already included
				$rtn = call_user_func($func[0], $action, $data);
			}else{
				// include plugin and try again
				if(file_exists($func[1])){
					include($func[1]);
					if(function_exists($func[0])){
						$rtn = call_user_func($func[0], $action, $data);
					}
				}else{
					$rtn = array('success' => false, 'message' => '', 'id' => 0);
				}
			}
            if(is_array($rtn)) if(isset($func[2])) $rtn['id'] = $func[2];
		}
        if(!is_array($rtn)) $rtn = json_encode(array('success' => true, 'message' => '', 'id' => 0));
        echo json_encode($rtn);
		break;
	case 'getpluginrepairform':
		// part 1: prepare and open repair dialog
		$plugin_id = intval(getRequestVar('id'));
		echo $_plugins->getSettingsPluginRepairForm($plugin_id);
		break;
	case 'repairplugincfg':
		// part 2: try to repair plugin.info file
        list($rtnval, $rtnstr) = $_plugins->doSettingsPluginRepair($val);
        exitAjax($rtnval, $rtnstr);
		break;
	case 'getsettingshelpfile':
		if(!empty($val)){
			$val = SITE_PATH.$val;
			if(file_exists($val)){
				$fcontents = file_get_contents($val);
				$fcontents = str_replace(array("\"", "<", ">"), array("&quot;", "&lt;", "&gt;"), $fcontents);
				$fcontents = str_replace(array("\n"), array("<br/>"), $fcontents);
				echo '<p>'.$fcontents.'</p>';
			}
		}
		break;
    case 'getusagezoneslegend':
        echo '
            <span id="plugin_zonesel_front" class="plugin_zonesel" alt="Click to show front-zone plugins"><img src="'.WEB_URL.ADMIN_FOLDER.CORE_FOLDER.'images/general/plugin_front.png" title="Front" alt="Front" width="10" />&nbsp;Front</span>&nbsp;|&nbsp;
            <span id="plugin_zonesel_admin" class="plugin_zonesel" alt="Click to show admin-zone plugins"><img src="'.WEB_URL.ADMIN_FOLDER.CORE_FOLDER.'images/general/plugin_admin.png" title="Admin" alt="Admin" width="10" />&nbsp;Admin</span>&nbsp;|&nbsp;
            <span id="plugin_zonesel_both" class="plugin_zonesel" alt="Click to show dual-zone plugins"><img src="'.WEB_URL.ADMIN_FOLDER.CORE_FOLDER.'images/general/plugin_both.png" title="Both" alt="Both" width="10" />&nbsp;Both</span>
        ';
        break;
    case 'reloadpluginstab':
        switch($val){
            case 'installed':
                $_plugins->showSettingsPluginsInstalledList();
                break;
            case 'find':
                $param = array();
                $param['search'] = getIfSet($search);
                $param['active'] = getIfSet($filterstate);
                $param['zone'] = getIfSet($filterzone);
                $_plugins->showSettingsPluginsInstalledList($param);
                break;
            case 'findext':
                $param = array();
                $param['search'] = getIfSet($search);
                $param['active'] = getIfSet($filterstate);
                $param['zone'] = getIfSet($filterzone);
                $_plugins->showSettingsPluginsExtensions($param);
                break;
            default:
                break;
        }
        break;

	// USERS

	case 'setuseractivestate':
		$id = intval(getRequestVar('id'));
		if($id > 0){
            if($_db_control->setTable(ACCOUNTS_TABLE)->setFieldvals(array("activated" => $val))->setWhere("id='{$id}'")->updateRec()){
                $_events->processTriggerEvent('setUserActiveState', array($id, $val));
				exitAjax(true, $val);
			}else{
				exitAjax(false, null);
			}
		}
		break;
	case 'deleteuserdata':
		$user_id = intval(getRequestVar('id'));
		if($user_id > 0){
            $_events->processTriggerEvent('deleteUser', $id);
            $rtn = $_db_control->setTable(ACCOUNTS_TABLE)->setWhere("id='{$user_id}'")->deleteRec();
			exitAjax($rtn, '');
		}
		break;
	case 'addnewuser':
		$sel = "";
		$val = intval($val) + 1;
		$user_levels = getConsts(USERTYPE_PREFIX);
		foreach($user_levels as $key => $lvl){
			if($lvl >= $_users->getUserLevel()){
				$sel .= '<option value="'.$lvl.'">'.ucwords(strtolower(substr($key, 9))).'</option>';
			}
		}
		$data = <<<EOT
				<div class="user_row user_new">
					<input type="hidden" class="user_id" name="users_id[$val]" value="0" />
					<div class="user_leftside">
						<span class="user_name"><i>- New -</i></span><br/>
						<span class="user_actions"><a href="" class="user_del">Delete</a></span>
						<input type="hidden" name="users_active[$val]" id="users_active{$val}" value="1" />
						<input type="hidden" name="users_cur[$val]" class="user_cur" value="" />
					</div>
					<div class="user_rightside">
						<span>New Name:</span> <input type="text" name="users_name[$val]" id="users_name{$val}" size="20" value=""/><br/>
						<span>New Password:</span> <input type="password" name="users_pass[$val]" id="users_pass{$val}" size="20" value=""/><br/>
						<span>Email Address:</span> <input type="text" name="users_email[$val]" id="users_email{$val}" size="30" value=""/><br/>
						<span>User Level:</span> <select name="users_level[$val]" id="users_level{$val}">
							{$sel}
							</select>
					</div>
				</div>

EOT;
		echo $data;
		break;
    case 'revertuagroup':
        $val = intval($val);
        if($val >= ADMLEVEL_DEVELOPER && $group != ''){
            if($val > 0) $val = pow(2, $val);
            $group = $_users->getAllowanceNameFromKey($group);
            $ua_values = $_users->getAllowancesByUserType($val, $group);
            if($ua_values !== false){
                exitAjax(true, array_values($ua_values));
            }else{
                exitAjax(false, '');
            }
        }else{
            echo "bad";
            exitAjax(false, '');
        }
        break;
    case 'addnewua':
    	if($val != ''){
    		$key = $_users->getAllowanceKeyFromName($val);
    		$r = $_users->addCustomAllowance($key);
    		if(is_array($r)){
    			$item = '<span class="ua_item" id="ua_item_'.$key.'">'.PHP_EOL;
    			$item.= ' ...'.$val.'  <a rel="'.$key.'" href="#" class="ua_delete" title="Delete this custom allowance"></a>'.PHP_EOL;
    			$item.= '</span>'.PHP_EOL;

    			$val_row = '<div rel="'.$key.'" id="ua_value_row_'.$key.'" class="ua_value_row">'.PHP_EOL;
    			for($i = 0; $i < count($r); $i++) $val_row .= '<span><input type="checkbox" value="1" class="custom_allowances_group_item_'.$i.'" name="ca_value['.$key.']['.$i.']"></span>'.PHP_EOL;
    			$val_row.= '</div>'.PHP_EOL;
    			exitAjax(true, array('i' => $item, 'v' => $val_row));
    		}else{
    			exitAjax(false, $r);
    		}
    	}else{
    		exitAjax(false);
    	}
    	break;
    case 'deleteua':
    	if($val != '' && !in_array($val, array_keys($_users->allowances))){
    		exitAjax($_users->deleteAllowance($val, false));
    	}else{
    		exitAjax(false);
    	}
    	break;
    case 'checkpasswordstrength':
    	$arry = $_sec->checkpasswordstrength($val);
    	echo json_encode(array('success' => true, 'label' => $arry[0], 'strength' => $arry[1]));
    	break;
    case 'getpassword':
        $opts = getRequestVar('opts');
        parse_str(urldecode($opts));
        $pwd = $_sec->createPassword(10, $user_pass_generate_opts);
        echo $pwd;
        break;
    case 'takeoveredit':
        $data_type = getRequestVar('data_type');
        $data_id = getRequestVar('data_id');
        if($_users->giveCurrentUserDataControl($data_type, $data_id))
            exitAjax(true);
        else
            exitAjax(false);
        break;

	// ROBOTS

	case 'revertrobotfile':
		$revertfile = SITE_PATH.ADMIN_FOLDER.$val;
		$success = false;
		if(!empty($val) && file_exists($revertfile)){
			// first backup the current file
            chmod(SITE_PATH."robots.txt", 0777);
            if(copy (SITE_PATH."robots.txt", SITE_PATH.ADMIN_FOLDER.REV_FOLDER."robots.".date("YmdHis").".txt")){
                $fcontents = file_get_contents($revertfile);
                file_put_contents(SITE_APTH."robots.txt", $fcontents);
                $success = true;
            }
            chmod(SITE_PATH."robots.txt", 0644);
		}
		exitAjax($success, '');
		break;

    // DATA ALIASES

    case 'validatedataaliasrule':
        if(!empty($val)){
            list(,$error) = $_data->convertDataAliasRuletoPattern($val);
            if($error == ''){
                exitAjax(true, (($_data->dataAliasIsCategory($val)) ? 'Valid category rule' : 'Valid data rule'));
            }else{
                exitAjax(false, $error.'...');
            }
        }else{
            exitAjax(true, '');
        }
        break;
    case 'deletealias':
        $id = intval($val);
        if($id > 0){
            $_data->deleteAlias($id);
            exitAjax(true);
        }
        break;

    // CRONS

    case 'cron-run':
    case 'cron-enable':
    case 'cron-disable':
    case 'cron-delete':
        if(!empty($val)){
            $ok = false;
            ob_start();
            switch($op){
                case 'cron-run':
                    $ok = $_cron->run_crons(array('id' => $val));
                    break;
                case 'cron-enable':
                    $ok = $_cron->resume_crons(null, $val);
                    break;
                case 'cron-disable':
                    $ok = $_cron->suspend_crons(null, $val);
                    break;
                case 'cron-delete':
                    $ok = $_cron->delete_cron(null, $val);
                    break;
            }
            ob_end_clean();
            if($ok)
                exitAjax(true);
            else
                exitAjax(false);
        }else{
            exitAjax(false, 'Missing cron ID');
        }
        break;

	// DB MANAGER
    // -- DB Manager operations require the database controller to be disabled

	case 'trytoaccessdb':
		$domain  = getRequestVar('d');
		$db_host = getRequestVar('h');
		$db_user = getRequestVar('u');
		$db_pass = getRequestVar('p');
		$db_name = getRequestVar('n');
		$db_port = intval(getRequestVar('r'));
		if(!empty($db_host) && !empty($db_user) && !empty($db_name)){
			$db_hostport = (($db_port > 0 && $db_port != 3306) ? $db_host.':'.$db_port : $db_host);
            $_error->setErrorSuppression();                  // @ might not be caught and ignored by PHP
			$link = @mysqli_connect($db_hostport, $db_user, $db_pass, $db_name);
			if($link){
				$retn = prepDBINI($domain, $db_host, $db_name, $db_user, $db_pass, $db_port);
                if($retn == ''){
                    exitAjax(true, "Yureka! Your settings worked.  The site will continue starting.");
                }else{
                    exitAjax(false, $retn);
                }
			}else{
				exitAjax(false, "Houston we have a problem! Connection to the provided database server was not possible.");
			}
		}else{
			exitAjax(false, "Not so fast! Please specify the Host, Username, and Database Name values.");
		}
		break;
	case 'updatedbsettings':
		break;
	case 'getdbdump':
        if(empty($sel)) $sel = '*';
        if(!empty($new_dom) && substr($new_dom, -1, 1) != "/") $new_dom .= "/";
        if(!empty($new_root) && substr($new_root, -1, 1) != "/") $new_root .= "/";
        $tables_dropped = (strtolower($table_treat) == 'dropped_then_created');
        $records_truncated = (strtolower($record_treat) == 'truncated_then_inserted');
        $file = getDBDumpFDBX($sel, $new_dom, $new_root, $tables_dropped, $records_truncated);

		if($file !== false && $file != '')
            exitAjax(true, $file);
        else
            exitAjax(false);
		break;

	// REGISTER/TRIGGERS

	case 'executelistaction':
		$param = getRequestVar('param');		// action, row_id
		$pagedata = getRequestVar('pagedata');
		$page_subject = getIfSet($pagedata['page_subject']);
		$page_ingroup = getIfSet($pagedata['page_ingroup']);
		$page_url = urldecode(getIfSet($pagedata['page_url']));
		$base_page_url = preg_replace("/((\?|\&).*)/i", "", $page_url);
		$page_flds = json_decode(getIfSet($param['page_flds']));
		$x_data = getIfSet($param['x_data']);				// supplementary data for secondary functions

		// get the db tables used on the calling page
        // this function uses the register table to "recall" the data table referenced by the calling page
        list($main_table, $child_table) = getTablesFromPagedata($pagedata['page_ingroup']);

		// get the parameters saved by the showlist operation
        $funcrec = null;
        if(isset($_SESSION[$page_url]))
            if(isset($_SESSION[$page_url]['persist_data']))
                $list_params = $_SESSION[$page_url]['persist_data'];

		// see if there is a registered trigger function to handle the action
		$retn = null;
		$gotopage = null;
        $html = null;
		list($retn, $continue) = $_events->executeTrigger(TF_LISTACTION, $param, $base_page_url);

		if($continue && !empty($main_table)){
			// function call failed or nothing there,
			// so handle the process the traditional way
		    switch($param['action']) {
                case DEF_ACTION_ADD:
                    $gotopage = WEB_URL.ADMIN_FOLDER.((!empty($page_ingroup)) ? $page_ingroup."/" : null)."add/";
                    break;
                case DEF_ACTION_EDIT:
                    $gotopage = WEB_URL.ADMIN_FOLDER.((!empty($page_ingroup)) ? $page_ingroup."/" : null)."edit/?row_id=".$row_id;
                    break;
                case DEF_ACTION_DELETE:
		    		if(FULL_DELETE) {
                        if(!empty($child_table)){
                            // delete child records of subject record from child table
                            $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldVals(array("parent_id" => 0))->setWhere("data_type = '{$child_table}' AND parent_id = '{$param['row_id']}'")->updateRec();
                        }

                        if(substr($main_table, 0, strlen(DB_TABLE_PREFIX)) == DB_TABLE_PREFIX || $main_table == PAGES_TABLE){
    						// delete child records of subject record from same table
                            $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldVals(array("parent_id" => 0))->setWhere("data_type = '{$main_table}' AND parent_id = '{$param['row_id']}'")->updateRec();

                            // delete subject record
                            $_db_control->setTable($main_table)->setWhere("id = '{$param['row_id']}'")->deleteRec();
                            $_db_control->setTable(ATTRIBUTES_TABLE)->setWhere("data_type = '{$main_table}' AND data_id = '{$param['row_id']}'")->deleteRec();
                        }else{
                            // delete child records of subject record from same table
                            $ids = $_db_control->setTable($main_table)->setFields("id")->setWhere("parent_id = '".$param['row_id']."'")->getRec(true);
                            if(!empty($ids)){
                                $_db_control->setTable($main_table)->setFieldVals(array("parent_id" => 0))->setWhere("id IN ('".join("','", $ids)."')")->updateRec();
                            }
                            // delete subject record
                            $_db_control->setTable($main_table)->setWhere("id = '{$param['row_id']}'")->deleteRec();
                        }
					} else {
                        $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("deleted" => 1))->setWhere("data_type = '{$main_table}' AND data_id = '{$param['row_id']}'")->updateRec();
					}
		    		break;
		    	case DEF_ACTION_UNDELETE:
                    $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("deleted" => 0))->setWhere("data_id = '{$param['row_id']}' AND data_type = '".$main_table."'")->updateRec();
		    		break;
		    	case DEF_ACTION_PUBLISH:
                    $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("published" => 1, "date_published" => "NOW()"))->setWhere("data_id = '{$param['row_id']}' AND data_type = '".$main_table."'")->updateRec();
		    		break;
		    	case DEF_ACTION_UNPUBLISH:
                    $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("published" => 0))->setWhere("data_id = '{$param['row_id']}' AND data_type = '".$main_table."'")->updateRec();
		    		break;
		    	case DEF_ACTION_OPEN:	// reserved for sections
                    break;
		    	case DEF_ACTION_DEFAULT:
                    $cat = intval($_db_control->setTable($main_table)->setFields("cat_id")->setWhere("id = '{$param['row_id']}'")->getRecItem());
                    $_db_control->setTable($main_table)->setFieldvals(array("gallery_def" => 0))->setWhere("cat_id = '$cat'")->updateRec(); // clear all defaults
                    $_db_control->setTable($main_table)->setFieldvals(array("gallery_def" => 1))->setWhere("id = '{$param['row_id']}'")->updateRec();
		    		break;
				case DEF_ACTION_CLONE:
                    $_db_control->setTable($main_table)->setFieldvals(array($list_params['titlefld'] => $x_data))->setDataID($param['row_id'])->cloneRec();
					break;
				case DEF_ACTION_SAVEORG:
					// ranks are posted as id:rank[,id:rank]...
					$ranks = explode(",", $x_data);
					foreach($ranks as $rank){
						$rankparts = explode(":", $rank);
                        $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("rank" =>$rankparts[1]))->setWhere("data_id = '".$rankparts[0]."'' AND data_type = '".$main_table."'")->updateRec();
					}
					break;
				default:
		    		break;
		    }
		}

		if(!empty($list_params) && is_null($gotopage)){
            $html = getDataListOutput($list_params, $main_table, $child_table);
	    }

        if(!empty($html) || !empty($gotopage))
            exitAjax(true, array('alert' => $retn, 'html' => $html, 'gotopage' => $gotopage));
        else
            exitAjax(false);
	    break;
    case 'ajaxtrigger':
        // hand-off task to registered trigger function
        list($retn) = $_events->executeTrigger($op, getIfSet($triggercode), $_REQUEST);
        exitAjax(!empty($retn), $retn);
        break;

    // LIST FORMS

    case 'preplistactionadvancedbox':
        $pagedata = getRequestVar('pagedata');
        $x_data = getRequestVar('x_data');              // supplementary data for secondary functions
        $row_ids = getRequestVar('row_ids');

        // see if there is a registered trigger function to handle the action
        $retn = null;
        $gotopage = null;
        $html = $_render->getListAdvancedActionDialog($row_ids, $pagedata, $x_data);
        exitAjax(true, array('alert' => $retn, 'html' => $html, 'gotopage' => $gotopage));
        break;
    case 'saveadvactions':
        $pagedata = getRequestVar('pagedata');
        $page_url = urldecode(getIfSet($pagedata['page_url']));
        $base_page_url = preg_replace("/((\?|\&).*)/i", "", $page_url);
        $row_ids = explode(",", getRequestVar('row_ids'));
        $actions = getRequestVar('data');

        // perform the advanced actions
        if(count($row_ids) > 0 && count($actions) > 0){
            $fieldvals = array();
            foreach($actions as $actionvalset){
                $actionkeyval = explode(">>>", $actionvalset);
                if($actionkeyval[0] == 'visibility'){
                    switch($actionkeyval[1]){
                        case 'published':
                            $fieldvals['published'] = 1; $fieldvals['draft'] = 0; $fieldvals['date_published'] = date(PHP_DATE_FORMAT." ".PHP_TIME_FORMAT);
                            break;
                        case 'draft':
                            $fieldvals['published'] = 0; $fieldvals['draft'] = 1;
                            break;
                    }
                }else{
                    $fieldvals[$actionkeyval[0]] = sanitizeText($actionkeyval[1]);
                }
            }

            foreach($row_ids as $row_id){
                $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals($fieldvals)->setWhere("data_id = '".$row_id."' AND data_type = '".$pagedata['page_ingroup']."'")->updateRec();
            }

            // get the parameters saved by the showlist operation
            $funcrec = $_db_control->setTable(REGISTER_TABLE)->setWhere("`type` = 'showlist' AND `alias` = '$page_url'")->getRec();
            if(!isBlank($funcrec[0]['parameters'])) {
                $list_params = json_decode($funcrec[0]['parameters'], true);
            }

            // get the db tables used on the calling page
            list($main_table, $child_table) = getTablesFromPagedata($pagedata['page_ingroup']);
            $html = getDataListOutput($list_params, $main_table);
            exitAjax(true, array('html' => $html));
        }else{
            exitAjax(false, '');
        }
        break;
	case 'loadorganizer':
		// builds the organizer panel dynamically rather than loading it on page start
		// this way the query that populated the list form is used
		$pagedata = getRequestVar('pagedata');
		$page_subject = $pagedata['page_subject'];
		$page_ingroup = $pagedata['page_ingroup'];
		$page_url = urldecode($pagedata['page_url']);
		$base_page_url = preg_replace("/((\?|\&).*)/i", "", $page_url);

		// get the db tables used on the calling page
        list($main_table, $child_table) = getTablesFromPagedata($pagedata['page_ingroup']);

		// get the parameters saved by the showlist operation
        $funcrec = $_db_control->setTable(REGISTER_TABLE)->setWhere("`type` = 'showlist' AND `alias` = '$page_url'")->getRec();
	    if(!isBlank($funcrec[0]['parameters'])) {
	    	$params = json_decode($funcrec[0]['parameters'], true);
	    	$orgrec = $_db_control->dbGetQuery($params['query']['sql'], $params['query']['args']);

	    	$outp = "";
			$rank = 0;
			if(count($orgrec) > 0){
				$outp.= "<p>Note: Drag boxes to rearrange them. When you're finished, click ";
				$outp.= "<input type=\"button\" class=\"action_saveorg\" value=\"Save Changes\" />\n";
				$outp.= ".</p>\n";
				$outp.= "<p>You can also edit an item by clicking on its thumbnail or title.</p>\n";
				$outp.= "<input type=\"hidden\" id=\"organize_mod\" value=\"\" />\n";
				$outp.= "<ul id=\"organize\" class=\"clearfix\">\n";
				foreach($orgrec as $orgitem){
					if(empty($params['imagefld'])){
						$outp.= "<li>";
						$outp.= $orgitem[$params['titlefld']];
					}else{
						// show image
						if($params['imagefld'] == "image") {
							$folder = MEDIA_FOLDER.IMG_UPLOAD_FOLDER;
							$no_pic = "";
						} else {
							$folder = MEDIA_FOLDER.THM_UPLOAD_FOLDER;
							$no_pic = "";
						}
						$photo_pic	= getIfSet($orgitem[$params['imagefld']]);
                        $height = 0;
                        $width = 100;
                        if(!empty($photo_pic)){
    						$path 		= pathinfo($photo_pic);
    						$filename 	= $path['basename'];
    						$photo_pic	= $folder.$tables['db_table']."/".$filename;
    						if ($filename == "" OR !@file_exists(SITE_PATH.$photo_pic)) $photo_pic = ADMIN_FOLDER."images/no-pic.gif";
    						list($width, $height, $origwidth, $origheight) = $_filesys->constrainImage(SITE_PATH.$photo_pic, ORG_THM_MAX_WIDTH, ORG_THM_MAX_HEIGHT);
                            $photo_pic = "<img src=\"".WEB_URL.$photo_pic."\" border=\"1\" width=\"$width\" height=\"$height\">";
                        }
                        $outp.= "<li style=\"width: ".$width."px; height: ".($height+30)."px\">";
						$outp.= "<a class=\"action_edit editfromorg\" rel=\"".$orgitem['id']."\">";
						$outp.= $photo_pic;
						$outp.= "<br/>".$orgitem[$params['titlefld']];
						$outp.= "</a>";
					}
					$outp.= "<input type=\"hidden\" id=\"rank".$rank."\" class=\"orgitem\" value=\"".$orgitem['id']."\"/>";
					$outp.= "</li>\n";
					$rank++;
				}
				$outp.= "</ul>\n";
			}else{
				$outp.= "There are no items to organize.";
			}
			$outp.= "</div>\n";

			echo $outp;
	    }
		break;

    // CMS EDITORS

    case 'getcmsparams':
        $editor = getIfSet($editor);
        if(!empty($editor)){
            $editor_rec = $_db_control->setTable(PLUGINS_TABLE)->setWhere("incl = '$editor'")->getRec();
            $atts = array(
                "web_url" => WEB_URL,
                "front_theme_url" => WEB_URL.THEME_FOLDER.THEME.CSS_FOLDER,
                "editor_url" => WEB_URL.$editor_rec[0]['folder']
            );
            $custom_settings = json_decode($editor_rec[0]['custom_settings'], true);
            if(is_array($custom_settings)) $atts += $custom_settings;
            exitAjax(true, $atts);
        }else{
            exitAjax(false, null);
        }
        break;

    case 'showstats':
        $_render->showStats();
        break;

    // FILES AND MAIL

    case 'ftptest':
        if(!empty($host) && !empty($user) && !empty($pass)){
            $ftp = new FTP(false);
            if($ftp->connect($host) === true){
                try{
                    if($ftp->login($user, $pass) === true){
                        $ftp->close();
                        exitAjax(true, null);
                    }else
                        exitAjax(false, "FTP connection to ".$host." using user:".$user.", pass:".$pass." failed.");
                } catch (FtpException $e) {
                    exitAjax(false, "FTP connection failed: ".$e->getMessage());
                }
            }else{
                exitAjax(false, "FTP connection to ".$host." failed.");
            }
        }else{
            exitAjax(false, "Host, username and password are required.");
        }
        break;
    case 'smtptest':
        if(!empty($host) && !empty($port)){
            $result = MailerClass::checkSMTPHost($host, $encr, $port);
            if($result === false)
                exitAjax(false, "Communication with ".(($encr != '') ? $encr.'://' : '').$host." on ".$port.' failed.');
            else
                exitAjax(true, null);
        }else{
            exitAjax(false, "Host, port and encryption are required.");
        }
        break;

    // TUNING

    case 'resetcaches':
        $result = $_cache->clear();
        exitAjax(!$result, null);
        break;

    // REPORTS

    case 'clearreportauditlogs':
        $user_id = intval($val);
        if($user_id > 0 && !empty($category)){
            if($_db_control->setTable(EVENT_LOG_TABLE)->setWhere("`user_id` = '".$user_id."' AND `category` = '".$category."'")->deleteRec()){
                $_events->processTriggerEvent('clearAuditLogs');
                exitAjax(true, null);
            }else{
                exitAjax(false, "The event logs could not be cleared.");
            }
        }
        break;
    case 'clearreporterrorlogs':
        $f = fopen(SITE_PATH.ADMIN_FOLDER.CACHE_FOLDER."error.log" ,"w");
        if($f){
            $_events->processTriggerEvent('clearReportErrorLogs');
            fwrite($f, "");
            fclose($f);
            exitAjax(true, null);
        }else{
            exitAjax(false, "The event logs could not be cleared.");
        }
        break;

    // MISC

    case 'downloadfile':
        if($f != ''){
            $f = SITE_PATH.$f;
            if(file_exists($f)){
                // download file
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Content-Type: application/force-download");
                if($t == 'xml' || $t == 'fdbx'){
                    header("Content-Type: application/xml");
                    header("Content-Type: application/download");
                }
                header('Content-disposition: attachment; filename='.basename($f));
                header('Content-length: '.filesize($f));
                readfile($f);
                unlink($f);
                exit;
            }
        }
        break;
}
exit;

/*------------------------------------------------------------------------------------------*/

/**
 * Return JSON results
 * @param boolean $success
 * @param mixed $rtndata
 */
function exitAjax($success, $rtndata = null, $moredata = null){
    global $_error, $_events;

    $_error->setErrorSuppression(false);
    if(is_null($moredata))
       echo json_encode(array('success' => $success, 'rtndata' => $rtndata));
    else
	   echo json_encode(array('success' => $success, 'rtndata' => $rtndata, 'contents' => $moredata));
    $_events->processTriggerEvent(__FUNCTION__);             // alert triggered functions when function executes
	exit;
}

/**
 * Return an array containing main and child tables (optional) from a comma or slash-separated string
 * @param string table1, table2
 * @return array main, child
 */
function getTablesFromPagedata($tablestr){
    $commasep_tables = explode(",", $tablestr);
    $slashsep_tables = explode("/", $tablestr);
    if(count($commasep_tables) > 1){
        $main_table = $commasep_tables[0];
        $child_table = $commasep_tables[1];
    }elseif(count($slashsep_tables) > 1){
        $main_table = $slashsep_tables[1];
        $child_table = null;
    }else{
        $main_table = $tablestr;
        $child_table = null;
    }

    return array($main_table, $child_table);
}

/**
 * Retrieve the HTML rendering of the data list block based on
 * a simulated version of the $_page object
 * @param array $list_params
 * @param array $tables
 * @return string $html
 */
function getDataListOutput($list_params, $main_table){
    global $_render, $_db_control;

    // the ajaxpage array is a simulated version of the $_page object
    // passed to form functions so that the originating page data is preserved
    $ajaxpage = array(
        'altparams' => $list_params['altparams'],
        'altgroups' => $list_params['altgroups'],
        'addqueries' => $list_params['addqueries'],
        'titlefld' => $list_params['titlefld'],
        'imagefld' => $list_params['imagefld'],
        'thumbfld' => $list_params['thumbfld']
    );
    if(isSerializedString($list_params['query_obj'])){
        $query_obj = unserialize($list_params['query_obj']);
        $recset = $_db_control->redo($query_obj);
        $cols = (array) $list_params['cols'];
        $colsize = (array) $list_params['colsize'];
        $colattr = (array) $list_params['colattr'];
        $totalcols = (array) $list_params['totalcols'];
        $buttons = (array) $list_params['buttons'];
        $colattr = $_render->prepColAttr($cols, $colattr);
        $_db_control->setTable($main_table);

        // using the listbody parameters, get the HTML that will be returned to refresh the list object
        ob_start();
        $_render->showListDataRows($recset, $cols, $colsize, $totalcols, $colattr, $buttons, $list_params['buttoncondindex'], $list_params['buttontagfield'], $ajaxpage);
        $html = ob_get_clean();
        return $html;
    }
}

/**
 * Change active theme
 * @param string $curtheme
 * @param string $tovalue
 */
function changeTheme($curtheme, $tovalue){
    global $_filesys;

	// update database settings, theme value
    $_db_control->setTable(SETTINGS_TABLE)->setFieldvals(array("value" => $tovalue))->setWhere("name='THEME'")->updateRec();
	$GLOBALS['THEME'] = $tovalue;
	// update master.css file
	//$cssfolder = substr(STYLES_FOLDER, strlen(INC_FOLDER));
	$_filesys->chMod2(SITE_PATH.STYLES_FOLDER, "0757");
	if($_filesys->chMod2(SITE_PATH.STYLES_FOLDER."master.css", "0757")){
		if(false !== ($fcontents = file_get_contents(SITE_PATH.STYLES_FOLDER."master.css"))){
			$fcontents = str_replace($curtheme, $tovalue, $fcontents);
			file_put_contents(SITE_PATH.STYLES_FOLDER."master.css", $fcontents);
		}
	}else{
		echo "Cannot change to theme file '$tovalue'!";
	}
	displaySettingsThemesTab();
}

?>
