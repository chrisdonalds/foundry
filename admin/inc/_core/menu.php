<?php
// ---------------------------
//
// FOUNDRY ADMIN MENUS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
//
global $_menus, $_events, $_themes;

if(!defined("VALID_LOAD")) die("This file cannot be accessed directly!");

$_events->processTriggerEvent('menu_display');                // alert triggered functions when function executes

$dir = VHOST.ADMIN_FOLDER;

// header

?>
        <div id="header">
            <div id="sitetitle">
                <h1><?php echo (SITE_NAME == "") ? "My ".SYS_NAME." Site" : SITE_NAME; ?></h1>
            </div>
<?php

// top and sub navigation menus

echo $_menus->getAdminMenuHTML();

// toolbar

$toolbar_def = array(
    "usertool" => true,
    "gotowebsite" => true,
    "settings" => true,
    "help" => true,
    "about" => true
);
$themepath = $_themes->getThemePathUnder("admin");
$toolbar_mod = $_events->processQuickTriggerEvent("adminToolbar", array($toolbar_def));
if(empty($toolbar_mod))
    $toolbar_mod = $toolbar_def;
elseif(isset($toolbar_mod[0]))
    $toolbar_mod = $toolbar_mod[0];
?>
            <div id="admtoolbar">
                <?php
                if((bool) getIfSet($toolbar_mod['gotowebsite'])) { ?>
                <div id="admgotowebsite" class="admtoolbaricon">
                    <a href="<?php echo WEB_URL?>" title="Click to go to <?php echo SITE_NAME?> website" class="fa fa-home"></a>
                </div>
                <?php }

                // Additional toolbar elements
                echo $_events->processQuickTriggerEvent("adminToolbarAddons");

                if((bool) getIfSet($toolbar_mod['settings'])) { ?>
                <div id="admsettings" class="admtoolbaricon">
                    <a href="#" id="settingsbtn" title="Click to edit settings" class="fa fa-wrench"></a>
                </div>
                <?php }

                if((bool) getIfSet($toolbar_mod['help'])) { ?>
                <div id="admhelp" class="admtoolbaricon">
                    <a href="#" rel="<?php echo urlencode($_page->help)?>" title="Click for help" class="fa fa-support"></a>
                </div>
                <?php }

                if((bool) getIfSet($toolbar_mod['about'])) { ?>
                <div id="admabout" class="admtoolbaricon">
                    <a href="#" data-width="600px" data-height="600px" data-url="aboutdialog.php" title="Click to view credits and system environment" class="fa fa-exclamation-circle dialogbutton"></a>
                </div>
                <?php }

                if((bool) getIfSet($toolbar_mod['usertool'])) { ?>
                <div id="admusertool" class="admtoolbaricon">
                    <a href="#" title="Click to Log Out" class="fa fa-power-off"></a>
                </div>
                <?php } ?>
            </div>
        </div>

        <div id="admPopupBalloon"><div class="inner_a"></div><div class="inner_b"></div></div>
        <div class="dialogpanel helpdialog" data-width="600" data-height="400" title="<?php echo BUSINESS?> Admin Help" style="display: none;"></div>
<?php
$_events->processTriggerEvent('menu_display_done');                // alert triggered functions when function executes
?>