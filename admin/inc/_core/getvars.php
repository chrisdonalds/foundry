<?php
// ---------------------------
//
// FOUNDRY VARIABLE HANDLER FUNCTIONS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define("VL_FILEPATHS", "file paths");
define("VL_DATAALIASES", "data aliases");
define("VL_DATAMETAS", "data metas");
define("VL_PLUGINS", "plugins");
define("VL_EXTENSIONS", "extensions");
define("VL_FRAMEWORKS", "frameworks");
define("VL_THEMES", "themes");
define("VL_MEDIAATTS", "media attributes");
define("VL_FTPSET", "ftp settings");
define("VL_EMAILSET", "email settings");

/**
 * Return SQL-friendly sanitized text from $_REQUEST wrapped in ''
 * @param string $var
 * @param boolean $cleanhtml
 * @return string
 */
function getQuotedRequestVar($var, $cleanhtml = false){
    if (isset($var) && strlen($var) > 0){
        if(isset($_REQUEST["$var"])){
            $value = $_REQUEST["$var"];
            if (get_magic_quotes_gpc()) {
            	if ($cleanhtml) {
		        	// htmlentities to convert eg. ' to &#39;  (require an html_entities_decode later)
					// stripslashes to convert eg. \" to "
					// strip_tags to convert eg. <b> to &lt;b&gt;
					$value = strip_tags(htmlentities(stripslashes($value), ENT_QUOTES));
				}else{
					$value = stripslashes($value);
				}
    		}
			$value = iconv("UTF-8", "ISO-8859-1//IGNORE", $value);
			$value = str_replace("'", "&#39;", $value);
			$value = str_replace("\"", "&#34;", $value);
    		$value = "'" . mysql_escape_string($value) . "'";
    	}else{
    		$value = "''";
    	}
    	return $value;
    }else{
    	return "";
    }
}

/**
 * Return SQL-friendly sanitized text from $_REQUEST
 * @param string $var
 * @param boolean $stripbreak
 * @param integer $options
 * @param mixed $key
 * @param string $typecase
 * @return string
 */
function getRequestVar($var, $stripbreak = false, $options = ENT_QUOTES, $key = null, $typecase = null){
    if (isset($var) && strlen($var) > 0){
		if($key == null) {
			if(isset($_REQUEST[$var])) $request_var = $_REQUEST["$var"];
		}else{
			if(isset($_REQUEST[$var][$key])) $request_var = $_REQUEST["$var"][$key];
		}
        if(isset($request_var)){
        	// htmlentities to convert eg. ' to &#39;  (require an html_entities_decode later)
			// stripslashes to convert eg. \" to "
            if(!is_array($request_var)){
                $value = stripslashes(trim($request_var));
                $value = iconv("UTF-8", "ISO-8859-1//IGNORE", $value);
                $value = str_replace("'", "&#39;", $value);
                if ($options == ENT_QUOTES) {
                    $value = str_replace("\"", "&#34;", $value);
                }
                $value = str_replace("&nbsp;</p>", "&#160;</p>", $value);
                if ($stripbreak) {
                    $value = preg_replace("/^(<p>)+/i", "", $value);
                    $value = preg_replace("/(<\/p>)+$/i", "", $value);
                    $value = preg_replace("/(<br>)+$/i", "", $value);
                }
                if(in_array($typecase, array("string", "boolean", "bool", "float", "array", "object"))){
                	$value = settype($value, $typecase);
                }
                return $value;
            }else{
                return $request_var;
            }
        }else{
	        return null;
        }
    }else{
        return null;
    }
}

/**
 * Return SQL-friendly sanitized array of values from $_REQUEST
 * @param array $args                       List of variable element names to retrieve from $_REQUEST
 * @param string $group                     The parent element that the variables all belong to
 * @return array
 */
function getRequestData($args, $group = null){
    $retn = array();
    if(is_array($args)){
        if(empty($group)){
            foreach($args as $var){
                $retn[$var] = sanitizeText(getRequestVar($var, true));
            }
        }else{
            foreach($args as $var){
                $retn[$var] = sanitizeText(getRequestVar($group, true, ENT_QUOTES, $var));
            }
        }
    }
    return $retn;
}

/**
 * Return sanitized text
 * @param string $content
 * @param integer $options
 * @return string
 */
function sanitizeText($content, $options = ENT_QUOTES){
    if (isset($content)){
    	// htmlentities to convert eg. ' to &#39;  (require an html_entities_decode later)
		// stripslashes to convert eg. \" to "
        if(is_array($content)) return sanitizeArray($content);

		$value = stripslashes(strip_tags(trim($content)));
		$value = iconv("UTF-8", "ISO-8859-1//IGNORE", $value);
		$value = str_replace("'", "&#39;", $value);
		if(($options & ENT_QUOTES) > 0) $value = str_replace("\"", "&#34;", $value);
		$value = str_replace("&nbsp;</p>", "&#160;</p>", $value);
        return $value;
    }else{
        return null;
    }
}

/**
 * Return sanitized array
 * @param string $var
 * @param integer $options
 * @return string
 */
function sanitizeArray($array, $options = ENT_QUOTES){
    if (is_array($array) || is_object($array)){
        // htmlentities to convert eg. ' to &#39;  (require an html_entities_decode later)
        // stripslashes to convert eg. \" to "
        foreach($array as $key => $arrayval){
            $value = stripslashes(strip_tags(trim($arrayval)));
            $value = iconv("UTF-8", "ISO-8859-1//IGNORE", $value);
            $value = str_replace("'", "&#39;", $value);
            if(($options & ENT_QUOTES) > 0) $value = str_replace("\"", "&#34;", $value);
            $value = str_replace("&nbsp;</p>", "&#160;</p>", $value);
            $array[$key] = $value;
        }
        return $array;
    }else{
        return null;
    }
}

/**
 * Return sanitized text originally formatted by CMS editor
 * @param string $content
 * @param integer $options
 * @return string
 */
function sanitizeEditorText($content){
    if(CMS_EDITOR == 'ckeditor'){
    	$content = str_replace(array("&#034;"), array("\""), $content);
    }
	return $content;
}

/**
 * Return a sanitized version of input text suitable for database querying
 * @param string $text
 * @return string
 */
function prepareTextForDB($text){
    $text = mysql_real_escape_string($text);
    return $text;
}

/**
 * Return a sanitized version of input text from database result
 * @param string $text
 * @return string
 */
function prepareTextFromDB($text){
    $text = str_replace("\\\\", "\\", $text);
    return $text;
}

/**
 * Remove HTML tags from text
 * @param string $var
 * @return string
 */
function stripHtmlTags($var){
	$var = preg_replace("/<([^>]+)>/i", "", $var);
	return $var;
}

/**
 * Return text formatted in one of four capitalization formats
 * @param string $text
 * @param string $cap_type [optional]
 * @return string
 */
function capitalizeText($text, $cap_type = CAP_WORDS) {
	if ($text != "") {
		switch ($cap_type) {
			case CAP_NONE:
				$text = strtolower($text);
				break;
			case CAP_FIRST:
				$text = strtoupper(substr($text, 0, 1)).strtolower(substr($text, 1));
				break;
			case CAP_WORDS:
				$text = mb_convert_case($text, MB_CASE_TITLE, "UTF-8");
				break;
			case CAP_ALL:
				$text = strtoupper($text);
				break;
		}
	}
	return $text;
}

/**
 * Return text truncated to specific number of words
 * @deprecated
 * @param string $content
 * @param integer $length [optional]
 * @param string $finish [optional] ...
 * @param string $linkto [optional] URL
 * @return string
 */
function limitWords($content, $length = 20, $finish = '...', $linkto = '') {
	// Clean and explode our content; strip all HTML tags, and special characters.
	$words = explode(' ', strip_tags(preg_replace('/[^(\x20-\x7F)]*/','', $content)));
	// Get a count of all words
	$count = count($words);
	$limit = ($count > $length) ? $length : $count;
	// if we have more words than we want to show, add finish
	if($linkto != '') $finish = ' [<a href="'.$linkto.'">'.$finish.'</a>]';
	$end   = ($count > $length) ? $finish : '';
	// create output
	for($w = 0; $w <= $limit; $w++) {
		$output .= $words[$w];
		if($w < $limit) $output .= ' ';
	}
	// return end result
	return $output.$end;
}

/**
 * Return string limited to specific length and appended with ...
 * @deprecated
 * @param string $text
 * @param integer $maxlen
 * @return string
 */
function formatText($text, $maxlen, $dottrailer = '...') {
    if(strlen($text) > $maxlen - 3) $text = substr($text, 0, $maxlen - 3).$dottrailer;
    return $text;
}

/**
 * Return cleaned, lower-cased text
 * @param string $text
 * @return string
 */
function condenseText($text) {
	if ($text != "") {
		$text = strtolower(preg_replace("/[ \.\[\$\|*\+\?\{\\]+/i", "", $text));
	}
	return $text;
}

/**
 * Advanced version of uc_words sensitive to geographic abbreviations, roman numerals, and name prefixes
 * @param string $str
 * @return string
 */
function ucwordsAdv($str) {
    if(!is_string($str)) return $str;

	$all_uppercase = 'Po|Rr|Se|Sw|Ne|Nw|';
	$all_uppercase.= 'Bc|Ab|Sk|Mb|Qu|Nb|Ns|Pe|Nf|Nu|Nt|Yt|';		// Canadian provinces (ON same as English word)
	$all_uppercase.= 'Al|Ak|Az|Ar|Ca|Co|Ct|De|Fl|Ga|Id|Il|';
	$all_uppercase.= 'Ia|Ks|Ky|La|Me|Md|Ma|Mi|Mn|Ms|Mo|Mt|';
	$all_uppercase.= 'Ne|Nv|Nh|Nj|Nm|Ny|Nc|Nd|Oh|Pa|Ri|Sc|';
	$all_uppercase.= 'Sd|Tn|Tx|Ut|Vt|Va|Wa|Wv|Wi|Wy|';				// US States (HI, IN, OK, OR same as English words)
    $all_uppercase.= 'Ca|Us|Usa|Uk|';                               // Country abbreviations
	$all_uppercase.= 'Pdf|Asp|Ups|Usps|Asp|Php|';                   // Technical/Company abbreviations
	$all_uppercase.= 'i|ii|iii|iv|v|vi|vii|viii|ix|x|xx|xxx|';
	$all_uppercase.= 'xl|l|lx|lxx|lxxx|lc|c|';						// Roman numerals
	$all_uppercase.= 'Xs|Cia|Fbi|Rcmp|Csis|Abc|Nbc|Cbs|Ctv|Cbc';	// Others
	$all_lowercase = 'De La|De Las|Der|Van De|Van Der|Vit De';
	$all_lowercase.= 'Von|Or|And|A|As|By|In|Of|Or|To|The';
	$prefixes = 'Mc';
	$suffixes = "'S|&#39;S";
	$is_name = false;

    // captialize all first letters
    $str = preg_replace_callback('/\\b(\\w)/', function($matches){ return strtoupper($matches[0]); }, strtolower(trim($str)));

    if ($all_uppercase) {
        // capitalize acronymns and initialisms e.g. PHP
        $str = preg_replace_callback("/\\b($all_uppercase)\\b/", function($matches){ return strtoupper($matches[0]); }, $str);
    }
    if ($all_lowercase) {
        // decapitalize short words e.g. and
        if ($is_name) {
            // all occurences will be changed to lowercase
            $str = preg_replace_callback("/\\b($all_lowercase)\\b/", function($matches){ return strtolower($matches[0]); }, $str);
        } else {
            // first and last word will not be changed to lower case (i.e. titles)
            $str = preg_replace_callback("/(?<=\\W)($all_lowercase)(?=\\W)/", function($matches){ return strtolower($matches[0]); }, $str);
        }
    }
    if ($prefixes) {
        // capitalize letter after certain name prefixes e.g 'Mc'
        $str = preg_replace_callback("/\\b($prefixes)(\\w)/", function($matches){ return $matches[0].strtoupper($matches[1]); }, $str);
    }
    if ($suffixes) {
        // decapitalize certain word suffixes e.g. 's
        $str = preg_replace_callback("/(\\w)($suffixes)\\b/", function($matches){ return $matches[0].strtolower($matches[0]); }, $str);
    }
	return $str;
}

/**
  * Split a string into an array of "delimited" tokens, taking "grouping" character strings into account
  * @param string $string			Input string
  * @param string $delim			Delimiter string (default space)
  * @param string $group			Grouping character (default ")
  * @return string
  */
function tokenize($string, $delim = ' ', $group = '"') {
    for($tokens = array(), $nextToken = strtok($string, ' '); $nextToken !== false; $nextToken = strtok($delim)){
        if($nextToken{0} == $group)
            $nextToken = $nextToken{strlen($nextToken)-1} == $group ?
                substr($nextToken, 1, -1) : substr($nextToken, 1) . $delim . strtok($group);
        $tokens[] = $nextToken;
    }
    return $tokens;
}

/**
 * Advanced version of PHP extract.  ExtractVariables decodes HTML entities
 * @param array $arry
 */
function extractVariables($arry, $extract_type = EXTR_OVERWRITE, $prefix = null) {
    if(is_array($arry)){
        foreach ($arry as $key => $value){
            if(is_array($value)) {
                extractVariables($value, $extract_type, $prefix);
            }else{
                $tempvar = html_entity_decode($value, ENT_NOQUOTES);
                if(!isset($$key) || $extract_type == EXTR_OVERWRITE){
                    if($extract_type == EXTR_PREFIX_ALL && !is_null($prefix)){
                        $GLOBALS[$prefix.$key] = $tempvar;
                    }else{
                        $GLOBALS[$key] = $tempvar;
                    }
                }elseif(($extract_type == EXTR_PREFIX_SAME || $extract_type == EXTR_PREFIX_ALL) && !is_null($prefix)){
                    $GLOBALS[$prefix.$key] = $tempvar;
                }elseif($extract_type == EXTR_IF_EXISTS){
                    $GLOBALS[$key] = $tempvar;
                }elseif($extract_type == EXTR_PREFIX_IF_EXISTS && !is_null($prefix)){
                    $GLOBALS[$prefix.$key] = $tempvar;
                }elseif($extract_type == EXTR_SKIP){
                    continue;
                }
            }
        }
    }
}

/**
 * Insert an array element after a specific key.  If $after is blank or not found, element will be appended to array.
 * @param array $array 		Source array (by reference)
 * @param string $after 	Key of element that will precede the insertion (element will be inserted after this key)
 * @param string $key 		Key of new element
 * @param mixed $element 	The element to insert
 */
function injectArrayElement(&$array, $after, $key, $element){
	if(is_array($array)){
		$array[$key] = $element;
		if(!empty($after) && isset($array[$after])){
			unset($array[$key]);
			$newarray = array();
			foreach($array as $akey => $aval){
				$newarray[$akey] = $aval;
				if($akey == $after){
					$newarray[$key] = $element;
				}
			}
			$array = $newarray;
		}
	}
}

/**
 * Split an array element at a specific key.
 * @param array $array      Source array
 * @param string $before    Key of element that, if exists, marks the point at which the array is split
 */
function splitArray($array, $before){
    if(is_array($array) && is_string($before)){
        $first = $second = array();
        $before = trim(strtolower($before));
        $found = false;
        foreach($array as $k => $v){
            if(strtolower($k) == $before) $found = true;
            if($found)
                $second[$k] = $v;
            else
                $first[$k] = $v;
        }
    }else{
        $first = $array;
        $second = array();
    }
    return array($first, $second);
}

/**
 * Return variable value, if set, or null
 * @param mixed $var
 * @param mixed $default
 * @return mixed|null
 */
function getIfSet(&$var, $default = null){
	if(isset($var)){
		return $var;
	}else{
		return $default;
	}
}

/**
 * Return integer value, if set, or 0
 * @param mixed $var
 * @param mixed $default
 * @return mixed|int
 */
function getIntValIfSet(&$var, $default = false){
    $val = getIfSet($var);
	if(!empty($val))
		return intval($var);
	else
		return $default;
}

/**
 * Return boolean value, if set, or false
 * @param mixed $var
 * @param mixed $default
 * @return mixed|boolean
 */
function getBooleanIfSet(&$var, $default = false){
    $val = getIfSet($var);
    if(!empty($val))
		return (bool) $var;
	else
		return $default;
}

/**
 * Return the number of elements of an array if the array exists or zero if it doesn't
 * @param array $array
 * @return int
 */
function countIfSet(&$array){
	if(is_array(getIfSet($array))){
		return count($array);
	}else{
		return 0;
	}
}

/**
 * Check each array element and return the array with elements set, or null
 * @param array $array
 * @param array $strElements
 * @param array $intElements
 * @return array
 */
function getIfSetArray($array, $strElements = array(), $intElements = array()){
    $rtn = array();
    if(is_array($array)){
        if(is_array($strElements)){
            foreach($strElements as $elem => $val){
                $rtn[$elem] = getIfSet($array[$elem], $val);
            }
        }
        if(is_array($intElements)){
            foreach($intElements as $elem => $val){
                $rtn[$elem] = getIntValIfSet($array[$elem], $val);
            }
        }
    }
    return $rtn;
}

/**
 * Return scalar value, if set, or null
 * @param mixed $var
 * @param mixed $element
 * @param mixed $default
 * @return mixed|boolean
 */
function getScalarIfSet(&$var, $element, $default = null){
    if(!is_array($var))
        return $var;
    else
        return getIfSet($var[$element], $default);
}

/**
 * Return a formatted value, if set, or null
 * @param mixed $var
 * @param string $default
 * @param string $format                    A PHP sprintf() format
 * @return mixed|string
 */
function getFormatIfSet(&$var, $default = null, $format = ""){
    if(!isset($var))
        return $default;
    else
        return sprintf($format, $var);
}

/**
 * Returns an array containing only the desired values found by key from the source array, or
 *      an array of keys with values that match the optional $value string
 * @param array $array                  Source array
 * @param string|integer $key           Key or index of element desired
 * @param string [optional]             String value to search for
 * @param boolean [optional]            Returns array without null, false, or zero values
 * @return array
 */
function getArrayElements($array, $key, $value = null, $not_null = false){
    if (is_array($key) || !is_array($array)) return array();
    $funct = create_function('$e', 'return is_array($e) && array_key_exists("'.$key.'", $e) ? $e["'. $key .'"] : null;');
    $outarray = array_map($funct, $array);
    if(!is_null($value)){
        $outarray = array_keys($outarray, $value);
    }elseif($not_null){
        $outarray = array_filter($outarray);
    }
    return $outarray;
}

/**
 * Returns if any elements in array1 are in array2
 * @param array $array1
 * @param array $array2
 * @return boolean
 */
function getIfArraysIntersect($array1, $array2){
    $rtn = false;
    if(is_array($array1) && is_array($array2)){
        foreach($array1 as $elem){
            if(in_array($elem, $array2)){
                $rtn = true;
                break;
            }
        }
    }
    return $rtn;
}
/**
 * Return whether or not variable is '', blank, empty, not set, or null
 * @param mixed $var                The variable to test
 */
function isBlank(&$var){
	return (!isset($var) || $var == '' || (empty($var) && !is_numeric($var)) || is_null($var));
}

/**
 * Return whether or not variable can be cast to a string
 * @param  mixed $var               The variable to test
 */
function isStringable(&$var){
	return (!is_array($var) && !is_object($var) && !is_resource($var));
}

/**
 * Return constant value, if set, or null
 * @param $constname	           The name of the constant
 * @return mixed|null
 */
function getConstIfSet($constname){
	if(defined($constname)){
		return constant($constname);
	}else{
		return null;
	}
}

/**
 * Returns whether or not string is valid JSON
 * @param string $json_string       The string to test
 * @return boolean
 */
function isJSONString($json_string){
    $val = json_decode($json_string);
    return (
        json_last_error() == JSON_ERROR_NONE
        && (string) intval($json_string) != $json_string
        && !is_null($json_string)
        && $json_string != "");
}

/**
 * Corrects invalid string lengths in UTF-8 encoded serialized strings
 * @param string $serialized        The string to repair
 * @return string
 */
function serializeFix($serialized){
    $serialized = preg_replace_callback(
        '!(?<=^|;)s:(\d+)(?=:"(.*?)";(?:}|a:|s:|b:|d:|i:|o:|N;))!s',
        'serializeFix_callback',
        $serialized
    );
}

/**
 * Callback for serializeFix()
 * @param string $match
 * @return string
 */
function serializeFix_callback($match){
    return 's:' . strlen($match[2]);
}

/**
 * Returns whether or not string is a well-formed serialized string
 * @param string $ser_string        The string to test
 * @return boolean
 */
function isSerializedString($ser_string){
    global $_error;

    $_error->setErrorSuppression();                  // @ might not be caught and ignored by PHP
    $data = @unserialize($ser_string);
    $_error->setErrorSuppression(false);
    if ($ser_string === 'b:0;' || $data !== false)
        return true;
    else
        return false;
}

/**
 * Return a string with the needle replaced with $replace.  It also checks if the $haystack is a JSON or serialized object
 * @param string $needle            The string to look for
 * @param string $haystack          The string to look in
 * @param string $replace           The replacement string
 * @return null|string
 */
function replaceStringinHybridString($needle, $haystack, $replace){
    if(isJSONString($haystack)){
        $data = json_decode($haystack, true);
        if(is_array($data)){
            foreach($data as $key => $str){
                $data[$key] = str_replace($needle, $replace, $str);
            }
            return json_encode($data);
        }
    }elseif(isSerializedString($haystack)){
        $data = @unserialize($haystack);
        if(is_array($data)){
            foreach($data as $key => $str){
                $data[$key] = str_replace($needle, $replace, $str);
            }
            return serialize($data);
        }
    }else{
        return str_replace($needle, $replace, $haystack);
    }
    return null;
}

/**
 * Prepends a slash, if needed, to a string
 * @param string $string            The string to test
 * @param string $slash             The slash character to prepend
 * @return string
 */
function prependSlash($string, $slash = "/"){
    if(substr($string, 0, 1) != $slash && !empty($string)) $string = $slash.$string;
    return $string;
}

/**
 * Appends a slash, if needed, to a string
 * @param string $string            The string to test
 * @param string $slash             The slash character to append
 * @return string
 */
function appendSlash($string, $slash = "/"){
    if(substr($string, -1, 1) != $slash && !empty($string)) $string .= $slash;
    return $string;
}

/**
 * Parse an attribute string in the form attr:'value', attr:'value...
 * @param string $atts
 * @param array $defaults [optional]
 * @return array Attributes in array
 */
function parseAttributes($atts, $defaults = null){
    $attrs = array();
    if(is_array($defaults)) foreach($defaults as $elem) $attrs[$elem] = "";
    if(is_array($atts)){
        foreach($atts as $attkey => $attval){
            $attrs[trim($attkey)] = trim($attval, "' ");
        }
    }
    return $attrs;
}

/**
 * Returns whether $needle is in $haystack.  in() treats $haystack as an array if it is a string list
 * @param string $needle
 * @param string|array $haystack
 * @return boolean
 */
function in($needle, $haystack){
    $ok = false;
    if(is_string($needle)){
        if(!is_array($haystack)) $haystack = explode(",", $haystack);
        if(in_array($needle, $haystack)) $ok = true;
    }
    return $ok;
}

/**
 * Return the HTML of a clickvar dropdown list
 * @param array $typelist           Null or an array of typelist values
 *                                   VL_EMAILSET, VL_FTPSET, VL_MEDIAATTS, VL_EXTENSIONS, VL_THEMES
 *                                   VL_FRAMEWORKS, VL_PLUGINS, VL_DATAMETAS, VL_FILEPATHS
 * @param mixed $customlist         Empty array (default) or a custom array of option values (key => value)
 * @return string
 */
function showClickableVariables($typelist = null, $customlist = array()){
    global $_system;

    $html = '';
    if(!is_array($typelist)) $typelist = (array) $typelist;
    foreach($typelist as $type){
        $arry = array();
        $heading = $type;
        switch($type){
            case VL_EMAILSET:
                $arry = array(
                    "SMTP Host" => $_system->configs['SMTP_HOST'],
                    "SMTP User" => $_system->configs['SMTP_USERNAME'],
                    "SMTP Password" => $_system->configs['SMTP_PASSWORD'],
                    "SMTP Port" => $_system->configs['SMTP_PORT'],
                    "SMTP Security" => $_system->configs['SMTP_SECURE_MODE']
                );
                break;
            case VL_FTPSET:
                $arry = array(
                    "FTP Host" => $_system->configs['FTPHOST'],
                    "FTP Connection Type" => $_system->configs['FTPCONN'],
                    "FTP Username" => $_system->configs['FTPUSER'],
                    "FTP Password" => $_system->configs['FTPPASS']
                );
                break;
            case VL_MEDIAATTS:
                break;
            case VL_EXTENSIONS:
                foreach($_system->plugins as $plugin){
                    if($plugin['builtin'] == 1 && $plugin['is_framework'] == 0 && $plugin['is_deleted'] == 0) $arry[$plugin['incl']] = $plugin['name'];
                }
                break;
            case VL_THEMES:
                break;
            case VL_FRAMEWORKS:
                foreach($_system->plugins as $plugin){
                    if($plugin['builtin'] == 0 && $plugin['is_framework'] == 1 && $plugin['is_deleted'] == 0) $arry[$plugin['incl']] = $plugin['name'];
                }
                break;
            case VL_PLUGINS:
                foreach($_system->plugins as $plugin){
                    if($plugin['builtin'] == 0 && $plugin['is_framework'] == 0 && $plugin['is_deleted'] == 0) $arry[$plugin['incl']] = $plugin['name'];
                }
                break;
            case VL_DATAMETAS:
                $arry = array(
                    "CATEGORY" => "@cat@",
                    "CODE" => "@code@",
                    "ID" => "@id@",
                    "CAT_ID" => "@catid@",
                    "TYPE" => "@type@",
                    "YEAR" => "@year@",
                    "MONTH" => "@month@",
                    "DAY" => "@day@",
                    "HOUR" => "@hour@",
                    "MINUTE" => "@minute@",
                    "SECOND" => "@second@",
                );
                break;
            case VL_DATAALIASES:
                break;
            case VL_FILEPATHS:
                $arry = array(
                    "WEB_URL" => WEB_URL,
                    "SITE_PATH" => SITE_PATH,
                    "ADMIN_FOLDER" => ADMIN_FOLDER,
                    "INC_FOLDER" => ADMIN_FOLDER.INC_FOLDER,
                    "CORE_FOLDER" => ADMIN_FOLDER.CORE_FOLDER,
                    "EDITOR_FOLDER" => ADMIN_FOLDER.EDITOR_FOLDER,
                    "LIB_FOLDER" => ADMIN_FOLDER.LIB_FOLDER,
                    "PLUGINS_FOLDER" => ADMIN_FOLDER.PLUGINS_FOLDER,
                    "EXTENSIONS_FOLDER" => ADMIN_FOLDER.EXTENSIONS_FOLDER,
                    "THEME_FOLDER" => ADMIN_FOLDER.THEME_FOLDER,
                    "SETTINGS_FOLDER" => ADMIN_FOLDER.SETTINGS_FOLDER
                );
                break;
            default:
                $arry = $customlist;
                break;
        }

        $optgroup = '';
        if(!empty($arry)){
            $optgroup = '<optgroup label="'.$heading.'">';
            foreach($arry as $key => $elem){
                $optgroup .= '<option value="'.$elem.'">'.$key.'</option>';
            }
            $optgroup .= '</optgroup>';
        }
        if($optgroup != '') $html .= $optgroup;
    }
    if($html != '') $html = '<div class="clickvars"><a href="#">[Available Variables]</a><select>'.$html.'</select></div>';
    return $html;
}
?>