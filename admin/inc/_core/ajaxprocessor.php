<?php
// ---------------------------
//
// AJAX PROCESSOR
//
//  - Handles generic SQL requests
//  - Use Ajaxwrapper for specific functions
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

// define("IN_AJAX", true);
// if(!defined("LOADER_DOCUMENT_ROOT")) include("loader.php");

define("VALID_LOAD", true);
define("BASIC_GETINC", true);
define("VHOST", substr(str_replace("\\", "/", realpath(dirname(__FILE__)."/../../../")), strlen(realpath($_SERVER['DOCUMENT_ROOT'])))."/");
define("DB_USED", ((isset($_REQUEST['db_used'])) ? (bool) $_REQUEST['db_used'] : true));

include ($_SERVER['DOCUMENT_ROOT'].VHOST."admin/inc/_core/getinc.php");				// required - starts PHP incls!!!

// Retrieve data from Query String
//		-- action, table, flds, vals, crit, orderby
// Escape User Input to help prevent SQL Injection
extractVariables($_GET);

// Build query
if(!empty($action) && !empty($table)){
	$args = array();
	if(empty($flds)) $flds = "*";
	if(!empty($crit)) {
		$args[] = stripslashes($crit);
		$crit = " WHERE %s";
	}
	if(!empty($orderby)) {
		$args[] = $orderby;
		$orderby = " ORDER BY %s";
	}
	switch ($action){
		case "select":
			$sql = "SELECT {$flds} AS newfld FROM {$table}{$crit}{$orderby}";
			$qry_result = $_db_control->dbGetQuery($sql, $args);
			if($qry_result){
				// Build output string (returns to Ajax for displaying)
				$retn_string = "";
				for($i=0; $i<count($qry_result); $i++){
					if($i > 0) $retn_string .= "\n";
					$retn_string .= $qry_result[$i]['id']."|".$qry_result[$i]['newfld'];
				}
				echo $retn_string;
			}
		case "update":
			if(!empty($crit) && !empty($flds) && is_string($flds) && !empty($vals) && is_string($vals) && empty($orderby)){
				$fldarray = split("(,|, )", $flds);
				$valarray = split("(,|, )", $vals);
				$fldset   = "";
				for($i = 0; $i < count($fldarray); $i++){
					if($fldset != "") $fldset .= ", ";
					$fldset .= $fldarray[$i]."='".$valarray[$i]."'";
				}
				$sql = "UPDATE {$table} SET ".$fldset."{$crit}";
				$qry_result = $_db_control->dbUpdateQuery($sql, $args);
			}
			break;
		case "insert":
			if(empty($crit) && !empty($flds) && !empty($vals) && is_string($vals) && empty($orderby)){
				$valarray = split("(,|, )", $vals);
				$vals = implode("','", $valarray);
				$args[] = $flds;
				$args[] = $vals;
				$sql = "INSERT INTO {$table} (%s) VALUES ('%s')";
				$qry_result = $_db_control->dbInsertQuery($sql, $args);
			}
			break;
		case "delete":
			if(!empty($crit) && empty($orderby)){
				$sql = "DELETE FROM {$table}{$crit}";
				$qry_result = $_db_control->dbDeleteQuery($sql, $args);
			}
			break;
	}
}
?>
