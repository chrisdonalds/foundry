<?php
// ---------------------------
//
// FOUNDRY PUBLIC SITE GD FUNCTIONS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
//

extract($_GET);
switch($op){
	case 'mask':
		maskText($st, $fs, $fg);
		break;
}

function maskText($string, $fontsize = 3, $fg = "#000000"){
	header ("Content-type: image/png");

	//Get string info
	$string = urldecode(base64_decode($string));

	//Get the size of the string
	$width = imagefontwidth($fontsize) * strlen($string);
	$height = imagefontheight($fontsize);

	//Create the image
	$img = @imagecreatetruecolor($width, $height)
	      or die("Cannot Initialize new GD image stream");

	//Make it transparent
	imagesavealpha($img, true);
	$trans_colour = imagecolorallocatealpha($img, 0, 0, 0, 127);
	imagefill($img, 0, 0, $trans_colour);

	//Get the text color
	$color = hex2RGB($fg);
	$text_color = imagecolorallocate($img, $color['red'], $color['green'], $color['blue']);

	//Draw the string
	imagestring($img, $fontsize, 0, 2, $string, $text_color);

	//Output the image
	imagepng($img);
	imagedestroy($img);
}

/**
 * Convert a hexadecimal color code to its RGB equivalent
 *
 * @param string $hexStr (hexadecimal color value)
 * @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
 * @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
 * @return array or string (depending on second parameter. Returns False if invalid hex color value)
 */
function hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {
    $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
    $rgbArray = array();
    if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
        $colorVal = hexdec($hexStr);
        $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
        $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
        $rgbArray['blue'] = 0xFF & $colorVal;
    } elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
        $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
        $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
        $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
    } else {
        //return false; //Invalid hex color code
        $rgbArray['red'] = 0;
        $rgbArray['green'] = 0;
        $rgbArray['blue'] = 0;
    }
    return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
}
?>