<?php
// ---------------------------
//
// INIT LOADER
// - For use by any foreign or plugin code
//   that requires loading of Foundry Admin
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
if(isset($_SERVER['DOCUMENT_ROOT']) && $_SERVER['DOCUMENT_ROOT'] != ''){
    define ("LOADER_DOCUMENT_ROOT", $_SERVER['DOCUMENT_ROOT']);
    if(!defined('VHOST')) define("VHOST", substr(str_replace("\\", "/", realpath(dirname(__FILE__)."/../../../")), strlen(realpath($_SERVER['DOCUMENT_ROOT'])))."/");
}else{
    define ("LOADER_DOCUMENT_ROOT", str_replace("\\", "/", realpath(dirname(__FILE__)."/../../../")));
    if(!defined('VHOST')) define("VHOST", "/");
}

// start Foundry bootstrap
include (LOADER_DOCUMENT_ROOT.VHOST."admin/inc/_core/getinc.php");

// login url redirection
if(isset($url)){
	$path = dirname($url);
	chdir("../../../../");
	include($url);
	exit;
}

// $_GET values
$func_caller = getRequestVar('fn');
$func_to_call = getRequestVar('fc');
$func_params = getRequestVar('fp');       // val,val2
loader_call_func($func_caller, $func_to_call, $func_params);

// If script did not end in above function call, continue with rest of script page

// direct file bypasses
$filespec = getRequestVar('f');
if(empty($filespec)) $filespec = substr($_SERVER['REQUEST_URI'], strlen(VHOST));
if(strpos($filespec, "ajaxwrapper.php") !== false || strpos($filespec, AJAX_ALIAS) !== false){
    // ... ajaxwrapper
    return;
}elseif(strpos($filespec, THUMB_GEN_ALIAS) !== false){
    // ... thumbgen
    unset($_GET['f']);
    header('location: '.WEB_URL.THUMB_GEN.'?'.http_build_query($_GET));
    return;
}elseif(strpos($filespec, DB_CFG_ALIAS) !== false){
    // ... database config
    return;
}

// get primary active theme folder
$themefolder = $_themes->getThemePathUnder("admin");

// run crons
if(!PHP_CLI) $_cron->run_crons();

if($themefolder !== false && file_exists(SITE_PATH.$themefolder)){
    // prepare _PAGE class properties

    $fileparts = parse_url($filespec);
    $filename = getIfSet($fileparts['path']);

    $_page->name = $filename;
    if(preg_match("/[.]/", $_page->name)) {
        // remove extension from name, file is ok
        $_page->name = preg_replace("/([.].*)/", "", $_page->name);
    }
    $_page->urlpath = explode("/", $_page->name);

    // get file query elements
    $fileparts = parse_url($_SERVER['REQUEST_URI']);
    $filequery = getIfSet($fileparts['query']);
    parse_str($filequery, $qv);
    if(count($qv) > 0) {
        $_page->queryvars = $qv;
    }else{
        $_page->queryvars = array();
    }

    // physical file or file alias?
    if(file_exists(SITE_PATH.$filename)){
        // physical file
        $_page->target = $filename;
        $_page->targettype = TARGETTYPE_PHPFILE;
        $_page->found = true;
    }else{
        // any system files?
        $sysfiles = array(
            "settings(.*)" => ADMIN_FOLDER.SETTINGS_FOLDER."settings.php$1",
        );
        foreach($sysfiles as $sysalias => $syspath){
            preg_match("/".$sysalias."/i", $filename, $matches);
            if(count($matches) > 0){
                // matched to system file
                $filename = (count($matches) == 1) ? $syspath : str_replace("$1", $matches[1], $syspath);
                if(strpos($sysalias, "settings") !== false)
                    $_page->targettype = TARGETTYPE_SETTINGS;
                else
                    $_page->targettype = TARGETTYPE_SYSTEMFILE;
                $_page->target = $filename;
                $_page->found = true;
                break;
            }
        }

        // get the alias instead
        if(!$_page->found) $_menus->getAdminMenuTargetFromAlias();
    }

    if($_page->found){
        // late-include plugins that satisfy contexts
        $_plugins->includePluginsMatchingContexts();

        // load an aliased file
        if($_page->targettype != TARGETTYPE_PHPFILE)
            @include(SITE_PATH.$_page->target);
        return;
    }else{
        $errlink = $_filesys->getErrorDocFile("admin", "404");
        if($errlink !== false){
            //... EC140: nothing to handle the URI, go to physical 404.php or database '404' page
            @include_once($errlink);
            return;
        }else{
            //... EC142: safety net. output a fast-404 page
            echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"
                \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
            <html xmlns=\"http://www.w3.org/1999/xhtml\">
            <head>
            <title>Page not found</title>
            <meta name=\"copyright\" content=\"Copyright ".date("Y")." ".SYS_NAME." Development Group\"/>
            <meta name=\"content-language\" content=\"EN\" />
            <meta name=\"generator\" content=\"".SYS_NAME." ".CODE_VER." (".CODE_VER_NAME.")\" />
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
            </head>
            <body>
            <h1>Oops, We Have a Problem!</h1>
            <p>The page or resource, ".strtoupper($_page->uri).", was not found.</p>
            <p>As this message is presented on the rare occasion that the site has encountered a problem,
            we recommend than you:</p>
            <ul>
            <li>Discuss this issue with the webmaster or developer, or;</li>
            <li>Contact the administrator or hosting provider.</li>
            </ul>
            <p>ERROR: EC142</p>
            </body>
            </html>";
            exit;
        }
    }
}else{
    //... EC150: no theme folder found
    echo "
    <h1>Oops, We Have a Problem!</h1>
    <p>No active website themes have been installed or activated, and the default website theme could not be found.</p>
    <p>We recommend than you discuss this issue with the webmaster or developer.</p>
    <p>ERROR: EC150</p>";
    exit;
}

function loader_call_func($func_caller, $func_to_call, $func_params = null){
    // Call system functions dynamically
    if($func_to_call != ''){
        if(function_exists($func_to_call)) {
            if($func_params != ''){
                $func_params = urldecode($func_params);
                $func_param_arry = explode(",", $func_params);
                call_user_func_array($func_to_call, $func_param_arry);
            }else{
                call_user_func($func_to_call);
            }
        }else{
            if($func_caller != '') $func_caller = "called from '$func_caller' ";
            die("Init: Function named '$func_to_call' {$func_caller} does not exist.");
        }
    }
}
?>
