<?php
// ------------------------------------------
//
// FOUNDRY ADMIN LOADER & INITIATOR
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ------------------------------------------
//
//*****************************************************************************
//   MANDATORY ADMIN INCLUSIONS
//*****************************************************************************

define("IN_ADMIN", true);
if(!defined("DB_USED")) define("DB_USED", true);
if(!defined("VHOST")) define("VHOST", "/");
if(!defined("IN_AJAX")) define("IN_AJAX", false);
if(!defined("VALID_LOAD")) define("VALID_LOAD", true);

//*****************************************************************************
//   BASIC LOAD
//*****************************************************************************

include_once (LOADER_DOCUMENT_ROOT.VHOST."admin/inc/_config/configs.php");
if(strpos(SITE_PATH, "/") === false) die("Configuration Loading Failure: Check path to configs.php in admin getinc.php");
require_once (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."common_core.php");
if(!defined("CORELOADED")) die("Core Engine (COMMON_CORE) was not loaded!");

require_once (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."collector.class.php");
if(!defined("COLLECTLIBLOADED")) die("DI Collector Class Library was not loaded!");

//*****************************************************************************
//   DATABASE AND CLASS LIBRARIES SETUP
//*****************************************************************************

$_system = SystemClass::init($configs);
$_events   = EventsClass::init();
$_events->processTriggerEvent('init_system');                // alert triggered functions when function executes
$_debug    = DebugClass::init();

if(DB_USED){
    // the database is active in almost all cases -- except when the database is
    // being reconfigured, backed up, restored, or is under some sort of conditioning
    require_once (SITE_PATH.ADMIN_FOLDER.DB_FOLDER."db_wrapper.class.php");
    require_once (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."db-control.class.php");
    $_db_control = new DB_controller(DBHOST, DBNAME, DBUSER, DBPASS, DBPORT);
    if(!defined("DBLOADED")) die("Database controller class was not loaded!");
    $_collector = new CollectorClass($_db_control);

    // DI Collector-dependent classes
    $_media  = MediaClass::init($_collector);
}
$_error    = ErrorClass::init();
$_settings = SettingsClass::init();
$_users    = UsersClass::init();
$_stats    = StatsClass::init();
$_filesys  = FileSysClass::init();
$_sec      = SecurityClass::init();
$_menus    = MenuClass::init();
$_data     = DataClass::init();
$_cron     = CronClass::init();
$_cache    = CacheClass::init();

//*****************************************************************************
//   LONG LOAD (QUICK LOAD SKIPS PLUGINS AND FORMS, ETC)
//*****************************************************************************

if(!QUICK_LOAD){
    $_themes        = ThemeClass::init();
    $_plugins       = PluginsClass::init();
    $_editors       = EditorsClass::init();
    $_render        = AdminRenderClass::init();
    $_fields        = FieldsClass::init();
    $_tax           = TaxonomiesClass::init();
    $_page          = PageClass::init();
    $_js            = JSBlock::init();
    $_rss           = RSSClass::init();
    $_inflect       = new Inflector();
}

//*****************************************************************************
//   SUPPLEMENTARY INCLUSIONS
//*****************************************************************************

require_once (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."getvars.php");
require_once (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."geterrormsgs.php");
include_once (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."getbrowser.php");

//*****************************************************************************
//   SESSION PREPARATION
//*****************************************************************************

session_name('admin');
session_start();

//*****************************************************************************
//   SYSTEM PREPARATION
//*****************************************************************************

if(!QUICK_LOAD){
    $_events->processTriggerEvent('admin_getinc_start_regload');                // alert triggered functions when function executes
    $_SESSION['prevpage'] = getIfSet($_SESSION['curpage']);
    $_SESSION['curpage'] = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $info = array("referer" => $_SESSION['prevpage'], "curpage" => $_SESSION['curpage'], "prevpage" => $_SESSION['prevpage']);
    $_system->info = $info;

    //*****************************************************************************
    //   USER-CLASS PREPARATION
    //*****************************************************************************

    $_users->isloggedin = getIfSet($_SESSION['admlogin']);
    $_users->id = getIfSet($_SESSION['userdata']['id']);
    $_users->level = getIfSet($_SESSION['userdata']['level']);
    $_users->logintimestamp = getIfSet($_SESSION['timestamp']);
    $_users->username = getIfSet($_SESSION['userdata']['username']);
    $_users->firstname = getIfSet($_SESSION['userdata']['firstname']);
    $_users->lastname = getIfSet($_SESSION['userdata']['lastname']);
    $_users->email = getIfSet($_SESSION['userdata']['email']);
    $_users->image = getIfSet($_SESSION['userdata']['image']);
    $_users->thumb = getIfSet($_SESSION['userdata']['thumb']);
    $_users->twitter = getIfSet($_SESSION['userdata']['twitter_link']);
    $_users->googleplus = getIfSet($_SESSION['userdata']['google_plus_link']);
    $_users->facebook = getIfSet($_SESSION['userdata']['facebook_link']);
    $_users->avatar = getIfSet($_SESSION['userdata']['avatar']);
    $_users->iphash = $_users->getClientIPHash();
    if(DB_USED){
        $_users->activelist = $_db_control->setTable(ACCOUNTS_TABLE)->setWhere("activated=1 AND blocked=0")->setOrder("username")->getRec();
        $_users->initAllowances();
        $_users->defineCustomUserTypes();
    }else{
        $_users->activelist = null;
    }

    //*****************************************************************************
    //   GENERAL PERSISTENCE/PAGE-CLASS PREPARATION
    //*****************************************************************************

    $_page->uri = $_SERVER['REQUEST_URI'];		// uri is used by SESSION to limit values to specific URL
    $_page->row_id = intval(getRequestVar('row_id'));
    $_page->title = preg_replace("/(^[a-z]*-)|(\.[a-z]*$)/i", "", basename($_SERVER['SCRIPT_FILENAME']));
    $_page->nonce = $_sec->createNonce();
    $_page->titlefld = 'itemtitle';
    $_page->imagefld = 'image';
    $_page->thumbfld = 'thumb';

    if(isset($_REQUEST['page'])) $_SESSION[$_page->uri]['page'] = getRequestVar('page');
    if(isset($_SESSION[$_page->uri]['page'])){
    	$_page->pagenum = (($_SESSION[$_page->uri]['page'] == 0) ? 1 : $_SESSION[$_page->uri]['page']);
    }else{
    	$_page->pagenum = 1;
    }
    $_page->limit = LIST_ROWLIMIT;
    $_page->offset = ($_page->pagenum - 1) * LIST_ROWLIMIT;
    $_page->savebuttonpressed = getRequestVar(SAVEBUTTONPRESSED);

    //*****************************************************************************
    //   LIST SEARCH PERSISTENCE
    //*****************************************************************************

    if(isset($_REQUEST['search_text'])) $_SESSION[$_page->uri]['search_text'] = getRequestVar('search_text');
    if(isset($_REQUEST['search_by'])) $_SESSION[$_page->uri]['search_by'] = getRequestVar('search_by');
    if(isset($_REQUEST['sort_by'])) $_SESSION[$_page->uri]['sort_by'] = getRequestVar('sort_by');
    if(isset($_REQUEST['sort_dir'])) $_SESSION[$_page->uri]['sort_dir'] = getRequestVar('sort_dir');
    $_page->search_text = getIfSet($_SESSION[$_page->uri]['search_text']);
    $_page->search_by = getIfSet($_SESSION[$_page->uri]['search_by']);
    $_page->sort_by = getIfSet($_SESSION[$_page->uri]['sort_by']);
    $_page->sort_dir = getIfSet($_SESSION[$_page->uri]['sort_dir']);
    if($_page->search_by == '' || is_null($_page->search_by)) $_page->search_by = 'all';

    //*****************************************************************************
    //   DATA ALIASES
    //*****************************************************************************

    $_data->initDataObjects();
    $_tax->initTaxonomies();
    if(count($_data->datatables) > 0){
        foreach($_data->datatables as $table){
            $_data->reinforceDataAttributes($table);
        }
    }

    if(!defined("BASIC_GETINC")){

    	//*****************************************************************************
        //   ADVANCED INIT
        //
    	//   RESETS
    	//*****************************************************************************

    	$_users->purgeLoginSessions(0, 1, 0, 0);
    	prepareSessionElements();

        //*****************************************************************************
        //   ADMIN MENUS
        //*****************************************************************************

        $_menus->reinforceAdminMenus();

    	//*****************************************************************************
    	//   LOAD INCLUDED ADMIN-FACING PLUGINS
    	//*****************************************************************************

    	$_plugins->getInstalledPlugins();
    	$_plugins->initPluginsandFrameworks();

        //*****************************************************************************
        //   THEME INCLUSIONS
        //*****************************************************************************

        $_themes->getInstalledThemes();

        //*****************************************************************************
        //   LOAD CUSTOM THEME FUNCTIONS
        //*****************************************************************************

        $_themes->initThemeOps();

        //*****************************************************************************
        //   HOUSECLEANING
        //*****************************************************************************

        $_users->getConcurrentLoginsUsingAcct();

    }else{

        //*****************************************************************************
        //    BASIC INIT
        //-----------------------------------------------------------------------------

    	if(DB_USED) $_plugins->initPluginsandFrameworks();

        //*****************************************************************************
        //   THEME INCLUSIONS

    //*****************************************************************************
    //   DATA ALIASES
    //*****************************************************************************
        //*****************************************************************************

        $_themes->getInstalledThemes();

        //*****************************************************************************
        //   LOAD CUSTOM THEME FUNCTIONS
        //*****************************************************************************

        $_themes->initThemeOps();
    }
}

//*****************************************************************************
//   POST-LOAD
//*****************************************************************************

$_events->processTriggerEvent('admin_getinc_done');                // alert triggered functions when function executes
?>