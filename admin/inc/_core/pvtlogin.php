<?php
// -----------------------------
//
// FOUNDRY PRIVATE-VIEWING LOGIN
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
//
$rurl = urldecode((isset($_REQUEST['rurl']) ? $_REQUEST['rurl'] : ''));
define('PVTLOADED', true);
include("loader.php");

// login
$pvterr = "";
if(VHOST != "/") $rurl = str_replace(VHOST, "", $rurl);
if($rurl == "") $rurl = "/";
$remoteip = md5($_SERVER['REMOTE_ADDR']);
if (getRequestVar('pvtsubmit') == "Enter"){
    $pwd = getRequestVar('pvtpwd');
    unset($_SESSION['pvtlogin']);
    if($pwd != ""){
        $ok = $_users->getPrivateLogin($pwd);

        if($ok){
                // trigger
                $_events->createTriggerEvent('pvtlogin');
                $_events->executeTrigger('pvtlogin');

                // log user into system
                // set session for 24 hours from now
                $_SESSION['pvtlogin'] = true;
                setcookie('pvtlogin', date("Y-m-d H:i:s"), time()+60*60*24, '/');

                // set user persistence data
                $_SESSION['timestamp'] = time();

                // rebuild url that got us here
                $rurl = ltrim($rurl, "/");

                // return to calling page
                gotoPage (WEB_URL.$rurl);
        }else{
            $pvterr = "The password is not correct!";
        }
    }else{
        $pvterr = "Please provide a password!";
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo BUSINESS?> Private-Viewing Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="<?php echo COPYRIGHT_NAME?>" />
<meta name="distribution" content="Global" />
<meta name="content-language" content="EN" />
<?php
$_themes->prepHeadSection();
?>
</head>

<body class="login">
	<div id="wrapper">
		<div id="display_core_msg"></div>
		<div id="display_runtime_msg"></div>
		<div id="content-wrapper" class="login-wrap">
			<div id="pvtloginbox" class="clearfix">
				<h2><?php echo SITE_NAME ?></h2>
                <div id="pvtlogininnerbox">
                    <h3>Private Viewing Login</h3>
    				<div id="pvterror"></div>
    				<form method="post" action="pvtlogin.php">
                        <input type="hidden" name="rurl" value="<?php echo $rurl;?>"/>
                        <div id="pvtloginpass">
                            <p>
                                <label for="pvtpwd">Password:</label><br />
                                <input type="password" name="pvtpwd" id="pvtpwd" size="15" value="" autocorrect="off" autocapitalize="off" />
                            </p>
                        </div>
    					<div style="margin: 15px 0px; clear: both;">
    						<input type="submit" name="pvtsubmit" id="pvtsubmit" value="Enter"/>
    					</div>
    				</form>
                </div>
			</div>

<?php
if (!$_users->isPrivatelyLoggedin() || $pvterr != "" || isset($rurl)){
?>
            <script type="text/javascript" language="javascript">
                jQuery('#pvtpwd').focus();
                jQuery('#pvterror').html('<span><?php echo $pvterr?></span>');
            </script>
<?php }

$_render->showFooter();
?>

