<?php
// ---------------------------
//
// FOUNDRY ADMIN ERROR MESSAGES LIST
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

// success messages
define('SUCCESS_CREATE', 'The record created successfully. ');
define('SUCCESS_EDIT', 'The record updated successfully. ');
define('SUCCESS_EDITGROUP', 'All entries updated successfully. ');
define('SUCCESS_DELETE', 'The record deleted successfully. ');
define('SUCCESS_ACTIVATE', 'The record activated successfully. ');
define('SUCCESS_DEACTIVATE', 'The record deactivated successfully. ');

// error messages
define('LOGIN_ERROR', 'Invalid username or password, or your account was not found. ');
define('DUPLICATE_USERNAME', 'The username entered already exists. ');
define('DUPLICATE_RECORD', 'Another %s already exists%s.  Please choose a different %s. ');
define('FAILURE_CREATE', 'The record was not created successfully. ');
define('FAILURE_EDIT', 'The record was not updated successfully. ');
define('FAILURE_EDITGROUP', 'One or more entries were not updated successfully. ');
define('FAILURE_DELETE', 'The record was not deleted successfully. ');
define('FAILURE_ACTIVATE', 'The record was not activated successfully. ');
define('FAILURE_DEACTIVATE', 'The record was not deactivated successfully. ');
define('MISSING_ARG', 'Missing argument(s) for function: ');
define('ACCESS_PAGE_FAIL', 'You do not have the appropriate authorization to access this page.');
define('ACCESS_FUNC_FAIL', 'You do not have the appropriate authorization to access the requested operation.');
define('NONCE_INVALID', 'The form nonce cannot be validated.');
define('DATA_LOCKED', 'This data is currently locked for editing by another user.');

// DB errors
define('DB_DELETE_ERROR', 'DB: Deletion error. ');
define('DB_UPDATE_ERROR', 'DB: Updating error. ');
define('DB_SELECT_ERROR', 'DB: Selecting error. ');

// media errors
define('IMG_BAD_FORMAT', 'Invalid file format. ');
define('IMG_TOO_BIG', 'The uploaded file size exceeds limit');
define('IMG_DIM_TOO_BIG', 'The image [image] exceeds the maximum allowed dimensions');
define('IMG_DIM_WRONG_SIZE', 'The image [image] must be exactly the required dimensions');
define('IMG_FAILURE_COPY', 'File upload failed copying file to ');
define('IMG_PATH_UNWRITABLE', 'The upload path is not writable, please check permissions. ');
define('IMG_NOT_FOUND', 'Cannot find ');
define('IMG_NO_FILENAME', 'No filename provided. ');
define('IMG_CREATE_ERROR', 'Cannot create image GD. ');
define('IMG_RESIZE_ERROR', 'Cannot resize image GD. ');
?>