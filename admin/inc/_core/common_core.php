<?php
// ---------------------------
//
// FOUNDRY COMMON FUNCTIONS
//  - Core Support -
//
// ---------------------------
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// list of tool functions
// - Uses: MySQL 4.2+

define ("CORELOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

// start here
checkCodeVer();

// instantiate classes
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."system.class.php");
if(!defined("SYSLIBLOADED")) die("System Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."error.class.php");
if(!defined("ERRORLIBLOADED")) die("Error Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."debug.class.php");
if(!defined("DEBUGLIBLOADED")) die("Debug Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."sections.class.php");
if(!defined("SECTIONSLIBLOADED")) die("Sections Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."users.class.php");
if(!defined("USERSLIBLOADED")) die("Users Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."filesys.class.php");
if(!defined("FILESYSLIBLOADED")) die("File System Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."security.class.php");
if(!defined("SECLIBLOADED")) die("Security Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."events.class.php");
if(!defined("EVENTSLIBLOADED")) die("Events Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."menu.class.php");
if(!defined("MENULIBLOADED")) die("Menu Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."data.class.php");
if(!defined("DATALIBLOADED")) die("Data Aliases Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."media.class.php");
if(!defined("MEDIALIBLOADED")) die("Media Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."statistics.class.php");
if(!defined("STATSLIBLOADED")) die("Statistics Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."mail.class.php");
if(!defined("MAILLIBLOADED")) die("Mail Class was not loaded (or Mailer extension class not found)!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."remote-svc.class.php");
if(!defined("REMOTELIBLOADED")) die("Remote Services Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."ftp/ftp.class.php");
if(!defined("FTPLIBLOADED")) die("FTP Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."settings.class.php");
if(!defined("SETTINGSLIBLOADED")) die("Settings Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."cron.class.php");
if(!defined("CRONLIBLOADED")) die("Cron Class was not loaded!");
include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."cache.class.php");
if(!defined("CACHELIBLOADED")) die("Cache Class was not loaded!");

if(!QUICK_LOAD){
    include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."themes.class.php");
    if(!defined("THEMESLIBLOADED")) die("Themes Class was not loaded!");
    include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."plugins.class.php");
    if(!defined("PLUGINSLIBLOADED")) die("Plugins class was not loaded!");
    include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."editors.class.php");
    if(!defined("EDITORSLIBLOADED")) die("Editors class was not loaded!");
    include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."rpc.class.php");
    if(!defined("RSSLIBLOADED")) die("RSS/RDF LIB class was not loaded!");
    include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."inflector.class.php");
    if(!defined("INFLECTORLIBLOADED")) die("Inflector class was not loaded!");
    include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."taxonomies.class.php");
    if(!defined("TAXLIBLOADED")) die("Taxonomies class was not loaded!");

    if(IN_ADMIN){
        include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."admin-render.class.php");
        if(!defined("ADMRENDERLIBLOADED")) die("Admin Render Class was not loaded!");
        include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."admin-ui.class.php");
        if(!defined("ADMINUILIBLOADED")) die("Admin UI Class Library was not loaded!");
        include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."fields.class.php");
        if(!defined("FIELDSLIBLOADED")) die("Fields Class was not loaded!");
    }else{
        include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."front-render.class.php");
        if(!defined("FRTRENDERLIBLOADED")) die("Frontside Render Class was not loaded!");
        include (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."front-ui.class.php");
        if(!defined("FRONTLIBLOADED")) die("Frontside UI Class Library was not loaded!");
    }
}

define('AS_ARRAY', 1);
define('AS_STRING', 2);
define('AS_NUMBER', 3);
define('AS_OBJ', 4);

// ----------- CORE FUNCTIONS ---------------

/**
 * Checks the code (system) version.  Ensures critical files are of compatible version
 * @internal Core function
 */
function checkCodeVer(){
	$codever = str_replace('.', '', CODE_VER);
	if(strlen($codever) < 4) $codever .= "0";
    preg_match("/(.{1})(.{1})(.{2})/i", CODE_VER_CORE, $cvc);
	if(intval($codever) < CODE_VER_CORE) die($codever.': Code versioning mismatch error!  Must be version '.$cvc[1].'.'.$cvc[2].'.'.$cvc[3].' or higher.  Check CODE_VER in '.CONFIG_FOLDER.'configs.php.');
}

/**
 * Returns code (system) version
 * @internal Core function
 * @return float
 */
function getCodeVer(){
    return convertCodeVer2Dec(CODE_VER);
}

/**
 * Convert version string (x.xx.xx) to a decimal (x.xxxx)
 * @param string $ver
 * @return float
 */
function convertCodeVer2Dec($ver){
	if($ver != ''){
		$ver_p = explode(".", $ver);
		$ver_r = $ver_p[0].".".sprintf("%02d", intval(getIfSet($ver_p[1]))).sprintf("%02d", intval(getIfSet($ver_p[2])));
		return $ver_r;
	}else{
		return 0;
	}
}

/**
 * Prepare $_SESSION elements
 * @internal Core function
 */
function prepareSessionElements(){
	if (!isset($_SESSION['admlogin'])) $_SESSION['admlogin'] = false;
	if (!isset($_SESSION['root'])) $_SESSION['root'] = '';
}

// ----------- PHP EXTENSIONS ----------------

/**
 *   Reference: http://us2.php.net/manual/en/function.dl.php
 *   Author: Brendon Crawford <endofyourself |AT| yahoo>
 *   Usage: loadLocalExtension("mylib.so");
 *   Returns: Extension Name (NOT the extension filename however)
 *   NOTE:
 *       This function can be used when you need to load a PHP extension (module,shared object,etc..),
 *       but you do not have sufficient privelages to place the extension in the proper directory where it can be loaded. This function
 *       will load the extension from the CURRENT WORKING DIRECTORY only.
 *       If you need to see which functions are available within a certain extension,
 *       use "get_extension_funcs()". Documentation for this can be found at
 *       "http://us2.php.net/manual/en/function.get-extension-funcs.php".
 * @param string $extensionFile
 */

function loadLocalExtension($extensionFile) {
    //make sure that we are ABLE to load libraries
    if(!(bool)ini_get("enable_dl") || (bool)ini_get("safe_mode")) {
        trigger_error("Loading extensions is not permitted.\n");
        return false;
    }

    //check to make sure the file exists
    if(!file_exists($extensionFile)) {
        trigger_error("Extension file '$extensionFile' does not exist.\n");
        return false;
    }

    //check the file permissions
    if(!is_executable($extensionFile)) {
        trigger_error("Extension file '$extensionFile' is not executable.\n");
        return false;
    }

    //we figure out the path
    $currentDir = getcwd()."/";
    $currentExtPath = ini_get("extension_dir");
    $subDirs = preg_match_all("/\//", $currentExtPath, $matches);
    unset($matches);

    //lets make sure we extracted a valid extension path
    if(!(bool)$subDirs) {
        trigger_error("Could not determine a valid extension path [extension_dir].\n");
        return false;
    }

    $extPathLastChar = strlen($currentExtPath) - 1;

    if($extPathLastChar == strrpos($currentExtPath, "/")) {
        $subDirs--;
    }

    $backDirStr = "";
    for($i = 1; $i <= $subDirs; $i++) {
        $backDirStr .= "..";
        if($i != $subDirs) {
            $backDirStr .= "/";
        }
    }

    //construct the final path to load
    $finalExtPath = $backDirStr.$currentDir.$extensionFile;

    //now we execute dl() to actually load the module
    if(!dl($finalExtPath)) {
        return false;
    }

    //if the module was loaded correctly, we must grab the module name
    $loadedExtensions = get_loaded_extensions();
    $thisExtName = $loadedExtensions[sizeof($loadedExtensions) - 1];

    //lastly, we return the extension name
    return $thisExtName;
}

/**
 * Determine if desired extension is already loaded.  If not, then try to auto-load it locally.
 * @param string $extension
 * @return boolean
 */
function extensionLoaded($extension){
    if(!extension_loaded($extension)){
        $retn = loadLocalExtension($extension.".so");
        return ($retn !== false);
    }
    return true;
}

/**
 * Return whether or not the ImageMagick extension is loaded and ready for use.
 * @return boolean
 */
function isImageMagickLoaded(){
	return (extension_loaded('magickwand') || extension_loaded('imagick'));
}

/**
 * Return whether or not the GD extension is loaded and ready for use.
 * @return boolean
 */
function isGDLibLoaded(){
    return (extension_loaded('gd') || extension_loaded('gd2'));
}

// ----------- SUPPORT FUNCTIONS ---------------

# --- Constants and Variables

/**
 * Return constant value
 * @param string $var
 * @return mixed
 */
function getConst($var) {
	if($var != "") {
		$var = strtoupper($var);
		if(defined($var)) {
			return constant($var);
		}else{
			return "Unknown Const";
		}
	}
}

/**
 * Return array with all constants starting with prefix
 * @param string $prefix
 * @return array
 */
function getConsts($prefix = ''){
    $arry = array();
    foreach(get_defined_constants() as $key => $val){
        if(substr(strtolower($key), 0, strlen($prefix)) == strtolower($prefix)) $arry[$key] = $val;
    }
    return $arry;
}

/**
 * Swap two variable values
 * @param object $obj1
 * @param object $obj2
 * @return array
 */
function swap($obj1, $obj2){
	return array($obj2, $obj1);
}

/**
 * Search for value in multidimensional array and return key
 * @param string $needle
 * @param array $haystack
 * @param boolean $bykey    True to search for a key instead of a value (default)
 * @return mixed
 */
function multiarray_search($needle, $haystack, $bykey = false) {
    if(is_array($haystack)){
        foreach($haystack as $key => $value){
            if(is_array($value)){
                if(($rtn = multiarray_search($needle, $value, $bykey)) !== false) return $rtn;
            }elseif($value === $needle && !$bykey){
                return $key;
            }elseif($key === $needle && $bykey){
                return $value;
            }
        }
    }
    return false;
}

/**
 * An upgraded version of array_merge where intersecting keys are made unique
 * @param array $array1
 * @param array $array2
 * @param array $keysuffix
 * @return array
 */
function array_blend($array1, $array2, $keysuffix){
	if(is_array($array1) && is_array($array2)){
		foreach($array1 as $key => $value){
			if(isset($array2[$key])) {
				// make shared key in array1 unique by adding a suffix to its key
				$array2[$key.$keysuffix] = $array2[$key];
			}
		}
		$array3 = $array1 + $array2;
	}
	return $array3;
}

/**
 * Advanced version of array concatenation where arrays are tested and only concatenated if both are valid
 * @param array $array1
 * @param array $array2
 * @return array
 */
function array_concat($array1, $array2){
    if(is_array($array1) && is_array($array2)){
        return $array1 + $array2;
    }elseif(is_array($array1)){
        return $array1;
    }elseif(is_array($array2)){
        return $array2;
    }else{
        return array();
    }
}

/**
 * Returns array containing unique values from a multidimensional array
 * @param array $array
 */
function multiarray_unique($array){
	$m_array = array();
	if(is_array($array)){
		foreach($array as $elem){
			$m_array = array_merge($m_array, (array)$elem);
		}
		$m_array = array_unique($m_array);
	}
	return $m_array;
}

/**
 * Convert array(class, method) to "class::method" typically for use by trigger-handlers
 * @param mixed $array
 * @return mixed String ("class::method") or null if array count is not 2
 */
function stringifyClassMethodArray($array){
    if(is_string($array)) return $array;

    if(count($array) == 2)
        return $array[0]."::".$array[1];
    else
        return null;
}

/**
 * Return the first matching value of a search for needle array elements in the haystack
 * @param array $haystack
 * @param array $needle_array
 * @return string
 */
function getFirstMatch($haystack, $needle_array){
    $match = '';
    if(is_array($haystack) && is_array($needle_array)){
        if(count($needle_array) > 0 && count($haystack) > 0){
            foreach($needle_array as $field){
                if(isset($haystack[$field])){
                    $match = $haystack[$field];
                    break;
                }
            }
        }
    }
    return $match;
}

/**
 * Output email address with the '@' symbol converted to GD graphic
 * @param string $email Address to obfuscate
 * @param integer $fs           Font size
 * @param string $fg            Font color
 * @param boolean $no_gfx       Set to true to display email as straight text rather than GD image
 */
function maskEmail($email, $fs = 3, $fg = "000000", $no_gfx = false){
    $link = preg_replace('/([\w]+)@([\w]+)\.([a-z0-9\.]+)/i', "javascript: parse_email('$1', '$2', '$3')", $email);
    if($no_gfx)
        echo '<a href="'.$link.'">'.$email.'</a>';
    else
        echo '<a href="'.$link.'"><img src="'.WEB_URL.ADMIN_FOLDER.CORE_FOLDER.'gdfuncs.php?op=mask&st='.urlencode(base64_encode($email)).'&fs='.$fs.'&fg='.$fg.'"></a>';
}

/**
 * Validate email formatting.  This function does not determine if an email address
 * can receive email messages, only that the address is well-formed.
 * @param string $email
 */
function validateEmail($email){
    $valid = preg_match("/^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$/i", $email);
    return ($valid > 0);
}

/**
 * Return the x of 2 ^ x where 2 ^ x = num
 * @param integer $num
 * @param boolean $shiftzero    True considers 0 as the first number
 *                              Mathematically zero cannot be calculated from 2 ^ x
 */
function getBinaryPower($num, $shiftzero = false){
    $pow = 0;
    if($num > 0) $pow = log($num) / log(2);
    if($shiftzero && $num > 0) $pow += 1;
    return $pow;
}

/**
 * Convert bytes to human readable format
 * @param integer bytes Size in bytes to convert
 * @return string
 */
function formatFileSize($bytes, $precision = 2){
    $kilobyte = 1024;
    $megabyte = $kilobyte * 1024;
    $gigabyte = $megabyte * 1024;
    $terabyte = $gigabyte * 1024;
    $retn = null;

    if (($bytes >= 0) && ($bytes < $kilobyte)) {
        $retn = $bytes . ' B';
    } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
        $retn = round($bytes / $kilobyte, $precision) . ' KB';
    } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
        $retn = round($bytes / $megabyte, $precision) . ' MB';
    } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
        $retn = round($bytes / $gigabyte, $precision) . ' GB';
    } elseif ($bytes >= $terabyte) {
        $retn = round($bytes / $terabyte, $precision) . ' TB';
    } else {
        $retn = $bytes . ' B';
    }
    return $retn;
}

# --- Date/Time

/**
 * Split date string into year, month, day and place parts into GLOBALS
 * @param string $varname
 */
function tokenizeDateVar($varname){
    $datevar = $GLOBALS[$varname];
	if($datevar == "" || $datevar == "0000-00-00") $datevar = date("Y/m/d");
	$GLOBALS[$varname.'_m'] = date("m", strtotime($datevar));
	$GLOBALS[$varname.'_d'] = date("j", strtotime($datevar));
	$GLOBALS[$varname.'_y'] = date("Y", strtotime($datevar));
    $GLOBALS[$varname] = $datevar;
}

/**
 * Convert a PHP date to ISO8601 format
 * @param string $date
 * @param string $sep
 * @return string
 */
function convertPHPtoISODate($date, $sep){
    $pdate = explode($sep, $date);
    $idate = array();
    foreach($pdate as $pd){
        switch($pd){
            case 'Y':
                $idate[] = 'yy';
                break;
            case 'y':
                $idate[] = 'y';
                break;
            case 'F':
                $idate[] = 'MM';
                break;
            case 'M':
                $idate[] = 'M';
                break;
            case 'm':
                $idate[] = 'mm';
                break;
            case 'n':
                $idate[] = 'm';
                break;
            case 'd':
                $idate[] = 'dd';
                break;
            case 'j':
                $idate[] = 'd';
                break;
        }
    }
    return join($sep, $idate);
}

/**
 * Return whether value is a valid date
 * @param str $date
 * @return boolean
 */
function isDate($date){
    $date = trim($date);
    return (strtotime($date) !== false);
}

/**
 * Return whether or not parameter is a valid date
 * @param string $date
 * @param string $format
 * @return boolean
 */
function validateDate($date, $format = ""){
    global $_error;

    if($format == "") $format = DATE_FORMAT;
    switch(strtoupper($format)){
        case 'YYYY/MM/DD':
        case 'YYYY-MM-DD':
            preg_match('/([\d]{4})[-.\/]([\d]{2})[-.\/]([\d]{2})/', $date, $matches);
            $y = getIfSet($matches[1]);
            $m = getIfSet($matches[2]);
            $d = getIfSet($matches[3]);
            break;
        case 'YYYY/DD/MM':
        case 'YYYY-DD-MM':
            preg_match('/([\d]{4})[-.\/]([\d]{2})[-.\/]([\d]{2})/', $date, $matches);
            $y = getIfSet($matches[1]);
            $d = getIfSet($matches[2]);
            $m = getIfSet($matches[3]);
            break;
        case 'DD-MM-YYYY':
        case 'DD/MM/YYYY':
            preg_match('/([\d]{2})[-.\/]([\d]{2})[-.\/]([\d]{4})/', $date, $matches);
            $d = getIfSet($matches[1]);
            $m = getIfSet($matches[2]);
            $y = getIfSet($matches[1]);
            break;
        case 'MM-DD-YYYY':
        case 'MM/DD/YYYY':
            preg_match('/([\d]{2})[-.\/]([\d]{2})[-.\/]([\d]{4})/', $date, $matches);
            $m = getIfSet($matches[1]);
            $d = getIfSet($matches[2]);
            $y = getIfSet($matches[1]);
            break;
        case 'YYYYMMDD':
            $y = substr($date, 0, 4);
            $m = substr($date, 4, 2);
            $d = substr($date, 6, 2);
            break;
        case 'YYYYDDMM':
            $y = substr($date, 0, 4);
            $d = substr($date, 4, 2);
            $m = substr($date, 6, 2);
            break;
        default:
            $_error->addErrorMsg("Invalid Date Format '$format'");
    }
    return ((checkdate($m, $d, $y)) ? date(PHP_DATE_FORMAT, mktime(0, 0, 0, $m, $d, $y)) : null);
}

/**
 * Return the difference between two times as periods (x hours, y minutes, z seconds)
 * @param mixed $time1                  First time
 * @param mixed $time2                  Time to compare
 * @param string $format                long (seconds), med (secs), short (s)
 * @return string
 */
function getTimeDiff($time1, $time2, $format = 'med'){
    if(!is_numeric($time1)) $time1 = strtotime($time1);
    if(!is_numeric($time2)) $time2 = strtotime($time2);
    $format = trim(strtolower($format));
    // Elapsed time
    $etime = abs($time2 - $time1);

    // Adjectives
    if($time1 < $time2){
        // future diff
        if($time1 - time() < 60){
            $adj = " from now";
        }else{
            $adj = " later";
        }
    }else{
        // past diff
        if($time2 - time() < 60){
            $adj = " ago";
        }else{
            $adj = " past";
        }
    }

    // If no elapsed time, return 0
    if ($etime < 1){
        return 'now';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
    );

    if($format == 'short'){
        $a_single = array( 'year'   => 'y',
                           'month'  => 'm',
                           'day'    => 'd',
                           'hour'   => 'h',
                           'minute' => 'mn',
                           'second' => 's'
        );
        $a_plural = $a_single;
    }elseif($format == 'med'){
        $a_single = array( 'year'   => 'yr',
                           'month'  => 'mth',
                           'day'    => 'day',
                           'hour'   => 'hr',
                           'minute' => 'min',
                           'second' => 'sec'
        );
        $a_plural = array( 'year'   => 'yrs',
                           'month'  => 'mths',
                           'day'    => 'days',
                           'hour'   => 'hrs',
                           'minute' => 'mins',
                           'second' => 'secs'
        );
    }else{
        $a_single = $a;
        $a_plural = array( 'year'   => 'years',
                           'month'  => 'months',
                           'day'    => 'days',
                           'hour'   => 'hours',
                           'minute' => 'minutes',
                           'second' => 'seconds'
        );
    }

    $estring = null;
    foreach ($a as $secs => $str){
        // Divide elapsed time by seconds
        $d = $etime / $secs;
        if ($d >= 1){
            // Round to the next lowest integer
            $r = floor($d);
            // Calculate time to remove from elapsed time
            $rtime = $r * $secs;
            // Recalculate and store elapsed time for next loop
            if(($etime - $rtime)  < 0){
                $etime -= ($r - 1) * $secs;
            } else {
                $etime -= $rtime;
            }
            // Create string to return
            if($r > 0)
                $estring = $estring . $r . ' ' . ($r > 1 ? $a_plural[$str] : $a_single[$str]) . ' ';
        }
    }
    return $estring . $adj;
}

/**
 * Return array of timezones
 * @return array
 */
function getTimezones(){
    $tz_array = array(
        "Africa/Abidjan" => "Abidjan", "Africa/Accra" => "Accra", "Africa/Addis_Ababa" => "Addis Ababa",
        "Africa/Algiers" => "Algiers", "Africa/Asmara" => "Asmara", "Africa/Bamako" => "Bamako",
        "Africa/Bangui" => "Bangui", "Africa/Banjul" => "Banjul", "Africa/Bissau" => "Bissau",
        "Africa/Blantyre" => "Blantyre", "Africa/Brazzaville" => "Brazzaville", "Africa/Bujumbura" => "Bujumbura",
        "Africa/Cairo" => "Cairo", "Africa/Casablanca" => "Casablanca", "Africa/Ceuta" => "Ceuta",
        "Africa/Conakry" => "Conakry", "Africa/Dakar" => "Dakar", "Africa/Dar_es_Salaam" => "Dar es Salaam",
        "Africa/Djibouti" => "Djibouti", "Africa/Douala" => "Douala", "Africa/El_Aaiun" => "El Aaiun",
        "Africa/Freetown" => "Freetown", "Africa/Gaborone" => "Gaborone", "Africa/Harare" => "Harare",
        "Africa/Johannesburg" => "Johannesburg", "Africa/Kampala" => "Kampala", "Africa/Khartoum" => "Khartoum",
        "Africa/Kigali" => "Kigali", "Africa/Kinshasa" => "Kinshasa", "Africa/Lagos" => "Lagos",
        "Africa/Libreville" => "Libreville", "Africa/Lome" => "Lome", "Africa/Luanda" => "Luanda",
        "Africa/Lubumbashi" => "Lubumbashi", "Africa/Lusaka" => "Lusaka", "Africa/Malabo" => "Malabo",
        "Africa/Maputo" => "Maputo", "Africa/Maseru" => "Maseru", "Africa/Mbabane" => "Mbabane",
        "Africa/Mogadishu" => "Mogadishu", "Africa/Monrovia" => "Monrovia", "Africa/Nairobi" => "Nairobi",
        "Africa/Ndjamena" => "Ndjamena", "Africa/Niamey" => "Niamey", "Africa/Nouakchott" => "Nouakchott",
        "Africa/Ouagadougou" => "Ouagadougou", "Africa/Porto-Novo" => "Porto-Novo", "Africa/Sao_Tome" => "Sao Tome",
        "Africa/Tripoli" => "Tripoli", "Africa/Tunis" => "Tunis", "Africa/Windhoek" => "Windhoek",
        "America/Adak" => "Adak", "America/Anchorage" => "Anchorage", "America/Anguilla" => "Anguilla",
        "America/Antigua" => "Antigua", "America/Araguaina" => "Araguaina", "America/Argentina/Buenos_Aires" => "Argentina - Buenos Aires",
        "America/Argentina/Catamarca" => "Argentina - Catamarca", "America/Argentina/Cordoba" => "Argentina - Cordoba",
        "America/Argentina/Jujuy" => "Argentina - Jujuy", "America/Argentina/La_Rioja" => "Argentina - La Rioja",
        "America/Argentina/Mendoza" => "Argentina - Mendoza", "America/Argentina/Rio_Gallegos" => "Argentina - Rio Gallegos",
        "America/Argentina/Salta" => "Argentina - Salta", "America/Argentina/San_Juan" => "Argentina - San Juan",
        "America/Argentina/San_Luis" => "Argentina - San Luis", "America/Argentina/Tucuman" => "Argentina - Tucuman",
        "America/Argentina/Ushuaia" => "Argentina - Ushuaia", "America/Aruba" => "Aruba",
        "America/Asuncion" => "Asuncion", "America/Atikokan" => "Atikokan", "America/Bahia" => "Bahia",
        "America/Barbados" => "Barbados", "America/Belem" => "Belem", "America/Belize" => "Belize",
        "America/Blanc-Sablon" => "Blanc-Sablon", "America/Boa_Vista" => "Boa Vista",
        "America/Bogota" => "Bogota", "America/Boise" => "Boise", "America/Cambridge_Bay" => "Cambridge Bay",
        "America/Campo_Grande" => "Campo Grande", "America/Cancun" => "Cancun", "America/Caracas" => "Caracas",
        "America/Cayenne" => "Cayenne", "America/Cayman" => "Cayman", "America/Chicago" => "Chicago",
        "America/Chihuahua" => "Chihuahua", "America/Costa_Rica" => "Costa Rica", "America/Cuiaba" => "Cuiaba",
        "America/Curacao" => "Curacao", "America/Danmarkshavn" => "Danmarkshavn", "America/Dawson" => "Dawson",
        "America/Dawson_Creek" => "Dawson Creek", "America/Denver" => "Denver", "America/Detroit" => "Detroit",
        "America/Dominica" => "Dominica", "America/Edmonton" => "Edmonton", "America/Eirunepe" => "Eirunepe",
        "America/El_Salvador" => "El Salvador", "America/Fortaleza" => "Fortaleza", "America/Glace_Bay" => "Glace Bay",
        "America/Godthab" => "Godthab", "America/Goose_Bay" => "Goose Bay", "America/Grand_Turk" => "Grand Turk",
        "America/Grenada" => "Grenada", "America/Guadeloupe" => "Guadeloupe", "America/Guatemala" => "Guatemala",
        "America/Guayaquil" => "Guayaquil", "America/Guyana" => "Guyana", "America/Halifax" => "Halifax",
        "America/Havana" => "Havana", "America/Hermosillo" => "Hermosillo", "America/Indiana/Indianapolis" => "Indiana - Indianapolis",
        "America/Indiana/Knox" => "Indiana - Knox", "America/Indiana/Marengo" => "Indiana - Marengo",
        "America/Indiana/Petersburg" => "Indiana - Petersburg", "America/Indiana/Tell_City" => "Indiana - Tell City",
        "America/Indiana/Vevay" => "Indiana - Vevay", "America/Indiana/Vincennes" => "Indiana - Vincennes",
        "America/Indiana/Winamac" => "Indiana - Winamac", "America/Inuvik" => "Inuvik",
        "America/Iqaluit" => "Iqaluit", "America/Jamaica" => "Jamaica", "America/Juneau" => "Juneau",
        "America/Kentucky/Louisville" => "Kentucky - Louisville", "America/Kentucky/Monticello" => "Kentucky - Monticello",
        "America/La_Paz" => "La Paz", "America/Lima" => "Lima", "America/Los_Angeles" => "Los Angeles",
        "America/Maceio" => "Maceio", "America/Managua" => "Managua", "America/Manaus" => "Manaus",
        "America/Marigot" => "Marigot", "America/Martinique" => "Martinique", "America/Mazatlan" => "Mazatlan",
        "America/Menominee" => "Menominee", "America/Merida" => "Merida", "America/Mexico_City" => "Mexico City",
        "America/Miquelon" => "Miquelon", "America/Moncton" => "Moncton", "America/Monterrey" => "Monterrey",
        "America/Montevideo" => "Montevideo", "America/Montreal" => "Montreal", "America/Montserrat" => "Montserrat",
        "America/Nassau" => "Nassau", "America/New_York" => "New York", "America/Nipigon" => "Nipigon",
        "America/Nome" => "Nome", "America/Noronha" => "Noronha", "America/North_Dakota/Center" => "North Dakota - Center",
        "America/North_Dakota/New_Salem" => "North Dakota - New Salem", "America/Panama" => "Panama",
        "America/Pangnirtung" => "Pangnirtung", "America/Paramaribo" => "Paramaribo", "America/Phoenix" => "Phoenix",
        "America/Port-au-Prince" => "Port-au-Prince", "America/Port_of_Spain" => "Port of Spain",
        "America/Porto_Velho" => "Porto Velho", "America/Puerto_Rico" => "Puerto Rico",
        "America/Rainy_River" => "Rainy River", "America/Rankin_Inlet" => "Rankin Inlet",
        "America/Recife" => "Recife", "America/Regina" => "Regina", "America/Resolute" => "Resolute",
        "America/Rio_Branco" => "Rio Branco", "America/Santarem" => "Santarem", "America/Santiago" => "Santiago",
        "America/Santo_Domingo" => "Santo Domingo", "America/Sao_Paulo" => "Sao Paulo",
        "America/Scoresbysund" => "Scoresbysund", "America/Shiprock" => "Shiprock",
        "America/St_Barthelemy" => "St Barthelemy", "America/St_Johns" => "St Johns",
        "America/St_Kitts" => "St Kitts", "America/St_Lucia" => "St Lucia",
        "America/St_Thomas" => "St Thomas", "America/St_Vincent" => "St Vincent",
        "America/Swift_Current" => "Swift Current", "America/Tegucigalpa" => "Tegucigalpa",
        "America/Thule" => "Thule", "America/Thunder_Bay" => "Thunder Bay",
        "America/Tijuana" => "Tijuana", "America/Toronto" => "Toronto", "America/Tortola" => "Tortola",
        "America/Vancouver" => "Vancouver", "America/Whitehorse" => "Whitehorse",
        "America/Winnipeg" => "Winnipeg", "America/Yakutat" => "Yakutat",
        "America/Yellowknife" => "Yellowknife", "Antarctica/Casey" => "Casey",
        "Antarctica/Davis" => "Davis", "Antarctica/DumontDUrville" => "DumontDUrville",
        "Antarctica/Mawson" => "Mawson", "Antarctica/McMurdo" => "McMurdo",
        "Antarctica/Palmer" => "Palmer", "Antarctica/Rothera" => "Rothera", "Antarctica/South_Pole" => "South Pole",
        "Antarctica/Syowa" => "Syowa", "Antarctica/Vostok" => "Vostok", "Arctic/Longyearbyen" => "Longyearbyen",
        "Asia/Aden" => "Aden", "Asia/Almaty" => "Almaty", "Asia/Amman" => "Amman",
        "Asia/Anadyr" => "Anadyr", "Asia/Aqtau" => "Aqtau", "Asia/Aqtobe" => "Aqtobe",
        "Asia/Ashgabat" => "Ashgabat", "Asia/Baghdad" => "Baghdad", "Asia/Bahrain" => "Bahrain",
        "Asia/Baku" => "Baku", "Asia/Bangkok" => "Bangkok", "Asia/Beirut" => "Beirut",
        "Asia/Bishkek" => "Bishkek", "Asia/Brunei" => "Brunei", "Asia/Choibalsan" => "Choibalsan",
        "Asia/Chongqing" => "Chongqing", "Asia/Colombo" => "Colombo", "Asia/Damascus" => "Damascus",
        "Asia/Dhaka" => "Dhaka", "Asia/Dili" => "Dili", "Asia/Dubai" => "Dubai",
        "Asia/Dushanbe" => "Dushanbe", "Asia/Gaza" => "Gaza", "Asia/Harbin" => "Harbin",
        "Asia/Ho_Chi_Minh" => "Ho Chi Minh", "Asia/Hong_Kong" => "Hong Kong", "Asia/Hovd" => "Hovd",
        "Asia/Irkutsk" => "Irkutsk", "Asia/Jakarta" => "Jakarta", "Asia/Jayapura" => "Jayapura",
        "Asia/Jerusalem" => "Jerusalem", "Asia/Kabul" => "Kabul", "Asia/Kamchatka" => "Kamchatka",
        "Asia/Karachi" => "Karachi", "Asia/Kashgar" => "Kashgar", "Asia/Kathmandu" => "Kathmandu",
        "Asia/Kolkata" => "Kolkata", "Asia/Krasnoyarsk" => "Krasnoyarsk", "Asia/Kuala_Lumpur" => "Kuala Lumpur",
        "Asia/Kuching" => "Kuching", "Asia/Kuwait" => "Kuwait", "Asia/Macau" => "Macau",
        "Asia/Magadan" => "Magadan", "Asia/Makassar" => "Makassar", "Asia/Manila" => "Manila",
        "Asia/Muscat" => "Muscat", "Asia/Nicosia" => "Nicosia", "Asia/Novosibirsk" => "Novosibirsk",
        "Asia/Omsk" => "Omsk", "Asia/Oral" => "Oral", "Asia/Phnom_Penh" => "Phnom Penh",
        "Asia/Pontianak" => "Pontianak", "Asia/Pyongyang" => "Pyongyang", "Asia/Qatar" => "Qatar",
        "Asia/Qyzylorda" => "Qyzylorda", "Asia/Rangoon" => "Rangoon", "Asia/Riyadh" => "Riyadh",
        "Asia/Sakhalin" => "Sakhalin", "Asia/Samarkand" => "Samarkand", "Asia/Seoul" => "Seoul",
        "Asia/Shanghai" => "Shanghai", "Asia/Singapore" => "Singapore", "Asia/Taipei" => "Taipei",
        "Asia/Tashkent" => "Tashkent", "Asia/Tbilisi" => "Tbilisi", "Asia/Tehran" => "Tehran",
        "Asia/Thimphu" => "Thimphu", "Asia/Tokyo" => "Tokyo", "Asia/Ulaanbaatar" => "Ulaanbaatar",
        "Asia/Urumqi" => "Urumqi", "Asia/Vientiane" => "Vientiane", "Asia/Vladivostok" => "Vladivostok",
        "Asia/Yakutsk" => "Yakutsk", "Asia/Yekaterinburg" => "Yekaterinburg",
        "Asia/Yerevan" => "Yerevan", "Atlantic/Azores" => "Azores", "Atlantic/Bermuda" => "Bermuda",
        "Atlantic/Canary" => "Canary", "Atlantic/Cape_Verde" => "Cape Verde",
        "Atlantic/Faroe" => "Faroe", "Atlantic/Madeira" => "Madeira", "Atlantic/Reykjavik" => "Reykjavik",
        "Atlantic/South_Georgia" => "South Georgia", "Atlantic/Stanley" => "Stanley",
        "Atlantic/St_Helena" => "St Helena", "Australia/Adelaide" => "Adelaide",
        "Australia/Brisbane" => "Brisbane", "Australia/Broken_Hill" => "Broken Hill",
        "Australia/Currie" => "Currie", "Australia/Darwin" => "Darwin",
        "Australia/Eucla" => "Eucla", "Australia/Hobart" => "Hobart", "Australia/Lindeman" => "Lindeman",
        "Australia/Lord_Howe" => "Lord Howe", "Australia/Melbourne" => "Melbourne",
        "Australia/Perth" => "Perth", "Australia/Sydney" => "Sydney", "Europe/Amsterdam" => "Amsterdam",
        "Europe/Andorra" => "Andorra", "Europe/Athens" => "Athens", "Europe/Belgrade" => "Belgrade",
        "Europe/Berlin" => "Berlin", "Europe/Bratislava" => "Bratislava", "Europe/Brussels" => "Brussels",
        "Europe/Bucharest" => "Bucharest", "Europe/Budapest" => "Budapest", "Europe/Chisinau" => "Chisinau",
        "Europe/Copenhagen" => "Copenhagen", "Europe/Dublin" => "Dublin", "Europe/Gibraltar" => "Gibraltar",
        "Europe/Guernsey" => "Guernsey", "Europe/Helsinki" => "Helsinki", "Europe/Isle_of_Man" => "Isle of Man",
        "Europe/Istanbul" => "Istanbul", "Europe/Jersey" => "Jersey", "Europe/Kaliningrad" => "Kaliningrad",
        "Europe/Kiev" => "Kiev", "Europe/Lisbon" => "Lisbon", "Europe/Ljubljana" => "Ljubljana",
        "Europe/London" => "London", "Europe/Luxembourg" => "Luxembourg", "Europe/Madrid" => "Madrid",
        "Europe/Malta" => "Malta", "Europe/Mariehamn" => "Mariehamn", "Europe/Minsk" => "Minsk",
        "Europe/Monaco" => "Monaco", "Europe/Moscow" => "Moscow", "Europe/Oslo" => "Oslo",
        "Europe/Paris" => "Paris", "Europe/Podgorica" => "Podgorica", "Europe/Prague" => "Prague",
        "Europe/Riga" => "Riga", "Europe/Rome" => "Rome", "Europe/Samara" => "Samara",
        "Europe/San_Marino" => "San Marino", "Europe/Sarajevo" => "Sarajevo", "Europe/Simferopol" => "Simferopol",
        "Europe/Skopje" => "Skopje", "Europe/Sofia" => "Sofia", "Europe/Stockholm" => "Stockholm",
        "Europe/Tallinn" => "Tallinn", "Europe/Tirane" => "Tirane", "Europe/Uzhgorod" => "Uzhgorod",
        "Europe/Vaduz" => "Vaduz", "Europe/Vatican" => "Vatican", "Europe/Vienna" => "Vienna",
        "Europe/Vilnius" => "Vilnius", "Europe/Volgograd" => "Volgograd", "Europe/Warsaw" => "Warsaw",
        "Europe/Zagreb" => "Zagreb", "Europe/Zaporozhye" => "Zaporozhye", "Europe/Zurich" => "Zurich",
        "Indian/Antananarivo" => "Antananarivo", "Indian/Chagos" => "Chagos", "Indian/Christmas" => "Christmas",
        "Indian/Cocos" => "Cocos", "Indian/Comoro" => "Comoro", "Indian/Kerguelen" => "Kerguelen",
        "Indian/Mahe" => "Mahe", "Indian/Maldives" => "Maldives", "Indian/Mauritius" => "Mauritius",
        "Indian/Mayotte" => "Mayotte", "Indian/Reunion" => "Reunion", "Pacific/Apia" => "Apia",
        "Pacific/Auckland" => "Auckland", "Pacific/Chatham" => "Chatham", "Pacific/Easter" => "Easter",
        "Pacific/Efate" => "Efate", "Pacific/Enderbury" => "Enderbury", "Pacific/Fakaofo" => "Fakaofo",
        "Pacific/Fiji" => "Fiji", "Pacific/Funafuti" => "Funafuti", "Pacific/Galapagos" => "Galapagos",
        "Pacific/Gambier" => "Gambier", "Pacific/Guadalcanal" => "Guadalcanal", "Pacific/Guam" => "Guam",
        "Pacific/Honolulu" => "Honolulu", "Pacific/Johnston" => "Johnston", "Pacific/Kiritimati" => "Kiritimati",
        "Pacific/Kosrae" => "Kosrae", "Pacific/Kwajalein" => "Kwajalein", "Pacific/Majuro" => "Majuro",
        "Pacific/Marquesas" => "Marquesas", "Pacific/Midway" => "Midway", "Pacific/Nauru" => "Nauru",
        "Pacific/Niue" => "Niue", "Pacific/Norfolk" => "Norfolk", "Pacific/Noumea" => "Noumea",
        "Pacific/Pago_Pago" => "Pago Pago", "Pacific/Palau" => "Palau", "Pacific/Pitcairn" => "Pitcairn",
        "Pacific/Ponape" => "Ponape", "Pacific/Port_Moresby" => "Port Moresby", "Pacific/Rarotonga" => "Rarotonga",
        "Pacific/Saipan" => "Saipan", "Pacific/Tahiti" => "Tahiti", "Pacific/Tarawa" => "Tarawa",
        "Pacific/Tongatapu" => "Tongatapu", "Pacific/Truk" => "Truk", "Pacific/Wake" => "Wake",
        "Pacific/Wallis" => "Wallis", "UTC" => "UTC",
        "Offset/UTC-12" => "UTC-12", "Offset/UTC-11.5" => "UTC-11:30",
        "Offset/UTC-11" => "UTC-11", "Offset/UTC-10.5" => "UTC-10:30",
        "Offset/UTC-10" => "UTC-10", "Offset/UTC-9.5" => "UTC-9:30",
        "Offset/UTC-9" => "UTC-9", "Offset/UTC-8.5" => "UTC-8:30",
        "Offset/UTC-8" => "UTC-8", "Offset/UTC-7.5" => "UTC-7:30",
        "Offset/UTC-7" => "UTC-7", "Offset/UTC-6.5" => "UTC-6:30",
        "Offset/UTC-6" => "UTC-6", "Offset/UTC-5.5" => "UTC-5:30",
        "Offset/UTC-5" => "UTC-5", "Offset/UTC-4.5" => "UTC-4:30",
        "Offset/UTC-4" => "UTC-4", "Offset/UTC-3.5" => "UTC-3:30",
        "Offset/UTC-3" => "UTC-3", "Offset/UTC-2.5" => "UTC-2:30",
        "Offset/UTC-2" => "UTC-2", "Offset/UTC-1.5" => "UTC-1:30",
        "Offset/UTC-1" => "UTC-1", "Offset/UTC-0.5" => "UTC-0:30",
        "Offset/UTC+0" => "UTC+0", "Offset/UTC+0.5" => "UTC+0:30",
        "Offset/UTC+1" => "UTC+1", "Offset/UTC+1.5" => "UTC+1:30",
        "Offset/UTC+2" => "UTC+2", "Offset/UTC+2.5" => "UTC+2:30",
        "Offset/UTC+3" => "UTC+3", "Offset/UTC+3.5" => "UTC+3:30",
        "Offset/UTC+4" => "UTC+4", "Offset/UTC+4.5" => "UTC+4:30",
        "Offset/UTC+5" => "UTC+5", "Offset/UTC+5.5" => "UTC+5:30",
        "Offset/UTC+6" => "UTC+6", "Offset/UTC+6.5" => "UTC+6:30",
        "Offset/UTC+7" => "UTC+7", "Offset/UTC+7.5" => "UTC+7:30",
        "Offset/UTC+8" => "UTC+8", "Offset/UTC+8.5" => "UTC+8:30",
        "Offset/UTC+9" => "UTC+9", "Offset/UTC+9.5" => "UTC+9:30",
        "Offset/UTC+10" => "UTC+10", "Offset/UTC+10.5" => "UTC+10:30",
        "Offset/UTC+11" => "UTC+11", "Offset/UTC+11.5" => "UTC+11:30",
        "Offset/UTC+12" => "UTC+12", "Offset/UTC+12.5" => "UTC+12:30",
        "Offset/UTC+13" => "UTC+13", "Offset/UTC+13.5" => "UTC+13:20",
        "Offset/UTC+14" => "UTC+14"
        );
    return $tz_array;
}

/**
 * Return array of cities with timezone attributes
 * @return array
 */
function getCitiesWithTimezones(){
	$timezones = DateTimeZone::listAbbreviations();

	$cities = array();
	foreach($timezones as $key => $zones){
	    foreach($zones as $id => $zone){
	        /**
	         * Only get timezones explicitely not part of "Others".
	         * @see http://www.php.net/manual/en/timezones.others.php
	         */
	        if (preg_match( '/^(America|Antartica|Arctic|Asia|Atlantic|Europe|Indian|Pacific)\//', $zone['timezone_id']))
	            $cities[$zone['timezone_id']][] = $key;
	    }
	}

	// For each city, have a comma separated list of all possible timezones for that city.
	foreach($cities as $key => $value)
	    $cities[$key] = join(', ', $value);

	// Only keep one city (the first and also most important) for each set of possibilities.
	$cities = array_unique($cities);

	// Sort by area/city name.
	ksort($cities);

	return $cities;
}

# --- Slugs

/**
 * Return a sanitized, URL-safe version of input string (code)
 * @param string $str
 * @return string
 */
function codify($str, $table = '', $code_field = 'code'){
    global $_db_control;

	$code = str_replace(" ", "-", $str);
	$code = preg_replace("/(\*|\?|\&|\+|'|`|\%|=|\||\.|\<|\>|:|\/|\\|\"|\#039;|\#39;|\#034;|\#34;|\#)/", "_", strtolower($code));

    if($table != '' && $code_field != ''){
        $code_array = $_db_control->setTable($table)->setFields("code")->getRec();
        if(count($code_array) > 0){
            $code_array = $_db_control->flattenDBArray($code_array, "", "code");
            if(in_array($code, $code_array)){
                // code already exists. to make unique, add the next index to it
                $indx = 1;
                while(in_array($code."_".$indx, $code_array)){
                    $indx++;
                }
                $code .= "_".$indx;
            }
        }
    }
	return $code;
}

/**
 * Convert string to slug (a slug is different from a code in that it can only contain alphanumeric chars, hyphen and underscore)
 * @param string $str
 * @return string
 */
function slugify($str){
    $str = strtolower(preg_replace("/[^a-z0-9\-_]/i", "", $str));
    return $str;
}

/**
 * Generate a GUID
 * @param string $val
 * @param boolean $static
 * @return string
 */
function guid($val = null, $static = false){
    if (function_exists('com_create_guid') && !$static){
        return com_create_guid();
    }else{
        if(!empty($val) && $static){
            $charid = strtoupper(md5($val));
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
        }
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12)
                .chr(125);// "}"
        return $uuid;
    }
}

/**
 * Generate a unique key by appending an incremented index
 * @param string $key
 * @param array $key_array
 * @return string
 */
function getUniqueKey($key, $key_array){
    if(is_array($key_array) && !empty($key_array)){
        $indx = "";
        while(in_array($key.$indx, $key_array)){
            $indx = intval($indx) + 1;
        }
        $key .= $indx;
    }
    return $key;
}
?>