/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 *
 * CKEDITOR CONFIGURATION FILE
 *
 * Foundry script
 */
jQuery(document).ready(function($){
    $.foundry.setupeditorparams('ckeditor');
    if($.foundry.format_tags == undefined) $.foundry.format_tags = 'p;h1;h2;h3;h4;h5;h6;pre;address;div';
    if($.foundry.colors == undefined) $.foundry.colors = 'FFFFFF,666666,000000';

    /**
     * Normal CKEditor script
     */

    CKEDITOR.editorConfig = function( config ) {
        if($.foundry.browseurl.split("?", 1));
        if($.foundry.uploadurl.split("?", 1));
        config.filebrowserBrowseUrl = $.foundry.browseurl+'?type=Files';
        config.filebrowserImageBrowseUrl = $.foundry.browseurl+'?type=Images';
        config.filebrowserFlashBrowseUrl = $.foundry.browseurl+'?type=Flash';
        config.filebrowserUploadUrl = $.foundry.uploadurl+'?type=Files';
        config.filebrowserImageUploadUrl = $.foundry.uploadurl+'?type=Images';
        config.filebrowserFlashUploadUrl = $.foundry.uploadurl+'?type=Flash';
        config.width = $.foundry.width+'px';
        config.resize_maxWidth = $.foundry.width;
        config.resize_minWidth = $.foundry.width;
        config.resize_maxHeight = $.foundry.height;
        config.startupFocus = false;
        config.contentsCss = $.foundry.css;
        config.colorButton_colors = $.foundry.colors;
        config.format_tags = $.foundry.format_tags;        // default is p;h1;h2;h3;h4;h5;h6;pre;address;div
        config.enterMode = CKEDITOR.ENTER_BR;
        config.scayt_autoStartup = ($.foundry.scayt == 1);

    	// Define changes to default configuration here.
    	// For complete reference see:
    	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

    	// The toolbar groups arrangement, optimized for two toolbar rows.
    	config.toolbarGroups = [
    		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
    		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
    		{ name: 'links' },
    		{ name: 'insert' },
    		{ name: 'forms' },
    		{ name: 'tools' },
    		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
    		{ name: 'others' },
    		'/',
            { name: 'styles' },
    		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
    		{ name: 'colors' },
    		{ name: 'about' }
    	];

    	// Remove some buttons provided by the standard plugins, which are
    	// not needed in the Standard(s) toolbar.
    	//config.removeButtons = 'Underline,Subscript,Superscript';

    	// Simplify the dialog windows.
    	config.removeDialogTabs = 'image:advanced;link:advanced';
    };
});