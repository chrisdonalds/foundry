<?php
/*
CKEDITOR FOUNDRY COMPANION
Web Template 4.0
Chris Donalds <cdonalds01@gmail.com>
========================================
*/

function ckeditor_settings($action = null, $data = ''){
    global $_plugins;
    
    $folder = $_plugins->getPluginFolder();
    $incl = $_plugins->getPluginVerb($folder, 'folder');
    if($action == null){
        // return dialog HTML
        $settings = json_decode($_plugins->getPluginCustomSetting($incl, PLUGIN_SETTINGS_SAVETOSTD), true);
        $html = '
        <style>
            #pluginsettingsform { padding: 10px 0; }
            #pluginsettingsform label { display: inline-block; width: 130px; font-weight: normal; float: left; }
            #pluginsettingsform div { margin-bottom: 3px; overflow: hidden; }
            #ckeditor_browseurl, #ckeditor_uploadurl, #ckeditor_css, #ckeditor_tags { width: 400px; }
            #ckeditor_width, #ckeditor_height {}
        </style>
        <script type="text/Javascript">
            $jq(document).ready(function($){
                $("#pluginsettingsform input, #pluginsettingsform textarea").click(function(){
                    $("#pluginsettingsform .clickvar_target").removeClass("clickvar_target");
                    $(this).addClass("clickvar_target");
                });
            });
        </script>
        <p>These settings affect the CKEditor interface and interactions with other related plugins.</p>
        <div><label>Browse URL:</label><input type="text" name="browseurl" id="ckeditor_browseurl" class="clickvar_target" value="'.getIfSet($settings['browseurl'], WEB_URL.ADMIN_FOLDER.EDITOR_FOLDER.'ckfinder/ckfinder.html').'" /></div>
        <div><label>Upload URL:</label><input type="text" name="uploadurl" id="ckeditor_uploadurl" value="'.getIfSet($settings['uploadurl'], WEB_URL.ADMIN_FOLDER.EDITOR_FOLDER.'ckfinder/core/connector/php/connector.php').'" /><p class="settingsdescr">The `type=Files...` part is added automatically.</p></div>
        <div><label>Editor Size:</label><input type="text" name="width" id="ckeditor_width" value="'.getIfSet($settings['width'], 750).'" size="10" /> px x <input type="text" name="height" id="ckeditor_height" value="'.getIfSet($settings['height'], 520).'" size="10" /> px (W x H)</div>
        <div><label>Custom CSS Path:</label><input type="text" name="css" id="ckeditor_css" value="'.getIfSet($settings['css']).'" /></div>
        <div><label>Allowed Tags:</label><textarea name="format_tags" id="ckeditor_tags" rows="3" cols="30">'.getIfSet($settings['format_tags'], 'p;h1;h2;h3;h4;h5;h6;pre;address;div').'</textarea></div>
        <div><label>Spell Check as You Type (SCAYT):</label><input type="checkbox" name="scayt" id="ckeditor_scayt" value="1"'.((getIfSet($settings['scayt']) == 1) ? ' checked="checked"' : '').' /> Active</div>
        '.showClickableVariables(array(VL_FILEPATHS, "Editor URL"), array("Editor URL" => WEB_URL.ADMIN_FOLDER.EDITOR_FOLDER, "CKFinder Browse URL" => "ckfinder/ckfinder.html", "CKFinder Upload URL" => "ckfinder/core/connector/php/connector.php"));
        return $_plugins->pluginSettingsDialogContents("CKEditor", $html, __FUNCTION__);
    }elseif($action == PLUGIN_SETTINGS_SAVE){
        // save data to database
        parse_str($data, $arry);
        $result = $_plugins->savePluginCustomSettings($incl, json_encode($arry), PLUGIN_SETTINGS_SAVETOSTD);
        return $_plugins->pluginSettingsDialogButtonPressed(null, true);
    }elseif($action == PLUGIN_SETTINGS_CLOSE){
        // closed
        return $_plugins->pluginSettingsDialogButtonPressed(null, false);
    }
}

/**
 * Initiate the script loading process
 */
function ckeditor_load(){
    global $_plugins;

    $folder = $_plugins->getPluginFolder();
    $_plugins->addScriptSourceLine(
        'script',
        WEB_URL.$folder,
        "ckeditor.js",
        "",
        "",
        "",
        true,
        false,
        null,
        true
    );
}

/**
 * Output CKEditor-specific HTML script
 * @param array $atts       Field parameters presented in array format
 * @uses string $id                 ID of the textarea
 * @uses string $toolbar            Name of the toolbar
 * @uses string $ht                 Height (optional)
 * @uses string $wd                 Width (optional)
 * @uses string $maxlentext         Maximum length (optional)
 */
function ckeditor_render($atts, $id, $ht = "", $wd = "", $maxlen = 0){
    global $_users;
    
    $maxlentext = (($maxlen > 0) ? " var cktimer = setInterval(\"countChars(CKEDITOR.instances.{$id}, '{$id}count', false, {$maxlen})\", 500);" : "");
    $toolbar = getIfSet($atts['toolbar'], "FormatOnly");
    if($_users->userIsAtleast(ADMLEVEL_SYSADMIN) && strpos($toolbar, "WithSource") === false) $toolbar .= "WithSource";
    $toolbar = "'{$toolbar}'".(($ht != '' || $wd != '') ? ','.PHP_EOL : '');

    print <<<EOT
<script type="text/javascript">
    //<![CDATA[
        CKEDITOR.replace('{$id}', {
            toolbar : {$toolbar}{$ht}{$wd}
        });
    //]]>
    {$maxlentext}
</script>
<noscript>
    <p><strong>This editor requires JavaScript to run</strong>. In a browser with no JavaScript
    support, you should still see the contents (HTML data).  However, you will
    only be able to edit it normally, without the benefits of the editor's rich features.</p>
</noscript>
EOT;
}

?>