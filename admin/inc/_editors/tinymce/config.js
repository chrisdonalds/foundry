//
// TINY MCE CONFIGURATION FILE
//
// get parameters

jQuery(document).ready(function($){
    $.foundry.setupeditorparams('tiny_mce');

    tinymce.init({
        // General options
        selector : "textarea.tiny_mce",
        theme : "modern",
        height: $.foundry.height,
        width: $.foundry.width,
        theme_advanced_resizing_max_height : $.foundry.height,
        theme_advanced_resizing_min_width : $.foundry.width,
        theme_advanced_resizing_max_width : $.foundry.width,
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
             "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ],
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
        content_css : $.foundry.css
    });
});