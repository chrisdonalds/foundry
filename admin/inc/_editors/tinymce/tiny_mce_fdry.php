<?php
/*
TINY MCE FOUNDRY COMPANION
Web Template 4.0
Chris Donalds <cdonalds01@gmail.com>
========================================
*/

function tiny_mce_settings($action = null){
    global $_plugins;

    $folder = $_plugins->getPluginFolder();
    $incl = $_plugins->getPluginVerb($folder, 'folder');
    if($action == null){
        // return dialog HTML
        $settings = json_decode($_plugins->getPluginCustomSetting($incl, PLUGIN_SETTINGS_SAVETOSTD), true);
        $html = '
        <style>
            #pluginsettingsform { padding: 10px 0; }
            #pluginsettingsform label { display: inline-block; width: 130px; font-weight: normal; float: left; }
            #pluginsettingsform div { margin-bottom: 3px; overflow: hidden; }
            #tiny_mce_browseurl, #tiny_mce_uploadurl, #tiny_mce_css, #tiny_mce_tags { width: 400px; }
            #tiny_mce_width, #tiny_mce_height {}
        </style>
        <script type="text/Javascript">
            $jq(document).ready(function($){
                $("#pluginsettingsform input, #pluginsettingsform textarea").click(function(){
                    $("#pluginsettingsform .clickvar_target").removeClass("clickvar_target");
                    $(this).addClass("clickvar_target");
                });
            });
        </script>
        <div><label>Editor Size:</label><input type="text" name="width" id="tiny_mce_width" value="'.getIfSet($settings['width'], 750).'" size="10" /> px x <input type="text" name="height" id="tiny_mce_height" value="'.getIfSet($settings['height'], 520).'" size="10" /> px (W x H)</div>
        <div><label>Main CSS Path:</label><input type="text" name="css" id="tiny_mce_css" value="'.getIfSet($settings['css']).'" /></div>
        '.showClickableVariables(array(VL_FILEPATHS, "Editor URL"), array("Editor URL" => WEB_URL.ADMIN_FOLDER.EDITOR_FOLDER, "CKFinder Browse URL" => "ckfinder/ckfinder.html", "CKFinder Upload URL" => "ckfinder/core/connector/php/connector.php"));
        return $_plugins->pluginSettingsDialogContents("Tiny MCE", $html, __FUNCTION__);
    }elseif($action == PLUGIN_SETTINGS_SAVE){
        // save data to database
        parse_str($data, $arry);
        $result = $_plugins->savePluginCustomSettings($incl, json_encode($arry), PLUGIN_SETTINGS_SAVETOSTD);
        return $_plugins->pluginSettingsDialogButtonPressed(null, true);
    }elseif($action == PLUGIN_SETTINGS_CLOSE){
        // closed
        return $_plugins->pluginSettingsDialogButtonPressed(null, false);
    }
}

/**
 * Initiate the script loading process
 */
function tiny_mce_load(){
    global $_plugins;

    $folder = $_plugins->getPluginFolder();
    $_plugins->addScriptSourceLine(
        'script',
        WEB_URL.$folder,
        "tinymce.min.js"
    );
    $_plugins->addScriptSourceLine(
        'script',
        WEB_URL.$folder,
        "config.js", 
        "",
        "",
        "",
        false,
        false,
        null,
        true
    );
}

?>