<?php
// ---------------------------
//
// FOUNDRY ADMIN HELP DIALOG CONTENTS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
//
define("BASIC_GETINC", true);
include("inc/_core/loader.php");

// load user-defined help content -- if prepared
$data = '';
$gz_data = getRequestVar('data');
if($gz_data != '') $data = gzinflate(urldecode($gz_data));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<?php
$_plugins->loadPluginScripts("jqueryui", array("widgets" => "tabs"));
$_plugins->loadPluginScripts("basic");
$_plugins->showScriptSourceLines(true);
?>
</head>

<body>
	<div id="helptabs">
		<ul>
			<?php if($data != '') { ?><li><a href="#help_instructions">Instructions</a></li><?php } ?>
			<li><a href="#help_usage">Usage Help</a></li>
		</ul>
		<?php if($data != ''){ ?>
		<div id="help_instructions">
			<?php echo $data; ?>
		</div>
        <?php } ?>
        <div id="help_usage">
        </div>
	</div>
</body>
</html>