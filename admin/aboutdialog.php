<?php
// ---------------------------
//
// FOUNDRY ADMIN "ABOUT" DIALOG CONTENTS
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
//
define("BASIC_GETINC", true);
include("inc/_core/loader.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<?php
$_plugins->loadPluginScripts("basic");
$_plugins->showScriptSourceLines(true);

$login_img = $_media->getLoginImage();
$logo = $_media->getFlexThumbnail($login_img[0]);
?>
</head>

<body>
    <div id="help_about">
        <img src="<?php echo $logo ?>" alt="<?php echo SYS_NAME?>" />
        <p>
            <h3><?php echo SYS_NAME." ".CODE_VER." \"".CODE_VER_NAME."\""?></h3>
        </p>
        <p>
            &copy; <?php echo date("Y")?> <?php echo COPYRIGHT_NAME ?> and contributors.  All rights reserved.<br/>
            <?php echo SYS_NAME?> is free software released under the <a href="http://www.gnu.org/licenses/gpl-2.0.html" target="_blank">GNU/GPL License</a><br/></br>
            <?php if(COPYRIGHT_WEB != '') echo 'Visit <a href="'.COPYRIGHT_WEB.'" target="_blank">'.COPYRIGHT_WEB.'</a> for more.'?>
        </p>
        <hr/>
        <p style="text-align: left;">
            <span><strong>Installation Environment</strong>&nbsp; <?php if($_users->userIsAllowedTo(UA_VIEW_ADVANCED_SETTINGS)){?> (<a href="<?php echo WEB_URL.ADMIN_FOLDER ?>settings?tab=advanced&amp;sub=adv_reports" id="help_phpcfg_link2" title="Display System Report">View Report</a>)<?php } ?> </span><br/>
            <em>Web Server:</em> <?php echo getenv('SERVER_SOFTWARE')?><br/>
            <em>PHP Version:</em> <?php echo phpversion()?><br/>
            <em>Database Version:</em> <?php echo mysqli_get_client_version()?><br/>
            <em>Zend Engine Version:</em> <?php echo zend_version()?><br/>
            <em>Memory Usage:</em> <?php echo sprintf("%01.3f", memory_get_usage(true) / 1024)?> Kb<br/>
            <em>Admin Themeset:</em> <a href="<?php echo WEB_URL.ADMIN_FOLDER?>settings?tab=themes&mp;sub=theme_admin" title="View/edit theme settings"><?php echo $_themes->getAdminThemeAttr()?></a><br/><br/>
        </p>
        <hr/>
        <p style="text-align:left;">
            <span><strong>You are Using</strong></span><br/>
            <?php
            $browser = browser_detection("full_assoc");
            switch(strtolower($browser['browser_name'])){
                case 'gecko':
                    $browser_name = 'Firefox';
                    $browser_number = $browser['moz_data'][3];
                    $browser_notes = '';
                    $browser_bkpos = 1;
                    break;
                case 'msie':
                    $browser_name = 'Internet Explorer';
                    $browser_number = $browser['browser_math_number'];
                    $browser_notes = (($browser['os'] == 'nt' && $browser['os_number'] >= 6 && $browser_number < 9) ? 'You should upgrade your browser to the latest version.' : '');
                    $browser_bkpos = 4;
                    break;
                case 'chrome':
                    $browser_name = 'Google Chrome';
                    $browser_number = $browser['webkit_data'][1];
                    $browser_notes = '';
                    $browser_bkpos = 0;
                    break;
                case 'safari':
                    $browser_name = 'Safari';
                    $browser_number = $browser['browser_math_number'];
                    $browser_notes = '';
                    $browser_bkpos = 3;
                    break;
                default:
                    $browser_name = ucwords($browser['browser_name']);
                    $browser_number = $browser['browser_math_number'];
                    $browser_notes = '';
                    $browser_bkpos = -1;
                    break;
            }
            if($browser_bkpos >= 0){
            ?>
            <span class="browser_logo" style="background: url(<?php echo WEB_URL.ADMIN_FOLDER.CORE_FOLDER."images/general/browser_logos.png" ?>) no-repeat <?php echo (-$browser_bkpos * 36)?>px 0px;"></span>
            <?php } ?>
            Browser/Agent: <?php echo $browser_name?> <?php echo $browser_number?><br/>
            <?php echo $browser_notes?>
        </p>
        <hr/>
    </div>
</body>
</html>