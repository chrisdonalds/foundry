<?php

//  ------------------------------------------------------------------------------------

include("inc/_core/loader.php");
$_render->showHeader();
?>

    <div id="content-wrapper">
        <div id="contentarea">
            <div id="titlecenter">Welcome to <?php echo SYS_NAME?></div>
            <div class="center">To start, select one of the Sections from the Navigation Bar above.</div>
        </div>
    </div>
</div>

<?php
$_render->showFooter();
?>