/* ------------------------------------------------------------------------------------
Author: 		Chris Satterthwaite
Updated: 		May.18.2010
Updated By: 	Chris Satterthwaite
--------------------------------------------------------------------------------------- */
/* Start The document Ready function
--------------------------------------------------------------------------------------- */
$(document).ready(function(){
    /* Form Script
    ----------------------------------------------------------------------------------- */
	$('input, textarea, select').focus(function(){
		$(this).addClass("over");
		}).blur(function(){
		$(this).removeClass("over");
	});

	var runningRequest = false;
    var ajaxurl = $('#ajaxurl').val();

    $('.search_input').keyup(function(e){
        e.preventDefault();
        var $q = $(this);

        if($q.val() == ''){
            $('div.search_suggest').html('');
            return false;
        }

        //Abort opened requests to speed it up
        if(runningRequest){
            request.abort();
        }

        runningRequest = true;
        $q.addClass('search_suggest_wait');
        request = $.getJSON(ajaxurl+'ajax.php',{
            op: 'search',
            val: $q.val()
        }, function(data){
            showResults(data, $q);
            runningRequest = false;
            $q.removeClass('search_suggest_wait');
        });

        //Create HTML structure for the results and insert it on the result div
        function showResults(data, elem){
            var highlight = elem.val();
            var resultHtml = '';
            $.each(data, function(i, item){
                resultHtml+='<div class="result">';
                resultHtml+='<a href="'+item.link+'" target="_blank">'+item.title+'</a>';
                // resultHtml+='<p>'+item.post.replace(highlight, '<span class="highlight">'+highlight+'</span>')+'</p>';
                // resultHtml+='<p>'+item.post+'</p>';
                // resultHtml+='<a href="#" class="readMore">Read more..</a>'
                resultHtml+='</div>';
            });

            if(resultHtml != '')
                elem.closest('.search_form').find('.search_suggest').html(resultHtml).show();
            else
                elem.closest('.search_form').find('.search_suggest').hide();
        }

        $('form').submit(function(e){
            //e.preventDefault();
        });
    });

    $('*').click(function(){
        if ($(this).closest('.search_suggest').length == 0)
            $('.search_suggest').hide();
    });
});


/* Email Hide Script
--------------------------------------------------------------------------------------- */
function parse_email(user, server, domain, subject) {
	if(user && server && domain){
		emailto = "mailto:"+user+'@'+server+'.'+domain+"?subject="+subject;
		window.location = emailto;
	}
}

