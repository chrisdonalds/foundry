<?php
// ---------------------------
//
// FOUNDRY THUMBNAIL GENERATOR
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

/*
Usage: <img src="admin/inc/_core/thumb.php?f=filepath&w=width&h=height&m=watermark" />
*/
include("loader.php");
if(!isImageMagickLoaded()) die('Thumbgen: Imagemagick not installed!');

if(isset($_GET['f'])){
	$f = $_GET['f'];
	$w = intval((isset($_GET['w'])) ? $_GET['w'] : 0);		// width
	$h = intval((isset($_GET['h'])) ? $_GET['h'] : 0);		// height
	$m = ((isset($_GET['m'])) ? $_GET['m'] : '');			// max size
	$c = (isset($_GET['c']) ? (bool) $_GET['c'] : 100);		// compression
	$x = (isset($_GET['x']) ? $_GET['x'] : null);			// crop type ('tl', 'tr', 'bl', 'br', 'c') default = 'c'
															// - additional params = xw and xh

	if($m == 'max'){
		$mh = $h; $mw = $w;
    }else{
        $mh = 0; $mw = 0;
	}

	if(!file_exists($f)) $f = SITE_PATH.$_GET['f'];
	if(!file_exists($f)) $f = SITE_PATH.IMG_UPLOAD_FOLDER.$_GET['f'];

	if(file_exists($f)){
		$p = pathinfo($f);
		$e = $p['extension'];
		if($e == 'jpg') $e = 'jpeg';
		$thumb = new Imagick($f);
		$thumb->setImageFormat($e);

		$cw = $thumb->getImageWidth();
		$ch = $thumb->getImageHeight();
		if($cw > 0 && $ch > 0){
			if($w <= 0 && $h <= 0) {
				// use current dimensions
				$w = $cw;
				$h = $ch;
			} elseif($w <= 0) {
				// height was specified. resize width proportionally
				$w = $cw * ($h / $ch);
			} else {
				// width was specified. resize height proportionally
				$h = $ch * ($w / $cw);
			}

			if($w < 0) $w = 0;
			if($h < 0) $h = 0;

			// crop dimensions
			if(!is_null($x)) {
				$xw = intval((isset($_GET['xw'])) ? $_GET['xw'] : $w);		// crop width
				$xh = intval((isset($_GET['xh'])) ? $_GET['xh'] : $h);		// crop height
			}

			// push-to-max
			if($m == 'max' && $h > 0 && $w > 0){
				$a = $w / $h;
				if($mw > $w){
					$h = $h * $mw / $w; $w = $mw;
				}
				if($mh > $h){
					$w = $w * $mh / $h; $h = $mh;
				}
			}

			// Compress image and remove extraneous data
			if($c > 0 && $c < 100){
				switch($e){
					case 'jpeg':
					    $thumb->setImageCompression(Imagick::COMPRESSION_JPEG);
						$thumb->setImageCompressionQuality($c);
						break;
					case 'png':
					    $thumb->setImageCompression(Imagick::COMPRESSION_JPEG);
						break;
				}
				$thumb->stripImage();
			}

    		// Alternative thumbnailing methods //
			//$thumb->resizeImage($w, $h, Imagick::FILTER_LANCZOS, 1);
			//exec('convert small_image.jpg -gravity Center -resize "208x120>" -background white -extent 208x120 s_icon.gif');

			// Create thumbnail
			$thumb->thumbnailImage($w, $h);

			// cropping
			if(in_array($x, array('tl', 'tr', 'bl', 'br', 'c'))) {
				$imageinfo = $thumb->identifyImage();
				if(isset($imageinfo['geometry'])){
					$size = $imageinfo['geometry'];
					if(isset($size['width']) && isset($size['height'])){
						$iw = intval($size['width']);
						$ih = intval($size['height']);
						if($xw < $iw || $xh < $ih){
							// desired region is smaller than the image size on one or both sides
							switch($x){
								case 'tl':
									$l = 0;
									$t = 0;
									break;
								case 'tr':
									$l = ($iw - $xw);
									$t = 0;
									break;
								case 'bl':
									$l = 0;
									$t = ($ih - $xh);
									break;
								case 'br':
									$l = ($iw - $xw);
									$t = ($ih - $xh);
									break;
								default:
									$l = ($iw - $xw) / 2;
									$t = ($ih - $xh) / 2;
									break;
							}
							$thumb->cropImage($xw, $xh, $l, $t);
						}
					}
				}
			}

			// Output image back to header stream
			header('Content-Type: image/'.$e);
			echo $thumb;

			// Clear memory
			$thumb->destroy();
		}else{
			echo "Thumbgen: Image '$f' dimensions unreadable!<br>";
		}
	}else{
		echo "Thumbgen: Image '$f' not found!<br>";
	}
}else{
	echo "Thumbgen: 'f' parameter is required!<br>";
}
?>