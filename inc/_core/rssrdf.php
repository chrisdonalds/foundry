<?php
// ---------------------------
//
// RSS, RDF, ATOM OUTPUT GENERATOR
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

/**
 * Generate a RSS file from datebase content.
 * Content is polled from triggered functions
 */
function do_rss(){
	global $_events, $_rss, $_db_control, $_users;

	//Allow external methods to craft the RSS dataset
	list($dataset) = $_events->processTriggerEvent(__FUNCTION__, null, null);
	if(is_null($dataset)){
		//No data was provided
    	$dataset = $_db_control
    		->setTable(PAGES_TABLE)
    		->setFields(array("`".PAGES_TABLE."`.id, `".PAGES_TABLE."`.pagetitle as title, `".PAGES_CONTENT_TABLE."`.content"))
    		->setJoins(array(array("type" => "LEFT JOIN", "table" => PAGES_CONTENT_TABLE, "comp" => PAGES_CONTENT_TABLE.".pageid=".PAGES_TABLE.".id")))
    		->setOrder("`".PAGES_TABLE."`.pagename ASC")
    		->getData();
	}

	if(empty($dataset)) return false;

	//Create RSS feed object
	$bus = htmlentities(BUSINESS);
	$site = htmlentities(SITE_NAME);

	//SetChannel ($url, $title, $description, $lang, $copyright, $creator, $subject)
	$_rss->rss_setChannel(WEB_URL.'xml.rss', $bus, $site, LANG_ISO, '(c) '.date("Y").' '.$bus, $bus, '');

	//SetImage ($url) [optional]
	$_rss->rss_setImage(WEB_URL.'images/logo.png');

	//SetItem ($url, $title, $description)
	foreach ($dataset as $line) {
		if ($line['id'] > 0 && $line['title'] != "" && $line['content'] != "" && $line['alias'] != "") {
			$title = htmlentities(strip_tags(str_replace("&#39;", "'", $line['title']), ENT_QUOTES));
			$item = "<![CDATA[";
			if(isDate($line['date_published']))
				$date = $line['date_published'];
			elseif(isDate($line['date_created']))
				$date = $line['date_created'];
			else
				$date = null;
			if(!is_null($date)) {
				$item .= "<b>When:</b> ".date("F j, Y", strtotime($date));
				if(isDate($line['viewable_to'])) $item .= " to ".date("F j, Y", strtotime($line['viewable_to']));
				$item.= "<br/><br/>\n";
			}
			$item .= substr(strip_tags(nl2br(html_entity_decode($line['content']))), 0, 50)."...";
			$item .= "]]>";
	        $_rss->rss_setItem(WEB_URL.ltrim($line['alias'], "/"),
	                        	$title,
	                        	$item);
		}
	}
	echo $_rss->rss_output();
}

/**
 * Generate a RDF file from datebase content.
 * Content is polled from triggered functions
 */
function do_rdf(){
	global $_events, $_rss, $_db_control, $_users;

	//Allow external methods to craft the RSS dataset
	list($dataset) = $_events->processTriggerEvent(__FUNCTION__, null, null);
	if(is_null($dataset)){
		//No data was provided
    	$dataset = $_db_control
    		->setTable(PAGES_TABLE)
    		->setFields(array("`".PAGES_TABLE."`.id, `".PAGES_TABLE."`.pagetitle as title, `".PAGES_CONTENT_TABLE."`.content"))
    		->setJoins(array(array("type" => "LEFT JOIN", "table" => PAGES_CONTENT_TABLE, "comp" => PAGES_CONTENT_TABLE.".pageid=".PAGES_TABLE.".id")))
    		->setOrder("`".PAGES_TABLE."`.pagename ASC")
    		->getData();
	}

	if(empty($dataset)) return false;

	//Create RSS feed object
	$bus = htmlentities(BUSINESS);
	$site = htmlentities(SITE_NAME);

	//SetChannel ($url, $title, $description, $lang, $copyright, $creator, $subject)
	$_rss->rss_setChannel(WEB_URL.'xml.rdf', $bus, $site, LANG_ISO, '(c) '.date("Y").' '.$bus, $bus, '');

	//SetImage ($url) [optional]
	$_rss->rss_setImage(WEB_URL.'images/logo.png');

	//SetItem ($url, $title, $description)
	foreach ($dataset as $line) {
		if ($line['id'] > 0 && $line['title'] != "" && $line['content'] != "" && $line['alias'] != "") {
			$title = htmlentities(strip_tags(str_replace("&#39;", "'", $line['title']), ENT_QUOTES));
			$item = "<![CDATA[";
			if(isDate($line['date_published']))
				$date = $line['date_published'];
			elseif(isDate($line['date_created']))
				$date = $line['date_created'];
			else
				$date = null;
			if(!is_null($date)) {
				$item .= "<b>When:</b> ".date("F j, Y", strtotime($date));
				if(isDate($line['viewable_to'])) $item .= " to ".date("F j, Y", strtotime($line['viewable_to']));
				$item.= "<br/><br/>\n";
			}
			$item .= substr(strip_tags(nl2br(html_entity_decode($line['content']))), 0, 50)."...";
			$item .= "]]>";
	        $_rss->rss_setItem(WEB_URL.ltrim($line['alias'], "/"),
	                        	$title,
	                        	$item);
		}
	}
	echo $_rss->rdf_output();
}

/**
 * Generate a ATOM file from datebase content.
 * Content is polled from triggered functions
 */
function do_atom(){
	global $_events, $_rss, $_db_control, $_users;

	//Allow external methods to craft the RSS dataset
	list($dataset) = $_events->processTriggerEvent(__FUNCTION__, null, null);
	list($generator) = $_events->processTriggerEvent('get_atom_generator', null, null);
	if(is_null($dataset)){
		//No data was provided
    	$dataset = $_db_control
    		->setTable(PAGES_TABLE)
    		->setFields(array("`".PAGES_TABLE."`.id, `".PAGES_TABLE."`.pagetitle as title, `".PAGES_CONTENT_TABLE."`.content"))
    		->setJoins(array(array("type" => "LEFT JOIN", "table" => PAGES_CONTENT_TABLE, "comp" => PAGES_CONTENT_TABLE.".pageid=".PAGES_TABLE.".id")))
    		->setOrder("`".PAGES_TABLE."`.pagename ASC")
    		->getData();
	}

	if(empty($dataset)) return false;

	//Create RSS feed object
	$bus = htmlentities(BUSINESS);
	$site = htmlentities(SITE_NAME);
	$id = WEB_URL.'atom.xml';

	//SetChannel ($url, $title, $description, $lang, $copyright, $creator, $subject)
	$_rss->atom_setChannel($id, '/atom.xml', $bus, $site, LANG_ISO, '(c) '.date("Y").' '.$bus, array("name" => $bus, "email" => ADMIN_EMAIL, "uri" => WEB_URL));

	//SetImage ($url) [optional]
	$_rss->rss_setImage(WEB_URL.'images/logo.png');

	//SetItem ($url, $title, $description)
	foreach ($dataset as $line) {
		if ($line['id'] > 0 && $line['title'] != "" && $line['content'] != "" && $line['alias'] != "") {
			$title = htmlentities(strip_tags(str_replace("&#39;", "'", $line['title']), ENT_QUOTES));
			$item = substr(strip_tags(nl2br(html_entity_decode($line['content']))), 0, 50)."...";
	        $_rss->atom_setItem($line['id'],
	        					$line['alias'],
	                        	$title,
	                        	$item,
	                        	$line['date_updated'],
	                        	$line['date_published'],
	                        	$_users->getUserInfo($line['user_id'], 'firstname').' '.$_users->getUserInfo($line['user_id'], 'lastname')
	                        	);
		}
	}
	echo $_rss->atom_output();
}
?>