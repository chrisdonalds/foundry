<?php
// ------------------------------------------
//
// FOUNDRY FRONT LOADER & INITIATOR
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ------------------------------------------
//
//*****************************************************************************
//   MANDATORY FRONTEND INCLUSIONS
//*****************************************************************************

define("IN_ADMIN", false);
if(!defined("DB_USED")) define("DB_USED", true);
if(!defined("VHOST")) define("VHOST", "/");
if(!defined("IN_AJAX")) define("IN_AJAX", false);
if(!defined("VALID_LOAD")) define("VALID_LOAD", true);
if(!defined('IS_INSTALLER')) define('IS_INSTALLER', false);

//*****************************************************************************
//   BASIC LOAD
//*****************************************************************************

include_once (LOADER_DOCUMENT_ROOT.VHOST."admin/inc/_config/configs.php");
if(strpos(SITE_PATH, "/") === false) die("Configuration Loading Failure: Check path to configs.php in getinc.php");
include_once (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."common_core.php");
if(!defined("CORELOADED")) die("Core Engine (COMMON_CORE) was not loaded!");

require_once (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."collector.class.php");
if(!defined("COLLECTLIBLOADED")) die("DI Collector Class Library was not loaded!");

//*****************************************************************************
//   DATABASE AND CLASS LIBRARIES SETUP
//*****************************************************************************

$_system = SystemClass::init($configs);
$_events   = EventsClass::init();
$_events->processTriggerEvent('init_system');                // alert triggered functions when function executes
$_debug    = DebugClass::init();

if(DB_USED){
    // the database is active in almost all cases -- except when the database is
    // being reconfigured, backed up, restored, or is under some sort of conditioning
    require_once (SITE_PATH.ADMIN_FOLDER.DB_FOLDER."db_wrapper.class.php");
    require_once (SITE_PATH.ADMIN_FOLDER.LIB_FOLDER."db-control.class.php");
    $_db_control = new DB_controller(DBHOST, DBNAME, DBUSER, DBPASS, DBPORT);
    if(!defined("DBLOADED")) die("Database controller class was not loaded!");
    $_collector = new CollectorClass($_db_control);

    // DI Collector-dependent classes
    $_media  = MediaClass::init($_collector);
}
$_error    = ErrorClass::init();
$_settings = SettingsClass::init();
$_users    = UsersClass::init();
$_stats    = StatsClass::init();
$_filesys  = FileSysClass::init();
$_sec      = SecurityClass::init();
$_menus    = MenuClass::init();
$_data     = DataClass::init();
$_cron     = CronClass::init();
$_cache    = CacheClass::init();

//*****************************************************************************
//   LONG LOAD (QUICK LOAD SKIPS PLUGINS AND FORMS, ETC)
//*****************************************************************************

if(!QUICK_LOAD){
    $_themes        = ThemeClass::init();
    $_plugins       = PluginsClass::init();
    $_render        = FrontRenderClass::init();
    $_tax           = TaxonomiesClass::init();
    $_page          = PageClass::init();
    $_data          = DataClass::init();
    $_rss           = RSSClass::init();
    $_inflect       = new Inflector();
}

//*****************************************************************************
//   SUPPLEMENTARY INCLUSIONS
//*****************************************************************************

include_once (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."getvars.php");
include_once (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."geterrormsgs.php");
include_once (SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."getbrowser.php");

//----------------------------- BASIC INIT ------------------------------------

//*****************************************************************************
//   SESSION PREPARATION
//*****************************************************************************

if(defined('IS_INSTALLER'))
    session_name('installer');
else
    session_name('front');
session_start();
$_error->initErrorMsg();

//*****************************************************************************
//   USER-CLASS PREPARATION
//*****************************************************************************

$_users->isloggedin = getIfSet($_SESSION['admlogin']);
$_users->id = getIfSet($_SESSION['userdata']['id']);
$_users->level = getIfSet($_SESSION['userdata']['level']);
$_users->logintimestamp = getIfSet($_SESSION['timestamp']);
$_users->username = getIfSet($_SESSION['userdata']['username']);
$_users->firstname = getIfSet($_SESSION['userdata']['firstname']);
$_users->lastname = getIfSet($_SESSION['userdata']['lastname']);
if(DB_USED){
    $_users->activelist = $_db_control->setTable(ACCOUNTS_TABLE)->setWhere("activated=1 AND blocked=0")->setOrder("username")->getRec();
    $_users->initAllowances();
    $_users->defineCustomUserTypes();
}else{
    $_users->activelist = null;
}

//*****************************************************************************
//   SYSYEM PREPARATION
//*****************************************************************************

if(!QUICK_LOAD){

	if(!defined("BASIC_GETINC")){

        //*****************************************************************************
        //   ADVANCED INIT
        //
		//   RESETS
		//*****************************************************************************

		prepareSessionElements();

		//*****************************************************************************
		//   LOAD INCLUDED FRONT-FACING PLUGINS
		//*****************************************************************************

		$_plugins->initPluginsandFrameworks();

        //*****************************************************************************
        //   THEME INCLUSIONS
        //*****************************************************************************

        $_themes->getInstalledThemes("website", false);
        $path = $_themes->getThemePathUnder("website");
        $_themes->initThemeOps("website");

		//*****************************************************************************
		//   DATA ALIASES AND TAXONOMIES
		//*****************************************************************************

        $_data->initDataObjects();
        $_tax->initTaxonomies();
	}
}

//*****************************************************************************
//   POST-LOAD
//*****************************************************************************

$_events->processTriggerEvent('front_getinc_done');                // alert triggered functions when function executes
?>