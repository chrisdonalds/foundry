<?php
// ---------------------------
//
// PUBLIC SITE LOADER
// - Form & Page
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
if(isset($_SERVER['DOCUMENT_ROOT']) && $_SERVER['DOCUMENT_ROOT'] != ''){
    define ("LOADER_DOCUMENT_ROOT", $_SERVER['DOCUMENT_ROOT']);
    if(!defined('VHOST')) define("VHOST", substr(str_replace("\\", "/", realpath(dirname(__FILE__)."/../../")), strlen(realpath($_SERVER['DOCUMENT_ROOT'])))."/");
}else{
    define ("LOADER_DOCUMENT_ROOT", str_replace("\\", "/", realpath(dirname(__FILE__)."/../../")));
    if(!defined('VHOST')) define("VHOST", "/");
}

// start Foundry bootstrap
include (LOADER_DOCUMENT_ROOT.VHOST."inc/_core/getinc.php");

// $_GET values
$func_caller = getRequestVar('f');
$func_to_call = getRequestVar('fc');
$func_params = getRequestVar('fp');       // val,val2
loader_call_func($func_caller, $func_to_call, $func_params);

// run crons
if(!defined('IN_AJAX')) define('IN_AJAX', false);
// if($func_caller == INSTALLER_ALIAS){
//     // ... installer
//     $n = $_sec->createNonce(STATIC_SALT);
//     if(file_exists(SITE_PATH.'installer.php')) header('location: '.WEB_URL.'installer.php?n='.$n);
//     exit;
// }
if(!PHP_CLI && !IS_INSTALLER) $_cron->run_crons();

if(!QUICK_LOAD){
    if(SITEOFFLINE != SITE_VISIBILITY_MAINTENANCE){
    	// site online or private viewing
        if(SITEOFFLINE == SITE_VISIBILITY_PRIVATE && !$_users->isPrivatelyLoggedin() && !defined('PVTLOADED'))
            $_filesys->gotoPage(WEB_URL.ADMIN_FOLDER.CORE_FOLDER."pvtlogin.php?rurl=".urlencode($_SERVER['REQUEST_URI']));

        // get default active theme folder
        $themefolder = $_themes->getThemePathUnder("website");

        if($themefolder !== false && file_exists(SITE_PATH.$themefolder)){
            // prepare _PAGE class properties
            $filespec = getRequestVar('f');
            if(empty($filespec)) $filespec = substr($_SERVER['REQUEST_URI'], strlen(VHOST));

            $fileparts = parse_url($filespec);
            $filename = getIfSet($fileparts['path']);

        	$_page->name = $filename;
        	$_page->file = $filename;
        	if(preg_match("/[.]/", $_page->name)) {
        		// remove extension from name, file is ok
        		$_page->name = preg_replace("/([.].*)/", "", $_page->name);
        	}else{
        		// add .php to file, name is ok
        		$_page->file .= ".php";
        	}
            $_page->urlpath = explode("/", $_page->name);

            // get file query elements
            $fileparts = parse_url($_SERVER['REQUEST_URI']);
            $filequery = getIfSet($fileparts['query']);
            parse_str($filequery, $qv);
            if(count($qv) > 0) {
                $_page->queryvars = $qv;
            }else{
                $_page->queryvars = array();
            }

            if(basename($_page->name) == "__ajax"){
                //... ECAJ: AJAX redirector
                $_REQUEST += $qv;       // add query to request global
                @include_once(SITE_PATH.ADMIN_FOLDER.CORE_FOLDER."ajaxwrapper.php");
                return;
            }elseif(preg_match("/thumb\.php/", $_SERVER['REQUEST_URI'])){
                //... ECTH: thumbnail generator file
                $_page->found = true;
                @include(SITE_PATH.THUMB_GEN);
                return;
            }elseif(basename($_page->name) == THUMB_GEN_ALIAS){
                //... ECTH: thumbnail generation redirector
                $_page->found = true;
                unset($_GET['f']);
                header('location: '.WEB_URL.THUMB_GEN.'?'.http_build_query($_GET));
                return;
            }elseif(basename($_page->name) == RSS_ALIAS){
                //... ECRS: rss generation redirector
                $_page->found = true;
                unset($_GET['f']);
                @include(SITE_PATH.RSSRDF_GEN);
                do_rss();
                return;
            }elseif(basename($_page->name) == RDF_ALIAS){
                //... ECRD: rss generation redirector
                $_page->found = true;
                unset($_GET['f']);
                @include(SITE_PATH.RSSRDF_GEN);
                do_rdf();
                return;
            }elseif(basename($_page->name) == ATOM_ALIAS){
                //... ECAT: rss generation redirector
                $_page->found = true;
                unset($_GET['f']);
                @include(SITE_PATH.RSSRDF_GEN);
                do_atom();
                return;
            }elseif($_page->urlpath[0] == REST_ALIAS){
                //... ECRT: REST API redirector
                // initRestEndpoint();
                return;
            }elseif($_page->urlpath[0] == CRON_ALIAS){
                //... ECCR: CRON API redirector
                $_cron->run_crons(array(), true, getRequestVar('ckey'));
                return;
        	}elseif(file_exists(SITE_PATH.$themefolder.$_page->file)) {
        		//... EC01: physical file found.  page will have to obtain data itself
                $_render->setupDirectPage($themefolder.$_page->file);
        		@include_once(SITE_PATH.$themefolder.$_page->file);
                return;
        	}else{
                if($_render->setupPage()){
                    //... EC02: go to PHP template file that will handle a page alias
                    @include(SITE_PATH.$_page->target);
                    exit;
                }elseif($_render->setupPageData()){
                    //... EC03: go to {metabase}.php/data.php for dynamic data alias processing
                    @include_once($file);
                    exit;
                }elseif($_render->setupPageController()){
                    //... EC04: call the applicable controller class method
                    exit;
                }
            }

            if(!$_page->found){
                $errlink = $_filesys->getErrorDocFile("website", "404");
                if($errlink !== false){
                    //... EC40: nothing to handle the URI, go to physical 404.php or database '404' page
                    @include_once($errlink);
                    return;
                }else{
                    //... EC142: safety net. output a fast-404 page
                    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"
                        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
                    <html xmlns=\"http://www.w3.org/1999/xhtml\">
                    <head>
                    <title>Page not found</title>
                    <meta name=\"copyright\" content=\"Copyright ".date("Y")." ".SYS_NAME." Development Group\"/>
                    <meta name=\"content-language\" content=\"EN\" />
                    <meta name=\"generator\" content=\"".SYS_NAME." ".CODE_VER." (".CODE_VER_NAME.")\" />
                    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
                    </head>
                    <body>
                    <h1>Oops, We Have a Problem!</h1>
                    <p>The page or resource, ".strtoupper($_page->name).", was not found.</p>
                    <p>As this message is presented on the rare occasion that the site has encountered a problem,
                    we recommend than you:</p>
                    <ul>
                    <li>Discuss this issue with the webmaster, or;</li>
                    <li>Contact the administrator or hosting provider.</li>
                    </ul>
                    <p>ERROR: EC142</p>
                    </body>
                    </html>";
                    exit;
                }
            }
        }else{
            //... EC50: no theme folder found
            echo "
            <h1>Oops, We Have a Problem!</h1>
            <p>No active website themes have been installed or activated, and the default website theme could not be found.</p>
            <p>We recommend than you discuss this issue with the webmaster or developer.</p>
            <p>ERROR: EC50</p>";
            exit;
        }
    }elseif(SITEOFFLINE == 1){
    	// site offline

    	if(file_exists(SITE_PATH.$themefolder."offline.php")){
            //... EC10: offline page found
    		@include_once(SITE_PATH.$themefolder."offline.php");
            return;
    	}else{
            //... EC11: simple offline statement
    		echo "
    		<h1>Site is Down for Maintenance</h1>
    		<p>".SITEOFFLINE_MSG."</p>";
    		exit;
    	}
    }
}

function loader_call_func($func_caller, $func_to_call, $func_params = null){
    // Call system functions dynamically
    if($func_to_call != ''){
        if(function_exists($func_to_call)) {
            if($func_params != ''){
                $func_params = urldecode($func_params);
                $func_param_arry = explode(",", $func_params);
                call_user_func_array($func_to_call, $func_param_arry);
            }else{
                call_user_func($func_to_call);
            }
        }else{
            if($func_caller != '') $func_caller = "called from '$func_caller' ";
            die("Init: Function named '$func_to_call' {$func_caller} does not exist.");
        }
    }
}
?>
