<?php ?>
        <form class="search_form" method="POST" action="<?php echo WEB_URL?>search_results">
            <p class="search_fld"><input placeholder="search" type="text" name="search" value="<?php echo getIfSet($search)?>" class="search_input" autocomplete="off" /></p>
            <p class="go_btn"><input type="submit" name="submit" value="go" /></p>
            <div class="search_suggest"></div>
        </form>
