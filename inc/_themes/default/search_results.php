<?php
/*
Title:			Template Content/html Page
Author: 		Chris Satterthwaite, Chris Donalds
Updated: 		May.18.2010
Updated By: 	Chris Satterthwaite, Chris Donalds
 */

// META tags -- Leave blank to use the values set in the Admin system
$metatitle			= "";
$metakeywords 		= "";
$metadescription 	= "";
// navon states (name of page, same as css classname for nav object)
$navname			= "";
// Plugin inclusion value -- eg. lightbox, form2email, googlemap, cdcal, login... (space separated list)
$incl				= "dynsearch";
// UICore Plugin inclusion value -- eg. sortable,draggable,droppable,accordion,progressbar,dialog,datepicker
$uicore				= "";

startPage();
$error = "";
$found_recs = array();
$found_num = 0;
if(isset($_POST) && getRequestVar('search') != ""){
    extractVariables($_REQUEST);
    if(!empty($search) && isPluginIncluded($incl)){
        $search_clause = dosearch(  $search,
                                    array("text" => array("d.itemtitle", "d.description", "d.filename")
                                       )
                                    );

        // append returned string to current WHERE clause
        if($search_clause !== false){
            $where_clause = $search_clause;

            $found_num  = getRecNumRows(
                                "data_pdfs as d",
                                "d.*",
                                $where_clause);
            $found_recs = getRec(
                                "data_pdfs as d",
                                "d.*",
                                $where_clause, "d.itemtitle ASC", "", "");

        }
    }else{
        gotoPage(WEB_URL.'main');
    }
}
#-------------------------------------------------------------------------------
?>
	<div id="search_div">
        <?php include(SITE_PATH."search_form.php"); ?>
        <p id="search_div_stmt">All the forms and catalogues in one place</p>
        <div id="search_title">Searched: <span><?php echo $search?></span></div>
        <?php
        if($found_num > 0){
            echo '<ul id="pdf_list_search">'.PHP_EOL;
            foreach($found_recs as $pdf){
                $title = $pdf['itemtitle'];
                $descr = substr($pdf['description'], 0, 200);
            ?>
            <li class="pdf">
                <?php if ($pdf['file_type'] != "") {?>
                <span class="pdf_form_type <?php echo $pdf['file_type']?>"><?php echo ucwords_smart($pdf['file_type'])?></span>
                <?php } ?>
                <a href="<?php echo WEB_URL.$pdf['filename']?>" target="_blank" title="<?php echo  $pdf['itemtitle']?>"><?php echo $title?></a>
                <?php if($pdf['archived'] == 0){ ?>
                <span class="pdf_archive_status">Current</span>
                <?php } else { ?>
                <span class="pdf_archive_status archived">Archived</span>
                <?php } ?>
                <span class="pdf_description"><?php echo $descr?></span>
            </li>
            <?php
                //}
            }
        } else {
            ?><p class="no_pdfs">No documents found.</p><?php
        }
	 ?></div>
<?php
showFooter();
?>