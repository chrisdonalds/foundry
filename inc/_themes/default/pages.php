<?php
/*
Title:			Generic/Dynamic Page
Author: 		Chris Satterthwaite, Chris Donalds
Updated: 		May.18.2010
Updated By: 	Chris Satterthwaite, Chris Donalds
 */

$_render->startPage();
// $_page->show_properties();
// queueMacro('testmacro');
// function testmacro($args){
//     return "<div>".join(", ", $args)."</div>";
// }

?>
	<div class="section">
		<h1 admin="page:<?php echo $_page->id?>"><?php $_render->showTitle(); ?></h1>
		<?php $_render->showContents(0, 'id="whatever"'); ?>
	</div>
<?php
$_render->showPageFooter();
die('pages');
?>