<?php
// ---------------------------
//
// FOUNDRY LIBRARY CLASSES
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define ("DATALIBLOADED", true);
if(!defined("VALID_LOAD")) die ("This file cannot be accessed directly!");

// Data Aliases meta codes
define("DA_CATEGORY", "@cat@");
define("DA_TERM", "@term@");
define("DA_TERM_ID", "@termid@");
define("DA_CODE", "@code@");
define("DA_ID", "@id@");
define("DA_TYPE", "@type@");
define("DA_YEAR", "@year@");
define("DA_MONTH", "@month@");
define("DA_DAY", "@day@");
define("DA_HOUR", "@hour@");
define("DA_MINUTE", "@minute@");
define("DA_SECOND", "@second@");
define("ATTR_CLASS_ADMIN_ALIAS", "_admin_alias");
define("ATTR_CLASS_DATA_ALIAS", "_data_alias");
define("ATTR_CLASS_TAX_ALIAS", "_tax_alias");

/* DATA ALIAS CLASS
 *
 * Maintains data aliases and rules
 */
class DataClass {
	// overloaded data
	private $_keys = array(	"table" => "", "childtable" => "", "id" => 0, "aliases" => array(), "datatables" => array(),
							"iscategory" => false, "queryvars" => "", "pattern" => "", "datatype" => "",
                           	"childdatatype" => "", "dbrec" => array(), "query" => "",
                           	"nonce" => "", "found" => "", "rulebase" => "", "error" => "",
                           	"ispublished" => false, "isdraft" => false, "created" => "", "updated" => "",
                           	"published" => "", "numrows" => 0 );
	private $_subkeys = array("page_aliases" => array(), "data_aliases" => array(), "admin_aliases" => array(), "taxonomy_aliases" => array());
	private $_db = null;
	protected static $_instance = null;

	public function __construct() {
	}

	private function __clone(){
	}

	public static function init(){
		global $_db_control, $_events;

		$s = new self;
		$s->_db = $_db_control;
		$_events->processTriggerEvent(__FUNCTION__.'_data');				// alert triggered functions when function executes
		return $s;
	}

	/**
	 * Ensures that the records from the subject datatype table
	 *                  1) have corresponding attributes
	 *                  2) have an alias
	 *                  3) have a path to a valid file
	 * @param string $datatype
	 * @param boolean $existanceCheck
	 * @param boolean $aliasCheck
	 * @param boolean $pathCheck
	 */
	function reinforceDataAttributes($datatype, $existanceCheck = true, $aliasCheck = true, $pathCheck = true, $adminCheck = true){
	    global $_db_control;

	    $data = $_db_control->setTable($datatype)->setJoins(array(array("type" => "LEFT JOIN", "table" => ATTRIBUTES_TABLE, "comp" => $datatype.".id = ".ATTRIBUTES_TABLE.".data_id AND ".ATTRIBUTES_TABLE.".data_type = '".$datatype."'")))->setWhere(ATTRIBUTES_TABLE.".attribute_class != '".ATTR_CLASS_DATA_ALIAS."' OR ".ATTRIBUTES_TABLE.".attribute_class IS NULL")->setOrder(ATTRIBUTES_TABLE.".data_id")->getRecJoin();
	    if(count($data) == 0) return true;

	    $ok = true;
	    foreach($data as $datum){
	        $values = array();

	        // 1) (if requested) check to make sure each datatype record has a corresponding attribute record
	        if($existanceCheck){
	            if(!isset($datum['data_id'])){
	                $values['alias'] = null;
	                $values['path'] = null;
	                $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("data_type" => $datatype, "data_id" => $datum['id'], "attribute_class" => "data"))->insertRec();
	            }
	        }

	        // 2) (if requested) check that each datatype record has a validly constructed alias
	        if($aliasCheck){
	            if(is_null($datum['alias']) || (!isset($datum['data_id']) && isset($values['data_id']))){
	                if(isset($datum['key']))
	                    $values['alias'] = $datum['key'];
	                elseif(isset($datum['code']))
	                    $values['alias'] = $datum['code'];
	                if(isset($datum['name']))
	                    $values['alias'] = codify($datum['name']);
	                if(isset($datum['pagename']))
	                    $values['alias'] = codify($datum['pagename']);
	                if(isset($datum['attribute_class']) || !in_array($datum['attribute_class'], array('data', 'rule')))
	                    $values['attribute_class'] = "data";
	            }
	        }

	        // 3) (if requested) check that each datatype record has a validly accessible path

	        // 4) update attribute record
	        if(!empty($values)){
	            $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals($values)->setWhere("`data_id` = '".$datum['id']."' AND `data_type` = '".$datatype."'")->updateRec();
	        }
	    }
//2
	    // 5) check if attributes exist and contains valid info for admin system dependant attributes
	    // - For instance, the Pages type is an important menu
	    if($adminCheck){
	        if($datatype == PAGES_TABLE){
	            $recs = $_db_control->setTable(ATTRIBUTES_TABLE)->setWhere("`attribute_class` = '".ATTR_CLASS_ADMIN_ALIAS."' AND `data_type` = '".PAGES_TABLE."' AND `alias` = 'pages/list'")->getRec();
	            if(count($recs) > 1){
	                $_db_control->setTable(ATTRIBUTES_TABLE)->setWhere("`attribute_class` = '".ATTR_CLASS_ADMIN_ALIAS."' AND `data_type` = '".PAGES_TABLE."' AND `alias` = '".PAGES_TABLE."/list'")->deleteRec();
	                $recs = array();
	          	}

	            if(empty($recs)){
	                $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("attribute_class" => ATTR_CLASS_ADMIN_ALIAS, "data_type" => PAGES_TABLE, "alias" => PAGES_TABLE."/list", "data_id" => 0))->insertRec();
	            }else{
	                $problem = false;
	                if($recs[0]['attribute_class'] != ATTR_CLASS_ADMIN_ALIAS) $problem = true;
	                if($recs[0]['data_type'] != PAGES_TABLE) $problem = true;
	                if($recs[0]['alias'] != PAGES_TABLE."/list") $problem = true;
	                if($recs[0]['data_id'] != '0') $problem = true;
	                if($problem) $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("attribute_class" => ATTR_CLASS_ADMIN_ALIAS, "data_type" => PAGES_TABLE, "alias" => PAGES_TABLE."/list", "data_id" => 0))->setWhere("`attribute_id` = '".$recs[0]['attribute_id']."'")->updateRec();
	            }
	        }
	    }
	}

	/**
	 * Causes a page to become the site home page, resetting all others to normal pages
	 * @param string $pagetitle
	 */
	function switchHomePageTo($datatype, $pagename){
	    global $_db_control, $_events;

	    if(!empty($datatype) && !empty($pagename)){
	        $id = $_db_control->setTable($datatype)->setFields("id")->setWhere("pagename = '".$pagename."'")->setLimit()->getRec();
	        if(!empty($id)){
	            $id = $id[0]['id'];
	            $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("homepage" => 0))->setWhere("data_id != '$id' AND data_type = '$datatype'")->updateRec();
	            $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("homepage" => 1))->setWhere("data_id = '$id' AND data_type = '$datatype'")->updateRec();
	        }
	    }
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Initialize all data objects
	 */
	function initDataObjects(){
		global $_db_control, $_events;

		$this->_keys['data_aliases'] = $this->getDataAliases(null, null, null, 0, ATTR_CLASS_DATA_ALIAS);
		$this->_keys['taxonomy_aliases'] = $this->getDataAliases(null, null, null, 0, ATTR_CLASS_TAX_ALIAS);
		$this->_keys['admin_aliases'] = $this->getDataAliases(null, null, null, 0, ATTR_CLASS_ADMIN_ALIAS);
		$this->_keys['page_aliases'] = $this->getDataAliases();
		$this->_keys['datatables'] = $_db_control->getDataTables();
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Return either the entire data alias structure or data alias set for a single datatype
	 * @param string $datatype [optional]   If provided, only the data alias set for this datatype will be returned
	 * @param string|array $datafields      If provided, are prepended to the returned fields list
	 * @param integer $id                   If provided, causes getDataAliases to get the alias by id
	 * @param string $alias                 If provided, causes getDataAliases to get the alias by code
	 * @param string $class                 If provided, limits the result to attribute to attribute_class
	 * @return array                        Data alias set
	 */
	function getDataAliases($datatype = "", $datafields = null, $alias = null, $id = 0, $class = 'data'){
	    global $_db_control;

	    $aliases = array();
	    if(DB_USED){
	        $data_tables = $_db_control->getDataTables();
	        $datatype = trim(str_replace(DB_TABLE_PREFIX, "", $datatype));
	        $crit = "`published` = 1";
	        $fields = "";
	        $id = intval($id);
	        if(!empty($datatype) && in_array(DB_TABLE_PREFIX.$datatype, $data_tables)) {
	            $crit .= " AND `data_type` = '".$datatype."'";
	            if(is_array($datafields)) $fields = "`".join("`, `", $datafields)."`, ";
	        }
	        if($id > 0)
	            $crit .= " AND `attribute_id` = '$id'";
	        if(!is_null($alias))
	            $crit .= " AND `alias` = '$alias'";
	        if($class != 'data'){
	            $crit .= " AND `attribute_class` = '$class'";
	        }else{
	            $crit .= " AND `attribute_class` = 'data'";
	        }

	        $rec = $_db_control->setTable(ATTRIBUTES_TABLE)->setFields($fields."`attribute_id`, `data_id`, `alias`, `data_type`")->setWhere($crit)->setOrder("`data_type`")->getRec();
	        if(is_array($rec)){
	            foreach($rec as $row){
	                list($row['pattern'], $row['error']) = $this->convertDataAliasRuletoPattern($row['alias']);
	                $aliases[$row['attribute_id']] = $row;
	            }
	        }
	    }
	    switch($class){
	    	case ATTR_CLASS_ADMIN_ALIAS:
			    $this->_subkeys['admin_aliases'] = $aliases;
	    		break;
	    	case ATTR_CLASS_DATA_ALIAS:
			    $this->_subkeys['data_aliases'] = $aliases;
	    		break;
	    	case ATTR_CLASS_TAX_ALIAS:
			    $this->_subkeys['taxonomy_aliases'] = $aliases;
	    		break;
	    }
	    return $aliases;
	}

	/**
	 * Return the derived alias for a destination.  First checking the register, then generating the path from the parameters
	 * @param string $datatype                  The data type (table)
	 * @param string $targettype                The target type (term, link...)
	 * @param string $action                	The target action (add, edit, list...)
	 * @return string
	 */
	function getAdminDataAlias($datatype, $targettype, $action = null){
	    global $_db_control;

	    if(!empty($datatype) && !empty($targettype)){
	        $datatype = strtolower($datatype);
	        $targettype = strtolower($targettype);
	        $target = null;

	        $rec = $_db_control->setTable(ATTRIBUTES_TABLE)->setFields("`alias`")->setWhere("`attribute_class` = '".ATTR_CLASS_ADMIN_ALIAS."' AND `data_type` = '$datatype' AND `key` = '$targettype'")->getRec();
	        if(count($rec) > 0){
	            $target = ADMIN_FOLDER.$rec[0]['alias'];
	        }else{
	            $target = ADMIN_FOLDER.$datatype.prependSlash($targettype).prependSlash($action);
	        }

	        return $target;
	    }else{
	        return null;
	    }
	}

	/**
	 * Check if the provided data alias already exists for the specified data type, and return true if it does not exist
	 * @param string $datatype
	 * @param string $alias
	 * @param integer $current_id 				The index id from the data table to compare with
	 * @return boolean 							True if not used, false if match found
	 */
	function checkDataAliasExists($datatype, $alias, $current_id = 0){
		global $_db_control, $_error;

		$ok = true;
        if($_db_control->setTable(ATTRIBUTES_TABLE)->setFields("data_id")->setWhere("attribute_class = 'data' AND alias = '".$alias."' AND data_id != '".$current_id."'")->getRecNumRows() > 0 && !empty($alias)) {
    		$_error->addErrorMsg("The alias `".strtoupper($alias)."` already exists.");
    		$ok = false;
        }
        return $ok;
	}

	/**
	 * Check if the provided code already exists for the specified data type, and return true if it does not exist
	 * @param string $datatype
	 * @param string $code
	 * @param integer $current_id 				The index id from the data table to compare with
	 * @return boolean 							True if not used, false if match found
	 */
	function checkAliasExists($datatype, $code, $current_id = 0){
		global $_db_control, $_error;

		$ok = true;
        if($_db_control->setTable($datatype)->setFields("id")->setWhere("code = '".$code."' AND id != '".$current_id."'")->getRecNumRows() > 0 && !empty($code)) {
    		$_error->addErrorMsg("The code `".strtoupper($code)."` already exists.");
    		$ok = false;
        }
        return $ok;
	}

	/**
	 * Convert Data Alias Rule (/events/@year@/@month@/@day@/@code@) to
	 * pattern (/events/(\d{4})/(\d{2})/(\d{2})/([^\/]+)$).
	 * Rule -- the human-readable version of the pattern regex (data-type/literal/@meta-tags@)
	 * Pattern -- the actual regex used to parse urls
	 * @param string $rule
	 * @return array (pattern, error)
	 */
	function convertDataAliasRuletoPattern($rule){
	    $pattern = '';
	    $error = '';
	    if($rule != ''){
	        // break rule up by directory separator (/)
	        $rule_parts = explode("/", ltrim(strtolower(str_replace('\@', 'à', $rule)), "/"));
	        $patt_parts = array();
	        $rule_started = false;
	        foreach($rule_parts as $rule_piece){
	            switch($rule_piece){
	                case DA_TERM:       // single category
	                case DA_CODE:
	                    $patt_parts[] = "([^/]+)";
	                    $rule_started = true;
	                    break;
	                case DA_TERM_ID:
	                case DA_ID:
	                    $patt_parts[] = "(\d)";
	                    $rule_started = true;
	                    break;
	                case DA_MONTH:
	                case DA_DAY:
	                case DA_HOUR:
	                case DA_MINUTE:
	                case DA_SECOND:
	                    $patt_parts[] = "(\d{2})";
	                    $rule_started = true;
	                    break;
	                case DA_YEAR:
	                    $patt_parts[] = "(\d{4})";
	                    $rule_started = true;
	                    break;
	                default:
	                    if(!$rule_started){
	                        $patt_parts[] = $rule_piece;
	                    }elseif(strpos($rule_piece, '@') !== false){
	                        if($error == ''){
	                            $error = 'Error at \''.$rule_piece.'\'';
	                        }
	                    }
	                    break;
	            }
	        }
	        $pattern = str_replace('à', '\@', "/".join("/", $patt_parts)."$");
	    }
	    return array($pattern, $error);
	}

	/**
	 * Convert Data Alias pattern (/events/(\d{4})/(\d{2})/(\d{2})/([^\/]+)$) to
	 * sample URL (/events/2012/12/05/word).
	 * Pattern -- the actual regex used to parse urls
	 * @param string $url
	 * @internal Core function
	 */
	function createSampleURLFromDataPattern($pattern){
	    $url = '';
	    $error = '';
	    if($pattern != ''){
	        // convert /] to something different so that the / isn't seen as a delimiter
	        $pattern = str_replace("/]", "|]", $pattern);

	        // break pattern up by directory separator (/)
	        $patt_parts = explode("/", ltrim(strtolower($pattern), "/"));
	        $url_parts = array();
	        $patt_started = false;
	        foreach($patt_parts as $patt_piece){
	            switch($patt_piece){
	                case "([^|]+)":
	                    $url_parts[] = "sample";
	                    $patt_started = true;
	                    break;
	                case "(.*)":
	                    $url_parts[] = "category";
	                    $patt_started = true;
	                    break;
	                case "(\d)":
	                    $url_parts[] = strval(rand(1, 100));
	                    $patt_started = true;
	                    break;
	                case "(\d{2})":
	                    $url_parts[] = strval(rand(1, 12));
	                    $patt_started = true;
	                    break;
	                case "(\d{4})":
	                    $patt_parts[] = strval(rand(2000, date("Y")));
	                    $patt_started = true;
	                    break;
	                default:
	                    if(!$patt_started){
	                        $url_parts[] = $patt_piece;
	                    }else{
	                        if($error == ''){
	                            $error = 'Error at \'/'.substr($patt_piece, 0, 5).'\'';
	                        }
	                    }
	                    break;
	            }
	        }
	        $url = "/".join("/", $url_parts);
	    }
	    return array($url, $error);
	}

	/**
	 * Return whether a Data Alias Rule (represented by specific meta tags) is for categorized data
	 * or record data
	 * @param string $rule
	 */
	function dataAliasIsCategory($rule){
	    // a category rule has @cat@/@catid@ as the first meta-tag (no matter
	    // how many literal segments are found ahead of it)
	    $rule_parts = explode("/", ltrim(strtolower($rule), "/"));
	    $cat_tag_found = false;
	    $non_cat_tag_found = false;
	    foreach($rule_parts as $indx => $rule_piece){
	        if(($rule_piece == DA_TERM || $rule_piece == DA_TERM_ID) && !$cat_tag_found){
	            // @cat@ or @catid@ tag found for the first time
	            $cat_tag_found = true;
	        }elseif(preg_match("/@([^@]+)@/i", $rule_piece)){
	            // anything else found ensures this rule is not
	            // able to properly parse a category URI
	            $non_cat_tag_found = true;
	        }
	    }
	    return ($cat_tag_found && !$non_cat_tag_found);
	}

	/**
	 * Check if a sanitized alias already exists.  Return the id if it does.  Create a new record if not.
	 * @param array $args                   An array of attributes to save
	 *                                          - data_type
	 *											- attribute_class ('data', '_admin_alias', '_data_alias')
	 *											- newalias (the new alias pattern)
	 *											- alias_id (record id of this alias, required if updating the alias)
	 * @param integer $id
	 */
	function saveDataAlias($args){
	    global $_db_control, $_events;

	    if(!is_array($args)) return false;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	    $data_type = getIfSet($args['data_type']);
	    $attribute_class = getIfSet($args['attribute_class']);
	    $newalias = strtolower(preg_replace("/[^a-z0-9\-_\/\@]/i", "", getIfSet($args['newalias'])));
	    $id = getIntValIfSet($args['alias_id'], 0);
	    if(!empty($data_type) && !empty($newalias) && !empty($attribute_class)){
            $new_id = $_db_control->setTable(ATTRIBUTES_TABLE)->setFieldvals(array("data_type" => $data_type, "alias" => $newalias, "data_id" => 0, "attribute_class" => $attribute_class))->setWhere("attribute_id = '".$id."'")->replaceRec();
            if($new_id > 0) $id = $new_id;
	    }
	    $this->initDataObjects();
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	    return $id;
	}

	/**
	 * Delete an alias and disassociate all related objects (menus, etc.)
	 * @param integer $id
	 */
	function deleteAlias($id){
		global $_db_control, $_events;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
		if($_db_control->setTable(ATTRIBUTES_TABLE)->setWhere("attribute_id = '".$id."'")->deleteRec()){
			// reset all related menus and taxonomies
			$_db_control->setTable(MENUS_TABLE)->setFieldvals(array("alias" => 0))->setWhere("alias = '".$id."'")->updateRec();
			$_db_control->setTable(TAXONOMIES_TABLE)->setFieldvals(array("data_alias" => 0))->setWhere("data_alias = '".$id."'")->updateRec();
		}
		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	}

	/**
	 * Populate the data class with a recordset
	 * @param array $rec
	 * @param boolean $newclass     True to instantiate a new class thus leaving $_data unchanged
	 * @internal Core function
	 * @return array
	 */
	function setupData($rec, $newclass = false){
	    global $_data, $_events;

		$_events->processTriggerEvent(__FUNCTION__);				// alert triggered functions when function executes
	    if($newclass){
	        $d = DataClass::init();
	    }else{
	        $d = $_data;
	    }

	    $d->dbrec = $rec;
	    if(isset($rec['id'])){
	        $d->id = $rec['id'];
	    }elseif(isset($rec[0]['id']) && count($rec) == 1){
	        $d->id = $rec[0]['id'];
	    }else{
	        $d->id = 0;
	    }
	    if(isset($rec['published']) == 1){
	        $d->ispublished = true;
	    }elseif(isset($rec[0]['published']) && count($rec) == 1){
	        $d->ispublished = true;
	    }else{
	        $d->ispublished = false;
	    }
	    if(isset($rec['draft']) == 1){
	        $d->isdraft = true;
	    }elseif(isset($rec[0]['draft']) && count($rec) == 1){
	        $d->isdraft = true;
	    }else{
	        $d->isdraft = false;
	    }
	    $d->created = multiarray_search('date_created', $rec, true);
	    $d->updated = multiarray_search('date_updated', $rec, true);
	    $d->published = multiarray_search('date_published', $rec, true);
	    $d->iscategory = (multiarray_search('cat_id', $rec, true) === false);
	    $d->numrows = count($rec);
	    $d->nonce = '';
	    $d->rulebase = '';
	    $d->datatype = '--';
	    $d->childdatatype = '--';
	    $d->query = '';
	    $d->queryvars = array();
	    $d->found = true;

		$_events->processTriggerEvent(__FUNCTION__.'_done');				// alert triggered functions when function executes
	    return $d;
	}

	/**
	 * Return array of pages from database
	 * @param string $args
	 * @param string $sort                  Direction to which pagetitles are sorted. (ASC or DESC)
	 * @param string $limit                 The SQL limiting condition. ("offset, totalrows")
	 * @param string $returntype            What to return. (menu, selectmenu, list, divlist, titlearray, namearray)
	 */
	function getPageAliases($args, $sort="asc", $limit="", $returntype="", $addblank=true){
	    global $_db_control;

	    $sqlargs = str_replace(",", " AND ", $args);
	    $pages = $_db_control->setTable(PAGES_TABLE)->setWhere($sqlargs)->setOrder(PAGES_TABLE.".pagetitle ".$sort)->setLimit($limit)->getData();
	    $data = array();
	    if(count($pages) > 0 && is_array($pages)){
	        array_unshift($data, "- No page -");
	        switch($returntype){
	            case 'menu':
	            case 'selectmenu':
	                foreach($pages as $page){
	                    $data[] = '<option value="'.$page['id'].'">'.$page['pagetitle'].'</option>';
	                }
	                if($addblank) array_unshift($data, '<option value="">- Select Page -</option>');
	                return join(PHP_EOL, $data);
	            case 'list':
	                foreach($pages as $page){
	                    $data[] = '<li id="p'.$page['id'].'">'.$page['pagetitle'].'</li>';
	                }
	                if($addblank) array_unshift($data, '<li id="p0">- Select Page -</li>');
	                return join(PHP_EOL, $data);
	            case 'divlist':
	                foreach($pages as $page){
	                    $data[] = '<div id="'.$page['id'].'"><span>'.$page['pagetitle'].'</span></div>';
	                }
	                array_unshift($data, '<div id="p0">- Select Page -</div>');
	                return join(PHP_EOL, $data);
	            case 'titlearray':
	                foreach($pages as $page){
	                    $data[$page['id']] = $page['pagetitle'];
	                }
	                return $data;
	            case 'namearray':
	                foreach($pages as $page){
	                    $data[$page['id']] = $page['pagename'];
	                }
	                return $data;
	        }
	    }
	    return $data;
	}

	public function __get($name){
		$v = null;
		if(array_key_exists($name, $this->_keys)){
			// return scalar value
			$v = $this->_keys[$name];
		}elseif(array_key_exists($name, $this->_subkeys)){
			// return subarray value
			$v = $this->_subkeys[$name];
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot get '$name'.  It is not a valid _DATA property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
		return $v;
	}

	public function __set($name, $value){
		if(array_key_exists($name, $this->_keys)){
			// set scalar value
			$this->_keys[$name] = $value;
		}elseif(array_key_exists($name, $this->_subkeys)){
			// set subarray value
			$this->_subkeys[$name][] = $value;
		}else{
			$d = debug_backtrace();
			trigger_error("Cannot set '$name'.  It is not a valid _DATA property, called on line ".$d[0]['line']." in ".$d[0]['file'].". ");
		}
	}
}

?>