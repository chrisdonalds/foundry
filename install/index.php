<?php
// ---------------------------
//
// SYSTEM INSTALLER
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2012  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------
if(!isset($_SERVER['HTTP_HOST']) || empty($_SERVER['HTTP_HOST']) || php_sapi_name() == 'cli'){
    die('The installer cannot be started from a PHP-CLI/FRM instance.');
}

// Load the Foundry engine in minimized, database-disabled, installer mode
define("BASIC_GETINC", true);
define("DB_USED", false);
define("IS_INSTALLER", true);
define("QUICK_LOAD", true);

if(!defined('LOADER_DOCUMENT_ROOT')){
    include("../inc/_core/loader.php");
}

define("FORM_BTNS", '<a href="?s=back" id="backbtn" class="backcls btn" rel="backrel">< Back</a><a href="?s=adv" id="advbtn" class="advcls btn" rel="advrel">Continue ></a>');
$const = get_defined_constants();
$maxstages = 5;
$stage = ((!empty($_REQUEST['s'])) ? intval($_REQUEST['s']) : 1);
$stageLabels = array(
    1 => 'Region',
    2 => 'Database',
    3 => 'Site Info',
    4 => 'Account Info',
    5 => 'Review',
    6 => 'Complete'
);

// Validate
validateInstallRerun();
validateStep();

// Ensure step being loaded does not skip any previous stage
while(!validateLoad($stage) && $stage > 1) $stage--;

// Go
installStep($stage);

/**
 * Builds Installer page
 * @param string $stage
 */
function installStep($stage = 1){
    global $const, $stage, $maxstages, $_sec;

    $logo = WEB_URL.ADMIN_FOLDER."images/logo/logo.png";
    $nonce = $_sec->createNonce(WEB_URL.$stage);
    $ajax_url = $const['WEB_URL']."install/installajax.php";
    $nextstage = $stage + 1;

// if($stage == 1)     session_unset();

    $_SESSION['installer']['errblock'] = null;
    if(!empty($_SESSION['installer']['err'])){
        $_SESSION['installer']['errblock'] = '<p class="error">The following problem(s) need to be resolved: <br/>- '.join('<br/>- ', $_SESSION['installer']['err']).'</p>';
    }

    if($stage < $maxstages)
        $title = 'Step '.$stage;
    elseif($stage == $maxstages)
        $title = 'Review';
    else
        $title = 'Completing...';

    echo <<<EOT
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>{$const['SYS_NAME']} {$const['CODE_VER']} Installer - {$title}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="content-language" content="EN" />
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Expires" content="-1"/>
        <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.js"></script>
        <link href="{$const['WEB_URL']}{$const['ADMIN_FOLDER']}{$const['JS_FOLDER']}ui/ui.theme.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="{$const['WEB_URL']}install/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="{$const['WEB_URL']}install/css/installer.css" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" language="javascript" src="{$const['WEB_URL']}{$const['ADMIN_FOLDER']}{$const['JS_FOLDER']}ui/ui.script.js"></script>
        <script type="text/javascript" language="javascript" src="{$const['WEB_URL']}{$const['ADMIN_FOLDER']}{$const['JS_FOLDER']}ui/ui.dialog.js"></script>
        <script type="text/javascript" language="javascript" src="{$const['WEB_URL']}install/script.js"></script>
    </head>

<body>
    <div id="wrapper">
        <div id="content-wrapper">
            <div id="contentarea">
                <div id="logo"><img src="{$logo}" alt="{$const['SYS_NAME']} Logo" /></div>
                <form action="" method="POST" id="installform" class="form">
                    <input type="hidden" id="install_ajax_url" value="{$ajax_url}" />
                    <input type="hidden" name="curstage" id="curstage" value="{$stage}" />
                    <input type="hidden" name="n" id="nonce" value="{$nonce}" />
                    <input type="hidden" name="s" id="stage" value="{$nextstage}" />
                    <input type="hidden" name="back" id="back" value="" />
EOT;
    echo PHP_EOL;

    if($_SESSION['installer']['ok_to_install']){
        if($stage == $maxstages)
            installSectionLast();
        elseif($stage > $maxstages)
            installExecute();
        else
            call_user_func('installSection'.$stage);
        echo PHP_EOL;
    }else{
    echo <<<EOT
                    <h1>{$const['SYS_NAME']} Installer</h1>
                    <p>
                        We have detected that {$const['SYS_NAME']} has already been installed in this environment.
                    </p>
                    <p>
                        The installation will not continue...
                    </p>
                    <p>
                        You can either <a href="{$const['WEB_URL']}" class="bold">view the site</a> or <a href="{$const['WEB_URL']}{$const['ADMIN_FOLDER']}" class="bold">login to the admin area</a>.
                    </p>
EOT;

    }

    echo <<<EOT
                </form>
            </div>
        </div>
EOT;

    echo PHP_EOL;
    echo <<<EOT
        </body>
    </html>
EOT;
}

/**
 * Installer section
 */
function installSection1(){
    global $const, $stage;

    $formbtns = getFormButtons();
    $progress = getStageProgress();
    $tzdata = getTimezones();
    $tzgroup = '';
    $tzmenu = '<select name="timezone">';
    $cfgtimezone = getIfSet($_system->configs['TIMEZONE']);
    $timezone = getValue('timezone');
    $language = getValue('language');

    foreach($tzdata as $tzkey => $tz){
        $tzkey_parts = explode("/", $tzkey);
        if($tzgroup != $tzkey_parts[0]) {
            $tzmenu .= (($tzgroup != '') ? '</optgroup>'.PHP_EOL : '').'<optgroup label="'.ucwords($tzkey_parts[0]).'">'.PHP_EOL;
            $tzgroup = $tzkey_parts[0];
        }
        $tzmenu .= '<option value="'.$tzkey.'"'.(($tzkey == $timezone) ? ' selected="selected"' : '').'>'.$tz.'</option>'.PHP_EOL;
    }
    $tzmenu .= '</optgroup></select>'.PHP_EOL;

    $langdata = array(
        "en-US" => "English (US)",
        "en-CA" => "English (Canada)",
        "en-UK" => "English (UK)",
    );
    $langmenu  = '<select name="language">';
    foreach($langdata as $langkey => $lang){
        $langmenu .= '<option value="'.$langkey.'"'.(($langkey == $language) ? ' selected="selected"' : '').'>'.$lang.'</option>';
    }
    $langmenu .= '</select>';

    echo <<<EOT
                    <h1>Welcome to {$const['SYS_NAME']} {$const['CODE_VER']}</h1>
                    {$progress}
                    {$_SESSION['installer']['errblock']}
                    <p>{$const['SYS_NAME']} is a robust website content management system, crafted with a fine balance between ease of use to get started quickly, and feature richness to carry your site to the highest levels.</p>
                    <p>In a moment your website will be ready for use.  The first step is to setup some regional information.</p>

                    <p class="cfgblock">
                        <label for="region_lang">Language</label><span class="cfgdata">{$langmenu}</span>
                    </p>
                    <p class="cfgblock">
                        <label for="region_tz">Timezone</label><span class="cfgdata">{$tzmenu}</span>
                    </p>
                    <p class="cfgblock"></p>
                    <p class="cfgbuttons">
                        {$formbtns}
                    </p>
EOT;
}

/**
 * Installer section
 */
function installSection2(){
    global $const, $stage;

    $formbtns = getFormButtons();
    $progress = getStageProgress();
    $db_host  = getValue('db_host');
    $db_name  = getValue('db_name');
    $db_user  = getValue('db_user');
    $db_pass  = getValue('db_pass');
    $db_port  = getValue('db_port');
    echo <<<EOT
                    <h1>{$const['SYS_NAME']} Installer - Step {$stage}</h1>
                    {$progress}
                    {$_SESSION['installer']['errblock']}
                    <p>We're going to need some information to establish a connection with the database that will serve the site.</p>
                    <p>If you don't have the database connection information at this point, perhaps you can ask your web host or system administrator for it.</p>
                    <p class="cfgblock">
                        <label for="db_domain">Domain</label><span class="cfgdata"><input type="text" name="db_domain" id="db_domain" value="{$_SERVER['HTTP_HOST']}" readonly="readonly" style="background-color: #ddd"/></span><span class="cfgnote"></span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="db_host">Database Host</label><span class="cfgdata"><input type="text" name="db_host" id="db_host" value="{$db_host}"/></span><span class="cfgnote">Some servers accept <b>localhost</b> as the host.  Otherwise you will need to contact your provider.</span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="db_name">Database Name</label><span class="cfgdata"><input type="text" name="db_name" id="db_name" value="{$db_name}"/></span><span class="cfgnote">The name of the database you want to use with this site.</span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="db_user">Username</label><span class="cfgdata"><input type="text" name="db_user" id="db_user" value="{$db_user}"/></span><span class="cfgnote">The username which grants access to this database.<br/>It must be granted SELECT, INSERT, DELETE, UPDATE, CREATE and ALTER privileges.</span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="db_pass">Password</label><span class="cfgdata"><input type="password" name="db_pass" id="db_pass" value=""/></span><span class="cfgnote">The password for that username.</span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="db_port">Port</label><span class="cfgdata"><input type="text" name="db_port" id="db_port" value="{$db_port}"/></span><span class="cfgnote">Typically set to <b>3306</b>.</span>
                    </p>
                    <p class="cfgblock"></p>
                    <p>
                        For your convenience, once a successful connection to the database has been made, this configuration will be made available to CLI processes.
                        <br/>
                        Note: ensure that the <strong>{$const['ADMIN_FOLDER']}{$const['CONFIG_FOLDER']}db.ini</strong> file is writable (CHMOD 777) ...
                    </p>
                    <p class="cfgbuttons">
                        {$formbtns}
                    </p>
EOT;
}

/**
 * Installer section
 */
function installSection3(){
    global $const, $stage;

    $formbtns = getFormButtons();
    $progress = getStageProgress();
    $site_name  = getValue('site_name');
    $site_byline  = getValue('site_byline');
    $business  = getValue('business');
    $bus_address  = getValue('bus_address');
    $bus_phone  = getValue('bus_phone');
    $owner_email  = getValue('owner_email');
    echo <<<EOT
                    <h1>{$const['SYS_NAME']} Installer - Step {$stage}</h1>
                    {$progress}
                    {$_SESSION['installer']['errblock']}
                    <p>Now, let's give your site a name, and if this is a commercial site, you are welcome to provide some business information as well.</p>
                    <p class="cfgblock">
                        <label for="site_name">Site Name</label><span class="cfgdata"><input type="text" name="site_name" id="site_name" value="{$site_name}" /></span><span class="cfgnote">This name is typically displayed on the home page.</span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="site_byline">Site Byline</label><span class="cfgdata"><input type="text" name="site_byline" id="site_byline" value="{$site_byline}" /></span><span class="cfgnote">Now's your chance to add your business slogan.</span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="business">Business Name</label><span class="cfgdata"><input type="text" name="business" id="business" value="{$business}" /></span><span class="cfgnote"></span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="bus_address">Business Address</label><span class="cfgdata"><textarea name="bus_address" id="">{$bus_address}</textarea></span><span class="cfgnote"></span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="bus_phone">Business Phone</label><span class="cfgdata"><input type="text" name="bus_phone" id="bus_phone" value="{$bus_phone}" /></span><span class="cfgnote"></span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="owner_email">Business Email</label><span class="cfgdata"><input type="text" name="owner_email" id="owner_email" value="{$owner_email}" /></span><span class="cfgnote"></span><br/>
                    </p>
                    <p class="cfgblock"></p>
                    <p class="cfgbuttons">
                        {$formbtns}
                    </p>
EOT;
}

/**
 * Installer section
 */
function installSection4(){
    global $const, $stage;

    $formbtns = getFormButtons();
    $progress = getStageProgress();
    $admin_user  = getValue('admin_user');
    $admin_pass  = getValue('admin_pass');
    $admin_email  = getValue('admin_email');
    $twitter_link  = getValue('twitter_link');
    $facebook_link  = getValue('facebook_link');
    $googleplus_link  = getValue('googleplus_link');
    echo <<<EOT
                    <h1>{$const['SYS_NAME']} Installer - Step {$stage}</h1>
                    {$progress}
                    {$_SESSION['installer']['errblock']}
                    <p>We're almost done.</p>
                    <p>One of the last steps is to create an initial administrative account that will be used to access all operations of the site.  Later, additional user accounts may be prepared for other individuals.</p>
                    <p class="cfgblock">
                        <label for="admin_user">Admin Username</label><span class="cfgdata"><input type="text" name="admin_user" id="admin_user" value="{$admin_user}" /></span><span class="cfgnote"></span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="admin_pass">Admin Password</label><span class="cfgdata"><input type="password" class="password" name="admin_pass" id="admin_pass" value="" /><input type="text" class="pass_hidden hidden" name="admin_pass_hidden" id="admin_pass_hidden" value="" />
                        <span class="pass_strength"></span></span>
                        <span class="cfgnote">
                            <a href="#" class="pass_generate"><i class="fa fa-key"></i> Generate</a>&nbsp;
                            <a href="#" class="pass_view"><i class="fa fa-eye"></i> View</a>
                        </span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="admin_pass">Confirm Password</label><span class="cfgdata"><input type="password" name="admin_pass_confirm" id="admin_pass_confirm" value="" /></span><span class="cfgnote">Enter the password again...</span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="admin_email">Admin Email</label><span class="cfgdata"><input type="text" name="admin_email" id="admin_email" value="{$admin_email}" /></span><span class="cfgnote">This email address may be used to send important site notifications.</span><br/>
                    </p>
                    <p>The following entries are completely optional.  They are there to build a relationship between popular social media sites and this one.</p>
                    <p class="cfgblock">
                        <label for="facebook_link">Facebook URL</label><span class="cfgdata"><input type="text" name="facebook_link" id="facebook_link" value="{$facebook_link}" /></span><span class="cfgnote"></span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="twitter_link">Twitter Link</label><span class="cfgdata"><input type="text" name="twitter_link" id="twitter_link" value="{$twitter_link}" /></span><span class="cfgnote"></span><br/>
                    </p>
                    <p class="cfgblock">
                        <label for="googleplus_link">Google+ Link</label><span class="cfgdata"><input type="text" name="googleplus_link" id="googleplus_link" value="{$googleplus_link}" /></span><span class="cfgnote"></span><br/>
                    </p>
                    <p class="cfgblock"></p>
                    <p class="cfgbuttons">
                        {$formbtns}
                    </p>
EOT;
}

/**
 * Installer section
 */
function installSectionLast(){
    global $const, $stage;

    $formbtns = getFormButtons();
    $progress = getStageProgress();
    $review = null;
    foreach($_SESSION['installer']['values'] as $k => $v){
        $_SESSION['installer']['values'][$k] = strip_tags($v);
        if(strpos($k, 'pass') !== false && !empty($v)) $v = '******';
        if(strpos($k, 'confirm') !== false || strpos($k, 'hidden') !== false || in_array($k, array("op")) || preg_match("/<(.*)>/", $v)){
            unset($_SESSION['installer']['values'][$k]);
            continue;
        }
        $review .= '<span class="reviewlabel">'.ucwords(str_replace("_", " ", $k)).'</span><span>'.$v.'</span><br/>';
    }
    echo <<<EOT
                    <h1>{$const['SYS_NAME']} Installer - Review</h1>
                    {$progress}
                    {$_SESSION['installer']['errblock']}
                    <p>Now, take a moment to review the various settings and, if needed, go back and make any changes.</p>
                    <p class="cfgblock">{$review}</p>
                    <p class="cfgblock"></p>
                    <p class="cfgbuttons">
                        {$formbtns}
                    </p>
EOT;
}

/**
 * Finalize stages and task each stage execution processor
 */
function installExecute(){
    global $const, $stage;

    $formbtns = getFormButtons();
    $progress = getStageProgress();
    $ajax_url = $const['WEB_URL']."install/installajax.php";
    $admin_url = $const['WEB_URL'].$const['ADMIN_FOLDER'];
    echo <<<EOT
                    <h1>{$const['SYS_NAME']} Installer - Completion</h1>
                    {$progress}
                    {$_SESSION['installer']['errblock']}
                    <p>We've collected everything needed to setup {$const['SYS_NAME']}.  Once the setup is complete you will be taken to the administration login.</p>
                    <input type="hidden" id="admin_url" value="{$admin_url}" />
                    <p id="cfginstallprocess" class="cfgblock"><strong class="cfgbold">Please wait while your site is being prepared...</strong></p>
                    <p class="cfgbuttons">
                        {$formbtns}
                    </p>
EOT;
}


/**
 * Installer section validation rule
 * @return array $rules
 */
function getValidateRules1(){
    $rules = array(
        "timezone" => array("test" => "notblank", "msg" => "The default timezone is missing"),
        "language" => array("test" => "notblank", "msg" => "A system language is missing")
    );
    return $rules;
}

/**
 * Installer section validation rule
 * @return array $rules
 */
function getValidateRules2(){
    $rules = array(
        "db_host" => array("test" => "notblank", "msg" => "Database host is missing"),
        "db_name" => array("test" => "notblank", "msg" => "Database name is missing"),
        "db_user" => array("test" => "notblank", "msg" => "Database user name is missing"),
    );
    return $rules;
}

/**
 * Installer section validation rule
 * @return array $rules
 */
function getValidateRules3(){
    $rules = array(
        "site_name" => array("test" => "notblank", "msg" => "The site's name is missing"),
        "business" => array("test" => "notblank", "msg" => "The business name is missing"),
        "owner_email" => array("test" => "notblank", "msg" => "The business email address is missing")
    );
    return $rules;
}

/**
 * Installer section validation rule
 * @return array $rules
 */
function getValidateRules4(){
    $rules = array(
        "admin_user" => array("test" => "notblank", "msg" => "Administrator account username is missing"),
        "admin_pass" => array("test" => "notblank", "msg" => "Administrator account password is missing"),
        "admin_pass_confirm" => array("test" => "match", "field" => "admin_pass", "msg" => "Administrator account passwords do not match"),
        "admin_email" => array("test" => "notblank", "msg" => "Administrator account email address is missing"),
    );
    return $rules;
}

/**
 * Installer section validation rule
 * @return array $rules
 */
function getValidateRules5(){
    $rules = array(
    );
    return $rules;
}

/* HELPER FUNCTIONS */

/**
 * Check if current page was called from a previously completed step
 * @param integer $stage
 * @return boolean
 */
function validateLoad($stage){
    global $const, $_sec;

    $ok = true;
    if($stage > 1)
        if(!isset($_SESSION['installer']['stagesdone'][$stage - 1]) || $_SESSION['installer']['stagesdone'][$stage - 1] != true) $ok = false;
    return $ok;
}

/**
 * Validate the individual steps, checking the nonce value, and updating the session if the step validates successfully
 * @return boolean
 */
function validateStep(){
    global $const, $stage, $_sec;

    $err = array();

    // check if form was submitted from expected process
    $laststage = getIntValIfSet($_POST['curstage']);
    if(!empty($_POST) && empty($_REQUEST['back']) && $laststage > 0){
        $nonce = $_sec->createNonce(WEB_URL.$laststage);
        if(empty($_POST['n'])){
            // nonce not provided
            $err[] = 'Invalid form submission.';
        }elseif($nonce != $_POST['n']){
            // nonce does not match
            $err[] = 'Invalid form submission.';
        }elseif(function_exists("getValidateRules".$laststage)){
            $rules = call_user_func("getValidateRules".$laststage);
            if(!empty($rules)){
                // check posted data against validation rules
                foreach($rules as $var => $rule){
                    switch($rule['test']){
                        case 'notblank':
                            if(empty($_POST[$var])) $err[] = $rule['msg'];
                            break;
                        case 'blank':
                            if(!empty($_POST[$var])) $err[] = $rule['msg'];
                            break;
                        case 'numeric':
                            if(!isset($_POST[$var]) || !numeric($_POST[$var])) $err[] = $rule['msg'];
                            break;
                        case 'alphanumeric':
                            if(!isset($_POST[$var]) || !preg_match("/([^a-zA-Z0-9])/", $var)) $err[] = $rule['msg'];
                            break;
                        case 'alphanumsymbols':
                            if(!isset($_POST[$var]) || !preg_match("/([^a-zA-Z0-9\!@#$%\^&\*\(\)_\+{}:\"<>\?,\./;'[]-=\]\)/", $var)) $err[] = $rule['msg'];
                            break;
                        case 'match':
                            if(!isset($_POST[$var]) || getIfSet($_POST[$rule['field']]) != $_POST[$var]) $err[] = $rule['msg'];
                            break;
                    }
                }
            }
        }
    }

    $ok = empty($err);
    if(!$ok) {
        // go back to last stage
        if(!empty($laststage)) $stage = $laststage;
    }else{
        foreach($_POST as $k => $v) if(!in_array($k, array("curstage", "n", "s", "back", "op"))) $_SESSION['installer']['values'][$k] = addslashes(trim($v));
    }
    $_SESSION['installer']['stagesdone'][$laststage] = $ok;
    $_SESSION['installer']['err'] = $err;
    return $ok;
}

/**
 * Determines whether or not it is safe to install the system.  The routine looks for a host record in the database
 * init file, and if found tries to log into the registered database.  If both are true, the installation cannot
 * continue.
 */
function validateInstallRerun(){
    $dbset = readDBINI();
    $ok_to_install = true;
    if(!empty($dbset)){
        $host = str_replace(array(PROTOCOL.'www.', PROTOCOL), '', SERVER);
        $hostext = substr(SERVER, -3);
        if(!empty($host) && isset($dbset[$host])){
            if(!isset($dbset[$host]['dbport']) || $dbset[$host]['dbport'] == 0) $dbset[$host]['dbport'] = 3306;
            foreach($dbset[$host] as $key => $val) define(strtoupper($key), $val);
            define ("DBHOSTPORT", ((DBPORT != 3306 && DBPORT > 0) ? DBHOST.":".DBPORT : DBHOST));
            $db_link = new mysqli(DBHOSTPORT, DBUSER, DBPASS, DBNAME);
            $ok_to_install = (empty($db_link));
        }
    }
    $_SESSION['installer']['ok_to_install'] = $ok_to_install;
}

/**
 * Return a formatted button block HTML
 * @return string
 */
function getFormButtons($buttonstr = null){
    global $stage, $maxstages, $const;

    if(empty($buttonstr)) $buttonstr = $const['FORM_BTNS'];
    if($stage == 1){
        // first
        $s = str_replace(array("backcls", "advcls", "backrel", "advrel", "?s=back", "?s=adv"), array("disabled", "", $stage-1, $stage+1, "", "?s=".($stage+1)), $buttonstr);
    }elseif($stage == $maxstages){
        // last
        $s = str_replace(array("backcls", "advcls", "backrel", "advrel", "Continue >", "?s=back", "?s=adv"), array("", "", $stage-1, $stage+1, "Install", "?s=".($stage-1)."&back=1", "?s=".($stage+1)), $buttonstr);
    }elseif($stage > $maxstages){
        // complete
        $s = str_replace(array("backcls", "advcls", "backrel", "advrel", "Continue >", "?s=back", "?s=adv"), array("", "disabled", $stage-1, $stage+1, "Launch Site", "?s=".($stage-1)."&back=1", "?s=done"), $buttonstr);
    }else{
        // all other
        $s = str_replace(array("backcls", "advcls", "backrel", "advrel", "?s=back", "?s=adv"), array("", "", $stage-1, $stage+1, "?s=".($stage-1)."&back=1", "?s=".($stage+1)), $buttonstr);
    }
    return $s;
}

/**
 * Return the progress bar HTML
 * @return string
 */
function getStageProgress(){
    global $stage, $stageLabels;

    $html = '<div id="progress">';
    foreach($stageLabels as $i => $label){
        if($i < $stage)
            $html .= '<span class="progressdone">';
        elseif($i > $stage)
            $html .= '<span class="progresstodo">';
        else
            $html .= '<span class="progresscur">';
        $html .= $label.'</span>';
    }
    $html .= '</div>';
    return $html;
}

/**
 * An enhanced version of getIfSet() which also checks if the desired variable was saved to the $_SESSION
 * @param array $obj
 * @return mixed
 */
function getValue($obj){
    $val = null;
    if(isset($_SESSION['installer']['values'][$obj]))
        $val = $_SESSION['installer']['values'][$obj];
    else
        $val = getIfSet($_POST[$obj]);
    return $val;
}
?>