<?php
// ---------------------------
//
// INSTALLER AJAX PROCESSOR
//
// Author: Chris Donalds, cdonalds01@gmail.com
// Copyright (C) 2015  Chris Donalds

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// ---------------------------

define("IN_AJAX", true);
define("BASIC_GETINC", true);
define("DB_USED", false);
define("IS_INSTALLER", true);
define("QUICK_LOAD", true);

if(!defined('LOADER_DOCUMENT_ROOT')){
    include("../inc/_core/loader.php");
}

$op = strtolower(getRequestVar("op"));
$val = getRequestVar("val");
extractVariables($_REQUEST);
$_events->processTriggerEvent('installajaxcall', $op);                // alert triggered functions when function executes

switch($op){
    case 'checkpasswordstrength':
        $arry = $_sec->checkpasswordstrength($val);
        echo json_encode(array('success' => true, 'label' => $arry[0], 'strength' => $arry[1]));
        break;
    case 'getpassword':
        $opts = getRequestVar('opts');
        parse_str(urldecode($opts));
        $pwd = $_sec->createPassword(10);
        echo $pwd;
        break;
    case 'doinstall':
        $results = array();
        extractVariables($_SESSION['installer']['values']);
        $ok = true;
        ob_start();

        // first try to connect with database using settings provided during install interview
        $db_link = false;
        if(!empty($db_host) && !empty($db_name) && !empty($db_user) && isset($db_pass)){
            $db_hostport = $db_host;
            if(!empty($db_port) && $db_port != "3306") $db_hostport .= ":".$db_port;
            $db_link = new mysqli($db_hostport, $db_user, $db_pass, $db_name);
        }

        if($db_link->connect_errno == 0) {
            $results[] = "Database '".$db_name."' connected successfully.";

            if(function_exists('updateDBINI')){
                $retn = prepDBINI($db_domain, $db_host, $db_name, $db_user, $db_pass, $db_port);
                if(empty($retn)){
                    $results[] = "Database connection saved.";
                }else{
                    $results[] = "<span class=\"cfgerr\">Could not save database connection settings to db.ini. Error: ".$retn."</span>";
                    $ok = false;
                }
            }

            if($ok){
                // create database tables
                if(function_exists("checkDB")) {
                    checkDB();
                    $results[] = "System tables created successfully.";
                }else{
                    $results[] = "<span class=\"cfgerr\">Could not create system tables. Error: ".mysqli_connect_error()."</span>";
                    $ok = false;
                }

                // then save settings table values
                $vars = array(
                    "language",
                    "timezone",
                    "site_name",
                    "site_byline",
                    "business",
                    "bus_address",
                    "bus_phone",
                    "owner_email",
                );
                foreach($vars as $var){
                    $sql = "UPDATE `".SETTINGS_TABLE."` SET `value` = '".$_SESSION['installer']['values'][$var]."' WHERE `name` = '".strtoupper($var)."' LIMIT 1";
                    if(mysqli_query($db_link, $sql)){
                        $results[] = "Saved setting '".$var."'.";
                    }else{
                        $results[] = "<span class=\"cfgerr\">Could not save setting '".$var."'.  Error: ".mysqli_connect_error()."</span>";
                        $ok = false;
                    }
                }

                if($ok){
                    // and finally the admin user account
                    $password = $_sec->createHash($admin_pass, false);
                    $phash = '';
                    $pcle = md5($admin_user.$admin_pass);
                    $sql = "INSERT INTO `".ACCOUNTS_TABLE."` (`username`, `password`, `phash`, `pcle`, `level`, `email`, `facebook_link`, `twitter_link`, `google_plus_link`)
                        VALUES ('".$admin_user."', '".$password."', '".$phash."', '".$pcle."', '".ADMLEVEL_DEVELOPER."', '".$admin_email."', '".$facebook_link."', '".$twitter_link."', '".$googleplus_link."')";
                    if(mysqli_query($db_link, $sql)){
                        $results[] = "Saved admin user account.";
                    }else{
                        $results[] = "<span class=\"cfgerr\">Could not save admin user account.  Error: ".mysqli_connect_error()."</span>";
                        $ok = false;
                    }
                }
            }
        }else{
            $results[] = "<span class=\"cfgerr\">Could not connect with database.  Error: ".mysqli_connect_error()."</span>";
            $ok = false;
        }
        $html = join("<br/>", $results);
        if($ok) $html .= "<br/><br/><strong class=\"cfgbold\">Your new site will launch in a few seconds...</strong>";
        ob_end_clean();
        echo json_encode(array("success" => $ok, "html" => $html));
        break;
}
exit;
?>