jQuery(document).ready(function($){
    $('#advbtn').click(function(e){
        e.preventDefault();
        if(!$(this).hasClass('disabled'))
            $('#installform').submit();
    });

    $('.disabled').click(function(e){
        e.preventDefault();
    });

    if($('#cfginstallprocess').length){
        var install_ajax_url = $('#install_ajax_url').val();
        $.post(
            install_ajax_url,
            {'op':'doinstall'},
            function(jsonrtn){
                $('#cfginstallprocess').html(jsonrtn.html);
                if(jsonrtn.success){
                    setTimeout(function(){ window.location = $('#admin_url').val() }, 1000);
                    // window.location = $('#admin_url').val();
                }
            },
            "json"
        );
    }

    $(document).delegate('.password', 'keyup', function(){
        var install_ajax_url = $('#install_ajax_url').val();
        var elem = $(this);
        var pass = elem.val();
        $.post(
            install_ajax_url,
            {op:'checkpasswordstrength', val:pass},
            function(data){
                elem.siblings('.pass_strength').text(data.label).removeClass('state0 state1 state2 state3 state4 state5').addClass('state'+data.strength+' dottedborder');
            },
            'json'
        );
    });

    $(document).delegate('.pass_generate', 'click', function(e){
        e.preventDefault();
        var install_ajax_url = $('#install_ajax_url').val();
        var elem = $(this);
        var pwdfield = elem.parent().prev().find('.password');
        $.post(
            install_ajax_url,
            {op:'getpassword'},
            function(val){
                pwdfield.val(val).trigger('keyup');
            },
            'text'
        );
        return false;
    });

    $(document).delegate('.pass_view', 'mousedown', function(e){
        var elem = $(this);
        var pwdfield = elem.parent().prev().find('.password');
        var altfield = pwdfield.siblings('.pass_hidden');
        altfield.val(pwdfield.val());
        pwdfield.addClass('hidden');
        altfield.removeClass('hidden');
    });

    $(document).delegate('.pass_view', 'mouseup', function(e){
        var elem = $(this);
        var pwdfield = elem.parent().prev().find('.password');
        var altfield = pwdfield.siblings('.pass_hidden');
        pwdfield.removeClass('hidden');
        altfield.addClass('hidden');
    });

    $(document).delegate('.pass_view', 'click', function(e){
        e.preventDefault();
    });
});