--------------------------------------------------------------------------------------------------
                                        F O U N D R Y
                               Website Design and CMS Platform
--------------------------------------------------------------------------------------------------

Author: Chris Donalds, cdonalds01@gmail.com
Current Stable Version: 5.0.0 (alpha)
Copyright (C) 2015, Chris Donalds.

Foundry is a robust website content management system built on PHP and using MySQL.
It crafts a fine balance between ease of implementation so that non-technical users
can get started quickly, and feature richness and an unfettered PHP framework ideally
suited for the most advanced programmer.

Foundry leverages several web technologies such as jQuery and CKEditor to provide an
environment that smoothly presents content, while not bogging developers down with
bloated, cumbersome structure and rules that only increases programming time.

It:
- includes possibly the fastest database configuration and startup tool online
- is fully portable... because most professional sites are developed and deployed on
    different servers, it doesn't restrict you to one domain or configuration
- is built to grow with a dynamic plugin and framework installation system.  Enabled plugins can be selectively
    initiated where needed rather than automatically loaded on every URL.  You can actually test a plugin on
    one page without affecting the rest of the site.
- includes a powerful event triggering mechanism that allows developers to adjust logic easily
- is built following a true OOP architecture
- allows for quick frontend page creation.  Start a page with less than 10 lines of code
    (if you want to code)
- understands RSS, Atom, Analytics, mod rewrites, multiple editors, XML, SEO,
    trackbacks and pingbacks, mobile detection and more...
- can present page content via SEO-friendly URLs (called aliases), controllers (MVC),
    rewrite rule managed URLs, or direct file access
- understands the need for security with six standard user types and over 80 different allowances
    covering just about every aspect of the system
- comes ready to implement a dozen scripting frameworks from AngularJS to jQuery to Script.aculo.us.
    Activate them with one click.
- is not a blog system so there is no such thing as a pre-established "post"
    structure.  Add the Foundry Blog package and it quickly becomes one.
- does not limit how database tables are structured like most platforms.  Furthermore, your custom database
    tables can be automatically extended by attribute data such as published date, alias, metatags, password locking,
    and much more
- has a very small file footprint: about 5Mb loaded
- plays well with popular browsers and readers (this version supports IE(R) 7+, Firefox(R) 4+,
    Google(R) Chrome(tm), Opera(R), CSS 3, HTML 5)
- allows for multiple child-aware themes for BOTH the front and back ends, and multiple text
    editors (CKEditor(tm) is set as the default)
- has a dynamic error trapping and debugging subsystem that can be limited to show errors only to logged in users
    and not the public

This software contains several modules and supplied plugins, of which I give credit to, some of which are:

- JQuery(R) Validator Pack (Jörn Zaefferer)
- ImgEdit/Jcrop Plugin (Kelly Hallman)
- Browser Detector (Anthony Hand)

Individual plugins (/admin/inc/_plugins) may contain their own licenses and/or
requirements.

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Get the full text of the GPL here: http://www.gnu.org/licenses/gpl.txt

--------------------------------------------------------------------------------------------------
